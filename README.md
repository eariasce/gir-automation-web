## GIR-AUTOMATION-WEB

### Archetype
````text 
gir-automation-web/
├── src/
├── main/
│   ├── java/
│   ├── interbank.com.pe/
│   └── exceptions
│   └── interactions
│   └── models
│   └── questions
│   └── runners
│   └── stepsdefinitions
│   └── tasks
│   └── userinterface
│   └── utils
├── test/
├─ resources
│   ├── features
│   └── webdriver
├── serenity.conf
├── azure-pipelines.yml
├── pom.xml
├── README.md
└── serenity.properties
````

### Compile code Maven
Open a command window and run:
````
mvn clean
mvn clean verify
mvn verify -Denvironment=uat -Dcucumber.filter.tags="@TEST01"
mvn pmd:check

Command run local:
clean verify -Dcucumber.filter.tags=@TEST01
````
