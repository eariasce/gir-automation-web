package com.everis.giru.stepsdefinitions.historial;

import com.everis.giru.rest.steps.ConsultaFilterInboxSteps;
import com.everis.giru.tasks.atencionCliente.BuscarTramiteBandejaResolucion;
import com.everis.giru.tasks.atencionCliente.SubirArchivo;
import com.everis.giru.tasks.login.*;
import com.everis.giru.tasks.retipificacion.Retipificacion;
import com.everis.giru.tasks.tramites.*;
import com.everis.giru.userinterface.atencionCliente.ValidacionesRegistroPage;
import com.everis.giru.userinterface.tramites.historial.ValidaNumeroTramiteHistorialPage;

import com.everis.giru.utils.ApiCommons;
import com.everis.giru.utils.ExcelFileService;
import com.everis.giru.utils.NotificacionSlack;
import com.everis.giru.utils.WriteToFile;
import com.everis.tdm.Do;
import io.cucumber.java.en.Then;
import net.serenitybdd.core.Serenity;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class HistorialStepDefinitions {
    
    ValidaNumeroTramiteHistorialPage validaNumeroTramiteHistorialPage = new ValidaNumeroTramiteHistorialPage();
    ValidacionesRegistroPage validacionRegistroPage = new ValidacionesRegistroPage();
    ConsultaFilterInboxSteps consultaFilterInboxSteps = new ConsultaFilterInboxSteps();
   //Agregado por segunda instancia
    String nroAsesorReasignado = "";
    String areaInicial = "";
    Retipificacion retipificacion = new Retipificacion();
    @Then("se valida en historial el reclamo registrado y seguimiento")
    public void seValidaEnHistorialElReclamoRegistradoYSeguimiento() throws ParseException {
        String numeroTramite = Serenity.sessionVariableCalled("numeroTramite").toString();
        validaNumeroTramiteHistorialPage.validaNumeroTramiteHistorial(numeroTramite);
        validaNumeroTramiteHistorialPage.validaDatosResumenHistorial();
        //TODO: VALIDA DESCARGA HR
        //realizarBusquedaTramitePage.validoHRDescargado();
        //TODO: OBSERVACION
        validacionRegistroPage.SalirDetalle();
    }


    @Then("^se valida en historial los tramites en prorroga enviados en seguimiento$")
    public void seValidaEnHistorialProrrogaYSeguimiento() throws IOException, ParseException {

            List<String> prorrogas = ExcelFileService.ProrrogasTramiteList();
            List<String> prorrogasdias = ExcelFileService.ProrrogasDiasList();

            for (int i = 0; i < prorrogas.size() - 1; i++) {

       //             if (i < prorrogas.size() - 1 && i > prorrogas.size() -3) {
                        validaNumeroTramiteHistorialPage.validaNumeroTramiteHistorial(prorrogas.get(i));
                        validaNumeroTramiteHistorialPage.validoProrroga(prorrogas.get(i),prorrogasdias.get(i));
                        //     validaNumeroTramiteHistorialPage.validaDatosResumenHistorial();
                        validacionRegistroPage.SalirDetalle();

         //           }

            }

        /*
        String numeroTramite = Serenity.sessionVariableCalled("numeroTramite").toString();
        validaNumeroTramiteHistorialPage.validaNumeroTramiteHistorial(numeroTramite);
        validaNumeroTramiteHistorialPage.validoProrroga();
        validaNumeroTramiteHistorialPage.validaDatosResumenHistorial();

        //TODO: VALIDA DESCARGA HR
            //realizarBusquedaTramitePage.validoHRDescargado();
            //TODO: OBSERVACION
            validacionRegistroPage.SalirDetalle();
            */


    }

    @Then("^se valida en historial los tramites de cartas enviadas en seguimiento$")
    public void seValidaEnHistorialEnvioCartaSeguimiento() throws IOException, ParseException {

        List<String> prorrogas = ExcelFileService.EnvioCartasTramiteList();
        List<String> prorrogasFechas = ExcelFileService.EnvioCartasFechaList();
        List<String> prorrogasEstados = ExcelFileService.EnvioCartasEstadoEnvioList();
        List<String> prorrogasComentarios = ExcelFileService.EnvioCartasComentarioList();

        for (int i = 0; i < prorrogas.size() - 1; i++) {

            //             if (i < prorrogas.size() - 1 && i > prorrogas.size() -3) {
            validaNumeroTramiteHistorialPage.validoEnvioCartaHistorial(prorrogas.get(i),prorrogasFechas.get(i),prorrogasEstados.get(i),prorrogasComentarios.get(i));
            //     validaNumeroTramiteHistorialPage.validaDatosResumenHistorial();
            validacionRegistroPage.SalirDetalle();

            //           }

        }

        /*
        String numeroTramite = Serenity.sessionVariableCalled("numeroTramite").toString();
        validaNumeroTramiteHistorialPage.validaNumeroTramiteHistorial(numeroTramite);
        validaNumeroTramiteHistorialPage.validoProrroga();
        validaNumeroTramiteHistorialPage.validaDatosResumenHistorial();

        //TODO: VALIDA DESCARGA HR
            //realizarBusquedaTramitePage.validoHRDescargado();
            //TODO: OBSERVACION
            validacionRegistroPage.SalirDetalle();
            */


    }
    @Then("se valida en historial el tramite con su resolucion a segunda instancia")
    public void seValidaEnHistorialElTramiteConSuResolucion3() {
        try {
            String numeroTramite = Serenity.sessionVariableCalled("numeroTramite").toString();

            validaNumeroTramiteHistorialPage.validaNumeroTramiteHistorial(numeroTramite);
        } catch (Exception e) {
            WriteToFile.writeLinesToFile("erro2daInstancia");
            NotificacionSlack.enviarmensajeSlack(e);
            throw new AssertionError(e.getMessage());
        }
    }

    @Then("se valida en historial el tramite {string} con su resolucion a segunda instancia")
    public void seValidaEnHistorialElTramiteConSuResolucion3(String tramite) {
        try {
        String numeroTramite = tramite;

        theActorInTheSpotlight().attemptsTo(
                SeleccionarMenuPrincipal.withData("Historial")
        );

        validaNumeroTramiteHistorialPage.validaNumeroTramiteHistorial(numeroTramite);
        } catch (Exception e) {
            WriteToFile.writeLinesToFile("erro2daInstancia");
            NotificacionSlack.enviarmensajeSlack(e);
            throw new AssertionError(e.getMessage());
        }
    }
    @Then("se valida en historial el tramite {string} para ver su estado")
    public void seValidaEnHistorialElTramiteConSuResolucion4(String tramite) {
        try {
            String numeroTramite = tramite;

            theActorInTheSpotlight().attemptsTo(
                    SeleccionarMenuPrincipal.withData("Historial")
            );

            if(tramite.equals("validarTramite"))
            {
//                validaNumeroTramiteHistorialPage.validaNumeroTramiteHistorial(listaTramites);
//                validaNumeroTramiteHistorialPage.validaNumeroTramiteHistorial(listaTramites);
//                validaNumeroTramiteHistorialPage.validaNumeroTramiteHistorial(listaTramites);

            }
            else {
                validaNumeroTramiteHistorialPage.validaNumeroTramiteHistorial(numeroTramite);
            }
        } catch (Exception e) {
            WriteToFile.writeLinesToFile("erro2daInstancia");
            NotificacionSlack.enviarmensajeSlack(e);
            throw new AssertionError(e.getMessage());
        }
    }

    @Then("se valida en historial el tramite {string} con su resolucion")
    public void seValidaEnHistorialElTramiteConSuResolucion2(String tramite) {
        try {
            String numeroTramite = tramite;

            theActorInTheSpotlight().attemptsTo(
                    SeleccionarMenuPrincipal.withData("Historial")
            );

            validaNumeroTramiteHistorialPage.validaNumeroTramiteHistorial(numeroTramite);
            validaNumeroTramiteHistorialPage.validaDatosResumenBandeja();
        } catch (Exception e) {
            NotificacionSlack.enviarmensajeSlack(e);
            throw new AssertionError(e.getMessage());
        }
    }




    @Then("se valida en historial el tramite con su resolucion")
    public void seValidaEnHistorialElTramiteConSuResolucion() {
        try {
            String numeroTramite = Serenity.sessionVariableCalled("numeroTramite").toString();
            String tramite = Serenity.sessionVariableCalled("numeroTramite").toString();

            theActorInTheSpotlight().attemptsTo(
                    SeleccionarMenuPrincipal.withData("Historial")
            );

            validaNumeroTramiteHistorialPage.validaNumeroTramiteHistorial(numeroTramite);
            validaNumeroTramiteHistorialPage.validaDatosResumenBandeja();

//COMENTAR EL TRY SI NO SE DESEA SEGUNDA INSTANCIA

/*
            try{


//NOTA SE AGREGA EXPERIMENTALMENTE PARA SEGUNDA INSTANCIA
            segundaInstanciaValidacion();
            registrarTramiteBD("En Proceso");
            theActorInTheSpotlight().attemptsTo(
                    new CerrarGiru()
            );

            //2da inst
            String rol = Serenity.sessionVariableCalled("getProfile");

            theActorCalled(rol).attemptsTo(
                    AbrirGiru.loginGiruPage()
            );
            Serenity.setSessionVariable("fueDerivado").to("No");
            Serenity.setSessionVariable("estoyEnResolutoria").to("No");

            numeroTramite = Serenity.sessionVariableCalled("numeroTramite").toString();
            tramite = Serenity.sessionVariableCalled("numeroTramite").toString();

            consultaFilterInboxSteps.seArmaElCuerpoDePeticionCreacionCliente(numeroTramite);

            for (int i = 1; i < 4; i++) {
                if (consultaFilterInboxSteps.agregoLosEncabezadosEnLaPeticion2() == 200) {
                    System.out.println("Nro de veces que consulta al servicio de Derivación: " + i);
                    break;
                }
            }

            consultaFilterInboxSteps.validoLaRespuestaObtenida();
            nroAsesorReasignado = consultaFilterInboxSteps.validoCamposObtenidos();

            Serenity.setSessionVariable("numeroTramite").to(numeroTramite);
            Serenity.setSessionVariable("nroAsesorReasignado").to(nroAsesorReasignado);



            theActorInTheSpotlight().attemptsTo(
                    AutenticarGiru.withData(nroAsesorReasignado, "Peru123.")
            );

            //CLICK INICIAR SESION
            theActorInTheSpotlight().attemptsTo(
                    new ButtonIniciarSesion()
            );
            Serenity.getDriver().manage().timeouts().implicitlyWait(7, TimeUnit.SECONDS);
            if (Serenity.getDriver().findElements(By.xpath("//p[text()=' *EL ID O CONTRASEÑA NO COINCIDEN ']")).size() > 0) {
                WriteToFile.writeLinesToFile("erro2daInstancia");
                WriteToFile.writeLinesToFileResol(tramite +",errorUsuario");

                throw new AssertionError("ERROR! --> EL ID O CONTRASEÑA NO COINCIDEN ");
            }
            //variables tdm
            if (rol.contains("ASESOR DE RETENCIÓN")) {
                Serenity.setSessionVariable("getProfile").to("ESPECIALISTA");
            } else if (rol.contains("SUPERVISOR RETENCIÓN")) {
                Serenity.setSessionVariable("getProfile").to("SUPERVISOR");
            } else if (rol.contains("GERENTE DE TIENDA/GERENTE ASISTENTE") || rol.contains("SUPERVISOR CC") || rol.contains("REPRESENTANTE FINANCIERO")) {
                Serenity.setSessionVariable("getProfile").to("ASESOR");
            } else {
                Serenity.setSessionVariable("getProfile").to(rol);
            }

            Serenity.setSessionVariable("UsuarioSesion").to(nroAsesorReasignado);


            theActorInTheSpotlight().attemptsTo(
                    SeleccionarMenuPrincipal.withData("Tramites"));

            theActorInTheSpotlight().attemptsTo(
                    BuscarTramiteBandejaResolucion.withData( Serenity.sessionVariableCalled("numeroTramite")));


            String tipoResol = Serenity.sessionVariableCalled("tipoResol");
            String areaValidacionOpcional = Serenity.sessionVariableCalled("areaValidacionOpcional");

            if (!tipoResol.equals("Derivar-2")) {
                if (!areaValidacionOpcional.equals("")) {
                    tipoResol = "Derivar";
                }
            }

            theActorInTheSpotlight().attemptsTo(
                    SeleccionarTipoResolucion.withData(tipoResol)
            );

             tipoResol = Serenity.sessionVariableCalled("tipoResol").toString();

            if (!tipoResol.equals("Retipificar")) {
                theActorInTheSpotlight().attemptsTo(
                        IngresarComentarioInterno.withData("comentarioInterno")
                );
            }

            String subirarchivo = Serenity.sessionVariableCalled("subirarchivo").toString();
             tipoResol = Serenity.sessionVariableCalled("tipoResol").toString();

            if (!tipoResol.equals("Retipificar")) {
                theActorInTheSpotlight().attemptsTo(
                        SubirArchivo.withData(subirarchivo)
                );
            }

            if (!tipoResol.equals("Retipificar")) {
                theActorInTheSpotlight().attemptsTo(
                        new ResolucionNoEnviaraCartaCliente()
                );
            }

            String opciones_resuelve_o_retipifica = tipoResol;

            //TODO: TIPO DE RESOLUCION: PROCEDE - NO PROCEDE - RETIPIFICA
            System.out.println("todo a realizar: " + opciones_resuelve_o_retipifica);
            String opcion_resuelve_o_retipifica = opciones_resuelve_o_retipifica.split(":")[0];
            System.out.println("opcion a realizar: " + opcion_resuelve_o_retipifica);

            Serenity.setSessionVariable("configuracion_resolucion").to(opciones_resuelve_o_retipifica);

            String camposRetipificacion = "";
            //String camposMovimientos = "";

            //Serenity.setSessionVariable("camposRetipificacion").to("");
            //Serenity.setSessionVariable("camposMovimientos").to("");

            if (opciones_resuelve_o_retipifica.contains(":")) {
                camposRetipificacion = opciones_resuelve_o_retipifica.split(":")[1];
                //    camposMovimientos = opciones_resuelve_o_retipifica.split(":")[2];
                Serenity.setSessionVariable("camposRetipificacion").to(camposRetipificacion);

                //Serenity.setSessionVariable("camposMovimientos").to(camposMovimientos);

            }

            String tipoproducto = Serenity.sessionVariableCalled("tipoproducto");
            String tipotramite = Serenity.sessionVariableCalled("tipotramite");
            String tipologia = Serenity.sessionVariableCalled("tipologia");

            EscenarioValidacion escenarioValidacion;
            areaInicial = "";
            String areaResolutora = "";
            String areaValidadoraOpcional = "";
            String areaVal = "";

            try {
                System.out.println("<<TIPO PRODUCTO: >> " + tipoproducto.toUpperCase(Locale.ROOT) + " <<TIPO TRAMITE: >> " + tipotramite + " <<NOMBRE TIPOLOGIA: >> " + tipologia + " <<AREA VALI: >> " + areaVal);
                escenarioValidacion = Do.getEscenarioValidacion(tipoproducto.toUpperCase(Locale.ROOT), tipotramite, tipologia, areaVal);

                areaInicial = escenarioValidacion.getAreaInicial();
                areaResolutora = escenarioValidacion.getAreaResolutora();
                areaValidadoraOpcional = escenarioValidacion.getAreaValidadoraOpcional();
            } catch (Exception e) {
                System.out.println(e.getMessage());
                throw new AssertionError("No se encuentra escenario de validación resolución" + e.getMessage());
            }

            System.out.println("debug opciones");

            if (!Objects.equals(areaInicial, areaResolutora) && Objects.equals(areaValidadoraOpcional, "")) {
                System.out.println("CONECTAR: AREA RESOLUTORA NO - RESOLUCIÓN - VALIDADORA OPCIONAL VACIO SI ***");
                areaFinalDiferenteAreaResolutoriaYAreaValidadoraOpcionalVacio(opcion_resuelve_o_retipifica);
            }

            //TODO: EN AREA RESOLUTORA - FLUJO RETIPIFICACIÓN
            if (Objects.equals(areaInicial, areaResolutora) && Objects.equals(areaValidadoraOpcional, "")) {
                if (opcion_resuelve_o_retipifica.equals("RETIPIFICA")) {
                    System.out.println("CONECTAR: RETIPIFICA ***");
                    retipificacion.validaRetificacion();
                }
            }

            if (!Objects.equals(areaInicial, areaResolutora) && !Objects.equals(areaVal, "")) {
                realizarDerivacionValidarEnOtraArea();
                //TODO: AREA VALIDADORA - PROCEDE VALIDACIÓN Y NO PROCEDE VALIDACIÓN
                System.out.println("CONECTAR: AREA VALIDADORA - PROCEDE VALIDACIÓN Y NO PROCEDE VALIDACIÓN ***");
                //USUARIO DE AREA ORIGEN DEBE MARCAR COMO PROCEDE PARA ENVIAR A AREA RESOLUTORA
                areaFinalDiferenteAreaResolutoriaYAreaValidadoraOpcionalVacio("RESUELVE");

                //Se espera a procesar tramite en el area final
                areaFinalDiferenteAreaResolutoriaYAreaValidadoraOpcionalVacio(opcion_resuelve_o_retipifica);
            }

            if (Objects.equals(areaInicial, areaResolutora) && !Objects.equals(areaVal, "")) {
                realizarDerivacionValidarEnOtraArea();
                //TODO: AREA RESOLUTORA - PROCEDE Y NO PROCEDE
                System.out.println("CONECTAR: AREA RESOLUTORA DIRECTO - PROCEDE Y NO PROCEDE ***");
                //Se espera a procesar tramite en el area final
                areaFinalDiferenteAreaResolutoriaYAreaValidadoraOpcionalVacio(opcion_resuelve_o_retipifica);
            }

            //TODO: OBSERVACION
            validacionRegistroPage.SalirDetalle();

            //SI LLEGO AQUI TERMINO RESOLVER TRAMITE OK SEGUNDA INSTANCIA
             numeroTramite = Serenity.sessionVariableCalled("numeroTramite").toString();

            String tiporesol = Serenity.sessionVariableCalled("tipoResol");
            WriteToFile.writeLinesToFileResol(numeroTramite +",ok,"+tiporesol);

            registrarTramiteBD(tipoResol);

           try{
               Thread.sleep(4000);
           }catch (Exception e)
           {
               System.out.println(e);
           }
            //FIN AGREGADO EXPERIMENTALMENTE PARA SEGUNDA INSTANCIA
            }catch (Exception e)
            {
                WriteToFile.writeLinesToFile("erro2daInstancia");
                NotificacionSlack.enviarmensajeSlack(e);
                throw new AssertionError(e.getMessage());
            }

*/
        } catch (Exception e) {
            NotificacionSlack.enviarmensajeSlack(e);
            throw new AssertionError(e.getMessage());
        }
    }

    //Agregado por segunda instancia
    public void realizarDerivacionValidarEnOtraArea() {
        validacionRegistroPage.consultarServicioDeAreaAsignada();

        //CIERRO SESION
        theActorInTheSpotlight().attemptsTo(
                new CerrarGiru()
        );

        //INICIO SESION
        theActorInTheSpotlight().attemptsTo(
                new IniciarSesionReasignado()
        );

        //INGRESO TRAMITES
        theActorInTheSpotlight().attemptsTo(
                SeleccionarMenuPrincipal.withData("Tramites")
        );

        //INGRESO TRAMITE
        String numeroTramite = Serenity.sessionVariableCalled("numeroTramite").toString();

        theActorInTheSpotlight().attemptsTo(
                BuscarTramiteBandejaResolucion.withData(numeroTramite)
        );

        String resol = Serenity.sessionVariableCalled("resolucion");

        if (resol.equals("Procede")) {
            theActorInTheSpotlight().attemptsTo(
                    SeleccionarTipoResolucion.withData("Procede")
            );

            theActorInTheSpotlight().attemptsTo(
                    new ResolucionNoEnviaraCartaCliente()
            );

        }

        if (resol.equals("No Procede")) {
            theActorInTheSpotlight().attemptsTo(
                    SeleccionarTipoResolucion.withData("No Procede")
            );
            theActorInTheSpotlight().attemptsTo(
                    new ResolucionNoEnviaraCartaCliente()
            );

        }

        if (resol.equals("Derivar")) {
            theActorInTheSpotlight().attemptsTo(SeleccionarTipoResolucion.withData("Derivar"));
            theActorInTheSpotlight().attemptsTo(SeleccionarRazonYArea.withData("RequiereSerDevueltoAreaAnterior", areaInicial));
            theActorInTheSpotlight().attemptsTo(new ConfirmarResolucionDer());
            //CUSTODIA DE DOCUMENTOS FINAL
        }

    }
    //Agregado por segunda instancia
    public void areaFinalDiferenteAreaResolutoriaYAreaValidadoraOpcionalVacio(String opcion_resuelve_o_retipifica) {
        validacionRegistroPage.consultarServicioDeAreaAsignada();

        //CIERRO SESION
        theActorInTheSpotlight().attemptsTo(
                new CerrarGiru()
        );

        //INICIO SESION
        theActorInTheSpotlight().attemptsTo(
                new IniciarSesionReasignado()
        );

        //INGRESO TRAMITES
        theActorInTheSpotlight().attemptsTo(
                SeleccionarMenuPrincipal.withData("Tramites")
        );

        //INGRESO TRAMITE
        String numeroTramite = Serenity.sessionVariableCalled("numeroTramite").toString();

        theActorInTheSpotlight().attemptsTo(
                BuscarTramiteBandejaResolucion.withData(numeroTramite)
        );

        String tipoResol = Serenity.sessionVariableCalled("tipoResolF").toString();

        if (opcion_resuelve_o_retipifica.equals("RETIPIFICA")) {
            retipificacion.validaRetificacion();
        } else {

            System.out.println("ESTOY EN RESOLUCION - PROCEDE Y NO PROCEDE .....");
            theActorInTheSpotlight().attemptsTo(
                    SeleccionarTipoResolucion.withData(tipoResol)
            );

            theActorInTheSpotlight().attemptsTo(
                    IngresarComentarioInterno.withData("PRUEBA RESOLUCIÓN")
            );

            String subirarchivo = Serenity.sessionVariableCalled("subirarchivo").toString();

            theActorInTheSpotlight().attemptsTo(
                    SubirArchivo.withData(subirarchivo)
            );

            theActorInTheSpotlight().attemptsTo(
                    new ResolucionNoEnviaraCartaCliente()
            );

        }

    }

    public String getValueFecha() {
        //TODO: ADD FECHA AUTOGENERADO
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
        String valueDate = dateFormat.format(date);
        return valueDate;
    }

    @Then("se continua con la validacion del flujo en segunda instancia")
    public void segundaInstanciaValidacion() {

        try {
            WebElement btnSegundaInstancia = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(60)).until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@class='ml-10p link-simple c-green-exception']")));
            btnSegundaInstancia.click();

            //TODO: SE ESPERA A PROCESAR TRAMITE
            try {
                Thread.sleep(2000);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

            WebElement btnConfirmar = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(60)).until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='confirmation__btn-confirm']")));
            btnConfirmar.click();

            try {
                Thread.sleep(7000);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

            int mov = Serenity.getDriver().findElements(By.xpath("//*[@id='claim-form__footer-continue']")).size();

            for(int i = 1; i <= mov; i++)
            {
                try {
                    Thread.sleep(1000);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
//                Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(0));
//                int checkAmount = i-1;
//                if(Serenity.getDriver().findElements(By.id("Add_Amount_"+ checkAmount)).size() >0)
//                {
//                    System.out.println("Si existe agregar movimiento, se agrega uno");
//                    WebElement addAmount = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(60)).until(ExpectedConditions.elementToBeClickable(By.id("Add_Amount_"+ checkAmount)));
//                    ((JavascriptExecutor) Serenity.getDriver()).executeScript("arguments[0].scrollIntoView(true)", addAmount);
//
//
//                    Serenity.getDriver().findElement(By.id("Add_Amount_"+ checkAmount)).click();
//
//
//                    try {WebElement valor= new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(0)).withTimeout(Duration.ofSeconds(0)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("((//*[@class='panel-body'])["+i+"]//input[@formcontrolname='amount'])[last()]")));valor.click();}catch (Exception e) { System.out.println(e.getMessage());}
//
//                    try {WebElement valor = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(0)).withTimeout(Duration.ofSeconds(0)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("((//*[@class='panel-body'])["+i+"]//input[@id='Monto'])[last()]")));valor.sendKeys("50");}catch (Exception e) { System.out.println(e.getMessage());}
//                    try {WebElement valor = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(0)).withTimeout(Duration.ofSeconds(0)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("((//*[@class='panel-body'])["+i+"]//input[@formcontrolName='Monto'])[last()]")));valor.sendKeys("50");}catch (Exception e) { System.out.println(e.getMessage());}
//
//
//                    try {WebElement valor= new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(0)).withTimeout(Duration.ofSeconds(0)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("((//*[@class='panel-body'])["+i+"]//input[@id='Fecha'])[last()]")));valor.sendKeys(getValueFecha());}catch (Exception e) { System.out.println(e.getMessage());}
//                    try {WebElement valor = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(0)).withTimeout(Duration.ofSeconds(0)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("((//*[@class='panel-body'])["+i+"]//input[@formcontrolName='Fecha'])[last()]")));valor.sendKeys(getValueFecha());}catch (Exception e) { System.out.println(e.getMessage());}
//
//
//                    try {WebElement valor= new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(0)).withTimeout(Duration.ofSeconds(0)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("((//*[@class='panel-body'])["+i+"]//input[@id='Establecimiento'])[last()]")));valor.sendKeys("TAMBO");}catch (Exception e) { System.out.println(e.getMessage());}
//
//
//                    try {WebElement valor= new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(0)).withTimeout(Duration.ofSeconds(0)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("((//*[@class='panel-body'])["+i+"]//input[@formcontrolname='Hora'])[last()]")));valor.sendKeys("10:30");}catch (Exception e) { System.out.println(e.getMessage());}
//
//
//                    try {WebElement valor= new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(0)).withTimeout(Duration.ofSeconds(0)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("((//*[@class='panel-body'])["+i+"]//input[@id='EstablecimientoTransaccion'])[last()]")));valor.sendKeys("RAPPI COMPRA");}catch (Exception e) { System.out.println(e.getMessage());}
//                    try {WebElement valor= new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(0)).withTimeout(Duration.ofSeconds(0)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("((//*[@class='panel-body'])["+i+"]//input[@formcontrolname='EstablecimientoTransaccion'])[last()]")));valor.sendKeys("RAPPI COMPRA");}catch (Exception e) { System.out.println(e.getMessage());}
//
//
//                    try {WebElement valor= new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(0)).withTimeout(Duration.ofSeconds(0)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("((//*[@class='panel-body'])["+i+"]//input[@formcontrolname='Descripcion'])[last()]")));valor.sendKeys("RAPPI COMPRA");}catch (Exception e) { System.out.println(e.getMessage());}
//                    try {WebElement valor= new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(0)).withTimeout(Duration.ofSeconds(0)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("((//*[@class='panel-body'])["+i+"]//input[@id='Descripcion'])[last()]")));valor.sendKeys("RAPPI COMPRA");}catch (Exception e) { System.out.println(e.getMessage());}
//
//
//                    try {WebElement valor= new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(0)).withTimeout(Duration.ofSeconds(0)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("((//*[@class='panel-body'])["+i+"]//input[@id='Hora'])[last()]")));valor.sendKeys("10:15");}catch (Exception e) { System.out.println(e.getMessage());}
//
//
//                    try {WebElement valor= new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(0)).withTimeout(Duration.ofSeconds(0)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("((//*[@class='panel-body'])["+i+"]//input[@id='CodigoSuministroContrato'])[last()]")));valor.sendKeys("255366");}catch (Exception e) { System.out.println(e.getMessage());}
//
//                    try {WebElement valor= new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(0)).withTimeout(Duration.ofSeconds(0)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("((//*[@class='panel-body'])["+i+"]//input[@id='ImporteSolicitado'])[last()]")));valor.sendKeys("50");}catch (Exception e) { System.out.println(e.getMessage());}
//
//                    try {WebElement valor= new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(0)).withTimeout(Duration.ofSeconds(0)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("((//*[@class='panel-body'])["+i+"]//input[@formcontrolname='CuotasSolicitadas'])[last()]")));valor.sendKeys("12");}catch (Exception e) { System.out.println(e.getMessage());}
//
//                    try {WebElement valor= new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(0)).withTimeout(Duration.ofSeconds(0)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("((//*[@class='panel-body'])["+i+"]//input[@formcontrolname='Comercio'])[last()]")));valor.sendKeys("RAPPI");}catch (Exception e) { System.out.println(e.getMessage());}
//
//
//                    try {WebElement valor= new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(0)).withTimeout(Duration.ofSeconds(0)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("((//*[@class='panel-body'])["+i+"]//div[@id='CanalUtilizadoReclamado'])[last()]")));
//
//                        Serenity.getDriver().findElement(By.xpath("((//*[@class='panel-body'])["+i+"]//div[@id='CanalUtilizadoReclamado'])[last()]")).click();
//                        Serenity.getDriver().findElement(By.xpath("((//*[@class='panel-body'])["+i+"]//*[@id='App IBK'])[last()]")).click();
//                    }catch (Exception e) { System.out.println(e.getMessage());}
//                    try {WebElement valor= new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(0)).withTimeout(Duration.ofSeconds(0)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("((//*[@class='panel-body'])["+i+"]//input[@formcontrolname='Motivo'])[last()]")));valor.sendKeys("RAPPI");}catch (Exception e) { System.out.println(e.getMessage());}
//
//
//                    try {WebElement valor= new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(0)).withTimeout(Duration.ofSeconds(0)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("((//*[@class='panel-body'])["+i+"]//input[@formcontrolname='ServicioEmpresa'])[last()]")));valor.sendKeys("TAMBO");}catch (Exception e) { System.out.println(e.getMessage());}
//                    try {WebElement valor= new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(0)).withTimeout(Duration.ofSeconds(0)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("((//*[@class='panel-body'])["+i+"]//input[@id='ServicioEmpresa'])[last()]")));valor.sendKeys("TAMBO");}catch (Exception e) { System.out.println(e.getMessage());}
//
//                    try {WebElement valor= new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(0)).withTimeout(Duration.ofSeconds(0)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("((//*[@class='panel-body'])["+i+"]//input[@id='ClienteSuministroContrato'])[last()]")));valor.sendKeys("105255");}catch (Exception e) { System.out.println(e.getMessage());}
//
//
//                    try {WebElement valor= new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(0)).withTimeout(Duration.ofSeconds(0)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("((//*[@class='panel-body'])["+i+"]//input[@id='supplyCode'])[last()]")));valor.sendKeys("0150250");}catch (Exception e) { System.out.println(e.getMessage());}
//
//
//                    try {WebElement valor= new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(0)).withTimeout(Duration.ofSeconds(0)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("((//*[@class='panel-body'])["+i+"]//input[@id='Servicio'])[last()]")));valor.sendKeys("IBK");}catch (Exception e) { System.out.println(e.getMessage());}
//
//
//                    try {WebElement valor= new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(0)).withTimeout(Duration.ofSeconds(0)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("((//*[@class='panel-body'])["+i+"]//input[@id='Codigo'])[last()]")));valor.sendKeys("0150250");}catch (Exception e) { System.out.println(e.getMessage());}
//
//
//                    try {WebElement valor= new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(0)).withTimeout(Duration.ofSeconds(0)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("((//*[@class='panel-body'])["+i+"]//input[@id='TipoOperacion'])[last()]")));
//                        Serenity.getDriver().findElement(By.xpath("((//*[@class='panel-body'])["+i+"]//div[@id='TipoOperacion'])[last()]")).click();
//                        Serenity.getDriver().findElement(By.xpath("((//*[@class='panel-body'])["+i+"]//li[@id='Depósito'])[last()]")).click();
//                    }catch (Exception e) { System.out.println(e.getMessage());}
//
//                    try {WebElement valor= new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(0)).withTimeout(Duration.ofSeconds(0)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("((//*[@class='panel-body'])["+i+"]//label[@class='pb-14p'])[last()]")));valor.click();}catch (Exception e) { System.out.println(e.getMessage());}
//
//                    try {WebElement valor= new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(0)).withTimeout(Duration.ofSeconds(0)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("((//*[@class='panel-body'])["+i+"]//input[@formcontrolname='FechaArriboProductoServicio'])[last()]")));valor.sendKeys(getValueFecha());}catch (Exception e) { System.out.println(e.getMessage());}
//
//
//                }
                Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(20));
                WebElement btnConfirmoMovimiento = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(60)).until(ExpectedConditions.elementToBeClickable(By.xpath("(//*[@id='claim-form__footer-continue'])[" + i +"]")));
                btnConfirmoMovimiento.click();
            }

            if(Serenity.getDriver().findElements(By.xpath("//*[contains(text(), 'Ayudemos a resolver el trámite')]")).size()>0) {
                WebElement labelTramite = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(60)).withTimeout(Duration.ofSeconds(1)).until(ExpectedConditions.elementToBeClickable(By.xpath("//*[contains(text(), 'Ayudemos a resolver el trámite')]")));
                ((JavascriptExecutor) Serenity.getDriver()).executeScript("arguments[0].scrollIntoView(true)", labelTramite);
            }
            //asd
            WebElement labelRepLegal = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(60)).withTimeout(Duration.ofSeconds(1)).until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='isLegalRepresentative']/../label")));
            ((JavascriptExecutor) Serenity.getDriver()).executeScript("arguments[0].scrollIntoView(true)", labelRepLegal);

            labelRepLegal.click();
            try {
                Thread.sleep(1000);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

            WebElement inputNames = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(60)).until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='names']")));
            inputNames.sendKeys("Eduardo Alexis");
            WebElement inputLastNames = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(60)).until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='lastNames']")));
            inputLastNames.sendKeys("Arias Cespedes");
            WebElement telephone = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(60)).until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='telephone']")));
            telephone.sendKeys("959149978");

            WebElement correo = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(60)).until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='email']")));
            correo.sendKeys("alexisce004@gmail.com");

            WebElement domicilio = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(60)).until(ExpectedConditions.elementToBeClickable(By.xpath("//*[contains(text(), 'Domicilio')]/../app-ibk-select")));
            domicilio.click();

            WebElement agregardomicilio = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(60)).until(ExpectedConditions.elementToBeClickable(By.xpath("(//*[contains(text(), 'Agregar una nueva dirección. ')])[1]")));
            agregardomicilio.click();

         //nuevo dir

            //TODO: Departamento (*)
            Serenity.getDriver().findElement(By.id("department")).click();
            Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='department']//li)[1]")).click();
            //TODO: Provincia (*)
            Serenity.getDriver().findElement(By.id("province")).click();
            Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='province']//li)[1]")).click();
            //TODO: Distrito (*)
            Serenity.getDriver().findElement(By.id("district")).click();
            Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='district']//li)[1]")).click();
            //TODO: Tipo de via (*)
            Serenity.getDriver().findElement(By.id("streetType")).click();
            Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='streetType']//li)[14]")).click();
            //TODO: Nombre de vía (*)
            Serenity.getDriver().findElement(By.id("streetName")).sendKeys("Andromeda");
            //TODO: Urbanización
            Serenity.getDriver().findElement(By.id("neighborhood")).sendKeys("La Paz");
            //TODO: Número
            Serenity.getDriver().findElement(By.id("streetNumber")).sendKeys("850");
            //TODO: Manzana
            Serenity.getDriver().findElement(By.id("block")).sendKeys("J");
            //TODO: Lote
            Serenity.getDriver().findElement(By.id("lot")).sendKeys("7B");
            //TODO: Interior
            Serenity.getDriver().findElement(By.id("apartment")).sendKeys("20");
            //TODO: Referencia
            Serenity.getDriver().findElement(By.xpath("//textarea[@formcontrolname='landmark']")).sendKeys("GS4 Seguridad del Peru");
            //TODO: Button Añadir
            WebElement btnAddAddress = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(5))
                    .until(ExpectedConditions.elementToBeClickable(By.id("add-address__btn-add")));
            btnAddAddress.click();

            //NuevoDir


            //asd

            WebElement btnRegistroTramite = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(60)).until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='claim-form__btn-submit']")));
            btnRegistroTramite.click();
            //TODO: SE ESPERA A PROCESAR TRAMITE
            try {
                Thread.sleep(10000);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

        String valueNumeroTramiteFrontOne;

        Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(60));
        boolean labelTipoTramite = Serenity.getDriver().findElements(By.xpath("//*[@id='no-proceed-section']/div[2]/div[1]/span[2]")).size() > 0;
        if (labelTipoTramite) {
            System.err.println("OBTIENE NUMERO - TRAMITE - ENTRA ID no-proceed-section");
            //TODO: OBTIENE NUMERO TRAMITE DESDE FRONT
            WebElement lblNumeroTramiteFront = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(120)).until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='no-proceed-section']/div[2]/div[1]/span[2]")));
            String valueNumeroTramiteFront = lblNumeroTramiteFront.getText();
            valueNumeroTramiteFrontOne = valueNumeroTramiteFront;
            System.out.println("=======================================\nFRONT REGISTRO: Nº TRÁMITE: " + valueNumeroTramiteFront + "\n=======================================");
            Serenity.takeScreenshot();
        } else {
            System.err.println("OBTIENE NUMERO - TRAMITE - ENTRA ID proceed-section");
            //TODO: OBTIENE NUMERO TRAMITE DESDE FRONT
            WebElement lblNumeroTramiteFront = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(120)).until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='proceed-section']/div[2]/div[1]/span[2]")));
            String valueNumeroTramiteFront = lblNumeroTramiteFront.getText();
            valueNumeroTramiteFrontOne = valueNumeroTramiteFront;
            System.out.println("=======================================\nFRONT REGISTRO: Nº TRÁMITE: " + valueNumeroTramiteFront + "\n=======================================");
            Serenity.takeScreenshot();
        }
       String msg = Serenity.sessionVariableCalled("finalcsv");
        WriteToFile.writeLinesToFile(valueNumeroTramiteFrontOne +"," +msg);

            Serenity.setSessionVariable("numeroTramite").to(valueNumeroTramiteFrontOne);


        } catch (Exception e) {
            WriteToFile.writeLinesToFile("erro2daInstancia");
            NotificacionSlack.enviarmensajeSlack(e);
            throw new AssertionError(e.getMessage());
        }

        //TODO: BUTTON TERMINAR - ATENCIÓN
        WebElement btnTerminarAtencion = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(120)).until(ExpectedConditions.elementToBeClickable(By.id("end-atention")));
        btnTerminarAtencion.click();

    }
    @Then("se valida en historial el tramite con su retipificacion")
    public void seValidaEnHistorialElTramiteConSuRetipificacion() throws AWTException {
        try{
            validacionRegistroPage.SalirDetalle();
        } catch (Exception e) {
            WriteToFile.writeLinesToFile("No se encontro vista Resumen abierta");
        }
        try {

            String numeroTramite = Serenity.sessionVariableCalled("numeroTramite").toString();

            theActorInTheSpotlight().attemptsTo(
                    SeleccionarMenuPrincipal.withData("Historial")
            );

            validaNumeroTramiteHistorialPage.validaNumeroTramiteHistorial(numeroTramite);

            validacionRegistroPage.SalirDetalle();

        } catch (Exception e) {
            NotificacionSlack.enviarmensajeSlack(e);
            throw new AssertionError(e.getMessage());
        }
    }


}
