package com.everis.giru.stepsdefinitions.tramites;

import com.everis.giru.tasks.e2eTramites.*;
import com.everis.giru.tasks.login.SeleccionarMenuPrincipal;
import com.everis.giru.tasks.tramites.*;
import com.everis.giru.tasks.tramites.filtros.*;
import com.everis.giru.userinterface.atencionCliente.ValidacionesRegistroPage;
import com.everis.giru.userinterface.tramites.FiltrosBandejaPage;
import com.everis.giru.userinterface.tramites.GetAreaTramitePage;
import com.everis.giru.userinterface.tramites.contadorPage.GetContadorIzquierdoPage;
import com.everis.giru.userinterface.tramites.contadorPage.GetContadorDerechoPage;
import com.everis.giru.userinterface.tramites.paginacionPage.CambiarPaginaPage;
import com.everis.giru.userinterface.tramites.validaBusqueda.ValidaBuscarTramitePage;
import com.everis.giru.utils.ExcelFileService;
import com.everis.giru.utils.NotificacionSlack;
import com.everis.giru.utils.WriteToFile;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.netty.util.ThreadDeathWatcher;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Upload;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;
import static org.assertj.core.api.Assertions.assertThat;

public class TramitesStepDefinitions {

    ValidaBuscarTramitePage validaBuscarTramitePage = new ValidaBuscarTramitePage();
    GetContadorDerechoPage getContadorDerechoPage = new GetContadorDerechoPage();
    GetContadorIzquierdoPage getContadorIzquierdoPage = new GetContadorIzquierdoPage();
    GetAreaTramitePage getAreaTramitePage;

    CambiarPaginaPage cambiarPaginaPage;

    @And("^se selecciona (.*) del menu principal$")
    public void validaSeleccionarOpcionMenuPrincipal(String menuPrincipal) {
        ValidacionesRegistroPage validacionRegistroPage = new ValidacionesRegistroPage();
        try{
            validacionRegistroPage.SalirDetalle();
        } catch (Exception e) {
            WriteToFile.writeLinesToFile("No se encontro vista Resumen abierta");
        }

        try{
        theActorInTheSpotlight().attemptsTo(
                SeleccionarMenuPrincipal.withData(menuPrincipal)
        );


        } catch (Exception e) {
            WriteToFile.writeLinesToFile("erro2daInstancia");

            String nrotramite = Serenity.sessionVariableCalled("nrotramite");
            WriteToFile.writeLinesToFileResol(nrotramite +",error");

            NotificacionSlack.enviarmensajeSlack(e);
            throw new AssertionError(e.getMessage());
        }

    }

    @And("^se selecciona la opcion (.*) del perfil (.*) asignado al departamento (.*)$")
    public void validaSeleccionaOpcionTipologiaPerfilPorDepartamento(String tipologia, String perfil, String departamento) throws InterruptedException {

        theActorInTheSpotlight().attemptsTo(
                SeleccionarTipologia2.withData(tipologia, perfil, departamento)
        );

        Thread.sleep(1000);
    }

    @Then("^se visualiza un perfil (.*) de la tipologia (.*) del departamento (.*)$")
    public void validaVisualizaPerfilPorTipologiaDepartamento(String perfil, String tipologia, String departamento) throws InterruptedException {
        Thread.sleep(1000);
        theActorInTheSpotlight().attemptsTo(
                ValidaTipologiaDeterminada.withData(perfil, tipologia, departamento)
        );

        Serenity.setSessionVariable("perfilBuscar").to(perfil);
        Serenity.setSessionVariable("tipologiaBuscar").to(tipologia);
        Serenity.setSessionVariable("departamentoBuscar").to(departamento);


    }

    @And("^realiza la opcion buscar por (.*) y (.*) de tramites$")
    public void validaBuscarTipoNumeroDocumentoDeTramites(String tipoTramite, String numeroTramite) {

        theActorInTheSpotlight().attemptsTo(
                SeleccionarTipoTramites.withData(tipoTramite, numeroTramite)
        );

        theActorInTheSpotlight().attemptsTo(
                new ButtonBuscar()
        );

        Serenity.setSessionVariable("numeroTramite").to(numeroTramite);
        Serenity.setSessionVariable("tipoTramite").to(tipoTramite);

    }

    @Then("se verifica el tramite que existe en bandeja")
    public void validaTramiteExisteBandeja() throws InterruptedException {
        Thread.sleep(1000);
        validaBuscarTramitePage.validaBuscarTramite();

    }

    @Then("^se valida contador de (.*) de perfil (.*) del departamento (.*) que sean iguales$")
    public void validaContadorTotalTramites(String tipologia, String perfil, String departamento) {

        theActorInTheSpotlight().attemptsTo(
                ValidaContadorBandeja.withData(departamento, perfil, tipologia)
        );

    }

    @Then("^se valida la paginacion de (.*) registros en la tipologia (.*) de determinado (.*) del departamento (.*)$")
    public void validaPaginacionPorTipologia(String registros, String tipologia, String perfil, String departamento) throws InterruptedException {

        theActorInTheSpotlight().attemptsTo(
                ValidaPaginacion.withData(departamento, perfil, tipologia, registros)

        );

        Thread.sleep(1000);
    }

    @Then("^se selecciona filtros (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*) de la tipologia (.*) del perfil (.*) del departamento (.*)$")
    public void seleccionaFiltros(String fVencimiento, String fProductos, String fTipoTramite, String fTipologia, String fSegmento, String fEstado, String fNivelServicio, String fMoneda, String fMontoDesde, String fMontoHasta, String fMedioRespuesta, String tipologia, String perfil, String departamento) throws InterruptedException {
        Thread.sleep(500);
        Serenity.setSessionVariable("Tipologia").to(tipologia);
        if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("") || getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("0")) {
            getContadorDerechoPage.EsCeroTotalTramite();
            if (tipologia.equals("TodosMisTramites") || tipologia.equals("MiEquipo") || tipologia.equals("Asignar") || tipologia.equals("Resolver") || tipologia.equals("Monitorear") || tipologia.equals("TransaccionesEnProceso") || tipologia.equals("Retipificar")) {
                if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("Ingresa una búsqueda para ver resultados")) {
                    assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                } else {
                    assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                }

            } else {

                if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("No se encontraron coincidencias")) {
                    assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                } else {
                    assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                }
            }

        } else {
            //Filtro Vencimiento
            theActorInTheSpotlight().attemptsTo(
                    SeleccionarFiltroVencimiento.withData(departamento, perfil, tipologia, fVencimiento)
            );
            //Filtro Productos
            theActorInTheSpotlight().attemptsTo(
                    SeleccionarFiltroProducto.withData(departamento, perfil, tipologia, fProductos)
            );
            //Filtro TipoTramite
            theActorInTheSpotlight().attemptsTo(
                    SeleccionarFiltroTipoTramite.withData(departamento, perfil, tipologia, fTipoTramite)
            );
            //Filtro Tipologia
            theActorInTheSpotlight().attemptsTo(
                    SeleccionarFiltroTipologia.withData(fTipologia, perfil, departamento, fTipoTramite)
            );
            //Filtro Motivos - Nilo
            //TODO: A parter nueva versión ya no muestra filtro Motivos
//            theActorInTheSpotlight().attemptsTo(
//                    SeleccionarFiltroMotivos.withData(departamento, perfil, fTipoTramite, fTipologia, fMotivos)
//            );
            //Filtro Segmento - Renato
            theActorInTheSpotlight().attemptsTo(
                    SeleccionarFiltroSegmento.withData(departamento, perfil, tipologia, fSegmento)
            );
            //Filtro Estado - Nilo
            theActorInTheSpotlight().attemptsTo(
                    SeleccionarFiltroEstado.withData(departamento, perfil, tipologia, fEstado)
            );
            //Filtro Nivel de Servicio - Renato
            theActorInTheSpotlight().attemptsTo(
                    SeleccionarFiltroNivelServicio.withData(departamento, perfil, tipologia, fNivelServicio)
            );
            //Filtro Moneda y Monto - Nilo
            theActorInTheSpotlight().attemptsTo(
                    SeleccionarFiltroMonedaMonto.withData(departamento, perfil, tipologia, fMoneda, fMontoDesde, fMontoHasta)
            );
            //TODO: Agregar filtro - Autonomía manual

            //TODO: Cambio Mas filtro
            //Filtro Mas FIltros - Renato
            theActorInTheSpotlight().attemptsTo(
                    SeleccionarFiltroMedioRespuesta.withData(departamento, perfil, tipologia, fMedioRespuesta)
            );

        }
        getContadorIzquierdoPage.SetContadorANull();
    }

    @And("se valida el area seleccionada {string}")
    public void seValidaElAreaSeleccionada(String area) {
        getAreaTramitePage.validoArea(area);
    }


    @And("^sube el archivo de (.*) con los tramites generados$")
    public void subeCartaOnlineConTramitesGenerados(String proceso) throws Exception {

        if(proceso.equals("envio de cartas")) {

            // WriteToFile.convertProrrogaCSVtoXLSX();
            theActorInTheSpotlight().attemptsTo(
                    new EnvioCartasAdjuntaArchivo()
            );
        }
        else {
            WriteToFile.convertProrrogaCSVtoXLSX();

            theActorInTheSpotlight().attemptsTo(
                    new ProrrogaAdjuntaArchivo()
            );
        }
    }



    @And("sube el archivo de prorrogas con un tramite de seis prorrogas realizadas")
    public void subeElArchivoDeProrrogasConLosTramitesGeneradosSeisProrrogas()  {

        theActorInTheSpotlight().attemptsTo(
                new ProrrogaAdjuntaArchivoSeisProrrogas()
        );

    }

    @And("sube el archivo de prorrogas con mas de trecientos tramites")
    public void subeElArchivoDeProrrogasConLosTramitesGenerados2() throws Exception {

        WriteToFile.convertProrrogaCSVtoXLSX();

        theActorInTheSpotlight().attemptsTo(
                new ProrrogaAdjuntaArchivo301()
        );

    }

    @And("se muestra el mensaje de error de tramites")
    public void validaMensajeTramitesMaximo() {
        theActorInTheSpotlight().attemptsTo(
                new ValidaMensajeErrorProrrogas()
        );

    }

    @And("^se realiza filtro de tipo de tramites por (.*) para agregar (.*) dias de prorroga$")
    public void seRealizaFiltroDeTipoDeTramitesPor(String tipotramite, String prorroga) {
        //Filtro TipoTramite
        theActorInTheSpotlight().attemptsTo(
                SeleccionarFiltroTipoTramiteProrroga.withData("GPYR", "Especialista", "No", tipotramite)
        );

        cambiarPaginaPage.cambiarPaginacionAnteriorUltima();

//        WebElement btnAsignar = Serenity.getDriver().findElement(By.xpath("//button[contains(text(), 'Asignar')]"));
//
//        JavascriptExecutor js = (JavascriptExecutor) Serenity.getDriver();
//        js.executeScript("arguments[0].scrollIntoView();", btnAsignar);

       String tramite = cambiarPaginaPage.ValidarYObtenerPrimerTramiteRegistroAntiguo();


       try {
           //CREO EXCEL
           ExcelFileService.addTramitesProrroga(tramite, prorroga);
           //Agrego a CSV
           WriteToFile.writeLinesToFileProrroga(tramite + "," + prorroga);
       }
       catch (Exception e)
       {
           System.out.println(e.getMessage());
       }
    }
}
