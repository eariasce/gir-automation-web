package com.everis.giru.stepsdefinitions.tramites;

import com.everis.giru.tasks.atencionCliente.BuscarTramiteBandejaResolucion;
import com.everis.giru.tasks.atencionCliente.SubirArchivo;
import com.everis.giru.tasks.login.CerrarGiru;
import com.everis.giru.tasks.login.SeleccionarMenuPrincipal;
import com.everis.giru.tasks.retipificacion.Retipificacion;
import com.everis.giru.tasks.tramites.*;
import com.everis.giru.userinterface.atencionCliente.ValidacionesRegistroPage;
import com.everis.giru.utils.ApiCommons;
import com.everis.giru.utils.NotificacionSlack;
import com.everis.giru.utils.WriteToFile;
import com.everis.tdm.Do;
import com.everis.tdm.model.giru.EscenarioValidacion;
import com.everis.tdm.model.giru.resolucionComercial;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import net.serenitybdd.core.Serenity;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class ResolucionStepDefinitions {

    String areaInicial = "";

    ValidacionesRegistroPage validacionRegistroPage = new ValidacionesRegistroPage();
    Retipificacion retipificacion = new Retipificacion();

    @When("se ingresa el numero de tramite en la busqueda para seleccionarlo")
    public void seIngresaElNumeroDeTramiteEnLaBusquedaParaSeleccionarlo() {

        String numeroTramite = Serenity.sessionVariableCalled("numeroTramite").toString();

        theActorInTheSpotlight().attemptsTo(
                BuscarTramiteBandejaResolucion.withData(numeroTramite)
        );

    }

    @When("se ingresa el numero de tramite en segunda instancia en la busqueda para seleccionarlo")
    public void seIngresaElNumeroDeTramiteEnLaBusquedaParaSeleccionarloSegundaInstancia(DataTable dataTable) {

        List<Map<String, String>> list = dataTable.asMaps(String.class, String.class);

        List<List<String>> rows = dataTable.asLists(String.class);

        int resultado = 0;
        for (int i = 0; i < rows.size(); i++) {
            List<String> columns = rows.get(i);

            resultado = i - 1;

        }

        try {

            Serenity.setSessionVariable("tipoproducto").to(list.get(resultado).get("tipoProducto"));
            Serenity.setSessionVariable("tipologia").to(list.get(resultado).get("tipologia"));
            Serenity.setSessionVariable("subirarchivo").to(list.get(resultado).get("indicaArchivoAdjunto"));
            Serenity.setSessionVariable("carta").to(list.get(resultado).get("carta"));
        }
        catch (Exception e){

        }

            theActorInTheSpotlight().attemptsTo(
                BuscarTramiteBandejaResolucion.withData( Serenity.sessionVariableCalled("numeroTramite"))
        );

    }
    @And("^se abre la vista del tramite de Resumen y selecciona el tipo de resolucion (.*) con area a derivar (.*)$")
    public void seleccionaTipoResolucionTramite(String tipoResol, String areaValidacionOpcional) {

        try {
            Serenity.setSessionVariable("tipoResolF").to(tipoResol);
            Serenity.setSessionVariable("tipoResol").to(tipoResol);
            Serenity.setSessionVariable("resolucion").to(tipoResol);

            Serenity.setSessionVariable("areaValidacionOpcional").to(areaValidacionOpcional.trim());
            if (!tipoResol.equals("Derivar-2")) {
                if (!areaValidacionOpcional.equals("")) {
                    tipoResol = "Derivar";
                }
            }

            theActorInTheSpotlight().attemptsTo(
                    SeleccionarTipoResolucion.withData(tipoResol)
            );

        }
        catch(Exception e){

            String nrotramite = Serenity.sessionVariableCalled("nrotramite");
            WriteToFile.writeLinesToFileResol(nrotramite +",error");

        }
    }

    @And("^ingresa comentario (.*) opcional$")
    public void ingresaComentarioComentariointernoOpcional(String comentarioInterno) {

        try {
            String tipoResol = Serenity.sessionVariableCalled("tipoResol").toString();

            if (!tipoResol.equals("Retipificar")) {
                theActorInTheSpotlight().attemptsTo(
                        IngresarComentarioInterno.withData(comentarioInterno)
                );
            }
        }catch (Exception e)
        {

            String nrotramite = Serenity.sessionVariableCalled("nrotramite");
            WriteToFile.writeLinesToFileResol(nrotramite +",error");

        }

    }

    @And("^se selecciona razon (.*) y el area (.*) al cual se derivara el tramite$")
    public void seSeleccionaRazonRazonYElAreaAreaAlCualSeDerivaraElTramite(String razon, String area) {
        theActorInTheSpotlight().attemptsTo(SeleccionarRazonYArea.withData(razon, area));

    }

    @And("^se confirma la resolucion del tramite$")
    public void confirmarResolucionTramite() {

            String tipoResol = Serenity.sessionVariableCalled("tipoResol").toString();

            if (!tipoResol.equals("Retipificar")) {
                theActorInTheSpotlight().attemptsTo(
                        new ResolucionNoEnviaraCartaCliente()
                );
            }


    }

    @And("se confirma la derivacion del tramite")
    public void seConfirmaLaDerivacionDelTramite() {

        try {

            theActorInTheSpotlight().attemptsTo(
                    new ConfirmarResolucionDer()
            );

        } catch (Exception e) {
            NotificacionSlack.enviarmensajeSlack(e);
            throw new AssertionError(e.getMessage());
        }

    }

    @And("^segun el tramite se resuelve o deriva a area respectiva hasta (.*) el tramite (.*)$")
    public void segunElTramiteSeResuelveODerivaAAreaRespectivaHastaResolverElTramite(String opciones_resuelve_o_retipifica, String areaVal) {

        //TODO: TIPO DE RESOLUCION: PROCEDE - NO PROCEDE - RETIPIFICA
        System.out.println("todo a realizar: " + opciones_resuelve_o_retipifica);
        String opcion_resuelve_o_retipifica = opciones_resuelve_o_retipifica.split(":")[0];
        System.out.println("opcion a realizar: " + opcion_resuelve_o_retipifica);

        Serenity.setSessionVariable("configuracion_resolucion").to(opciones_resuelve_o_retipifica);

        String camposRetipificacion = "";
        //String camposMovimientos = "";

        //Serenity.setSessionVariable("camposRetipificacion").to("");
        //Serenity.setSessionVariable("camposMovimientos").to("");

        if (opciones_resuelve_o_retipifica.contains(":")) {
            camposRetipificacion = opciones_resuelve_o_retipifica.split(":")[1];
            //    camposMovimientos = opciones_resuelve_o_retipifica.split(":")[2];
            Serenity.setSessionVariable("camposRetipificacion").to(camposRetipificacion);

            //Serenity.setSessionVariable("camposMovimientos").to(camposMovimientos);

        }

        String tipoproducto = Serenity.sessionVariableCalled("tipoproducto");
        String tipotramite = Serenity.sessionVariableCalled("tipotramite");
        String tipologia = Serenity.sessionVariableCalled("tipologia");
        String comercial = Serenity.sessionVariableCalled("comercial");
        String tipoResol = Serenity.sessionVariableCalled("resolucion");


        EscenarioValidacion escenarioValidacion;
        resolucionComercial resCom;
        areaInicial = "";
        String areaResolutora = "";
        String areaValidadoraOpcional = "";

        try
        {
            areaValidadoraOpcional = Serenity.sessionVariableCalled("areaValidacionOpcional");
        }catch (Exception e)
        {
            areaValidadoraOpcional = "";
            System.out.println(e);
        }




        try {
            System.out.println("<<TIPO PRODUCTO: >> " + tipoproducto.toUpperCase(Locale.ROOT) + " <<TIPO TRAMITE: >> " + tipotramite + " <<NOMBRE TIPOLOGIA: >> " + tipologia + " <<AREA VALI: >> " + areaVal);

            if(comercial.equals("si")) {
                resCom = Do.getResolucionComercial(tipoproducto.toUpperCase(Locale.ROOT), tipotramite, tipologia, "");

                areaInicial = resCom.getAreaInicial();
                areaResolutora = resCom.getAreaResolutora();
                //areaValidadoraOpcional = resCom.getAreaValidadoraOpcional();

            }
            else {
                escenarioValidacion = Do.getEscenarioValidacion(tipoproducto.toUpperCase(Locale.ROOT), tipotramite, tipologia, "");

                areaInicial = escenarioValidacion.getAreaInicial();
                areaResolutora = escenarioValidacion.getAreaResolutora();
              //  areaValidadoraOpcional = areaValidadoraOpcional;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            if(!comercial.equals("si")) {
                throw new AssertionError("No se encuentra escenario de validación resolución" + e.getMessage());
            }
            else {
                areaInicial = "AREA";
                areaResolutora = "AREA";
                areaValidadoraOpcional = "";
            }
        }

        System.out.println("debug opciones");

        if (!Objects.equals(areaInicial, areaResolutora) && Objects.equals(areaValidadoraOpcional, "")) {
            System.out.println("CONECTAR: AREA RESOLUTORA NO - RESOLUCIÓN - VALIDADORA OPCIONAL VACIO SI ***");
            areaFinalDiferenteAreaResolutoriaYAreaValidadoraOpcionalVacio(opcion_resuelve_o_retipifica);
        }

        //TODO: EN AREA RESOLUTORA - FLUJO RETIPIFICACIÓN
        if (Objects.equals(areaInicial, areaResolutora) && Objects.equals(areaValidadoraOpcional, "")) {
            if (opcion_resuelve_o_retipifica.equals("RETIPIFICA")) {
                System.out.println("CONECTAR: RETIPIFICA ***");
                retipificacion.validaRetificacion();
            }
        }

        if (!Objects.equals(areaInicial, areaResolutora) && !Objects.equals(areaVal, "")) {

            //TODO: AREA VALIDADORA - PROCEDE VALIDACIÓN Y NO PROCEDE VALIDACIÓN
            System.out.println("CONECTAR: AREA VALIDADORA - PROCEDE VALIDACIÓN Y NO PROCEDE VALIDACIÓN ***");
            //USUARIO DE AREA ORIGEN DEBE MARCAR COMO PROCEDE PARA ENVIAR A AREA RESOLUTORA
            if((tipoResol.equals("Derivar-2-NoProcede")||tipoResol.equals("Derivar-2-Procede")))
            {

                if(tipoResol.equals("Derivar-2-NoProcede"))
                {
                    opcion_resuelve_o_retipifica="No Procede";
                }

                if (tipoResol.equals("Derivar-2-Procede")) {
                    opcion_resuelve_o_retipifica="Procede";
                }

                areaFinalDiferenteAreaResolutoriaYAreaValidadoraOpcionalVacio(opcion_resuelve_o_retipifica);
            }else{
                realizarDerivacionValidarEnOtraArea();
                areaFinalDiferenteAreaResolutoriaYAreaValidadoraOpcionalVacio("RESUELVE");
            }
           //Se espera a procesar tramite en el area final
            areaFinalDiferenteAreaResolutoriaYAreaValidadoraOpcionalVacio(opcion_resuelve_o_retipifica);
        }

        if (Objects.equals(areaInicial, areaResolutora) && !Objects.equals(areaVal, "")) {

            if(!areaValidadoraOpcional.equals("DerivarResolDirecto")) {
                realizarDerivacionValidarEnOtraArea();
            }
            else{
                Serenity.setSessionVariable("areaValidacionOpcional").to("");
            }
            //TODO: AREA RESOLUTORA - PROCEDE Y NO PROCEDE
            System.out.println("CONECTAR: AREA RESOLUTORA DIRECTO - PROCEDE Y NO PROCEDE ***");
            //Se espera a procesar tramite en el area final
            areaFinalDiferenteAreaResolutoriaYAreaValidadoraOpcionalVacio(opcion_resuelve_o_retipifica);
        }

        //TODO: OBSERVACION
        validacionRegistroPage.SalirDetalle();

        //SI LLEGO AQUI TERMINO RESOLVER TRAMITE OK SEGUNDA INSTANCIA
        String numeroTramite = Serenity.sessionVariableCalled("numeroTramite").toString();

        String tiporesol = Serenity.sessionVariableCalled("tipoResol");
        WriteToFile.writeLinesToFileResol(numeroTramite +",ok,"+tiporesol);
    }

    public void realizarDerivacionValidarEnOtraArea() {
        validacionRegistroPage.consultarServicioDeAreaAsignada();

        //CIERRO SESION
        theActorInTheSpotlight().attemptsTo(
                new CerrarGiru()
        );

        //INICIO SESION
        theActorInTheSpotlight().attemptsTo(
                new IniciarSesionReasignado()
        );

        //INGRESO TRAMITES
        theActorInTheSpotlight().attemptsTo(
                SeleccionarMenuPrincipal.withData("Tramites")
        );

        //INGRESO TRAMITE
        String numeroTramite = Serenity.sessionVariableCalled("numeroTramite").toString();

        theActorInTheSpotlight().attemptsTo(
                BuscarTramiteBandejaResolucion.withData(numeroTramite)
        );

        String resol = Serenity.sessionVariableCalled("resolucion");

        if (resol.equals("Procede")) {
            theActorInTheSpotlight().attemptsTo(
                    SeleccionarTipoResolucion.withData("Procede")
            );

            theActorInTheSpotlight().attemptsTo(
                    new ResolucionNoEnviaraCartaCliente()
            );

        }

        if (resol.equals("No Procede")) {
            theActorInTheSpotlight().attemptsTo(
                    SeleccionarTipoResolucion.withData("No Procede")
            );
            theActorInTheSpotlight().attemptsTo(
                    new ResolucionNoEnviaraCartaCliente()
            );

        }

        if (resol.equals("Derivar")) {
            theActorInTheSpotlight().attemptsTo(SeleccionarTipoResolucion.withData("Derivar"));
            theActorInTheSpotlight().attemptsTo(SeleccionarRazonYArea.withData("RequiereSerDevueltoAreaAnterior", areaInicial));
            theActorInTheSpotlight().attemptsTo(new ConfirmarResolucionDer());
            //CUSTODIA DE DOCUMENTOS FINAL
        }

    }

    public void areaFinalDiferenteAreaResolutoriaYAreaValidadoraOpcionalVacio(String opcion_resuelve_o_retipifica) {
        validacionRegistroPage.consultarServicioDeAreaAsignada();

        //CIERRO SESION
        theActorInTheSpotlight().attemptsTo(
                new CerrarGiru()
        );

        //INICIO SESION
        theActorInTheSpotlight().attemptsTo(
                new IniciarSesionReasignado()
        );

        //INGRESO TRAMITES
        theActorInTheSpotlight().attemptsTo(
                SeleccionarMenuPrincipal.withData("Tramites")
        );

        //INGRESO TRAMITE
        String numeroTramite = Serenity.sessionVariableCalled("numeroTramite").toString();

        theActorInTheSpotlight().attemptsTo(
                BuscarTramiteBandejaResolucion.withData(numeroTramite)
        );

        String tipoResol = Serenity.sessionVariableCalled("tipoResolF").toString();
        System.out.println(tipoResol);
        if (opcion_resuelve_o_retipifica.equals("RETIPIFICA")) {
            retipificacion.validaRetificacion();
        } else {

            System.out.println("ESTOY EN RESOLUCION - PROCEDE Y NO PROCEDE .....");
            theActorInTheSpotlight().attemptsTo(
                    SeleccionarTipoResolucion.withData(tipoResol)
            );

            theActorInTheSpotlight().attemptsTo(
                    IngresarComentarioInterno.withData("PRUEBA RESOLUCIÓN")
            );
            //Tener cuidado con esta variable! setear siempre!
            String subirarchivo = ApiCommons.SUBIARCHIVO;

            theActorInTheSpotlight().attemptsTo(
                    SubirArchivo.withData(subirarchivo)
            );

            theActorInTheSpotlight().attemptsTo(
                    new ResolucionNoEnviaraCartaCliente()
            );

        }

    }


}
