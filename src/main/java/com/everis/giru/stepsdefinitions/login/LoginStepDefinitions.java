package com.everis.giru.stepsdefinitions.login;

import com.everis.giru.rest.steps.ConsultaFilterInboxSteps;
import com.everis.giru.tasks.e2eTramites.IngresarSeccionComercial;
import com.everis.giru.tasks.login.*;
import com.everis.giru.userinterface.login.LoginPage;
import com.everis.giru.utils.ApiCommons;
import com.everis.giru.utils.NotificacionSlack;
import com.everis.giru.utils.WriteToFile;
import com.everis.tdm.Do;
import com.everis.giru.tasks.login.AbrirGiru;
import com.everis.giru.tasks.login.AutenticarGiru;
import com.everis.giru.tasks.login.ButtonIniciarSesion;
import com.everis.tdm.model.giru.Usuario;
import com.google.protobuf.Api;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.eo.Se;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.openqa.selenium.By;

import java.util.concurrent.TimeUnit;

import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;
import static org.junit.Assert.fail;

public class LoginStepDefinitions {

    ConsultaFilterInboxSteps consultaFilterInboxSteps = new ConsultaFilterInboxSteps();
    String nroAsesorReasignado = "";

    @Before
    public void before(Scenario scenario) {
        Serenity.setSessionVariable("scenario").to(scenario.getName());
        System.out.println(scenario.getName());
    }

    @Before
    public void setTheStage() {
        OnStage.setTheStage(new OnlineCast());
    }

    @Given("^que el usuario accede a la aplicacion GIRU$")
    public void validaUsuarioAccedeAplicacionGiru1() {
        try {
            theActorCalled("ASESOR").attemptsTo(
                    AbrirGiru.loginGiruPage()
            );
        } catch (Exception e) {
            NotificacionSlack.enviarmensajeSlack(e);
            throw new AssertionError(e.getMessage());
        }
    }

    @Given("^que el usuario (.*) accede a la aplicacion GIRU$")
    public void validaUsuarioAccedeAplicacionGiru(String rol) {
        try {
            //TODO: VARIABLE TEMPORAL COMERCIAL - SETEANDO
            Serenity.setSessionVariable("COMERCIAL").to("RETAIL");

            theActorCalled(rol).attemptsTo(
                    AbrirGiru.loginGiruPage()
            );

            //USAR TDM PARA TRAER USUARIO
            Usuario usuario = Do.getUsuarioPorRolTdm(rol);
            Serenity.setSessionVariable("comercial").to("no");
            Serenity.setSessionVariable("autonomia").to("no");
            //ESCRIBO USUARIO Y CONTRASENA
//            if (rol.equals("ESPECIALISTA") && Serenity.sessionVariableCalled("ambiente").equals("DEV")) {
//                theActorInTheSpotlight().attemptsTo(
//                        AutenticarGiru.withData("B31855", "Peru123.")
//                );
//            } else if (rol.equals("SUPERVISOR") && Serenity.sessionVariableCalled("ambiente").equals("DEV")) {
//                theActorInTheSpotlight().attemptsTo(
//                        AutenticarGiru.withData("B10951", "Peru123.")
//                );
//            } else {
                theActorInTheSpotlight().attemptsTo(
                        AutenticarGiru.withData(usuario.getUsuario(), usuario.getContrasena())
                );
//            }
            //CLICK INICIAR SESION
            theActorInTheSpotlight().attemptsTo(
                    new ButtonIniciarSesion()
            );


            //variables tdm
            if (rol.contains("ASESOR DE RETENCIÓN")) {
                ApiCommons.PERFIL_USUARIO = "ESPECIALISTA";
            } else if (rol.contains("SUPERVISOR RETENCIÓN")) {
                ApiCommons.PERFIL_USUARIO = "SUPERVISOR";
            } else if (rol.contains("GERENTE DE TIENDA/GERENTE ASISTENTE") || rol.contains("SUPERVISOR CC") || rol.contains("REPRESENTANTE FINANCIERO")) {
                ApiCommons.PERFIL_USUARIO = "ASESOR";
            } else {
                ApiCommons.PERFIL_USUARIO = rol;
            }

            ApiCommons.USUARIO = usuario.getUsuario();


               // Serenity.setSessionVariable("getProfile").to(rol);

           // Serenity.setSessionVariable("UsuarioSesion").to(usuario.getUsuario());

        } catch (Exception e) {
            WriteToFile.writeLinesToFile("erro2daInstancia");
            NotificacionSlack.enviarmensajeSlack(e);
            throw new AssertionError(e.getMessage());
        }
    }
    @Given("^que el usuario (.*) accede a la aplicacion GIRU en segunda instancia con tramite (.*)$")
    public void validaUsuarioAccedeAplicacionGiruEnSegundaInstancia(String rol, String tramite) {
        try {

           //TODO: VARIABLE TEMPORAL COMERCIAL - SETEANDO
            Serenity.setSessionVariable("COMERCIAL").to("RETAIL");

            theActorCalled(rol).attemptsTo(
                    AbrirGiru.loginGiruPage()
            );
            Serenity.setSessionVariable("fueDerivado").to("No");
            Serenity.setSessionVariable("estoyEnResolutoria").to("No");
            String numeroTramite = tramite;
            consultaFilterInboxSteps.seArmaElCuerpoDePeticionCreacionCliente(numeroTramite);

            for (int i = 1; i < 4; i++) {
                if (consultaFilterInboxSteps.agregoLosEncabezadosEnLaPeticion2() == 200) {
                    System.out.println("Nro de veces que consulta al servicio de Derivación: " + i);
                    break;
                }
            }

            consultaFilterInboxSteps.validoLaRespuestaObtenida();
            nroAsesorReasignado = consultaFilterInboxSteps.validoCamposObtenidos();

            Serenity.setSessionVariable("nrotramite").to(numeroTramite);
            Serenity.setSessionVariable("numeroTramite").to(numeroTramite);
            Serenity.setSessionVariable("nroAsesorReasignado").to(nroAsesorReasignado);



                theActorInTheSpotlight().attemptsTo(
                        AutenticarGiru.withData(nroAsesorReasignado, "Peru123.")
                );

            //CLICK INICIAR SESION
            theActorInTheSpotlight().attemptsTo(
                    new ButtonIniciarSesion()
            );
                Serenity.getDriver().manage().timeouts().implicitlyWait(7, TimeUnit.SECONDS);
                if (Serenity.getDriver().findElements(By.xpath("//p[text()=' *EL ID O CONTRASEÑA NO COINCIDEN ']")).size() > 0) {
                    WriteToFile.writeLinesToFile("erro2daInstancia");
                    WriteToFile.writeLinesToFileResol(tramite +",errorUsuario");

                    throw new AssertionError("ERROR! --> EL ID O CONTRASEÑA NO COINCIDEN ");
                }
            //variables tdm
            if (rol.contains("ASESOR DE RETENCIÓN")) {
               ApiCommons.PERFIL_USUARIO ="ESPECIALISTA";
            } else if (rol.contains("SUPERVISOR RETENCIÓN")) {
                ApiCommons.PERFIL_USUARIO ="SUPERVISOR";
            } else if (rol.contains("GERENTE DE TIENDA/GERENTE ASISTENTE") || rol.contains("SUPERVISOR CC") || rol.contains("REPRESENTANTE FINANCIERO")) {
                ApiCommons.PERFIL_USUARIO ="ASESOR";
            } else {
                ApiCommons.PERFIL_USUARIO ="ESPECIALISTA";
            }

            ApiCommons.USUARIO = nroAsesorReasignado;

        } catch (Exception e) {
            WriteToFile.writeLinesToFile("erro2daInstancia");
            WriteToFile.writeLinesToFileResol(tramite +",error");


            NotificacionSlack.enviarmensajeSlack(e);
            throw new AssertionError(e.getMessage());
        }
    }

    @Given("^que el usuario (.*) accede a la aplicacion GIRU (COMERCIAL)$")
    public void validaUsuarioAccedeAplicacionGiruComercial(String rol, String valor) {
        try {

            theActorCalled(rol).attemptsTo(
                    AbrirGiru.loginGiruPage()
            );

            if (valor.equals("COMERCIAL")) {
                Serenity.setSessionVariable("COMERCIAL").to("COMERCIAL");
            } else {
                Serenity.setSessionVariable("COMERCIAL").to("RETAIL");
            }
            Serenity.setSessionVariable("comercial").to("si");
            Serenity.setSessionVariable("autonomia").to("no");
            //USAR TDM PARA TRAER USUARIO
            Usuario usuario = Do.getUsuarioPorRolTdm(rol);

            //ESCRIBO USUARIO Y CONTRASENA
            if (rol.equals("ESPECIALISTA") && Serenity.sessionVariableCalled("ambiente").equals("DEV")) {
                theActorInTheSpotlight().attemptsTo(
                        AutenticarGiru.withData("B31855", "Peru123.")
                );
            } else if (rol.equals("SUPERVISOR") && Serenity.sessionVariableCalled("ambiente").equals("DEV")) {
                theActorInTheSpotlight().attemptsTo(
                        AutenticarGiru.withData("B10951", "Peru123.")
                );
            } else {
                theActorInTheSpotlight().attemptsTo(
                        AutenticarGiru.withData(usuario.getUsuario(), usuario.getContrasena())
                );
            }
            //CLICK INICIAR SESION
            theActorInTheSpotlight().attemptsTo(
                    new ButtonIniciarSesion()
            );

            //TODO: INGRESAR A SECCIÓN - COMERCIAL
            theActorInTheSpotlight().attemptsTo(
                    new IngresarSeccionComercial()
            );

            //variables tdm
           // g.to(rol);
            ApiCommons.PERFIL_USUARIO = rol;
            ApiCommons.USUARIO = usuario.getUsuario();
        } catch (Exception e) {
            NotificacionSlack.enviarmensajeSlack(e);
            throw new AssertionError(e.getMessage());
        }
    }

    @When("^se ingresan usuario (.*) y contrasena (.*)$")
    public void validaIngresoUsuarioContrasena(String user, String password) {
        try {
            theActorInTheSpotlight().attemptsTo(
                    AutenticarGiru.withData(user, password)
            );

            ApiCommons.USUARIO =user;

        } catch (Exception e) {
            NotificacionSlack.enviarmensajeSlack(e);
            throw new AssertionError(e.getMessage());
        }
    }

    @And("selecciona la opcion iniciar sesion")
    public void seleccionaLaOpcionIniciarSesion() {
        try {

            theActorInTheSpotlight().attemptsTo(
                    new ButtonIniciarSesion()
            );

        } catch (Exception e) {
            NotificacionSlack.enviarmensajeSlack(e);
            throw new AssertionError(e.getMessage());
        }
    }

    @And("se cierra sesion del usuario")
    public void seCierraSesionDelUsuario() {
        try {
            theActorInTheSpotlight().attemptsTo(
                    new CerrarGiru()
            );

        } catch (Exception e) {
            NotificacionSlack.enviarmensajeSlack(e);
            throw new AssertionError(e.getMessage());
        }
    }

    @And("^se inicia sesion con el usuario para realizar (.*)$")
    public void seIniciaSesionConElUsuarioParaRealizarProrrogas(String proceso) {
        Serenity.setSessionVariable("COMERCIAL").to("RETAIL");

        theActorCalled("ESPECIALISTA").attemptsTo(
                AbrirGiru.loginGiruPage()
        );


        theActorInTheSpotlight().attemptsTo(
                AutenticarGiru.withData("B10181", "Abc2023**")
        );

        ApiCommons.PERFIL_USUARIO = "ESPECIALISTA";
        ApiCommons.USUARIO = "B10181";

        theActorInTheSpotlight().attemptsTo(
                WaitUntil.the(LoginPage.BTN_INICIAR_SESION, isVisible()).forNoMoreThan(30).seconds(),
                Click.on(LoginPage.BTN_INICIAR_SESION)
        );


    }
}
