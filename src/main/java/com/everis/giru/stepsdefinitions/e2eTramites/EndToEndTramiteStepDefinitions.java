package com.everis.giru.stepsdefinitions.e2eTramites;

import com.everis.giru.tasks.e2eTramites.*;
import com.everis.giru.tasks.login.ButtonIniciarSesion;
import com.everis.giru.tasks.login.CerrarGiru;
import com.everis.giru.tasks.tramites.ButtonBuscar;
import com.everis.giru.tasks.tramites.SeleccionarTipoTramites;
import com.everis.giru.tasks.tramites.SeleccionarTipologia2;
import com.everis.giru.tasks.tramites.ValidaTipologiaDeterminada;
import com.everis.giru.userinterface.e2eTramite.GetNumeroTramitePage;
import com.everis.giru.userinterface.e2eTramite.resolucionTramite.SeleccionaTramitePage;
import com.everis.giru.userinterface.e2eTramite.validaBusquedaHistorico.ButtomBuscarEnHistorialPage;
import com.everis.giru.userinterface.e2eTramite.validaBusquedaHistorico.ValidaBuscarTramiteEnHistorialPage;
import com.everis.giru.userinterface.e2eTramite.validaBusquedaHistorico.ValidaBuscarTramiteResolutorEnHistorialPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import net.serenitybdd.core.Serenity;

import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class EndToEndTramiteStepDefinitions {

    GetNumeroTramitePage getNumeroTramitePage = new GetNumeroTramitePage();
    GetHistoricalInbox getHistoricalInbox = new GetHistoricalInbox();
    ButtomBuscarEnHistorialPage buttomBuscarEnHistorialPage = new ButtomBuscarEnHistorialPage();
    ValidaBuscarTramiteEnHistorialPage validaBuscarTramiteEnHistorialPage = new ValidaBuscarTramiteEnHistorialPage();
    SeleccionaTramitePage seleccionaTramitePage = new SeleccionaTramitePage();
    ValidaBuscarTramiteResolutorEnHistorialPage validaBuscarTramiteResolutorEnHistorialPage = new ValidaBuscarTramiteResolutorEnHistorialPage();
    GetNombrePerfil getNombrePerfil = new GetNombrePerfil();
    String NombrePerfilConsulta = null;
    String TipologiaConsulta = null;
    String atencionTramiteE2E = null;

    @And("selecciona la opcion registrar")
    public void seleccionaLaOpcionRegistrar() {
//        theActorInTheSpotlight().attemptsTo(
//                new RegistraTramite()
//        );

        //TODO: OBTENER TRAMITE - FRONT
        getNumeroTramitePage.validaObtenerTramiteFront();

    }

    @And("se consulta numero tramite registrado")
    public void seConsultaNumeroTramiteRegistrado() {

        //TODO: CONSULTAR TRAMITE - HISTORICAL INBOX - API
        getHistoricalInbox.setContadorIntentosConsultaTramiteACero();
        GetHistoricalInbox.consultarTramite();
        getHistoricalInbox.departamentoGlobalTramite();
        getHistoricalInbox.tipologiaGlobalTramite();
        getNumeroTramitePage.validaTerminarAtencion();
    }

    @And("^se selecciona la opcion (.*) para E2E$")
    public void validaSeleccionaOpcionTipologiaPerfilPorDepartamentoE2E(String tipologia) throws InterruptedException {
        System.out.println("DEPARTAMENTO SETEADO : " + getHistoricalInbox.getDepartamentoGlobal() + "\n");
        String usuarioConsultarPerfil = Serenity.sessionVariableCalled("userCode").toString();
        System.out.println("usuario a consultar : " + usuarioConsultarPerfil);

        GetNombrePerfil.obtenerNombrePerfilGiru(usuarioConsultarPerfil);
        NombrePerfilConsulta = getNombrePerfil.getProfileName();
        System.out.println("Perfil Selecciona : " + NombrePerfilConsulta);
        theActorInTheSpotlight().attemptsTo(
                SeleccionarTipologia2.withData(tipologia, NombrePerfilConsulta, getHistoricalInbox.getDepartamentoGlobal())
        );

        Thread.sleep(1000);
    }

    @And("se selecciona la tipologia del tramite creado para E2E")
    public void validaSeleccionTipologiaE2E() throws InterruptedException {
        System.out.println("TIPOLOGIA SETEADO : " + getHistoricalInbox.getTipologiaGlobal() + "\n");

        System.out.println("Perfil Selecciona : " + NombrePerfilConsulta);

        TipologiaConsulta = getHistoricalInbox.getTipologiaGlobal();

        if (NombrePerfilConsulta.equals("Supervisor")) {
            TipologiaConsulta = "Resolver";
        }

        theActorInTheSpotlight().attemptsTo(
                SeleccionarTipologia2.withData(TipologiaConsulta, NombrePerfilConsulta, getHistoricalInbox.getDepartamentoGlobal())
        );

        Thread.sleep(1000);
    }


    @Then("^se visualiza un perfil de la tipologia (.*) para E2E$")
    public void validaVisualizaPerfilPorTipologiaDepartamentoE2E(String tipologia) throws InterruptedException {
        Thread.sleep(1000);

        System.out.println("Perfil valida : " + NombrePerfilConsulta);
        theActorInTheSpotlight().attemptsTo(
                ValidaTipologiaDeterminada.withData(NombrePerfilConsulta, tipologia, getHistoricalInbox.getDepartamentoGlobal())
        );
        Serenity.setSessionVariable("perfilBuscar").to(NombrePerfilConsulta);
        Serenity.setSessionVariable("tipologiaBuscar").to(tipologia);
        Serenity.setSessionVariable("departamentoBuscar").to(getHistoricalInbox.getDepartamentoGlobal());
    }

    @Then("se visualiza el perfil de la tipologia para E2E")
    public void validaVisualizaTipologiaE2E() throws InterruptedException {
        Thread.sleep(1000);

        System.out.println("Perfil valida : " + NombrePerfilConsulta);
        theActorInTheSpotlight().attemptsTo(
                ValidaTipologiaDeterminada.withData(NombrePerfilConsulta, TipologiaConsulta, getHistoricalInbox.getDepartamentoGlobal())
        );

        Serenity.setSessionVariable("perfilBuscar").to(NombrePerfilConsulta);
        Serenity.setSessionVariable("tipologiaBuscar").to(TipologiaConsulta);
        Serenity.setSessionVariable("departamentoBuscar").to(getHistoricalInbox.getDepartamentoGlobal());
    }

    @And("selecciona la opcion terminar atencion")
    public void seleccionaOpcionTerminarAtencion() {

        theActorInTheSpotlight().attemptsTo(
                new CerrarGiru()
        );

    }

    @Given("que el usuario inicia sesion con tramite reasignado")
    public void validaUsuarioReasignadoIniciaSesion() {

        theActorInTheSpotlight().attemptsTo(
                new IniciaSesionReasignado()
        );

        theActorInTheSpotlight().attemptsTo(
                new ButtonIniciarSesion()
        );

    }

    @And("^se realiza la opcion buscar por (.*) y numero tramite de tramite$")
    public void realizaOpcionBuscarPorNumeroTramite(String tipoTramite) {

        theActorInTheSpotlight().attemptsTo(
                SeleccionarTipoTramites.withData(tipoTramite, getHistoricalInbox.getNumeroTramiteFront())
        );

        theActorInTheSpotlight().attemptsTo(
                new ButtonBuscar()
        );

    }

    @And("^realiza la opcion buscar por (.*) en Historico$")
    public void realizaOpcionBuscarPorNumeroTramiteHistorico(String tipoTramite) throws InterruptedException {

        Thread.sleep(1000);
        theActorInTheSpotlight().attemptsTo(
                SeleccionarTipoTramitesHistorial.withData(tipoTramite, getHistoricalInbox.getNumeroTramiteFront())
        );

        buttomBuscarEnHistorialPage.BuscarTramite();
        Thread.sleep(2000);

    }

    @Then("se verifica tramite en Historico")
    public void seValidaTramiteEnHistorico() {

        validaBuscarTramiteEnHistorialPage.validaBuscarTramite();

    }

    @And("selecciona el tramite en su bandeja respectiva y valida datos en resumen")
    public void seleccionaTramiteenBandeja() {

        seleccionaTramitePage.seleccionarTramiteBandeja();

        theActorInTheSpotlight().attemptsTo(
                new ValidaDatosResumenBandeja()
        );

    }

    @And("^realiza accion de (.*)$")
    public void realizaProcedimientoBandeja(String atencion) throws InterruptedException {
        atencionTramiteE2E = atencion;
        theActorInTheSpotlight().attemptsTo(
                RealizarAtencionE2E.withData(atencion)
        );
        Thread.sleep(2000);

        Serenity.setSessionVariable("perfilBuscar").to(atencion);
    }

    @Then("se verifica en el historico según su atencion")
    public void validaHistoricoAtencionRealizadaE2E() {
        validaBuscarTramiteResolutorEnHistorialPage.validaBuscarTramiteResolutor(atencionTramiteE2E);

    }

}
