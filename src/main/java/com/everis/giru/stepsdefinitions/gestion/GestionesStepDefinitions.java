package com.everis.giru.stepsdefinitions.gestion;

import com.everis.giru.tasks.gestion.IngresaBusquedaGestiones;
import com.everis.giru.tasks.gestion.RegistraGestiones;
import com.everis.giru.tasks.gestion.ValidaGestiones;
import com.everis.giru.userinterface.tramites.historial.ValidaNumeroTramiteHistorialPage;
import com.everis.giru.utils.ApiCommons;
import com.everis.giru.utils.WriteToFile;
import com.everis.tdm.Do;
import com.everis.tdm.model.giru.RegistroGestion;
import com.google.protobuf.Api;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.core.Serenity;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;
import java.util.Map;

import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;



public class GestionesStepDefinitions {
    ValidaNumeroTramiteHistorialPage validaNumeroTramiteHistorialPage = new ValidaNumeroTramiteHistorialPage();

    @When("realiza el registro de la gestión con los datos ingresados en la opcion recopila data de {string} para un cliente")
    public void gestionRegistro(String fuente, DataTable dataTable) throws InterruptedException {
        List<Map<String, String>> list = dataTable.asMaps(String.class, String.class);

        ApiCommons.GESTIONES_FUENTE_DATA = fuente;
        ApiCommons.GESTIONES_TIPO_BUSQUEDA = "Cliente";
        ApiCommons.GESTIONES_TIPO_DOC = list.get(0).get("tipoDocumento");
        ApiCommons.clientData = Do.spGetClientByDocumentType(ApiCommons.GESTIONES_TIPO_DOC);

        String gestionDescripcion = list.get(0).get("gestionDescripcion");
        String llamada = list.get(0).get("llamada");
        String comentario = list.get(0).get("comentario");

        //TODO: USAR SCREENPLAY - USAR TASK - CONSULTA GIRU-->CLIENTE-->TIPODOC-->INGRESA DOC
        theActorInTheSpotlight().attemptsTo(
            IngresaBusquedaGestiones.withData(ApiCommons.GESTIONES_TIPO_DOC, ApiCommons.clientData.get(0).getDocumentClient()));

        //TODO: INICIA REGISTRO DE "NUEVA GESTIÓN"
        theActorInTheSpotlight().attemptsTo(
            RegistraGestiones.withData(gestionDescripcion,comentario,llamada));

        //TODO: GUARDA EL REGISTRO CREADO EN TDM
        //GUARDA EL REGISTRO CREADO EN LA CLASE PARA PODER SER USADO EN LA VALIDACIÓN
        ApiCommons.registroGestion = new RegistroGestion();
        ApiCommons.registroGestion.setNroTramite(ApiCommons.GESTIONES_NRO_TRAMITE);
        ApiCommons.registroGestion.setFuenteData(ApiCommons.GESTIONES_FUENTE_DATA);
        ApiCommons.registroGestion.setTipoRegistro(ApiCommons.GESTIONES_TIPO_BUSQUEDA);
        ApiCommons.registroGestion.setTipoDocumento(ApiCommons.GESTIONES_TIPO_DOC);
        ApiCommons.registroGestion.setNumDocumento(ApiCommons.clientData.get(0).getDocumentClient());
        ApiCommons.registroGestion.setIdGestion(ApiCommons.tipoGestion.get(0).getDescripcionGestion() + "#" + ApiCommons.tipoGestion.get(0).getDescripcionGestionHija());
        ApiCommons.registroGestion.setLlamada(llamada);
        ApiCommons.registroGestion.setComentario(comentario);
        ApiCommons.registroGestion.setUsuario(ApiCommons.PERFIL_USUARIO + " - "+ ApiCommons.USUARIO);
        Do.PostRegistroGestionTdm(ApiCommons.GESTIONES_NRO_TRAMITE,
                ApiCommons.registroGestion.getUsuario(),
                ApiCommons.GESTIONES_FUENTE_DATA,
                ApiCommons.registroGestion.getTipoRegistro(),
                ApiCommons.GESTIONES_TIPO_DOC,
                ApiCommons.registroGestion.getNumDocumento(),
                ApiCommons.registroGestion.getIdGestion(),
                llamada,
                comentario);


    }

    @When("realiza la recopilacion de data de Giru para gestiones")
    public void gestionRegistroXXY(DataTable dataTable) throws InterruptedException {
        List<Map<String, String>> list = dataTable.asMaps(String.class, String.class);
        int resultado = 0;

        String NumDocumento = list.get(resultado).get("NumDocumento");

            Serenity.getDriver().findElement(By.xpath("//span[contains(text(), 'Seleccione')]/../../../..")).click();
            Serenity.getDriver().findElement(By.xpath("//span[contains(text(), 'Seleccione')]/../../../..//span[contains(text(), 'DNI')]")).click();

        WebElement txtGestiones = Serenity.getDriver().findElement(By.xpath("//ibk-inputgroup-text[@formcontrolname='document']"));
        txtGestiones.sendKeys("71401090");
        txtGestiones.sendKeys(Keys.ENTER);

            try {
                Thread.sleep(2000);
            } catch (Exception e) {
                System.out.println(e);
            }
            WebElement btnNuevaGestion = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(30)).until(ExpectedConditions.elementToBeClickable(By.xpath("//button[contains(text(), 'Registrar nueva gestión')]")));
            btnNuevaGestion.click();

            WebElement tipoGestion = Serenity.getDriver().findElement(By.xpath("//app-ibk-select[@id='managementType']"));
        WebElement gest = Serenity.getDriver().findElement(By.xpath("//app-ibk-select[@id='management']"));

            int maxGest =  Serenity.getDriver().findElements(By.xpath("//app-ibk-select[@id='managementType']//li")).size();

            for(int i = 167;i <=maxGest;i++)
            {
                tipoGestion.click();
                Thread.sleep(2000);

                WebElement eleI = Serenity.getDriver().findElement(By.xpath("(//app-ibk-select[@id='managementType']//li)["+ i + "]"));
                String eleIname = eleI.getText();
                String eleId = eleI.getAttribute("id");

                Serenity.getDriver().findElement(By.xpath("(//app-ibk-select[@id='managementType']//li)["+ i + "]")).click();

                Thread.sleep(2000);
                int max = Serenity.getDriver().findElements(By.xpath("//app-ibk-select[@id='management']//li")).size();
                gest.click();
                for(int j = 1;j <=max;j++) {

                   WebElement eleJ =  Serenity.getDriver().findElement(By.xpath("(//app-ibk-select[@id='management']//li)[" + j + "]//span"));
                    WebElement eleJli =  Serenity.getDriver().findElement(By.xpath("(//app-ibk-select[@id='management']//li)[" + j + "]"));

                    String eleJname = eleJ.getText();
                    String eleJliId = eleJli.getAttribute("id");

                    Thread.sleep(500);
                    WriteToFile.writeLinesToFileGestion(eleJliId + "|"+ eleJname + "|"+ eleId);
                }
                gest.click();
            }

    }

    @Then("se crea la gestion de manera exitosa")
    public void seCreaLaGestionDeManeraExitosa() throws InterruptedException {
        //Thread.sleep(3000);
        ApiCommons.GESTIONES_FUENTE_DATA = "Giru";
        theActorInTheSpotlight().attemptsTo(
                IngresaBusquedaGestiones.withData(ApiCommons.GESTIONES_TIPO_DOC,ApiCommons.clientData.get(0).getDocumentClient())
        );

        //VALIDACION POR FRONT BUSQUEDA
        String NombreTipoGestionTDM =  ApiCommons.registroGestion.getIdGestion().split("#")[0];
        String NombreGestionTDM =  ApiCommons.registroGestion.getIdGestion().split("#")[1];

        //APLICO FILTRO VALIDACIONES
        Serenity.getDriver().findElement(By.xpath("//*[contains(text(), ' Tipo de gestiones ')]")).click();

       if(Serenity.getDriver().findElements(By.xpath("//*[text() = ' "+NombreTipoGestionTDM+"']")).size()>0) {
           Serenity.getDriver().findElement(By.xpath("//*[text() = ' " + NombreTipoGestionTDM + "']")).click();
       }
          else
        {
            Serenity.getDriver().findElement(By.xpath("//*[contains(text(), ' Tipo de gestiones ')]/..//label[@for='tab-"+ApiCommons.tipoGestion.get(0).getIdTipoGestion()+"']")).click();

        }

        Serenity.getDriver().findElement(By.xpath("//*[contains(text(), 'Aplicar')]")).click();
        WebElement element = Serenity.getDriver().findElement(By.xpath("//*[contains(text(), 'Gestión')]"));
        element.click();
        //List<WebElement> elementosGestionList = Serenity.getDriver().findElements(By.xpath("//*[contains(text(), '  Gestión  ')]/..//input[@formcontrolname='management']"));
        if(Serenity.getDriver().findElements(By.xpath("//*[contains(text(), 'Gestión')]/..//*[contains(text(), '"+NombreGestionTDM+"')]")).size()>0)
        {
            Serenity.getDriver().findElement(By.xpath("//*[contains(text(), 'Gestión')]/..//*[contains(text(), '"+NombreGestionTDM+"')]")).click();
        }
        else
        {
            Serenity.getDriver().findElement(By.xpath("//*[contains(text(), 'Gestión')]/..//label[@for='tab-"+ApiCommons.tipoGestion.get(0).getIdTipoGestionHija()+"']")).click();

        }
          Serenity.getDriver().findElement(By.xpath("//*[contains(text(), 'Aplicar')]")).click();

        validaNumeroTramiteHistorialPage.gestionesTramites();
    }

    @And("se valida la gestion creada por usuario")
    public void seValidaLaGestionCreadaPorUsuario() {
        theActorInTheSpotlight().attemptsTo(
              new ValidaGestiones()
        );
        System.out.println("AQUI DEBO VALIDAR");
    }
}
