package com.everis.giru.stepsdefinitions.atencionCliente;

import com.everis.giru.rest.steps.ConsultaFilterInboxSteps;
import com.everis.giru.tasks.e2eTramites.IngresaSegmentoComercial;
import com.everis.giru.tasks.login.SeleccionarMenuPrincipal;
import com.everis.giru.tasks.tramites.IngresaDatosComercio;
import com.everis.giru.utils.ApiCommons;
import com.everis.giru.utils.ExcelFileService;
import com.everis.giru.utils.NotificacionSlack;
import com.everis.giru.utils.WriteToFile;
import com.everis.tdm.Do;
import com.everis.tdm.model.giru.ClienteTarjeta;
import com.everis.giru.tasks.atencionCliente.*;
import com.everis.tdm.model.giru.EscenarioValidacion;
import com.google.protobuf.Api;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.*;
import net.serenitybdd.core.Serenity;
import com.everis.giru.userinterface.atencionCliente.ValidacionesRegistroPage;
import net.serenitybdd.model.environment.EnvironmentSpecificConfiguration;
import net.thucydides.model.util.EnvironmentVariables;
import org.openqa.selenium.By;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class AtencionClienteStepDefinitions {
    String esComercial = "";
    EnvironmentVariables environmentVariables;
    ValidacionesRegistroPage validacionRegistroPage = new ValidacionesRegistroPage();
    ConsultaFilterInboxSteps consultaFilterInboxSteps = new ConsultaFilterInboxSteps();

    @And("se realiza la identificacion del cliente")
    public void validaIdentificarCliente() {
        try {
            String getProfile = ApiCommons.PERFIL_USUARIO;

            if (Objects.equals(getProfile, "ASESOR")) {
                String getTipoDocumento = Serenity.sessionVariableCalled("getTipoDocumento").toString();
                String getNumeroDocumento = Serenity.sessionVariableCalled("getNumeroDocumento").toString();

                theActorInTheSpotlight().attemptsTo(IdentificarCliente.withData(getTipoDocumento, getNumeroDocumento));

            }
        } catch (Exception e) {
            NotificacionSlack.enviarmensajeSlack(e);
            throw new AssertionError(e.getMessage());
        }
    }

    @And("si aplica comision de membresía, se selecciona la Razon Operativa {string} y el Tipo de Razon Operativa {string}")
    public void registraRazonOperativa(String razonOperativa, String tipoRazonOperativa) {

        try {
            theActorInTheSpotlight().attemptsTo(RegistraRazonOperativa.withData(razonOperativa, tipoRazonOperativa));
        } catch (Exception e) {
            NotificacionSlack.enviarmensajeSlack(e);
            throw new AssertionError(e.getMessage());
        }
    }

    @And("se adjuntara un archivo adicional en caso sea necesario")
    public void seAdjuntaraUnArchivoAdicionalEnCasoSeaNecesario() {
        try {

            String subirarchivo = Serenity.sessionVariableCalled("subirarchivo").toString();
            String tipoResol = Serenity.sessionVariableCalled("tipoResol").toString();

            if (!tipoResol.equals("Retipificar")) {
                theActorInTheSpotlight().attemptsTo(
                        SubirArchivo.withData(subirarchivo)
                );
            }

        } catch (Exception e) {
            NotificacionSlack.enviarmensajeSlack(e);

            String nrotramite = Serenity.sessionVariableCalled("nrotramite");
            WriteToFile.writeLinesToFileResol(nrotramite +",error");

            throw new AssertionError(e.getMessage());
        }

    }

    @And("se selecciona el medio de respuesta {string} segun la configuracion de la respuesta {string}")
    public void selecionaElMedioDeRespuestaSegunLaConfiguracionDeLaRespuesta(String mediorespuesta, String nuevoregistro) {
        try {

            theActorInTheSpotlight().attemptsTo(
                    RegistraMedioRespuesta.withData(mediorespuesta, nuevoregistro)
            );

            Serenity.setSessionVariable("mediorespuesta").to(mediorespuesta);
            Serenity.setSessionVariable("nuevoregistro").to(nuevoregistro);
            Serenity.setSessionVariable("getMedioRespuesta").to(mediorespuesta);
            Serenity.setSessionVariable("getNuevoRegistroRespuesta").to(nuevoregistro);

        } catch (Exception e) {
            NotificacionSlack.enviarmensajeSlack(e);
            throw new AssertionError(e.getMessage());
        }
    }

    @Then("se registra el tramite")
    public void seRegistraElTramite() {

        //TODO: GUARDAR NUMERO - TRAMITE
        String numeroTramite = validacionRegistroPage.guardarNumeroTramite();
        Serenity.setSessionVariable("numeroTramite").to(numeroTramite);

        String tipologia = "";
        String EstadoTarjeta = "";
        String MarcaTarjeta = "";
        String TipoTarjeta = "";
        String UsuarioRegistro = "";
        String Perfil = "";
        String TipoDocumento = "";

        String NumeroDocumento = "";
        String NumeroTarjeta = "";
        String NumeroCuenta = "";

        String Tramite = "";
        String Atencion = "";

        String AgregarComentarios = "";
        String RespuestaCliente = "";
        String AdjuntarArchivo = "";
        String MedioRespuesta = "";
        String NumeroCelular = "";
        String NuevoRegistroRespuesta = "";
        String TipoCMPMalConcretado = "";

        String RazonOperativa = "";
        String Operador = "";
        tipologia = Serenity.sessionVariableCalled("tipologia").toString();

        EstadoTarjeta = "";
        MarcaTarjeta = "";
        TipoTarjeta = "";
//        EstadoTarjeta = Serenity.sessionVariableCalled("estadotarjeta").toString();
//        MarcaTarjeta = Serenity.sessionVariableCalled("marcatarjeta").toString();
//        TipoTarjeta = Serenity.sessionVariableCalled("tipotarjeta").toString();

        UsuarioRegistro = ApiCommons.USUARIO;
        Perfil = ApiCommons.PERFIL_USUARIO;
        TipoDocumento = ApiCommons.clientes.getTipodocumento();

        NumeroDocumento = ApiCommons.clientes.getNumerodocumento();

        try {
            NumeroTarjeta = Serenity.sessionVariableCalled("nrotarjeta").toString();
        } catch (Exception e) {
            NumeroTarjeta = "";
            System.out.println(e.getMessage());
        }
        try {
            NumeroCuenta = Serenity.sessionVariableCalled("nrocuenta").toString();
        } catch (Exception e) {
            NumeroCuenta = "";
            System.out.println(e.getMessage());
        }
        Tramite = Serenity.sessionVariableCalled("tipotramite").toString();
        try {
            Atencion = Serenity.sessionVariableCalled("tipoproducto").toString();
        } catch (Exception e) {
            Atencion = "";
            System.out.println(e.getMessage());
        }

        AgregarComentarios = Serenity.sessionVariableCalled("comentarioAdicional").toString();
        RespuestaCliente = Serenity.sessionVariableCalled("respuestaCliente").toString();
        AdjuntarArchivo = Serenity.sessionVariableCalled("subirarchivo").toString();

        try {
            MedioRespuesta = Serenity.sessionVariableCalled("mediorespuesta").toString();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        try {
            NumeroCelular = Serenity.sessionVariableCalled("numcelular").toString();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        try {
            NuevoRegistroRespuesta = Serenity.sessionVariableCalled("nuevoregistro").toString();
        } catch (Exception e) {
            NuevoRegistroRespuesta = "";
            System.out.println(e.getMessage());
        }

        try {
            TipoCMPMalConcretado = Serenity.sessionVariableCalled("TipoCMPMalConcretado").toString();
        } catch (Exception e) {
            TipoCMPMalConcretado = "";
            System.out.println(e.getMessage());
        }
        try {
            RazonOperativa = Serenity.sessionVariableCalled("razonopertiva").toString();
        } catch (Exception e) {
            RazonOperativa = "";
            System.out.println(e.getMessage());
        }

        try {
            Operador = Serenity.sessionVariableCalled("operador").toString();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        String FechaConsumoMalConcretado = "";
        String TipoRazonOperativa = "";
        try {
            TipoRazonOperativa = Serenity.sessionVariableCalled("tiporazon").toString();
        } catch (Exception e) {
            TipoRazonOperativa = "";
            System.out.println(e.getMessage());
        }
        TipoRazonOperativa = "DataProrroga";
        String MotivoSucedidoTarjeta = "";
        String SeleccionarNoBloqueado = "";
        String DescripcionNoBloqueado = "";
        String NombreComercio = "";
        String CantidadTarjetas = "";

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("uuuu/MM/dd HH:mm:ss");
        ZonedDateTime now = ZonedDateTime.now();

        try {
            System.out.println(numeroTramite + " , " + tipologia + " , " + dtf.format(now) + " , " + EstadoTarjeta + " , " + MarcaTarjeta + " , " + TipoTarjeta + " , " + UsuarioRegistro + " , " + Perfil + " , " + TipoDocumento + " , " + NumeroDocumento + " , " + NumeroTarjeta + " , " + NumeroCuenta + " , " + Tramite + " , " + Atencion + " , " + AgregarComentarios + " , " + RespuestaCliente + " , " + AdjuntarArchivo + " , " + MedioRespuesta + " , " + NumeroCelular + " , " + NuevoRegistroRespuesta + " , " + RazonOperativa + " , " + TipoCMPMalConcretado + " , " + Operador + " , " + FechaConsumoMalConcretado + " , " + TipoRazonOperativa + " , " + MotivoSucedidoTarjeta + " , " + SeleccionarNoBloqueado + " , " + DescripcionNoBloqueado + " , " + NombreComercio + " , " + CantidadTarjetas);
            Do.PostRegistrarReclamoTdm(numeroTramite, tipologia, dtf.format(now), EstadoTarjeta, MarcaTarjeta, TipoTarjeta, UsuarioRegistro, Perfil, TipoDocumento, NumeroDocumento, NumeroTarjeta, NumeroCuenta, Tramite, Atencion, AgregarComentarios, RespuestaCliente, AdjuntarArchivo, MedioRespuesta, NumeroCelular, NuevoRegistroRespuesta, RazonOperativa, TipoCMPMalConcretado, Operador, FechaConsumoMalConcretado, TipoRazonOperativa, MotivoSucedidoTarjeta, SeleccionarNoBloqueado, DescripcionNoBloqueado, NombreComercio, CantidadTarjetas);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        String prorroga = "no";
        String fecharecepcion = "no";
        String estadoenvio = "no";
        String comentariocarta = "no";




        String frente = Serenity.sessionVariableCalled("COMERCIAL");


        try {
            prorroga = Serenity.sessionVariableCalled("prorroga");
        } catch (Exception e) {
            prorroga = "no";
            System.out.println(e);
        }
        try {
            if (!prorroga.equals("no")) {
                //CREO EXCEL
                ExcelFileService.addTramitesProrroga(numeroTramite,prorroga);
                //Agrego a CSV
                WriteToFile.writeLinesToFileProrroga(numeroTramite + "," + prorroga);

            }
        } catch (Exception e) {
            Serenity.setSessionVariable("prorroga").to("no");
        }

        try {

            fecharecepcion = Serenity.sessionVariableCalled("fecharecepcion");
            estadoenvio = Serenity.sessionVariableCalled("estadoenvio");
            comentariocarta = Serenity.sessionVariableCalled("comentariocarta");

            if(fecharecepcion.equals("actual"))
            {


                Date date = new Date();
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                String valueDate = dateFormat.format(date);

                fecharecepcion =valueDate ;
            }

            if (!fecharecepcion.equals("no") && !estadoenvio.equals("no") && !comentariocarta.equals("no") ) {
                //CREO EXCEL
                ExcelFileService.addTramitesEnvioCartas(numeroTramite,fecharecepcion,estadoenvio,comentariocarta);
                //Agrego a CSV
                WriteToFile.writeLinesToFileProrroga(numeroTramite + "," + prorroga);

            }
        } catch (Exception e) {
            Serenity.setSessionVariable("fecharecepcion").to("no");
            Serenity.setSessionVariable("estadoenvio").to("no");
            Serenity.setSessionVariable("comentariocarta").to("no");
        }



    }

    @And("selecciono lo sucedido {string} con la tarjeta, si se ha bloqueado {string}")
    public void validaSituacionFraudeTarjetas(String getMotivoSucedidoTarjeta, String flagTarjetaBloqueo) {
       /* try {

            theActorInTheSpotlight().attemptsTo(
                    SeleccionarSucedidoFlagBloqueoTarjeta.withData(getMotivoSucedidoTarjeta, flagTarjetaBloqueo)
            );

        } catch (Exception e) {
            NotificacionSlack.enviarmensajeSlack(e);
            throw new AssertionError(e.getMessage());
        }*/
    }

    @And("^se inicia con el proceso de registro de tramite$")
    public void seleccionaOpcionProductosRegistroTramite() {

        theActorInTheSpotlight().attemptsTo(
                new SeleccionarProducto()
        );

    }

    @And("^se elige el tipo de tramite (RECLAMO|PEDIDO|SOLICITUD) y la tipologia (NUEVA TASA|EMISIÓN PROACTIVA DE LEVANTAMIENTO DE GARANTÍA|LEVANTAMIENTO DE GARANTÍA|LIBERACIÓN DE RETENCIONES|CLIENTE VIAJERO|REPROGRAMACIÓN DE DEUDA SIN CAMPAÑA|REPROGRAMACIÓN DE DEUDA CON CAMPAÑA|TRASLADO DE CTS FÍSICO A INTERBANK|TRASLADO DE CTS DIGITAL A INTERBANK|REPOSICIÓN DE TARJETA DE DEBITO|CAMBIO DE TARJETA|ABONO DE MILLAS|INFORMACIÓN DE ESTABLECIMIENTO/ RET.EFECTIVO|ACTIVACIÓN DE ORDENES DE PAGO|TRÁMITES DOCUMENTARIOS POST VENTA|MEJORA DE CONDICIONES CHIP- RET|TRASLADOS DE CTS A OTRO BANCO|INCUMPLIMIENTO DE CAMPAÑA|DEVOLUCIÓN CUOTAS EN TRÁNSITO|INSATISFACCIÓN GESTIÓN DE ENTREGA DE TARJETA|CORREO/TELÉFONO INDEBIDO DE CAMPAÑAS|DENUNCIAS LEGAL|DENUNCIAS|LIBERACIÓN CARTA FIANZA Y CERTIFICADO BANCARIO|RECTIFICACIÓN SBS|DEVOLUCION DE DOCUMENTOS|INFORMACIÓN DE MOVIMIENTOS|COPIA DE BOLETA DE DEPOSITO LIMA|REVISIÓN DE DEPÓSITOS A TERCEROS|ATM DE OTRO BANCO NO ENTREGÓ DINERO|MODIFICACIÓN DE TASA COMPRA DE DEUDA TC|COMPRA DE DEUDA SIN AMPLIACIÓN|REGULARIZACIÓN DE COMPRA DE DEUDA TC|REVERSIÓN DE UPGRADE|MODIFICACIÓN DISPOSICIÓN DE EFECTIVO|SOLICITUD DE INFORMACIÓN DE SINIESTROS|LEVANTAMIENTO DE ENDOSO|CAMBIO DE SEGURO EXTERNO A INTERNO|ATENCIONES BOGOTÁ|SOLICITUD DE OP. DE PERSONAS SIN DISCERNIMIENTO|CORRECCIÓN DE PAGO REALIZADO|APLICACIÓN DE FONDOS POR SINIESTROS/DESGRAVAMEN|SOLICITUD TRAMITACIÓN DE DOCUMENTOS|ENDOSO SEGUROS DEL BIEN|CUOTA FLEXIBLE|CAMBIO DE FORMA DE ENVÍO DE CORRESPONDENCIA|LIQUIDACIÓN DE DEUDA|SANEAMIENTO HIPOTECAS IBK|LEVANTAMIENTO DE HIPOTECA MATRIZ|ATENCIÓN IBK AGENTE|PAGO CTS PESCA|SOLICITUD DE ALCANCÍA|DEPÓSITO A PLAZO - JUBILACIÓN|ENDOSO SEGUROS DE DESGRAVAMEN|COPIA DE BOLETA DE DEPÓSITO LIMA|PROTECCIÓN DE DATOS - DERECHO ARCO|BILLETE FALSO|VIDEO ARCO|MODIFICACIÓN DATOS PERSONALES|SOLICITUD DE INTÉRPRETE PARA USUARIO|CONSUMO/ RETIRO NO EFECTUADO|MODIFICACIÓN DE CUENTA SUELDO ASOCIADA|EMISIÓN DE PRE CONFORMIDAD AFP 25%|COMPRA DE DEUDA INTERNA|REPROGRAMACIÓN DE DEUDA TC CON CAMPAÑA|REPROGRAMACIÓN DE DEUDA TC SIN CAMPAÑA|MODIFICACIÓN CONDICIONES CUOTAS CRÉDITO|NO RECEPCIÓN DE TRANSFERENCIA IN/OUT|CONSTANCIA POR TRANSFERENCIAS RECIBIDAS|CUOTA INICIAL HASTA 25% AFP|DEVOLUCIÓN DE DOCUMENTOS|ANULACIÓN DE CRÉDITO|MODIFICACIÓN CONFIGURACIÓN CRÉDITO|DUPLICADO DE CRONOGRAMA|MODIFICACIÓN DE CONDICIONES CONTRACTUALES|CONSTANCIA DE BLOQUEO DE TARJETA|SOLICITUD VALIDACIÓN DE PODERES LIMA|DESAFILIACIÓN DE SEGURO|CONSTANCIA DE PAGO REALIZADO|PASE A CUOTAS SEGURO EXTRACASH|SEGURO DESGRAVAMEN FALLECIMIENTO|DIRECCIÓN/TELF. INDEBIDOS DE COBRANZA|DESBLOQUEO PREVENTIVO/SOBREENDEUDAMIENTO|REPROGRAMACIÓN DE DEUDA|UNIFICACIÓN DE MARCAS/TIPOS DE TARJETAS|ATM DE IB NO ENTREGÓ DINERO|ATM DE IB NO DEPOSITÓ DINERO|DESAFILIACIÓN/ACTUALIZACIÓN DE NÚMERO CELULAR|DESACUERDO CON EL SERVICIO|SOLICITUD VALIDACION DE PODERES LIMA|SOLICITUD DE ANULACIÓN DE SEGURO|CONSTANCIA DE NO ADEUDO|LIBERACIÓN DE RETENCIONES JUDICIALES|NO RECEPCIÓN DE DOCUMENTO|EMISIÓN DE TARJETA|LIBERACION DE RETENCIONES|OPERACIÓN DENEGADA|LIBERACION DE RETENCIONES|REGRABACIÓN DE PLÁSTICO|AFILIACIÓN / DESAFILIACIÓN DÉBITO AUTOMÁTICO|TRASLADO DE SALDO ACREEDOR|REDUCCIÓN DE LÍNEA TC|MODIFICACIÓN LÍNEA TC ADICIONAL|MODIFICACIÓN DE GRUPO DE LIQUIDACIÓN|BAJA DE TASA|DUPLICADO DE VOUCHER|DUPLICADO DE DOCUMENTO|INFORMACION DE MOVIMIENTOS|ATENCIÓN POR PRENVENCIÓN Y FRAUDES|CANCELACIÓN DE CUENTA|DESACUERDO DE CONDICIONES/TASAS/TARIFAS|REPORTE INDEBIDO CENTRALES DE RIESGO|DUPLICADO DE ESTADO DE CUENTA/SERVICIO DE COPIAS|INCORRECTA APLICACIÓN CD/EC/C. CUOTAS|TRANSACCIÓN MAL O NO PROCESADA|INSUFICIENTE INFORMACIÓN|SISTEMA DE RECOMPENSAS|DUPLICADO DE ESTADO DE CUENTA/SERVICIO DE COPIAS|DEVOLUCION DE SALDO ACREEDOR|PAGO ANTICIPADO Y ADELANTO DE CUOTAS|COBROS INDEBIDOS|CONSUMOS NO RECONOCIDOS|CONSUMOS MAL PROCESADOS|EXONERACIÓN DE COBROS|CONSUMOS NO RECONOCIDOS POR FRAUDE|CONSUMOS MAL PROCESADOS POR EL COMERCIO/MARCA|CUENTA/CANCELACIÓN NO RECONOCIDA|TRANSACCIÓN MAL O NO PROCESADA|MODIFICACION PLAZO|PROGRAMA DE RECOMPENSAS|PAGO ANTICIPADO Y ADELANTO DE CUOTAS)$")
    public void eligeTipoTramiteAndTipologiaRetail(String tramite, String tipologia) {
        //if(!tramite.equals("RECLAMO"))
        //{
        //    throw new AssertionError("no es reclamo, se cancela ejecucion");
        //}
        //TODO: Selecciona - Tipo Tramite
        theActorInTheSpotlight().attemptsTo(
                SeleccionarTipoTramite.withData(tramite)
        );

        String ambiente = Serenity.sessionVariableCalled("ambiente");
        String tipoproducto = Serenity.sessionVariableCalled("tipoproducto").toString();

        System.out.println(ambiente + " " + tipoproducto + " " + tramite + " " + tipologia);
        //TODO: OBTIENE - VALOR ID - DE TIPOLOGIAS - DESDE TDM
        String idTipologia = "-";
        if (!Serenity.sessionVariableCalled("ambiente").equals("DEV")) {
            idTipologia = Do.getTipologiaId(ambiente, tipoproducto, tramite, tipologia).getIdTipologiaElement();
        }

        Serenity.setSessionVariable("tipotramite").to(tramite);
        Serenity.setSessionVariable("tipologia").to(tipologia);

        System.out.println("*************** TIPOLOGIA ID ES: " + idTipologia);

        //TODO: INGRESA SELECCIONAR TIPOLOGIA POR ID´S
        theActorInTheSpotlight().attemptsTo(
                SeleccionarTipologiaById.withData(idTipologia)
        );

        Serenity.setSessionVariable("idTipologia").to(idTipologia);

        //TODO: SECCIÓN AUTONOMIA
        String autonomia = "";
        try {
            autonomia = Serenity.sessionVariableCalled("autonomia").toString();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        if (autonomia.equals("si")) {
            try {
                Thread.sleep(1000);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            String eventId = String.valueOf(Integer.valueOf(Serenity.sessionVariableCalled("eventId").toString()) - 1);
            String type = idTipologia;
            consultaFilterInboxSteps.agregoLosEncabezadosEnLaPeticionClientePorTipologia(eventId, type);
            consultaFilterInboxSteps.validoLaRespuestaObtenida();
            consultaFilterInboxSteps.obtengoCantidadConsultingSupervisor();

            String montoConsultant = Serenity.sessionVariableCalled("montoConsultant").toString();
            String montoSupervisor = Serenity.sessionVariableCalled("montoSupervisor").toString();

            if ((montoConsultant.equals("0.0") && montoSupervisor.equals("0.0")) || (montoConsultant.equals("0") && montoSupervisor.equals("0"))) {
                System.out.println("tipologia actual no cuenta con autonomia, se debe usar otra tipologia");
                try {
                    Thread.sleep(1000);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
                throw new AssertionError("no aplica autonomia tipologia seleccionada o criterios no cumplen, prueba con otra tarjeta o cliente");
            } else {
                System.out.println("tipologia con Autonomia, se usará este monto maximo");

                if (!montoConsultant.equals("0.0")) {
                    if (!montoConsultant.equals("0")) {
                        if (montoConsultant.contains(".")) {

                            String montoConsultantFinal = montoConsultant.split("\\.")[0];
                            Serenity.setSessionVariable("montoAutonomia").to(montoConsultantFinal);

                        } else {
                            Serenity.setSessionVariable("montoAutonomia").to(montoConsultant);

                        }
                    }
                }

            }


        }

        try {
            EscenarioValidacion escenarioValidacion = Do.getEscenarioValidacion(tipoproducto, tramite, tipologia, "");

            Serenity.setSessionVariable("areaInicial").to(escenarioValidacion.getAreaInicial().toUpperCase());

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    @And("^se elige el tipo tramite comercial (RECLAMO|PEDIDO|SOLICITUD) y la tipologia (DOCUMENTOS Y CONSTANCIAS|DESACUERDO CON LA ATENCIÓN|DOCUMENTOS Y CONSTANCIAS|COBROS INDEBIDOS|CANCELACIÓN DE CUENTA|DUPLICADO DE ESTADO DE CUENTA/SERVICIO DE COPIAS|NO RECEPCIÓN DE DOCUMENTO|EXONERACIÓN DE COBROS|CONSUMOS NO RECONOCIDOS POR FRAUDE|CONSUMOS MAL PROCESADOS POR EL COMERCIO/MARCA|DESAFILIACIÓN DE SEGURO|DUPLICADO DE DOCUMENTO|CONSUMO/ RETIRO NO EFECTUADO)$")
    public void eligeTipoTramiteAndTipologiaComercial(String tramite, String tipologia) {

        //TODO: Selecciona - Tipo Tramite
        theActorInTheSpotlight().attemptsTo(
                SeleccionarTipoTramite.withData(tramite)
        );
        String ambiente = Serenity.sessionVariableCalled("ambiente");
        String tipoproducto = Serenity.sessionVariableCalled("tipoproducto").toString();

        System.out.println(ambiente + " " + tipoproducto + " " + tramite + " " + tipologia);
        //TODO: OBTIENE - VALOR ID - DE TIPOLOGIAS - DESDE TDM
        String idTipologia = "-"; //COMENTAR SGTE IF PARA MODO AUTO
        if (!Serenity.sessionVariableCalled("ambiente").equals("DEV")) {
            idTipologia = Do.getTipologiaIdComercial(ambiente, tipoproducto, tramite, tipologia).getIdTipologiaElement();
        }

        Serenity.setSessionVariable("tipotramite").to(tramite);
        Serenity.setSessionVariable("tipologia").to(tipologia);

        System.out.println("*************** TIPOLOGIA ID ES: " + idTipologia);


        //TODO: INGRESA SELECCIONAR TIPOLOGIA POR ID´S
        theActorInTheSpotlight().attemptsTo(
                SeleccionarTipologiaById.withData(idTipologia)
        );

        Serenity.setSessionVariable("idTipologia").to(idTipologia);

        try {
            EscenarioValidacion escenarioValidacion = Do.getEscenarioValidacion(tipoproducto, tramite, tipologia, "");

            Serenity.setSessionVariable("areaInicial").to(escenarioValidacion.getAreaInicial().toUpperCase());

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }


    @And("^se obtiene informacion de cliente a traves de TDM y el producto (TARJETA DE CREDITO|TARJETA DE DEBITO|CUENTA) validar autonomia y estado (.*)$")
    public void seObtieneProductoRetailConEstadoAutonomia(String producto, String estado) {

        Serenity.setSessionVariable("autonomia").to("si");

        try {

            ClienteTarjeta clientes = Do.getClienteTarjetaDebitoPorFiltroTdm(producto, estado);
            Serenity.setSessionVariable("tipotarjeta").to("NA");
            Serenity.setSessionVariable("marcatarjeta").to("NA");
            Serenity.setSessionVariable("tipoproducto").to(producto);
            Serenity.setSessionVariable("estadotarjeta").to("NA");
            Serenity.setSessionVariable("estadocuenta").to(estado);
            Serenity.setSessionVariable("nrocuenta").to(clientes.getNumerocuenta());

            System.out.println("CUENTA " + clientes.getNumerocuenta());

            Serenity.setSessionVariable("nrotarjeta").to(clientes.getNumerotarjeta());
            Serenity.setSessionVariable("getTipoDocumento").to(clientes.getTipodocumento());
            Serenity.setSessionVariable("getNumeroDocumento").to(clientes.getNumerodocumento());
            Serenity.setSessionVariable("TipoCMPMalConcretado").to("");

            String getProfile = ApiCommons.PERFIL_USUARIO;

            if (!Objects.equals(getProfile, "ASESOR")) {

                try {
                    Thread.sleep(5000);

                    theActorInTheSpotlight().attemptsTo(
                            SeleccionarMenuPrincipal.withData("AtencionCliente")
                    );
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }

                //TODO: CARGAR O ACTUALIZAR EN FRONT EL FORMULARIO CUANDO NO APARECE EL FORMULARIO DE ATENCION
                Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(1));
                boolean btnValidar = Serenity.getDriver().findElements(By.id("search-form__btn-validate")).size() > 0;
                if (btnValidar) {
                    System.err.println("Cargo el formulario de atención al cliente");
                } else {
                    System.err.println("Procede a cargar la pagina");
                    Serenity.getDriver().navigate().refresh();

                    try {
                        Thread.sleep(10000);
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }

                    theActorInTheSpotlight().attemptsTo(
                            SeleccionarMenuPrincipal.withData("AtencionCliente")
                    );

                }

            }

            theActorInTheSpotlight().attemptsTo(
                    IdentificarCliente.withData(clientes.getTipodocumento(), clientes.getNumerodocumento())
            );


        } catch (Exception e) {
            NotificacionSlack.enviarmensajeSlack(e);
            throw new AssertionError(e.getMessage());
        }
    }


    @And("^se obtiene informacion de cliente a traves de TDM con producto (.*)$")
    public void seObtieneProductoRetailConDocumento(String producto) {

        String tipoDocumento =ApiCommons.documentType;
        System.out.println(" ******* TIPO DE DOCUMENTO ES: " + tipoDocumento);

        Serenity.setSessionVariable("autonomia").to("no");
        if(producto.contains(" y estado"))
        {
            producto = producto.split(" y estado")[0];
            System.out.println("producto: " + producto);
        }
        try {

            ApiCommons.clientes = Do.getClienteDocumentoProductoTdm(producto, tipoDocumento);
              Serenity.setSessionVariable("tipoproducto").to(producto);
              Serenity.setSessionVariable("TipoCMPMalConcretado").to("");

            String getProfile = ApiCommons.PERFIL_USUARIO;

            if (!Objects.equals(getProfile, "ASESOR")) {

                try {
                    Thread.sleep(5000);

                    theActorInTheSpotlight().attemptsTo(
                            SeleccionarMenuPrincipal.withData("AtencionCliente")
                    );
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }

                //TODO: CARGAR O ACTUALIZAR EN FRONT EL FORMULARIO CUANDO NO APARECE EL FORMULARIO DE ATENCION
                Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(1));
                boolean btnValidar = Serenity.getDriver().findElements(By.id("search-form__btn-validate")).size() > 0;
                if (btnValidar) {
                    System.err.println("Cargo el formulario de atención al cliente");
                } else {
                    System.err.println("Procede a cargar la pagina");
                    Serenity.getDriver().navigate().refresh();

                    try {
                        Thread.sleep(10000);
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }

                    theActorInTheSpotlight().attemptsTo(
                            SeleccionarMenuPrincipal.withData("AtencionCliente")
                    );

                }

            }

            theActorInTheSpotlight().attemptsTo(
                    IdentificarCliente.withData( ApiCommons.clientes.getTipodocumento(),  ApiCommons.clientes.getNumerodocumento())
            );

        } catch (Exception e) {
            NotificacionSlack.enviarmensajeSlack(e);
            throw new AssertionError(e.getMessage());
        }
    }


    @And("^se obtiene informacion de cliente a traves de TDM con el producto (.*) y estado (.*)$")
    public void seObtieneProductoRetailConDocumento(String producto, String estado) {

        String tipoDocumento =ApiCommons.documentType;
        System.out.println(" ******* TIPO DE DOCUMENTO ES: " + tipoDocumento);

        Serenity.setSessionVariable("autonomia").to("no");
        try {

            ApiCommons.clientes = Do.getClienteDocumentoProductoEstadoTdm(producto, tipoDocumento, estado);
            Serenity.setSessionVariable("tipoproducto").to(producto);
            Serenity.setSessionVariable("TipoCMPMalConcretado").to("");

            String getProfile = ApiCommons.PERFIL_USUARIO;

            if (!Objects.equals(getProfile, "ASESOR")) {

                try {
                    Thread.sleep(5000);

                    theActorInTheSpotlight().attemptsTo(
                            SeleccionarMenuPrincipal.withData("AtencionCliente")
                    );
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }

                //TODO: CARGAR O ACTUALIZAR EN FRONT EL FORMULARIO CUANDO NO APARECE EL FORMULARIO DE ATENCION
                Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(1));
                boolean btnValidar = Serenity.getDriver().findElements(By.id("search-form__btn-validate")).size() > 0;
                if (btnValidar) {
                    System.err.println("Cargo el formulario de atención al cliente");
                } else {
                    System.err.println("Procede a cargar la pagina");
                    Serenity.getDriver().navigate().refresh();

                    try {
                        Thread.sleep(10000);
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }

                    theActorInTheSpotlight().attemptsTo(
                            SeleccionarMenuPrincipal.withData("AtencionCliente")
                    );

                }

            }

            theActorInTheSpotlight().attemptsTo(
                    IdentificarCliente.withData( ApiCommons.clientes.getTipodocumento(),  ApiCommons.clientes.getNumerodocumento())
            );

        } catch (Exception e) {
            NotificacionSlack.enviarmensajeSlack(e);
            throw new AssertionError(e.getMessage());
        }
    }

//    @And("^se obtiene informacion de cliente a traves de TDM con producto (RAPPIBANK - TARJETA DE CRÉDITO|RAPPIBANK - CUENTAS|CARTA FIANZA/CERTIFICADO|SEGUROS RELACIONADOS|CUENTA CORRIENTE|CBME|SEGUROS|TARJETA DE CREDITO|TARJETA DE DEBITO|CUENTA) y estado (.*)$")
//    public void seObtieneProductoRetailConEstado(String producto, String estado) {
//
//        Serenity.setSessionVariable("autonomia").to("no");
//
//        try {
//
//            ClienteTarjeta clientes = Do.getClienteTarjetaDebitoPorFiltroTdm(producto, estado);
//            Serenity.setSessionVariable("tipotarjeta").to("NA");
//            Serenity.setSessionVariable("marcatarjeta").to("NA");
//            Serenity.setSessionVariable("tipoproducto").to(producto);
//            Serenity.setSessionVariable("estadotarjeta").to("NA");
//            Serenity.setSessionVariable("estadocuenta").to(estado);
//            Serenity.setSessionVariable("nrocuenta").to(clientes.getNumerocuenta());
//
//            System.out.println("CUENTA " + clientes.getNumerocuenta());
//
//            Serenity.setSessionVariable("nrotarjeta").to(clientes.getNumerotarjeta());
//            Serenity.setSessionVariable("getTipoDocumento").to(clientes.getTipodocumento());
//            Serenity.setSessionVariable("getNumeroDocumento").to(clientes.getNumerodocumento());
//            Serenity.setSessionVariable("TipoCMPMalConcretado").to("");
//
//            String getProfile = Serenity.sessionVariableCalled("getProfile").toString();
//
//            if (!Objects.equals(getProfile, "ASESOR")) {
//
//                try {
//                    Thread.sleep(5000);
//
//                    theActorInTheSpotlight().attemptsTo(
//                            SeleccionarMenuPrincipal.withData("AtencionCliente")
//                    );
//                } catch (Exception e) {
//                    System.out.println(e.getMessage());
//                }
//
//                //TODO: CARGAR O ACTUALIZAR EN FRONT EL FORMULARIO CUANDO NO APARECE EL FORMULARIO DE ATENCION
//                Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(1));
//                boolean btnValidar = Serenity.getDriver().findElements(By.id("search-form__btn-validate")).size() > 0;
//                if (btnValidar) {
//                    System.err.println("Cargo el formulario de atención al cliente");
//                } else {
//                    System.err.println("Procede a cargar la pagina");
//                    Serenity.getDriver().navigate().refresh();
//
//                    try {
//                        Thread.sleep(10000);
//                    } catch (Exception e) {
//                        System.out.println(e.getMessage());
//                    }
//
//                    theActorInTheSpotlight().attemptsTo(
//                            SeleccionarMenuPrincipal.withData("AtencionCliente")
//                    );
//
//                }
//
//            }
//
//            theActorInTheSpotlight().attemptsTo(
//                    IdentificarCliente.withData(clientes.getTipodocumento(), clientes.getNumerodocumento())
//            );
//
//        } catch (Exception e) {
//            NotificacionSlack.enviarmensajeSlack(e);
//            throw new AssertionError(e.getMessage());
//        }
//    }

//    @And("^se obtiene informacion de cliente a traves de TDM con producto (TRASLADOS CTS|SUNAT|SERVICIOS|MONEY EXCHANGE APP|RETIRO SIN TARJETA|PODERES|PAGOS PLANILLAS/PROVEEDORES|SANEAMIENTO PROY. INMB|CUENTA JUBILACIÓN|BANCA POR INTERNET|PLIN|IB AGENTE|PAY PAL|TRANSF. INTERNACIONALES|TUNKI|TOKEN|BILLETERA ELECTRÓNICA|ADELANTO SUELDO|LINEA CONVENIO|HIPOTECARIO|PRÉSTAMO PERSONAL|REFINANCIADO|EFECTIVO CON GARANTÍA HIPOTECARIO|VEHICULAR|ATENCION AL CLIENTE|COBRANZAS|REMESAS|PODER|APP INTERBANK|BANCA CELULAR|DEBITO AUTOMATICO|PAGO DE SERVICIO POR CANALES|TRANSF. NACIONALES)$")
//    public void seObtieneProductoOperacionesRetailSinEstado(String producto) {
//
//        Serenity.setSessionVariable("autonomia").to("no");
//
//        try {
//
//            ClienteTarjeta clientes = Do.getClienteTarjetaDebitoPorFiltroTdm(producto, "");
//            Serenity.setSessionVariable("tipoproducto").to(producto);
//            Serenity.setSessionVariable("nrocuenta").to(clientes.getNumerocuenta());
//            Serenity.setSessionVariable("nrotarjeta").to(clientes.getNumerotarjeta());
//            Serenity.setSessionVariable("getTipoDocumento").to(clientes.getTipodocumento());
//            Serenity.setSessionVariable("getNumeroDocumento").to(clientes.getNumerodocumento());
//
//            String getProfile = Serenity.sessionVariableCalled("getProfile").toString();
//
//            if (!Objects.equals(getProfile, "ASESOR")) {
//
//                try {
//                    Thread.sleep(5000);
//
//                    theActorInTheSpotlight().attemptsTo(
//                            SeleccionarMenuPrincipal.withData("AtencionCliente")
//                    );
//                } catch (Exception e) {
//                    System.out.println(e.getMessage());
//                }
//
//                //TODO: CARGAR O ACTUALIZAR EN FRONT EL FORMULARIO CUANDO NO APARECE EL FORMULARIO DE ATENCION
//                Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(1));
//                boolean btnValidar = Serenity.getDriver().findElements(By.id("search-form__btn-validate")).size() > 0;
//                if (btnValidar) {
//                    System.err.println("Cargo el formulario de atención al cliente");
//                } else {
//                    System.err.println("Procede a cargar la pagina");
//                    Serenity.getDriver().navigate().refresh();
//
//                    try {
//                        Thread.sleep(10000);
//                    } catch (Exception e) {
//                        System.out.println(e.getMessage());
//                    }
//
//                    theActorInTheSpotlight().attemptsTo(
//                            SeleccionarMenuPrincipal.withData("AtencionCliente")
//                    );
//
//                }
//
//            }
//
//            theActorInTheSpotlight().attemptsTo(
//                    IdentificarCliente.withData(clientes.getTipodocumento(), clientes.getNumerodocumento())
//            );
//        } catch (Exception e) {
//            NotificacionSlack.enviarmensajeSlack(e);
//            throw new AssertionError(e.getMessage());
//        }
//    }


    @And("^se obtiene informacion de comercio con producto (ATENCIÓN AL CLIENTE|CRÉDITO BPE|CUENTA NEGOCIOS|LETRAS Y CARTA FIANZA|CUENTA CORRIENTE|TARJETA DE DÉBITO|TARJETA DE CRÉDITO|SEGUROS RELACIONADOS|SEGUROS)")
    public void seObtieneProductoComercialSinEstado(String producto) throws InterruptedException {

        ApiCommons.clientes= Do.getClienteTarjetaDebitoPorFiltroTdm(producto, "");

        Serenity.setSessionVariable("tipoproducto").to(producto);
       String flagCliente = EnvironmentSpecificConfiguration.from(environmentVariables).getProperty("giru.flagCliente");

        System.out.println("Guardo datos en variables de sesion");

        String getProfile = ApiCommons.PERFIL_USUARIO;

        if (!Objects.equals(getProfile, "ASESOR")) {

            try {
                Thread.sleep(5000);

                theActorInTheSpotlight().attemptsTo(
                        SeleccionarMenuPrincipal.withData("AtencionCliente")
                );
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

            //TODO: CARGAR O ACTUALIZAR EN FRONT EL FORMULARIO CUANDO NO APARECE EL FORMULARIO DE ATENCION
            Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(1));
            boolean btnValidar = Serenity.getDriver().findElements(By.id("search-form__btn-validate")).size() > 0;
            if (btnValidar) {
                System.err.println("Cargo el formulario de atención al cliente");
            } else {
                System.err.println("Procede a cargar la pagina");
                Serenity.getDriver().navigate().refresh();

                try {
                    Thread.sleep(10000);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }

                theActorInTheSpotlight().attemptsTo(
                        SeleccionarMenuPrincipal.withData("AtencionCliente")
                );

            }

        }

        try {
            esComercial = Serenity.sessionVariableCalled("COMERCIAL").toString();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        if(esComercial.equals("COMERCIAL")){
            if(flagCliente.equals("1")) {
                theActorInTheSpotlight().attemptsTo(
                        IdentificarCliente.withData(ApiCommons.clientes.getTipodocumento(), ApiCommons.clientes.getNumerodocumento())
                );
            }else{
                Serenity.setSessionVariable("getTipoDocumento").to("RUC");
                Serenity.setSessionVariable("getNumeroDocumento").to("10467285169");

                theActorInTheSpotlight().attemptsTo(
                        IdentificarCliente.withData("RUC", "10467285169")
                );
                Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
                boolean btnNuevoCliente = Serenity.getDriver().findElements(By.id("confirmation__btn-confirm")).size() > 0;
                if (btnNuevoCliente) {
                    System.err.println("Cargo modal No Cliente");
                    Serenity.getDriver().findElement(By.id("confirmation__btn-confirm")).click();
                }else{
                    Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
                    Serenity.getDriver().findElement(By.id("confirmation__btn-confirm")).click();
                }


                //TODO: Ingresar datos de nuevo comercio
                System.err.println("Ingresa datos de no cliente");
                theActorInTheSpotlight().attemptsTo(
                        new IngresaDatosComercio("TEST COMERCIO")
                );


            }
        }else{
            theActorInTheSpotlight().attemptsTo(
                    IdentificarCliente.withData(ApiCommons.clientes.getTipodocumento(), ApiCommons.clientes.getNumerodocumento())
            );

            theActorInTheSpotlight().attemptsTo(
                    new IngresaSegmentoComercial()
            );

        }



    }

    @And("^se selecciona uno o varios MOTIVOS y se agregan movimientos$")
    public void eligeLosDatosDeRegistroDeLosMOTIVOS(DataTable dataTable) {

        List<Map<String, String>> list = dataTable.asMaps(String.class, String.class);

        List<List<String>> rows = dataTable.asLists(String.class);

        int resultado = 0;
        for (int i = 0; i < rows.size(); i++) {
            List<String> columns = rows.get(i);

            resultado = i - 1;

        }

        try {
            Serenity.setSessionVariable("flagTarjetaBloqueo").to(list.get(resultado).get("flagTarjetaBloqueo"));
            Serenity.setSessionVariable("getMotivoSucedidoTarjeta").to(list.get(resultado).get("getMotivoSucedidoTarjeta"));
            Serenity.setSessionVariable("formaDePago").to(list.get(resultado).get("formaDePago"));
            Serenity.setSessionVariable("CodigoMarca").to(list.get(resultado).get("CodigoMarca"));
            Serenity.setSessionVariable("IndicadorTipo").to(list.get(resultado).get("IndicadorTipo"));
            Serenity.setSessionVariable("NombreMotivo").to(list.get(resultado).get("NombreMotivo"));
            Serenity.setSessionVariable("ImporteSolicitado").to(list.get(resultado).get("ImporteSolicitado"));
            Serenity.setSessionVariable("MonedaTramite").to(list.get(resultado).get("MonedaTramite"));
            Serenity.setSessionVariable("LineaActual").to(list.get(resultado).get("LineaActual"));
            Serenity.setSessionVariable("LineaNueva").to(list.get(resultado).get("LineaNueva"));
            Serenity.setSessionVariable("GrupoLiquidacionActual").to(list.get(resultado).get("GrupoLiquidacionActual"));
            Serenity.setSessionVariable("GrupoLiquidacionSolicitado").to(list.get(resultado).get("GrupoLiquidacionSolicitado"));
            Serenity.setSessionVariable("tipoDocumentoTramite").to(list.get(resultado).get("tipoDocumentoTramite"));
            Serenity.setSessionVariable("productoServicioAdquirido").to(list.get(resultado).get("productoServicioAdquirido"));
            Serenity.setSessionVariable("tipoServicio").to(list.get(resultado).get("tipoServicio"));
            Serenity.setSessionVariable("importe").to(list.get(resultado).get("importe"));
            Serenity.setSessionVariable("cantidad").to(list.get(resultado).get("cantidad"));
            Serenity.setSessionVariable("aplicaAbono").to(list.get(resultado).get("aplicaAbono"));
            Serenity.setSessionVariable("origenSaldo").to(list.get(resultado).get("origenSaldo"));
            Serenity.setSessionVariable("Autonomia").to(list.get(resultado).get("Autonomia"));
            Serenity.setSessionVariable("Tienda").to(list.get(resultado).get("Tienda"));
            Serenity.setSessionVariable("canal").to(list.get(resultado).get("canal"));
            Serenity.setSessionVariable("operacionReconocida").to(list.get(resultado).get("operacionReconocida"));
            Serenity.setSessionVariable("medioComunicacion").to(list.get(resultado).get("medioComunicacion"));
            Serenity.setSessionVariable("flagTarjetaBloqueo").to(list.get(resultado).get("flagTarjetaBloqueo"));
            Serenity.setSessionVariable("tipoPrograma").to(list.get(resultado).get("tipoPrograma"));
            Serenity.setSessionVariable("tipoMoneda").to(list.get(resultado).get("moneda"));
            Serenity.setSessionVariable("nuevoMotivo").to(list.get(resultado).get("nuevoMotivo"));
            Serenity.setSessionVariable("aplicaAutonomia").to(list.get(resultado).get("aplicaAutonomia"));
            Serenity.setSessionVariable("tipoEnvio").to(list.get(resultado).get("tipoEnvio"));
            Serenity.setSessionVariable("importeSolicitado").to(list.get(resultado).get("importeSolicitado"));
            Serenity.setSessionVariable("importeReclamado").to(list.get(resultado).get("importeReclamado"));
            Serenity.setSessionVariable("medioAbono").to(list.get(resultado).get("medioAbono"));
            Serenity.setSessionVariable("reducirLineaCredito").to(list.get(resultado).get("reducirLineaCredito"));
            Serenity.setSessionVariable("tipoProducto").to(list.get(resultado).get("tipoProducto"));
            Serenity.setSessionVariable("tipoTransferencia").to(list.get(resultado).get("tipoTransferencia"));
            Serenity.setSessionVariable("tipoRemesa").to(list.get(resultado).get("tipoRemesa"));
            Serenity.setSessionVariable("tipoCuota").to(list.get(resultado).get("tipoCuota"));
            Serenity.setSessionVariable("carta").to(list.get(resultado).get("carta"));
            Serenity.setSessionVariable("compraDeuda").to(list.get(resultado).get("compraDeuda"));
            Serenity.setSessionVariable("quePasoConLaTarjeta").to(list.get(resultado).get("quePasoConLaTarjeta"));
            Serenity.setSessionVariable("abono").to(list.get(resultado).get("abono"));
            Serenity.setSessionVariable("beneficioReclamado").to(list.get(resultado).get("beneficioReclamado"));
            Serenity.setSessionVariable("tipoFiltroProducto").to(list.get(resultado).get("tipoFiltroProducto"));
            Serenity.setSessionVariable("medioPago").to(list.get(resultado).get("medioPago"));
            Serenity.setSessionVariable("tipoOperacion").to(list.get(resultado).get("tipoOperacion"));
            Serenity.setSessionVariable("lugarEntrega").to(list.get(resultado).get("lugarEntrega"));
            Serenity.setSessionVariable("cuentaSueldo").to(list.get(resultado).get("cuentaSueldo"));
            Serenity.setSessionVariable("salaryAccountCreation").to(list.get(resultado).get("salaryAccountCreation"));
            Serenity.setSessionVariable("medioDevolucion").to(list.get(resultado).get("medioDevolucion"));
            Serenity.setSessionVariable("agregarTarjetaCredito").to(list.get(resultado).get("agregarTarjetaCredito"));
            Serenity.setSessionVariable("tipoCuenta").to(list.get(resultado).get("tipoCuenta"));


            //Agregado para Comercial
            Serenity.setSessionVariable("numCuenta").to(list.get(resultado).get("numCuenta"));
            Serenity.setSessionVariable("numTarjeta").to(list.get(resultado).get("numTarjeta"));
            Serenity.setSessionVariable("periodoIni").to(list.get(resultado).get("periodoIni"));
            Serenity.setSessionVariable("periodoFin").to(list.get(resultado).get("periodoFin"));
            Serenity.setSessionVariable("recojoTienda").to(list.get(resultado).get("recojoTienda"));
            Serenity.setSessionVariable("numCredito").to(list.get(resultado).get("numCredito"));
            Serenity.setSessionVariable("numPoliza").to(list.get(resultado).get("numPoliza"));
            Serenity.setSessionVariable("nomSeguro").to(list.get(resultado).get("nomSeguro"));


            Serenity.setSessionVariable("cuentaMancomunada").to(list.get(resultado).get("cuentaMancomunada"));
            Serenity.setSessionVariable("razonNoBloqueo").to(list.get(resultado).get("razonNoBloqueo"));


            Serenity.setSessionVariable("esperabaRecibirFecha").to(list.get(resultado).get("esperabaRecibirFecha"));

            //Agregado para comercil para el bloque de Rezagadas
            Serenity.setSessionVariable("tipoOperacion").to(list.get(resultado).get("tipoOperacion"));
            Serenity.setSessionVariable("tipoOperacion").to(list.get(resultado).get("tipoOperacion"));

            Serenity.setSessionVariable("prorroga").to(list.get(resultado).get("prorroga"));

            Serenity.setSessionVariable("fecharecepcion").to(list.get(resultado).get("fecharecepcion"));
            Serenity.setSessionVariable("estadoenvio").to(list.get(resultado).get("estadoenvio"));
            Serenity.setSessionVariable("comentariocarta").to(list.get(resultado).get("comentariocarta"));

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }


        String motivo = list.get(resultado).get("motivos");
        String tipomoneda = list.get(resultado).get("moneda");
        String monto = list.get(resultado).get("monto");
        String cantidadmontomotivo = list.get(resultado).get("CantMovimientosPorMotivo");

        Serenity.setSessionVariable("CantMovimientosPorMotivo").to(cantidadmontomotivo);
        Serenity.setSessionVariable("motivos").to(motivo);
        Serenity.setSessionVariable("monto").to(monto);

        theActorInTheSpotlight().attemptsTo(
                RegistraMotivoMovimiento.withData(motivo, tipomoneda, monto, cantidadmontomotivo)
        );

    }

    @And("se ingresa una respuesta al cliente {string} y un comentario {string} opcional")
    public void ingresaRsptaYcometarioOpcional(String respuestaCliente, String comentarioAdicional) {
        try {
            theActorInTheSpotlight().attemptsTo(
                    RegistraComentarioRespuesta.withData(respuestaCliente, comentarioAdicional)
            );

            Serenity.setSessionVariable("respuestaCliente").to(respuestaCliente);
            Serenity.setSessionVariable("comentarioAdicional").to(comentarioAdicional);
        } catch (Exception e) {
            NotificacionSlack.enviarmensajeSlack(e);
            throw new AssertionError(e.getMessage());
        }
    }

    @And("^se selecciona o añade un numero de celular (.*) y un operador (MOVISTAR|CLARO|ENTEL|BITEL)$")
    public void seleccionaCelularOperador(String numeroCelular, String operador) {
        try {

            theActorInTheSpotlight().attemptsTo(
                    RegistraCelularOperador.withData(numeroCelular, operador)
            );

            Serenity.setSessionVariable("numcelular").to(numeroCelular);
            Serenity.setSessionVariable("operador").to(operador);


        } catch (Exception e) {
            NotificacionSlack.enviarmensajeSlack(e);
            throw new AssertionError(e.getMessage());
        }

    }

    @And("se adjunta opcionalmente un archivo {string}")
    public void seAdjuntaraUnArchivoOpcional(String enviarArchivo) {
        try {
            Serenity.setSessionVariable("subirarchivo").to(enviarArchivo);
            ApiCommons.SUBIARCHIVO = enviarArchivo;
            theActorInTheSpotlight().attemptsTo(
                    SubirArchivo.withData(enviarArchivo)
            );


        } catch (Exception e) {
            NotificacionSlack.enviarmensajeSlack(e);
            throw new AssertionError(e.getMessage());
        }
    }

    @And("se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite")
    public void seVerificaQueElNuevoUsuarioAsignadoPuedaVisualizarYAtenderElTramite() {
       try {

            String getProfile = ApiCommons.PERFIL_USUARIO;

            if (!getProfile.equals("ASESOR")) {
                String numeroTramite = Serenity.sessionVariableCalled("numeroTramite").toString();
                //String nroAsesorReasignado = "";
                //boolean seReasigna = false;

                validacionRegistroPage.consultarServicioDeAreaAsignada();

                String usuerReasigned = Serenity.sessionVariableCalled("nroAsesorReasignado").toString().toUpperCase();
                String newUserReasigned = ApiCommons.USUARIO.toUpperCase();

                //SOLO CIERRA SESIÓN CUANDO SE LE ASIGNA EL TRÁMITE A UN NUEVO USER
                String finalizarGeneracionTramite = Serenity.sessionVariableCalled("finalizarGeneracionTramite").toString();

                if (!usuerReasigned.equals(newUserReasigned) && !finalizarGeneracionTramite.equals("Si")) { //REASIGNA NUEVA AREA
                    validacionRegistroPage.cierroSesionEIngresoNuevoUser();

                    //INGRESO A BANDEJA TRAMITES
                    theActorInTheSpotlight().attemptsTo(
                            SeleccionarMenuPrincipal.withData("Tramites")
                    );

                    theActorInTheSpotlight().attemptsTo(
                            BuscarTramiteBandejaRegistro.withData(numeroTramite)
                    );

                }
            }


        } catch (Exception e) {
            NotificacionSlack.enviarmensajeSlack(e);
            throw new AssertionError(e.getMessage());
        }
    }


}
