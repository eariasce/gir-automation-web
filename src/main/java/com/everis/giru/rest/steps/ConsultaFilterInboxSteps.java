package com.everis.giru.rest.steps;

import com.everis.giru.utils.ApiCommons;
import com.everis.giru.utils.WebServiceEndPoints;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import net.serenitybdd.annotations.Step;
import net.serenitybdd.core.Serenity;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class ConsultaFilterInboxSteps {

    private static final String URL_BASE_HISTORICAL_INBOX_DEV = "http://20.75.124.117/inbox";
    private static final String URL_BASE_HISTORICAL_INBOX_STG = "https://eu2-ibk-apm-stg-ext-001.azure-api.net/giru-stg-inbox/stg";
    private static final String URL_BASE_HISTORICAL_INBOX_UAT = "https://eu2-ibk-apm-uat-ext-001.azure-api.net/giru-inbox/uat";

    public static final String NPS_SECURITY_SUBSCRIPTION_KEY_DEV = "46af994ace274a989a422449c024b026";
    public static final String NPS_SECURITY_SUBSCRIPTION_KEY_STG = "c99a61f28e87494c91b1f49d39854d0d";
    public static final String NPS_SECURITY_SUBSCRIPTION_KEY_UAT = "f954d6fcccd34705a727a228c6bc772e";
    private static final String TEMPLATE_GET_HISTORICAL_INBOX = "/templates/getHistoricalInbox-input.json";
    private static final String TEMPLATE_GET_CLIENT = "/templates/getClientInfoInput.json";


    public String bodyRequest;
    public Response response;

    public void seArmaElCuerpoDePeticionClienteActual(String numeroDni) {

        bodyRequest = ApiCommons.getTemplate(TEMPLATE_GET_CLIENT)
                .replace("{numeroDni}", numeroDni);

    }

    public void seArmaElCuerpoDePeticionCreacionCliente(String numeroTramite) {

        bodyRequest = ApiCommons.getTemplate(TEMPLATE_GET_HISTORICAL_INBOX)
                .replace("{numeroTramite}", numeroTramite);

    }
    @Step("construyo request y hago POST 2")
    public int agregoLosEncabezadosEnLaPeticion2() {

        String ambiente = Serenity.sessionVariableCalled("ambiente").toString();

        if (ambiente.equals("UAT")) {
            RestAssured.baseURI = URL_BASE_HISTORICAL_INBOX_UAT;

            response = given()
                    .log().all()
                    .contentType(ContentType.JSON)
                    .relaxedHTTPSValidation()
                    .header("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJiMTM0NTAiLCJzY29wZSI6WyJhbGwiXSwiZXhwIjoxNjg5NDA2NTcyLCJhdXRob3JpdGllcyI6WyJBTEwiXSwianRpIjoiYzU4NDE4ZjgtZTE5Ny00YjA0LTgzNmItZjQ3ZjUyZDEyYTUwIiwidXNlcmluZm8iOnsidXNlcm5hbWUiOiJiMTM0NTAiLCJuYW1lIjoiQmF6YW4gU2Fyb21vLCBWaWNreSBLYXJlbiIsInRpdGxlIjoiQVNFU09SIERFIFNFUlZJQ0lPUyBFU1BFQ0lBTElaQURPUyIsImRlcGFydG1lbnQiOiJEUFRPLiBDT05UQUNUIENFTlRFUiIsImRpdmlzaW9uIjoiVlAuTkVHT0NJT1MgUkVUQUlMIFkgQ0FOQUxFUyIsImRlbGl2ZXJ5T2ZmaWNlTmFtZSI6IkNBTkFMRVMgRElHSVRBTEVTIEJUIDEiLCJtb2JpbGUiOm51bGwsImVtYWlsIjoiVkJhemFuQGludGVyY29ycC5jb20ucGUiLCJncm91cHMiOlsiR0lSVV9QUkRfQVNFU09SIiwiR0lSVV9VQVRfQVNFU09SIl0sInN1cGVydmlzb3IiOiJPbGl2YXJlcyBDaWZyZSwgSnVhbiBQYWJsbyIsInN1cGVydmlzb3JVc2VyTmFtZSI6IkIyNTQ2NyIsImFwcGxpY2F0aW9uT3JpZ2luIjoiZ2lydWZyb250IiwiYXJlYUNvZGUiOiIxMDA0NzMiLCJhcmVhRGVzY3JpcHRpb24iOiJDQU5BTEVTIERJR0lUQUxFUyBCVCAxIiwicHJvZmlsZUNvZGUiOiJBU1IwMDAxIiwicHJvZmlsZU5hbWUiOiJBU0VTT1IiLCJjdXJyZW50RGF0ZSI6MTY4OTM2MzM3MiwiYXV0aG9yaXRpZXMiOlt7ImF1dGhvcml0eSI6IkFMTCJ9XX0sImNsaWVudF9pZCI6bnVsbH0.SSoGPdUuGNmvjBPzsozraQ3vE22QTaaEXVbb6BRqsGE")
                    .header("Ocp-Apim-Subscription-Key", NPS_SECURITY_SUBSCRIPTION_KEY_UAT)
                    .queryParam("page", "0")
                    .queryParam("size", "4")
                    .queryParam("sortBy", "FLAG_SUPERVISOR")
                    .queryParam("direction", "DESCENDING")
                    .when().
                    body(bodyRequest)
                    .post(WebServiceEndPoints.API_HISTORICAL_INBOX.getUrl())
                    .then()
                    .log().all()
                    .extract().response();


            return response.statusCode();

        } else if (ambiente.equals("STG")) {
            RestAssured.baseURI = URL_BASE_HISTORICAL_INBOX_STG;

            response = given()
                    .log().all()
                    .contentType(ContentType.JSON)
                    .relaxedHTTPSValidation()
                    .header("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJiMTM0NTAiLCJzY29wZSI6WyJhbGwiXSwiZXhwIjoxNjg5MzUxODQwLCJhdXRob3JpdGllcyI6WyJBTEwiXSwianRpIjoiN2E0NTQ5ODQtN2EwNi00YmI0LWJhNWItZGY5YzRmNzE2YjI0IiwidXNlcmluZm8iOnsidXNlcm5hbWUiOiJiMTM0NTAiLCJuYW1lIjoiQmF6YW4gU2Fyb21vLCBWaWNreSBLYXJlbiIsInRpdGxlIjoiQVNFU09SIERFIFNFUlZJQ0lPUyBFU1BFQ0lBTElaQURPUyIsImRlcGFydG1lbnQiOiJEUFRPLiBDT05UQUNUIENFTlRFUiIsImRpdmlzaW9uIjoiVlAuTkVHT0NJT1MgUkVUQUlMIFkgQ0FOQUxFUyIsImRlbGl2ZXJ5T2ZmaWNlTmFtZSI6IkNBTkFMRVMgRElHSVRBTEVTIEJUIDEiLCJtb2JpbGUiOm51bGwsImVtYWlsIjoiVkJhemFuQGludGVyY29ycC5jb20ucGUiLCJncm91cHMiOlsiR0lSVV9QUkRfQVNFU09SIiwiR0lSVV9VQVRfQVNFU09SIl0sInN1cGVydmlzb3IiOiJPbGl2YXJlcyBDaWZyZSwgSnVhbiBQYWJsbyIsInN1cGVydmlzb3JVc2VyTmFtZSI6IkIyNTQ2NyIsImFwcGxpY2F0aW9uT3JpZ2luIjoiZ2lydWZyb250IiwiYXJlYUNvZGUiOiIxMDA0NTQiLCJhcmVhRGVzY3JpcHRpb24iOiJDQU5BTEVTIERJR0lUQUxFUyBCVCAxIiwicHJvZmlsZUNvZGUiOiJBU1IwMDAxIiwicHJvZmlsZU5hbWUiOiJBU0VTT1IiLCJjdXJyZW50RGF0ZSI6MTY4OTM0ODI0MCwiYXV0aG9yaXRpZXMiOlt7ImF1dGhvcml0eSI6IkFMTCJ9XX0sImNsaWVudF9pZCI6bnVsbH0.g55tKJLjhGdEWpqcI0Mnd4Gq9pjB5SR_I5vaDNu5Vck")
                    .header("Ocp-Apim-Subscription-Key", NPS_SECURITY_SUBSCRIPTION_KEY_STG)
                    .queryParam("page", "0")
                    .queryParam("size", "4")
                    .queryParam("sortBy", "FLAG_SUPERVISOR")
                    .queryParam("direction", "DESCENDING")
                    .when().
                    body(bodyRequest)
                    .post(WebServiceEndPoints.API_HISTORICAL_INBOX.getUrl())
                    .then()
                    .log().all()
                    .extract().response();


            return response.statusCode();

        } else if (ambiente.equals("DEV")) {
            RestAssured.baseURI = URL_BASE_HISTORICAL_INBOX_DEV;

            response = given()
                    .log().all()
                    .contentType(ContentType.JSON)
                    .relaxedHTTPSValidation()
                    .header("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJiMTAwNDkiLCJzY29wZSI6WyJhbGwiXSwiZXhwIjoxNjU2MTA4NTQzLCJhdXRob3JpdGllcyI6WyJBTEwiXSwianRpIjoiNGQ0MmYxZDUtYWU2Ni00YTliLTgyNzEtODY0NmY0MGJjMGExIiwidXNlcmluZm8iOnsidXNlcm5hbWUiOiJiMTAwNDkiLCJuYW1lIjoiTWFkdWXDsW8gT2Nob2EsIEZsb3JlbmNpYSIsInRpdGxlIjoiU1VQRVJWSVNPUiBHRVNUSU9OIERFIFBFRElET1MgWSBSRUNMQU1PUyBUQyIsImRlcGFydG1lbnQiOiJEUFRPLiBTRVJWSUNJTyBBTCBDTElFTlRFIiwiZGl2aXNpb24iOiJWUC5ORUdPQ0lPUyBSRVRBSUwgWSBDQU5BTEVTIiwiZGVsaXZlcnlPZmZpY2VOYW1lIjoiR0VTVElPTiBERSBQRURJRE9TIFkgUkVDTEFNT1MgVEMiLCJtb2JpbGUiOm51bGwsImVtYWlsIjoiYW1hZHVlbm9AaW50ZXJjb3JwLmNvbS5wZSIsImdyb3VwcyI6WyJHSVJVX1BSRF9TVVBFUlZJU09SIiwiR0lSVV9VQVRfU1VQRVJWSVNPUiJdLCJzdXBlcnZpc29yIjoiQmFsbGVybyBDYW5jaW5vLCBBYnJhaGFtIiwic3VwZXJ2aXNvclVzZXJOYW1lIjoiYjkwNjgiLCJhcHBsaWNhdGlvbk9yaWdpbiI6ImdpcnVmcm9udCIsImFyZWFDb2RlIjoiMDAwMDA5IiwiYXJlYURlc2NyaXB0aW9uIjoiR2VzdGnDs24gRGUgUGVkaWRvcyBZIFJlY2xhbW9zIFRDIiwicHJvZmlsZUNvZGUiOiJTVlIwMDAxIiwicHJvZmlsZU5hbWUiOiJTVVBFUlZJU09SIiwiY3VycmVudERhdGUiOjE2NTYxMDQ5NDMsImF1dGhvcml0aWVzIjpbeyJhdXRob3JpdHkiOiJBTEwifV19LCJjbGllbnRfaWQiOm51bGx9.qITl9re1LjF5iEnv47wBTsYIwXZxSSa4HpV9LH9dP-4")
                    .header("Ocp-Apim-Subscription-Key", NPS_SECURITY_SUBSCRIPTION_KEY_DEV)
                    .queryParam("page", "0")
                    .queryParam("size", "4")
                    .queryParam("sortBy", "FLAG_SUPERVISOR")
                    .queryParam("direction", "DESCENDING")
                    .when().
                    body(bodyRequest)
                    .post(WebServiceEndPoints.API_HISTORICAL_INBOX.getUrl())
                    .then()
                    .log().all()
                    .extract().response();


            return response.statusCode();

        }

        else {
            return 0;
        }

    }
    @Step("construyo request y hago POST DNI")
    public int agregoLosEncabezadosEnLaPeticionClientePorDNI(String numeroDni) {

        String ambiente = Serenity.sessionVariableCalled("ambiente").toString();

        if (ambiente.equals("UAT")) {
            RestAssured.baseURI = "http://20.62.34.108/common/events";

            response = given()
                    .log().all()
                    .contentType(ContentType.JSON)
                    .relaxedHTTPSValidation()
                    .header("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJiMTM0NTAiLCJzY29wZSI6WyJhbGwiXSwiZXhwIjoxNjg5NDA2NTcyLCJhdXRob3JpdGllcyI6WyJBTEwiXSwianRpIjoiYzU4NDE4ZjgtZTE5Ny00YjA0LTgzNmItZjQ3ZjUyZDEyYTUwIiwidXNlcmluZm8iOnsidXNlcm5hbWUiOiJiMTM0NTAiLCJuYW1lIjoiQmF6YW4gU2Fyb21vLCBWaWNreSBLYXJlbiIsInRpdGxlIjoiQVNFU09SIERFIFNFUlZJQ0lPUyBFU1BFQ0lBTElaQURPUyIsImRlcGFydG1lbnQiOiJEUFRPLiBDT05UQUNUIENFTlRFUiIsImRpdmlzaW9uIjoiVlAuTkVHT0NJT1MgUkVUQUlMIFkgQ0FOQUxFUyIsImRlbGl2ZXJ5T2ZmaWNlTmFtZSI6IkNBTkFMRVMgRElHSVRBTEVTIEJUIDEiLCJtb2JpbGUiOm51bGwsImVtYWlsIjoiVkJhemFuQGludGVyY29ycC5jb20ucGUiLCJncm91cHMiOlsiR0lSVV9QUkRfQVNFU09SIiwiR0lSVV9VQVRfQVNFU09SIl0sInN1cGVydmlzb3IiOiJPbGl2YXJlcyBDaWZyZSwgSnVhbiBQYWJsbyIsInN1cGVydmlzb3JVc2VyTmFtZSI6IkIyNTQ2NyIsImFwcGxpY2F0aW9uT3JpZ2luIjoiZ2lydWZyb250IiwiYXJlYUNvZGUiOiIxMDA0NzMiLCJhcmVhRGVzY3JpcHRpb24iOiJDQU5BTEVTIERJR0lUQUxFUyBCVCAxIiwicHJvZmlsZUNvZGUiOiJBU1IwMDAxIiwicHJvZmlsZU5hbWUiOiJBU0VTT1IiLCJjdXJyZW50RGF0ZSI6MTY4OTM2MzM3MiwiYXV0aG9yaXRpZXMiOlt7ImF1dGhvcml0eSI6IkFMTCJ9XX0sImNsaWVudF9pZCI6bnVsbH0.SSoGPdUuGNmvjBPzsozraQ3vE22QTaaEXVbb6BRqsGE")
                    .header("Ocp-Apim-Subscription-Key", NPS_SECURITY_SUBSCRIPTION_KEY_UAT)
                    .header("Host", "20.62.34.108")
                    .queryParam("identityDocumentType", "DNI")
                    .queryParam("identityDocumentNumber", numeroDni)
                    .queryParam("representativeLegal", "0")
                    .when().
                    body(bodyRequest)
                    .post()
                    .then()
                    .log().all()
                    .extract().response();


            return response.statusCode();

        } else if (ambiente.equals("STG")) {
            RestAssured.baseURI = "http://20.7.216.52/common/events";


            response = given()
                    .log().all()
                    .contentType(ContentType.JSON)
                    .relaxedHTTPSValidation()
                    .header("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJiMTM0NTAiLCJzY29wZSI6WyJhbGwiXSwiZXhwIjoxNjg5MzUxODQwLCJhdXRob3JpdGllcyI6WyJBTEwiXSwianRpIjoiN2E0NTQ5ODQtN2EwNi00YmI0LWJhNWItZGY5YzRmNzE2YjI0IiwidXNlcmluZm8iOnsidXNlcm5hbWUiOiJiMTM0NTAiLCJuYW1lIjoiQmF6YW4gU2Fyb21vLCBWaWNreSBLYXJlbiIsInRpdGxlIjoiQVNFU09SIERFIFNFUlZJQ0lPUyBFU1BFQ0lBTElaQURPUyIsImRlcGFydG1lbnQiOiJEUFRPLiBDT05UQUNUIENFTlRFUiIsImRpdmlzaW9uIjoiVlAuTkVHT0NJT1MgUkVUQUlMIFkgQ0FOQUxFUyIsImRlbGl2ZXJ5T2ZmaWNlTmFtZSI6IkNBTkFMRVMgRElHSVRBTEVTIEJUIDEiLCJtb2JpbGUiOm51bGwsImVtYWlsIjoiVkJhemFuQGludGVyY29ycC5jb20ucGUiLCJncm91cHMiOlsiR0lSVV9QUkRfQVNFU09SIiwiR0lSVV9VQVRfQVNFU09SIl0sInN1cGVydmlzb3IiOiJPbGl2YXJlcyBDaWZyZSwgSnVhbiBQYWJsbyIsInN1cGVydmlzb3JVc2VyTmFtZSI6IkIyNTQ2NyIsImFwcGxpY2F0aW9uT3JpZ2luIjoiZ2lydWZyb250IiwiYXJlYUNvZGUiOiIxMDA0NTQiLCJhcmVhRGVzY3JpcHRpb24iOiJDQU5BTEVTIERJR0lUQUxFUyBCVCAxIiwicHJvZmlsZUNvZGUiOiJBU1IwMDAxIiwicHJvZmlsZU5hbWUiOiJBU0VTT1IiLCJjdXJyZW50RGF0ZSI6MTY4OTM0ODI0MCwiYXV0aG9yaXRpZXMiOlt7ImF1dGhvcml0eSI6IkFMTCJ9XX0sImNsaWVudF9pZCI6bnVsbH0.g55tKJLjhGdEWpqcI0Mnd4Gq9pjB5SR_I5vaDNu5Vck")
                    .header("Ocp-Apim-Subscription-Key", NPS_SECURITY_SUBSCRIPTION_KEY_STG)
                    .header("Host", "20.7.216.52")
                    .queryParam("identityDocumentType", "DNI")
                    .queryParam("identityDocumentNumber", numeroDni)
                    .queryParam("representativeLegal", "0")
                    .when().
                    body(bodyRequest)
                    .post()
                    .then()
                    .log().all()
                    .extract().response();


            return response.statusCode();

        } else if (ambiente.equals("DEV")) {
        RestAssured.baseURI = URL_BASE_HISTORICAL_INBOX_DEV;

        response = given()
                .log().all()
                .contentType(ContentType.JSON)
                .relaxedHTTPSValidation()
                .header("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJiMTAwNDkiLCJzY29wZSI6WyJhbGwiXSwiZXhwIjoxNjU2MTA4NTQzLCJhdXRob3JpdGllcyI6WyJBTEwiXSwianRpIjoiNGQ0MmYxZDUtYWU2Ni00YTliLTgyNzEtODY0NmY0MGJjMGExIiwidXNlcmluZm8iOnsidXNlcm5hbWUiOiJiMTAwNDkiLCJuYW1lIjoiTWFkdWXDsW8gT2Nob2EsIEZsb3JlbmNpYSIsInRpdGxlIjoiU1VQRVJWSVNPUiBHRVNUSU9OIERFIFBFRElET1MgWSBSRUNMQU1PUyBUQyIsImRlcGFydG1lbnQiOiJEUFRPLiBTRVJWSUNJTyBBTCBDTElFTlRFIiwiZGl2aXNpb24iOiJWUC5ORUdPQ0lPUyBSRVRBSUwgWSBDQU5BTEVTIiwiZGVsaXZlcnlPZmZpY2VOYW1lIjoiR0VTVElPTiBERSBQRURJRE9TIFkgUkVDTEFNT1MgVEMiLCJtb2JpbGUiOm51bGwsImVtYWlsIjoiYW1hZHVlbm9AaW50ZXJjb3JwLmNvbS5wZSIsImdyb3VwcyI6WyJHSVJVX1BSRF9TVVBFUlZJU09SIiwiR0lSVV9VQVRfU1VQRVJWSVNPUiJdLCJzdXBlcnZpc29yIjoiQmFsbGVybyBDYW5jaW5vLCBBYnJhaGFtIiwic3VwZXJ2aXNvclVzZXJOYW1lIjoiYjkwNjgiLCJhcHBsaWNhdGlvbk9yaWdpbiI6ImdpcnVmcm9udCIsImFyZWFDb2RlIjoiMDAwMDA5IiwiYXJlYURlc2NyaXB0aW9uIjoiR2VzdGnDs24gRGUgUGVkaWRvcyBZIFJlY2xhbW9zIFRDIiwicHJvZmlsZUNvZGUiOiJTVlIwMDAxIiwicHJvZmlsZU5hbWUiOiJTVVBFUlZJU09SIiwiY3VycmVudERhdGUiOjE2NTYxMDQ5NDMsImF1dGhvcml0aWVzIjpbeyJhdXRob3JpdHkiOiJBTEwifV19LCJjbGllbnRfaWQiOm51bGx9.qITl9re1LjF5iEnv47wBTsYIwXZxSSa4HpV9LH9dP-4")
                .header("Ocp-Apim-Subscription-Key", NPS_SECURITY_SUBSCRIPTION_KEY_DEV)
                .header("Host", "20.62.34.108")
                .queryParam("page", "0")
                .queryParam("size", "4")
                .queryParam("sortBy", "FLAG_SUPERVISOR")
                .queryParam("direction", "DESCENDING")
                .when().
                body(bodyRequest)
                .post(WebServiceEndPoints.API_HISTORICAL_INBOX.getUrl())
                .then()
                .log().all()
                .extract().response();


        return response.statusCode();

    }

        else {
            return 0;
        }
        
    }


    @Step("construyo request y hago POST TARJETA")
    public int agregoLosEncabezadosEnLaPeticionClientePorTarjeta(String codigoUnico) {

        String ambiente = Serenity.sessionVariableCalled("ambiente").toString();

        if (ambiente.equals("UAT")) {
            RestAssured.baseURI = "http://20.62.34.108/common/cards/"+codigoUnico+"/credit-cards/new";

            response = given()
                    .log().all()
                    .contentType(ContentType.JSON)
                    .relaxedHTTPSValidation()
                    .header("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJiMTM0NTAiLCJzY29wZSI6WyJhbGwiXSwiZXhwIjoxNjg5NDA2NTcyLCJhdXRob3JpdGllcyI6WyJBTEwiXSwianRpIjoiYzU4NDE4ZjgtZTE5Ny00YjA0LTgzNmItZjQ3ZjUyZDEyYTUwIiwidXNlcmluZm8iOnsidXNlcm5hbWUiOiJiMTM0NTAiLCJuYW1lIjoiQmF6YW4gU2Fyb21vLCBWaWNreSBLYXJlbiIsInRpdGxlIjoiQVNFU09SIERFIFNFUlZJQ0lPUyBFU1BFQ0lBTElaQURPUyIsImRlcGFydG1lbnQiOiJEUFRPLiBDT05UQUNUIENFTlRFUiIsImRpdmlzaW9uIjoiVlAuTkVHT0NJT1MgUkVUQUlMIFkgQ0FOQUxFUyIsImRlbGl2ZXJ5T2ZmaWNlTmFtZSI6IkNBTkFMRVMgRElHSVRBTEVTIEJUIDEiLCJtb2JpbGUiOm51bGwsImVtYWlsIjoiVkJhemFuQGludGVyY29ycC5jb20ucGUiLCJncm91cHMiOlsiR0lSVV9QUkRfQVNFU09SIiwiR0lSVV9VQVRfQVNFU09SIl0sInN1cGVydmlzb3IiOiJPbGl2YXJlcyBDaWZyZSwgSnVhbiBQYWJsbyIsInN1cGVydmlzb3JVc2VyTmFtZSI6IkIyNTQ2NyIsImFwcGxpY2F0aW9uT3JpZ2luIjoiZ2lydWZyb250IiwiYXJlYUNvZGUiOiIxMDA0NzMiLCJhcmVhRGVzY3JpcHRpb24iOiJDQU5BTEVTIERJR0lUQUxFUyBCVCAxIiwicHJvZmlsZUNvZGUiOiJBU1IwMDAxIiwicHJvZmlsZU5hbWUiOiJBU0VTT1IiLCJjdXJyZW50RGF0ZSI6MTY4OTM2MzM3MiwiYXV0aG9yaXRpZXMiOlt7ImF1dGhvcml0eSI6IkFMTCJ9XX0sImNsaWVudF9pZCI6bnVsbH0.SSoGPdUuGNmvjBPzsozraQ3vE22QTaaEXVbb6BRqsGE")
                    .header("Ocp-Apim-Subscription-Key", NPS_SECURITY_SUBSCRIPTION_KEY_UAT)
                    .header("Host", "20.62.34.108")
                       .when().
                    body(bodyRequest)
                    .get()
                    .then()
                    .log().all()
                    .extract().response();


            return response.statusCode();

        } else if (ambiente.equals("STG")) {
            RestAssured.baseURI = "http://20.7.216.52/common/cards/"+codigoUnico+"/credit-cards/new";


            response = given()
                    .log().all()
                    .contentType(ContentType.JSON)
                    .relaxedHTTPSValidation()
                    .header("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJiMTM0NTAiLCJzY29wZSI6WyJhbGwiXSwiZXhwIjoxNjg5MzUxODQwLCJhdXRob3JpdGllcyI6WyJBTEwiXSwianRpIjoiN2E0NTQ5ODQtN2EwNi00YmI0LWJhNWItZGY5YzRmNzE2YjI0IiwidXNlcmluZm8iOnsidXNlcm5hbWUiOiJiMTM0NTAiLCJuYW1lIjoiQmF6YW4gU2Fyb21vLCBWaWNreSBLYXJlbiIsInRpdGxlIjoiQVNFU09SIERFIFNFUlZJQ0lPUyBFU1BFQ0lBTElaQURPUyIsImRlcGFydG1lbnQiOiJEUFRPLiBDT05UQUNUIENFTlRFUiIsImRpdmlzaW9uIjoiVlAuTkVHT0NJT1MgUkVUQUlMIFkgQ0FOQUxFUyIsImRlbGl2ZXJ5T2ZmaWNlTmFtZSI6IkNBTkFMRVMgRElHSVRBTEVTIEJUIDEiLCJtb2JpbGUiOm51bGwsImVtYWlsIjoiVkJhemFuQGludGVyY29ycC5jb20ucGUiLCJncm91cHMiOlsiR0lSVV9QUkRfQVNFU09SIiwiR0lSVV9VQVRfQVNFU09SIl0sInN1cGVydmlzb3IiOiJPbGl2YXJlcyBDaWZyZSwgSnVhbiBQYWJsbyIsInN1cGVydmlzb3JVc2VyTmFtZSI6IkIyNTQ2NyIsImFwcGxpY2F0aW9uT3JpZ2luIjoiZ2lydWZyb250IiwiYXJlYUNvZGUiOiIxMDA0NTQiLCJhcmVhRGVzY3JpcHRpb24iOiJDQU5BTEVTIERJR0lUQUxFUyBCVCAxIiwicHJvZmlsZUNvZGUiOiJBU1IwMDAxIiwicHJvZmlsZU5hbWUiOiJBU0VTT1IiLCJjdXJyZW50RGF0ZSI6MTY4OTM0ODI0MCwiYXV0aG9yaXRpZXMiOlt7ImF1dGhvcml0eSI6IkFMTCJ9XX0sImNsaWVudF9pZCI6bnVsbH0.g55tKJLjhGdEWpqcI0Mnd4Gq9pjB5SR_I5vaDNu5Vck")
                    .header("Ocp-Apim-Subscription-Key", NPS_SECURITY_SUBSCRIPTION_KEY_STG)
                    .header("Host", "20.7.216.52")
                    .when().
                    body(bodyRequest)
                    .get()
                    .then()
                    .log().all()
                    .extract().response();


            return response.statusCode();

        } else if (ambiente.equals("DEV")) {
            RestAssured.baseURI = URL_BASE_HISTORICAL_INBOX_DEV;

            response = given()
                    .log().all()
                    .contentType(ContentType.JSON)
                    .relaxedHTTPSValidation()
                    .header("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJiMTAwNDkiLCJzY29wZSI6WyJhbGwiXSwiZXhwIjoxNjU2MTA4NTQzLCJhdXRob3JpdGllcyI6WyJBTEwiXSwianRpIjoiNGQ0MmYxZDUtYWU2Ni00YTliLTgyNzEtODY0NmY0MGJjMGExIiwidXNlcmluZm8iOnsidXNlcm5hbWUiOiJiMTAwNDkiLCJuYW1lIjoiTWFkdWXDsW8gT2Nob2EsIEZsb3JlbmNpYSIsInRpdGxlIjoiU1VQRVJWSVNPUiBHRVNUSU9OIERFIFBFRElET1MgWSBSRUNMQU1PUyBUQyIsImRlcGFydG1lbnQiOiJEUFRPLiBTRVJWSUNJTyBBTCBDTElFTlRFIiwiZGl2aXNpb24iOiJWUC5ORUdPQ0lPUyBSRVRBSUwgWSBDQU5BTEVTIiwiZGVsaXZlcnlPZmZpY2VOYW1lIjoiR0VTVElPTiBERSBQRURJRE9TIFkgUkVDTEFNT1MgVEMiLCJtb2JpbGUiOm51bGwsImVtYWlsIjoiYW1hZHVlbm9AaW50ZXJjb3JwLmNvbS5wZSIsImdyb3VwcyI6WyJHSVJVX1BSRF9TVVBFUlZJU09SIiwiR0lSVV9VQVRfU1VQRVJWSVNPUiJdLCJzdXBlcnZpc29yIjoiQmFsbGVybyBDYW5jaW5vLCBBYnJhaGFtIiwic3VwZXJ2aXNvclVzZXJOYW1lIjoiYjkwNjgiLCJhcHBsaWNhdGlvbk9yaWdpbiI6ImdpcnVmcm9udCIsImFyZWFDb2RlIjoiMDAwMDA5IiwiYXJlYURlc2NyaXB0aW9uIjoiR2VzdGnDs24gRGUgUGVkaWRvcyBZIFJlY2xhbW9zIFRDIiwicHJvZmlsZUNvZGUiOiJTVlIwMDAxIiwicHJvZmlsZU5hbWUiOiJTVVBFUlZJU09SIiwiY3VycmVudERhdGUiOjE2NTYxMDQ5NDMsImF1dGhvcml0aWVzIjpbeyJhdXRob3JpdHkiOiJBTEwifV19LCJjbGllbnRfaWQiOm51bGx9.qITl9re1LjF5iEnv47wBTsYIwXZxSSa4HpV9LH9dP-4")
                    .header("Ocp-Apim-Subscription-Key", NPS_SECURITY_SUBSCRIPTION_KEY_DEV)
                    .header("Host", "20.62.34.108")
                    .queryParam("page", "0")
                    .queryParam("size", "4")
                    .queryParam("sortBy", "FLAG_SUPERVISOR")
                    .queryParam("direction", "DESCENDING")
                    .when().
                    body(bodyRequest)
                    .post(WebServiceEndPoints.API_HISTORICAL_INBOX.getUrl())
                    .then()
                    .log().all()
                    .extract().response();


            return response.statusCode();

        }

        else {
            return 0;
        }

    }


    @Step("construyo request y hago POST TIPOLOGIAS")
    public int agregoLosEncabezadosEnLaPeticionClientePorTipologia(String eventId, String type) {

        String ambiente = Serenity.sessionVariableCalled("ambiente").toString();

        if (ambiente.equals("UAT")) {
            RestAssured.baseURI = "http://20.62.34.108/common/typologies";

            response = given()
                    .log().all()
                    .contentType(ContentType.JSON)
                    .relaxedHTTPSValidation()
                    .header("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJiMTAwNDkiLCJzY29wZSI6WyJhbGwiXSwiZXhwIjoxNjU2MTA4NTQzLCJhdXRob3JpdGllcyI6WyJBTEwiXSwianRpIjoiNGQ0MmYxZDUtYWU2Ni00YTliLTgyNzEtODY0NmY0MGJjMGExIiwidXNlcmluZm8iOnsidXNlcm5hbWUiOiJiMTAwNDkiLCJuYW1lIjoiTWFkdWXDsW8gT2Nob2EsIEZsb3JlbmNpYSIsInRpdGxlIjoiU1VQRVJWSVNPUiBHRVNUSU9OIERFIFBFRElET1MgWSBSRUNMQU1PUyBUQyIsImRlcGFydG1lbnQiOiJEUFRPLiBTRVJWSUNJTyBBTCBDTElFTlRFIiwiZGl2aXNpb24iOiJWUC5ORUdPQ0lPUyBSRVRBSUwgWSBDQU5BTEVTIiwiZGVsaXZlcnlPZmZpY2VOYW1lIjoiR0VTVElPTiBERSBQRURJRE9TIFkgUkVDTEFNT1MgVEMiLCJtb2JpbGUiOm51bGwsImVtYWlsIjoiYW1hZHVlbm9AaW50ZXJjb3JwLmNvbS5wZSIsImdyb3VwcyI6WyJHSVJVX1BSRF9TVVBFUlZJU09SIiwiR0lSVV9VQVRfU1VQRVJWSVNPUiJdLCJzdXBlcnZpc29yIjoiQmFsbGVybyBDYW5jaW5vLCBBYnJhaGFtIiwic3VwZXJ2aXNvclVzZXJOYW1lIjoiYjkwNjgiLCJhcHBsaWNhdGlvbk9yaWdpbiI6ImdpcnVmcm9udCIsImFyZWFDb2RlIjoiMDAwMDA5IiwiYXJlYURlc2NyaXB0aW9uIjoiR2VzdGnDs24gRGUgUGVkaWRvcyBZIFJlY2xhbW9zIFRDIiwicHJvZmlsZUNvZGUiOiJTVlIwMDAxIiwicHJvZmlsZU5hbWUiOiJTVVBFUlZJU09SIiwiY3VycmVudERhdGUiOjE2NTYxMDQ5NDMsImF1dGhvcml0aWVzIjpbeyJhdXRob3JpdHkiOiJBTEwifV19LCJjbGllbnRfaWQiOm51bGx9.qITl9re1LjF5iEnv47wBTsYIwXZxSSa4HpV9LH9dP-4")
                    .header("Ocp-Apim-Subscription-Key", NPS_SECURITY_SUBSCRIPTION_KEY_UAT)
                    .header("Host", "20.62.34.108")
                    .queryParam("productCode", "CREDIT_CARD")
                    .queryParam("type", type)
                    .queryParam("eventId", eventId)
                    .get()
                    .then()
                    .log().all()
                    .extract().response();


            return response.statusCode();

        } else if (ambiente.equals("STG")) {
            RestAssured.baseURI = "http://20.7.216.52/common/typologies";


            response = given()
                    .log().all()
                    .contentType(ContentType.JSON)
                    .relaxedHTTPSValidation()
                    .header("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJiMTM0NTAiLCJzY29wZSI6WyJhbGwiXSwiZXhwIjoxNjg5MzUxODQwLCJhdXRob3JpdGllcyI6WyJBTEwiXSwianRpIjoiN2E0NTQ5ODQtN2EwNi00YmI0LWJhNWItZGY5YzRmNzE2YjI0IiwidXNlcmluZm8iOnsidXNlcm5hbWUiOiJiMTM0NTAiLCJuYW1lIjoiQmF6YW4gU2Fyb21vLCBWaWNreSBLYXJlbiIsInRpdGxlIjoiQVNFU09SIERFIFNFUlZJQ0lPUyBFU1BFQ0lBTElaQURPUyIsImRlcGFydG1lbnQiOiJEUFRPLiBDT05UQUNUIENFTlRFUiIsImRpdmlzaW9uIjoiVlAuTkVHT0NJT1MgUkVUQUlMIFkgQ0FOQUxFUyIsImRlbGl2ZXJ5T2ZmaWNlTmFtZSI6IkNBTkFMRVMgRElHSVRBTEVTIEJUIDEiLCJtb2JpbGUiOm51bGwsImVtYWlsIjoiVkJhemFuQGludGVyY29ycC5jb20ucGUiLCJncm91cHMiOlsiR0lSVV9QUkRfQVNFU09SIiwiR0lSVV9VQVRfQVNFU09SIl0sInN1cGVydmlzb3IiOiJPbGl2YXJlcyBDaWZyZSwgSnVhbiBQYWJsbyIsInN1cGVydmlzb3JVc2VyTmFtZSI6IkIyNTQ2NyIsImFwcGxpY2F0aW9uT3JpZ2luIjoiZ2lydWZyb250IiwiYXJlYUNvZGUiOiIxMDA0NTQiLCJhcmVhRGVzY3JpcHRpb24iOiJDQU5BTEVTIERJR0lUQUxFUyBCVCAxIiwicHJvZmlsZUNvZGUiOiJBU1IwMDAxIiwicHJvZmlsZU5hbWUiOiJBU0VTT1IiLCJjdXJyZW50RGF0ZSI6MTY4OTM0ODI0MCwiYXV0aG9yaXRpZXMiOlt7ImF1dGhvcml0eSI6IkFMTCJ9XX0sImNsaWVudF9pZCI6bnVsbH0.g55tKJLjhGdEWpqcI0Mnd4Gq9pjB5SR_I5vaDNu5Vck")
                    .header("Ocp-Apim-Subscription-Key", NPS_SECURITY_SUBSCRIPTION_KEY_STG)
                    .header("Host", "20.7.216.52")
                    .queryParam("productCode", "CREDIT_CARD")
                    .queryParam("type", type)
                    .queryParam("eventId", eventId)
                    .get()
                    .then()
                    .log().all()
                    .extract().response();


            return response.statusCode();

        } else if (ambiente.equals("DEV")) {
            RestAssured.baseURI = URL_BASE_HISTORICAL_INBOX_DEV;

            response = given()
                    .log().all()
                    .contentType(ContentType.JSON)
                    .relaxedHTTPSValidation()
                    .header("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJiMTAwNDkiLCJzY29wZSI6WyJhbGwiXSwiZXhwIjoxNjU2MTA4NTQzLCJhdXRob3JpdGllcyI6WyJBTEwiXSwianRpIjoiNGQ0MmYxZDUtYWU2Ni00YTliLTgyNzEtODY0NmY0MGJjMGExIiwidXNlcmluZm8iOnsidXNlcm5hbWUiOiJiMTAwNDkiLCJuYW1lIjoiTWFkdWXDsW8gT2Nob2EsIEZsb3JlbmNpYSIsInRpdGxlIjoiU1VQRVJWSVNPUiBHRVNUSU9OIERFIFBFRElET1MgWSBSRUNMQU1PUyBUQyIsImRlcGFydG1lbnQiOiJEUFRPLiBTRVJWSUNJTyBBTCBDTElFTlRFIiwiZGl2aXNpb24iOiJWUC5ORUdPQ0lPUyBSRVRBSUwgWSBDQU5BTEVTIiwiZGVsaXZlcnlPZmZpY2VOYW1lIjoiR0VTVElPTiBERSBQRURJRE9TIFkgUkVDTEFNT1MgVEMiLCJtb2JpbGUiOm51bGwsImVtYWlsIjoiYW1hZHVlbm9AaW50ZXJjb3JwLmNvbS5wZSIsImdyb3VwcyI6WyJHSVJVX1BSRF9TVVBFUlZJU09SIiwiR0lSVV9VQVRfU1VQRVJWSVNPUiJdLCJzdXBlcnZpc29yIjoiQmFsbGVybyBDYW5jaW5vLCBBYnJhaGFtIiwic3VwZXJ2aXNvclVzZXJOYW1lIjoiYjkwNjgiLCJhcHBsaWNhdGlvbk9yaWdpbiI6ImdpcnVmcm9udCIsImFyZWFDb2RlIjoiMDAwMDA5IiwiYXJlYURlc2NyaXB0aW9uIjoiR2VzdGnDs24gRGUgUGVkaWRvcyBZIFJlY2xhbW9zIFRDIiwicHJvZmlsZUNvZGUiOiJTVlIwMDAxIiwicHJvZmlsZU5hbWUiOiJTVVBFUlZJU09SIiwiY3VycmVudERhdGUiOjE2NTYxMDQ5NDMsImF1dGhvcml0aWVzIjpbeyJhdXRob3JpdHkiOiJBTEwifV19LCJjbGllbnRfaWQiOm51bGx9.qITl9re1LjF5iEnv47wBTsYIwXZxSSa4HpV9LH9dP-4")
                    .header("Ocp-Apim-Subscription-Key", NPS_SECURITY_SUBSCRIPTION_KEY_DEV)
                    .header("Host", "20.62.34.108")
                    .queryParam("page", "0")
                    .queryParam("size", "4")
                    .queryParam("sortBy", "FLAG_SUPERVISOR")
                    .queryParam("direction", "DESCENDING")
                    .when().
                    body(bodyRequest)
                    .post(WebServiceEndPoints.API_HISTORICAL_INBOX.getUrl())
                    .then()
                    .log().all()
                    .extract().response();


            return response.statusCode();

        }

        else {
            return 0;
        }



    }

    public void validoLaRespuestaObtenida() {
        if (response.statusCode() != 200) {
            System.out.println("EL SERVICIO DE 'CONSULTA DE AREA y USUARIO'" + " NO RESPONDE ");
        }
        assertThat(response.statusCode(), is(200));
    }

    public void validoAreaAsignada() {
        String areaAsiganada = response.body().path("content[0].assignedSpecialistJson[0].currentAreaDescription").toString().toUpperCase();

        String areaInicial = Serenity.sessionVariableCalled("areaInicial");



        String tipologia = Serenity.sessionVariableCalled("tipologia").toString();
        String tipoProducto = Serenity.sessionVariableCalled("tipoproducto").toString();
        String motivo = "";
        try {
            motivo = Serenity.sessionVariableCalled("motivos").toString();
        }catch (Exception e)
        {
            System.out.println(e);
        }
        Serenity.setSessionVariable("finalizarGeneracionTramite").to("No");

        //PARA LOS MOTIVOS DISTINTOS DE 21 NO SE LE ASIGNA ÁREA NI USUARIO
        if (tipoProducto.equals("TARJETA DE CREDITO") && tipologia.equals("CONSUMOS MAL PROCESADOS POR EL COMERCIO/MARCA") && !motivo.equals("21")) {
            areaInicial = "Sin área";
            Serenity.setSessionVariable("finalizarGeneracionTramite").to("Si");
        }
        //  assertThat(areaAsiganada, is(areaInicial.toUpperCase()));
    }

    public String validoCamposObtenidos() {

        Serenity.setSessionVariable("nombreAsesor").to(response.body().path("content[0].assignedSpecialistJson[0].currentAreaUserName"));
        Serenity.setSessionVariable("nombreArea").to(response.body().path("content[0].assignedSpecialistJson[0].currentAreaDescription"));

    //    Serenity.setSessionVariable("getNumeroDocumento").to(response.body().path("content[0].documentNumber"));
        ApiCommons.clientes.setNumerodocumento(response.body().path("content[0].documentNumber"));
        //Serenity.setSessionVariable("documentType").to(response.body().path("content[0].documentType"));
        ApiCommons.clientes.setTipodocumento(response.body().path("content[0].documentType"));

        Serenity.setSessionVariable("tipotramite").to(response.body().path("content[0].transactionType"));


        return response.body().path("content[0].assignedSpecialistJson[0].currentAreaUserCode");
    }

    public String obtengoCampoCodigoUnico() {

        Serenity.setSessionVariable("codigoUnico").to(response.body().path("customer.id"));
        Serenity.setSessionVariable("eventId").to(response.body().path("id"));

        return response.body().path("customer.id");
    }

    public void obtengoCantidadConsultingSupervisor(){

        Serenity.setSessionVariable("montoConsultant").to(response.body().path("autonomies.consultant"));
        Serenity.setSessionVariable("montoSupervisor").to(response.body().path("autonomies.supervisor"));

    }

    public String obtengoFechaTarjeta() {

        Serenity.setSessionVariable("fechaTarjeta").to(response.body().path("accounts[0].cards[0].startDate"));

        return response.body().path("accounts[0].cards[0].startDate");
    }

}
