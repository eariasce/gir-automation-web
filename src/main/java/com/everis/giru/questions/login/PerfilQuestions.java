package com.everis.giru.questions.login;

import com.everis.giru.userinterface.login.LoginPage;
import net.serenitybdd.screenplay.Question;

public class PerfilQuestions {

    public static Question<String> perfilAsesor() {
        return actor -> LoginPage.LBL_ASESOR.resolveFor(actor).waitUntilVisible().getText();
    }

    public static Question<String> PerfilAsesorRetencion() {
        return actor -> LoginPage.LBL_ASESOR_RETENCION.resolveFor(actor).waitUntilVisible().getText();
    }

    public static Question<String> perfilEspecialista() {
        return actor -> LoginPage.LBL_ESPECIALISTA.resolveFor(actor).waitUntilVisible().getText();
    }

}
