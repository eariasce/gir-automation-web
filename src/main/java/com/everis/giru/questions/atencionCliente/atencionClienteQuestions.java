package com.everis.giru.questions.atencionCliente;

import com.everis.giru.userinterface.atencionCliente.RegistrarTramitePage;
import net.serenitybdd.screenplay.Question;

public class atencionClienteQuestions {

    public static Question<Boolean> validarFechaIncorrecta() {
        return actor -> RegistrarTramitePage.TXT_FECHA_ROJO.resolveFor(actor).isPresent();
    }

    public static Question<Boolean> validarMontoIncorrecto() {
        return actor -> RegistrarTramitePage.TXT_MONTO_ROJO.resolveFor(actor).isPresent();
    }

    public static Question<Boolean> validarSinRespuesta() {
        return actor -> RegistrarTramitePage.AREA_RESPUESTA_CLIENTE.resolveFor(actor).isPresent();
    }

    public static Question<Boolean> validarDNIIncorrecto() {
        return actor -> RegistrarTramitePage.TXT_NUMERO_DNI_ROJO.resolveFor(actor).isPresent();
    }

    public static Question<Boolean> validaBotonAgregarMovimientoManual() {
        return actor -> RegistrarTramitePage.TXT_NUMERO_DNI_ROJO.resolveFor(actor).isPresent();
    }

    public static Question<Boolean> validaComentarioBloqueadoNoLlenado() {
        return actor -> RegistrarTramitePage.TXT_COMENTARIO_BLOQUEADO.resolveFor(actor).isPresent();
    }

}
