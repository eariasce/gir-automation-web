package com.everis.giru.questions.e2eTramites;

import com.everis.giru.userinterface.e2eTramite.resolucionTramite.ResumenPage;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class DatosResumenQuestions {

    public static Question<String> numeroTarjetaResumen() {
        WaitUntil.the(ResumenPage.LBL_NUMTARJETA, isVisible()).forNoMoreThan(2).seconds();
        return actor -> ResumenPage.LBL_NUMTARJETA.resolveFor(actor).waitUntilVisible().getText();
    }

    public static Question<String> tipologiaResumen() {
        WaitUntil.the(ResumenPage.LBL_TIPOLOGIA, isVisible()).forNoMoreThan(2).seconds();
        return actor -> ResumenPage.LBL_TIPOLOGIA.resolveFor(actor).waitUntilVisible().getText();
    }

    public static Question<String> numeroTramiteResumen() {
        WaitUntil.the(ResumenPage.LBL_NUMEROTRAMITE, isVisible()).forNoMoreThan(2).seconds();
        return actor -> ResumenPage.LBL_NUMEROTRAMITE.resolveFor(actor).waitUntilVisible().getText();
    }

}
