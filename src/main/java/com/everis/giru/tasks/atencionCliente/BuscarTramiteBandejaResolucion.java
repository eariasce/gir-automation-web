package com.everis.giru.tasks.atencionCliente;

import com.everis.giru.userinterface.tramites.ValidarTramiteBandejaPage;
import lombok.SneakyThrows;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class BuscarTramiteBandejaResolucion implements Task {
    String esComercial="";
    private final String numerotramite;
    ValidarTramiteBandejaPage validarTramiteBandejaPage = new ValidarTramiteBandejaPage();

    String tipoProducto = Serenity.sessionVariableCalled("tipoproducto").toString();
    String tipologia = Serenity.sessionVariableCalled("tipologia").toString();

    public BuscarTramiteBandejaResolucion(String numerotramite) {
        this.numerotramite = numerotramite;
    }

    @SneakyThrows
    @Override
    public <T extends Actor> void performAs(T actor) {

        try {
            esComercial = Serenity.sessionVariableCalled("COMERCIAL").toString();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    if(esComercial.equals("COMERCIAL")){
        validarTramiteBandejaPage.buscarTramiteBandejaResolucion(numerotramite);
    }else {
        //TODO: TARJETA DE CREDITO - TIPOLOGIA PA - TRASLADOS CTS
        if (tipoProducto.equals("TARJETA DE CREDITO") || tipoProducto.equals("TARJETA DE DEBITO") ||
                tipoProducto.equals("TRASLADOS CTS") || tipoProducto.equals("CUENTA") ||
                tipoProducto.equals("COBRANZAS") || tipoProducto.equals("EFECTIVO CON GARANTÍA HIPOTECARIO")) {

            if (tipologia.equals("CAMBIO DE FORMA DE ENVÍO DE CORRESPONDENCIA") || tipologia.equals("ABONO DE MILLAS") ||
                    tipologia.equals("CAMBIO DE TARJETA") || tipologia.equals("CONSTANCIA DE NO ADEUDO") ||
                    tipologia.equals("ATENCIÓN POR PRENVENCIÓN Y FRAUDES") || tipologia.equals("TRÁMITES DOCUMENTARIOS POST VENTA")) {
                validarTramiteBandejaPage.buscarTramiteBandejaResolucionPA(numerotramite);

            } else if (tipologia.equals("TRASLADO DE CTS DIGITAL A INTERBANK") || tipologia.equals("TRASLADO DE CTS FÍSICO A INTERBANK") ||
                    tipologia.equals("TRASLADOS DE CTS A OTRO BANCO")) {
                validarTramiteBandejaPage.buscarTramiteBandejaResolucionTrasladoCTS(numerotramite);

            } else {
                validarTramiteBandejaPage.buscarTramiteBandejaResolucion(numerotramite);
            }

        } else {
            validarTramiteBandejaPage.buscarTramiteBandejaResolucion(numerotramite);
        }
    }

    }


    public static Performable withData(String numerotramite) {
        return instrumented(BuscarTramiteBandejaResolucion.class, numerotramite);
    }
}