package com.everis.giru.tasks.atencionCliente;

import com.everis.giru.tasks.login.SeleccionarMenuPrincipal;
import com.everis.giru.userinterface.atencionCliente.ValidacionesRegistroPage;
import com.everis.giru.userinterface.atencionCliente.tramiteReclamo.ButtonContinuarMotivo;
import com.everis.giru.utils.ApiCommons;
import com.everis.giru.utils.NotificacionSlack;
import com.everis.giru.utils.WriteToFile;
import com.everis.tdm.Do;
import com.everis.tdm.model.giru.ObtenerMotivoId;
import com.everis.tdm.model.giru.ObtenerMotivoIdComercial;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import org.openqa.selenium.By;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;
import java.util.List;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class RegistraMotivoMovimiento implements Task {

    ButtonContinuarMotivo buttonContinuarMotivo = new ButtonContinuarMotivo();
    ValidacionesRegistroPage validacionRegistroPage = new ValidacionesRegistroPage();

    String esComercial = "";

    private final String motivo;
    private final String tipomoneda;
    private final String montoMovimiento;
    private final String cantidadmontomotivo;

    public RegistraMotivoMovimiento(String motivo, String tipomoneda, String montoMovimiento, String cantidadmontomotivo) {
        this.motivo = motivo;
        this.tipomoneda = tipomoneda;
        this.montoMovimiento = montoMovimiento;
        this.cantidadmontomotivo = cantidadmontomotivo;
    }


    @Override
    public <T extends Actor> void performAs(T actor) {

        String ambiente = Serenity.sessionVariableCalled("ambiente").toString();
        String tipoproducto = Serenity.sessionVariableCalled("tipoproducto").toString();
        String tipologia = Serenity.sessionVariableCalled("tipologia").toString();

        try {
            esComercial = Serenity.sessionVariableCalled("COMERCIAL").toString();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        String motivos2 = "";
        int cantidad = 0;

        String[] selecciones = motivo.split("-");
        cantidad = selecciones.length;

        if (esComercial.equals("COMERCIAL")) {
            List<ObtenerMotivoIdComercial> listamotivosComercial = Do.getMotivoIdComercial(ambiente, tipoproducto, tipologia);

            for (int n = 0; n < listamotivosComercial.size(); n++) {

                for (int k = 0; k < selecciones.length; k++) {
                    String id = listamotivosComercial.get(Integer.valueOf(selecciones[k]) - 1).getIdMotivo();

                    if (n != listamotivosComercial.size() - 1) {
                        motivos2 = motivos2 + id + "-";
                    } else {
                        motivos2 = motivos2 + id;
                    }
                }
            }
        } else {
            //TODO: OBTIENE DESDE TDM - MOTIVOS
            List<ObtenerMotivoId> listamotivosRetail = Do.getMotivoId(ambiente, tipoproducto, tipologia);

            for (int n = 0; n < listamotivosRetail.size(); n++) {

                for (int k = 0; k < selecciones.length; k++) {
                    String id = listamotivosRetail.get(Integer.valueOf(selecciones[k]) - 1).getIdMotivo();

                    if (n != listamotivosRetail.size() - 1) {
                        motivos2 = motivos2 + id + "-";
                    } else {
                        motivos2 = motivos2 + id;
                    }
                }
            }
        }


        String[] motivos;
        if (Serenity.sessionVariableCalled("ambiente").equals("DEV")) {
            motivos = motivo.split("-");
        } else {
            motivos = motivos2.split("-");
        }


        if (esComercial.equals("COMERCIAL")) {
            for (int i = 0; i < cantidad; i++) {

                try {
                    Thread.sleep(3000);
                } catch (InterruptedException ie) {
                    System.out.println(ie);
                }

                if (motivo.contains("-")) {
                    System.out.println("Motivo a seleccionar: " + motivo.split("-")[i] + " de los motivos " + motivo);
                } else {
                    System.out.println("Motivo a seleccionar: " + motivos2.split("-")[i] + " de los motivos " + motivo);
                }

                //TODO: SELECCIONAR - DROPDOWN - MOTIVO
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();

                //TODO: VARIABLE CON VALOR - MOTIVO
                String motivoselect = "";
                if (!Serenity.sessionVariableCalled("ambiente").equals("DEV")) {
                    motivoselect = motivos2.split("-")[i];
                }

                if (Serenity.sessionVariableCalled("ambiente").equals("DEV")) {
                    Serenity.getDriver().findElement(By.xpath("(//*[@id='reasonClaim']/div[2]/ul/li[1])[last()]")).click();

                } else {
                    Serenity.getDriver().findElement(By.xpath("(//*[@id='" + motivoselect + "'])[last()]")).click();
                }

                if (tipoproducto.equals("CRÉDITO BPE") && i>0) {

                    if (tipologia.equals("COBROS INDEBIDOS")) {
                        //TODO: Selecciona - Agrega un movimiento manualmente

                        Serenity.getDriver().findElement(By.xpath("(//*[contains(text(), ' Agrega un movimiento manualmente ')])[last()]")).click();

                    }

                }
                if (tipoproducto.equals("TARJETA DE DÉBITO") && i>0) {

                    if (tipologia.equals("CONSUMOS NO RECONOCIDOS POR FRAUDE")) {
                        //TODO: Selecciona - Agrega un movimiento manualmente
                       // Serenity.getDriver().findElement(By.xpath("(//*[contains(text(), ' Agrega un movimiento manualmente ')])[last()]")).click();
                    }
                    //if (tipologia.equals("CONSUMOS MAL PROCESADOS POR EL COMERCIO/MARCA")) {
                        //TODO: Selecciona - Agrega un movimiento manualmente
                    //    Serenity.getDriver().findElement(By.xpath("(//*[contains(text(), ' Agrega un movimiento manualmente ')])[last()]")).click();
                    //}
                }

                if (tipoproducto.equals("SEGUROS RELACIONADOS") && i>0) {

                    if (tipologia.equals("COBROS INDEBIDOS")) {
                        //TODO: Selecciona - Agrega un movimiento manualmente
                        //Serenity.getDriver().findElement(By.xpath("(//*[contains(text(), ' Agrega un movimiento manualmente ')])[last()]")).click();
                    }

                }


                //TODO: CONDICION GRANDE - INGRESA TIPOLGIAS SIN TRANSACCIÓN - SIN MOVIMIENTO EN MOTIVO
                Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(1));
                boolean lblMontoTotal = Serenity.getDriver().findElements(By.xpath("//label[normalize-space()='Monto Total']")).size() > 0;
                if (!lblMontoTotal) {
                    System.err.println("INGRESA TIPOLOGIA SIN TRANSACCIÓN ES: " + tipologia);
                } else {
                    //TODO: Se ingresa tipologías con movimiento que no están en el IF grande
                    registraMovimiento(i, motivos[i]);
                    buttonContinuarMotivo.clickContinuar(i);
                }

//                //SE AGREGA CONDICIONES POR TIPOLOGIA
//                if (tipoproducto.equals("CUENTA NEGOCIOS")) {
//                    if (tipologia.equals("COBROS INDEBIDOS")) {
//                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).sendKeys("1231234567896");
//                        try{Thread.sleep(1000);}catch (Exception e){}
//                        //Serenity.getDriver().findElement(By.xpath("//*[@id='1']/span[text()='Cuenta corriente ']")).click();
//                        Serenity.getDriver().findElement(By.xpath("//app-ibk-select/div[@id='accountType']")).click();
//                        String tipoCuenta= Serenity.sessionVariableCalled("tipoCuenta");
//                        Serenity.getDriver().findElement(By.xpath("//*[contains(text(), '"+ tipoCuenta+"')][last()]")).click();
//                        //TODO: Seleccionar - Moneda
//                        Serenity.getDriver().findElement(By.id("currency")).click();
//                        Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='currency']//li)[1]")).click();
//                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[2]")).sendKeys("1231234567896");
//                        Serenity.getDriver().findElement(By.xpath("//*[@id='usedChannel']")).click();
//                        Serenity.getDriver().findElement(By.xpath("//*[@id='Banca Telefónica']")).click();
//                    }
//                }
//                if (tipoproducto.equals("LETRAS Y CARTA FIANZA")) {
//                    if (tipologia.equals("DOCUMENTOS Y CONSTANCIAS")) {
//                        Serenity.getDriver().findElement(By.id("type")).click();
//                        Serenity.getDriver().findElement(By.id("Letra")).click();
//                        //TODO: Ingresa Importe
//                        Serenity.getDriver().findElement(By.xpath("//*[@formcontrolname='typeNumber']")).sendKeys("12312312");
//                        Serenity.getDriver().findElement(By.id("storeNumber")).sendKeys("1");
//                    }
//                }
//                if (tipoproducto.equals("CRÉDITO BPE")) {
//                    if(tipologia.equals("DOCUMENTOS Y CONSTANCIAS"))
//                    {
//                        Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("11520532");
//                    }
//                    if (tipologia.equals("COBROS INDEBIDOS")) {
//                        Serenity.getDriver().findElement(By.id("creditNumber")).sendKeys("1231234567896");
//                        Serenity.getDriver().findElement(By.xpath("//*[@id='creditNumber']")).click();
//                        Serenity.getDriver().findElement(By.xpath("(//*[contains(text(), 'Orden de pago')])[1]/../../../..")).click();
//                        try {
//                            Thread.sleep(1000);
//                        } catch (Exception e) {
//                        }
//                        Serenity.getDriver().findElement(By.xpath("//*[@id='list']/li[@id='1']/span[text()='Orden de pago ']")).click();
//                        Serenity.getDriver().findElement(By.xpath("//*[@id='usedChannel']")).click();
//                        Serenity.getDriver().findElement(By.xpath("//*[@id='Banca Telefónica']")).click();
//                    }
//                }
//                if(tipologia.equals("DESACUERDO CON LA ATENCIÓN"))
//                {
//                    Serenity.getDriver().findElement(By.xpath("//*[@id='incidentDate']")).sendKeys("11/04/2023");
//                    Serenity.getDriver().findElement(By.xpath("//*[@id='productTypeCompany']")).click();
//                    try{Thread.sleep(1000);}catch (Exception e){}
//                    Serenity.getDriver().findElement(By.xpath("//*[@id='Banca Telefónica']/span")).click();
//                }

            }

        } else {
            for (int i = 0; i < cantidad; i++) {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException ie) {
                    System.out.println(ie);
                }


                if (motivo.contains("-")) {
                    System.out.println("Motivo a seleccionar: " + motivo.split("-")[i] + " de los motivos " + motivo);
                } else {
                    System.out.println("Motivo a seleccionar: " + motivos2.split("-")[i] + " de los motivos " + motivo);
                }

                //TODO: SELECCIONAR - DROPDOWN - MOTIVO
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();

                //TODO: VARIABLE CON VALOR - MOTIVO
                String motivoselect = "";
                if (!Serenity.sessionVariableCalled("ambiente").equals("DEV")) {
                    motivoselect = motivos2.split("-")[i];
                }
                if (Serenity.sessionVariableCalled("ambiente").equals("DEV")) {
                    Serenity.getDriver().findElement(By.xpath("(//*[@id='reasonClaim']/div[2]/ul/li[1])[last()]")).click();

                } else {
                //    try{Thread.sleep(51321312);}catch (Exception e){}
                    try {
                        Serenity.getDriver().findElement(By.xpath("(//*[@id='" + motivoselect + "'])[last()]")).click();
                    }catch(Exception e)
                    {
                        System.out.println(e.getMessage());
                        Serenity.getDriver().findElement(By.xpath("(//*[@id='reasonClaim']/div[2]/ul/li[1])[last()]")).click();

                    }
                }

                if (tipoproducto.equals("TARJETA DE CREDITO")) {
                    if (tipologia.equals("MODIFICACION PLAZO") || tipologia.equals("CONSUMOS NO RECONOCIDOS POR FRAUDE") ||
                            tipologia.equals("CONSUMOS MAL PROCESADOS POR EL COMERCIO/MARCA")) {
                        //TODO: Selecciona - Agrega un movimiento manualmente
                        Serenity.getDriver().findElement(By.id("Add_Amount_" + i)).click();
                    }
                } else if (tipoproducto.equals("TARJETA DE DEBITO")) {
                    if (tipologia.equals("CONSUMOS MAL PROCESADOS POR EL COMERCIO/MARCA") || tipologia.equals("CONSUMOS NO RECONOCIDOS POR FRAUDE")) {
                        //TODO: Selecciona - Agrega un movimiento manualmente
                        Serenity.getDriver().findElement(By.id("Add_Amount_" + i)).click();
                    }
                } else if (tipoproducto.equals("PRÉSTAMO PERSONAL") || tipoproducto.equals("REFINANCIADO") ||
                        tipoproducto.equals("EFECTIVO CON GARANTÍA HIPOTECARIO") || tipoproducto.equals("VEHICULAR")) {

                    if (tipologia.equals("MODIFICACIÓN CONFIGURACIÓN CRÉDITO")) {

                        if (ambiente.equals("UAT")) {

                            if (motivoselect.equals("722") || motivoselect.equals("725") || motivoselect.equals("728") || motivoselect.equals("719")) {
                                //TODO: Motivo - Cambio de moneda - Selecciona - Tipo de cambio
                                Serenity.getDriver().findElement(By.xpath("//div[@id='listselectbyreason_']")).click();
                                Serenity.getDriver().findElement(By.id("USD")).click();
                            }

                            if (motivoselect.equals("723") || motivoselect.equals("726") || motivoselect.equals("729") || motivoselect.equals("720")) {
                                //TODO: Motivo - Débito automático - Selecciona - Afiliación de cargo
                                Serenity.getDriver().findElement(By.xpath("//div[@id='listselectbyreason_']")).click();
                                //TODO: Selecciona - NO
                                Serenity.getDriver().findElement(By.id("NO")).click();
                            }

                            if (motivoselect.equals("724") || motivoselect.equals("727") || motivoselect.equals("730") || motivoselect.equals("721")) {
                                //TODO: Motivo - Modificación de fecha de pago - Selecciona - Nueva fecha de pago
                                Serenity.getDriver().findElement(By.xpath("//div[@id='listselectbyreason_']")).click();
                                //TODO: Selecciona - 17 O 2
                                Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='listselectbyreason_']//li)[1]")).click();
                            }

                        }

                        if (ambiente.equals("STG")) {
                            if (motivoselect.equals("442") || motivoselect.equals("439") || motivoselect.equals("497") || motivoselect.equals("445")) {
                                //TODO: Motivo - Cambio de moneda - Selecciona - Tipo de cambio
                                Serenity.getDriver().findElement(By.xpath("//div[@id='listselectbyreason_']")).click();
                                Serenity.getDriver().findElement(By.id("USD")).click();
                            }

                            if (motivoselect.equals("443") || motivoselect.equals("440") || motivoselect.equals("498") || motivoselect.equals("446")) {
                                //TODO: Motivo - Débito automático - Selecciona - Afiliación de cargo
                                Serenity.getDriver().findElement(By.xpath("//div[@id='listselectbyreason_']")).click();
                                //TODO: Selecciona - NO
                                Serenity.getDriver().findElement(By.id("NO")).click();
                            }

                            if (motivoselect.equals("444") || motivoselect.equals("441") || motivoselect.equals("499") || motivoselect.equals("447")) {
                                //TODO: Motivo - Modificación de fecha de pago - Selecciona - Nueva fecha de pago
                                Serenity.getDriver().findElement(By.xpath("//div[@id='listselectbyreason_']")).click();
                                //TODO: Selecciona - 17 O 2
                                Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='listselectbyreason_']//li)[1]")).click();
                            }

                        }

                        buttonContinuarMotivo.clickContinuar(i);
                    }

                    if (tipologia.equals("MODIFICACIÓN CONDICIONES CUOTAS CRÉDITO")) {

                        String tipoCuota = Serenity.sessionVariableCalled("tipoCuota").toString();

                        if (ambiente.equals("UAT")) {
                            if (motivoselect.equals("710") || motivoselect.equals("707") || motivoselect.equals("716") || motivoselect.equals("713")) {
                                //TODO: Motivo - Cambio de tipo de cuotas - Selecciona - Tipo de cuota
                                Serenity.getDriver().findElement(By.id("undefined")).click();
                                Serenity.getDriver().findElement(By.id(tipoCuota)).click();
                            }

                            if (motivoselect.equals("711") || motivoselect.equals("708") || motivoselect.equals("717") || motivoselect.equals("714")) {
                                //TODO: Motivo - Modificación de plazo - Ingresa - Plazo solicitado (meses)
                                Serenity.getDriver().findElement(By.xpath("(//input[@formcontrolname='selectbyreason'])[last()]")).sendKeys(tipoCuota);
                            }

                            if (motivoselect.equals("712") || motivoselect.equals("709") || motivoselect.equals("718") || motivoselect.equals("715")) {
                                //TODO: Motivo - Periodo de gracia - Selecciona - Periodo de gracia
                                Serenity.getDriver().findElement(By.id("undefined")).click();
                                Serenity.getDriver().findElement(By.id(tipoCuota)).click();
                            }
                        }

                        if (ambiente.equals("STG")) {
                            if (motivoselect.equals("433") || motivoselect.equals("430") || motivoselect.equals("494") || motivoselect.equals("436")) {
                                //TODO: Motivo - Cambio de tipo de cuotas - Selecciona - Tipo de cuota
                                Serenity.getDriver().findElement(By.id("undefined")).click();
                                Serenity.getDriver().findElement(By.id(tipoCuota)).click();
                            }

                            if (motivoselect.equals("434") || motivoselect.equals("431") || motivoselect.equals("495") || motivoselect.equals("437")) {
                                //TODO: Motivo - Modificación de plazo - Ingresa - Plazo solicitado (meses)
                                Serenity.getDriver().findElement(By.xpath("(//input[@formcontrolname='selectbyreason'])[last()]")).sendKeys(tipoCuota);
                            }

                            if (motivoselect.equals("435") || motivoselect.equals("432") || motivoselect.equals("496") || motivoselect.equals("438")) {
                                //TODO: Motivo - Periodo de gracia - Selecciona - Periodo de gracia
                                Serenity.getDriver().findElement(By.id("undefined")).click();
                                Serenity.getDriver().findElement(By.id(tipoCuota)).click();
                            }
                        }


                        buttonContinuarMotivo.clickContinuar(i);
                    }

                }

                //TODO: CONDICION GRANDE - INGRESA TIPOLGIAS SIN TRANSACCIÓN - SIN MOVIMIENTO EN MOTIVO
                Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(1));
                boolean lblMontoTotal = Serenity.getDriver().findElements(By.xpath("//label[normalize-space()='Monto Total']")).size() > 0;
                if (!lblMontoTotal) {
                    System.err.println("INGRESA TIPOLOGIA SIN TRANSACCIÓN ES: " + tipologia);
                } else {
                    if (tipoproducto.equals("TARJETA DE CREDITO") && tipologia.equals("COMPRA DE DEUDA INTERNA")) {
                        System.err.println("INGRESA TIPOLOGIA SIN TRANSACCIÓN ES: " + tipologia);
                    } else {
                        //TODO: Se ingresa tipologías con movimiento que no están en el IF grande
                        registraMovimiento(i, motivos[i]);
                        buttonContinuarMotivo.clickContinuar(i);
                    }
                }

            }

        }


        //TODO: TIPOLOGIAS- COMERCIAL
        if (esComercial.equals("COMERCIAL")) {
            if (tipoproducto.equals("LETRAS Y CARTA FIANZA")) {
                if (tipologia.equals("DOCUMENTOS Y CONSTANCIAS")) {
                    Serenity.getDriver().findElement(By.id("type")).click();
                    Serenity.getDriver().findElement(By.id("Letra")).click();
                    //TODO: Ingresa Importe
                    Serenity.getDriver().findElement(By.xpath("//*[@formcontrolname='typeNumber']")).sendKeys("12312312");
                    Serenity.getDriver().findElement(By.id("storeNumber")).sendKeys("1");
                }
            }

            //TODO: SELECCIONA OPCION - PRODUCTOS
            if (tipoproducto.equals("CRÉDITO BPE")) {

                if (tipologia.equals("DOCUMENTOS Y CONSTANCIAS")) {
                    //TODO: Ingresa - N° de crédito
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).sendKeys("13035456");
                }

                if (tipologia.equals("COBROS INDEBIDOS")) {
                    String valueMedioDevolucion = Serenity.sessionVariableCalled("medioDevolucion").toString();
                    String valueTipoCuenta = Serenity.sessionVariableCalled("tipoCuenta").toString();
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    String valueMoneda = Serenity.sessionVariableCalled("tipoMoneda").toString();

                    //TODO: Ingresa - N° de crédito
                    Serenity.getDriver().findElement(By.id("creditNumber")).sendKeys("13035456");
                    //TODO: Seleccionar - Medio de devolución
                    Serenity.getDriver().findElement(By.id("devolutionMedium")).click();
                    Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueMedioDevolucion + "'])[last()]")).click();
                    if(valueMedioDevolucion.equals("2")){
                        //TODO: Seleccionar - Tipo Cuenta
                        Serenity.getDriver().findElement(By.id("devolutionAccountType")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoCuenta + "'])[last()]")).click();
                        //TODO: Seleccionar - Moneda
                        Serenity.getDriver().findElement(By.id("currency")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - N° de cuenta para devolución
                        Serenity.getDriver().findElement(By.id("devolutionAccountNumber")).sendKeys("6465465456465");

                    }

                    //TODO: Seleccionar - Canal utilizado/reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueCanal + "'])[last()]")).click();
                }

            }
            if (tipoproducto.equals("CUENTA NEGOCIOS")) {
                if (tipologia.equals("COBROS INDEBIDOS")) {
                    //TODO: Ingresa - N° de CUENTA afecto
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).sendKeys("1231234567896");
                    try{Thread.sleep(1000);}catch (Exception e){}

                    ///TODO: Ingresa - Tipo de cuenta
                    Serenity.getDriver().findElement(By.id("accountType")).click();
                    Serenity.getDriver().findElement(By.xpath("(//li[@id=1])[last()]")).click();
                    ///TODO: Ingresa - moneda
                    Serenity.getDriver().findElement(By.id("currency")).click();
                    Serenity.getDriver().findElement(By.xpath("(//li[@id='USD'])[last()]")).click();

                    ///TODO: Ingresa - Num cuenta devolucion
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[2]")).sendKeys("1231234567896");
                    ///TODO: Ingresa - Canal
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.xpath("(//li[@id='App Interbank Empresas'])[last()]")).click();
                }
            }
            //TODO: SELECCIONA OPCION - PRODUCTOS
            if (tipoproducto.equals("CUENTA CORRIENTE")) {

                if (tipologia.equals("CANCELACIÓN DE CUENTA")) {
                    String valueMonto = Serenity.sessionVariableCalled("monto").toString();
                    String valueMoneda = Serenity.sessionVariableCalled("tipoMoneda").toString();


                    //TODO: Ingresa - N° de cuenta
                    Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("1126008996500000");
                    //TODO: Ingresa - Monto
                    Serenity.getDriver().findElement(By.id("amount")).sendKeys(valueMonto);
                    //TODO: Ingresa - Moneda
                    Serenity.getDriver().findElement(By.id("currency")).click();
                    Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueMoneda + "'])[last()]")).click();
                }

                if (tipologia.equals("COBROS INDEBIDOS")) {
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();

                    //TODO: Ingresa - N° de cuenta
                    Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("1126008996500000");
                    //TODO: Seleccionar - Canal utilizado/reclamado
                    Serenity.getDriver().findElement(By.id("channelClaimed")).click();
                    Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueCanal + "'])[last()]")).click();
                }
                if (tipologia.equals("DUPLICADO DE ESTADO DE CUENTA/SERVICIO DE COPIAS")) {
                    String valueNumCuenta = Serenity.sessionVariableCalled("numCuenta").toString();
                    String valueNumTarjeta = Serenity.sessionVariableCalled("numTarjeta").toString();
                    String valuePeriodoIni = Serenity.sessionVariableCalled("periodoIni").toString();
                    String valuePeriodoFin = Serenity.sessionVariableCalled("periodoFin").toString();
                    String valueRecojoTienda = Serenity.sessionVariableCalled("recojoTienda").toString();
                    String valueTienda = Serenity.sessionVariableCalled("Tienda").toString();
                    String valueMedioPago = Serenity.sessionVariableCalled("medioPago").toString();


                    //TODO: Ingresa - N° de cuenta
                    Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys(valueNumCuenta);
                    //TODO: Ingresa - N° de tarjeta
                    Serenity.getDriver().findElement(By.id("cardNumber")).sendKeys(valueNumTarjeta);
                    //TODO: Ingresa - Periodo Inicio
                    Serenity.getDriver().findElement(By.id("startPeriod")).sendKeys(valuePeriodoIni);
                    //TODO: Ingresa - Periodo Fin
                    Serenity.getDriver().findElement(By.id("endPeriod")).sendKeys(valuePeriodoFin);
                    //TODO: Selecciona radio button- Recojo Tienda
                    if(valueRecojoTienda.equals("Si")){
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagStore-1']")).click();

                        //TODO: Ingresa - Si se recojera en tienda ingresar Nombre Tienda
                        Serenity.getDriver().findElement(By.id("store")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTienda + "'])[last()]")).click();
                    }else if(valueRecojoTienda.equals("No")){
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagStore-0']")).click();
                    }

                    //TODO: Ingresa - Medio Pago
                    Serenity.getDriver().findElement(By.id("paymentMethod")).click();
                    Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueMedioPago + "'])[last()]")).click();
                }
                if (tipologia.equals("NO RECEPCIÓN DE DOCUMENTO")) {
                    String valueNumCuenta = Serenity.sessionVariableCalled("numCuenta").toString();
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    String valueTipoDocumento = Serenity.sessionVariableCalled("tipoDocumentoTramite").toString();

                    //TODO: Ingresa - N° de cuenta
                    Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys(valueNumCuenta);

                    //TODO: Selecciona - Tipo Documento
                    Serenity.getDriver().findElement(By.id("documentType")).click();
                    Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoDocumento + "'])")).click();

                    //TODO: Selecciona - Canal
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueCanal + "'])[last()]")).click();
                }
                if (tipologia.equals("EXONERACIÓN DE COBROS")) {
                    String valueNumCuenta = Serenity.sessionVariableCalled("numCuenta").toString();
                    //TODO: Ingresa - N° de cuenta
                    Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys(valueNumCuenta);
                }


            }
            //TODO: SELECCIONA OPCION - PRODUCTOS
            if (tipoproducto.equals("TARJETA DE DÉBITO")) {
                if (tipologia.equals("CONSUMOS NO RECONOCIDOS POR FRAUDE")) {
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    String valueCuenta = Serenity.sessionVariableCalled("numCuenta").toString();
                    String valueNumTarjeta = Serenity.sessionVariableCalled("numTarjeta").toString();
                    String valueCuentaMancomunada = Serenity.sessionVariableCalled("cuentaMancomunada").toString();
                    String valueQuePasoTarjeta = Serenity.sessionVariableCalled("quePasoConLaTarjeta").toString();
                    String valueFlagTarjetaBloqueo = Serenity.sessionVariableCalled("flagTarjetaBloqueo").toString();


                    //TODO: Ingresa - N° de cuenta
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).sendKeys(valueCuenta);
                    //TODO: Ingresa - N° de tarjeta
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[2]")).sendKeys(valueNumTarjeta);
                    //TODO: Selecciona - Es o no cuenta mancomunada
                    if(valueCuentaMancomunada.equals("Si")) {
                        Serenity.getDriver().findElement(By.xpath("//label[@for='isJointAccount-1']")).click();
                    }else if(valueCuentaMancomunada.equals("No")) {
                        Serenity.getDriver().findElement(By.xpath("//label[@for='isJointAccount-0']")).click();
                    }
                    //TODO: Selecciona - Que paso con la tarjeta
                    Serenity.getDriver().findElement(By.id("debitCardHappened")).click();
                    Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueQuePasoTarjeta + "'])[last()]")).click();

                    if(valueFlagTarjetaBloqueo.equals("1")){
                        Serenity.getDriver().findElement(By.xpath("//label[@for='checkLockDebitCard']")).click();
                    }else if(valueFlagTarjetaBloqueo.equals("0")){
                        String valueRazonNoBloqueo = Serenity.sessionVariableCalled("razonNoBloqueo").toString();
                        Serenity.getDriver().findElement(By.id("commentLockDebitCard")).sendKeys(valueRazonNoBloqueo);
                    }
                    //TODO: Seleccionar - Canal utilizado/reclamado
                    Serenity.getDriver().findElement(By.id("channelUsed")).click();
                    Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueCanal + "'])[last()]")).click();
                }
                if (tipologia.equals("CONSUMOS MAL PROCESADOS POR EL COMERCIO/MARCA")) {
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    String valueCuenta = Serenity.sessionVariableCalled("numCuenta").toString();
                    String valueNumTarjeta = Serenity.sessionVariableCalled("numTarjeta").toString();
                    String valueCuentaMancomunada = Serenity.sessionVariableCalled("cuentaMancomunada").toString();

                    //TODO: Ingresa - N° de cuenta
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='accountNumber'])")).sendKeys(valueCuenta);
                    //TODO: Ingresa - N° de tarjeta
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='cardNumber'])")).sendKeys(valueNumTarjeta);
                    //TODO: Selecciona - Es o no cuenta mancomunada
                    if(valueCuentaMancomunada.equals("Si")) {
                        Serenity.getDriver().findElement(By.xpath("//label[@for='isJointAccount-1']")).click();
                    }else if(valueCuentaMancomunada.equals("No")) {
                        Serenity.getDriver().findElement(By.xpath("//label[@for='isJointAccount-0']")).click();
                    }

                    //TODO: Seleccionar - Canal utilizado/reclamado
                    Serenity.getDriver().findElement(By.id("channelUsed")).click();
                    Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueCanal + "'])[last()]")).click();
                }
                if (tipologia.equals("CONSUMO/ RETIRO NO EFECTUADO")) {

                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    llenarDatosParticularesRezagadas();
                    //TODO: Seleccionar - Canal utilizado/reclamado
                    Serenity.getDriver().findElement(By.id("channelUsed")).click();
                    Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueCanal + "'])[last()]")).click();
                }
            }

            //TODO: SELECCIONA OPCION - PRODUCTOS
            if (tipoproducto.equals("SEGUROS RELACIONADOS")) {
                if (tipologia.equals("COBROS INDEBIDOS")) {

                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    llenarDatosParticularesRezagadas();
                    //TODO: Seleccionar - Canal utilizado/reclamado
                    Serenity.getDriver().findElement(By.id("channelUsed")).click();
                    Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueCanal + "'])[last()]")).click();
                }
                if (tipologia.equals("DESAFILIACIÓN DE SEGURO")) {
                    llenarDatosParticularesRezagadas();
                }
                if (tipologia.equals("DUPLICADO DE DOCUMENTO")) {
                    llenarDatosParticularesRezagadas();
                }
            }
            //TODO: SELECCIONA OPCION - PRODUCTOS
            if (tipoproducto.equals("SEGUROS")) {
                if (tipologia.equals("DUPLICADO DE DOCUMENTO")) {
                    llenarDatosParticularesRezagadas();
                }
            }
            //TODO: SELECCIONA OPCION - PRODUCTOS
            if (tipoproducto.equals("TARJETA DE CRÉDITO")) {
                if (tipologia.equals("CONSUMOS NO RECONOCIDOS POR FRAUDE")||tipologia.equals("CONSUMOS MAL PROCESADOS POR EL COMERCIO/MARCA")) {
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    llenarDatosParticularesRezagadas();
                    //TODO: Seleccionar - Canal utilizado/reclamado
                    Serenity.getDriver().findElement(By.id("channelUsed")).click();
                    Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueCanal + "'])[last()]")).click();
                }
            }



            //TODO: SELECCIONA OPCION - ATENCIÓN AL CLIENTE
            if (tipoproducto.equals("ATENCIÓN AL CLIENTE")) {

                if (tipologia.equals("DESACUERDO CON LA ATENCIÓN")) {
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Ingresa - Fecha del incidente
                    Serenity.getDriver().findElement(By.id("incidentDate")).sendKeys(getValueFecha());
                    //TODO: Seleccionar - Canal utilizado/reclamado
                    Serenity.getDriver().findElement(By.id("productTypeCompany")).click();
                    Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueCanal + "'])[last()]")).click();
                }
            }


        } else {
            //TODO: TIPOLOGIAS- RETAIL
            //TODO: SE AGREGA CAMPOS ADICIONALES DESPUES DE CERRAR MOTIVO
            //TODO: SELECCIONA OPCION - PRODUCTOS
            if (tipoproducto.equals("CUENTA")) {

                if (tipologia.equals("DUPLICADO DE ESTADO DE CUENTA/SERVICIO DE COPIAS")) {
                    //TODO: Ingresa - Periodo de inicio
                    Serenity.getDriver().findElement(By.id("periodStart")).sendKeys("20/05/2022");
                    //TODO: Ingresa - Periodo de fin
                    Serenity.getDriver().findElement(By.id("periodEnd")).sendKeys(getValueFecha());

                    String tienda = Serenity.sessionVariableCalled("Tienda").toString();
                    String autonomia = Serenity.sessionVariableCalled("Autonomia").toString();

                    //TODO: Selecciona - ¿Tienda?
                    if (tienda.equals("No")) {
                        Serenity.getDriver().findElement(By.xpath("//label[@for='store-0']")).click();
                    } else if (tienda.equals("Si")) {
                        Serenity.getDriver().findElement(By.xpath("//label[@for='store-1']")).click();
                    }

                    //TODO: Selecciona - ¿Aplica autonomía?
                    if (autonomia.equals("No")) {
                        Serenity.getDriver().findElement(By.xpath("//label[@for='autonomy-0']")).click();
                    } else if (autonomia.equals("Si")) {
                        Serenity.getDriver().findElement(By.xpath("//label[@for='autonomy-1']")).click();
                    }

                }

                if (tipologia.equals("SISTEMA DE RECOMPENSAS")) {
                    String tipoPrograma = Serenity.sessionVariableCalled("tipoPrograma").toString();
                    String Pcantidad = Serenity.sessionVariableCalled("cantidad").toString();
                    String Pimporte = Serenity.sessionVariableCalled("importe").toString();

                    //TODO: Ingresa datos dentro del motivo seleccionado
                    Serenity.getDriver().findElement(By.id("beneficio")).click();
                    Serenity.getDriver().findElement(By.id(tipoPrograma)).click();

                    if (tipoPrograma.equals("Descuento cuenta sueldo")) {
                        //TODO: Ingresa Importe
                        Serenity.getDriver().findElement(By.xpath("//*[@formcontrolname='importe']")).sendKeys(Pimporte);
                        //TODO: Ingresa fecha Operacion
                        Serenity.getDriver().findElement(By.xpath("//*[@formcontrolname='fechaOperacion']")).sendKeys(getValueFecha());
                        //TODO: Ingresa Establecimiento
                        Serenity.getDriver().findElement(By.xpath("//*[@formcontrolname='establecimiento']")).sendKeys("Establecimiento Test");
                    }

                    if (tipoPrograma.equals("Descuento en Cuotas") || tipoPrograma.equals("Efectivo")) {
                        Serenity.getDriver().findElement(By.xpath("//*[@formcontrolname='importe']")).sendKeys(Pimporte);
                    }

                    if (tipoPrograma.equals("Millas")) {
                        Serenity.getDriver().findElement(By.xpath("//*[@formcontrolname='cantidad']")).sendKeys(Pcantidad);
                    }

                    if (tipoPrograma.equals("Opciones sorteo millonario") || tipoPrograma.equals("Vales") || tipoPrograma.equals("Vales ruleta")) {
                        Serenity.getDriver().findElement(By.xpath("//*[@formcontrolname='cantidad']")).sendKeys(Pcantidad);
                        Serenity.getDriver().findElement(By.xpath("//*[@formcontrolname='importe']")).sendKeys(Pimporte);
                    }

                    if (!tipoproducto.equals("CUENTA")) {
                        Serenity.getDriver().findElement(By.xpath("//*[@formcontrolname='fechaOperacion']")).sendKeys(getValueFecha());
                    }

                }

                if (tipologia.equals("CANCELACIÓN DE CUENTA")) {
                    //TODO: Seleccionar - Moneda
                    Serenity.getDriver().findElement(By.id("currency")).click();
                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='currency']//li)[1]")).click();
                    //TODO: Ingresa - Monto
                    Serenity.getDriver().findElement(By.id("amount")).sendKeys("150");
                }

                if (tipologia.equals("DUPLICADO DE DOCUMENTO") || tipologia.equals("NO RECEPCIÓN DE DOCUMENTO")) {
                    String tipoDocumentoTramite = Serenity.sessionVariableCalled("tipoDocumentoTramite").toString();

                    //TODO: Seleccionar - Tipo de documento
                    Serenity.getDriver().findElement(By.id("documentType")).click();
                    Serenity.getDriver().findElement(By.id(tipoDocumentoTramite)).click();
                }

                if (tipologia.equals("ATM DE IB NO DEPOSITÓ DINERO")) {
                    String valueMedioAbono = Serenity.sessionVariableCalled("medioAbono").toString();
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Ingresa - Ubicación del cajero
                    Serenity.getDriver().findElement(By.id("locationCashier")).sendKeys("MIRAFLORES");
                    //TODO: Seleccionar - Medio de abono
                    Serenity.getDriver().findElement(By.id("paymentMedium")).click();
                    Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueMedioAbono + "'])[last()]")).click();
                    //TODO: Seleccionar - Canal utlizado / reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueCanal + "'])[last()]")).click();
                }

                if (tipologia.equals("ATENCIÓN IBK AGENTE")) {
                    //TODO: Ingresa - Ubicación del cajero
                    Serenity.getDriver().findElement(By.id("locationStore")).sendKeys("MIRAFLORES");
                }

                if (tipologia.equals("INCUMPLIMIENTO DE CAMPAÑA")) {
                    String valueBenefitClaimed = Serenity.sessionVariableCalled("beneficioReclamado").toString();
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();

                    //TODO: Seleccionar - Beneficio reclamado
                    Serenity.getDriver().findElement(By.id("benefitClaimed")).click();
                    Serenity.getDriver().findElement(By.id(valueBenefitClaimed)).click();

                    if (valueBenefitClaimed.equals("Cashback")) {
                        //TODO: Ingresa - Importe Reclamado
                        Serenity.getDriver().findElement(By.id("requestedAmount")).sendKeys("1500");
                    }

                    if (valueBenefitClaimed.equals("Millas")) {
                        //TODO: Ingresa Millas - Cantidad reclamada
                        Serenity.getDriver().findElement(By.id("claimedAmount")).sendKeys("500");
                    }

                    //TODO: Seleccionar - Canal utilizado/reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueCanal + "'])[last()]")).click();
                }

                if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA")) {
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Seleccionar - Canal utilizado/reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueCanal + "'])[last()]")).click();
                }

                if (tipologia.equals("TRASLADOS DE CTS A OTRO BANCO")) {
                    //TODO: Ingresa - Centro de trabajo
                    Serenity.getDriver().findElement(By.id("workPlace")).sendKeys("NTTDATA");
                    //TODO: Selecciona - Entidad financiera
                    Serenity.getDriver().findElement(By.id("financialEntity")).click();
                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='financialEntity']//li)[1]")).click();
                    //TODO: Ingresa - Teléfono fijo cliente
                    Serenity.getDriver().findElement(By.id("customerPhone")).sendKeys("945315131");
                    //TODO: Ingresa - Teléfono de trabajo
                    Serenity.getDriver().findElement(By.id("phoneWork")).sendKeys("945315132");
                    //TODO: Selecciona - Moneda
                    //Serenity.getDriver().findElement(By.id("moneda")).click();
                    //Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='moneda']//li)[1]")).click();
                    //TODO: Ingresa - Monto
                    Serenity.getDriver().findElement(By.id("amount")).sendKeys("2500");
                }

            }

            if (tipoproducto.equals("TARJETA DE CREDITO")) {

                if(tipologia.equals("CONSUMOS NO RECONOCIDOS POR FRAUDE"))
                {
                    String getMotivoSucedidoTarjeta = Serenity.sessionVariableCalled("getMotivoSucedidoTarjeta");
                    String flagTarjetaBloqueo = Serenity.sessionVariableCalled("flagTarjetaBloqueo");

                    //TODO: Selecciona - ¿Qué sucedió con la tarjeta de crédito?
                    Serenity.getDriver().findElement(By.id("cardFraudSituation")).click();
                    Serenity.getDriver().findElement(By.id(getMotivoSucedidoTarjeta)).click();

                    //TODO: Seleccionar - He bloqueado la tarjeta de crédito afectada por el consumo no reconocido por fraude.
                    if (flagTarjetaBloqueo.equals("Si")) {
                        System.out.println("SI SELECCIONO FLAG DE BLOQUEO: " + flagTarjetaBloqueo);
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagLockCard-0']")).click();
                    }

                    if (flagTarjetaBloqueo.equals("No")) {
                        System.out.println("NO SELECCIONO FLAG DE BLOQUEO: " + flagTarjetaBloqueo);

                        //TODO: ¿Por qué no se ha bloqueado la tarjeta?
                        Serenity.getDriver().findElement(By.xpath("//textarea[@formcontrolname='notLockCardComment']")).sendKeys("Prueba no se ha bloqueado la tarjeta");
                    }

                }
                if (tipologia.equals("PROGRAMA DE RECOMPENSAS")) {
                    String tipoPrograma = Serenity.sessionVariableCalled("tipoPrograma").toString();
                    String valueCantidad = Serenity.sessionVariableCalled("cantidad").toString();
                    String valueImporte = Serenity.sessionVariableCalled("importe").toString();

                    //TODO: Ingresa datos dentro del motivo seleccionado
                    Serenity.getDriver().findElement(By.id("beneficio")).click();
                    Serenity.getDriver().findElement(By.id(tipoPrograma)).click();

                    if (tipoPrograma.equals("Descuento cuenta sueldo")) {
                        //TODO: Ingresa Importe
                        Serenity.getDriver().findElement(By.xpath("//*[@formcontrolname='importe']")).sendKeys(valueImporte);
                        //TODO: Ingresa fecha Operacion
                        Serenity.getDriver().findElement(By.xpath("//*[@formcontrolname='fechaOperacion']")).sendKeys(getValueFecha());
                        //TODO: Ingresa Establecimiento
                        Serenity.getDriver().findElement(By.xpath("//*[@formcontrolname='establecimiento']")).sendKeys("Establecimiento Test");
                    }

                    if (tipoPrograma.equals("Descuento en Cuotas") || tipoPrograma.equals("Efectivo")) {
                        Serenity.getDriver().findElement(By.xpath("//*[@formcontrolname='importe']")).sendKeys(valueImporte);
                    }

                    if (tipoPrograma.equals("Millas")) {
                        Serenity.getDriver().findElement(By.xpath("//*[@formcontrolname='cantidad']")).sendKeys(valueCantidad);
                    }

                    if (tipoPrograma.equals("Opciones sorteo millonario") || tipoPrograma.equals("Vales") || tipoPrograma.equals("Vales ruleta")) {
                        Serenity.getDriver().findElement(By.xpath("//*[@formcontrolname='cantidad']")).sendKeys(valueCantidad);
                        Serenity.getDriver().findElement(By.xpath("//*[@formcontrolname='importe']")).sendKeys(valueImporte);
                    }

                    if (!tipoproducto.equals("CUENTA")) {
                        Serenity.getDriver().findElement(By.xpath("//*[@formcontrolname='fechaOperacion']")).sendKeys(getValueFecha());
                    }

                }

                if (tipologia.equals("PAGO ANTICIPADO Y ADELANTO DE CUOTAS")) {
                    String aplicaAbono = Serenity.sessionVariableCalled("aplicaAbono").toString();

                    if (!aplicaAbono.equals("No")) {
                        //TODO: Seleccionar - ¿Aplica devolución?
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagDerivation-1']")).click();
                        //TODO: Seleccionar - Medio de abono
                        Serenity.getDriver().findElement(By.id("depositType")).click();
                        Serenity.getDriver().findElement(By.id(aplicaAbono)).click();

                        try {
                            Thread.sleep(8000);
                        } catch (Exception e) {
                            System.out.println(e.getMessage());
                        }

                        if (aplicaAbono.equals("Abono en cuenta")) {
                            Serenity.getDriver().findElement(By.id("accountNumber")).click();
                            Serenity.getDriver().findElement(By.xpath("(//*[@id='accountNumber']//li)[1]")).click();
                        }
                    }

                }

                if (tipologia.equals("DEVOLUCION DE SALDO ACREEDOR")) {
                    String aplicaAbono = Serenity.sessionVariableCalled("aplicaAbono").toString();
                    String origenSaldo = Serenity.sessionVariableCalled("origenSaldo").toString();

                    try {
                        Thread.sleep(3000);
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    //TODO: Ingresa - Importe solicitado
                    Serenity.getDriver().findElement(By.id("requestedAmount")).sendKeys("100");
                    //TODO: Selecciona  - Medio de Abono
                    Serenity.getDriver().findElement(By.id("paymentMethod")).click();

                    if (aplicaAbono.equals("Tarjeta de Crédito")) {
                        Serenity.getDriver().findElement(By.id("CREDIT_CARD")).click();
                    }

                    if (aplicaAbono.equals("Orden de Pago")) {
                        Serenity.getDriver().findElement(By.id("PAY_ORDER")).click();
                    }

                    if (aplicaAbono.equals("Abono en Cuenta") || aplicaAbono.equals("Abono en cuenta")) {
                        Serenity.getDriver().findElement(By.id("PAYMENT_ACCOUNT")).click();
                        try {
                            Thread.sleep(3000);
                        } catch (Exception e) {
                            System.out.println(e.getMessage());
                        }
                        //TODO: Selecciona una cuenta
                        Serenity.getDriver().findElement(By.id("paymentAccount")).click();
                        Serenity.getDriver().findElement(By.xpath("(//*[@id='paymentAccount']//li)[1]")).click();
                    }

                    try {
                        Thread.sleep(2000);
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    validacionRegistroPage.seleccionarListadoOrigenSaldo(origenSaldo);

                }

                if (tipologia.equals("DUPLICADO DE ESTADO DE CUENTA/SERVICIO DE COPIAS")) {
                    //TODO: Ingresa - Periodo de inicio
                    Serenity.getDriver().findElement(By.id("periodStart")).sendKeys("20/05/2022");
                    //TODO: Ingresa - Periodo de fin
                    Serenity.getDriver().findElement(By.id("periodEnd")).sendKeys(getValueFecha());

                    String tienda = Serenity.sessionVariableCalled("Tienda").toString();
                    String autonomia = Serenity.sessionVariableCalled("Autonomia").toString();

                    //TODO: Selecciona - ¿Tienda?
                    if (tienda.equals("No")) {
                        Serenity.getDriver().findElement(By.xpath("//label[@for='store-0']")).click();
                    } else if (tienda.equals("Si")) {
                        Serenity.getDriver().findElement(By.xpath("//label[@for='store-1']")).click();
                    }

                    //TODO: Selecciona - ¿Aplica autonomía?
                    if (autonomia.equals("No")) {
                        Serenity.getDriver().findElement(By.xpath("//label[@for='autonomy-0']")).click();
                    } else if (autonomia.equals("Si")) {
                        Serenity.getDriver().findElement(By.xpath("//label[@for='autonomy-1']")).click();
                    }

                }

                if (tipologia.equals("SISTEMA DE RECOMPENSAS")) {
                    String tipoPrograma = Serenity.sessionVariableCalled("tipoPrograma").toString();
                    String Pcantidad = Serenity.sessionVariableCalled("cantidad").toString();
                    String Pimporte = Serenity.sessionVariableCalled("importe").toString();

                    //TODO: Ingresa datos dentro del motivo seleccionado
                    Serenity.getDriver().findElement(By.id("beneficio")).click();
                    Serenity.getDriver().findElement(By.id(tipoPrograma)).click();

                    if (tipoPrograma.equals("Descuento cuenta sueldo")) {
                        //TODO: Ingresa Importe
                        Serenity.getDriver().findElement(By.xpath("//*[@formcontrolname='importe']")).sendKeys(Pimporte);
                        //TODO: Ingresa fecha Operacion
                        Serenity.getDriver().findElement(By.xpath("//*[@formcontrolname='fechaOperacion']")).sendKeys(getValueFecha());
                        //TODO: Ingresa Establecimiento
                        Serenity.getDriver().findElement(By.xpath("//*[@formcontrolname='establecimiento']")).sendKeys("Establecimiento Test");
                    }

                    if (tipoPrograma.equals("Descuento en Cuotas") || tipoPrograma.equals("Efectivo")) {
                        Serenity.getDriver().findElement(By.xpath("//*[@formcontrolname='importe']")).sendKeys(Pimporte);
                    }

                    if (tipoPrograma.equals("Millas")) {
                        Serenity.getDriver().findElement(By.xpath("//*[@formcontrolname='cantidad']")).sendKeys(Pcantidad);
                    }

                    if (tipoPrograma.equals("Opciones sorteo millonario") || tipoPrograma.equals("Vales") || tipoPrograma.equals("Vales ruleta")) {
                        Serenity.getDriver().findElement(By.xpath("//*[@formcontrolname='cantidad']")).sendKeys(Pcantidad);
                        Serenity.getDriver().findElement(By.xpath("//*[@formcontrolname='importe']")).sendKeys(Pimporte);
                    }

                    if (!tipoproducto.equals("CUENTA")) {
                        Serenity.getDriver().findElement(By.xpath("//*[@formcontrolname='fechaOperacion']")).sendKeys(getValueFecha());
                    }

                }

                if (tipologia.equals("ATENCIÓN POR PRENVENCIÓN Y FRAUDES")) {
                    String operacionReconocida = Serenity.sessionVariableCalled("operacionReconocida").toString();
                    String medioComunicacion = Serenity.sessionVariableCalled("medioComunicacion").toString();
                    String flagTarjetaBloqueo = Serenity.sessionVariableCalled("flagTarjetaBloqueo").toString();

                    //TODO: Seleccionar - ¿Reconoces la operación?
                    if (operacionReconocida.equals("No")) {
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagAcknowledgeOperation-0']")).click();
                    } else if (operacionReconocida.equals("Si")) {
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagAcknowledgeOperation-1']")).click();
                    }

                    //TODO: Selecciona - Medio de comunicación
                    Serenity.getDriver().findElement(By.id("mediaChannel")).click();
                    Serenity.getDriver().findElement(By.id(medioComunicacion)).click();

                    //TODO: Selecciona - He bloqueado la tarjeta de crédito
                    if (flagTarjetaBloqueo.equals("Si")) {
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagLockCard-']")).click();
                        System.out.println("SI SELECCIONA FLAG TARJETA BLOQUEO ES: " + flagTarjetaBloqueo);
                    } else if (flagTarjetaBloqueo.equals("No")) {
                        System.out.println("NO SELECCIONA FLAG TARJETA BLOQUEO ES: " + flagTarjetaBloqueo);
                    }

                }

                if (tipologia.equals("BAJA DE TASA")) {
                    //TODO: Ingresar Tasa solicitada %
                    Serenity.getDriver().findElement(By.id("rate")).click();
                    Serenity.getDriver().findElement(By.id("rate")).sendKeys("5.00");
                }

                if (tipologia.equals("MODIFICACIÓN DE GRUPO DE LIQUIDACIÓN")) {
                    String GrupoLiquidacionActual = Serenity.sessionVariableCalled("GrupoLiquidacionActual").toString();
                    String GrupoLiquidacionSolicitado = Serenity.sessionVariableCalled("GrupoLiquidacionSolicitado").toString();

                    //TODO: Seleccionar: Grupo de liquidación actual
                    Serenity.getDriver().findElement(By.id("currentLiquidationGroup")).click();
                    Serenity.getDriver().findElement(By.id(GrupoLiquidacionActual)).click();
                    //TODO: Seleccionar: Grupo de liquidación solicitado
                    Serenity.getDriver().findElement(By.id("requestedLiquidationGroup")).click();
                    Serenity.getDriver().findElement(By.xpath("(//*[@id='" + GrupoLiquidacionSolicitado + "'])[last()]")).click();
                }

                if (tipologia.equals("MODIFICACIÓN LÍNEA TC ADICIONAL")) {
                    //TODO: Seleccionar - Tarjeta de crédito
                    Serenity.getDriver().findElement(By.id("creditCard_0")).click();
                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='creditCard_0']//li)[1]")).click();
                    //TODO: Seleccionar tipo Moneda
                    String MonedaTramite = Serenity.sessionVariableCalled("tipoMoneda").toString();
                    Serenity.getDriver().findElement(By.id("currency_0")).click();
                    Serenity.getDriver().findElement(By.id(MonedaTramite)).click();
                    //TODO: Ingresar monto
                    String monto = Serenity.sessionVariableCalled("monto").toString();
                    Serenity.getDriver().findElement(By.xpath("//input[@formcontrolname='amount']")).sendKeys(monto);
                }

                if (tipologia.equals("REDUCCIÓN DE LÍNEA TC")) {
                    String MonedaTramite = Serenity.sessionVariableCalled("MonedaTramite").toString();
                    String LineaActual = Serenity.sessionVariableCalled("LineaActual").toString();
                    String LineaNueva = Serenity.sessionVariableCalled("LineaNueva").toString();

                    //TODO: Seleccionar tipo Moneda
                    Serenity.getDriver().findElement(By.id("select-default")).click();
                    Serenity.getDriver().findElement(By.id(MonedaTramite)).click();
                    //TODO: Ingresar Línea actual y Línea solicitada (Menor a la línea actual)
                    Serenity.getDriver().findElement(By.xpath("//input[@formcontrolname='currentLine']")).sendKeys(LineaActual);
                    Serenity.getDriver().findElement(By.xpath("//input[@formcontrolname='requestedLine']")).sendKeys(LineaNueva);
                }

                if (tipologia.equals("TRASLADO DE SALDO ACREEDOR")) {
                    //TODO: Seleccionar tipo Moneda
                    String MonedaTramite = Serenity.sessionVariableCalled("MonedaTramite").toString();
                    Serenity.getDriver().findElement(By.id("currency")).click();
                    Serenity.getDriver().findElement(By.id(MonedaTramite)).click();
                    //TODO: Ingresar Importe solicitado
                    String importeSolicitado = Serenity.sessionVariableCalled("ImporteSolicitado").toString();
                    Serenity.getDriver().findElement(By.id("requestedAmount")).sendKeys(importeSolicitado);
                }

                if (tipologia.equals("AFILIACIÓN / DESAFILIACIÓN DÉBITO AUTOMÁTICO")) {
                    String nombreMotivo = Serenity.sessionVariableCalled("NombreMotivo").toString();

                    if (nombreMotivo.equals("Afiliación pago mínimo") || nombreMotivo.equals("Afiliación pago mes")) {
                        //TODO: Selecciona Cuenta Soles
                        Serenity.getDriver().findElement(By.id("accountNumberPEN")).click();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='accountNumberPEN']/div[2]/ul/li[1]")).click();
                        //TODO: Selecciona Cuenta Dolares
                        Serenity.getDriver().findElement(By.id("accountNumberUSD")).click();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='accountNumberUSD']/div[2]/ul/li[1]")).click();
                    }

                    if (nombreMotivo.equals("Modificación cuenta / modalidad")) {
                        //TODO: Selecciona Tipo de pago
                        Serenity.getDriver().findElement(By.id("tipoPago")).click();
                        Serenity.getDriver().findElement(By.id("Pago mes")).click();

                        //TODO: Selecciona Cuenta Soles
                        Serenity.getDriver().findElement(By.id("accountNumberPEN")).click();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='accountNumberPEN']/div[2]/ul/li[1]")).click();
                        //TODO: Selecciona Cuenta Dolares
                        Serenity.getDriver().findElement(By.id("accountNumberUSD")).click();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='accountNumberUSD']/div[2]/ul/li[1]")).click();
                    }

                    if (nombreMotivo.equals("Desafiliación")) {
                        System.out.println("ENTRO CON EXITO: " + nombreMotivo);
                    }

                }

                if (tipologia.equals("REGRABACIÓN DE PLÁSTICO")) {
                    //TODO: Ingresa Nombre reducido
                    Serenity.getDriver().findElement(By.id("reducedName")).sendKeys("EARIAS");
                }

                if (tipologia.equals("NO RECEPCIÓN DE DOCUMENTO")) {
                    String tipoDocumentoTramite = Serenity.sessionVariableCalled("tipoDocumentoTramite").toString();
                    String tipoEnvio = Serenity.sessionVariableCalled("tipoEnvio").toString();

                    //TODO: Seleccionar - Tipo de documento
                    Serenity.getDriver().findElement(By.id("documentType")).click();
                    Serenity.getDriver().findElement(By.id(tipoDocumentoTramite)).click();
                    //TODO: Seleccionar - Tipo de envio
                    Serenity.getDriver().findElement(By.id("shipmentType")).click();
                    Serenity.getDriver().findElement(By.id(tipoEnvio)).click();
                }

                if (tipologia.equals("DESACUERDO DE CONDICIONES/TASAS/TARIFAS") || tipologia.equals("REPORTE INDEBIDO CENTRALES DE RIESGO")) {
                    String canal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Seleccionar - Canal utlizado / reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(canal)).click();
                }

                if (tipologia.equals("EMISIÓN DE TARJETA")) {
                    String CodigoMarca = Serenity.sessionVariableCalled("CodigoMarca").toString();
                    String IndicadorTipo = Serenity.sessionVariableCalled("IndicadorTipo").toString();

                    //TODO: Seleccionar Código de marca
                    Serenity.getDriver().findElement(By.id("select-default")).click();
                    Serenity.getDriver().findElement(By.id(CodigoMarca)).click();
                    //TODO: Indicador de tipo
                    Serenity.getDriver().findElement(By.id("type")).click();
                    Serenity.getDriver().findElement(By.id(IndicadorTipo)).click();
                }

                if (tipologia.equals("UNIFICACIÓN DE MARCAS/TIPOS DE TARJETAS")) {
                    //TODO: Ingresa - Línea solicitada
                    Serenity.getDriver().findElement(By.id("requestedLine")).sendKeys("1500");
                    //TODO: Seleccionar - Tarjeta a cancelar
                    Serenity.getDriver().findElement(By.id("cancelingCard")).click();
                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='cancelingCard']//li)[1]")).click();
                }

                if (tipologia.equals("CONSTANCIA DE BLOQUEO DE TARJETA")) {
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();

                    //TODO: Ingresa - N° de bloqueo
                    Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("11520532");
                    //TODO: Ingresa - Hora de bloqueo
                    Serenity.getDriver().findElement(By.id("lockTime")).sendKeys("0820");
                    //TODO: Ingresa - Fecha
                    Serenity.getDriver().findElement(By.id("lockDate")).sendKeys(getValueFecha());
                    //TODO: Seleccionar - Canal utlizado / reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueCanal + "'])[last()]")).click();
                }

                if (tipologia.equals("ATM DE IB NO DEPOSITÓ DINERO")) {
                    String valueMedioAbono = Serenity.sessionVariableCalled("medioAbono").toString();
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Ingresa - Ubicación del cajero
                    Serenity.getDriver().findElement(By.id("locationCashier")).sendKeys("MIRAFLORES");
                    //TODO: Seleccionar - Medio de abono
                    Serenity.getDriver().findElement(By.id("paymentMedium")).click();
                    Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueMedioAbono + "'])[last()]")).click();
                    //TODO: Seleccionar - Canal utlizado / reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueCanal + "'])[last()]")).click();

                }

                if (tipologia.equals("ATM DE IB NO ENTREGÓ DINERO")) {
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Ingresa - Ubicación del cajero
                    Serenity.getDriver().findElement(By.id("locationCashier")).sendKeys("MIRAFLORES");
                    //TODO: Seleccionar - Canal utlizado / reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueCanal + "'])[last()]")).click();
                }

                if (tipologia.equals("REPROGRAMACIÓN DE DEUDA")) {
                    String valueReducirLineaCredito = Serenity.sessionVariableCalled("reducirLineaCredito").toString();
                    //TODO: Seleccionar - Reducir línea de crédito
                    if (valueReducirLineaCredito.equals("Si")) {
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagLowerCreditLine-1']")).click();
                    }

                    if (valueReducirLineaCredito.equals("No")) {
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagLowerCreditLine-0']")).click();
                    }

                    //TODO: Ingresa - Monto Tot. S/. Referencial
                    Serenity.getDriver().findElement(By.id("referenceAmount")).sendKeys("1200");
                    //TODO: Ingresa - N° de cuotas solicitadas
                    Serenity.getDriver().findElement(By.id("requestedFee")).sendKeys("1");
                    //TODO: Ingresa - Tasa solicitada %
                    Serenity.getDriver().findElement(By.id("requestedRate")).sendKeys("5");
                    //TODO: Ingresa - Tasa solicitada %
                    Serenity.getDriver().findElement(By.id("gracePeriod")).sendKeys("2");
                }

                if (tipologia.equals("COMPRA DE DEUDA INTERNA")) {
                    String compraDeuda = Serenity.sessionVariableCalled("compraDeuda").toString();

                    //TODO: Ingresa - Número Tarjeta de crédito
                    Serenity.getDriver().findElement(By.xpath("(//div[@id='creditCard_0'])[1]")).click();
                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='creditCard_0']//li)[1]")).click();
                    //TODO: Selecciona - Moneda
                    Serenity.getDriver().findElement(By.xpath("(//div[@id='currency_0'])[1]")).click();
                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='currency_0']//li)[1]")).click();
                    //TODO: Ingresa - Monto
                    Serenity.getDriver().findElement(By.xpath("(//input[@formcontrolname='amount'])[last()]")).sendKeys("150");

                    //TODO: Ingresa - Plazo
                    Serenity.getDriver().findElement(By.id("term")).sendKeys("12");
                    //TODO: Selecciona - Moneda Compra Deuda
                    Serenity.getDriver().findElement(By.id("purchaseDebtCurrency")).click();
                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='purchaseDebtCurrency']//li)[1]")).click();
                    //TODO: Ingresa - Tipo Compra Deuda
                    Serenity.getDriver().findElement(By.id("purchaseDebtType")).click();
                    Serenity.getDriver().findElement(By.id(compraDeuda)).click();
                    //TODO: Ingresa - Tasa
                    Serenity.getDriver().findElement(By.id("rate")).sendKeys("6");
                    //TODO: Ingresa - Periodo de Gracia
                    Serenity.getDriver().findElement(By.id("gracePeriod")).sendKeys("20");
                }

                if (tipologia.equals("REPROGRAMACIÓN DE DEUDA TC CON CAMPAÑA")) {
                    String valueReducirLineaCredito = Serenity.sessionVariableCalled("reducirLineaCredito").toString();
                    //TODO: Seleccionar - Reducir línea de crédito
                    if (valueReducirLineaCredito.equals("Si")) {
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagLineOfCredit-1']")).click();
                    }

                    if (valueReducirLineaCredito.equals("No")) {
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagLineOfCredit-0']")).click();
                    }

                    //TODO: Ingresa - Monto Tot. S/. Referencial
                    Serenity.getDriver().findElement(By.id("amount")).sendKeys("1200");
                    //TODO: Ingresa - N° de cuotas solicitadas
                    Serenity.getDriver().findElement(By.id("requestedQuotas")).sendKeys("1");
                    //TODO: Ingresa - Tasa solicitada %
                    Serenity.getDriver().findElement(By.id("requestedRate")).sendKeys("5");
                    //TODO: Ingresa - Tasa solicitada %
                    Serenity.getDriver().findElement(By.id("gracePeriod")).sendKeys("2");
                }

                if (tipologia.equals("ENDOSO SEGUROS DE DESGRAVAMEN")) {
                    //TODO: Ingresa - Monto endoso
                    Serenity.getDriver().findElement(By.id("amount")).sendKeys("1000");
                    //TODO: Ingresa - N° de póliza
                    Serenity.getDriver().findElement(By.id("policyNumber")).sendKeys("123456");
                    //TODO: Ingresa - Cía. de seguros
                    Serenity.getDriver().findElement(By.id("insuranceCia")).sendKeys("123123");
                    //TODO: Seleccione - Moneda póliza
                    Serenity.getDriver().findElement(By.id("policyCurrency")).click();
                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='policyCurrency']//li)[1]")).click();
                }

                if (tipologia.equals("MODIFICACIÓN DISPOSICIÓN DE EFECTIVO")) {
                    String agregarTarjetaCredito = Serenity.sessionVariableCalled("agregarTarjetaCredito").toString();
                    int addCreditCard = Integer.parseInt(agregarTarjetaCredito);
                    for (int j = 0; j < addCreditCard; j++) {
                        //TODO: Ingresa - A formulario Tarjetas de crédito deuda
                        //TODO: Selecciona - Tarjeta de Crédito
                        Serenity.getDriver().findElement(By.id("creditCard_" + j + "")).click();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='creditCard_" + j + "']/div[2]/ul/li[" + (j + 1) + "]")).click();
                        //TODO: Selecciona -  Moneda
                        Serenity.getDriver().findElement(By.id("currency_" + j + "")).click();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='currency_" + j + "']/div[2]/ul/li[1]")).click();
                        //TODO: Ingresa - Monto Solicitado
                        Serenity.getDriver().findElement(By.xpath("(//input[@formcontrolname='amount'])[last()]")).sendKeys("555");

                        if (j != addCreditCard - 1) {
                            Serenity.getDriver().findElement(By.id("Add_Amount")).click();
                        }

                    }
                }

                if (tipologia.equals("REVERSIÓN DE UPGRADE")) {
                    //TODO: Ingresa N° Tarjeta Actual
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).sendKeys("123123123123123");
                    //TODO: Ingresa N° Tarjeta Anterior
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[last()]")).sendKeys("123123123123122");
                }

                if (tipologia.equals("DUPLICADO DE DOCUMENTO") || tipologia.equals("DEVOLUCION DE DOCUMENTOS")) {
                    String tipoDocumentoTramite = Serenity.sessionVariableCalled("tipoDocumentoTramite").toString();
                    //TODO: Seleccionar - Tipo de documento
                    Serenity.getDriver().findElement(By.id("documentType")).click();
                    Serenity.getDriver().findElement(By.id(tipoDocumentoTramite)).click();
                }

                if (tipologia.equals("ATENCIÓN IBK AGENTE")) {
                    //TODO: Ingresa - Ubicación del cajero
                    Serenity.getDriver().findElement(By.id("locationStore")).sendKeys("MIRAFLORES");
                }

                if (tipologia.equals("COMPRA DE DEUDA SIN AMPLIACIÓN")) {
                    String agregarTarjetaCredito = Serenity.sessionVariableCalled("agregarTarjetaCredito").toString();
                    int addCreditCard = Integer.parseInt(agregarTarjetaCredito);
                    for (int j = 0; j < addCreditCard; j++) {
                        //TODO: Ingresa - A formulario Tarjetas de crédito deuda
                        //TODO: Ingresa - Tarjeta de crédito
                        Serenity.getDriver().findElement(By.xpath("(//input[@formcontrolname='creditCard'])[last()]")).sendKeys("123123123123123");
                        //TODO: Selecciona -  Moneda
                        Serenity.getDriver().findElement(By.id("currency_" + j + "")).click();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='currency_" + j + "']/div[2]/ul/li[1]")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@formcontrolname='amount'])[last()]")).sendKeys("500");
                        //TODO: Ingresa - Entidad Financiera
                        Serenity.getDriver().findElement(By.xpath("(//input[@formcontrolname='bank'])[last()]")).sendKeys("INTERBANK");

                        if (j != addCreditCard - 1) {
                            //TODO: Selecciona - Agregar tarjeta de crédito deuda
                            Serenity.getDriver().findElement(By.id("Add_Amount")).click();
                        }

                    }

                    //TODO: Ingresa - Monto Total Referencial S/.
                    Serenity.getDriver().findElement(By.id("referencialAmount")).sendKeys("10000");
                    //TODO: Ingresa - Plazo
                    Serenity.getDriver().findElement(By.id("term")).sendKeys("10");
                    //TODO: Ingresa - Afectación tasa resolvente
                    Serenity.getDriver().findElement(By.id("revolvingRateAffectation")).sendKeys("654321");
                    //TODO: Ingresa - Tasa
                    Serenity.getDriver().findElement(By.id("rate")).sendKeys("15");
                    //TODO: Ingresa - Periodo de Gracia
                    Serenity.getDriver().findElement(By.id("gracePeriod")).sendKeys("2");
                    //TODO: Selecciona Autonomía - En este caso "NO"
                    Serenity.getDriver().findElement(By.xpath("//label[@for='No-0']")).click();
                }

                if (tipologia.equals("MODIFICACIÓN DE TASA COMPRA DE DEUDA TC")) {
                    //TODO: Ingresa Monto
                    Serenity.getDriver().findElement(By.id("amount")).sendKeys("1000");
                    //TODO: Ingresa Tasa Solicitada
                    Serenity.getDriver().findElement(By.id("requestedRate")).sendKeys("15");
                    //TODO: Ingresa Tasa Actual
                    Serenity.getDriver().findElement(By.id("currentRate")).sendKeys("10");
                }

                if (tipologia.equals("CORRECCIÓN DE PAGO REALIZADO")) {
                    String canal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Ingresa Canal Utilizado
                    Serenity.getDriver().findElement(By.id("channelUsed")).click();
                    Serenity.getDriver().findElement(By.id(canal)).click();
                }

                if (tipologia.equals("REGULARIZACIÓN DE COMPRA DE DEUDA TC")) {
                    //TODO: Ingresa - Número Tarjeta de crédito
                    Serenity.getDriver().findElement(By.xpath("(//input[@formcontrolname='creditCard'])[last()]")).sendKeys("4213555322311541");
                    //TODO: Selecciona - Moneda
                    Serenity.getDriver().findElement(By.xpath("(//div[@id='currency_0'])[1]")).click();
                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='currency_0']//li)[1]")).click();
                    //TODO: Ingresa - Monto
                    Serenity.getDriver().findElement(By.xpath("(//input[@formcontrolname='amount'])[last()]")).sendKeys("150");
                    //TODO: Ingresa - Entidad Financiera
                    Serenity.getDriver().findElement(By.xpath("(//input[@formcontrolname='bank'])[last()]")).sendKeys("INTERBANK");
                    //TODO: Ingresa - Monto Total Referencial S/.
                    Serenity.getDriver().findElement(By.id("referencialAmount")).sendKeys("180");
                }

                if (tipologia.equals("CAMBIO DE FORMA DE ENVÍO DE CORRESPONDENCIA")) {
                    String valueTipoEnvio = Serenity.sessionVariableCalled("tipoEnvio").toString();

                    //TODO: Selecciona - Tipo de envio
                    Serenity.getDriver().findElement(By.id("deliverType")).click();
                    Serenity.getDriver().findElement(By.xpath("//*[contains(text(), '" + valueTipoEnvio + "')][last()]")).click();

                    if (valueTipoEnvio.equals("Envío físico")) {
                        //TODO: Selecciona - Dirección de envio
                        Serenity.getDriver().findElement(By.id("deliverAddress")).click();
                        Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='deliverAddress']//li)[1]")).click();
                    }

                    if (valueTipoEnvio.equals("Envío por mail")) {
                        //TODO: Selecciona - Email de envio
                        Serenity.getDriver().findElement(By.id("deliverEmail")).click();
                        Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='deliverEmail']//li)[1]")).click();
                    }

                    if (valueTipoEnvio.equals("Envío físico y por mail")) {
                        //TODO: Selecciona - Dirección de envio
                        Serenity.getDriver().findElement(By.id("deliverAddress")).click();
                        Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='deliverAddress']//li)[1]")).click();
                        //TODO: Selecciona - Email de envio
                        Serenity.getDriver().findElement(By.id("deliverEmail")).click();
                        Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='deliverEmail']//li)[1]")).click();
                    }

                    //TODO: Selecciona - Forma de envio
                    Serenity.getDriver().findElement(By.id("deliverForm")).click();
                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='deliverForm']//li)[1]")).click();
                }

                if (tipologia.equals("INCUMPLIMIENTO DE CAMPAÑA")) {
                    String valueBenefitClaimed = Serenity.sessionVariableCalled("beneficioReclamado").toString();
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();

                    //TODO: Seleccionar - Beneficio reclamado
                    Serenity.getDriver().findElement(By.id("benefitClaimed")).click();
                    Serenity.getDriver().findElement(By.id(valueBenefitClaimed)).click();

                    if (valueBenefitClaimed.equals("Cashback")) {
                        //TODO: Ingresa - Importe Reclamado
                        Serenity.getDriver().findElement(By.id("requestedAmount")).sendKeys("1500");
                    }

                    if (valueBenefitClaimed.equals("Millas")) {
                        //TODO: Ingresa Millas - Cantidad reclamada
                        Serenity.getDriver().findElement(By.id("claimedAmount")).sendKeys("500");
                    }

                    //TODO: Seleccionar - Canal utilizado/reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueCanal + "'])[last()]")).click();
                }

                if (tipologia.equals("ABONO DE MILLAS")) {
                    //TODO: Ingresa - Cantidad de millas
                    Serenity.getDriver().findElement(By.id("milesAmount")).sendKeys("1500");
                }

                if (tipologia.equals("CAMBIO DE TARJETA")) {
                    String valueLugarEntrega = Serenity.sessionVariableCalled("lugarEntrega").toString();

                    //TODO: Selecciona - Marcatipo destino
                    Serenity.getDriver().findElement(By.xpath("(//div[@id='select-default'])[1]")).click();
                    Serenity.getDriver().findElement(By.xpath("((//app-ibk-select)[4]//div[@id='select-default'])//li[1]")).click();
                    //TODO: Selecciona - Tipo cliente
                    Serenity.getDriver().findElement(By.xpath("(//div[@id='select-default'])[2]")).click();
                    Serenity.getDriver().findElement(By.xpath("((//app-ibk-select)[5]//div[@id='select-default'])//li[1]")).click();

                    if (valueLugarEntrega.equals("Tienda")) {
                        //TODO: Selecciona radio button - Tienda
                        Serenity.getDriver().findElement(By.xpath("//label[@for='placeDelivery-1|Tienda']")).click();
                        //TODO: Selecciona combo - Tienda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='select-default'])[3]")).click();
                        Serenity.getDriver().findElement(By.xpath("((//app-ibk-select)[6]//div[@id='select-default'])//li[1]")).click();
                    }

                    if (valueLugarEntrega.equals("Dirección exacta")) {
                        //TODO: Selecciona radio button - Dirección exacta
                        Serenity.getDriver().findElement(By.xpath("//label[@for='placeDelivery-0|Dirección exacta']")).click();
                        //TODO: Selecciona combo - Dirección exacta
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='select-default'])[3]")).click();
                        Serenity.getDriver().findElement(By.xpath("((//app-ibk-select)[6]//div[@id='select-default'])//li[1]")).click();
                    }

                    //TODO: Ingresa - Fecha de entrega
                    Serenity.getDriver().findElement(By.id("deliverDate")).sendKeys(getValueFecha());
                    //TODO: Selecciona combo - Horario de entrega
                    Serenity.getDriver().findElement(By.xpath("(//div[@id='select-default'])[4]")).click();
                    Serenity.getDriver().findElement(By.xpath("((//app-ibk-select)[7]//div[@id='select-default'])//li[1]")).click();
                }

                if (tipologia.equals("ATM DE OTRO BANCO NO ENTREGÓ DINERO")) {
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Ingresa - Ubicación del cajero
                    Serenity.getDriver().findElement(By.id("locationAgent")).sendKeys("MIRAFLORES");
                    //TODO: Selecciona - Canal utilizado/reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

                if (tipologia.equals("CLIENTE VIAJERO")) {
                    //TODO: Ingresa - Fecha de salida
                    Serenity.getDriver().findElement(By.id("departureDate")).sendKeys(getValueFecha());
                    //TODO: Ingresa - Fecha de retorno
                    Serenity.getDriver().findElement(By.id("returnDate")).sendKeys(getValueFecha());
                    //TODO: Ingresa - Ingresa los países a los que viajará
                    Serenity.getDriver().findElement(By.id("search")).sendKeys("PARAGUAY");
                    Serenity.getDriver().findElement(By.xpath("//li[contains(text(),'PARAGUAY')]")).click();
                    //TODO: Ingresa - Correo electrónico
                    Serenity.getDriver().findElement(By.xpath("//input[@id='email']")).sendKeys("alexisce004@GMAIL.COM");
                    //TODO: Ingresa - Celular
                    Serenity.getDriver().findElement(By.xpath("//input[@id='telephone']")).sendKeys("959149978");
                }

            }

            if (tipoproducto.equals("TARJETA DE DEBITO")) {

                if(tipologia.equals("CONSUMOS NO RECONOCIDOS POR FRAUDE"))
                {
                    String getMotivoSucedidoTarjeta = Serenity.sessionVariableCalled("getMotivoSucedidoTarjeta");
                    String flagTarjetaBloqueo = Serenity.sessionVariableCalled("flagTarjetaBloqueo");

                    //TODO: Selecciona - ¿Qué sucedió con la tarjeta de crédito?
                    Serenity.getDriver().findElement(By.id("cardFraudSituation")).click();
                    Serenity.getDriver().findElement(By.id(getMotivoSucedidoTarjeta)).click();

                    //TODO: Seleccionar - He bloqueado la tarjeta de crédito afectada por el consumo no reconocido por fraude.
                    if (flagTarjetaBloqueo.equals("Si")) {
                        System.out.println("SI SELECCIONO FLAG DE BLOQUEO: " + flagTarjetaBloqueo);
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagLockCard-0']")).click();
                    }

                    if (flagTarjetaBloqueo.equals("No")) {
                        System.out.println("NO SELECCIONO FLAG DE BLOQUEO: " + flagTarjetaBloqueo);

                        //TODO: ¿Por qué no se ha bloqueado la tarjeta?
                        Serenity.getDriver().findElement(By.xpath("//textarea[@formcontrolname='notLockCardComment']")).sendKeys("Prueba no se ha bloqueado la tarjeta");
                    }

                }
                if (tipologia.equals("ATENCIÓN POR PRENVENCIÓN Y FRAUDES")) {
                    String operacionReconocida = Serenity.sessionVariableCalled("operacionReconocida").toString();
                    String medioComunicacion = Serenity.sessionVariableCalled("medioComunicacion").toString();
                    String flagTarjetaBloqueo = Serenity.sessionVariableCalled("flagTarjetaBloqueo").toString();

                    //TODO: Seleccionar - ¿Reconoces la operación?
                    if (operacionReconocida.equals("No")) {
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagAcknowledgeOperation-0']")).click();
                    } else if (operacionReconocida.equals("Si")) {
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagAcknowledgeOperation-1']")).click();
                    }

                    //TODO: Selecciona - Medio de comunicación
                    Serenity.getDriver().findElement(By.id("mediaChannel")).click();
                    Serenity.getDriver().findElement(By.id(medioComunicacion)).click();
                    //TODO: Selecciona - He bloqueado la tarjeta de crédito
                    if (flagTarjetaBloqueo.equals("Si")) {
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagLockCard-']")).click();
                        System.out.println("SI SELECCIONA FLAG TARJETA BLOQUEO ES: " + flagTarjetaBloqueo);
                    } else if (flagTarjetaBloqueo.equals("No")) {
                        System.out.println("NO SELECCIONA FLAG TARJETA BLOQUEO ES: " + flagTarjetaBloqueo);
                    }

                }

                if (tipologia.equals("ATM DE IB NO ENTREGÓ DINERO") || tipologia.equals("ATM DE OTRO BANCO NO ENTREGÓ DINERO")) {
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Ingresa - Ubicación del cajero
                    Serenity.getDriver().findElement(By.id("locationCashier")).sendKeys("MIRAFLORES");
                    //TODO: Seleccionar - Canal utlizado / reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueCanal + "'])[last()]")).click();
                }

                if (tipologia.equals("PROGRAMA DE RECOMPENSAS")) {
                    //TODO: Ingresa - Fecha de operación
                    Serenity.getDriver().findElement(By.id("operationDate")).sendKeys(getValueFecha());
                }

                if (tipologia.equals("REVISIÓN DE DEPÓSITOS A TERCEROS")) {
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Seleccionar - Canal utlizado / reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueCanal + "'])[last()]")).click();
                }

                if (tipologia.equals("ABONO DE MILLAS")) {
                    //TODO: Ingresa - Cantidad de millas
                    Serenity.getDriver().findElement(By.id("milesAmount")).sendKeys("1400");
                }

                if (tipologia.equals("CLIENTE VIAJERO")) {
                    //TODO: Ingresa - Fecha de salida
                    Serenity.getDriver().findElement(By.id("departureDate")).sendKeys(getValueFecha());
                    //TODO: Ingresa - Fecha de retorno
                    Serenity.getDriver().findElement(By.id("returnDate")).sendKeys(getValueFecha());
                    //TODO: Ingresa - Ingresa los países a los que viajará
                    Serenity.getDriver().findElement(By.id("search")).sendKeys("MEXICO");
                    Serenity.getDriver().findElement(By.xpath("//li[contains(text(),'MEXICO')]")).click();
                    //TODO: Ingresa - Correo electrónico
                    Serenity.getDriver().findElement(By.xpath("//input[@id='email']")).sendKeys("alexisce004@GMAIL.COM");
                    //TODO: Ingresa - Celular
                    Serenity.getDriver().findElement(By.xpath("//input[@id='telephone']")).sendKeys("959149978");
                }

            }

            if (tipoproducto.equals("SEGUROS")) {

                if (tipologia.equals("DUPLICADO DE DOCUMENTO") || tipologia.equals("DESAFILIACIÓN DE SEGURO") ||
                        tipologia.equals("INFORMACION DE MOVIMIENTOS")) {
                    String tipoProducto = Serenity.sessionVariableCalled("tipoProducto").toString();
                    //TODO: Seleccionar - Tipo de producto
                    Serenity.getDriver().findElement(By.id("productType")).click();
                    Serenity.getDriver().findElement(By.xpath("(//*[@id='" + tipoProducto + "'])")).click();
                    //TODO: Ingresa - N° de tipo de producto
                    Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("1007003469143");
                }

                if (tipologia.equals("COBROS INDEBIDOS")) {
                    String tipoProducto = Serenity.sessionVariableCalled("tipoProducto").toString();
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Seleccionar - Tipo de producto
                    Serenity.getDriver().findElement(By.id("productType")).click();
                    Serenity.getDriver().findElement(By.xpath("(//*[@id='" + tipoProducto + "'])")).click();
                    //TODO: Ingresar N° de tipo de Producto
                    Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("12345678910");
                    //TODO: Ingresa Canal Utilizado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

                if (tipologia.equals("SEGURO DESGRAVAMEN FALLECIMIENTO")) {
                    //TODO: Ingresa Parentesco con el Cliente
                    Serenity.getDriver().findElement(By.id("contactRelationship")).sendKeys("Padre");
                    //TODO: Ingresa Documento Identidad de Contacto
                    Serenity.getDriver().findElement(By.id("contactDocument")).sendKeys("72819468");
                    //TODO: Ingresa Nombre de Contacto
                    Serenity.getDriver().findElement(By.id("contactName")).sendKeys("Luis Ayala");
                    //TODO: Ingresa Fecha de Fallecimiento
                    Serenity.getDriver().findElement(By.id("deathName")).sendKeys("05082020");
                }

                if (tipologia.equals("PASE A CUOTAS SEGURO EXTRACASH")) {
                    //TODO: Seleccionar - Nro de tarjeta
                    Serenity.getDriver().findElement(By.id("cardNumber")).click();
                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='cardNumber']//li)[1]")).click();
                    //TODO: Ingresa Nro de Cuotas Realizadas
                    Serenity.getDriver().findElement(By.id("nrQuotasRequested")).sendKeys("200");
                    //TODO: Ingresa Monto
                    Serenity.getDriver().findElement(By.id("amount")).sendKeys("5000");
                }

                if (tipologia.equals("NO RECEPCIÓN DE DOCUMENTO")) {
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Ingresa - N° de tarjeta
                    Serenity.getDriver().findElement(By.id("cardNumber")).sendKeys("1234567891011");
                    //TODO: Ingresa Canal Utilizado
                    Serenity.getDriver().findElement(By.id("channelUsed")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

                if (tipologia.equals("CAMBIO DE SEGURO EXTERNO A INTERNO")) {
                    //TODO: Ingresa - N° de crédito
                    Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("1234567891011");
                }

                if (tipologia.equals("MODIFICACIÓN DE CONDICIONES CONTRACTUALES")) {
                    //TODO: Ingresa - N° de tarjeta
                    Serenity.getDriver().findElement(By.id("cardNumber")).sendKeys("4213555322311541");
                    //TODO: Ingresa - N° de cuenta
                    Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("1007003469143");
                    //TODO: Seleccionar - Moneda
                    Serenity.getDriver().findElement(By.id("currency")).click();
                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='currency']//li)[1]")).click();
                }

            }

            if (tipoproducto.equals("ADELANTO SUELDO")) {

                if (tipologia.equals("COBROS INDEBIDOS")) {
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Seleccionar - Canal utlizado / reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

                if (tipologia.equals("INFORMACION DE MOVIMIENTOS")) {
                    //TODO: Ingresa - N° Tarjeta
                    Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("123123123123123");
                    //TODO: Ingresa - Tipo de Convenio
                    Serenity.getDriver().findElement(By.id("agreementType")).sendKeys("Privado");
                }

                if (tipologia.equals("MODIFICACIÓN DE CUENTA SUELDO ASOCIADA")) {
                    //TODO: Ingresa - Cuenta Sueldo Actual
                    Serenity.getDriver().findElement(By.id("currentSalaryAccount")).sendKeys("123123123123123");
                    //TODO: Ingresa - Cuenta Sueldo Nueva
                    Serenity.getDriver().findElement(By.id("currentSalaryNew")).sendKeys("123123123123124");
                }

                if (tipologia.equals("CONSUMO/ RETIRO NO EFECTUADO")) {
                    //TODO: Seleccionar - ¿Qué pasó con la tarjeta?
                    Serenity.getDriver().findElement(By.id("cardFraudSituation")).click();
                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='cardFraudSituation']//li)[1]")).click();
                    //TODO: Ingresa - N° de tarjeta
                    Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("123123123123123");
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Seleccionar - Canal utlizado / reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

                if (tipologia.equals("DUPLICADO DE DOCUMENTO")) {
                    String tipoDocumentoTramite = Serenity.sessionVariableCalled("tipoDocumentoTramite").toString();
                    //TODO: Seleccionar - Tipo de documento
                    Serenity.getDriver().findElement(By.id("documentType")).click();
                    Serenity.getDriver().findElement(By.id(tipoDocumentoTramite)).click();
                    //TODO: Ingresa - N° de tarjeta
                    Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("123123123123123");
                    //TODO: Ingresa - N° de cuenta
                    Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("1007003469143");
                }

            }

            if (tipoproducto.equals("LINEA CONVENIO")) {

                if (tipologia.equals("BAJA DE TASA")) {
                    //TODO: Ingresa - Tasa solicitada %
                    Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("2");
                }

                if (tipologia.equals("DUPLICADO DE DOCUMENTO")) {
                    String tipoDocumentoTramite = Serenity.sessionVariableCalled("tipoDocumentoTramite").toString();
                    //TODO: Seleccionar - Tipo de documento
                    Serenity.getDriver().findElement(By.id("documentType")).click();
                    Serenity.getDriver().findElement(By.id(tipoDocumentoTramite)).click();
                }

                if (tipologia.equals("ENDOSO SEGUROS DE DESGRAVAMEN")) {
                    //TODO: Ingresa Monto de endoso
                    Serenity.getDriver().findElement(By.id("amount")).sendKeys("1000");
                    //TODO: Ingresa N° Poliza
                    Serenity.getDriver().findElement(By.id("policyNumber")).sendKeys("123456");
                    //TODO: Ingresa Cía de Seguros
                    Serenity.getDriver().findElement(By.id("insuranceCia")).sendKeys("123123");
                    //TODO: Seleccione Moneda Credito
                    Serenity.getDriver().findElement(By.id("creditCurrency")).click();
                    Serenity.getDriver().findElement(By.xpath("//*[@id='creditCurrency']/div[2]/ul/li[1]")).click();
                    //TODO: Seleccione Moneda Poliza
                    Serenity.getDriver().findElement(By.id("policyCurrency")).click();
                    Serenity.getDriver().findElement(By.xpath("//*[@id='policyCurrency']/div[2]/ul/li[1]")).click();
                }

                if (tipologia.equals("DESACUERDO DE CONDICIONES/TASAS/TARIFAS") || tipologia.equals("NO RECEPCIÓN DE DOCUMENTO")) {
                    String canal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Ingresa Canal Utilizado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(canal)).click();
                }

                if (tipologia.equals("REPORTE INDEBIDO CENTRALES DE RIESGO")) {
                    String canal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Ingresa Canal Utilizado
                    Serenity.getDriver().findElement(By.id("channel")).click();
                    Serenity.getDriver().findElement(By.id(canal)).click();
                }

                if (tipologia.equals("CONSUMO/ RETIRO NO EFECTUADO")) {
                    String canal = Serenity.sessionVariableCalled("canal").toString();
                    String cardFraudeSituation = Serenity.sessionVariableCalled("quePasoConLaTarjeta").toString();
                    //TODO: Seleccionar Que paso con la Tarjeta
                    Serenity.getDriver().findElement(By.id("cardFraudSituation")).click();
                    Serenity.getDriver().findElement(By.id(cardFraudeSituation)).click();
                    //TODO: Ingresar Numero Tarjeta
                    Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("123123123123123");
                    //TODO: Ingresa Canal Utilizado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(canal)).click();
                }

            }

            if (tipoproducto.equals("HIPOTECARIO")) {

                if (tipologia.equals("BAJA DE TASA")) {
                    //TODO: Ingresa - Tasa solicitada %
                    Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("2");
                }

                if (tipologia.equals("NO RECEPCIÓN DE DOCUMENTO")) {
                    String tipoDocumentoTramite = Serenity.sessionVariableCalled("tipoDocumentoTramite").toString();
                    //TODO: Seleccionar - Tipo de documento
                    Serenity.getDriver().findElement(By.id("paymentDocumentType")).click();
                    Serenity.getDriver().findElement(By.id(tipoDocumentoTramite)).click();
                }

                if (tipologia.equals("DEVOLUCIÓN DE DOCUMENTOS") || tipologia.equals("DUPLICADO DE DOCUMENTO")) {
                    String tipoDocumentoTramite = Serenity.sessionVariableCalled("tipoDocumentoTramite").toString();
                    //TODO: Seleccionar - Tipo de documento
                    Serenity.getDriver().findElement(By.id("documentType")).click();
                    Serenity.getDriver().findElement(By.id(tipoDocumentoTramite)).click();
                }

                if (tipologia.equals("CUOTA INICIAL HASTA 25% AFP")) {
                    //TODO: Ingresa - Cantidad de vouchers
                    Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("121516");
                    //TODO: Ingresa - Monto
                    Serenity.getDriver().findElement(By.id("amount")).sendKeys("120");
                    //TODO: Ingresa - Moneda
                    Serenity.getDriver().findElement(By.id("currency")).click();
                    Serenity.getDriver().findElement(By.id("PEN")).click();
                }

                if (tipologia.equals("EMISIÓN DE PRE CONFORMIDAD AFP 25%")) {
                    //TODO: Ingresa - Cantidad de vouchers
                    Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("121516");
                    //TODO: Ingresa - Fecha de Abono
                    Serenity.getDriver().findElement(By.id("paymentDate")).sendKeys("12/12/2022");
                    //TODO: Ingresa - Monto
                    Serenity.getDriver().findElement(By.id("amount")).sendKeys("120");
                    //TODO: Ingresa - Moneda
                    Serenity.getDriver().findElement(By.id("currency")).click();
                    Serenity.getDriver().findElement(By.id("PEN")).click();
                }

                if (tipologia.equals("CUOTA FLEXIBLE")) {
                    //TODO: Ingresa - Fecha de cuota para aplazar
                    Serenity.getDriver().findElement(By.id("datePostpone")).sendKeys(getValueFecha());
                }

                if (tipologia.equals("ENDOSO SEGUROS DE DESGRAVAMEN")) {
                    //TODO: Ingresa Monto de endoso
                    Serenity.getDriver().findElement(By.id("amount")).sendKeys("1000");
                    //TODO: Ingresa N° Poliza
                    Serenity.getDriver().findElement(By.id("policyNumber")).sendKeys("123456");
                    //TODO: Ingresa Cía de Seguros
                    Serenity.getDriver().findElement(By.id("insuranceCia")).sendKeys("123123");
                    //TODO: Seleccione Moneda Credito
                    Serenity.getDriver().findElement(By.id("creditCurrency")).click();
                    Serenity.getDriver().findElement(By.xpath("//*[@id='creditCurrency']/div[2]/ul/li[1]")).click();
                    //TODO: Seleccione Moneda Poliza
                    Serenity.getDriver().findElement(By.id("policyCurrency")).click();
                    Serenity.getDriver().findElement(By.xpath("//*[@id='policyCurrency']/div[2]/ul/li[1]")).click();
                }

                if (tipologia.equals("REPORTE INDEBIDO CENTRALES DE RIESGO")) {
                    String canal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Ingresa Canal Utilizado
                    Serenity.getDriver().findElement(By.id("channel")).click();
                    Serenity.getDriver().findElement(By.id(canal)).click();
                }

                if (tipologia.equals("REPROGRAMACIÓN DE DEUDA")) {
                    //TODO: Ingresa - Periodo de gracia
                    Serenity.getDriver().findElement(By.id("gracePeriod")).sendKeys("12");
                    //TODO: Ingresa - N° de cuotas solicitadas
                    Serenity.getDriver().findElement(By.id("requestedFee")).sendKeys("10");
                    //TODO: Ingresa - Monto Total S/. Referencial
                    Serenity.getDriver().findElement(By.id("referenceAmount")).sendKeys("10000");
                    //TODO: Ingresa - Tasa Solicitada %
                    Serenity.getDriver().findElement(By.id("requestedRate")).sendKeys("8");
                }

                if (tipologia.equals("TRÁMITES DOCUMENTARIOS POST VENTA")) {
                    //TODO: Ingresa - Email
                    Serenity.getDriver().findElement(By.id("personalEmail")).sendKeys("alexisce004@GMAIL.COM");
                    //TODO: Ingresa - Celular
                    Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("959149978");
                }

                if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA") || tipologia.equals("COBROS INDEBIDOS") ||
                        tipologia.equals("CONSUMO/ RETIRO NO EFECTUADO")) {
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Selecciona - Canal utilizado / reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

                if (tipologia.equals("LEVANTAMIENTO DE GARANTÍA")) {
                    //TODO: Ingresa - N° de partida
                    Serenity.getDriver().findElement(By.id("itemNumber")).sendKeys("COD250");
                    //TODO: Seleccionar - Tipo de inmueble
                    Serenity.getDriver().findElement(By.id("propertyType")).click();
                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='propertyType']//li)[1]")).click();
                   Serenity.getDriver().findElement(By.xpath("//*[contains(text(), ' Seleccione... ')]/../../../..")).click();
                    Serenity.getDriver().findElement(By.id("Lima")).click();
                    //Serenity.getDriver().findElement(By.id("Provincia")).click();
                    // Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("MIRAFLORES NRO 255");
                    //TODO: Seleccionar - Tipo de documento cónyuge/coodeudor1
                    Serenity.getDriver().findElement(By.id("documentType")).click();
                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='documentType']//li)[1]")).click();
                    //TODO: Ingresa - N° de documento cónyuge/coodeudor1
                    Serenity.getDriver().findElement(By.id("documentNumber")).sendKeys("18810011");
                    //TODO: Seleccionar - Tipo de documento cónyuge/coodeudor2
                    Serenity.getDriver().findElement(By.id("documentTypeOther")).click();
                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='documentTypeOther']//li)[1]")).click();
                    //TODO: Ingresa - N° de documento cónyuge/coodeudor2
                    Serenity.getDriver().findElement(By.id("documentNumberOther")).sendKeys("18820012");
                }

            }

            if (tipoproducto.equals("PRÉSTAMO PERSONAL")) {

                if (tipologia.equals("BAJA DE TASA")) {
                    //TODO: Ingresa - Tasa solicitada %
                    Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("2");
                }

                if (tipologia.equals("DEVOLUCIÓN DE DOCUMENTOS") || tipologia.equals("DUPLICADO DE DOCUMENTO")) {
                    String tipoDocumentoTramite = Serenity.sessionVariableCalled("tipoDocumentoTramite").toString();
                    //TODO: Seleccionar - Tipo de documento
                    Serenity.getDriver().findElement(By.id("documentType")).click();
                    Serenity.getDriver().findElement(By.id(tipoDocumentoTramite)).click();
                }

                if (tipologia.equals("CUOTA FLEXIBLE")) {
                    //TODO: Ingresa - Fecha cuota aplazar
                    Serenity.getDriver().findElement(By.id("datePostpone")).sendKeys(getValueFecha());
                }

                if (tipologia.equals("ENDOSO SEGUROS DE DESGRAVAMEN")) {
                    //TODO: Ingresa - Monto
                    Serenity.getDriver().findElement(By.id("amount")).sendKeys("150");
                    //TODO: Ingresa - N° de poliza
                    Serenity.getDriver().findElement(By.id("policyNumber")).sendKeys("130354");
                    //TODO: Ingresa - Cía. de seguros
                    Serenity.getDriver().findElement(By.id("insuranceCia")).sendKeys("COD2052699595959959");
                    //TODO: Seleccionar - Moneda crédito
                    Serenity.getDriver().findElement(By.id("creditCurrency")).click();
                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='creditCurrency']//li)[1]")).click();
                    //TODO: Seleccionar - Moneda póliza
                    Serenity.getDriver().findElement(By.id("policyCurrency")).click();
                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='policyCurrency']//li)[1]")).click();
                }

                if (tipologia.equals("NO RECEPCIÓN DE DOCUMENTO") || tipologia.equals("CONSUMO/ RETIRO NO EFECTUADO") ||
                        tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA")) {
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();

                    //TODO: Seleccionar - Canal utlizado / reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueCanal + "'])[last()]")).click();
                }

                if (tipologia.equals("REPROGRAMACIÓN DE DEUDA CON CAMPAÑA")) {
                    //TODO: Ingresa - Periodo de gracia
                    Serenity.getDriver().findElement(By.id("gracePeriod")).sendKeys("12");
                    //TODO: Ingresa - Monto Tot. S/. Referencial
                    Serenity.getDriver().findElement(By.id("totalReferenceAmount")).sendKeys("456,236.15");
                    //TODO: Ingresa - Tasa solicitada %
                    Serenity.getDriver().findElement(By.id("requestedRate")).sendKeys("5");
                }

                if (tipologia.equals("REPROGRAMACIÓN DE DEUDA SIN CAMPAÑA")) {
                    //TODO: Ingresa - Periodo de gracia
                    Serenity.getDriver().findElement(By.id("gracePeriod")).sendKeys("10");
                    //TODO: Ingresa - Monto Tot. S/. Referencial
                    Serenity.getDriver().findElement(By.id("totalReferenceAmount")).sendKeys("456,236.15");
                }

            }

            if (tipoproducto.equals("REFINANCIADO")) {

                if (tipologia.equals("BAJA DE TASA")) {
                    //TODO: Ingresa - Tasa solicitada %
                    Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("2");
                }

                if (tipologia.equals("DEVOLUCIÓN DE DOCUMENTOS") || tipologia.equals("DUPLICADO DE DOCUMENTO")) {
                    String tipoDocumentoTramite = Serenity.sessionVariableCalled("tipoDocumentoTramite").toString();
                    //TODO: Seleccionar - Tipo de documento
                    Serenity.getDriver().findElement(By.id("documentType")).click();
                    Serenity.getDriver().findElement(By.id(tipoDocumentoTramite)).click();
                }

                if (tipologia.equals("ENDOSO SEGUROS DE DESGRAVAMEN")) {
                    //TODO: Ingresa - Monto endoso
                    Serenity.getDriver().findElement(By.id("amount")).sendKeys("1000");
                    //TODO: Ingresa - N° de póliza
                    Serenity.getDriver().findElement(By.id("policyNumber")).sendKeys("123456");
                    //TODO: Ingresa - Cía. de seguros
                    Serenity.getDriver().findElement(By.id("insuranceCia")).sendKeys("123123");
                    //TODO: Seleccione Moneda crédito
                    Serenity.getDriver().findElement(By.id("creditCurrency")).click();
                    Serenity.getDriver().findElement(By.xpath("//*[@id='creditCurrency']/div[2]/ul/li[1]")).click();
                    //TODO: Seleccione Moneda póliza
                    Serenity.getDriver().findElement(By.id("policyCurrency")).click();
                    Serenity.getDriver().findElement(By.xpath("//*[@id='policyCurrency']/div[2]/ul/li[1]")).click();
                }

                if (tipologia.equals("CUOTA FLEXIBLE")) {
                    //TODO: Ingresa - Fecha de cuota aplazar
                    Serenity.getDriver().findElement(By.id("datePostpone")).sendKeys(getValueFecha());
                }

                if (tipologia.equals("CONSUMO/ RETIRO NO EFECTUADO") || tipologia.equals("NO RECEPCIÓN DE DOCUMENTO") ||
                        tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA")) {
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Seleccionar - Canal utilizado/reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

                if (tipologia.equals("REPROGRAMACIÓN DE DEUDA CON CAMPAÑA")) {
                    //TODO: Ingresa - Periodo de gracia
                    Serenity.getDriver().findElement(By.id("gracePeriod")).sendKeys("12");
                    //TODO: Ingresa - Monto Tot. S/. Referencial
                    Serenity.getDriver().findElement(By.id("totalReferenceAmount")).sendKeys("456,236.15");
                    //TODO: Ingresa - Tasa solicitada %
                    Serenity.getDriver().findElement(By.id("requestedRate")).sendKeys("5");
                }

                if (tipologia.equals("REPROGRAMACIÓN DE DEUDA SIN CAMPAÑA")) {
                    //TODO: Ingresa - Periodo de gracia
                    Serenity.getDriver().findElement(By.id("gracePeriod")).sendKeys("10");
                    //TODO: Ingresa - Monto Tot. S/. Referencial
                    Serenity.getDriver().findElement(By.id("totalReferenceAmount")).sendKeys("456,236.15");
                }

            }

            if (tipoproducto.equals("EFECTIVO CON GARANTÍA HIPOTECARIO")) {

                if (tipologia.equals("BAJA DE TASA")) {
                    //TODO: Ingresa - Tasa solicitada %
                    Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("2");
                }

                if (tipologia.equals("DEVOLUCIÓN DE DOCUMENTOS") || tipologia.equals("DUPLICADO DE DOCUMENTO")) {
                    String tipoDocumentoTramite = Serenity.sessionVariableCalled("tipoDocumentoTramite").toString();
                    //TODO: Seleccionar - Tipo de documento
                    Serenity.getDriver().findElement(By.id("documentType")).click();
                    Serenity.getDriver().findElement(By.id(tipoDocumentoTramite)).click();
                }

                if (tipologia.equals("ENDOSO SEGUROS DE DESGRAVAMEN")) {
                    //TODO: Ingresa - Monto endoso
                    Serenity.getDriver().findElement(By.id("amount")).sendKeys("150");
                    //TODO: Ingresa - N° de póliza
                    Serenity.getDriver().findElement(By.id("policyNumber")).sendKeys("130354");
                    //TODO: Ingresa - Cía. de seguros
                    Serenity.getDriver().findElement(By.id("insuranceCia")).sendKeys("2052699595959959");
                    //TODO: Seleccionar - Moneda crédito
                    Serenity.getDriver().findElement(By.id("creditCurrency")).click();
                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='creditCurrency']//li)[1]")).click();
                    //TODO: Seleccionar - Moneda póliza
                    Serenity.getDriver().findElement(By.id("policyCurrency")).click();
                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='policyCurrency']//li)[1]")).click();
                }

                if (tipologia.equals("CUOTA FLEXIBLE")) {
                    //TODO: Ingresa - Fecha de cuota aplazar
                    Serenity.getDriver().findElement(By.id("datePostpone")).sendKeys(getValueFecha());
                }

                if (tipologia.equals("CONSUMO/ RETIRO NO EFECTUADO") ||
                        tipologia.equals("COBROS INDEBIDOS")) {
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Seleccionar - Canal utilizado/reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

                if (tipologia.equals("NO RECEPCIÓN DE DOCUMENTO")) {
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Seleccionar - Canal utilizado/reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();

                    String tipoDocumentoTramite = Serenity.sessionVariableCalled("tipoDocumentoTramite").toString();
                    //TODO: Seleccionar - Tipo de documento
                    Serenity.getDriver().findElement(By.id("paymentDocumentType")).click();
                    Serenity.getDriver().findElement(By.id(tipoDocumentoTramite)).click();
                }

                if (tipologia.equals("REPORTE INDEBIDO CENTRALES DE RIESGO") || tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA")) {
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Seleccionar - Canal utilizado/reclamado
                    Serenity.getDriver().findElement(By.id("channelClaimed")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

                if (tipologia.equals("EMISIÓN DE PRE CONFORMIDAD AFP 25%")) {
                    //TODO: Ingresa - Cantidad de vouchers
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).sendKeys("10");
                    //TODO: Ingresa - Fecha de abono
                    Serenity.getDriver().findElement(By.id("paymentDate")).sendKeys(getValueFecha());
                    //TODO: Ingresa - Monto
                    Serenity.getDriver().findElement(By.id("amount")).sendKeys("541,158.17");
                    //TODO: Seleccionar - Moneda
                    Serenity.getDriver().findElement(By.id("currency")).click();
                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='currency']//li)[1]")).click();
                }

                if (tipologia.equals("CUOTA INICIAL HASTA 25% AFP")) {
                    //TODO: Ingresa - Cantidad de vouchers
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).sendKeys("10");
                    //TODO: Ingresa - Monto
                    Serenity.getDriver().findElement(By.id("amount")).sendKeys("541,158.17");
                    //TODO: Seleccionar - Moneda
                    Serenity.getDriver().findElement(By.id("currency")).click();
                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='currency']//li)[1]")).click();
                }

                if (tipologia.equals("REPROGRAMACIÓN DE DEUDA")) {
                    //TODO: Ingresa - Periodo de gracia
                    Serenity.getDriver().findElement(By.id("gracePeriod")).sendKeys("10");
                    //TODO: Ingresa - N° de cuotas solicitadas
                    Serenity.getDriver().findElement(By.id("requestedFee")).sendKeys("5");
                    //TODO: Ingresa - Monto Tot. S/. Referencial
                    Serenity.getDriver().findElement(By.id("totalReferenceAmount")).sendKeys("541,158.17");
                    //TODO: Ingresa - Tasa solicitada %
                    Serenity.getDriver().findElement(By.id("requestedRate")).sendKeys("6");
                }

                if (tipologia.equals("TRÁMITES DOCUMENTARIOS POST VENTA")) {
                    //TODO: Ingresa - Email
                    Serenity.getDriver().findElement(By.id("personalEmail")).sendKeys("alexisce004@GMAIL.COM");
                    //TODO: Ingresa - Celular
                    Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("959149978");
                }

            }

            if (tipoproducto.equals("VEHICULAR")) {

                if (tipologia.equals("BAJA DE TASA")) {
                    //TODO: Ingresa - Tasa solicitada %
                    Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("2");
                }

                if (tipologia.equals("DEVOLUCIÓN DE DOCUMENTOS") || tipologia.equals("DUPLICADO DE DOCUMENTO")) {
                    String tipoDocumentoTramite = Serenity.sessionVariableCalled("tipoDocumentoTramite").toString();
                    //TODO: Seleccionar - Tipo de documento
                    Serenity.getDriver().findElement(By.id("documentType")).click();
                    Serenity.getDriver().findElement(By.id(tipoDocumentoTramite)).click();
                }

                if (tipologia.equals("CUOTA FLEXIBLE")) {
                    //TODO: Ingresa - Fecha de cuota aplazar
                    Serenity.getDriver().findElement(By.id("datePostpone")).sendKeys(getValueFecha());
                }

                if (tipologia.equals("ENDOSO SEGUROS DE DESGRAVAMEN")) {
                    //TODO: Ingresa - Monto
                    Serenity.getDriver().findElement(By.id("amount")).sendKeys("150");
                    //TODO: Ingresa - N° de poliza
                    Serenity.getDriver().findElement(By.id("policyNumber")).sendKeys("130354");
                    //TODO: Ingresa - Cía. de seguros
                    Serenity.getDriver().findElement(By.id("insuranceCia")).sendKeys("2052699595959959");
                    //TODO: Seleccionar - Moneda crédito
                    Serenity.getDriver().findElement(By.id("creditCurrency")).click();
                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='creditCurrency']//li)[1]")).click();
                    //TODO: Seleccionar - Moneda póliza
                    Serenity.getDriver().findElement(By.id("policyCurrency")).click();
                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='policyCurrency']//li)[1]")).click();
                }

                if (tipologia.equals("REPROGRAMACIÓN DE DEUDA")) {
                    //TODO: Ingresa - Periodo de gracia
                    Serenity.getDriver().findElement(By.id("gracePeriod")).sendKeys("12");
                    //TODO: Ingresa - N° de cuotas solicitadas
                    Serenity.getDriver().findElement(By.id("requestedFee")).sendKeys("10");
                    //TODO: Ingresa - Monto Total S/. Referencial
                    Serenity.getDriver().findElement(By.id("referenceAmount")).sendKeys("10000");
                    //TODO: Ingresa - Tasa Solicitada %
                    Serenity.getDriver().findElement(By.id("requestedRate")).sendKeys("8");
                }

                if (tipologia.equals("CONSUMO/ RETIRO NO EFECTUADO") || tipologia.equals("NO RECEPCIÓN DE DOCUMENTO")) {
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Seleccionar - Canal utilizado/reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

                if (tipologia.equals("LEVANTAMIENTO DE GARANTÍA")) {
                    Serenity.getDriver().findElement(By.xpath("//*[contains(text(), ' Seleccione... ')]/../../../..")).click();
                    Serenity.getDriver().findElement(By.id("Lima")).click();

                        //TODO: Ingresa - Número de placa
                    Serenity.getDriver().findElement(By.id("plateNumber")).sendKeys("F5U597");


                  /*  //TODO: Ingresa - Ubicación tienda
                    Serenity.getDriver().findElement(By.id("locationStore")).sendKeys("SURCO NRO 448");
                    //TODO: Ingresa - Número de placa
                    Serenity.getDriver().findElement(By.id("plateNumber")).sendKeys("F5U597");*/
                }

            }

            if (tipoproducto.equals("CUENTA CORRIENTE")) {

                if (tipologia.equals("REPORTE INDEBIDO CENTRALES DE RIESGO")) {
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Seleccionar - Canal utlizado / reclamado
                    Serenity.getDriver().findElement(By.id("channel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

            }

            if (tipoproducto.equals("SEGUROS RELACIONADOS")) {

                if (tipologia.equals("ENDOSO SEGUROS DEL BIEN")) {
                    //TODO: Ingresa N° Credito
                    Serenity.getDriver().findElement(By.id("creditNumber")).sendKeys("1231234567896");
                    //TODO: Ingresa N° Poliza
                    Serenity.getDriver().findElement(By.id("policyNumber")).sendKeys("123456");
                    //TODO: Ingresa Monto de endoso
                    Serenity.getDriver().findElement(By.id("amount")).sendKeys("1000");
                    //TODO: Ingresa Cía de Seguros
                    Serenity.getDriver().findElement(By.id("insuranceName")).sendKeys("123123");
                    //TODO: Seleccionar - Moneda crédito
                    Serenity.getDriver().findElement(By.id("creditCurrency")).click();
                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='creditCurrency']//li)[1]")).click();
                    //TODO: Seleccione - Moneda Poliza
                    Serenity.getDriver().findElement(By.id("policyCurrency")).click();
                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='policyCurrency']//li)[1]")).click();
                }

                if (tipologia.equals("APLICACIÓN DE FONDOS POR SINIESTROS/DESGRAVAMEN")) {
                    //TODO: Ingresa N° Credito
                    Serenity.getDriver().findElement(By.id("creditNumber")).sendKeys("123123123");
                }

                if (tipologia.equals("SOLICITUD DE INFORMACIÓN DE SINIESTROS")) {
                    //TODO: Ingresa - N° de cuenta(Opcional)
                    Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("1007000046856");
                    //TODO: Ingresa - N° de tarjeta de crédito(Opcional)
                    Serenity.getDriver().findElement(By.id("cardNumber")).sendKeys("4213555322311541");
                    //TODO: Ingresa - Monto Tot. S/. Referencial
                    Serenity.getDriver().findElement(By.id("referenceAmount")).sendKeys("350");
                    //TODO: Seleccionar - Moneda ingreso
                    Serenity.getDriver().findElement(By.id("incomeCurrency")).click();
                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='incomeCurrency']//li)[1]")).click();
                    //TODO: Ingresa - Plan vigente
                    Serenity.getDriver().findElement(By.id("currentPlan")).sendKeys("UN AÑO PRUEBA PRUEBA PRUEBA");
                }

                if (tipologia.equals("LEVANTAMIENTO DE ENDOSO")) {
                    //TODO: Ingresa - N° de crédito
                    Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("12345678");
                    //TODO: Ingresa - Cía. de seguros
                    Serenity.getDriver().findElement(By.id("insuranceCia")).sendKeys("123123");
                    //TODO: Ingresa - N° de póliza
                    Serenity.getDriver().findElement(By.id("policyNumber")).sendKeys("130354");
                }

                if (tipologia.equals("DUPLICADO DE DOCUMENTO")) {
                    String valueTipoFiltroProducto = Serenity.sessionVariableCalled("tipoFiltroProducto").toString();
                    //TODO: Seleccionar - Tipo filtro producto
                    Serenity.getDriver().findElement(By.id("productType")).click();
                    Serenity.getDriver().findElement(By.id(valueTipoFiltroProducto)).click();
                    //TODO: Ingresa - N° de tipo de producto
                    Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("4213555322311541");
                }

                if (tipologia.equals("INFORMACION DE MOVIMIENTOS")) {
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Ingresa - N° de tarjeta
                    Serenity.getDriver().findElement(By.id("cardNumber")).sendKeys("4213555322311541");
                    //TODO: Ingresa - N° de crédito
                    Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("12312345678969");
                    //TODO: Seleccionar - Canal utilizado/reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

                if (tipologia.equals("COBROS INDEBIDOS")) {
                    String valueTipoProducto = Serenity.sessionVariableCalled("tipoProducto").toString();
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();

                    //TODO: Seleccionar - Tipo de producto
                    Serenity.getDriver().findElement(By.id("productType")).click();
                    Serenity.getDriver().findElement(By.id(valueTipoProducto)).click();
                    //TODO: Ingresa - N° de tipo de producto
                    Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("4213555322311541");
                    //TODO: Seleccionar - Motivo de Cobro
                    Serenity.getDriver().findElement(By.id("reasonPayment")).click();
                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='reasonPayment']//li)[1]")).click();
                    //TODO: Seleccionar - Canal utilizado/reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

            }

            if (tipoproducto.equals("CBME")) {

                if (tipologia.equals("COPIA DE BOLETA DE DEPÓSITO LIMA")) {
                    //TODO: Ingresa - N° de cuenta
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).sendKeys("1007000046856");
                }

            }

            if (tipoproducto.equals("CARTA FIANZA/CERTIFICADO")) {

                if (tipologia.equals("DUPLICADO DE DOCUMENTO")) {
                    String tipoDocumentoTramite = Serenity.sessionVariableCalled("tipoDocumentoTramite").toString();
                    //TODO: Seleccionar - Tipo de documento
                    Serenity.getDriver().findElement(By.id("documentType")).click();
                    Serenity.getDriver().findElement(By.id(tipoDocumentoTramite)).click();
                    //TODO: Ingresa - N° de cuenta
                    Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("1007003469143");
                }

            }

            if (tipoproducto.equals("RAPPIBANK - TARJETA DE CRÉDITO")) {

                if (tipologia.equals("ATM DE IB NO ENTREGÓ DINERO")) {
                    String valueNumeroTarjeta = ApiCommons.clientes.getNumerotarjeta();
                    String valueCuenta =ApiCommons.clientes.getNumerocuenta();
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();

                    //TODO: Ingresa - N° de Tarjeta
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).sendKeys(valueNumeroTarjeta);
                    //TODO: Ingresa - N° de cuenta
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[2]")).sendKeys(valueCuenta);
                    //TODO: Ingresa - Código Cajero
                    Serenity.getDriver().findElement(By.id("codeATM")).sendKeys("COD123");
                    //TODO: Seleccionar - Canal utilizado / reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

                if (tipologia.equals("CORREO/TELÉFONO INDEBIDO DE CAMPAÑAS")) {
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Ingresa - Nombre del Titular
                    Serenity.getDriver().findElement(By.id("accountHolder")).sendKeys("JOSE LUIS");
                    //TODO: Seleccionar - Canal utilizado/reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

                if (tipologia.equals("DESACUERDO DE CONDICIONES/TASAS/TARIFAS")) {
                    String valueNumeroTarjeta = ApiCommons.clientes.getNumerotarjeta();
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();

                    //TODO: Ingresa - N° de tarjeta
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).sendKeys(valueNumeroTarjeta);
                    //TODO: Ingresa - Monto
                    Serenity.getDriver().findElement(By.id("amount")).sendKeys("123,456.78");
                    //TODO: Seleccionar - Canal utilizado / reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

                if (tipologia.equals("DIRECCIÓN/TELF. INDEBIDOS DE COBRANZA")) {
                    String valueNumeroTarjeta = ApiCommons.clientes.getNumerotarjeta();
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();

                    //TODO: Ingresa - N° de crédito
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).sendKeys("13035489");
                    //TODO: Ingresa - N° de tarjeta
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[2]")).sendKeys(valueNumeroTarjeta);
                    //TODO: Ingresa - Nombre del deudor
                    Serenity.getDriver().findElement(By.id("debtorName")).sendKeys("JUAN CARLOS");
                    //TODO: Seleccionar - Canal utilizado/reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

                if (tipologia.equals("INSATISFACCIÓN GESTIÓN DE ENTREGA DE TARJETA") || tipologia.equals("REPORTE INDEBIDO CENTRALES DE RIESGO")) {
                    String valueNumeroTarjeta = ApiCommons.clientes.getNumerotarjeta();
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();

                    //TODO: Ingresa - N° de tarjeta
                    Serenity.getDriver().findElement(By.id("cardNumber")).sendKeys(valueNumeroTarjeta);
                    //TODO: Seleccionar - Canal utilizado/reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

                if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA")) {
                    String valueNumeroTarjeta = ApiCommons.clientes.getNumerotarjeta();
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();

                    //TODO: Ingresa - N° de tarjeta
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).sendKeys(valueNumeroTarjeta);
                    //TODO: Seleccionar - Canal utilizado/reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

                if (tipologia.equals("COBROS INDEBIDOS") || tipologia.equals("CONSUMO/ RETIRO NO EFECTUADO")) {
                    String valueCuenta = ApiCommons.clientes.getNumerocuenta();
                    String valueNumeroTarjeta = ApiCommons.clientes.getNumerotarjeta();
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();

                    //TODO: Ingresa - N° de cuenta (Opcional)
                    Serenity.getDriver().findElement(By.id("cardNumber")).sendKeys(valueCuenta);
                    //TODO: Ingresa - N° de tarjeta de crédito (Opcional)
                    Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys(valueNumeroTarjeta);
                    //TODO: Ingresa - N° de Retiros/Cobros/Consumos(Opcional)
                    Serenity.getDriver().findElement(By.id("numCollConsWithdraw")).sendKeys("250");
                    //TODO: Seleccionar - Canal utilizado / reclamado
                    Serenity.getDriver().findElement(By.id("channelUsed")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

                if (tipologia.equals("LIBERACIÓN DE RETENCIONES")) {
                    String valueCuenta = ApiCommons.clientes.getNumerocuenta();
                    String valueNumeroTarjeta = ApiCommons.clientes.getNumerotarjeta();
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();

                    //TODO: Ingresa - N° de cuenta (Opcional)
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).sendKeys(valueCuenta);
                    //TODO: Ingresa - N° de tarjeta de crédito (Opcional)
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[2]")).sendKeys(valueNumeroTarjeta);
                    //TODO: Ingresa - N° de Retiros/Cobros/Consumos(Opcional)
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[3]")).sendKeys("305");
                    //TODO: Seleccionar - Canal utilizado / reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

                if (tipologia.equals("NO RECEPCIÓN DE DOCUMENTO")) {
                    String valueCuenta = ApiCommons.clientes.getNumerocuenta();
                    String valueNumeroTarjeta = ApiCommons.clientes.getNumerotarjeta();
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();

                    //TODO: Ingresa - N° de cuenta (Opcional)
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).sendKeys(valueCuenta);
                    //TODO: Ingresa - N° de tarjeta (Opcional)
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[2]")).sendKeys(valueNumeroTarjeta);
                    //TODO: Ingresa - Fecha de operación (Opcional)
                    Serenity.getDriver().findElement(By.id("operationDate")).sendKeys(getValueFecha());
                    //TODO: Ingresa - Cantidad (Opcional)
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[3]")).sendKeys("355");
                    //TODO: Seleccionar - Canal utilizado / reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

                if (tipologia.equals("OPERACIÓN DENEGADA")) {
                    String valueCuenta = ApiCommons.clientes.getNumerocuenta();
                    String valueNumeroTarjeta = ApiCommons.clientes.getNumerotarjeta();
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();

                    //TODO: Ingresa - N° de cuenta (Opcional)
                    Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys(valueCuenta);
                    //TODO: Ingresa - N° de tarjeta de crédito (Opcional)
                    Serenity.getDriver().findElement(By.id("cardNumber")).sendKeys(valueNumeroTarjeta);
                    //TODO: Ingresa - N° de Retiros/Cobros/Consumos(Opcional)
                    Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("256");
                    //TODO: Seleccionar - Canal utilizado / reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

                if (tipologia.equals("SISTEMA DE RECOMPENSAS")) {
                    String valueCuenta = ApiCommons.clientes.getNumerocuenta();
                    String valueNumeroTarjeta = ApiCommons.clientes.getNumerotarjeta();
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();

                    //TODO: Ingresa - N° de cuenta (Opcional)
                    Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys(valueCuenta);
                    //TODO: Ingresa - N° de tarjeta (Opcional)
                    Serenity.getDriver().findElement(By.id("cardNumber")).sendKeys(valueNumeroTarjeta);
                    //TODO: Ingresa - Fecha de operación (Opcional)
                    //try{Thread.sleep(421231123);}catch (Exception e){};
                    Serenity.getDriver().findElement(By.id("fechaOperacion")).sendKeys(getValueFecha());
                    //TODO: Ingresa - Cantidad (Opcional)
                    Serenity.getDriver().findElement(By.id("amount")).sendKeys("360");
                    //TODO: Seleccionar - Canal utilizado / reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

            }

            if (tipoproducto.equals("RAPPIBANK - CUENTAS")) {

                if (tipologia.equals("COBROS INDEBIDOS") || tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA")) {
                    String valueCuenta = ApiCommons.clientes.getNumerocuenta();
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();

                    //TODO: Ingresa - N° de cuenta
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).sendKeys(valueCuenta);
                    //TODO: Seleccionar - Canal utilizado / reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

                if (tipologia.equals("CONSUMO/ RETIRO NO EFECTUADO") || tipologia.equals("CUENTA/CANCELACIÓN NO RECONOCIDA")) {
                    String valueCuenta = ApiCommons.clientes.getNumerocuenta();
                    String valueNumeroTarjeta = ApiCommons.clientes.getNumerotarjeta();
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();

                    //TODO: Ingresa - N° de cuenta (Opcional)
                    Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys(valueCuenta);
                    //TODO: Ingresa - N° de tarjeta de crédito (Opcional)
                    Serenity.getDriver().findElement(By.id("cardNumber")).sendKeys(valueNumeroTarjeta);
                    //TODO: Ingresa - N° de Retiros/Cobros/Consumos(Opcional)
                    Serenity.getDriver().findElement(By.id("numCollConsWithdraw")).sendKeys("305");
                    //TODO: Seleccionar - Canal utilizado / reclamado
                    Serenity.getDriver().findElement(By.id("channelUsed")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

                if (tipologia.equals("DESACUERDO CON EL SERVICIO")) {
                    String valueCuenta = ApiCommons.clientes.getNumerocuenta();
                    String valueNumeroTarjeta = ApiCommons.clientes.getNumerotarjeta();
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();

                    //TODO: Ingresa - N° de cuenta (Opcional)
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).sendKeys(valueCuenta);
                    //TODO: Ingresa - N° de tarjeta de crédito (Opcional)
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[2]")).sendKeys(valueNumeroTarjeta);
                    //TODO: Ingresa - Fecha de operación (Opcional)
                    Serenity.getDriver().findElement(By.id("chargeDate")).sendKeys(getValueFecha());
                    //TODO: Ingresa - Cantidad (Opcional)
                    Serenity.getDriver().findElement(By.id("amount")).sendKeys("250");
                    //TODO: Seleccionar - Canal utilizado / reclamado
                    Serenity.getDriver().findElement(By.id("channelUsed")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

                if (tipologia.equals("DESACUERDO DE CONDICIONES/TASAS/TARIFAS") || tipologia.equals("INSATISFACCIÓN GESTIÓN DE ENTREGA DE TARJETA")) {
                    String valueCuenta = ApiCommons.clientes.getNumerocuenta();
                    String valueNumeroTarjeta = ApiCommons.clientes.getNumerotarjeta();
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();

                    //TODO: Ingresa - N° de cuenta (Opcional)
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).sendKeys(valueCuenta);
                    //TODO: Ingresa - N° de tarjeta (Opcional)
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[2]")).sendKeys(valueNumeroTarjeta);
                    //TODO: Ingresa - Fecha de operación (Opcional)
                    Serenity.getDriver().findElement(By.id("operationDate")).sendKeys(getValueFecha());
                    //TODO: Ingresa - Cantidad (Opcional)
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[3]")).sendKeys("350");
                    //TODO: Seleccionar - Canal utilizado / reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

                if (tipologia.equals("LIBERACIÓN DE RETENCIONES")) {
                    String valueCuenta = ApiCommons.clientes.getNumerocuenta();
                    String valueNumeroTarjeta = ApiCommons.clientes.getNumerotarjeta();
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();

                    //TODO: Ingresa - N° de cuenta (Opcional)
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).sendKeys(valueCuenta);
                    //TODO: Ingresa - N° de tarjeta de crédito (Opcional)
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[2]")).sendKeys(valueNumeroTarjeta);
                    //TODO: Ingresa - N° de Retiros/Cobros/Consumos(Opcional)
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[3]")).sendKeys("305");
                    //TODO: Seleccionar - Canal utilizado / reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

                if (tipologia.equals("OPERACIÓN DENEGADA") || tipologia.equals("REVISIÓN DE DEPÓSITOS A TERCEROS")) {
                    String valueCuenta =ApiCommons.clientes.getNumerocuenta();
                    String valueNumeroTarjeta = ApiCommons.clientes.getNumerotarjeta();
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();

                    //TODO: Ingresa - N° de cuenta (Opcional)
                    Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys(valueCuenta);
                    //TODO: Ingresa - N° de tarjeta de crédito (Opcional)
                    Serenity.getDriver().findElement(By.id("cardNumber")).sendKeys(valueNumeroTarjeta);
                    //TODO: Ingresa - N° de Retiros/Cobros/Consumos(Opcional)
                    Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("306");
                    //TODO: Seleccionar - Canal utilizado / reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

                if (tipologia.equals("SISTEMA DE RECOMPENSAS")) {
                    String valueCuenta = ApiCommons.clientes.getNumerocuenta();
                    String valueNumeroTarjeta = ApiCommons.clientes.getNumerotarjeta();
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();

                    //TODO: Ingresa - N° de cuenta (Opcional)
                    Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys(valueCuenta);
                    //TODO: Ingresa - N° de tarjeta (Opcional)
                    Serenity.getDriver().findElement(By.id("cardNumber")).sendKeys(valueNumeroTarjeta);
                    //TODO: Ingresa - Fecha de operación (Opcional)
                    Serenity.getDriver().findElement(By.id("date")).sendKeys(getValueFecha());
                    //TODO: Ingresa - Cantidad (Opcional)
                    Serenity.getDriver().findElement(By.id("amount")).sendKeys("360");
                    //TODO: Seleccionar - Canal utilizado / reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

            }

            //TODO: TIPOLOGIAS DE OPERACIONES
            if (tipoproducto.equals("APP INTERBANK")) {

                String tarjeta = ApiCommons.clientes.getNumerotarjeta();
                String cuenta = ApiCommons.clientes.getNumerocuenta();

                if (tipologia.equals("CONSTANCIA DE PAGO REALIZADO")) {
                    //TODO: Selecciona - Nro. de tarjeta
                    Serenity.getDriver().findElement(By.id("cardNumber")).click();
                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='cardNumber']//li)[1]")).click();
                    //TODO: Selecciona - Nro. de cuenta
                    Serenity.getDriver().findElement(By.id("accountNumber")).click();
                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='accountNumber']//li)[1]")).click();
                }

                if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA")) {
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();

                    //TODO: Ingresa - Nro. de tarjeta
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).sendKeys(tarjeta);
                    //TODO: Ingresa - Nro. de cuenta
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[last()]")).sendKeys(cuenta);
                    //TODO: Seleccionar - Canal utlizado / reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueCanal + "'])[last()]")).click();

                }

            }

            if (tipoproducto.equals("BANCA CELULAR")) {

                if (tipologia.equals("DESAFILIACIÓN/ACTUALIZACIÓN DE NÚMERO CELULAR")) {
                    //TODO: Ingresa - Celular
                    Serenity.getDriver().findElement(By.id("cellPhoneNumber")).sendKeys("959149978");
                }

                if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA")) {
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Ingresa - Celular
                    Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("941916922");
                    //TODO: Seleccionar - Canal utlizado / reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

            }

            if (tipoproducto.equals("COBRANZAS")) {

                if (tipologia.equals("CONSTANCIA DE NO ADEUDO")) {
                    //TODO: Ingresa - N° de crédito
                    Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("32322316");
                }

                if (tipologia.equals("DIRECCIÓN/TELF. INDEBIDOS DE COBRANZA")) {

                    String tipoProducto = Serenity.sessionVariableCalled("tipoProducto").toString();

                    if (tipoProducto.equals("Credito")) {
                        tipoProducto = "Crédito";

                    }
                    if (tipoProducto.equals("Tarjeta de Credito")) {
                        tipoProducto = "Tarjeta de Crédito";

                    }
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();

                    //TODO: Seleccionar - Tipo de producto
                    Serenity.getDriver().findElement(By.id("productType")).click();
                    Serenity.getDriver().findElement(By.xpath("(//*[@id='" + tipoProducto + "'])")).click();
                    //TODO: Ingresa - N° de tipo de producto
                    Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("11149292");
                    //TODO: Ingresa - Nombre del deudor
                    Serenity.getDriver().findElement(By.id("debtorName")).sendKeys("Eduardo Alexis");
                    //TODO: Seleccionar - Canal utlizado / reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueCanal + "'])[last()]")).click();

                }

                if (tipologia.equals("COBROS INDEBIDOS")) {

                    String tipoProducto = Serenity.sessionVariableCalled("tipoProducto").toString();
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();

                    //TODO: Seleccionar - Tipo de producto
                    Serenity.getDriver().findElement(By.id("productType")).click();
                    Serenity.getDriver().findElement(By.xpath("(//*[@id='" + tipoProducto + "'])")).click();
                    //TODO: Ingresa - N° de tipo de producto
                    Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("11149292");
                    //TODO: Seleccionar - Canal utlizado / reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueCanal + "'])[last()]")).click();

                }
                if (tipologia.equals("REPORTE INDEBIDO CENTRALES DE RIESGO")) {

                    String tipoProducto = Serenity.sessionVariableCalled("tipoProducto").toString();
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();

                    //TODO: Seleccionar - Tipo de producto
                    Serenity.getDriver().findElement(By.id("productType")).click();
                    Serenity.getDriver().findElement(By.xpath("(//*[@id='" + tipoProducto + "'])")).click();
                    //TODO: Ingresa - N° de tipo de producto
                    Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("11149292");
                    //TODO: Seleccionar - Canal utlizado / reclamado
                    Serenity.getDriver().findElement(By.id("channel")).click();
                    Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueCanal + "'])[last()]")).click();

                }
                if (tipologia.equals("RECTIFICACIÓN SBS")) {
                    //TODO: Ingresa - N° de tarjeta
                    Serenity.getDriver().findElement(By.id("cardNumber")).sendKeys("1234567891011");
                    //TODO: Ingresa - N° de crédito
                    Serenity.getDriver().findElement(By.id("creditNumber")).sendKeys("12345678");
                }

            }

            if (tipoproducto.equals("DEBITO AUTOMATICO")) {

                if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA") || tipologia.equals("COBROS INDEBIDOS")) {
                    String formaDePago = Serenity.sessionVariableCalled("formaDePago").toString();
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();

                    //TODO: Seleccionar - Forma de pago
                    Serenity.getDriver().findElement(By.id("paymentForm")).click();
                    Serenity.getDriver().findElement(By.id(formaDePago)).click();

                    if (formaDePago.equals("Cuenta") || formaDePago.equals("Tarjeta de Credito")) {
                        //TODO: Ingresa - N° de forma de pago
                        Serenity.getDriver().findElement(By.xpath("(//input[@formcontrolname='paymentNumber'])[last()]")).sendKeys("12345678");
                        //TODO: Seleccionar - Canal utilizado / reclamado
                        Serenity.getDriver().findElement(By.id("usedChannel")).click();
                        Serenity.getDriver().findElement(By.id(valueCanal)).click();
                    }

                    if (formaDePago.equals("Efectivo")) {
                        //TODO: Seleccionar - Canal utilizado / reclamado
                        Serenity.getDriver().findElement(By.id("usedChannel")).click();
                        Serenity.getDriver().findElement(By.id(valueCanal)).click();
                    }

                }

            }

            if (tipoproducto.equals("PAGO DE SERVICIO POR CANALES")) {

                if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA") || tipologia.equals("COBROS INDEBIDOS")) {
                    String formaDePago = Serenity.sessionVariableCalled("formaDePago").toString();
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();

                    //TODO: Seleccionar - Forma de pago
                    Serenity.getDriver().findElement(By.id("paymentForm")).click();
                    Serenity.getDriver().findElement(By.id(formaDePago)).click();

                    if (formaDePago.equals("Cuenta") || formaDePago.equals("Tarjeta de Credito")) {
                        //TODO: Ingresa - N° de forma de pago
                        Serenity.getDriver().findElement(By.xpath("(//input[@formcontrolname='paymentNumber'])[last()]")).sendKeys("12345678");
                        //TODO: Seleccionar - Canal utlizado / reclamado
                        Serenity.getDriver().findElement(By.id("usedChannel")).click();
                        Serenity.getDriver().findElement(By.id(valueCanal)).click();
                    }

                    if (formaDePago.equals("Efectivo")) {
                        //TODO: Seleccionar - Canal utlizado / reclamado
                        Serenity.getDriver().findElement(By.id("usedChannel")).click();
                        Serenity.getDriver().findElement(By.id(valueCanal)).click();
                    }

                }

                if (tipologia.equals("ATM DE IB NO DEPOSITÓ DINERO")) {
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Ingresa - Ubicación del cajero
                    Serenity.getDriver().findElement(By.id("locationCashier")).sendKeys("MIRAFLORES");
                    //TODO: Seleccionar - Canal utlizado / reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

                if (tipologia.equals("ATENCIÓN IBK AGENTE")) {
                    //TODO: Ingresa - Ubicación del cajero
                    Serenity.getDriver().findElement(By.id("locationStore")).sendKeys("MIRAFLORES");
                }

                if (tipologia.equals("CORRECCIÓN DE PAGO REALIZADO")) {
                    //TODO: Ingresa - Nro de cuenta
                    Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("1007003469143");
                }

            }

            if (tipoproducto.equals("PODER")) {

                if (tipologia.equals("SOLICITUD VALIDACIÓN DE PODERES LIMA")) {
                    Serenity.getDriver().findElement(By.id("ownerName")).sendKeys("Eduardo Alexis");
                    Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("10209282930");
                }

            }

            if (tipoproducto.equals("REMESAS")) {

                if (tipologia.equals("ATM DE IB NO ENTREGÓ DINERO")) {
                    String cuenta = ApiCommons.clientes.getNumerocuenta();
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();

                    //TODO: Ingresa - Nro de cuenta
                    Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys(cuenta);
                    //TODO: Ingresa - Ubicación del cajero
                    Serenity.getDriver().findElement(By.id("locationCashier")).sendKeys("MIRAFLORES");
                    //TODO: Ingresa - Código de remesa
                    Serenity.getDriver().findElement(By.id("remittanceCode")).sendKeys("2005262");
                    //TODO: Seleccionar - Canal utlizado / reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

                if (tipologia.equals("CONSTANCIA POR TRANSFERENCIAS RECIBIDAS")) {
                    String tipoRemesa = Serenity.sessionVariableCalled("tipoRemesa").toString();

                    //TODO: Seleccionar - Tipo de remesa
                    Serenity.getDriver().findElement(By.id("remittanceType")).click();
                    Serenity.getDriver().findElement(By.id(tipoRemesa)).click();
                    //TODO: Ingresa - Periodo de inicio
                    Serenity.getDriver().findElement(By.id("startPeriod")).sendKeys(getValueFecha());
                    //TODO: Ingresa - Periodo de fin
                    Serenity.getDriver().findElement(By.id("endPeriod")).sendKeys(getValueFecha());
                }

                if (tipologia.equals("COBROS INDEBIDOS")) {
                    String tipoRemesa = Serenity.sessionVariableCalled("tipoRemesa").toString();

                    //TODO: Seleccionar - Tipo de remesa
                    Serenity.getDriver().findElement(By.id("remittanceType")).click();
                    Serenity.getDriver().findElement(By.id(tipoRemesa)).click();
                    //TODO: Ingresa - Código de remesa
                    Serenity.getDriver().findElement(By.id("remittanceCode")).sendKeys("2005262");
                }

                if (tipologia.equals("NO RECEPCIÓN DE TRANSFERENCIA IN/OUT")) {
                    String cuenta = ApiCommons.clientes.getNumerocuenta();

                    //TODO: Ingresa - Nombre del Banco Emisor/Destino
                    Serenity.getDriver().findElement(By.id("bankname")).sendKeys("INTERBANK");
                    //TODO: Ingresa - Número de cuenta
                    // 1007003469143
                    Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys(cuenta);
                }

                if (tipologia.equals("CONSUMO/ RETIRO NO EFECTUADO")) {
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Ingresa - Nro de cuenta
                    Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("123123123123123");
                    //TODO: Seleccionar - Canal utlizado / reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

                if (tipologia.equals("ATENCIÓN IBK AGENTE")) {
                    //TODO: Ingresa - Ubicación del cajero
                    Serenity.getDriver().findElement(By.id("locationStore")).sendKeys("MIRAFLORES");
                }

            }

            if (tipoproducto.equals("TRANSF. NACIONALES")) {

                if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA")) {
                    String tarjeta = ApiCommons.clientes.getNumerotarjeta();
                    String cuenta = ApiCommons.clientes.getNumerocuenta();
                    String valueTipoTransferencia = Serenity.sessionVariableCalled("tipoTransferencia").toString();
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();

                    //TODO: Ingresa - Tarjeta
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).sendKeys(tarjeta);
                    //TODO: Ingresa - Cuenta
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[last()]")).sendKeys(cuenta);
                    //TODO: Ingresa - Tipo de transferencia
                    Serenity.getDriver().findElement(By.id("typeOfTransfer")).click();
                    Serenity.getDriver().findElement(By.id(valueTipoTransferencia)).click();
                    //TODO: Seleccionar - Canal utlizado / reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

                if (tipologia.equals("COBROS INDEBIDOS")) {
                    String valueTipoTransferencia = Serenity.sessionVariableCalled("tipoTransferencia").toString();
                    String valueNumeroCuenta = ApiCommons.clientes.getNumerocuenta();
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();

                    //TODO: Ingresa - Tipo de transferencia
                    Serenity.getDriver().findElement(By.id("typeOfTransfer")).click();
                    Serenity.getDriver().findElement(By.id(valueTipoTransferencia)).click();
                    //TODO: Ingresa - N° de cuenta
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[last()]")).sendKeys(valueNumeroCuenta);
                    //TODO: Seleccionar - Canal utilizado/reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

            }

            if (tipoproducto.equals("BILLETERA ELECTRÓNICA")) {

                if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA")) {
                    String cuenta = ApiCommons.clientes.getNumerocuenta();
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();

                    //TODO: Ingresa - Cuenta
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).sendKeys(cuenta);
                    //TODO: Ingresa - Celular
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[last()]")).sendKeys("941916922");
                    //TODO: Seleccionar - Canal utlizado / reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

                if (tipologia.equals("INFORMACION DE MOVIMIENTOS")) {
                    String cuenta = ApiCommons.clientes.getNumerocuenta();
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Ingresa - Cuenta
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).sendKeys(cuenta);
                    //TODO: Seleccionar - Canal utlizado / reclamado
                    Serenity.getDriver().findElement(By.id("channelUsed")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

                if (tipologia.equals("CONSUMO/ RETIRO NO EFECTUADO")) {
                    String cuenta = ApiCommons.clientes.getNumerocuenta();
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Ingresa - Cuenta
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).sendKeys(cuenta);
                    //TODO: Seleccionar - Canal utlizado / reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

            }

            if (tipoproducto.equals("TOKEN")) {

                if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA")) {
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Ingresa - Cuenta
                    Serenity.getDriver().findElement(By.id("tokenSerialNumber")).sendKeys("AAAABBBBCCCC");
                    //TODO: Ingresa - Celular
                    Serenity.getDriver().findElement(By.id("contactPhone")).sendKeys("941916922");
                    //TODO: Seleccionar - Canal utlizado / reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

            }

            if (tipoproducto.equals("TUNKI")) {

                if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA")) {
                    String tarjeta =ApiCommons.clientes.getNumerotarjeta();
                    String cuenta = ApiCommons.clientes.getNumerocuenta();
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Ingresa - Cuenta
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).sendKeys(cuenta);
                    //TODO: Ingresa - Tarjeta
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[last()]")).sendKeys(tarjeta);
                    //TODO: Seleccionar - Canal utlizado / reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

                if (tipologia.equals("ATM DE IB NO ENTREGÓ DINERO")) {
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Ingresa - Ubicación del cajero
                    Serenity.getDriver().findElement(By.id("locationCashier")).sendKeys("Torre Bloom");
                    //TODO: Seleccionar - Canal utlizado / reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

            }

            if (tipoproducto.equals("TRANSF. INTERNACIONALES")) {

                if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA") || tipologia.equals("COBROS INDEBIDOS")) {
                    String cuenta = ApiCommons.clientes.getNumerocuenta();
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Ingresa - Cuenta
                    Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys(cuenta);
                    //TODO: Seleccionar - Canal utlizado / reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }
                if (tipologia.equals("CONSTANCIA POR TRANSFERENCIAS RECIBIDAS")) {
                    //TODO: Ingresa - Cuenta
                    Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("123123123123123");
                    //TODO: Ingresa - Fecha Operación
                    Serenity.getDriver().findElement(By.id("operationDate")).sendKeys("12/12/2022");
                }
                if (tipologia.equals("INFORMACION DE MOVIMIENTOS")) {
                    String valueTipoTransferencia = Serenity.sessionVariableCalled("tipoTransferencia").toString();
                    //TODO: Ingresa - Tipo de transferencia
                    Serenity.getDriver().findElement(By.id("typeOfTransfer")).click();
                    Serenity.getDriver().findElement(By.id(valueTipoTransferencia)).click();
                    //TODO: Ingresa - Cuenta
                    Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("123123123123123");
                }
                if (tipologia.equals("NO RECEPCIÓN DE TRANSFERENCIA IN/OUT")) {
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Ingresa - Cuenta
                    Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("123123123123123");
                    //TODO: Ingresa - Nombre de Banco Emisor/Destino
                    Serenity.getDriver().findElement(By.id("destinationIssuingBank")).sendKeys("IBK");
                    //TODO: Seleccionar - Canal utlizado / reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }


            }

            if (tipoproducto.equals("PAY PAL")) {

                if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA")) {
                    String cuenta = ApiCommons.clientes.getNumerocuenta();
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Ingresa - Cuenta
                    Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys(cuenta);
                    //TODO: Seleccionar - Canal utlizado / reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

            }

            if (tipoproducto.equals("IB AGENTE")) {

                if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA")) {
                    String tarjeta = ApiCommons.clientes.getNumerotarjeta();
                    String cuenta = ApiCommons.clientes.getNumerocuenta();
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Ingresa - Cuenta
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).sendKeys(cuenta);
                    //TODO: Ingresa - Tarjeta
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[last()]")).sendKeys(tarjeta);
                    //TODO: Seleccionar - Canal utlizado / reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

                if (tipologia.equals("COBROS INDEBIDOS")) {
                    String valueCuenta =ApiCommons.clientes.getNumerocuenta();
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Ingresa - N° de cuenta
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).sendKeys(valueCuenta);
                    //TODO: Seleccionar - Canal utilizado/reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

            }

            if (tipoproducto.equals("PLIN")) {

                if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA")) {
                    String tarjeta = ApiCommons.clientes.getNumerotarjeta();
                    String cuenta = ApiCommons.clientes.getNumerocuenta();
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Ingresa - Cuenta
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).sendKeys(cuenta);
                    //TODO: Ingresa - Tarjeta
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[last()]")).sendKeys(tarjeta);
                    //TODO: Seleccionar - Canal utlizado / reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

                if (tipologia.equals("ATM DE IB NO ENTREGÓ DINERO")) {
                    String valueCuenta =ApiCommons.clientes.getNumerocuenta();
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();

                    //TODO: Ingresa - N° de celular
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).sendKeys("959149978");
                    //TODO: Ingresa - N° de cuenta
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[2]")).sendKeys(valueCuenta);
                    //TODO: Seleccionar - Cógido Cajero
                    Serenity.getDriver().findElement(By.id("codeATM")).sendKeys("COD123");
                    //TODO: Seleccionar - Canal utilizado / reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

            }

            if (tipoproducto.equals("BANCA POR INTERNET")) {

                if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA")) {
                    String tarjeta = ApiCommons.clientes.getNumerotarjeta();
                    String cuenta = ApiCommons.clientes.getNumerocuenta();
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Ingresa - Cuenta
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).sendKeys(cuenta);
                    //TODO: Ingresa - Tarjeta
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[last()]")).sendKeys(tarjeta);
                    //TODO: Seleccionar - Canal utlizado / reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

            }

            if (tipoproducto.equals("PAGOS PLANILLAS/PROVEEDORES")) {

                if (tipologia.equals("INFORMACION DE MOVIMIENTOS")) {
                    //TODO: Ingresa - Cuenta
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).sendKeys("123123123123123123");
                }

                if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA")) {
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Ingresa - Cuenta
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).sendKeys("123123123123123123");
                    //TODO: Seleccionar - Canal utlizado / reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

                if (tipologia.equals("ACTIVACIÓN DE ORDENES DE PAGO")) {
                    //TODO: Ingresa - Nombre Ordenante de la OP original
                    Serenity.getDriver().findElement(By.id("OPNameOriginal")).sendKeys("JUNIOR LUIS");
                    //TODO: Ingresa - Fecha de operación
                    Serenity.getDriver().findElement(By.id("operationDate")).sendKeys(getValueFecha());
                    //TODO: Ingresa - N° OP original
                    Serenity.getDriver().findElement(By.id("OPOriginal")).sendKeys("ABCDEFABCDEFABCDEFABCDEFABCDEFABCDEFABCDEFABCDEFABCDEFABCDEFABCDEFABCDEFABCDEFABCDEFABCDEFABCDEFABCD");
                    //TODO: Ingresa - Nombre del Titular
                    Serenity.getDriver().findElement(By.id("accountHolder")).sendKeys("JUAN CARLOS");
                    //TODO: Ingresa - Monto
                    Serenity.getDriver().findElement(By.id("amount")).sendKeys("123,456.78");
                }

                if (tipologia.equals("COBROS INDEBIDOS")) {
                    String valueCuenta = ApiCommons.clientes.getNumerocuenta();
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Ingresa - N° de cuenta
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).sendKeys(valueCuenta);
                    //TODO: Seleccionar - Canal utilizado/reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

            }

            if (tipoproducto.equals("RETIRO SIN TARJETA")) {

                if (tipologia.equals("ATENCIONES BOGOTÁ")) {
                    //TODO: Ingresa - N° de Cuenta Opcional
                    Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("123123123123123123");
                    //TODO: Ingresa - N° de Tarjeta Crédito Opcional
                    Serenity.getDriver().findElement(By.id("cardNumber")).sendKeys("123123123123123123");
                    //TODO: Ingresa - Ubicación del Cajero
                    Serenity.getDriver().findElement(By.id("locationCashier")).sendKeys("Torre Bloom");

                }

            }

            if (tipoproducto.equals("CUENTA JUBILACIÓN")) {

                if (tipologia.equals("DEPÓSITO A PLAZO - JUBILACIÓN")) {
                    //TODO: Ingresa - Nro de cuenta
                    String cuenta =ApiCommons.clientes.getNumerocuenta();
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).sendKeys(cuenta);
                    //TODO: Ingresa - Fecha de operación
                    Serenity.getDriver().findElement(By.id("operationDate")).sendKeys(getValueFecha());
                    //TODO: Ingresa - Plazo
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[2]")).sendKeys("6");
                    //TODO: Ingresa - Tasa
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[last()]")).sendKeys("6");
                    //TODO: Ingresa - Monto
                    Serenity.getDriver().findElement(By.id("amount")).sendKeys("150");
                    //TODO: Seleccionar - Moneda
                    Serenity.getDriver().findElement(By.id("curency")).click();
                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='curency']//li)[1]")).click();
                }

            }

            if (tipoproducto.equals("TRASLADOS CTS")) {

                if (tipologia.equals("TRASLADO DE CTS DIGITAL A INTERBANK") || tipologia.equals("TRASLADO DE CTS FÍSICO A INTERBANK")) {
                    String valueCuentaSueldo = Serenity.sessionVariableCalled("cuentaSueldo").toString();
                    String valueSalaryAccountCreation = Serenity.sessionVariableCalled("salaryAccountCreation").toString();

                    //TODO: Ingresa - Centro Trabajo
                    Serenity.getDriver().findElement(By.id("workCenter")).sendKeys("NTTDATA");
                    //TODO: Selecciona - Entidad financiera
                    Serenity.getDriver().findElement(By.id("financialEntity")).click();
                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='financialEntity']//li)[1]")).click();
                    //TODO: Ingresa - RUC
                    Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("20521586134");
                    //TODO: Selecciona - ¿Tiene cuenta sueldo?
                    Serenity.getDriver().findElement(By.id("salaryAccountValid")).click();
                    Serenity.getDriver().findElement(By.id(valueCuentaSueldo)).click();
                    //TODO: Ingresa - Teléfono fijo cliente
                    Serenity.getDriver().findElement(By.id("landLineCustomer")).sendKeys("959149978");
                    //TODO: Ingresa - Teléfono trabajo
                    Serenity.getDriver().findElement(By.id("phoneWork")).sendKeys("959149978");
                    //TODO: Selecciona - Oferta
                    Serenity.getDriver().findElement(By.id("offer")).click();
                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='offer']//li)[1]")).click();
                    //TODO: Ingresa - Lugar de entrega
                    Serenity.getDriver().findElement(By.id("placeDelivery")).sendKeys("DELIVERY");
                    //TODO: Selecciona - Se creo cuenta sueldo al cliente?
                    Serenity.getDriver().findElement(By.id("salaryAccountCreation")).click();
                    Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueSalaryAccountCreation + "'])[2]")).click();
                }

            }

            if (tipoproducto.equals("SERVICIOS")) {

                if (tipologia.equals("ATENCIÓN IBK AGENTE")) {
                    //TODO: Ingresa - Ubicación Agente
                    Serenity.getDriver().findElement(By.id("locationAgent")).sendKeys("MIRAFLORES");
                    //TODO: Ingresa - N° de celular
                    Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("961773012");
                }

            }

            if (tipoproducto.equals("SUNAT")) {

                if (tipologia.equals("CONSTANCIA DE PAGO REALIZADO")) {
                    String valueNumeroTarjeta = ApiCommons.clientes.getNumerotarjeta();
                    String valueCuenta = ApiCommons.clientes.getNumerocuenta();
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();

                    //TODO: Ingresa - N° de Tarjeta
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).sendKeys(valueNumeroTarjeta);
                    //TODO: Ingresa - N° de cuenta
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[2]")).sendKeys(valueCuenta);
                    //TODO: Seleccionar - Canal utilizado/reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

                if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA")) {
                    String valueNumeroTarjeta = ApiCommons.clientes.getNumerotarjeta();
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();

                    //TODO: Ingresa - N° de tarjeta
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).sendKeys(valueNumeroTarjeta);
                    //TODO: Seleccionar - Canal utilizado/reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

                if (tipologia.equals("CORRECCIÓN DE PAGO REALIZADO")) {
                    String valueCuenta = ApiCommons.clientes.getNumerocuenta();
                    String valueMedioPago = Serenity.sessionVariableCalled("medioPago").toString();

                    //TODO: Ingresa - N° de cuenta (Opcional)
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).sendKeys(valueCuenta);
                    //TODO: Seleccionar - Medio de pago
                    Serenity.getDriver().findElement(By.id("paymentMethod")).click();
                    Serenity.getDriver().findElement(By.id(valueMedioPago)).click();
                }

                if (tipologia.equals("COBROS INDEBIDOS")) {
                    String valueCuenta = ApiCommons.clientes.getNumerocuenta();
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();

                    //TODO: Ingresa - N° de cuenta
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).sendKeys(valueCuenta);
                    //TODO: Seleccionar - Canal utilizado/reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }

            }

            if (tipoproducto.equals("MONEY EXCHANGE APP")) {

                if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA")) {
                    String valueCuenta = ApiCommons.clientes.getNumerocuenta();
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Ingresa - N° de cuenta
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).sendKeys(valueCuenta);
                    //TODO: Seleccionar - Canal utilizado/reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id(valueCanal)).click();
                }
            }

            //TODO: TIPOLOGIAS DE ATENCIÓN
            if (tipoproducto.equals("ATENCION AL CLIENTE")) {

                if (tipologia.equals("MODIFICACIÓN DATOS PERSONALES")) {
                    //TODO: Departamento (*)
                    Serenity.getDriver().findElement(By.id("department")).click();
                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='department']//li)[1]")).click();
                    //TODO: Provincia (*)
                    Serenity.getDriver().findElement(By.id("province")).click();
                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='province']//li)[1]")).click();
                    //TODO: Distrito (*)
                    Serenity.getDriver().findElement(By.id("district")).click();
                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='district']//li)[1]")).click();
                    //TODO: Tipo de via (*)
                    Serenity.getDriver().findElement(By.id("streetType")).click();
                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='streetType']//li)[14]")).click();
                    //TODO: Nombre de vía (*)
                    Serenity.getDriver().findElement(By.id("trackName")).sendKeys("Andromeda");
                    //TODO: Urbanización
                    Serenity.getDriver().findElement(By.xpath("//*[@id='urbanization ']")).sendKeys("La Paz");
                    //TODO: Número
                    Serenity.getDriver().findElement(By.id("streetNumber")).sendKeys("850");
                    //TODO: Manzana
                    Serenity.getDriver().findElement(By.id("block")).sendKeys("J");
                    //TODO: Lote
                    Serenity.getDriver().findElement(By.id("lot")).sendKeys("7B");
                    //TODO: Interior
                    Serenity.getDriver().findElement(By.id("apartment")).sendKeys("20");
                    //TODO: Referencia
                    Serenity.getDriver().findElement(By.xpath("//textarea[@formcontrolname='reference']")).sendKeys("GS4 Seguridad del Peru");
                    //TODO: Numero Celular
                    Serenity.getDriver().findElement(By.id("cellPhoneNumber")).sendKeys("923568122");
                    //TODO: Selecciona Operador
                    Serenity.getDriver().findElement(By.id("tab-aditional-MOVISTAR")).click();
                    //TODO: Nuevo email
                    Serenity.getDriver().findElement(By.id("personalEmail")).sendKeys("SILVAJAR@GMAIL.COM");
                }

                if (tipologia.equals("VIDEO ARCO")) {
                    //TODO: Ingresar Ubicación cajero / tienda
                    Serenity.getDriver().findElement(By.id("locationStore")).sendKeys("Torre Bloom");
                    //TODO: Ingresar Fecha de operación
                    Serenity.getDriver().findElement(By.id("operationDate")).sendKeys("12/12/2022");
                    //TODO: Ingresar N° tienda utilizado
                    Serenity.getDriver().findElement(By.id("numberStore")).sendKeys("123");
                    //TODO: Ingresar Rango Horario
                    Serenity.getDriver().findElement(By.id("sheduleRange")).sendKeys("8am");
                    //TODO: Secuencia/Ubicación cámara de requerimiento
                    Serenity.getDriver().findElement(By.id("suRequestChamber")).sendKeys("1011");
                    //TODO: Secuencia/Ubicación cámara hoy
                    Serenity.getDriver().findElement(By.id("suChamber")).sendKeys("1015");
                    //TODO: Ingresa Medio para la Entrega
                    Serenity.getDriver().findElement(By.id("mediumForDelivery")).click();
                    Serenity.getDriver().findElement(By.id("Envío de Link por correo")).click();
                }

                if (tipologia.equals("BILLETE FALSO")) {
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Ingresa - Nro. de cuenta
                    Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("13131313131313");
                    //TODO: Seleccionar - Canal utlizado / reclamado
                    Serenity.getDriver().findElement(By.id("channelClaimed")).click();
                    Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueCanal + "'])[last()]")).click();
                }

                if (tipologia.equals("PROTECCIÓN DE DATOS - DERECHO ARCO")) {
                    //TODO: Ingresar N° Tarjeta Opcional
                    Serenity.getDriver().findElement(By.id("cardNumber")).sendKeys("123123123123123");
                    //TODO: Ingresar N° Crédito Opcional
                    Serenity.getDriver().findElement(By.id("creditNumber")).sendKeys("1212121212121");
                }

                if (tipologia.equals("CORREO/TELÉFONO INDEBIDO DE CAMPAÑAS")) {
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Ingresa - Nombre del Titular
                    Serenity.getDriver().findElement(By.id("accountHolder")).sendKeys("EDWARD");
                    //TODO: Seleccionar - Canal utilizado/reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueCanal + "'])[last()]")).click();
                }

                if (tipologia.equals("INSATISFACCIÓN GESTIÓN DE ENTREGA DE TARJETA")) {
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Ingresa - N° de tarjeta
                    Serenity.getDriver().findElement(By.id("cardNumber")).sendKeys("4213555322311541");
                    //TODO: Seleccionar - Canal utilizado/reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueCanal + "'])[last()]")).click();
                }

                if (tipologia.equals("DESACUERDO CON EL SERVICIO")) {
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Seleccionar - Canal utilizado/reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueCanal + "'])[last()]")).click();
                }


            }

        }


    }


    public String getValueFecha() {
        //TODO: ADD FECHA AUTOGENERADO
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
        String valueDate = dateFormat.format(date);
        return valueDate;
    }


    //TODO: REGISTRA - TIPOLOGIAS CON TRANSACCION - MOVIMIENTO
    private void registraMovimiento(int i, String motivo) {

        //int cantidadmontos = Integer.parseInt(cantidadmontomotivo);
        //agregado solo para data adicional
        String cantidadmontosT = cantidadmontomotivo;
        String[] listacantidad = cantidadmontosT.split("-");
        //agregado solo para data adicional
        String valueMonto = "";
        valueMonto = montoMovimiento;


        //String valueTipoMoneda = "";
        //agregado para data adicional
        //valueTipoMoneda = tipomoneda;
        String valueTipoMonedaT[] = tipomoneda.split("-");

        //TODO: VALIDO AUTONOMIA - ACTIVIDAD EN RESOLUCION AUTOMATICA
        String autonomia = "";
        try {
            autonomia = Serenity.sessionVariableCalled("autonomia");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        if (autonomia.equals("si")) {
            int montoAutonomia = Integer.parseInt(Serenity.sessionVariableCalled("montoAutonomia"));
            int montoAIngresar = Integer.parseInt(valueMonto);

            if (montoAutonomia < montoAIngresar) {
                throw new AssertionError("Monto excedido de autonomia, revisar cliente en heidisql si tiene los valores de segmento correctos, o si se ha ingresado el monto incorrectamente");
            } else {
                System.out.println("Monto correcto para autonomia, se ingresara dicho monto en el campo de motivo");
            }

        }
        //Agregado solo para data adicional
        int cantidadmontos = Integer.valueOf(listacantidad[0]);
        String valueTipoMoneda = valueTipoMonedaT[0];

        try {
             cantidadmontos = Integer.valueOf(listacantidad[i]);
        }catch (Exception e)
        {
            cantidadmontos = Integer.valueOf(listacantidad[0]);
        }
        try {
            valueTipoMoneda = valueTipoMonedaT[i];
        }catch (Exception e)
        {
            valueTipoMoneda = valueTipoMonedaT[0];
        }
        //TODO: TIPOLOGIAS- COMERCIAL
        if (esComercial.equals("COMERCIAL")) {

            for (int j = 0; j < cantidadmontos; j++) {

                //TODO: VARIABLE GLOBAL - TIPO PRODUCTO - TIPOLOGIA
                String tipoProducto = Serenity.sessionVariableCalled("tipoproducto").toString();
                String tipologia = Serenity.sessionVariableCalled("tipologia").toString();

                System.err.println("INGRESA TIPOLOGIA CON TRANSACCIÓN ES: " + tipologia);
                //SE AGREGA CAMPOS DE MOVIMIENTOS DE LA TIPOLOGIA
                if (tipoProducto.equals("CUENTA NEGOCIOS")) {
                    if (tipologia.equals("COBROS INDEBIDOS")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        int valornuevo = i+1;
                        //  Serenity.getDriver().findElement(By.id("Moneda_" + i + "_" + j)).click();
                        Serenity.getDriver().findElement(By.xpath("(//*[@id='Moneda'])["+valornuevo+"]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//*[@id='Moneda'])["+valornuevo+"]/div[2]/ul/li[@id='" + valueTipoMoneda + "']")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Hora de Operación
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Hora'])[last()]")).sendKeys("10:30");
                        //TODO: Ingresa - Establecimiento/Transaccion
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='EstablecimientoTransaccion'])[last()]")).sendKeys("RAPPI COMPRA");
                    }
                }

                //TODO: MOTIVO MOVIMIENTO - PRODUCTOS
                if (tipoProducto.equals("CRÉDITO BPE")) {

                    if (tipologia.equals("COBROS INDEBIDOS")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        int valornuevo = i+1;
                        //  Serenity.getDriver().findElement(By.id("Moneda_" + i + "_" + j)).click();
                        Serenity.getDriver().findElement(By.xpath("(//*[@id='Moneda_"+i+"_" + j + "'])")).click();
                        Serenity.getDriver().findElement(By.xpath("(//*[@id='Moneda_"+i+"_" + j + "'])/div[2]/ul/li[@id='" + valueTipoMoneda + "']")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto_"+i+"_"+j+"'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha_"+i+"_"+j+"'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Hora de Operación
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Hora_"+i+"_"+j+"'])[last()]")).sendKeys("10:30");
                        //TODO: Ingresa - Establecimiento/Transaccion
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Comercio"+i+"_"+j+"'])[last()]")).sendKeys("RAPPI COMPRA");
                    }

                }
                if (tipoProducto.equals("CUENTA CORRIENTE")) {
                    if (tipologia.equals("COBROS INDEBIDOS")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        int valornuevo = i+1;
                        //  Serenity.getDriver().findElement(By.id("Moneda_" + i + "_" + j)).click();
                        Serenity.getDriver().findElement(By.xpath("(//*[@id='Moneda'])["+valornuevo+"]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//*[@id='Moneda'])["+valornuevo+"]/div[2]/ul/li[@id='" + valueTipoMoneda + "']")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Hora de Operación
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Hora'])[last()]")).sendKeys("10:30");
                        //TODO: Ingresa - Establecimiento/Transaccion
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("RAPPI COMPRA");
                    }
                    if (tipologia.equals("EXONERACIÓN DE COBROS")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        int valornuevo = i+1;
                        //  Serenity.getDriver().findElement(By.id("Moneda_" + i + "_" + j)).click();
                        Serenity.getDriver().findElement(By.xpath("(//*[@id='Moneda'])["+valornuevo+"]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//*[@id='Moneda'])["+valornuevo+"]/div[2]/ul/li[@id='" + valueTipoMoneda + "']")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Hora de Operación
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Hora'])[last()]")).sendKeys("10:30");
                        //TODO: Ingresa - Establecimiento/Transaccion
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("RAPPI COMPRA");
                    }


                }

                if (tipoProducto.equals("TARJETA DE DÉBITO")) {
                    if (tipologia.equals("CONSUMOS NO RECONOCIDOS POR FRAUDE")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        int valornuevo = i+1;
                        //  Serenity.getDriver().findElement(By.id("Moneda_" + i + "_" + j)).click();
                        Serenity.getDriver().findElement(By.xpath("(//*[@id='Moneda'])["+valornuevo+"]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//*[@id='Moneda'])["+valornuevo+"]/div[2]/ul/li[@id='" + valueTipoMoneda + "']")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transaccion
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("RAPPI COMPRA");
                    }
                    if (tipologia.equals("CONSUMOS MAL PROCESADOS POR EL COMERCIO/MARCA")) {
                       if(j==0){
                        //TODO: Agregar fila de nuevo movimiento
                        Serenity.getDriver().findElement(By.xpath("(//*[@id='Add_Amount_"+i+"'])")).click();
                       }
                        //TODO: Selecciona combo - Tipo Moneda
                        //  Serenity.getDriver().findElement(By.id("Moneda_" + i + "_" + j)).click();
                        Serenity.getDriver().findElement(By.xpath("(//*[@id='Moneda_"+i+"_" + j + "'])")).click();
                        Serenity.getDriver().findElement(By.xpath("(//*[@id='Moneda_"+i+"_" + j + "'])/div[2]/ul/li[@id='" + valueTipoMoneda + "']")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto_"+i+"_"+j+"'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha_"+i+"_"+j+"'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transaccion
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Comercio"+i+"_" + j + "'])[last()]")).sendKeys("RAPPI COMPRA");
                            //Campos especiales para el motivo: Se cargó consumo no concretado y desistió de la compra
                            if(motivo.equals("2468")||motivo.equals("1575")){
                                //TODO: Se intento adquirir Producto o Servicio
                                String valuePoductoServicio = Serenity.sessionVariableCalled("productoServicioAdquirido").toString();
                                if(valuePoductoServicio.equals("Producto")) {
                                    Serenity.getDriver().findElement(By.xpath("//label[@for='ps_PS001_"+i+"_"+j+"']")).click();
                                }else if(valuePoductoServicio.equals("Servicio")) {
                                    Serenity.getDriver().findElement(By.xpath("//label[@for='ps_PS002_"+i+"_"+j+"']")).click();
                                    // TODO: Seleccionar Servicio
                                    String valueNomServicio = Serenity.sessionVariableCalled("tipoServicio").toString();
                                    Serenity.getDriver().findElement(By.xpath("(//*[@id='tp_"+i+"_" + j + "'])")).click();
                                    //Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueNomServicio + "'])[last()]")).click();
                                    Serenity.getDriver().findElement(By.xpath("(//span[contains(text(),'"+valueNomServicio+"')])")).click();


                                }
                                Serenity.getDriver().findElement(By.xpath("(//input[@id='hopeDate_"+i+"_" + j + "'])[last()]")).sendKeys(getValueFecha());
                            }

                        }

                    if (tipologia.equals("CONSUMO/ RETIRO NO EFECTUADO")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        int valornuevo = i+1;
                        //  Serenity.getDriver().findElement(By.id("Moneda_" + i + "_" + j)).click();
                        llenarDatosMovimientoRezagados(valornuevo, valueTipoMoneda,valueMonto);
                    }

                }

                if (tipoProducto.equals("TARJETA DE CRÉDITO")) {
                    if (tipologia.equals("CONSUMOS NO RECONOCIDOS POR FRAUDE")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        int valornuevo = i+1;
                        //  Serenity.getDriver().findElement(By.id("Moneda_" + i + "_" + j)).click();
                        Serenity.getDriver().findElement(By.xpath("(//*[@id='Moneda'])["+valornuevo+"]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//*[@id='Moneda'])["+valornuevo+"]/div[2]/ul/li[@id='" + valueTipoMoneda + "']")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transaccion
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("RAPPI COMPRA");
                    }
                    if (tipologia.equals("CONSUMOS MAL PROCESADOS POR EL COMERCIO/MARCA")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        int valornuevo = i+1;
                        //  Serenity.getDriver().findElement(By.id("Moneda_" + i + "_" + j)).click();
                        Serenity.getDriver().findElement(By.xpath("(//*[@id='Moneda'])["+valornuevo+"]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//*[@id='Moneda'])["+valornuevo+"]/div[2]/ul/li[@id='" + valueTipoMoneda + "']")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transaccion
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("RAPPI COMPRA");
                    }
                }

                if (tipoProducto.equals("SEGUROS RELACIONADOS")) {
                    if (tipologia.equals("COBROS INDEBIDOS")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        int valornuevo = i+1;
                        //  Serenity.getDriver().findElement(By.id("Moneda_" + i + "_" + j)).click();
                        llenarDatosMovimientoRezagados(valornuevo, valueTipoMoneda,valueMonto);

                    }
                }


                if (j != cantidadmontos - 1) {
                    Serenity.getDriver().findElement(By.id("Add_Amount_" + i)).click();
                }

            }

        } else {

            for (int j = 0; j < cantidadmontos; j++) {

                //TODO: VARIABLE GLOBAL - TIPO PRODUCTO - TIPOLOGIA
                String tipoProducto = Serenity.sessionVariableCalled("tipoproducto").toString();
                String tipologia = Serenity.sessionVariableCalled("tipologia").toString();

                System.err.println("INGRESA TIPOLOGIA CON TRANSACCIÓN ES: " + tipologia);

                //TODO: MOTIVO MOVIMIENTO - PRODUCTOS
                if (tipoProducto.equals("CUENTA")) {

                    if (tipologia.equals("EXONERACIÓN DE COBROS") || tipologia.equals("COBROS INDEBIDOS")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.id("Moneda_" + i + "_" + j)).click();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Moneda_" + i + "_" + j + "']/div[2]/ul/li[@id='" + valueTipoMoneda + "']")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@formcontrolname='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@formcontrolname='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Descripción
                        Serenity.getDriver().findElement(By.xpath("(//input[@formcontrolname='Descripcion'])[last()]")).sendKeys("RAPPI");
                    }

                    if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA") || tipologia.equals("CUENTA/CANCELACIÓN NO RECONOCIDA")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion'])[last()]")).sendKeys("TAMBO");
                    }

                    if (tipologia.equals("INFORMACION DE MOVIMIENTOS")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.id("Moneda_" + i + "_" + j)).click();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Moneda_" + i + "_" + j + "']/div[2]/ul/li[@id='" + valueTipoMoneda + "']")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@formcontrolname='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@formcontrolname='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento / Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@formcontrolname='Establecimiento'])[last()]")).sendKeys("RAPPI");
                    }

                    if (tipologia.equals("ATM DE IB NO DEPOSITÓ DINERO")) {
                        String valueImporteSolicitado = Serenity.sessionVariableCalled("importeSolicitado").toString();
                        String valueImporteReclamado = Serenity.sessionVariableCalled("importeReclamado").toString();

                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Importe Solicitado
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ImporteSolicitado'])[last()]")).sendKeys(valueImporteSolicitado);
                        //TODO: Ingresa - Importe Reclamado
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueImporteReclamado);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Hora de Operación
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Hora'])[last()]")).sendKeys("1030");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='EstablecimientoTransaccion'])[last()]")).sendKeys("TAMBO");
                    }

                    if (tipologia.equals("ATENCIÓN IBK AGENTE") || tipologia.equals("LIBERACIÓN DE RETENCIONES JUDICIALES")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys("100");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("TAMBO");
                    }

                }

                if (tipoProducto.equals("TARJETA DE CREDITO")) {

                    if (tipologia.equals("EXONERACIÓN DE COBROS") || tipologia.equals("COBROS INDEBIDOS")) {
                        //TODO: Ingresa - Fecha de movimiento
                        Serenity.getDriver().findElement(By.xpath("(//input[@formcontrolname='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.id("Moneda_" + i + "_" + j)).click();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Moneda_" + i + "_" + j + "']/div[2]/ul/li[@id='" + valueTipoMoneda + "']")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@formcontrolname='Monto'])[last()]")).sendKeys(valueMonto);
                    }

                    if (tipologia.equals("MODIFICACION PLAZO")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.id("Moneda_" + i + "_" + j)).click();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Moneda_" + i + "_" + j + "']/div[2]/ul/li[@id='" + valueTipoMoneda + "']")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@formcontrolname='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@formcontrolname='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento / Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@formcontrolname='Establecimiento'])[last()]")).sendKeys("Prueba Establecimiento");
                        //TODO: Ingresa - Cuotas Solicitadas
                        Serenity.getDriver().findElement(By.xpath("(//input[@formcontrolname='CuotasSolicitadas'])[last()]")).sendKeys("12");
                    }

                    if (tipologia.equals("PAGO ANTICIPADO Y ADELANTO DE CUOTAS") || tipologia.equals("DUPLICADO DE VOUCHER")) {
                        //TODO: Selecciona combo - Tipo Moneda ##MONEDA
                        //Serenity.getDriver().findElement(By.id("Moneda_" + i + "_" + j)).click();
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        //Serenity.getDriver().findElement(By.xpath("//*[@id='Moneda_" + i + "_" + j + "']/div[2]/ul/li[@id='" + valueTipoMoneda + "']")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Monto
                           Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento / Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("Prueba Establecimiento");
                    }

                    if (tipologia.equals("CONSUMOS NO RECONOCIDOS POR FRAUDE") || tipologia.equals("CONSUMOS MAL PROCESADOS POR EL COMERCIO/MARCA")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.id("Moneda_" + i + "_" + j)).click();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Moneda_" + i + "_" + j + "']/div[2]/ul/li[@id='" + valueTipoMoneda + "']")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@formcontrolname='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@formcontrolname='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Comercio
                        Serenity.getDriver().findElement(By.xpath("(//input[@formcontrolname='Comercio'])[last()]")).sendKeys("RAPPI");
                    }

                    if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento / Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("TAMBO");
                        //TODO: Selecciona combo - Canal utilizado / Reclamado
                        String canal = Serenity.sessionVariableCalled("canal").toString();
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='CanalUtilizadoReclamado'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//*[@id='" + canal + "'])[last()]")).click();
                    }

                    if (tipologia.equals("LIBERACION DE RETENCIONES") || tipologia.equals("OPERACIÓN DENEGADA") ||
                            tipologia.equals("ATENCIÓN IBK AGENTE") || tipologia.equals("CORRECCIÓN DE PAGO REALIZADO") ||
                            tipologia.equals("INFORMACIÓN DE ESTABLECIMIENTO/ RET.EFECTIVO") || tipologia.equals("INFORMACION DE MOVIMIENTOS") ||
                            tipologia.equals("ATM DE OTRO BANCO NO ENTREGÓ DINERO")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("TAMBO");
                    }

                    if (tipologia.equals("ATM DE IB NO DEPOSITÓ DINERO")) {
                        String valueImporteSolicitado = Serenity.sessionVariableCalled("importeSolicitado").toString();
                        String valueImporteReclamado = Serenity.sessionVariableCalled("importeReclamado").toString();
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Importe Solicitado
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ImporteSolicitado'])[last()]")).sendKeys(valueImporteSolicitado);
                        //TODO: Ingresa - Importe Reclamado
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueImporteReclamado);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Hora de Operación
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Hora'])[last()]")).sendKeys("1030");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='EstablecimientoTransaccion'])[last()]")).sendKeys("TAMBO");
                    }

                    if (tipologia.equals("ATM DE IB NO ENTREGÓ DINERO")) {
                        String valueImporteSolicitado = Serenity.sessionVariableCalled("importeSolicitado").toString();
                        String valueImporteReclamado = Serenity.sessionVariableCalled("importeReclamado").toString();
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Importe Solicitado
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ImporteSolicitado'])[last()]")).sendKeys(valueImporteSolicitado);
                        //TODO: Ingresa - Importe Reclamado
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueImporteReclamado);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='EstablecimientoTransaccion'])[last()]")).sendKeys("TAMBO");

                    }

                }

                if (tipoProducto.equals("TARJETA DE DEBITO")) {

                    if (tipologia.equals("EXONERACIÓN DE COBROS") || tipologia.equals("COBROS INDEBIDOS")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.id("Moneda_" + i + "_" + j)).click();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Moneda_" + i + "_" + j + "']/div[2]/ul/li[@id='" + valueTipoMoneda + "']")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@formcontrolname='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@formcontrolname='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Descripción
                        Serenity.getDriver().findElement(By.xpath("(//input[@formcontrolname='Descripcion'])[last()]")).sendKeys("RAPPI COMPRA");
                    }

                    if (tipologia.equals("CONSUMOS NO RECONOCIDOS POR FRAUDE") || tipologia.equals("CONSUMOS MAL PROCESADOS POR EL COMERCIO/MARCA")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.id("Moneda_" + i + "_" + j)).click();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Moneda_" + i + "_" + j + "']/div[2]/ul/li[@id='" + valueTipoMoneda + "']")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@formcontrolname='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@formcontrolname='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Descripción
                        Serenity.getDriver().findElement(By.xpath("(//input[@formcontrolname='Motivo'])[last()]")).sendKeys("RAPPI");
                    }

                    if (tipologia.equals("ATM DE IB NO ENTREGÓ DINERO")) {
                        String valueImporteSolicitado = Serenity.sessionVariableCalled("importeSolicitado").toString();
                        String valueImporteReclamado = Serenity.sessionVariableCalled("importeReclamado").toString();
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Importe Solicitado
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ImporteSolicitado'])[last()]")).sendKeys(valueImporteSolicitado);
                        //TODO: Ingresa - Importe Reclamado
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueImporteReclamado);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='EstablecimientoTransaccion'])[last()]")).sendKeys("TAMBO");
                    }

                    if (tipologia.equals("LIBERACION DE RETENCIONES") || tipologia.equals("OPERACIÓN DENEGADA") ||
                            tipologia.equals("ATM DE OTRO BANCO NO ENTREGÓ DINERO") || tipologia.equals("INFORMACIÓN DE ESTABLECIMIENTO/ RET.EFECTIVO")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("TAMBO");
                    }

                    if (tipologia.equals("REVISIÓN DE DEPÓSITOS A TERCEROS")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='EstablecimientoTransaccion'])[last()]")).sendKeys("TAMBO");
                    }

                }

                if (tipoProducto.equals("SEGUROS")) {

                    if (tipologia.equals("COBROS INDEBIDOS")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("TAMBO");
                    }

                    if (tipologia.equals("INFORMACION DE MOVIMIENTOS")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("TAMBO");
                    }

                }

                if (tipoProducto.equals("SEGUROS RELACIONADOS")) {

                    if (tipologia.equals("INFORMACION DE MOVIMIENTOS") || tipologia.equals("COBROS INDEBIDOS")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("TAMBO");
                    }

                }

                if (tipoProducto.equals("CBME")) {

                    if (tipologia.equals("COPIA DE BOLETA DE DEPÓSITO LIMA")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("TAMBO");
                    }

                }

                if (tipoProducto.equals("CUENTA CORRIENTE")) {

                    if (tipologia.equals("COPIA DE BOLETA DE DEPÓSITO LIMA")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("TAMBO");
                    }

                }

                if (tipoProducto.equals("ADELANTO SUELDO")) {

                    if (tipologia.equals("ANULACIÓN DE CRÉDITO") || tipologia.equals("COBROS INDEBIDOS")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("TAMBO");
                    }

                    if (tipologia.equals("CONSUMO/ RETIRO NO EFECTUADO")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='EstablecimientoTransaccion'])[last()]")).sendKeys("TAMBO");
                    }

                }

                if (tipoProducto.equals("LINEA CONVENIO")) {

                    if (tipologia.equals("ANULACIÓN DE CRÉDITO") || tipologia.equals("PAGO ANTICIPADO Y ADELANTO DE CUOTAS") ||
                            tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA") || tipologia.equals("DUPLICADO DE VOUCHER") ||
                            tipologia.equals("INFORMACION DE MOVIMIENTOS") || tipologia.equals("ENDOSO SEGUROS DE DESGRAVAMEN") ||
                            tipologia.equals("CONSUMO/ RETIRO NO EFECTUADO") || tipologia.equals("DEVOLUCIÓN CUOTAS EN TRÁNSITO")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("TAMBO");
                    }

                }

                if (tipoProducto.equals("HIPOTECARIO")) {

                    if (tipologia.equals("PAGO ANTICIPADO Y ADELANTO DE CUOTAS") || tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA") ||
                            tipologia.equals("COBROS INDEBIDOS") || tipologia.equals("EXONERACIÓN DE COBROS") ||
                            tipologia.equals("CONSUMO/ RETIRO NO EFECTUADO")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("TAMBO");
                    }

                }

                if (tipoProducto.equals("EFECTIVO CON GARANTÍA HIPOTECARIO")) {

                    if (tipologia.equals("EXONERACIÓN DE COBROS") || tipologia.equals("COBROS INDEBIDOS") ||
                            tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA") || tipologia.equals("PAGO ANTICIPADO Y ADELANTO DE CUOTAS") ||
                            tipologia.equals("CONSUMO/ RETIRO NO EFECTUADO")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha de operación
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("TAMBO");
                    }

                }

                if (tipoProducto.equals("PRÉSTAMO PERSONAL")) {

                    if (tipologia.equals("ANULACIÓN DE CRÉDITO") || tipologia.equals("EXONERACIÓN DE COBROS") ||
                            tipologia.equals("COBROS INDEBIDOS") || tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA") ||
                            tipologia.equals("PAGO ANTICIPADO Y ADELANTO DE CUOTAS") || tipologia.equals("CONSUMO/ RETIRO NO EFECTUADO")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha de operación
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("TAMBO");
                    }

                }

                if (tipoProducto.equals("VEHICULAR")) {

                    if (tipologia.equals("ANULACIÓN DE CRÉDITO") || tipologia.equals("EXONERACIÓN DE COBROS") ||
                            tipologia.equals("COBROS INDEBIDOS") || tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA") ||
                            tipologia.equals("PAGO ANTICIPADO Y ADELANTO DE CUOTAS") || tipologia.equals("CONSUMO/ RETIRO NO EFECTUADO")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha de operación
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("TAMBO");
                    }

                }

                if (tipoProducto.equals("REFINANCIADO")) {

                    if (tipologia.equals("ANULACIÓN DE CRÉDITO") || tipologia.equals("EXONERACIÓN DE COBROS") ||
                            tipologia.equals("COBROS INDEBIDOS") || tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA") ||
                            tipologia.equals("PAGO ANTICIPADO Y ADELANTO DE CUOTAS") || tipologia.equals("CONSUMO/ RETIRO NO EFECTUADO")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha de operación
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("TAMBO");
                    }

                }

                if (tipoProducto.equals("RAPPIBANK - TARJETA DE CRÉDITO")) {

                    if (tipologia.equals("ATM DE IB NO ENTREGÓ DINERO")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Importe Solicitado
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ImporteSolicitado'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Importe Reclamado
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("TAMBO COMPRA");
                    }

                    if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA") || tipologia.equals("COBROS INDEBIDOS") ||
                            tipologia.equals("CONSUMO/ RETIRO NO EFECTUADO") || tipologia.equals("LIBERACIÓN DE RETENCIONES") ||
                            tipologia.equals("OPERACIÓN DENEGADA")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("TAMBO");
                    }

                }

                if (tipoProducto.equals("RAPPIBANK - CUENTAS")) {

                    if (tipologia.equals("COBROS INDEBIDOS") || tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA") ||
                            tipologia.equals("CONSUMO/ RETIRO NO EFECTUADO") || tipologia.equals("CUENTA/CANCELACIÓN NO RECONOCIDA") ||
                            tipologia.equals("LIBERACIÓN DE RETENCIONES") || tipologia.equals("OPERACIÓN DENEGADA") ||
                            tipologia.equals("REVISIÓN DE DEPÓSITOS A TERCEROS")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("TAMBO");
                    }

                }

                //TODO: MOTIVO MOVIMIENTO - OPERACIONES
                if (tipoProducto.equals("TRANSF. NACIONALES")) {

                    if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA") || tipologia.equals("COBROS INDEBIDOS")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("TAMBO");
                    }

                }

                if (tipoProducto.equals("REMESAS")) {

                    if (tipologia.equals("ATM DE IB NO ENTREGÓ DINERO")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Importe Solicitado
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ImporteSolicitado'])[last()]")).sendKeys("150");
                        //TODO: Ingresa - Importe Reclamado
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys("100");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/ Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='EstablecimientoTransaccion'])[last()]")).sendKeys("TAMBO COMPRA");
                    }

                    if (tipologia.equals("COBROS INDEBIDOS") || tipologia.equals("NO RECEPCIÓN DE TRANSFERENCIA IN/OUT") || tipologia.equals("CONSUMO/ RETIRO NO EFECTUADO") || tipologia.equals("ATENCIÓN IBK AGENTE")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha de operación
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("TAMBO");
                    }

                }

                if (tipoProducto.equals("DEBITO AUTOMATICO")) {

                    if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA") || tipologia.equals("COBROS INDEBIDOS")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.id("Moneda_" + i + "_" + j)).click();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Moneda_" + i + "_" + j + "']/div[2]/ul/li[@id='" + valueTipoMoneda + "']")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@formcontrolname='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@formcontrolname='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento /Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@formcontrolname='Establecimiento'])[last()]")).sendKeys("TAMBO COMPRA");
                        //TODO: Ingresa - Servicio/ Empresa
                        Serenity.getDriver().findElement(By.xpath("(//input[@formcontrolname='ServicioEmpresa'])[last()]")).sendKeys("TAMBO");
                    }

                }

                if (tipoProducto.equals("APP INTERBANK")) {

                    if (tipologia.equals("CONSTANCIA DE PAGO REALIZADO")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("RAPPI");
                        //TODO: Ingresa - Cod. Cliente/Suministro/Nro. Contrato
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ClienteSuministroContrato'])[last()]")).sendKeys("105255");

                    }

                    if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("RAPPI");
                    }

                }

                if (tipoProducto.equals("PAGO DE SERVICIO POR CANALES")) {

                    if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA") || tipologia.equals("COBROS INDEBIDOS")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.id("Moneda_" + i + "_" + j)).click();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Moneda_" + i + "_" + j + "']/div[2]/ul/li[@id='" + valueTipoMoneda + "']")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@formcontrolname='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@formcontrolname='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento /Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@formcontrolname='Establecimiento'])[last()]")).sendKeys("TAMBO COMPRA");
                        //TODO: Ingresa - Servicio/ Empresa
                        Serenity.getDriver().findElement(By.xpath("(//input[@formcontrolname='ServicioEmpresa'])[last()]")).sendKeys("TAMBO");
                    }

                    if (tipologia.equals("ATM DE IB NO DEPOSITÓ DINERO")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();

                        //TODO: Ingresa - Importe Solicitado
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ImporteSolicitado'])[last()]")).sendKeys("180");
                        //TODO: Ingresa - Importe Reclamado
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Hora de Operación
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Hora'])[last()]")).sendKeys("0530");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='EstablecimientoTransaccion'])[last()]")).sendKeys("TAMBO COMPRA");
                        //TODO: Ingresa - Cod.Cliente/Suministro/N° contrato
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ClienteSuministroContrato'])[last()]")).sendKeys("0150250");

                    }

                    if (tipologia.equals("ATENCIÓN IBK AGENTE")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("TAMBO COMPRA");
                        //TODO: Ingresa - Cod.Cliente/Suministro/N° contrato
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='supplyCode'])[last()]")).sendKeys("0150250");

                    }

                    if (tipologia.equals("CORRECCIÓN DE PAGO REALIZADO")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("TAMBO COMPRA");
                        //TODO: Ingresa - Servicio/Empresa
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Servicio'])[last()]")).sendKeys("IBK");
                        //TODO: Ingresa - Cod.Cliente/Suministro/N° contrato
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Codigo'])[last()]")).sendKeys("0150250");
                    }

                }

                if (tipoProducto.equals("COBRANZAS")) {

                    if (tipologia.equals("COBROS INDEBIDOS")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("TAMBO");
                    }
                }

                if (tipoProducto.equals("BILLETERA ELECTRÓNICA")) {

                    if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA") || tipologia.equals("INFORMACION DE MOVIMIENTOS") || tipologia.equals("CONSUMO/ RETIRO NO EFECTUADO")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha de operación
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("TAMBO");
                    }
                }

                if (tipoProducto.equals("TUNKI")) {

                    if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha de operación
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("TAMBO");
                    }
                    if (tipologia.equals("ATM DE IB NO ENTREGÓ DINERO")) {
                        String valueImporteSolicitado = Serenity.sessionVariableCalled("importeSolicitado").toString();
                        String valueImporteReclamado = Serenity.sessionVariableCalled("importeReclamado").toString();
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Importe Solicitado
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ImporteSolicitado'])[last()]")).sendKeys(valueImporteSolicitado);
                        //TODO: Ingresa - Importe Reclamado
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueImporteReclamado);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Hora de Operación
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Hora'])[last()]")).sendKeys("10:10");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='EstablecimientoTransaccion'])[last()]")).sendKeys("TAMBO");

                    }
                }

                if (tipoProducto.equals("TRANSF. INTERNACIONALES")) {

                    if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA") || tipologia.equals("COBROS INDEBIDOS") || tipologia.equals("INFORMACION DE MOVIMIENTOS")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha de operación
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("TAMBO");
                    }
                    if (tipologia.equals("CONSTANCIA POR TRANSFERENCIAS RECIBIDAS") || tipologia.equals("NO RECEPCIÓN DE TRANSFERENCIA IN/OUT")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha de operación
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='EstablecimientoTransaccion'])[last()]")).sendKeys("TAMBO");
                    }

                }

                if (tipoProducto.equals("PAY PAL")) {

                    if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha de operación
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("TAMBO");
                    }
                }

                if (tipoProducto.equals("BANCA CELULAR")) {
                    if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha de operación
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("TAMBO");
                    }

                }

                if (tipoProducto.equals("IB AGENTE")) {

                    if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha de operación
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("TAMBO");
                        //TODO: Ingresa - Código de Suministro
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='CodigoSuministroContrato'])[last()]")).sendKeys("COD123");
                    }

                    if (tipologia.equals("COBROS INDEBIDOS")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("TAMBO");
                    }

                }

                if (tipoProducto.equals("PLIN")) {

                    if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha de operación
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("TAMBO");
                    }

                    if (tipologia.equals("ATM DE IB NO ENTREGÓ DINERO")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Importe Solicitado
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ImporteSolicitado'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Importe Reclamado
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Hora de Operación
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Hora'])[last()]")).sendKeys("10:35");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("TAMBO COMPRA");
                    }

                }

                if (tipoProducto.equals("BANCA POR INTERNET")) {

                    if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha de operación
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("TAMBO");
                    }
                }

                if (tipoProducto.equals("PAGOS PLANILLAS/PROVEEDORES")) {

                    if (tipologia.equals("INFORMACION DE MOVIMIENTOS") || tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha de operación
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("TAMBO");
                    }

                    if (tipologia.equals("COBROS INDEBIDOS")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("TAMBO");
                        //TODO: Ingresa - Cod. cliente/Suministro/Nºcontrato
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ClienteSuministroContrato'])[last()]")).sendKeys("PHARMAX S.A.C");
                    }

                }

                if (tipoProducto.equals("RETIRO SIN TARJETA")) {

                    if (tipologia.equals("ATENCIONES BOGOTÁ")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Importe Solicitado
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ImporteSolicitado'])[last()]")).sendKeys("1200");
                        //TODO: Ingresa - Importe Reclamado
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys("1400");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Hora de Operación
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Hora'])[last()]")).sendKeys("1030");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='EstablecimientoTransaccion'])[last()]")).sendKeys("TAMBO");
                    }

                }

                if (tipoProducto.equals("SERVICIOS")) {

                    if (tipologia.equals("ATENCIÓN IBK AGENTE")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("TAMBO");
                        //TODO: Ingresa - Hora de Operación
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Hora'])[last()]")).sendKeys("10:35");
                        //TODO: Ingresa - Cod. Cliente/Suministro/Nro de contrato
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='CodigoSuministroContrato'])[last()]")).sendKeys("255366");
                    }

                }

                if (tipoProducto.equals("SUNAT")) {

                    if (tipologia.equals("CONSTANCIA DE PAGO REALIZADO")) {
                        String valueMedioPago = Serenity.sessionVariableCalled("medioPago").toString();

                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Selecciona - Medio de Pago
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='MedioPago'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueMedioPago + "'])[last()]")).click();
                        //TODO: Ingresa - Cod. cliente/Suministro/Nºcontrato
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ClienteSuministroContrato'])[last()]")).sendKeys("255366");
                    }

                    if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA")) {
                        String valueTipoOperacion = Serenity.sessionVariableCalled("tipoOperacion").toString();

                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Selecciona - Tipo de operación
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='TipoOperacion'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoOperacion + "'])[last()]")).click();
                        //TODO: Ingresa - Servicio/ Empresa
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ServicioEmpresa'])[last()]")).sendKeys("TAMBO");
                        //TODO: Ingresa - Cod. cliente/ Suministro/ Nºcontrato
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ClienteSuministroContrato'])[last()]")).sendKeys("COD255366");
                    }

                    if (tipologia.equals("CORRECCIÓN DE PAGO REALIZADO")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("TAMBO");
                        //TODO: Ingresa - Servicio/Empresa
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Servicio'])[last()]")).sendKeys("COMPRA/TAMBO");
                        //TODO: Ingresa - Código/Suministro/Nro de contrato
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ClienteSuministroContrato'])[last()]")).sendKeys("255366");
                    }

                    if (tipologia.equals("COBROS INDEBIDOS")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("TAMBO");
                    }

                }

                if (tipoProducto.equals("MONEY EXCHANGE APP")) {

                    if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("TAMBO");
                        //TODO: Ingresa - Hora de Operación
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Hora'])[last()]")).sendKeys("10:35");
                    }

                }

                //TODO: MOTIVO MOVIMIENTO - ATENCION AL CLIENTE
                if (tipoProducto.equals("ATENCION AL CLIENTE")) {

                    if (tipologia.equals("BILLETE FALSO")) {
                        //TODO: Selecciona combo - Tipo Moneda
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
                        //TODO: Ingresa - Fecha de operación
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("TAMBO");
                    }
                }


                //TODO: APLICA - SOLO TIPOLOGIA: CONSUMOS MAL PROCESADOS POR EL COMERCIO/MARCA

                String productoServicioAdquirido = "";
                String tipoServicio = "";
                try {
                    productoServicioAdquirido = Serenity.sessionVariableCalled("productoServicioAdquirido").toString();
                    tipoServicio = Serenity.sessionVariableCalled("tipoServicio").toString();

                    Thread.sleep(2000);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }

                //TODO: MOTIVO 22 - TARJETA DE CREDITO
                if (tipoProducto.equals("TARJETA DE CREDITO") && tipologia.equals("CONSUMOS MAL PROCESADOS POR EL COMERCIO/MARCA") && motivo.equals("22")) {

                    //TODO: Se intentó adquirir un:
                    if (productoServicioAdquirido.equals("Producto")) {
                        Serenity.getDriver().findElement(By.xpath("(//label[@for='ps_PS001_" + i + "_" + j + "'])[last()]")).click();
                    }

                    if (productoServicioAdquirido.equals("Servicio")) {
                        Serenity.getDriver().findElement(By.xpath("(//label[@for='ps_PS002_" + i + "_" + j + "'])[last()]")).click();

                        //TODO: El servicio corresponde a:
                        if (tipoServicio.equals("Hotel")) {
                            Serenity.getDriver().findElement(By.xpath("(//label[@for='tp_TSVC001_" + i + "_" + j + "'])[last()]")).click();
                        }

                        if (tipoServicio.equals("Tour")) {
                            Serenity.getDriver().findElement(By.xpath("(//label[@for='tp_TSVC002_" + i + "_" + j + "'])[last()]")).click();
                        }

                        if (tipoServicio.equals("Vuelos")) {
                            Serenity.getDriver().findElement(By.xpath("(//label[@for='tp_TSVC003_" + i + "_" + j + "'])[last()]")).click();
                        }

                        if (tipoServicio.equals("Otros")) {
                            Serenity.getDriver().findElement(By.xpath("(//label[@for='tp_TSVC004_" + i + "_" + j + "'])[last()]")).click();
                        }

                    }

                    //TODO: Ingresa - Esperaba recibirlo el:
                    Serenity.getDriver().findElement(By.xpath("(//input[@formcontrolname='FechaArriboProductoServicio'])[last()]")).sendKeys(getValueFecha());

                }

                //TODO: MOTIVO 35 - TARJETA DE DEBITO
                if (tipoProducto.equals("TARJETA DE DEBITO") && tipologia.equals("CONSUMOS MAL PROCESADOS POR EL COMERCIO/MARCA") && motivo.equals("35")) {

                    //TODO: Se intentó adquirir un:
                    if (productoServicioAdquirido.equals("Producto")) {
                        Serenity.getDriver().findElement(By.xpath("(//label[@for='ps_PS001_" + i + "_" + j + "'])[last()]")).click();
                    }

                    if (productoServicioAdquirido.equals("Servicio")) {
                        Serenity.getDriver().findElement(By.xpath("(//label[@for='ps_PS002_" + i + "_" + j + "'])[last()]")).click();

                        //TODO: El servicio corresponde a:
                        Serenity.getDriver().findElement(By.xpath("(//ibk-select)[last()]")).click();
                        validacionRegistroPage.SeleccionarListadoServicios(tipoServicio);
                    }

                    //TODO: Ingresa - Esperaba recibirlo el:
                    Serenity.getDriver().findElement(By.xpath("(//input[@formcontrolname='FechaArriboProductoServicio'])[last()]")).sendKeys(getValueFecha());
                }

                if (j != cantidadmontos - 1) {
                    Serenity.getDriver().findElement(By.id("Add_Amount_" + i)).click();
                }

            }

        }


    }

    private void llenarDatosMovimientoRezagados(int valornuevo, String valueTipoMoneda, String valueMonto) {
        Serenity.getDriver().findElement(By.xpath("(//*[@id='Moneda'])["+valornuevo+"]")).click();
        Serenity.getDriver().findElement(By.xpath("(//*[@id='Moneda'])["+valornuevo+"]/div[2]/ul/li[@id='" + valueTipoMoneda + "']")).click();
        //TODO: Ingresa - Monto
        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys(valueMonto);
        //TODO: Ingresa - Fecha
        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
        //TODO: Ingresa - Establecimiento/Transaccion
        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("CORPACK");
    }

    //[cOMERCIAL-Agregado para el llenado de campos particulares de las tipologias Fase 2 Rezagadas
    private void llenarDatosParticularesRezagadas(){
        String tipoOperacion = Serenity.sessionVariableCalled("tipoOperacion").toString();

        //TODO: Seleccionar - Tipo Operación
        Serenity.getDriver().findElement(By.id("operationType")).click();
        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + tipoOperacion + "'])[last()]")).click();
        String valueNumCuenta ="";
        String valueNumTarjeta ="";
        String valueNumCredito ="";
        String valueNumPoliza="";
        String valueNomSeguro ="";

        try {
             valueNumCuenta = Serenity.sessionVariableCalled("numCuenta").toString();
        } catch (Exception e) {
            valueNumCuenta="";
            System.out.println(e.getMessage());
        }

        try {
            valueNumTarjeta = Serenity.sessionVariableCalled("numTarjeta").toString();
        } catch (Exception e) {
            valueNumTarjeta="";
            System.out.println(e.getMessage());
        }


        try {
            valueNumCredito = Serenity.sessionVariableCalled("numCredito").toString();
        } catch (Exception e) {
            valueNumCredito="";
            System.out.println(e.getMessage());
        }

        try {
            valueNumPoliza = Serenity.sessionVariableCalled("numPoliza").toString();
        } catch (Exception e) {
            valueNumPoliza="";
            System.out.println(e.getMessage());
        }

        try {
            valueNomSeguro = Serenity.sessionVariableCalled("nomSeguro").toString();
        } catch (Exception e) {
            valueNomSeguro="";
            System.out.println(e.getMessage());
        }

     if(tipoOperacion.equals("Cuentas TC - TD")){
            if(!valueNumCuenta.equals("")){
                Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).sendKeys(valueNumCuenta);
            }
            if(!valueNumTarjeta.equals("")){
                Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[2]")).sendKeys(valueNumTarjeta);
            }
        }else if(tipoOperacion.equals("Seguros")){

            if(!valueNumCredito.equals("")){
                Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).sendKeys(valueNumCredito);
            }
            if(!valueNumCuenta.equals("")){
                Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[2]")).sendKeys(valueNumCuenta);
            }
            if(!valueNumTarjeta.equals("")){
                Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[3]")).sendKeys(valueNumTarjeta);
            }
            if(!valueNumPoliza.equals("")){
                Serenity.getDriver().findElement(By.xpath("(//input[@id='policyNumber'])")).sendKeys(valueNumPoliza);
            }
            if(!valueNomSeguro.equals("")){
                Serenity.getDriver().findElement(By.xpath("(//input[@id='insuranceName'])")).sendKeys(valueNomSeguro);
            }
        }

    }


    public static Performable withData(String motivo, String tipomoneda, String montoMovimiento, String cantidadmontomotivo) {
        return instrumented(RegistraMotivoMovimiento.class, motivo, tipomoneda, montoMovimiento, cantidadmontomotivo);
    }


}
