package com.everis.giru.tasks.atencionCliente;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class RegistraRazonOperativa implements Task {

    private final String razonOperativa;
    private final String tipoRazonOperativa;

    public RegistraRazonOperativa(String razonOperativa, String tipoRazonOperativa) {
        this.razonOperativa = razonOperativa;
        this.tipoRazonOperativa = tipoRazonOperativa;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        if (razonOperativa.equals("Si")) {

            actor.attemptsTo(
                    Click.on(Target.the("Elige SI en la razon operativa RADIO BUTTON").located(By.xpath("//*[@for='flag-operative-1']")))
            );

            actor.attemptsTo(
                    Click.on(Target.the("Selecciona la razon operativa COMBOBOX").located(By.id("operativeReason")))
            );

            actor.attemptsTo(
                    Click.on(Target.the("Selecciona la razon operativa").located(By.id(tipoRazonOperativa)))
            );

        }

    }

    public static Performable withData(String razonOperativa, String tipoRazonOperativa) {
        return instrumented(RegistraRazonOperativa.class, razonOperativa, tipoRazonOperativa);
    }

}
