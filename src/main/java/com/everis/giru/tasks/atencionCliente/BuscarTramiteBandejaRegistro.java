package com.everis.giru.tasks.atencionCliente;

import com.everis.giru.userinterface.tramites.ValidarTramiteBandejaPage;
import lombok.SneakyThrows;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class BuscarTramiteBandejaRegistro implements Task {

    private final String numerotramite;
    ValidarTramiteBandejaPage validarTramiteBandejaPage = new ValidarTramiteBandejaPage();
    
    public BuscarTramiteBandejaRegistro(String numerotramite) {
        this.numerotramite = numerotramite;
    }

    @SneakyThrows
    @Override
    public <T extends Actor> void performAs(T actor) {
        validarTramiteBandejaPage.buscarTramiteBandejaRegistro(numerotramite);
    }

    public static Performable withData(String numerotramite) {
        return instrumented(BuscarTramiteBandejaRegistro.class, numerotramite);
    }
}