package com.everis.giru.tasks.atencionCliente;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import org.openqa.selenium.By;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class SeleccionarSucedidoFlagBloqueoTarjeta implements Task {

    private final String getMotivoSucedidoTarjeta;
    private final String flagTarjetaBloqueo;

    public SeleccionarSucedidoFlagBloqueoTarjeta(String getMotivoSucedidoTarjeta, String flagTarjetaBloqueo) {
        this.getMotivoSucedidoTarjeta = getMotivoSucedidoTarjeta;
        this.flagTarjetaBloqueo = flagTarjetaBloqueo;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        //TODO: Selecciona - ¿Qué sucedió con la tarjeta de crédito?
        Serenity.getDriver().findElement(By.id("cardFraudSituation")).click();
        Serenity.getDriver().findElement(By.id(getMotivoSucedidoTarjeta)).click();

        //TODO: Seleccionar - He bloqueado la tarjeta de crédito afectada por el consumo no reconocido por fraude.
        if (flagTarjetaBloqueo.equals("Si")) {
            System.out.println("SI SELECCIONO FLAG DE BLOQUEO: " + flagTarjetaBloqueo);
            Serenity.getDriver().findElement(By.xpath("//label[@for='flagLockCard-0']")).click();
        }

        if (flagTarjetaBloqueo.equals("No")) {
            System.out.println("NO SELECCIONO FLAG DE BLOQUEO: " + flagTarjetaBloqueo);

            //TODO: ¿Por qué no se ha bloqueado la tarjeta?
            Serenity.getDriver().findElement(By.xpath("//textarea[@formcontrolname='notLockCardComment']")).sendKeys("Prueba no se ha bloqueado la tarjeta");
        }

    }


    public static Performable withData(String getMotivoSucedidoTarjeta, String flagTarjetaBloqueo) {
        return instrumented(SeleccionarSucedidoFlagBloqueoTarjeta.class, getMotivoSucedidoTarjeta, flagTarjetaBloqueo);
    }

}
