package com.everis.giru.tasks.atencionCliente;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.Locale;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

/**
 * @author Nilo Carrión
 */
public class SeleccionarTipologiaById implements Task {

    private final String tipologia;

    public SeleccionarTipologiaById(String tipologia) {
        this.tipologia = tipologia;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        //TODO: Selecciona la tipología - id
        if (tipologia.equals("-")) {
            actor.attemptsTo(
                    WaitUntil.the(By.id("typology"), isVisible()).forNoMoreThan(20).seconds(),
                    Click.on(By.id("typology"))
            );

            String nombreTipologia = Serenity.sessionVariableCalled("tipologia");
            String[] frases = nombreTipologia.toLowerCase(Locale.ROOT).replace("/", " ").replace("ts", "TS").split(" ");
            String superString = "";
            for (int i = 0; i < frases.length; i++) {
                if (frases[i].trim().length() > 1) {
                    superString = superString + frases[i].substring(1) + " ";
                } else {
                    superString = superString + frases[i] + " ";
                }
                System.out.println(superString + "\n");
            }
            String[] updatedfrases = superString.split(" ");
            if (frases.length == 1) {
                WebElement elemento = Serenity.getDriver().findElement(By.xpath("//*[contains(text(), '" + updatedfrases[0] + "')]"));
                elemento.click();
            }
            if (frases.length == 2) {
                WebElement elemento = Serenity.getDriver().findElement(By.xpath("//*[contains(text(), '" + updatedfrases[0] + "') and contains(text(), '" + updatedfrases[1] + "')]"));
                elemento.click();
            }
            if (frases.length == 3) {
                WebElement elemento = Serenity.getDriver().findElement(By.xpath("//*[contains(text(), '" + updatedfrases[0] + "') and contains(text(), '" + updatedfrases[1] + "') and contains(text(), '" + updatedfrases[2] + "')]"));
                elemento.click();
            }
            if (frases.length >= 4) {
                WebElement elemento = Serenity.getDriver().findElement(By.xpath("//*[contains(text(), '" + updatedfrases[0] + "') and contains(text(), '" + updatedfrases[1] + "') and contains(text(), '" + updatedfrases[2] + "') and contains(text(), '" + updatedfrases[3] + "')]"));
                elemento.click();
            }
            // WebElement lblTypologyId = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='typology']/div/ul/li[1]")));
            // lblTypologyId.click();
        } else {
            actor.attemptsTo(
                    WaitUntil.the(By.id("typology"), isVisible()).forNoMoreThan(20).seconds(),
                    Click.on(By.id("typology"))
            );

            WebElement lblTypologyId = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.id(tipologia)));
            lblTypologyId.click();
        }
    }

    public static Performable withData(String tipologia) {
        return instrumented(SeleccionarTipologiaById.class, tipologia);
    }

}
