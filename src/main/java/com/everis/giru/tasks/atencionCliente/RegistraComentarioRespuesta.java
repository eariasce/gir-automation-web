package com.everis.giru.tasks.atencionCliente;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import org.openqa.selenium.By;

import java.time.Duration;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class RegistraComentarioRespuesta implements Task {

    private final String respuestaCliente;
    private final String comentarioAdicional;

    public RegistraComentarioRespuesta(String respuestaCliente, String comentarioAdicional) {
        this.respuestaCliente = respuestaCliente;
        this.comentarioAdicional = comentarioAdicional;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        if (comentarioAdicional.equals("Si")) {

            Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(1));
            boolean txtAreaComentarioAdicional = Serenity.getDriver().findElements(By.xpath("//textarea[@formcontrolname='internalComments']")).size() > 0;
            if (txtAreaComentarioAdicional) {
                System.err.println("internalComments");
                Serenity.getDriver().findElement(By.xpath("//textarea[@formcontrolname='internalComments']")).sendKeys("Comentario Interno de prueba opcional");

            } else {
                System.err.println("internal_comments");
                Serenity.getDriver().findElement(By.xpath("//textarea[@formcontrolname='internal_comments']")).sendKeys("Comentario Interno de prueba opcional");

            }

        }

        if (respuestaCliente.equals("Si")) {
            Serenity.getDriver().findElement(By.xpath("//textarea[@formcontrolname='comments']")).sendKeys("Respuesta para el cliente obligatoria");
        }else if (respuestaCliente.equals("No")){
            System.out.println("No se ingresa respuesta de cliente");
        }


    }

    public static Performable withData(String respuestaCliente, String comentariosAdicionales) {
        return instrumented(RegistraComentarioRespuesta.class, respuestaCliente, comentariosAdicionales);
    }

}
