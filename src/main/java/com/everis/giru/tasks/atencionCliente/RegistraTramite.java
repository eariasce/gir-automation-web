package com.everis.giru.tasks.atencionCliente;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import org.openqa.selenium.By;

public class RegistraTramite implements Task {

    @Override
    public <T extends Actor> void performAs(T actor) {
        Serenity.getDriver().findElement(By.id("claim-form__btn-submit")).click();
        Serenity.takeScreenshot();
    }

}
