package com.everis.giru.tasks.atencionCliente;

import com.everis.giru.userinterface.atencionCliente.GetListCreditCardsPage;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.model.environment.EnvironmentSpecificConfiguration;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.thucydides.model.util.EnvironmentVariables;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class SeleccionarProducto implements Task {

    GetListCreditCardsPage getListCreditCardsPage = new GetListCreditCardsPage();


    @Override
    public <T extends Actor> void performAs(T actor) {

        String esComercial = "";
        String tipoProducto = "";
        String ambiente = "";

        try {
            esComercial = Serenity.sessionVariableCalled("COMERCIAL").toString();
            ambiente = Serenity.sessionVariableCalled("ambiente").toString();
            tipoProducto = Serenity.sessionVariableCalled("tipoproducto").toString();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }


        if (esComercial.equals("COMERCIAL")) {

            //TODO: SELECCIONA OPCION - PRODUCTOS
            if (tipoProducto.equals("CRÉDITO BPE") ||  tipoProducto.equals("CUENTA NEGOCIOS") ||tipoProducto.equals("LETRAS Y CARTA FIANZA")||tipoProducto.equals("CUENTA CORRIENTE")||tipoProducto.equals("TARJETA DE DÉBITO")||tipoProducto.equals("TARJETA DE CRÉDITO")||tipoProducto.equals("SEGUROS")||tipoProducto.equals("SEGUROS RELACIONADOS")) {

                WebElement lblProducto = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(20)).until(ExpectedConditions.elementToBeClickable(By.xpath("//*[contains(text(), 'Productos')]")));
                lblProducto.click();

                if( tipoProducto.equals("CUENTA NEGOCIOS"))
                {
                    WebElement lblCbme = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--0']")));
                    lblCbme.click();
                }
                if( tipoProducto.equals("CRÉDITO BPE"))
                {
                    WebElement lblCbme = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--1']")));
                    lblCbme.click();
                }

                if( tipoProducto.equals("LETRAS Y CARTA FIANZA"))
                {
                    WebElement lblCbme = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--2']")));
                    lblCbme.click();
                }

                if(ambiente.equals("UAT")){
                    if( tipoProducto.equals("CUENTA CORRIENTE"))
                    {
                        WebElement lblCbme = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--4']")));
                        lblCbme.click();
                    }
                    if( tipoProducto.equals("TARJETA DE DÉBITO"))
                    {
                        WebElement lblCbme = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--5']")));
                        lblCbme.click();
                    }
                    if( tipoProducto.equals("SEGUROS"))
                    {
                        WebElement lblCbme = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--7']")));
                        lblCbme.click();
                    }
                    if( tipoProducto.equals("SEGUROS RELACIONADOS"))
                    {
                        WebElement lblCbme = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--8']")));
                        lblCbme.click();
                    }

                }else{
                    if( tipoProducto.equals("CUENTA CORRIENTE"))
                    {
                        WebElement lblCbme = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--7']")));
                        lblCbme.click();
                    }
                    if( tipoProducto.equals("TARJETA DE DÉBITO"))
                    {
                        WebElement lblCbme = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--3']")));
                        lblCbme.click();
                    }
                    if( tipoProducto.equals("SEGUROS"))
                    {
                        WebElement lblCbme = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--5']")));
                        lblCbme.click();
                    }
                    if( tipoProducto.equals("SEGUROS RELACIONADOS"))
                    {
                        WebElement lblCbme = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--6']")));
                        lblCbme.click();
                    }
                }



                if( tipoProducto.equals("TARJETA DE CRÉDITO"))
                {
                    WebElement lblCbme = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--9']")));
                    lblCbme.click();
                }



            }


            //TODO: SELECCIONA OPCION - ATENCIÓN AL CLIENTE
            if (tipoProducto.equals("ATENCIÓN AL CLIENTE")) {
                WebElement lblOperaciones = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(20)).until(ExpectedConditions.elementToBeClickable(By.xpath("//span[normalize-space()='Atención']")));
                lblOperaciones.click();
            }
        } else {

            //TODO: SELECCIONA OPCION - PRODUCTOS
            if (tipoProducto.equals("TARJETA DE CREDITO") || tipoProducto.equals("TARJETA DE DEBITO") ||
                    tipoProducto.equals("CUENTA") || tipoProducto.equals("SEGUROS") ||
                    tipoProducto.equals("ADELANTO SUELDO") || tipoProducto.equals("LINEA CONVENIO") ||
                    tipoProducto.equals("HIPOTECARIO") || tipoProducto.equals("PRÉSTAMO PERSONAL") ||
                    tipoProducto.equals("REFINANCIADO") || tipoProducto.equals("EFECTIVO CON GARANTÍA HIPOTECARIO") ||
                    tipoProducto.equals("VEHICULAR") || tipoProducto.equals("CBME") ||
                    tipoProducto.equals("CUENTA CORRIENTE") || tipoProducto.equals("SEGUROS RELACIONADOS") ||
                    tipoProducto.equals("CARTA FIANZA/CERTIFICADO") || tipoProducto.equals("RAPPIBANK - CUENTAS") ||
                    tipoProducto.equals("RAPPIBANK - TARJETA DE CRÉDITO")) {

                WebElement lblProducto = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(20)).until(ExpectedConditions.elementToBeClickable(By.xpath("//*[contains(text(), 'Productos')]")));
                lblProducto.click();

                if (tipoProducto.equals("TARJETA DE CREDITO")) {
                    WebElement lblTarjetaCredito = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--0']")));
                    lblTarjetaCredito.click();

                    getListCreditCardsPage.selectListCardsCredit();

                    //TODO: Para ambiente UAT - STG y producto Tarjeta de crédito, muestra modal campaña
                    //TODO: Selecciona - Modal Campaña

                    try {
                        Thread.sleep(5000);
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }

//                    if (ambiente.equals("UAT")) {
//                        Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(25));
//                        WebElement lblModalCampana = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.id("generic-error__btn-understood")));
//                        lblModalCampana.click();
//                    }

                }

                if (tipoProducto.equals("TARJETA DE DEBITO")) {
                    WebElement lblTarjetaDebito = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--1']")));
                    lblTarjetaDebito.click();

                    getListCreditCardsPage.selectListCardsDebit();
                }

                if (tipoProducto.equals("CUENTA") || tipoProducto.equals("CUENTA CORRIENTE")) {
                    WebElement lblCuenta = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--2']")));
                    lblCuenta.click();

                    getListCreditCardsPage.selectListCuenta();
                }

                if (tipoProducto.equals("SEGUROS")) {
                    WebElement lblSeguros = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--3']")));
                    lblSeguros.click();

                    getListCreditCardsPage.selectListSeguro();
                }

                if (tipoProducto.equals("ADELANTO SUELDO")) {
                    WebElement lblAdelantoSueldo = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--4']")));
                    lblAdelantoSueldo.click();

                    getListCreditCardsPage.selectListCardAdelantoSueldo();
                }

                if (tipoProducto.equals("LINEA CONVENIO")) {
                    WebElement lblLineaConvenio = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--4']")));
                    lblLineaConvenio.click();

                    getListCreditCardsPage.selectListCardsLineaConvenio();
                }

                if (tipoProducto.equals("HIPOTECARIO") || tipoProducto.equals("PRÉSTAMO PERSONAL") || tipoProducto.equals("REFINANCIADO") || tipoProducto.equals("EFECTIVO CON GARANTÍA HIPOTECARIO") || tipoProducto.equals("VEHICULAR")) {

                    WebElement lblCreditosPersonales = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--5']")));
                    lblCreditosPersonales.click();

                    //TODO: Selecciona un crédito
                    getListCreditCardsPage.selectListCreditosPersonales();
                }

                if (tipoProducto.equals("CBME")) {
                    WebElement lblCbme = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--7']")));
                    lblCbme.click();
                }

                if (tipoProducto.equals("CARTA FIANZA/CERTIFICADO")) {
                    WebElement lblCartaFianzaCertificado = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--8']")));
                    lblCartaFianzaCertificado.click();
                }

                if (tipoProducto.equals("SEGUROS RELACIONADOS")) {
                    WebElement lblSegurosRelacionados = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--9']")));
                    lblSegurosRelacionados.click();
                }

                if (tipoProducto.equals("RAPPIBANK - TARJETA DE CRÉDITO")) {
                    WebElement lblRappiBankTarjetaCredito = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--10']")));
                    lblRappiBankTarjetaCredito.click();
                }

                if (tipoProducto.equals("RAPPIBANK - CUENTAS")) {
                    WebElement lblRappiBank = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--11']")));
                    lblRappiBank.click();
                }

            }

            //TODO: SELECCIONA OPCION - OPERACIONES
            if (tipoProducto.equals("DEBITO AUTOMATICO") || tipoProducto.equals("PAGO DE SERVICIO POR CANALES") || tipoProducto.equals("TRANSF. NACIONALES") ||
                    tipoProducto.equals("COBRANZAS") || tipoProducto.equals("APP INTERBANK") || tipoProducto.equals("REMESAS") || tipoProducto.equals("PODER") || tipoProducto.equals("BANCA CELULAR") ||
                    tipoProducto.equals("BILLETERA ELECTRÓNICA") || tipoProducto.equals("TOKEN") || tipoProducto.equals("TUNKI") || tipoProducto.equals("TRANSF. INTERNACIONALES") ||
                    tipoProducto.equals("PAY PAL") || tipoProducto.equals("IB AGENTE") || tipoProducto.equals("PLIN") || tipoProducto.equals("BANCA POR INTERNET") ||
                    tipoProducto.equals("CUENTA JUBILACIÓN") || tipoProducto.equals("SANEAMIENTO PROY. INMB") || tipoProducto.equals("ATENCIÓN IBK AGENTE") ||
                    tipoProducto.equals("PAGOS PLANILLAS/PROVEEDORES") || tipoProducto.equals("PODERES") || tipoProducto.equals("RETIRO SIN TARJETA") ||
                    tipoProducto.equals("MONEY EXCHANGE APP") || tipoProducto.equals("SERVICIOS") || tipoProducto.equals("SUNAT") ||
                    tipoProducto.equals("TRASLADOS CTS")) {

                WebElement lblOperaciones = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//span[normalize-space()='Operaciones']")));
                lblOperaciones.click();

                if (tipoProducto.equals("COBRANZAS")) {
                    WebElement lblCobranzas = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--0']")));
                    lblCobranzas.click();
                }

                if (tipoProducto.equals("REMESAS")) {
                    WebElement lblRemesas = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--1']")));
                    lblRemesas.click();
                }

                if (tipoProducto.equals("PODER")) {
                    WebElement lblPoder = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--2']")));
                    lblPoder.click();
                }

                if (tipoProducto.equals("APP INTERBANK")) {
                    WebElement lblAppInterbank = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--3']")));
                    lblAppInterbank.click();
                }

                if (tipoProducto.equals("BANCA CELULAR")) {
                    WebElement lblBancaCelular = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--4']")));
                    lblBancaCelular.click();
                }

                if (tipoProducto.equals("DEBITO AUTOMATICO")) {
                    WebElement lblDebitoAutomatico = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--5']")));
                    lblDebitoAutomatico.click();
                }

                if (tipoProducto.equals("PAGO DE SERVICIO POR CANALES")) {
                    WebElement lblPagoServicioCanales = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--6']")));
                    lblPagoServicioCanales.click();
                }

                if (tipoProducto.equals("TRANSF. NACIONALES")) {
                    WebElement lblTransfNacionales = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--7']")));
                    lblTransfNacionales.click();
                }
                if (tipoProducto.equals("BANCA POR INTERNET")) {
                    WebElement lblBancaPorInternet = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--8']")));
                    lblBancaPorInternet.click();
                }
                if (tipoProducto.equals("BILLETERA ELECTRÓNICA")) {
                    WebElement lblBilleteraElectronica = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--9']")));
                    lblBilleteraElectronica.click();
                }
                if (tipoProducto.equals("IB AGENTE")) {
                    WebElement lblIbAgente = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--10']")));
                    lblIbAgente.click();
                }
                if (tipoProducto.equals("PAY PAL")) {
                    WebElement lblPaypal = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--11']")));
                    lblPaypal.click();
                }
                if (tipoProducto.equals("PLIN")) {
                    WebElement lblPlin = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--12']")));
                    lblPlin.click();
                }
                if (tipoProducto.equals("TOKEN")) {
                    WebElement lblToken = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--14']")));
                    lblToken.click();
                }
                if (tipoProducto.equals("TRANSF. INTERNACIONALES")) {
                    WebElement lblTransfInternacionales = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--15']")));
                    lblTransfInternacionales.click();
                }

                if (tipoProducto.equals("TUNKI")) {
                    WebElement lblTunki = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--16']")));
                    lblTunki.click();
                }
                if (tipoProducto.equals("PAGOS PLANILLAS/PROVEEDORES")) {
                    WebElement lblPagosPlanillasProveedores = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--17']")));
                    lblPagosPlanillasProveedores.click();
                }
                if (tipoProducto.equals("CUENTA JUBILACIÓN")) {
                    WebElement lblCuentaJubilacion = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--18']")));
                    lblCuentaJubilacion.click();
                }
                if (tipoProducto.equals("RETIRO SIN TARJETA")) {
                    WebElement lblRetiroSinTarjeta = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--19']")));
                    lblRetiroSinTarjeta.click();
                }
                if (tipoProducto.equals("PODERES")) {
                    WebElement lblPoderes = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--20']")));
                    lblPoderes.click();
                }
                if (tipoProducto.equals("SANEAMIENTO PROY. INMB")) {
                    WebElement lblSaneamientoProyInmb = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--21']")));
                    lblSaneamientoProyInmb.click();
                }

                if (tipoProducto.equals("TRASLADOS CTS")) {
                    WebElement lbltrasladosCts = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--22']")));
                    lbltrasladosCts.click();
                }

                if (tipoProducto.equals("SERVICIOS")) {
                    WebElement lblServicios = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--23']")));
                    lblServicios.click();
                }

                if (tipoProducto.equals("SUNAT")) {
                    WebElement lblSunat = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--24']")));
                    lblSunat.click();
                }

                if (tipoProducto.equals("MONEY EXCHANGE APP")) {
                    WebElement lblMoneyExchangeApp = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='item-product item-product--25']")));
                    lblMoneyExchangeApp.click();
                }

            }

            //TODO: SELECCIONA OPCION - ATENCIÓN AL CLIENTE
            if (tipoProducto.equals("ATENCION AL CLIENTE")) {
                WebElement lblOperaciones = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(20)).until(ExpectedConditions.elementToBeClickable(By.xpath("//span[normalize-space()='Atención']")));
                lblOperaciones.click();
            }

        }


    }

}
