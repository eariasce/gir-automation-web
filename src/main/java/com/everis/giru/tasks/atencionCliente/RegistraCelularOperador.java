package com.everis.giru.tasks.atencionCliente;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class RegistraCelularOperador implements Task {

    private final String numeroCelular;
    private final String operador;

    public RegistraCelularOperador(String numeroCelular, String operador) {
        this.numeroCelular = numeroCelular;
        this.operador = operador;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        if (numeroCelular.equals("REGISTRADO1")) {
            //TODO: Selecciona el número de celular
            Serenity.getDriver().findElement(By.xpath("(//div[@id='telephone'])[1]")).click();
            Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='telephone']//li)[1]")).click();
        } else {
            Serenity.getDriver().findElement(By.xpath("(//div[@id='telephone'])[1]")).click();
            //TODO: Seleccionar - Agrega un número nuevo.
            Serenity.getDriver().findElement(By.xpath("//*[@id='telephone']/div[2]/ul/li[@id='CREATE']")).click();
            //TODO: Ingresa el nuevo número proporcionado
            String comercial = Serenity.sessionVariableCalled("comercial");
            if(comercial.equals("si")) {
                WebElement txtNumeroCelular = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(10))
                        .until(ExpectedConditions.elementToBeClickable(By.id("phone-number-form__phone-number")));
                txtNumeroCelular.sendKeys("987293664");
            }
            else {
                WebElement txtNumeroCelular = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(10))
                        .until(ExpectedConditions.elementToBeClickable(By.id("phone-number-form__phone-number")));
                txtNumeroCelular.sendKeys("959149978");

            }
            try {
                Thread.sleep(1000);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

            //TODO: Añadir
            WebElement btnAnadir = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(3))
                    .until(ExpectedConditions.elementToBeClickable(By.id("phone-number__btn-add")));
            btnAnadir.click();
        }

        //TODO: Selecciona un operador
        if (operador.equals("MOVISTAR")) {
            Serenity.getDriver().findElement(By.id("tab-MOVISTAR")).click();
        }

        if (operador.equals("CLARO")) {
            Serenity.getDriver().findElement(By.id("tab-CLARO")).click();
        }

        if (operador.equals("ENTEL")) {
            Serenity.getDriver().findElement(By.id("tab-ENTEL")).click();
        }

        if (operador.equals("BITEL")) {
            Serenity.getDriver().findElement(By.id("tab-BITEL")).click();
        }

    }

    public static Performable withData(String numeroCelular, String operador) {
        return instrumented(RegistraCelularOperador.class, numeroCelular, operador);
    }

}
