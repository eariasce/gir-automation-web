package com.everis.giru.tasks.atencionCliente;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class RegistraMedioRespuesta implements Task {

    private final String mediorespuesta;
    private final String nuevoregistro;

    public RegistraMedioRespuesta(String mediorespuesta, String nuevoregistro) {
        this.mediorespuesta = mediorespuesta;
        this.nuevoregistro = nuevoregistro;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        //TODO: Selecciona un medio de respuesta
        if (mediorespuesta.equals("Correo")) {
            //TODO: Selecciona un medio de respuesta - EMAIL
            Serenity.getDriver().findElement(By.xpath("//label[@for='responseType-EMAIL']")).click();
            //TODO: Selecciona un correo electrónico
            Serenity.getDriver().findElement(By.xpath("(//div[@id='email'])[1]")).click();

            if (nuevoregistro.equals("RegistradoONE")) {
                //TODO: Primer elemento de combobox correos
                Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='email']//li)[1]")).click();
            } else {
                //TODO: Seleccionar - Agregar nuevo correo electrónico.

                String comercial = Serenity.sessionVariableCalled("comercial");
            if(comercial.equals("si")) {

                String validarCorreo = Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='email']//li)[1]")).getText();
                if(validarCorreo.contains("rut.testjira@gmail.com"))
                {
                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='email']//li)[1]")).click();
                }
                else {

                    Serenity.getDriver().findElement(By.xpath("//*[@id='email']/div[2]/ul/li[@id='CREATE']")).click();

                    //TODO: Ingresa el nuevo correo electrónico proporcionado
                    WebElement txtnewEmail = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(10))
                            .until(ExpectedConditions.elementToBeClickable(By.id("newEmail")));
                    txtnewEmail.sendKeys("rut.testjira@gmail.com");

                    Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(4));
                    Serenity.getDriver().findElement(By.id("email__btn")).click();
                }
            }
            else
            {
                //TODO: Ingresa el nuevo correo electrónico proporcionado
                Serenity.getDriver().findElement(By.xpath("//*[@id='email']/div[2]/ul/li[@id='CREATE']")).click();
                WebElement txtnewEmail = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(10))
                        .until(ExpectedConditions.elementToBeClickable(By.id("newEmail")));
                txtnewEmail.sendKeys("alexisce004@gmail.com");

                Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(4));

                if(Serenity.getDriver().findElements(By.id("email__btn")).size() >0)
                {
                    Serenity.getDriver().findElement(By.id("email__btn")).click();

                }else {
                    Serenity.getDriver().findElement(By.id("add-email__btn-add")).click();
                }

            }
                try {
                    Thread.sleep(1000);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }

                //TODO: Añadir
                //WebElement btnAnadir = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(4))
                        //.until(ExpectedConditions.elementToBeClickable(By.id("add-email__btn-add")));

                //btnAnadir.click();

            }
            
        }

        if (mediorespuesta.equals("Direccion")) {
            //TODO: Selecciona un medio de respuesta - Dirección
            Serenity.getDriver().findElement(By.xpath("//label[@for='responseType-ADDRESS']")).click();
            //TODO: Selecciona un correo electrónico
            Serenity.getDriver().findElement(By.id("address")).click();

            if (nuevoregistro.equals("Registrado")) {
                //TODO: Primer elemento de combobox direccion
                Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='address']//li)[1]")).click();
            }

            if (nuevoregistro.equals("Nueva")) {
                //TODO: Selecciona - Agregar una nueva dirección
                Serenity.getDriver().findElement(By.xpath("//*[@id='address']/div[2]/ul/li[@id='CREATE']")).click();

                //TODO: Departamento (*)
                Serenity.getDriver().findElement(By.id("department")).click();
                Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='department']//li)[1]")).click();
                //TODO: Provincia (*)
                Serenity.getDriver().findElement(By.id("province")).click();
                Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='province']//li)[1]")).click();
                //TODO: Distrito (*)
                Serenity.getDriver().findElement(By.id("district")).click();
                Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='district']//li)[1]")).click();
                //TODO: Tipo de via (*)
                Serenity.getDriver().findElement(By.id("streetType")).click();
                Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='streetType']//li)[14]")).click();
                //TODO: Nombre de vía (*)
                Serenity.getDriver().findElement(By.id("streetName")).sendKeys("Andromeda");
                //TODO: Urbanización
                Serenity.getDriver().findElement(By.id("neighborhood")).sendKeys("La Paz");
                //TODO: Número
                Serenity.getDriver().findElement(By.id("streetNumber")).sendKeys("850");
                //TODO: Manzana
                Serenity.getDriver().findElement(By.id("block")).sendKeys("J");
                //TODO: Lote
                Serenity.getDriver().findElement(By.id("lot")).sendKeys("7B");
                //TODO: Interior
                Serenity.getDriver().findElement(By.id("apartment")).sendKeys("20");
                //TODO: Referencia
                Serenity.getDriver().findElement(By.xpath("//textarea[@formcontrolname='landmark']")).sendKeys("GS4 Seguridad del Peru");
                //TODO: Button Añadir
                WebElement btnAddAddress = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(5))
                        .until(ExpectedConditions.elementToBeClickable(By.id("add-address__btn-add")));
                btnAddAddress.click();

            }

        }

    }

    public static Performable withData(String mediorespuesta, String nuevoregistro) {
        return instrumented(RegistraMedioRespuesta.class, mediorespuesta, nuevoregistro);
    }

}
