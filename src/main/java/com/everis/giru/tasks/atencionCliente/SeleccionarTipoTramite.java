package com.everis.giru.tasks.atencionCliente;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

import static net.serenitybdd.screenplay.Tasks.instrumented;

/**
 * @author Nilo Carrión
 */
public class SeleccionarTipoTramite implements Task {

    private final String tramite;

    public SeleccionarTipoTramite(String tramite) {
        this.tramite = tramite;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        //TODO: Elige el tipo de tramite
        Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(20));
        WebElement cmbRequestType = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(20)).until(ExpectedConditions.elementToBeClickable(By.id("requestType")));
        cmbRequestType.click();

        switch (tramite) {
            case "PEDIDO":
                Serenity.getDriver().findElement(By.id("TTR0002")).click();
                break;
            case "RECLAMO":
                Serenity.getDriver().findElement(By.id("TTR0001")).click();
                break;
            case "SOLICITUD":
                Serenity.getDriver().findElement(By.id("TTR0003")).click();
                break;
            default:
                System.out.println("No se encuentra tipo de tramite !!!");
        }

    }

    public static Performable withData(String tramite) {
        return instrumented(SeleccionarTipoTramite.class, tramite);
    }

}
