package com.everis.giru.tasks.atencionCliente;

import lombok.SneakyThrows;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Upload;
import org.openqa.selenium.By;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class SubirArchivo implements Task {

    private final String subirArchivo;

    public SubirArchivo(String subirArchivo) {
        this.subirArchivo = subirArchivo;

    }

    @SneakyThrows
    @Override
    public <T extends Actor> void performAs(T actor) {

        if (subirArchivo.equals("Si")) {
            actor.attemptsTo(
                    Upload.theClasspathResource("archivoReclamo/TestAuto.txt").to(Serenity.getDriver().findElement(By.xpath("//input[@id='files_uploader']")))
            );

        }

        try {
            Thread.sleep(2000);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public static Performable withData(String subirArchivo) {
        return instrumented(SubirArchivo.class, subirArchivo);
    }

}
