package com.everis.giru.tasks.atencionCliente;

import com.everis.giru.userinterface.atencionCliente.IdentificarClientePage;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.model.environment.EnvironmentSpecificConfiguration;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.thucydides.model.util.EnvironmentVariables;
import org.openqa.selenium.By;

import java.time.Duration;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class IdentificarCliente implements Task {

    private final String tipoDocumento;
    private final String numeroDocumento;
    IdentificarClientePage identificarClientePage;

    public IdentificarCliente(String tipoDocumento, String numeroDocumento) {
        this.tipoDocumento = tipoDocumento;
        this.numeroDocumento = numeroDocumento;
    }
String esComercial="";
EnvironmentVariables environmentVariables;
    @Override
    public <T extends Actor> void performAs(T actor) {

        identificarClientePage.seleccionarTipoDocumento(tipoDocumento);

        identificarClientePage.ingresarDocumento(numeroDocumento);

        //TODO: Botón validar
        Serenity.getDriver().findElement(By.id("search-form__btn-validate")).click();
        try {
            esComercial = Serenity.sessionVariableCalled("COMERCIAL").toString();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        //TODO: Validar si cliente esta activo para Comercial
        String flagCliente = EnvironmentSpecificConfiguration.from(environmentVariables).getProperty("giru.flagCliente"); ;
        try {
            Thread.sleep(20000);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        if(esComercial.equals("")||flagCliente.equals("1")){
            //TODO: ATENCION AL CLIENTE, RE INTENTAR, AGREGAR EN SCRIPT ( 1 INTENTOS)
            //TODO: No hubo coincidencias
            Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(1));
            boolean btnVolverIntentar = Serenity.getDriver().findElements(By.id("confirmation__btn-cancel")).size() > 0;
            if (btnVolverIntentar) {
                //TODO: Botón - volver a intentar
                try {
                    Thread.sleep(10000);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }

                Serenity.getDriver().findElement(By.id("confirmation__btn-cancel")).click();

                identificarClientePage.seleccionarTipoDocumento(tipoDocumento);
                identificarClientePage.ingresarDocumento(numeroDocumento);
                //TODO: Botón validar
                Serenity.getDriver().findElement(By.id("search-form__btn-validate")).click();
            } else {
                System.err.println("SI INGRESO CORECTAMENTE");
            }
        }


    }

    public static Performable withData(String tipoDocumento, String numeroDocumento) {
        return instrumented(IdentificarCliente.class, tipoDocumento, numeroDocumento);
    }

}
