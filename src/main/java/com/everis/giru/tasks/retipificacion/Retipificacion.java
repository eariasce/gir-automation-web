package com.everis.giru.tasks.retipificacion;

import com.everis.tdm.Do;
import com.everis.tdm.model.giru.ObtenerMotivoId;
import com.everis.tdm.model.giru.ObtenerMotivoIdComercial;
import net.serenitybdd.core.Serenity;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Retipificacion {
    String esComercial = "";
    public void validaRetificacion() {

        try {
            esComercial = Serenity.sessionVariableCalled("COMERCIAL").toString();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        // TODO: METODO RETIPIFICACION
        String camposRetipificacion = Serenity.sessionVariableCalled("camposRetipificacion").toString();

        //TODO: VARIABLE GLOBAL - PRODUCTO Y TIPOLOGIA
        String productoRetificar = camposRetipificacion.split("-")[0];
        String tipologiaRetificar = camposRetipificacion.split("-")[2];
        //TODO: CLICK EN EL ENLACE VERDE QUE DICE RETIPIFICA
        Serenity.getDriver().findElement(By.xpath("//a[normalize-space()='Retipificar trámite']")).click();

        if (esComercial.equals("COMERCIAL")) {
            // TODO: Selecciona el - Producto
            if (productoRetificar.equals("CUENTA_NEGOCIOS")||productoRetificar.equals("CUENTA_CORRIENTE")||productoRetificar.equals("CUENTA_AHORRO")
                    ||productoRetificar.equals("CBPE")||productoRetificar.equals("LYC")||productoRetificar.equals("TD")
                    ||productoRetificar.equals("TC")||productoRetificar.equals("T_CTS")||productoRetificar.equals("SEGUROS")
                    ||productoRetificar.equals("SEGUROS_RELACIONADOS")) {
                try {
                    Thread.sleep(2000);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
                // TODO: Selecciona el Producto
                Serenity.getDriver().findElement(By.id("mainProduct")).click();
                Serenity.getDriver().findElement(By.id("S4")).click();
                //Serenity.getDriver().findElement(By.xpath("//*[contains(text(), 'Productos')]")).click();
                // TODO: Selecciona el sub-producto Cuenta Negocios
                Serenity.getDriver().findElement(By.id("productType")).click();
                if(productoRetificar.equals("CUENTA_NEGOCIOS")){
                Serenity.getDriver().findElement(By.id("BUSINESS_ACCOUNT")).click();
                }
                // TODO: Selecciona el sub-producto Cuenta Corriente
                else if(productoRetificar.equals("CUENTA_CORRIENTE")){
                    Serenity.getDriver().findElement(By.id("ACCOUNT_CHECKING")).click();
                }
                // TODO: Selecciona el sub-producto Cuenta Ahorros
                else if(productoRetificar.equals("CUENTA_AHORRO")){
                    Serenity.getDriver().findElement(By.id("ACCOUNT_SAVINGS")).click();
                }
                // TODO: Selecciona el sub-producto Crédito BPE
                else if(productoRetificar.equals("CBPE")){
                    Serenity.getDriver().findElement(By.id("BPE_CREDIT")).click();
                }
                // TODO: Selecciona el sub-producto Letras y Carta Fianza
                else if(productoRetificar.equals("LYC")){
                    Serenity.getDriver().findElement(By.id("LETTER_BOND_LETTER")).click();
                }
                // TODO: Selecciona el sub-producto Tarjeta de Débito
                else if(productoRetificar.equals("TD")){
                    Serenity.getDriver().findElement(By.id("DEBIT_CARD")).click();
                }
                // TODO: Selecciona el sub-producto Tarjeta de crédito
                else if(productoRetificar.equals("TC")){
                    Serenity.getDriver().findElement(By.id("CREDIT_CARD")).click();
                }
                // TODO: Selecciona el sub-producto Traslados CTS
                else if(productoRetificar.equals("T_CTS")){
                    Serenity.getDriver().findElement(By.id("CTS_TRANSFER")).click();
                }
                // TODO: Selecciona el sub-producto Seguros
                else if(productoRetificar.equals("SEGUROS")){
                    Serenity.getDriver().findElement(By.id("INSURANCE")).click();
                }
                // TODO: Selecciona el sub-producto Seguros Relacionados
                else if(productoRetificar.equals("SEGUROS_RELACIONADOS")){
                    Serenity.getDriver().findElement(By.id("RELATED_INSURANCE")).click();
                }
            }
            // TODO: Selecciona el - Operaciones
            else  if (productoRetificar.equals("SERVICIOS")||productoRetificar.equals("T_NAC")||productoRetificar.equals("T_INT")
                    ||productoRetificar.equals("COBRANZAS")||productoRetificar.equals("SAN_PROY")||productoRetificar.equals("BPI")){
                    try {
                        Thread.sleep(2000);
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    Serenity.getDriver().findElement(By.id("mainProduct")).click();
                    Serenity.getDriver().findElement(By.id("S6")).click();
                    Serenity.getDriver().findElement(By.id("productType")).click();

                    // TODO: Selecciona el sub-producto Servicios
                    if(productoRetificar.equals("SERVICIOS")){
                        Serenity.getDriver().findElement(By.id("SERVICES")).click();
                    }
                    // TODO: Selecciona el sub-producto  Transf. Nacionales
                    else if(productoRetificar.equals("T_NAC")){
                        Serenity.getDriver().findElement(By.id("NATIONAL_TRANSFERS")).click();
                    }
                    // TODO: Selecciona el sub-producto  Transf. Internacionales
                    else if(productoRetificar.equals("T_INT")){
                        Serenity.getDriver().findElement(By.id("INTERNATIONAL_TRANSF")).click();
                    }
                    // TODO: Selecciona el sub-producto  Cobranzas
                    else if(productoRetificar.equals("COBRANZAS")){
                        Serenity.getDriver().findElement(By.id("COLLECTIONS")).click();
                    }
                    // TODO: Selecciona el sub-producto  Saneamiento Proy. Inmb
                    else if(productoRetificar.equals("SAN_PROY")){
                        Serenity.getDriver().findElement(By.id("SANITATION_PROJ")).click();
                    }
                    // TODO: Selecciona el sub-producto Banca por internet
                    else if(productoRetificar.equals("BPI")){
                        Serenity.getDriver().findElement(By.id("INTERNET_BANKING")).click();
                    }

                }

            // TODO: Selecciona el - Atencion
            else if (productoRetificar.equals("ATENCION_CLIENTE")){
                try {
                    Thread.sleep(2000);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
                Serenity.getDriver().findElement(By.id("mainProduct")).click();
                Serenity.getDriver().findElement(By.id("S5")).click();
                Serenity.getDriver().findElement(By.id("productType")).click();
                Serenity.getDriver().findElement(By.id("CUSTOMER_SUPPORT")).click();

            }

        }
        //****RETAIL******
        else{

            if (productoRetificar.equals("TC") || productoRetificar.equals("TD") || productoRetificar.equals("CUENTA") ||
                    productoRetificar.equals("SEGUROS") || productoRetificar.equals("SEGUROS_RELACIONADOS") || productoRetificar.equals("CBME") ||
                    productoRetificar.equals("CUENTA_CORRIENTE") || productoRetificar.equals("CARTA_FIANZA_CERTIFICADO") ||
                    productoRetificar.equals("RAPPIBANK_CUENTAS") || productoRetificar.equals("RAPPIBANK_TARJETA_CREDITO")) {

                try {
                    Thread.sleep(2000);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }

                // TODO: Selecciona el Producto
                Serenity.getDriver().findElement(By.id("mainProduct")).click();
                Serenity.getDriver().findElement(By.id("S1")).click();
                // TODO: Selecciona el sub-producto TC
                Serenity.getDriver().findElement(By.id("productType")).click();

                if (productoRetificar.equals("TC")) {
                    // TODO: Selecciona el sub-producto TC
                    Serenity.getDriver().findElement(By.id("CREDIT_CARD")).click();
                    // TODO: Selecciona la tarjeta de crédito
                    Serenity.getDriver().findElement(By.id("card")).click();
                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='card']//li)[1]")).click();
                }

                if (productoRetificar.equals("TD")) {
                    // TODO: Selecciona el sub-producto TD
                    Serenity.getDriver().findElement(By.id("DEBIT_CARD")).click();
                    // TODO: Selecciona la tarjeta de débito
                    Serenity.getDriver().findElement(By.id("card")).click();
                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='card']//li)[1]")).click();

                    try {
                        Thread.sleep(2000);
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    // TODO: Selecciona la cuenta
                    Serenity.getDriver().findElement(By.id("account")).click();
                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='account']//li)[1]")).click();
                }

                if (productoRetificar.equals("CUENTA") || productoRetificar.equals("CUENTA_CORRIENTE")) {
                    // TODO: Selecciona el sub-producto CUENTA
                    Serenity.getDriver().findElement(By.id("ACCOUNT")).click();

                    try {
                        Thread.sleep(1000);
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    // TODO: Selecciona la cuentas
                    Serenity.getDriver().findElement(By.id("card")).click();
                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='card']//li)[1]")).click();
                }

                if (productoRetificar.equals("SEGUROS")) {
                    // TODO: Selecciona el SubProducto - Seguros
                    Serenity.getDriver().findElement(By.id("INSURANCE")).click();
                    // TODO: Selecciona el N° Poliza
                    Serenity.getDriver().findElement(By.id("card")).click();
                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='card']//li)[1]")).click();
                }

                if (productoRetificar.equals("SEGUROS_RELACIONADOS")) {
                    // TODO: Selecciona el SubProducto - Seguros Relacionados
                    Serenity.getDriver().findElement(By.id("RELATED_INSURANCE")).click();
                }

                if (productoRetificar.equals("CBME")) {
                    // TODO: Selecciona el SubProducto - Seguros
                    Serenity.getDriver().findElement(By.id("CBME")).click();
                }

                if (productoRetificar.equals("CARTA_FIANZA_CERTIFICADO")) {
                    // TODO: Selecciona el SubProducto - Carta Fianza/Certificado
                    Serenity.getDriver().findElement(By.id("GUARANTEE_BANK_CERT")).click();
                }

                if (productoRetificar.equals("RAPPIBANK_TARJETA_CREDITO")) {
                    // TODO: Selecciona el SubProducto - RappiBank - Tarjeta de Crédito
                    Serenity.getDriver().findElement(By.id("RAPPIB_TC")).click();
                }

                if (productoRetificar.equals("RAPPIBANK_CUENTAS")) {
                    // TODO: Selecciona el SubProducto - RappiBank Cuentas
                    Serenity.getDriver().findElement(By.id("RAPPIB")).click();
                }


            }

            if (productoRetificar.equals("EFECTIVO_GARANTIA_HIPOTECARIO") || productoRetificar.equals("PRESTAMO_PERSONAL") ||
                    productoRetificar.equals("REFINANCIADO") || productoRetificar.equals("VEHICULAR") ||
                    productoRetificar.equals("HIPOTECARIO")) {
                try {
                    Thread.sleep(4000);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }

                // TODO: Selecciona el Producto
                Serenity.getDriver().findElement(By.id("mainProduct")).click();
                Serenity.getDriver().findElement(By.id("S1")).click();
                // TODO: Selecciona el SubProducto - Créditos Personales
                Serenity.getDriver().findElement(By.id("productType")).click();
                Serenity.getDriver().findElement(By.id("PERSONAL_LOAN")).click();

                if (productoRetificar.equals("HIPOTECARIO")) {
                    // TODO: Selecciona un tipo de Crédito Hipotecario
                    Serenity.getDriver().findElement(By.id("creditType")).click();
                    Serenity.getDriver().findElement(By.id("1")).click();
                }

                if (productoRetificar.equals("EFECTIVO_GARANTIA_HIPOTECARIO")) {
                    // TODO: Selecciona un tipo de Crédito Efectivo Con Garantía Hipotecario
                    Serenity.getDriver().findElement(By.id("creditType")).click();
                    Serenity.getDriver().findElement(By.id("2")).click();
                }
                if (productoRetificar.equals("PRESTAMO_PERSONAL")) {
                    // TODO: Selecciona un tipo de Crédito Prestamo Personal
                    Serenity.getDriver().findElement(By.id("creditType")).click();
                    Serenity.getDriver().findElement(By.id("3")).click();
                }
                if (productoRetificar.equals("REFINANCIADO")) {
                    // TODO: Selecciona un tipo de Crédito Refinanciado
                    Serenity.getDriver().findElement(By.id("creditType")).click();
                    Serenity.getDriver().findElement(By.id("5")).click();
                }
                if (productoRetificar.equals("VEHICULAR")) {
                    // TODO: Selecciona un tipo de Crédito Vehicular
                    Serenity.getDriver().findElement(By.id("creditType")).click();
                    Serenity.getDriver().findElement(By.id("4")).click();
                }
                // TODO: Selecciona el N° Crédito
                Serenity.getDriver().findElement(By.id("card")).click();
                Serenity.getDriver().findElement(By.xpath("//*[@id='card']/div[2]/ul/li[1]")).click();

            }

            if (productoRetificar.equals("LINEA_CONVENIO") || productoRetificar.equals("ADELANTO_SUELDO")) {
                try {
                    Thread.sleep(2000);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }

                // TODO: Selecciona el Producto
                Serenity.getDriver().findElement(By.id("mainProduct")).click();
                Serenity.getDriver().findElement(By.id("S1")).click();
                // TODO: Selecciona el SubProducto - Créditos por Convenios
                Serenity.getDriver().findElement(By.id("productType")).click();
                Serenity.getDriver().findElement(By.id("AGREEMENT")).click();

                if (productoRetificar.equals("LINEA_CONVENIO")) {
                    // TODO: Selecciona un tipo de Crédito Linea Convenio
                    Serenity.getDriver().findElement(By.id("creditType")).click();
                    Serenity.getDriver().findElement(By.id("1")).click();
                }

                if (productoRetificar.equals("ADELANTO_SUELDO")) {
                    // TODO: Selecciona un tipo de Crédito Adelanto Sueldo
                    Serenity.getDriver().findElement(By.id("creditType")).click();
                    Serenity.getDriver().findElement(By.id("2")).click();
                }

                // TODO: Selecciona el N° Crédito
                Serenity.getDriver().findElement(By.id("card")).click();
                Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='card']//li)[1]")).click();
            }

            // TODO: Selecciona el Producto - Operaciones
            if (productoRetificar.equals("COBRANZAS") || productoRetificar.equals("REMESAS") ||
                    productoRetificar.equals("PODER") || productoRetificar.equals("APPIBK") ||
                    productoRetificar.equals("BANCA_CELULAR") || productoRetificar.equals("AUTOMATIC_DEBIT") ||
                    productoRetificar.equals("PAGO_SERVICIO_CANALES") || productoRetificar.equals("TRANSF_NACIONALES") ||
                    productoRetificar.equals("BANCA_POR_INTERNET") || productoRetificar.equals("BILLETERA_ELECTRONICA") ||
                    productoRetificar.equals("IB_AGENTE") || productoRetificar.equals("PAYPAL") ||
                    productoRetificar.equals("PLIN") || productoRetificar.equals("DIGITAL_CARD") ||
                    productoRetificar.equals("TOKEN") || productoRetificar.equals("TRANSF_INTERNACIONALES") ||
                    productoRetificar.equals("TUNKI") || productoRetificar.equals("PAGOS_PLANILLAS_PROVEEDORES") ||
                    productoRetificar.equals("MONEY_EXCHANGE_APP") || productoRetificar.equals("SUNAT")
            ) {

                try {
                    Thread.sleep(4000);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }

                // TODO: Selecciona el Producto - Operaciones
                Serenity.getDriver().findElement(By.id("mainProduct")).click();
                Serenity.getDriver().findElement(By.id("S2")).click();
                // TODO: Selecciona el sub-producto
                Serenity.getDriver().findElement(By.id("productType")).click();

                if (productoRetificar.equals("COBRANZAS")) {
                    // TODO: Selecciona el sub-producto COBRANZAS
                    Serenity.getDriver().findElement(By.id("COLLECTIONS")).click();
                }

                if (productoRetificar.equals("REMESAS")) {
                    // TODO: Selecciona el sub-producto - REMESAS
                    Serenity.getDriver().findElement(By.id("REMITTANCE")).click();
                }

                if (productoRetificar.equals("PODER")) {
                    // TODO: Selecciona el sub-producto - PODER
                    Serenity.getDriver().findElement(By.id("POWER")).click();
                }

                if (productoRetificar.equals("APPIBK")) {
                    // TODO: Selecciona el sub-producto - APP INTERBANK
                    Serenity.getDriver().findElement(By.id("APP_IBK")).click();
                }

                if (productoRetificar.equals("BANCA_CELULAR")) {
                    // TODO: Selecciona el sub-producto - BANCA CELULAR
                    Serenity.getDriver().findElement(By.id("CELL_BANKING")).click();
                }

                if (productoRetificar.equals("AUTOMATIC_DEBIT")) {
                    // TODO: Selecciona el sub-producto - DEBITO AUTOMATICO
                    Serenity.getDriver().findElement(By.id("AUTOMATIC_DEBIT")).click();
                }

                if (productoRetificar.equals("PAGO_SERVICIO_CANALES")) {
                    // TODO: Selecciona el sub-producto - PAGO POR SERVICIOS POR CANALES
                    Serenity.getDriver().findElement(By.id("PAYMENT_CHANNELS")).click();
                }

                if (productoRetificar.equals("TRANSF_NACIONALES")) {
                    // TODO: Selecciona el sub-producto - TRANSFERENCIAS NACIONALES
                    Serenity.getDriver().findElement(By.id("NATIONAL_TRANSFERS")).click();
                }

                if (productoRetificar.equals("BANCA_POR_INTERNET")) {
                    // TODO: Selecciona el sub-producto - BANCA POR INTERNET
                    Serenity.getDriver().findElement(By.id("INTERNET_BANKING")).click();
                }

                if (productoRetificar.equals("BILLETERA_ELECTRONICA")) {
                    // TODO: Selecciona el sub-producto - BILLETERA ELECTRÓNICA
                    Serenity.getDriver().findElement(By.id("ELECTRONIC_WALLET")).click();
                }

                if (productoRetificar.equals("IB_AGENTE")) {
                    // TODO: Selecciona el sub-producto - IB AGENTE
                    Serenity.getDriver().findElement(By.id("IB_AGENT")).click();
                }

                if (productoRetificar.equals("PAYPAL")) {
                    // TODO: Selecciona el sub-producto - PAY PAL
                    Serenity.getDriver().findElement(By.id("PAYPAL")).click();
                }

                if (productoRetificar.equals("PLIN")) {
                    // TODO: Selecciona el sub-producto - PLIN
                    Serenity.getDriver().findElement(By.id("PLIN")).click();
                }

                if (productoRetificar.equals("DIGITAL_CARD")) {
                    // TODO: Selecciona el sub-producto - DIGITAL_CARD
                    Serenity.getDriver().findElement(By.id("DIGITAL_CARD")).click();
                }

                if (productoRetificar.equals("TOKEN")) {
                    // TODO: Selecciona el sub-producto - TOKEN
                    Serenity.getDriver().findElement(By.id("TOKEN")).click();
                }

                if (productoRetificar.equals("TRANSF_INTERNACIONALES")) {
                    // TODO: Selecciona el sub-producto - TRANSF. INTERNACIONALES
                    Serenity.getDriver().findElement(By.id("INTERNATIONAL_TRANSF")).click();
                }

                if (productoRetificar.equals("TUNKI")) {
                    // TODO: Selecciona el sub-producto - TUNKI
                    Serenity.getDriver().findElement(By.id("TUNKI")).click();
                }

                if (productoRetificar.equals("PAGOS_PLANILLAS_PROVEEDORES")) {
                    // TODO: Selecciona el sub-producto - PAGOS PLANILLAS/PROVEEDORES
                    Serenity.getDriver().findElement(By.id("PAYMENT_PAYROLL_SUPP")).click();
                }

                if (productoRetificar.equals("SUNAT")) {
                    // TODO: Selecciona el sub-producto - Sunat
                    Serenity.getDriver().findElement(By.id("SUNAT")).click();
                }

                if (productoRetificar.equals("MONEY_EXCHANGE_APP")) {
                    // TODO: Selecciona el sub-producto - MONEY_EXCHANGE_APP
                    Serenity.getDriver().findElement(By.id("MONEX_APP")).click();
                }

            }

            // TODO: Selecciona el Producto - Atención
            if (productoRetificar.equals("ATENCION_CLIENTE")) {
                try {
                    Thread.sleep(4000);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }

                // TODO: Selecciona el Producto - Atencion
                Serenity.getDriver().findElement(By.id("mainProduct")).click();
                Serenity.getDriver().findElement(By.id("S3")).click();
                // TODO: Selecciona el sub-producto
                Serenity.getDriver().findElement(By.id("productType")).click();
                Serenity.getDriver().findElement(By.id("CUSTOMER_SUPPORT")).click();
            }
    }

        //TODO: VARIABLE GLOBAL - TIPO DE TRAMITE
        String opciones_resuelve_o_retipifica = Serenity.sessionVariableCalled("configuracion_resolucion").toString();
        String retipificar_opciones = opciones_resuelve_o_retipifica.split(":")[1];
        String tipoTramiteRetificar = retipificar_opciones.split("-")[1];

        if (tipoTramiteRetificar.equals("PEDIDO")) {
            try {
                Thread.sleep(3000);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            // TODO: Selecciona el tipo de trámite
            Serenity.getDriver().findElement(By.id("requestType")).click();
            Serenity.getDriver().findElement(By.id("Pedido")).click();
        }

        if (tipoTramiteRetificar.equals("RECLAMO")) {
            try {
                Thread.sleep(3000);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            // TODO: Selecciona el tipo de trámite
            Serenity.getDriver().findElement(By.id("requestType")).click();
            Serenity.getDriver().findElement(By.id("Reclamo")).click();
        }

        String newProducto = "";
        switch (productoRetificar) {
            case "BPI":
                newProducto = "BANCA POR INTERNET";
                break;
            case "SAN_PROY":
                newProducto = "SANEAMIENTO PROY. INMB";
                break;
            case "T_NAC":
                newProducto = "TRANSF. NACIONALES";
                break;
            case "T_INT":
                newProducto = "TRANSF. INTERNACIONALES";
                break;
            case "TC":
            switch (esComercial) {
                case "COMERCIAL":
                    newProducto = "TARJETA DE CRÉDITO";
                    break;
                case "":
                    newProducto = "TARJETA DE CREDITO";
                    break;
            }
            break;
            case "TD":
                switch (esComercial) {
                    case "COMERCIAL":
                        newProducto = "TARJETA DE DÉBITO";
                        break;
                    case "":
                        newProducto = "TARJETA DE DEBITO";
                        break;

                        default:
                        System.out.println("NO ENCUENTRA NUEVO PRODUCTO: " + newProducto);
                }
                break;

            case "CUENTA":
                newProducto = "CUENTA";
                break;
            case "CUENTA_CORRIENTE":
                newProducto = "CUENTA CORRIENTE";
                break;
            case "CBME":
                newProducto = "CBME";
                break;
            case "CARTA_FIANZA_CERTIFICADO":
                newProducto = "CARTA FIANZA/CERTIFICADO";
                break;
            case "EFECTIVO_GARANTIA_HIPOTECARIO":
                newProducto = "EFECTIVO CON GARANTÍA HIPOTECARIO";
                break;
            case "PRESTAMO_PERSONAL":
                newProducto = "PRÉSTAMO PERSONAL";
                break;
            case "REFINANCIADO":
                newProducto = "REFINANCIADO";
                break;
            case "VEHICULAR":
                newProducto = "VEHICULAR";
                break;
            case "HIPOTECARIO":
                newProducto = "HIPOTECARIO";
                break;
            case "ADELANTO_SUELDO":
                newProducto = "ADELANTO SUELDO";
                break;
            case "LINEA_CONVENIO":
                newProducto = "LINEA CONVENIO";
                break;
            case "SEGUROS":
                newProducto = "SEGUROS";
                break;
            case "SEGUROS_RELACIONADOS":
                newProducto = "SEGUROS RELACIONADOS";
                break;
            case "RAPPIBANK_TARJETA_CREDITO":
                newProducto = "RAPPIBANK - TARJETA DE CRÉDITO";
                break;
            case "RAPPIBANK_CUENTAS":
                newProducto = "RAPPIBANK - CUENTAS";
                break;
            case "COBRANZAS":
                newProducto = "COBRANZAS";
                break;
            case "REMESAS":
                newProducto = "REMESAS";
                break;
            case "PODER":
                newProducto = "PODER";
                break;
            case "APPIBK":
                newProducto = "APP INTERBANK";
                break;
            case "BANCA_CELULAR":
                newProducto = "BANCA CELULAR";
                break;
            case "AUTOMATIC_DEBIT":
                newProducto = "DEBITO AUTOMATICO";
                break;
            case "PAGO_SERVICIO_CANALES":
                newProducto = "PAGO DE SERVICIO POR CANALES";
                break;
            case "TRANSF_NACIONALES":
                newProducto = "TRANSF. NACIONALES";
                break;
            case "BANCA_POR_INTERNET":
                newProducto = "BANCA POR INTERNET";
                break;
            case "BILLETERA_ELECTRONICA":
                newProducto = "BILLETERA ELECTRÓNICA";
                break;
            case "IB_AGENTE":
                newProducto = "IB AGENTE";
                break;
            case "PAYPAL":
                newProducto = "PAY PAL";
                break;
            case "PLIN":
                newProducto = "PLIN";
                break;
            case "DIGITAL_CARD":
                newProducto = "DIGITAL_CARD";
                break;
            case "TOKEN":
                newProducto = "TOKEN";
                break;
            case "TRANSF_INTERNACIONALES":
                newProducto = "TRANSF. INTERNACIONALES";
                break;
            case "TUNKI":
                newProducto = "TUNKI";
                break;
            case "PAGOS_PLANILLAS_PROVEEDORES":
                newProducto = "PAGOS PLANILLAS/PROVEEDORES";
                break;
            case "SUNAT":
                newProducto = "SUNAT";
                break;
            case "MONEY_EXCHANGE_APP":
                newProducto = "MONEY EXCHANGE APP";
                break;
            case "ATENCION_CLIENTE":
                newProducto = "ATENCION AL CLIENTE";
                break;
            case "CUENTA_NEGOCIOS":
                newProducto = "CUENTA NEGOCIOS";
                break;
            case "CBPE":
                newProducto = "CRÉDITO CBPE";
                break;


            default:
                System.out.println("NO ENCUENTRA NUEVO PRODUCTO: " + newProducto);
        }


        String nuevoTipologia = "";
        switch (tipologiaRetificar.trim()) {

            case "desacuerdoConLaAtencion":
                nuevoTipologia = "Desacuerdo con la atención";
                break;
            case "cobrosIndebidos":
                nuevoTipologia = "Cobros Indebidos";
                break;
            case "exoneracionCobros":
                nuevoTipologia = "Exoneración de Cobros";
                break;
            case "consumosNoReconocidosFraude":
                nuevoTipologia = "Consumos no reconocidos por fraude";
                break;
            case "consumosMalProcesados":
                nuevoTipologia = "Consumos mal procesados por el comercio/marca";
                break;
            case "cuentaCancelacionNoReconocida":
                nuevoTipologia = "Cuenta/Cancelación no reconocida";
                break;
            case "transaccionMalONoProcesada":
                nuevoTipologia = "Transacción mal o no procesada";
                break;
            case "programaRecompensas":
                nuevoTipologia = "Programa de Recompensas";
                break;
            case "pagoAnticipadoAdelantoCuotas":
                nuevoTipologia = "Pago Anticipado y Adelanto de Cuotas";
                break;
            case "devolucionSaldoAcreedor":
                nuevoTipologia = "Devolucion de saldo acreedor";
                break;
            case "duplicadoEstadoCuentaServicioCopias":
                nuevoTipologia = "Duplicado de Estado de Cuenta/Servicio de Copias";
                break;
            case "modificacionPlazo":
                nuevoTipologia = "Modificacion Plazo";
                break;
            case "insuficienteInformacion":
                nuevoTipologia = "Insuficiente información";
                break;
            case "incorrectaAplicacionCuotas":
                nuevoTipologia = "Incorrecta aplicación CD/EC/C. cuotas";
                break;
            case "sistemaRecompensas":
                nuevoTipologia = "Sistema de Recompensas";
                break;
            case "desacuerdoCondicionesTT":
                nuevoTipologia = "Desacuerdo de Condiciones/Tasas/Tarifas";
                break;
            case "duplicadoDocumento":
                nuevoTipologia = "Duplicado de documento";
                break;
            case "informacionMovimientos":
                nuevoTipologia = "Informacion de movimientos";
                break;
            case "liberacionRetencionesJudiciales":
                nuevoTipologia = "Liberación de Retenciones Judiciales";
                break;
            case "noRecepcionDocumento":
                nuevoTipologia = "No recepción de documento";
                break;
            case "duplicadoVoucher":
                nuevoTipologia = "Duplicado de voucher";
                break;
            case "bajaTasa":
                nuevoTipologia = "Baja de Tasa";
                break;
            case "modificacionGrupoLiquidacion":
                nuevoTipologia = "Modificación de grupo de liquidación";
                break;
            case "reduccionLineaTC":
                nuevoTipologia = "Reducción de Línea TC";
                break;
            case "trasladoSaldoAcreedor":
                nuevoTipologia = "Traslado de saldo acreedor";
                break;
            case "afiliaDesafiliaDebito":
                nuevoTipologia = "Afiliación / desafiliación débito automático";
                break;
            case "regrabacionPlastico":
                nuevoTipologia = "Regrabación de plástico";
                break;
            case "liberacionRetenciones":
                nuevoTipologia = "Liberacion de retenciones";
                break;
            case "operacionDenegada":
                nuevoTipologia = "Operación Denegada";
                break;
            case "reporteIndebidoCentralesRiesgo":
                nuevoTipologia = "Reporte indebido Centrales de Riesgo";
                break;
            case "modificacionLineaTCAdicional":
                nuevoTipologia = "Modificación Línea TC Adicional";
                break;
            case "constanciaNoAdeudo":
                nuevoTipologia = "Constancia de no adeudo";
                break;
            case "constanciaPagoRealizado":
                nuevoTipologia = "Constancia de pago realizado";
                break;
            case "direccionTelfIndebidosCobranza":
                nuevoTipologia = "Dirección/Telf. indebidos de cobranza";
                break;
            case "aTMDeIbNoDepositoDinero":
                nuevoTipologia = "ATM de IB no depositó dinero";
                break;
            case "aTMDeIbNoEntregoDinero":
                nuevoTipologia = "ATM de IB no entregó dinero";
                break;
            case "constanciaTransferenciasRecibidas":
                nuevoTipologia = "Constancia por transferencias recibidas";
                break;
            case "noRecepcionTransferenciaInOut":
                nuevoTipologia = "No recepción de transferencia IN/OUT";
                break;
            case "duplicadoCronograma":
                nuevoTipologia = "Duplicado de cronograma";
                break;
            case "modificacionConfiguracionCredito":
                nuevoTipologia = "Modificación configuración crédito";
                break;
            case "anulacionCredito":
                nuevoTipologia = "Anulación de crédito";
                break;
            case "devolucionDocumentos":
                nuevoTipologia = "Devolución de documentos";
                break;
            case "modificacionCondicionesCuotasCredito":
                nuevoTipologia = "Modificación Condiciones Cuotas Crédito";
                break;
            case "modificacionCondicionesContractuales":
                nuevoTipologia = "Modificación de condiciones contractuales";
                break;
            case "unificacionMarcasTiposTarjetas":
                nuevoTipologia = "Unificación de marcas/tipos de tarjetas";
                break;
            case "emisionPreConformidadAfp25":
                nuevoTipologia = "Emisión de pre conformidad AFP 25%";
                break;
            case "consumoRetiroNoEfectuado":
                nuevoTipologia = "Consumo/ Retiro No Efectuado";
                break;
            case "solicitudDeIntérpreteParaUsuario":
                nuevoTipologia = "Solicitud de intérprete para usuario";
                break;
            case "DesacuerdoConElServicio":
                nuevoTipologia = "Desacuerdo con el servicio";
                break;
            case "ModificacionDatosPersonales":
                nuevoTipologia = "Modificación datos personales";
                break;
            case "VideoArco":
                nuevoTipologia = "Video Arco";
                break;
            case "BilleteFalso":
                nuevoTipologia = "Billete Falso";
                break;
            case "endosoSegurosBien":
                nuevoTipologia = "Endoso Seguros del Bien";
                break;
            case "cambioFormaEnvioCorrespondencia":
                nuevoTipologia = "Cambio de forma de Envío de correspondencia";
                break;
            case "cuotaFlexible":
                nuevoTipologia = "Cuota flexible";
                break;
            case "endosoSegurosDesgravamen":
                nuevoTipologia = "Endoso Seguros de Desgravamen";
                break;
            case "copiaBoletaDepósitoLima":
                nuevoTipologia = "Copia de boleta de depósito Lima";
                break;
            case "aTMOtroBancoNoEntregoDinero":
                nuevoTipologia = "ATM de otro banco no entregó dinero";
                break;
            case "revisionDepositosTerceros":
                nuevoTipologia = "Revisión de depósitos a terceros";
                break;
            case "liquidacionDeuda":
                nuevoTipologia = "Liquidación de deuda";
                break;
            case "correccionPagoRealizado":
                nuevoTipologia = "Corrección de pago realizado";
                break;
            case "modificacionDisposicionEfectivo":
                nuevoTipologia = "Modificación Disposición de Efectivo";
                break;
            case "reversionUpgrade":
                nuevoTipologia = "Reversión de Upgrade";
                break;
            case "correoTelefonoIndebidoCampanas":
                nuevoTipologia = "Correo/Teléfono Indebido de campañas";
                break;
            case "insatisfaccionGestionEntregaTarjeta":
                nuevoTipologia = "Insatisfacción gestión de entrega de tarjeta";
                break;
            case "devolucionCuotasTransito":
                nuevoTipologia = "Devolución cuotas en tránsito";
                break;
            case "mejoraCondicionesChipRet":
                nuevoTipologia = "Mejora de Condiciones CHIP- Ret";
                break;
            case "activacionOrdenesPago":
                nuevoTipologia = "Activación de Ordenes de Pago";
                break;
            case "informacionEstablecimientoRetEfectivo":
                nuevoTipologia = "Información de establecimiento/ ret.efectivo";
                break;
            case "incumplimientoCampaña":
                nuevoTipologia = "Incumplimiento de campaña";
                break;
            case "liberaciónRetenciones":
                nuevoTipologia = "Liberación de retenciones";
                break;

            default:
                System.out.println("NO ENCUENTRA NUEVA TIPOLOGIA: " + nuevoTipologia);
        }


        System.out.println("=======================================\nRETIPIFICACION - SE ACCEDE A CAMBIAR TIPOLOGIA FINAL ES: " + nuevoTipologia.toUpperCase() + "\n=======================================");
        try {
            Thread.sleep(2000);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        //TODO: Selecciona la tipología en Retipificación
        Serenity.getDriver().findElement(By.id("typology")).click();
        Serenity.getDriver().findElement(By.xpath("//app-ibk-select//div[@id='typology']//span[contains(text(), '" + nuevoTipologia + "')]")).click();

        try {
            Thread.sleep(2000);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        //TODO: VARIABLES GLOBALES
        String nuevoMotivo = "";

        try {
            //TODO: Nuevo Motivo
            nuevoMotivo = Serenity.sessionVariableCalled("nuevoMotivo");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }


        //TODO: FUNCION TDM
        String ambiente = Serenity.sessionVariableCalled("ambiente");
        String motivo = Serenity.sessionVariableCalled("nuevoMotivo").toString();

        //TODO: OBTIENE DESDE TDM - MOTIVOS
        int cantidadMotivo = 0;
        String motivos2 = "";

        if (esComercial.equals("COMERCIAL")) {
            List<ObtenerMotivoIdComercial> listamotivos = Do.getMotivoIdComercial(ambiente, newProducto, nuevoTipologia.toUpperCase(Locale.ROOT));
            String[] selecciones = motivo.split("-");
            cantidadMotivo = selecciones.length;

            for (int n = 0; n < listamotivos.size(); n++) {

                for (int k = 0; k < selecciones.length; k++) {
                    String id = listamotivos.get(Integer.valueOf(selecciones[k]) - 1).getIdMotivo();

                    if (n != listamotivos.size() - 1) {
                        motivos2 = motivos2 + id + "-";
                    } else {
                        motivos2 = motivos2 + id;
                    }
                }
            }

        }
        else {
            List<ObtenerMotivoId> listamotivos = Do.getMotivoId(ambiente, newProducto, nuevoTipologia.toUpperCase(Locale.ROOT));
            String[] selecciones = motivo.split("-");
            cantidadMotivo = selecciones.length;

            for (int n = 0; n < listamotivos.size(); n++) {

                for (int k = 0; k < selecciones.length; k++) {
                    String id = listamotivos.get(Integer.valueOf(selecciones[k]) - 1).getIdMotivo();

                    if (n != listamotivos.size() - 1) {
                        motivos2 = motivos2 + id + "-";
                    } else {
                        motivos2 = motivos2 + id;
                    }
                }
            }
        }


        for (int i = 0; i < cantidadMotivo; i++) {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException ie) {
                System.out.println(ie);
            }

            if (motivo.contains("-")) {
                System.out.println("Motivo a seleccionar: " + motivo.split("-")[i] + " de los motivos " + motivo);
            } else {
                System.out.println("Motivo a seleccionar: " + motivos2.split("-")[i] + " de los motivos " + motivo);
            }

            String motivoselect = "";
            motivoselect = motivos2.split("-")[i];

            nuevoMotivo = motivoselect;
        }


        if(esComercial.equals("COMERCIAL")){
            int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
            int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

            // TODO: PRODUCTOS
            if (newProducto.equals("CUENTA NEGOCIOS")) {
                if (nuevoTipologia.equals("Cobros Indebidos")) {
                    for (int i = 0; i < motivoRetificar; i++) {
                        for (int j = 0; j < movimientoRetificar; j++) {
                            //TODO: Selecciona Motivo
                            Serenity.getDriver().findElement(By.id("Reason_"+i)).click();
                            Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                            //TODO: AGREGAR MOVIMIENTO
                            if(i>0) {Serenity.getDriver().findElement(By.id("Add_Amount_" + i)).click();}
                            //TODO: Agrega movimientos
                            Serenity.getDriver().findElement(By.xpath("(//*[@id='Moneda_" + i + "_" + j + "'])")).click();
                            Serenity.getDriver().findElement(By.xpath("(//*[@id='Moneda_" + i + "_" + j + "'])/div[2]/ul/li[@id='PEN']")).click();

                            Serenity.getDriver().findElement(By.xpath("//*[@id='Monto_" + i + "_" + j + "']")).clear();
                            Serenity.getDriver().findElement(By.xpath("//*[@id='Monto_" + i + "_" + j + "']")).sendKeys("100");

                            Serenity.getDriver().findElement(By.xpath("//*[@id='Fecha_" + i + "_" + j + "']")).clear();
                            Serenity.getDriver().findElement(By.xpath("//*[@id='Fecha_" + i + "_" + j + "']")).sendKeys(getValueFecha());
                            //TODO: Ingresa - Hora de Operación
                            Serenity.getDriver().findElement(By.xpath("(//input[@id='Hora_" + i + "_" + j + "'])[last()]")).sendKeys("10:30");
                            //TODO: Ingresa - Establecimiento/Transaccion
                            Serenity.getDriver().findElement(By.xpath("(//input[@id='EstablecimientoTransaccion" + i + "_" + j + "'])[last()]")).sendKeys("RAPPI COMPRA");
                        }
                    }
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Ingresa - N° de CUENTA afecto
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='affectedAccountNumber'])")).sendKeys("1231234567896");
                    try {
                        Thread.sleep(1000);
                    } catch (Exception e) {
                    }

                    ///TODO: Ingresa - Tipo de cuenta
                    Serenity.getDriver().findElement(By.id("accountType")).click();
                    Serenity.getDriver().findElement(By.xpath("(//li[@id=1])[last()]")).click();
                    ///TODO: Ingresa - moneda
                    Serenity.getDriver().findElement(By.id("currency")).click();
                    Serenity.getDriver().findElement(By.xpath("(//li[@id='PEN'])[last()]")).click();

                    ///TODO: Ingresa - Num cuenta devolucion
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='devolutionAccountNumber'])")).sendKeys("1231234567896");
                    ///TODO: Ingresa - Canal
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueCanal + "'])[last()]")).click();
                }
            } else
            if (newProducto.equals("CUENTA CORRIENTE")) {
                if (nuevoTipologia.equals("Exoneración de Cobros")) {
                    for (int i = 0; i < motivoRetificar; i++) {
                        for (int j = 0; j < movimientoRetificar; j++) {
                            //TODO: Selecciona Motivo
                            Serenity.getDriver().findElement(By.id("Reason_"+i)).click();
                            Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                            //TODO: AGREGAR MOVIMIENTO
                            Serenity.getDriver().findElement(By.id("Add_Amount_" + i)).click();
                            //TODO: Agrega movimientos
                            Serenity.getDriver().findElement(By.xpath("(//*[@id='Moneda_" + i + "_" + j + "'])")).click();
                            Serenity.getDriver().findElement(By.xpath("(//*[@id='Moneda_" + i + "_" + j + "'])/div[2]/ul/li[@id='PEN']")).click();

                            Serenity.getDriver().findElement(By.xpath("//*[@id='Monto_" + i + "_" + j + "']")).clear();
                            Serenity.getDriver().findElement(By.xpath("//*[@id='Monto_" + i + "_" + j + "']")).sendKeys("120.30");

                            Serenity.getDriver().findElement(By.xpath("//*[@id='Fecha_" + i + "_" + j + "']")).clear();
                            Serenity.getDriver().findElement(By.xpath("//*[@id='Fecha_" + i + "_" + j + "']")).sendKeys(getValueFecha());
                            //TODO: Ingresa - Hora de Operación
                            Serenity.getDriver().findElement(By.xpath("(//input[@id='Hora_" + i + "_" + j + "'])[last()]")).sendKeys("10:30");
                            //TODO: Ingresa - Establecimiento/Transaccion
                            Serenity.getDriver().findElement(By.xpath("(//input[@id='EstablecimientoTransaccion" + i + "_" + j + "'])[last()]")).sendKeys("YAPE COMPRA");
                        }
                    }
                    String valueNumCuenta = Serenity.sessionVariableCalled("numCuenta").toString();
                    //TODO: Ingresa - N° de cuenta
                    Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys(valueNumCuenta);

                }
                else if(nuevoTipologia.equals("Cobros Indebidos")){
                     for (int i = 0; i < motivoRetificar; i++) {
                        for (int j = 0; j < movimientoRetificar; j++) {
                            //TODO: Selecciona Motivo
                            Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                            Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                            //TODO: Agrega movimientos
                            Serenity.getDriver().findElement(By.xpath("(//div[@id='Moneda'])[last()]")).click();
                            Serenity.getDriver().findElement(By.xpath("(//li[@id='USD'])[last()]")).click();

                            Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[last()]")).sendKeys("120.30");

                            Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[last()]")).sendKeys(getValueFecha());
                            //TODO: Ingresa - Hora de Operación
                            Serenity.getDriver().findElement(By.xpath("(//input[@id='Hora'])[last()]")).sendKeys("10:30");
                            //TODO: Ingresa - Establecimiento/Transaccion
                            Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("MISKA COMPRA");
                        }
                    }
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Ingresa - N° de cuenta
                    Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("1126008996500000");
                    //TODO: Seleccionar - Canal utilizado/reclamado
                    Serenity.getDriver().findElement(By.id("channelClaimed")).click();
                    Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueCanal + "'])[last()]")).click();
                }

            } else
            if (newProducto.equals("TARJETA DE DÉBITO")){
                if (nuevoTipologia.equals("Consumos no reconocidos por fraude")) {
                    for (int i = 0; i < motivoRetificar; i++) {
                        for (int j = 0; j < movimientoRetificar; j++) {
                            //TODO: Selecciona Motivo
                            Serenity.getDriver().findElement(By.id("Reason_"+i)).click();
                            Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                            //TODO: AGREGAR MOVIMIENTO
                            if(i>0) {Serenity.getDriver().findElement(By.id("Add_Amount_" + i)).click();}
                            //TODO: Agrega movimientos
                            Serenity.getDriver().findElement(By.xpath("(//*[@id='Moneda'][last()])")).click();
                            Serenity.getDriver().findElement(By.xpath("(//*[@id='Moneda'])/div[2]/ul/li[@id='PEN']")).click();

                            Serenity.getDriver().findElement(By.xpath("//*[@id='Monto'][last()]")).clear();
                            Serenity.getDriver().findElement(By.xpath("//*[@id='Monto']")).sendKeys("150.63");

                            Serenity.getDriver().findElement(By.xpath("//*[@id='Fecha'][last()]")).clear();
                            Serenity.getDriver().findElement(By.xpath("//*[@id='Fecha']")).sendKeys(getValueFecha());
                           //TODO: Ingresa - Establecimiento/Transaccion
                            Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("RAPPI COMPRA");
                        }
                    }

                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    String valueCuenta = "1233333444456";
                    String valueNumTarjeta = "2788899992838399999";
                    String valueCuentaMancomunada = "Si";
                    String valueQuePasoTarjeta="";
                    String valueFlagTarjetaBloqueo = "1";
                    String nuevoMotivoItem="1";

                    if (nuevoMotivoItem.equals("1")){ valueQuePasoTarjeta = "Fue robada o usada en un secuestro";}else
                    if (nuevoMotivoItem.equals("2")){  valueQuePasoTarjeta = "Está en poder del cliente"; }


                    //TODO: Ingresa - N° de cuenta
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).sendKeys(valueCuenta);
                    //TODO: Ingresa - N° de tarjeta
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[2]")).sendKeys(valueNumTarjeta);
                    //TODO: Selecciona - Es o no cuenta mancomunada
                    if(valueCuentaMancomunada.equals("Si")) {
                        Serenity.getDriver().findElement(By.xpath("//label[@for='isJointAccount-1']")).click();
                    }else if(valueCuentaMancomunada.equals("No")) {
                        Serenity.getDriver().findElement(By.xpath("//label[@for='isJointAccount-0']")).click();
                    }
                    //TODO: Selecciona - Que paso con la tarjeta
                    Serenity.getDriver().findElement(By.id("debitCardHappened")).click();
                    Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueQuePasoTarjeta + "'])[last()]")).click();

                    if(valueFlagTarjetaBloqueo.equals("1")){
                        Serenity.getDriver().findElement(By.xpath("//label[@for='checkLockDebitCard']")).click();
                    }else if(valueFlagTarjetaBloqueo.equals("0")){
                        String valueRazonNoBloqueo = "No tenia internet";
                        Serenity.getDriver().findElement(By.id("commentLockDebitCard")).sendKeys(valueRazonNoBloqueo);
                    }
                    //TODO: Seleccionar - Canal utilizado/reclamado
                    Serenity.getDriver().findElement(By.id("channelUsed")).click();
                    Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueCanal + "'])[last()]")).click();
                }else
                if(nuevoTipologia.equals("Consumos mal procesados por el comercio/marca")){
                    for (int i = 0; i < motivoRetificar; i++) {
                        for (int j = 0; j < movimientoRetificar; j++) {
                            //TODO: Selecciona Motivo
                            Serenity.getDriver().findElement(By.id("Reason_"+i)).click();
                            Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                            //TODO: AGREGAR MOVIMIENTO
                            if(i>0) {Serenity.getDriver().findElement(By.id("Add_Amount_" + i)).click();}
                            //TODO: Agrega movimientos
                            //TODO: Agrega movimientos
                            Serenity.getDriver().findElement(By.xpath("(//*[@id='Moneda_" + i + "_" + j + "'])")).click();
                            Serenity.getDriver().findElement(By.xpath("(//*[@id='Moneda_" + i + "_" + j + "'])/div[2]/ul/li[@id='PEN']")).click();

                            Serenity.getDriver().findElement(By.xpath("//*[@id='Monto_" + i + "_" + j + "']")).clear();
                            Serenity.getDriver().findElement(By.xpath("//*[@id='Monto_" + i + "_" + j + "']")).sendKeys("120.30");

                            Serenity.getDriver().findElement(By.xpath("//*[@id='Fecha_" + i + "_" + j + "']")).clear();
                            Serenity.getDriver().findElement(By.xpath("//*[@id='Fecha_" + i + "_" + j + "']")).sendKeys(getValueFecha());

                            //TODO: Ingresa - Establecimiento/Transaccion
                            Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[last()]")).sendKeys("YAPE COMPRA");

                            if(motivo.equals("2468")||motivo.equals("1575")){
                                //TODO: Se intento adquirir Producto o Servicio
                                String valuePoductoServicio = "Producto";
                                if(valuePoductoServicio.equals("Producto")) {
                                    Serenity.getDriver().findElement(By.xpath("//label[@for='ps_PS001_"+i+"_"+j+"']")).click();
                                }else if(valuePoductoServicio.equals("Servicio")) {
                                    Serenity.getDriver().findElement(By.xpath("//label[@for='ps_PS002_"+i+"_"+j+"']")).click();
                                    // TODO: Seleccionar Servicio
                                    String valueNomServicio = "Latam";
                                    Serenity.getDriver().findElement(By.xpath("(//*[@id='tp_"+i+"_" + j + "'])")).click();
                                    //Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueNomServicio + "'])[last()]")).click();
                                    Serenity.getDriver().findElement(By.xpath("(//span[contains(text(),'"+valueNomServicio+"')])")).click();


                                }
                                Serenity.getDriver().findElement(By.xpath("(//input[@id='hopeDate_"+i+"_" + j + "'])[last()]")).sendKeys(getValueFecha());
                            }



                        }
                    }

                }

            } else
            if (newProducto.equals("CRÉDITO BPE")){
                if (nuevoTipologia.equals("COBROS INDEBIDOS")) {
                    for (int i = 0; i < motivoRetificar; i++) {
                        for (int j = 0; j < movimientoRetificar; j++) {
                            //TODO: Selecciona Motivo
                            Serenity.getDriver().findElement(By.id("Reason_"+i)).click();
                            Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                            //TODO: AGREGAR MOVIMIENTO
                            if(i>0) {Serenity.getDriver().findElement(By.id("Add_Amount_" + i)).click();}
                            //TODO: Agrega movimientos
                            Serenity.getDriver().findElement(By.xpath("(//*[@id='Moneda_" + i + "_" + j + "'])")).click();
                            Serenity.getDriver().findElement(By.xpath("(//*[@id='Moneda_" + i + "_" + j + "'])/div[2]/ul/li[@id='PEN']")).click();

                            Serenity.getDriver().findElement(By.xpath("//*[@id='Monto_" + i + "_" + j + "']")).clear();
                            Serenity.getDriver().findElement(By.xpath("//*[@id='Monto_" + i + "_" + j + "']")).sendKeys("120.30");

                            Serenity.getDriver().findElement(By.xpath("//*[@id='Fecha_" + i + "_" + j + "']")).clear();
                            Serenity.getDriver().findElement(By.xpath("//*[@id='Fecha_" + i + "_" + j + "']")).sendKeys(getValueFecha());
                            //TODO: Ingresa - Hora de Operación
                            Serenity.getDriver().findElement(By.xpath("(//input[@id='Hora_" + i + "_" + j + "'])[last()]")).sendKeys("10:30");
                            //TODO: Ingresa - Establecimiento/Transaccion
                            Serenity.getDriver().findElement(By.xpath("(//input[@id='EstablecimientoTransaccion" + i + "_" + j + "'])[last()]")).sendKeys("YAPE COMPRA");
                        }
                    }

                    String valueMedioDevolucion = "2";
                    String valueTipoCuenta = "2";
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    String valueMoneda = "USD";

                    //TODO: Ingresa - N° de crédito
                    Serenity.getDriver().findElement(By.id("creditNumber")).sendKeys("13035456");
                    //TODO: Seleccionar - Medio de devolución
                    Serenity.getDriver().findElement(By.id("devolutionMedium")).click();
                    Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueMedioDevolucion + "'])[last()]")).click();
                    if(valueMedioDevolucion.equals("2")){
                        //TODO: Seleccionar - Tipo Cuenta
                        Serenity.getDriver().findElement(By.id("devolutionAccountType")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueTipoCuenta + "'])[last()]")).click();
                        //TODO: Seleccionar - Moneda
                        Serenity.getDriver().findElement(By.id("currency")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueMoneda + "'])[last()]")).click();
                        //TODO: Ingresa - N° de cuenta para devolución
                        Serenity.getDriver().findElement(By.id("devolutionAccountNumber")).sendKeys("1152330000000");
                    }

                    //TODO: Seleccionar - Canal utilizado/reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueCanal + "'])[last()]")).click();
                }
            }

            // TODO: ATENCIÓN
            if (newProducto.equals("ATENCIÓN AL CLIENTE")){
                if (nuevoTipologia.equals("Desacuerdo con la atención")){
                    String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                    //TODO: Ingresa - Fecha del incidente
                    Serenity.getDriver().findElement(By.id("incidentDate")).sendKeys(getValueFecha());
                    //TODO: Seleccionar - Canal utilizado/reclamado
                    Serenity.getDriver().findElement(By.id("productTypeCompany")).click();
                    Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueCanal + "'])[last()]")).click();
                }
            }
            Serenity.setSessionVariable("tipoproducto").to(newProducto);
        }
        else{
            //****RETAIL****
            // TODO: PRODUCTOS
        if (newProducto.equals("TARJETA DE CREDITO")) {

            if (nuevoTipologia.equals("Cobros Indebidos") || nuevoTipologia.equals("Exoneración de Cobros")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Monto_" + i + "_" + j + "']")).clear();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Monto_" + i + "_" + j + "']")).sendKeys("100");

                        Serenity.getDriver().findElement(By.xpath("//*[@id='Fecha_" + i + "_" + j + "']")).clear();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Fecha_" + i + "_" + j + "']")).sendKeys(getValueFecha());
                    }
                }


            }

            if (nuevoTipologia.equals("Consumos no reconocidos por fraude") || nuevoTipologia.equals("Consumos mal procesados por el comercio/marca")) {

                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Monto_" + i + "_" + j + "']")).clear();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Monto_" + i + "_" + j + "']")).sendKeys("100");

                        Serenity.getDriver().findElement(By.xpath("//*[@id='Fecha_" + i + "_" + j + "']")).clear();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Fecha_" + i + "_" + j + "']")).sendKeys(getValueFecha());

                        Serenity.getDriver().findElement(By.xpath("//*[@id='Comercio" + i + "_" + j + "']")).clear();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Comercio" + i + "_" + j + "']")).sendKeys(getValueDescripcion());

                    }
                }
            }

            if (nuevoTipologia.equals("Transacción mal o no procesada")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Monto_" + i + "_" + j + "']")).clear();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Monto_" + i + "_" + j + "']")).sendKeys("100");

                        Serenity.getDriver().findElement(By.xpath("//*[@id='Fecha_" + i + "_" + j + "']")).clear();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Fecha_" + i + "_" + j + "']")).sendKeys(getValueFecha());

                        Serenity.getDriver().findElement(By.xpath("//*[@id='Establecimiento" + i + "_" + j + "']")).clear();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Establecimiento" + i + "_" + j + "']")).sendKeys(getValueDescripcion());

                        //TODO: Seleccinar - Canal utilizado / Reclamado
                        Serenity.getDriver().findElement(By.xpath("//*[@id='CanalUtilizadoReclamado_" + i + "_" + j + "']")).click();
                        Serenity.getDriver().findElement(By.xpath("//div[@id='CanalUtilizadoReclamado_" + i + "_" + j + "']//div//li[@id='App IBK']")).click();
                    }
                }
            }

            if (nuevoTipologia.equals("Sistema de Recompensas")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Seleccionar Beneficio
                Serenity.getDriver().findElement(By.id("beneficio")).click();
                Serenity.getDriver().findElement(By.id("Efectivo")).click();
                //TODO: Ingresar Importe
                Serenity.getDriver().findElement(By.xpath("//input[@formcontrolname='importe']")).sendKeys("100");
                //TODO: Ingresar Fecha de Operación
                Serenity.getDriver().findElement(By.xpath("//input[@formcontrolname='fechaOperacion']")).sendKeys(getValueFecha());
            }

            if (nuevoTipologia.equals("Insuficiente información")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
            }

            if (nuevoTipologia.equals("Desacuerdo de Condiciones/Tasas/Tarifas") || nuevoTipologia.equals("Reporte indebido Centrales de Riesgo")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();

                //TODO: Seleccionar - Canal utilizado/reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("App IBK")).click();
            }

            if (nuevoTipologia.equals("Incorrecta aplicación CD/EC/C. cuotas")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
            }

            if (nuevoTipologia.equals("No recepción de documento")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Seleccionar Tipo de documento
                Serenity.getDriver().findElement(By.id("documentType")).click();
                Serenity.getDriver().findElement(By.id("Contrato")).click();
                //TODO: Seleccionar Tipo de envío
                Serenity.getDriver().findElement(By.id("shipmentType")).click();
                Serenity.getDriver().findElement(By.id("Envío físico")).click();
            }

            if (nuevoTipologia.equals("Liberacion de retenciones") || nuevoTipologia.equals("Operación Denegada")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Monto_" + i + "_" + j + "']")).clear();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Monto_" + i + "_" + j + "']")).sendKeys("100");

                        Serenity.getDriver().findElement(By.xpath("//*[@id='Fecha_" + i + "_" + j + "']")).clear();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Fecha_" + i + "_" + j + "']")).sendKeys(getValueFecha());

                        Serenity.getDriver().findElement(By.xpath("//*[@id='Establecimiento" + i + "_" + j + "']")).clear();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Establecimiento" + i + "_" + j + "']")).sendKeys(getValueDescripcion());
                    }
                }

            }

            if (nuevoTipologia.equals("ATM de IB no depositó dinero")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Importe Solicitado
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ImporteSolicitado'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ImporteSolicitado'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Importe Reclamado
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Hora de Operación
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Hora'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Hora'])[" + (j + 1) + "]")).sendKeys("12:12");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='EstablecimientoTransaccion'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='EstablecimientoTransaccion'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());

                    }
                }
                //TODO: Seleccionar - Ubicación del Cajero
                Serenity.getDriver().findElement(By.id("Descripcion_")).clear();
                Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("Bloom Tower");
                //TODO: Seleccionar - Medio de Abono
                Serenity.getDriver().findElement(By.id("paymentMedium")).click();
                Serenity.getDriver().findElement(By.id("Abono en Cuenta")).click();
                //TODO: Seleccionar - Canal utlizado / reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Bca Telef")).click();
            }

            if (nuevoTipologia.equals("ATM de IB no entregó dinero")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Importe Solicitado
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ImporteSolicitado'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ImporteSolicitado'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Importe Reclamado
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='EstablecimientoTransaccion'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='EstablecimientoTransaccion'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                    }
                }
                //TODO: Seleccionar - Ubicación del Cajero
                Serenity.getDriver().findElement(By.id("locationCashier")).clear();
                Serenity.getDriver().findElement(By.id("locationCashier")).sendKeys("Bloom Tower");
                //TODO: Seleccionar - Canal utlizado / reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Bca Telef")).click();
            }

            if (nuevoTipologia.equals("Duplicado de voucher")) {

                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Monto_" + i + "_" + j + "']")).clear();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Monto_" + i + "_" + j + "']")).sendKeys("100");

                        Serenity.getDriver().findElement(By.xpath("//*[@id='Fecha_" + i + "_" + j + "']")).clear();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Fecha_" + i + "_" + j + "']")).sendKeys(getValueFecha());

                        Serenity.getDriver().findElement(By.xpath("//*[@id='Establecimiento" + i + "_" + j + "']")).clear();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Establecimiento" + i + "_" + j + "']")).sendKeys(getValueDescripcion());

                    }
                }
            }

            if (nuevoTipologia.equals("Modificacion Plazo")) {

                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Monto_" + i + "_" + j + "']")).clear();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Monto_" + i + "_" + j + "']")).sendKeys("100");

                        Serenity.getDriver().findElement(By.xpath("//*[@id='Fecha_" + i + "_" + j + "']")).clear();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Fecha_" + i + "_" + j + "']")).sendKeys(getValueFecha());

                        Serenity.getDriver().findElement(By.xpath("//*[@id='Establecimiento" + i + "_" + j + "']")).clear();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Establecimiento" + i + "_" + j + "']")).sendKeys(getValueDescripcion());

                        //TODO: Ingresa - Cuotas Solicitadas
                        Serenity.getDriver().findElement(By.xpath("//*[@id='CuotasSolicitadas_" + i + "_" + j + "']")).clear();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='CuotasSolicitadas_" + i + "_" + j + "']")).sendKeys("18");
                    }
                }
            }

            if (nuevoTipologia.equals("Programa de Recompensas")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Seleccionar - Beneficio
                Serenity.getDriver().findElement(By.id("beneficio")).click();
                Serenity.getDriver().findElement(By.id("Efectivo")).click();
                //TODO: Tipo Moneda
                Serenity.getDriver().findElement(By.id("moneda")).click();
                Serenity.getDriver().findElement(By.id("PEN")).click();
                //TODO: Ingresar Importe
                Serenity.getDriver().findElement(By.xpath("//input[@formcontrolname='importe']")).sendKeys("150");
                //TODO: Ingresar Fecha de Operación
                Serenity.getDriver().findElement(By.xpath("//input[@formcontrolname='fechaOperacion']")).sendKeys(getValueFecha());
            }

            if (nuevoTipologia.equals("Afiliación / desafiliación débito automático")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();

                if (nuevoMotivo.equals("140") || nuevoMotivo.equals("141") || nuevoMotivo.equals("167") || nuevoMotivo.equals("168")) {
                    //TODO: Moneda de deuda TC - Cuenta
                    //TODO: Seleccionar - Moneda de deuda TC - Soles
                    Serenity.getDriver().findElement(By.id("accountNumberPEN")).click();
                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='accountNumberPEN']//li)[1]")).click();
                    //TODO: Seleccionar - Moneda de deuda TC - Dolar
                    Serenity.getDriver().findElement(By.id("accountNumberUSD")).click();
                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='accountNumberUSD']//li)[1]")).click();
                }

                if (nuevoMotivo.equals("142") || nuevoMotivo.equals("169")) {
                    Serenity.getDriver().findElement(By.id("tipoPago")).click();
                    Serenity.getDriver().findElement(By.xpath("(//li[@id='Pago mes'])[1]")).click();
                    //TODO: Moneda de deuda TC - Cuenta
                    //TODO: Seleccionar - Moneda de deuda TC - Soles
                    Serenity.getDriver().findElement(By.id("accountNumberPEN")).click();
                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='accountNumberPEN']//li)[1]")).click();
                    //TODO: Seleccionar - Moneda de deuda TC - Dolar
                    Serenity.getDriver().findElement(By.id("accountNumberUSD")).click();
                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='accountNumberUSD']//li)[1]")).click();
                }

            }

            if (nuevoTipologia.equals("Duplicado de documento")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Seleccionar Tipo de documento
                Serenity.getDriver().findElement(By.id("documentType")).click();
                Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='documentType']//li)[1]")).click();
            }

            if (nuevoTipologia.equals("Pago Anticipado y Adelanto de Cuotas")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Monto_" + i + "_" + j + "']")).clear();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Monto_" + i + "_" + j + "']")).sendKeys("100");

                        Serenity.getDriver().findElement(By.xpath("//*[@id='Fecha_" + i + "_" + j + "']")).clear();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Fecha_" + i + "_" + j + "']")).sendKeys(getValueFecha());

                        Serenity.getDriver().findElement(By.xpath("//*[@id='Establecimiento" + i + "_" + j + "']")).clear();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Establecimiento" + i + "_" + j + "']")).sendKeys(getValueDescripcion());

                        //TODO: Selecciona - ¿Aplica devolución?
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagDerivation-0']")).click();
                    }
                }
            }

            if (nuevoTipologia.equals("Baja de Tasa")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Ingresa - Tasa solicitada %
                Serenity.getDriver().findElement(By.id("rate")).clear();
                Serenity.getDriver().findElement(By.id("rate")).sendKeys("5.00");
            }

            if (nuevoTipologia.equals("Devolucion de saldo acreedor")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Seleccionar Moneda
                Serenity.getDriver().findElement(By.id("currency")).click();
                Serenity.getDriver().findElement(By.id("PEN")).click();
                //TODO: Ingresar Importe solicitado
                Serenity.getDriver().findElement(By.id("requestedAmount")).sendKeys("10");
                //TODO: Seleccionar metodo de Pago
                Serenity.getDriver().findElement(By.id("paymentMethod")).click();
                Serenity.getDriver().findElement(By.id("Tarjeta de Crédito")).click();
                //TODO: Saldo de origen
                Serenity.getDriver().findElement(By.id("originBalance")).click();
                Serenity.getDriver().findElement(By.xpath("(//div[@id='originBalance'])//div[2]/ul/li[1]")).click();
            }

            if (nuevoTipologia.equals("Modificación de grupo de liquidación")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Seleccionar - Grupo de liquidación actual
                Serenity.getDriver().findElement(By.id("currentLiquidationGroup")).click();
                Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='currentLiquidationGroup']//li)[1]")).click();
                //TODO: Seleccionar - Grupo de liquidación solicitado
                Serenity.getDriver().findElement(By.id("requestedLiquidationGroup")).click();
                Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='requestedLiquidationGroup']//li)[1]")).click();
            }

            if (nuevoTipologia.equals("Modificación Línea TC Adicional")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Seleccionar - Tarjeta de crédito
                Serenity.getDriver().findElement(By.id("creditCard_0")).click();
                Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='creditCard_0']//li)[1]")).click();
                //TODO: Seleccionar Moneda
                Serenity.getDriver().findElement(By.id("currency_0")).click();
                Serenity.getDriver().findElement(By.id("PEN")).click();
                //TODO: Ingresar - Monto
                Serenity.getDriver().findElement(By.xpath("//input[@formcontrolname='amount']")).sendKeys("220");
            }

            if (nuevoTipologia.equals("Reducción de Línea TC")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Seleccionar Moneda
                Serenity.getDriver().findElement(By.id("currency")).click();
                Serenity.getDriver().findElement(By.id("PEN")).click();
                //TODO: Ingresar - Línea actual
                Serenity.getDriver().findElement(By.xpath("//*[contains(text(), 'Línea actual')]/../input")).sendKeys("1000");
                //TODO: Ingresar - Línea solicitada
                Serenity.getDriver().findElement(By.xpath("//*[contains(text(), 'Línea solicitada')]/../input")).sendKeys("100");
            }

            if (nuevoTipologia.equals("Traslado de saldo acreedor")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Seleccionar Moneda
                Serenity.getDriver().findElement(By.id("currency")).click();
                Serenity.getDriver().findElement(By.id("PEN")).click();
                //TODO: Ingresar - Importe solicitado
                Serenity.getDriver().findElement(By.id("requestedAmount")).sendKeys("150");
            }

            if (nuevoTipologia.equals("Duplicado de Estado de Cuenta/Servicio de Copias")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Ingresa Periodo de inicio
                Serenity.getDriver().findElement(By.id("startPeriod")).sendKeys(getValueFecha());
                //TODO: Ingresa Periodo de fin
                Serenity.getDriver().findElement(By.id("endPeriod")).sendKeys("02/10/2022");
                //TODO: ¿Tienda? SI
                Serenity.getDriver().findElement(By.xpath("//label[@for='flagStore-1']")).click();
                //TODO: ¿Tienda? NO
                //Serenity.getDriver().findElement(By.xpath("//label[@for='flagStore-0']")).click();
            }

            if (nuevoTipologia.equals("Regrabación de plástico")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Ingresa - Nombre reducido
                Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("EARIAS");
            }

            if (nuevoTipologia.equals("Unificación de marcas/tipos de tarjetas")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Ingresa - Línea Solicitada
                Serenity.getDriver().findElement(By.id("requestedLine")).clear();
                Serenity.getDriver().findElement(By.id("requestedLine")).sendKeys("12312312");
                //TODO: Ingresa - Tarjeta a Cancelar
                Serenity.getDriver().findElement(By.id("cancelingCard")).clear();
                Serenity.getDriver().findElement(By.id("cancelingCard")).sendKeys("123123123123123");
            }

            if (nuevoTipologia.equals("Corrección de pago realizado")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                    }
                }

                //TODO: Seleccionar - Canal utlizado / reclamado
                Serenity.getDriver().findElement(By.id("channelUsed")).click();
                Serenity.getDriver().findElement(By.id("Televentas")).click();
            }

            if (nuevoTipologia.equals("Endoso Seguros de Desgravamen")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Ingresa - Monto endoso
                Serenity.getDriver().findElement(By.id("amount")).sendKeys("1050");
                //TODO: Ingresa - N° de póliza
                Serenity.getDriver().findElement(By.id("policyNumber")).sendKeys("123457");
                //TODO: Ingresa - Cía. de seguros
                Serenity.getDriver().findElement(By.id("insuranceCia")).sendKeys("123124");
                //TODO: Seleccione - Moneda póliza
                Serenity.getDriver().findElement(By.id("policyCurrency")).click();
                Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='policyCurrency']//li)[1]")).click();
            }

            if (nuevoTipologia.equals("Modificación Disposición de Efectivo")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Selecciona - Tarjeta de crédito
                Serenity.getDriver().findElement(By.id("creditCard_0")).click();
                Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='creditCard_0']//li)[1]")).click();
                //TODO: Selecciona - Moneda
                Serenity.getDriver().findElement(By.id("currency_0")).click();
                Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='currency_0']//li)[1]")).click();
                //TODO: Ingresa - Monto Solicitado
                Serenity.getDriver().findElement(By.xpath("//input[@formcontrolname='amount']")).sendKeys("2580");
            }

            if (nuevoTipologia.equals("Reversión de Upgrade")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Ingresa - N° de tarjeta actual
                Serenity.getDriver().findElement(By.id("currentCardNumber")).sendKeys("4213555322311542");
                //TODO: Ingresa - N° de tarjeta anterior
                Serenity.getDriver().findElement(By.id("previousCardNumber")).sendKeys("4213555322311541");
            }

            if (nuevoTipologia.equals("Información de establecimiento/ ret.efectivo") || nuevoTipologia.equals("Informacion de movimientos")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                    }
                }

            }

            if (nuevoTipologia.equals("Incumplimiento de campaña")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Seleccionar - Beneficio reclamado
                Serenity.getDriver().findElement(By.id("benefitClaimed")).click();
                Serenity.getDriver().findElement(By.id("Cashback")).click();
                //TODO: Ingresa - Importe Reclamado
                Serenity.getDriver().findElement(By.id("requestedAmount")).clear();
                Serenity.getDriver().findElement(By.id("requestedAmount")).sendKeys("2500");
                //TODO: Seleccionar - Canal utilizado/reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Bca Telef")).click();
            }

            if (nuevoTipologia.equals("ATM de otro banco no entregó dinero")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());
                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                    }
                }

                //TODO: Ingresa - Ubicación del cajero
                Serenity.getDriver().findElement(By.id("locationAgent")).clear();
                Serenity.getDriver().findElement(By.id("locationAgent")).sendKeys("Bloom Tower");
                //TODO: Seleccionar - Canal utlizado / reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Bca Telef")).click();
            }


        }

        if (newProducto.equals("TARJETA DE DEBITO")) {

            if (nuevoTipologia.equals("Cobros Indebidos")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Monto_" + i + "_" + j + "']")).clear();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Monto_" + i + "_" + j + "']")).sendKeys("100");

                        Serenity.getDriver().findElement(By.xpath("//*[@id='Fecha_" + i + "_" + j + "']")).clear();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Fecha_" + i + "_" + j + "']")).sendKeys(getValueFecha());

                        Serenity.getDriver().findElement(By.xpath("//*[@id='Comercio" + i + "_" + j + "']")).clear();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Comercio" + i + "_" + j + "']")).sendKeys(getValueDescripcion());
                    }
                }
            }

            if (nuevoTipologia.equals("Consumos no reconocidos por fraude") || nuevoTipologia.equals("Consumos mal procesados por el comercio/marca")) {

                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Monto_" + i + "_" + j + "']")).clear();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Monto_" + i + "_" + j + "']")).sendKeys("100");

                        Serenity.getDriver().findElement(By.xpath("//*[@id='Fecha_" + i + "_" + j + "']")).clear();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Fecha_" + i + "_" + j + "']")).sendKeys(getValueFecha());

                        Serenity.getDriver().findElement(By.xpath("//*[@id='Motivo" + i + "_" + j + "']")).clear();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Motivo" + i + "_" + j + "']")).sendKeys(getValueDescripcion());
                    }
                }
            }

            if (nuevoTipologia.equals("Liberacion de retenciones") || nuevoTipologia.equals("Operación Denegada")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Monto_" + i + "_" + j + "']")).clear();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Monto_" + i + "_" + j + "']")).sendKeys("100");

                        Serenity.getDriver().findElement(By.xpath("//*[@id='Fecha_" + i + "_" + j + "']")).clear();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Fecha_" + i + "_" + j + "']")).sendKeys(getValueFecha());

                        Serenity.getDriver().findElement(By.xpath("//*[@id='Establecimiento" + i + "_" + j + "']")).clear();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Establecimiento" + i + "_" + j + "']")).sendKeys(getValueDescripcion());
                    }
                }
            }

            if (nuevoTipologia.equals("ATM de IB no entregó dinero")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Importe Solicitado
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ImporteSolicitado'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ImporteSolicitado'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Importe Reclamado
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='EstablecimientoTransaccion'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='EstablecimientoTransaccion'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                    }
                }
                //TODO: Seleccionar - Ubicación del Cajero
                Serenity.getDriver().findElement(By.id("locationCashier")).clear();
                Serenity.getDriver().findElement(By.id("locationCashier")).sendKeys("Bloom Tower");
                //TODO: Seleccionar - Canal utlizado / reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Bca Telef")).click();
            }

            if (nuevoTipologia.equals("ATM de otro banco no entregó dinero")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());
                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                    }
                }

                //TODO: Seleccionar - Ubicación del cajero
                Serenity.getDriver().findElement(By.id("locationCashier")).clear();
                Serenity.getDriver().findElement(By.id("locationCashier")).sendKeys("Bloom Tower");
                //TODO: Seleccionar - Canal utlizado / reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Bca Telef")).click();
            }

            if (nuevoTipologia.equals("Revisión de depósitos a terceros")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());
                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='EstablecimientoTransaccion'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='EstablecimientoTransaccion'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                    }
                }

                //TODO: Seleccionar - Canal utlizado / reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Whatsapp")).click();
            }

            //TODO: *********************************************** TIPO DE TRAMITE PEDIDO ***********************************************
            if (nuevoTipologia.equals("Exoneración de Cobros")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Monto_" + i + "_" + j + "']")).clear();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Monto_" + i + "_" + j + "']")).sendKeys("100");

                        Serenity.getDriver().findElement(By.xpath("//*[@id='Fecha_" + i + "_" + j + "']")).clear();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Fecha_" + i + "_" + j + "']")).sendKeys(getValueFecha());

                        Serenity.getDriver().findElement(By.xpath("//*[@id='Comercio" + i + "_" + j + "']")).clear();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Comercio" + i + "_" + j + "']")).sendKeys(getValueDescripcion());

                    }
                }
            }

            if (nuevoTipologia.equals("Programa de Recompensas")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Ingresa - Fecha de operación
                Serenity.getDriver().findElement(By.id("operationDate")).sendKeys(getValueFecha());
            }

            if (nuevoTipologia.equals("Información de establecimiento/ ret.efectivo")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                    }
                }

            }


        }

        if (newProducto.equals("CUENTA")) {

            if (nuevoTipologia.equals("Informacion de movimientos")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Monto_" + i + "_" + j + "']")).clear();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Monto_" + i + "_" + j + "']")).sendKeys("100");

                        Serenity.getDriver().findElement(By.xpath("//*[@id='Fecha_" + i + "_" + j + "']")).clear();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Fecha_" + i + "_" + j + "']")).sendKeys(getValueFecha());

                        Serenity.getDriver().findElement(By.xpath("//*[@id='Establecimiento" + i + "_" + j + "']")).clear();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Establecimiento" + i + "_" + j + "']")).sendKeys(getValueDescripcion());

                    }
                }

            }

            if (nuevoTipologia.equals("Cuenta/Cancelación no reconocida") || nuevoTipologia.equals("Transacción mal o no procesada") ||
                    nuevoTipologia.equals("Cobros Indebidos") || nuevoTipologia.equals("Exoneración de Cobros")) {

                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Monto_" + i + "_" + j + "']")).clear();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Monto_" + i + "_" + j + "']")).sendKeys("100");

                        Serenity.getDriver().findElement(By.xpath("//*[@id='Fecha_" + i + "_" + j + "']")).clear();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Fecha_" + i + "_" + j + "']")).sendKeys(getValueFecha());

                        Serenity.getDriver().findElement(By.xpath("//*[@id='Comercio" + i + "_" + j + "']")).clear();
                        Serenity.getDriver().findElement(By.xpath("//*[@id='Comercio" + i + "_" + j + "']")).sendKeys(getValueDescripcion());

                    }
                }

            }

            if (nuevoTipologia.equals("Duplicado de Estado de Cuenta/Servicio de Copias")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Ingresa Periodo de inicio
                Serenity.getDriver().findElement(By.id("startPeriod")).sendKeys(getValueFecha());
                //TODO: Ingresa Periodo de fin
                Serenity.getDriver().findElement(By.id("endPeriod")).sendKeys("02/09/2022");
                //TODO: ¿Tienda? SI
                Serenity.getDriver().findElement(By.xpath("//label[@for='flagStore-1']")).click();
                //TODO: ¿Tienda? NO
                //Serenity.getDriver().findElement(By.xpath("//label[@for='flagStore-0']")).click();
            }

            if (nuevoTipologia.equals("Duplicado de documento")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Seleccionar Tipo de documento
                Serenity.getDriver().findElement(By.id("documentType")).click();
                Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='documentType']//li)[1]")).click();
            }

            if (nuevoTipologia.equals("Desacuerdo de Condiciones/Tasas/Tarifas")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
            }

            if (nuevoTipologia.equals("Sistema de Recompensas")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Seleccionar Beneficio
                Serenity.getDriver().findElement(By.id("beneficio")).click();
                Serenity.getDriver().findElement(By.xpath("//li[@id='Descuento cuenta sueldo']")).click();
                //TODO: Ingresar Importe
                Serenity.getDriver().findElement(By.xpath("//input[@formcontrolname='importe']")).sendKeys("100");
                //TODO: Ingresar Fecha de Operación
                Serenity.getDriver().findElement(By.xpath("//input[@formcontrolname='fechaOperacion']")).sendKeys(getValueFecha());
                //TODO: Ingresar nombre de establecimiento
                Serenity.getDriver().findElement(By.xpath("//input[@formcontrolname='establecimiento']")).sendKeys(getValueDescripcion());

            }

            if (nuevoTipologia.equals("ATM de IB no depositó dinero")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Importe Solicitado
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ImporteSolicitado'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ImporteSolicitado'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Importe Reclamado
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Hora de Operación
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Hora'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Hora'])[" + (j + 1) + "]")).sendKeys("12:12");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='EstablecimientoTransaccion'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='EstablecimientoTransaccion'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());

                    }
                }
                //TODO: Ingresa - Ubicación del Cajero
                Serenity.getDriver().findElement(By.id("locationCashier")).clear();
                Serenity.getDriver().findElement(By.id("locationCashier")).sendKeys("Bloom Tower");
                //TODO: Seleccionar - Medio de Abono
                Serenity.getDriver().findElement(By.id("paymentMedium")).click();
                Serenity.getDriver().findElement(By.id("Abono en Cuenta")).click();
                //TODO: Seleccionar - Canal utlizado / reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Bca Telef")).click();
            }

            if (nuevoTipologia.equals("Incumplimiento de campaña")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Seleccionar - Beneficio reclamado
                Serenity.getDriver().findElement(By.id("benefitClaimed")).click();
                Serenity.getDriver().findElement(By.id("Cashback")).click();
                //TODO: Ingresa - Importe Reclamado
                Serenity.getDriver().findElement(By.id("requestedAmount")).clear();
                Serenity.getDriver().findElement(By.id("requestedAmount")).sendKeys("2500");
                //TODO: Seleccionar - Canal utilizado/reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Bca Telef")).click();
            }


        }

        if (newProducto.equals("CUENTA CORRIENTE")) {

            if (nuevoTipologia.equals("Copia de boleta de depósito Lima")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {

                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("125");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                    }
                }
            }
            if (nuevoTipologia.equals("Reporte indebido Centrales de Riesgo")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Seleccionar - Canal utilizado/reclamado
                Serenity.getDriver().findElement(By.id("channel")).click();
                Serenity.getDriver().findElement(By.id("IBK Agentes")).click();
            }
        }

        if (newProducto.equals("SEGUROS")) {

            if (nuevoTipologia.equals("Cobros Indebidos")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());
                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                    }
                }

                //TODO: Selecciona - Tipo de producto
                Serenity.getDriver().findElement(By.xpath("(//*[@id='productType'])[last()]")).click();
                Serenity.getDriver().findElement(By.id("Crédito")).click();
                //TODO: Ingresa - N° de tipo de producto
                Serenity.getDriver().findElement(By.id("productTypeNumber")).clear();
                Serenity.getDriver().findElement(By.id("productTypeNumber")).sendKeys("1007003469148");
                //TODO: Seleccionar - Canal utlizado / reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Whatsapp")).click();
            }

            if (nuevoTipologia.equals("Duplicado de documento")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Ingresa - Tipo de producto
                Serenity.getDriver().findElement(By.xpath("(//*[@id='productType'])[last()]")).click();
                Serenity.getDriver().findElement(By.id("Crédito")).click();
                //TODO: Ingresa - N° de tipo de producto
                Serenity.getDriver().findElement(By.id("productTypeNumber")).clear();
                Serenity.getDriver().findElement(By.id("productTypeNumber")).sendKeys("1007003469148");
            }

            if (nuevoTipologia.equals("Informacion de movimientos")) {
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());
                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                    }
                }

                //TODO: Ingresa - Tipo de producto
                Serenity.getDriver().findElement(By.xpath("(//*[@id='productType'])[last()]")).click();
                Serenity.getDriver().findElement(By.id("Crédito")).click();
                //TODO: Ingresa - N° de tipo de producto
                Serenity.getDriver().findElement(By.id("productTypeNumber")).clear();
                Serenity.getDriver().findElement(By.id("productTypeNumber")).sendKeys("1007003469149");
            }

            if (nuevoTipologia.equals("No recepción de documento")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Ingresa - N° de tarjeta
                Serenity.getDriver().findElement(By.id("cardNumber")).clear();
                Serenity.getDriver().findElement(By.id("cardNumber")).sendKeys("1007003469148");
                //TODO: Seleccionar - Canal utlizado / reclamado
                Serenity.getDriver().findElement(By.id("channelUsed")).click();
                Serenity.getDriver().findElement(By.id("Whatsapp")).click();
            }

            if (nuevoTipologia.equals("Modificación de condiciones contractuales")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Ingresa - N° de tarjeta
                Serenity.getDriver().findElement(By.id("cardNumber")).clear();
                Serenity.getDriver().findElement(By.id("cardNumber")).sendKeys("4213555322311541");
                //TODO: Ingresa - N° de cuenta
                Serenity.getDriver().findElement(By.id("accountNumber")).clear();
                Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("4213555322311541");
                //TODO: Seleccionar - Moneda
                Serenity.getDriver().findElement(By.id("currency")).click();
                Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='currency']//li)[1]")).click();
            }

        }

        if (newProducto.equals("EFECTIVO CON GARANTÍA HIPOTECARIO") || newProducto.equals("PRÉSTAMO PERSONAL") ||
                newProducto.equals("REFINANCIADO") || newProducto.equals("VEHICULAR")) {

            if (nuevoTipologia.equals("Baja de Tasa")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Seleccionar - Tasa Solicitada
                Serenity.getDriver().findElement(By.id("rate")).clear();
                Serenity.getDriver().findElement(By.id("rate")).sendKeys("25");
            }

            if (nuevoTipologia.equals("Informacion de movimientos") || nuevoTipologia.equals("Desacuerdo de Condiciones/Tasas/Tarifas") ||
                    nuevoTipologia.equals("Reporte indebido Centrales de Riesgo") || nuevoTipologia.equals("Duplicado de cronograma")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
            }

            if (nuevoTipologia.equals("Modificación configuración crédito") || nuevoTipologia.equals("Modificación Condiciones Cuotas Crédito")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Selecciona Campo Editar
                Serenity.getDriver().findElement(By.id("claim-form__edit")).click();
                Serenity.getDriver().findElement(By.id("listselectbyreason_")).click();
                Serenity.getDriver().findElement(By.xpath("//*[@id='listselectbyreason_']/div/div[2]/ul/li[1]")).click();
            }

            if (nuevoTipologia.equals("Anulación de crédito") || nuevoTipologia.equals("Exoneración de Cobros") ||
                    nuevoTipologia.equals("Cobros Indebidos") || nuevoTipologia.equals("Transacción mal o no procesada") ||
                    nuevoTipologia.equals("Pago Anticipado y Adelanto de Cuotas")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());
                //TODO: Selecciona Motivo

                try {
                    WebElement reason0 = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(10)).until(ExpectedConditions.elementToBeClickable(By.id("reasonClaim")));
                    Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                    Serenity.getDriver().findElement(By.xpath("(//*[@id='reasonClaim']/div[2]/ul/li)[1]")).click();
                } catch (Exception e) {
                    System.out.println(e);
                }
                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {

                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("125");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                    }
                }
            }

            if (nuevoTipologia.equals("Devolución de documentos")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Seleccionar Tipo de documento
                Serenity.getDriver().findElement(By.id("documentType")).click();
                Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='documentType']//li)[1]")).click();
            }

            if (nuevoTipologia.equals("Duplicado de documento")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Seleccionar Tipo de documento
                Serenity.getDriver().findElement(By.id("documentType")).click();
                Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='documentType']//li)[1]")).click();
            }

            if (nuevoTipologia.equals("Cuota flexible")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Ingresa - Fecha de cuota aplazar
                Serenity.getDriver().findElement(By.id("datePostpone")).sendKeys(getValueFecha());
            }

            if (nuevoTipologia.equals("Endoso Seguros de Desgravamen")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Ingresa - Monto de endoso
                Serenity.getDriver().findElement(By.id("amount")).sendKeys("100");
                //TODO: Ingresa - N° de póliza
                Serenity.getDriver().findElement(By.id("policyNumber")).sendKeys("123456");
                //TODO: Ingresa - Cía. de seguros
                Serenity.getDriver().findElement(By.id("insuranceCia")).sendKeys("123123");
                //TODO: Seleccionar - Moneda crédito
                Serenity.getDriver().findElement(By.id("creditCurrency")).click();
                Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='creditCurrency']//li)[1]")).click();
                //TODO: Seleccione - Moneda póliza
                Serenity.getDriver().findElement(By.id("policyCurrency")).click();
                Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='policyCurrency']//li)[1]")).click();
            }

            if (nuevoTipologia.equals("Consumo/ Retiro No Efectuado")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {

                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("125");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                    }
                }

                //TODO: Seleccionar - Canal utilizado/reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("IBK Agentes")).click();
            }

            if (nuevoTipologia.equals("No recepción de documento")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Seleccionar - Canal utilizado/reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Whatsapp")).click();
            }

            if (nuevoTipologia.equals("Cambio de forma de Envío de correspondencia") || nuevoTipologia.equals("Liquidación de deuda") ||
                    nuevoTipologia.equals("Mejora de Condiciones CHIP- Ret")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
            }

            if (nuevoTipologia.equals("Emisión de pre conformidad AFP 25%")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Ingresa - Cantidad de Vouchers
                Serenity.getDriver().findElement(By.id("numberVouchers")).clear();
                Serenity.getDriver().findElement(By.id("numberVouchers")).sendKeys("15");
                //TODO: Ingresa - Fecha de Abono
                Serenity.getDriver().findElement(By.id("paymentDate")).clear();
                Serenity.getDriver().findElement(By.id("paymentDate")).sendKeys("17/06/2023");
                //TODO: Ingresa - Monto
                Serenity.getDriver().findElement(By.id("amount")).clear();
                Serenity.getDriver().findElement(By.id("amount")).sendKeys("1000");
                //TODO: Ingresa - Moneda
                Serenity.getDriver().findElement(By.id("currency")).click();
                Serenity.getDriver().findElement(By.id("PEN")).click();
            }

            if (nuevoTipologia.equals("Levantamiento de garantía")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Ingresa - Ubicación tienda
                Serenity.getDriver().findElement(By.id("locationStore")).clear();
                Serenity.getDriver().findElement(By.id("locationStore")).sendKeys("SURCO NRO 550");
                //TODO: Ingresa - Número de placa
                Serenity.getDriver().findElement(By.id("plateNumber")).clear();
                Serenity.getDriver().findElement(By.id("plateNumber")).sendKeys("F5U597");
            }

        }

        if (newProducto.equals("HIPOTECARIO")) {

            if (nuevoTipologia.equals("Baja de Tasa")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Seleccionar - Tasa Solicitada
                Serenity.getDriver().findElement(By.id("rate")).clear();
                Serenity.getDriver().findElement(By.id("rate")).sendKeys("25");
            }

            if (nuevoTipologia.equals("Duplicado de cronograma") || nuevoTipologia.equals("Informacion de movimientos") ||
                    nuevoTipologia.equals("Liquidación de deuda") || nuevoTipologia.equals("Cambio de forma de Envío de correspondencia") ||
                    nuevoTipologia.equals("Desacuerdo de Condiciones/Tasas/Tarifas") || nuevoTipologia.equals("Mejora de Condiciones CHIP- Ret")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
            }

            if (nuevoTipologia.equals("Devolución de documentos")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Seleccionar Tipo de documento
                Serenity.getDriver().findElement(By.id("documentType")).click();
                Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='documentType']//li)[1]")).click();
            }

            if (nuevoTipologia.equals("Duplicado de documento")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Seleccionar Tipo de documento
                Serenity.getDriver().findElement(By.id("documentType")).click();
                Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='documentType']//li)[1]")).click();
            }

            if (nuevoTipologia.equals("Pago Anticipado y Adelanto de Cuotas") || nuevoTipologia.equals("Transacción mal o no procesada") || nuevoTipologia.equals("Cobros Indebidos") ||
                    nuevoTipologia.equals("Exoneración de Cobros") || nuevoTipologia.equals("Consumo/ Retiro No Efectuado")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("125");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                    }
                }

                if (nuevoTipologia.equals("Transacción mal o no procesada") || nuevoTipologia.equals("Cobros Indebidos") ||
                        nuevoTipologia.equals("Consumo/ Retiro No Efectuado")) {
                    //TODO: Seleccionar - Canal utilizado/reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id("Whatsapp")).click();
                }

                if (nuevoTipologia.equals("Exoneración de Cobros")) {
                    //TODO: Seleccionar - Aplica Autonomía - Si
                    Serenity.getDriver().findElement(By.xpath("//label[@for='applyAutonomy-1']")).click();
                }

            }

            if (nuevoTipologia.equals("No recepción de documento")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Seleccionar Tipo de documento
                Serenity.getDriver().findElement(By.id("paymentDocumentType")).click();
                Serenity.getDriver().findElement(By.id("Contrato")).click();
            }

            if (nuevoTipologia.equals("Emisión de pre conformidad AFP 25%")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Ingresa - Cantidad de Vouchers
                Serenity.getDriver().findElement(By.id("numberVouchers")).clear();
                Serenity.getDriver().findElement(By.id("numberVouchers")).sendKeys("15");
                //TODO: Ingresa - Fecha de Abono
                Serenity.getDriver().findElement(By.id("paymentDate")).clear();
                Serenity.getDriver().findElement(By.id("paymentDate")).sendKeys("12/12/2022");
                //TODO: Ingresa - Monto
                Serenity.getDriver().findElement(By.id("amount")).clear();
                Serenity.getDriver().findElement(By.id("amount")).sendKeys("1000");
                //TODO: Ingresa - Moneda
                Serenity.getDriver().findElement(By.id("currency")).click();
                Serenity.getDriver().findElement(By.id("PEN")).click();
            }

            if (nuevoTipologia.equals("Cuota flexible")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Ingresa fecha de cuota aplazar
                Serenity.getDriver().findElement(By.id("datePostpone")).clear();
                Serenity.getDriver().findElement(By.id("datePostpone")).sendKeys("12/12/2022");
            }

            if (nuevoTipologia.equals("Endoso Seguros de Desgravamen")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Ingresa - Monto de endoso
                Serenity.getDriver().findElement(By.id("amount")).sendKeys("100");
                //TODO: Ingresa - N° de póliza
                Serenity.getDriver().findElement(By.id("policyNumber")).sendKeys("123456");
                //TODO: Ingresa - Cía. de seguros
                Serenity.getDriver().findElement(By.id("insuranceCia")).sendKeys("123123");
                //TODO: Seleccionar - Moneda crédito
                Serenity.getDriver().findElement(By.id("creditCurrency")).click();
                Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='creditCurrency']//li)[1]")).click();
                //TODO: Seleccione - Moneda póliza
                Serenity.getDriver().findElement(By.id("policyCurrency")).click();
                Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='policyCurrency']//li)[1]")).click();
            }

            if (nuevoTipologia.equals("Reporte indebido Centrales de Riesgo")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Seleccionar - Canal utlizado / reclamado
                Serenity.getDriver().findElement(By.id("channel")).click();
                Serenity.getDriver().findElement(By.id("Bca Telef")).click();
            }

            if (nuevoTipologia.equals("Levantamiento de garantía")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();

                //TODO: Ingresa - N° de partida
                Serenity.getDriver().findElement(By.id("itemNumber")).clear();
                Serenity.getDriver().findElement(By.id("itemNumber")).sendKeys("COD255");
                //TODO: Seleccionar - Tipo de inmueble
                Serenity.getDriver().findElement(By.id("propertyType")).click();
                Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='propertyType']//li)[1]")).click();
                //TODO: Ingresa - Ubicación de tienda
                Serenity.getDriver().findElement(By.id("locationStore")).clear();
                Serenity.getDriver().findElement(By.id("locationStore")).sendKeys("MIRAFLORES NRO 350");
                //TODO: Seleccionar - Tipo de documento cónyuge/coodeudor1
                Serenity.getDriver().findElement(By.id("documentType")).click();
                Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='documentType']//li)[1]")).click();
                //TODO: Ingresa - N° de documento cónyuge/coodeudor1
                Serenity.getDriver().findElement(By.id("documentNumber")).clear();
                Serenity.getDriver().findElement(By.id("documentNumber")).sendKeys("18810011");
                //TODO: Seleccionar - Tipo de documento cónyuge/coodeudor2
                Serenity.getDriver().findElement(By.id("documentTypeOther")).click();
                Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='documentTypeOther']//li)[1]")).click();
                //TODO: Ingresa - N° de documento cónyuge/coodeudor2
                Serenity.getDriver().findElement(By.id("documentNumberOther")).clear();
                Serenity.getDriver().findElement(By.id("documentNumberOther")).sendKeys("18820012");
            }


        }

        if (newProducto.equals("ADELANTO SUELDO")) {

            if (nuevoTipologia.equals("Anulación de crédito")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("125");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                    }
                }
            }

            if (nuevoTipologia.equals("Informacion de movimientos")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Ingrese N° Tarjeta
                Serenity.getDriver().findElement(By.id("cardNumber")).clear();
                Serenity.getDriver().findElement(By.id("cardNumber")).sendKeys("1234567891011");
                Serenity.getDriver().findElement(By.id("Descripcion_")).clear();
                Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("Privado");
            }

            if (nuevoTipologia.equals("Duplicado de documento")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("Reason_0")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Tipo de documento
                Serenity.getDriver().findElement(By.id("documentType")).click();
                Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='documentType']//li)[1]")).click();
                //TODO: Ingrese N° Tarjeta
                Serenity.getDriver().findElement(By.id("cardNumber")).clear();
                Serenity.getDriver().findElement(By.id("cardNumber")).sendKeys("1234567891011");
                Serenity.getDriver().findElement(By.id("accountNumber")).clear();
                Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("1007003469143");
            }

            //TODO: *********************************************** TIPO DE TRAMITE RECLAMO ***********************************************
            if (nuevoTipologia.equals("Cobros Indebidos")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());
                try {
                    WebElement reason0 = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(10)).until(ExpectedConditions.elementToBeClickable(By.id("reasonClaim")));
                    Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                    Serenity.getDriver().findElement(By.xpath("(//*[@id='reasonClaim']/div[2]/ul/li)[1]")).click();
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                    }
                }

                //TODO: Seleccionar - Canal utlizado / reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Bca Telef")).click();
            }

            if (nuevoTipologia.equals("Consumo/ Retiro no efectuado")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());
                String valueCanal = Serenity.sessionVariableCalled("canal").toString();
                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='EstablecimientoTransaccion'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='EstablecimientoTransaccion'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                    }
                }
                //TODO: Seleccionar - ¿Qué pasó con la tarjeta?
                Serenity.getDriver().findElement(By.id("cardFraudSituation")).click();
                Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='cardFraudSituation']//li)[1]")).click();
                //TODO: Ingresa - N° de tarjeta
                Serenity.getDriver().findElement(By.id("cardNumber")).clear();
                Serenity.getDriver().findElement(By.id("cardNumber")).sendKeys("123123123123123");
                //TODO: Seleccionar - Canal utlizado / reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id(valueCanal)).click();
            }

        }

        if (newProducto.equals("LINEA CONVENIO")) {

            if (nuevoTipologia.equals("Baja de Tasa")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Seleccionar - Tasa Solicitada
                Serenity.getDriver().findElement(By.id("rate")).clear();
                Serenity.getDriver().findElement(By.id("rate")).sendKeys("25");
            }

            if (nuevoTipologia.equals("Modificación de condiciones contractuales")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();

            }

            if (nuevoTipologia.equals("Anulación de crédito") || nuevoTipologia.equals("Duplicado de voucher")
                    || nuevoTipologia.equals("Pago Anticipado y Adelanto de Cuotas") || nuevoTipologia.equals("Informacion de movimientos")
                    || nuevoTipologia.equals("Transacción mal o no procesada") || nuevoTipologia.equals("Devolución cuotas en tránsito")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("125");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                    }
                }
            }

            if (nuevoTipologia.equals("Duplicado de documento")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Seleccionar Tipo de documento
                Serenity.getDriver().findElement(By.id("documentType")).click();
                Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='documentType']//li)[1]")).click();
            }

            if (nuevoTipologia.equals("Endoso Seguros de Desgravamen")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Ingresa Monto de endoso
                Serenity.getDriver().findElement(By.id("amount")).sendKeys("1000");
                //TODO: Ingresa N° Poliza
                Serenity.getDriver().findElement(By.id("policyNumber")).sendKeys("123456");
                //TODO: Ingresa Cía de Seguros
                Serenity.getDriver().findElement(By.id("insuranceCia")).sendKeys("123123");
                //TODO: Seleccione Moneda Credito
                Serenity.getDriver().findElement(By.id("creditCurrency")).click();
                Serenity.getDriver().findElement(By.xpath("//*[@id='creditCurrency']/div[2]/ul/li[1]")).click();
                //TODO: Seleccione Moneda Poliza
                Serenity.getDriver().findElement(By.id("policyCurrency")).click();
                Serenity.getDriver().findElement(By.xpath("//*[@id='policyCurrency']/div[2]/ul/li[1]")).click();
            }

            if (nuevoTipologia.equals("Desacuerdo de Condiciones/Tasas/Tarifas") || nuevoTipologia.equals("No recepción de documento")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Seleccionar - Canal utlizado / reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Bca Telef")).click();
            }

            if (nuevoTipologia.equals("Consumo/ Retiro No Efectuado")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());
                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("125");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                    }
                }
                //TODO: Seleccionar - ¿Qué pasó con la tarjeta?
                Serenity.getDriver().findElement(By.id("cardFraudSituation")).click();
                Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='cardFraudSituation']//li)[1]")).click();
                //TODO: Ingresar Numero Tarjeta
                Serenity.getDriver().findElement(By.id("cardNumber")).sendKeys("123123123123123");
                //TODO: Seleccionar - Canal utlizado / reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Bca Telef")).click();
            }

            if (nuevoTipologia.equals("Reporte indebido Centrales de Riesgo")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Seleccionar - Canal utlizado / reclamado
                Serenity.getDriver().findElement(By.id("channel")).click();
                Serenity.getDriver().findElement(By.id("Bca Telef")).click();
            }

        }

        if (newProducto.equals("SEGUROS RELACIONADOS")) {

            if (nuevoTipologia.equals("Endoso Seguros del Bien")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Ingresa - N° de Crédito
                Serenity.getDriver().findElement(By.id("creditNumber")).sendKeys("12312345");
                //TODO: Ingresa - N° de póliza
                Serenity.getDriver().findElement(By.id("policyNumber")).sendKeys("123456");
                //TODO: Ingresa - Monto de endoso
                Serenity.getDriver().findElement(By.id("amount")).sendKeys("100");
                //TODO: Ingresa - Cía. de seguros
                Serenity.getDriver().findElement(By.id("insuranceName")).sendKeys("123123");
                //TODO: Seleccionar - Moneda crédito
                Serenity.getDriver().findElement(By.id("creditCurrency")).click();
                Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='creditCurrency']//li)[1]")).click();
                //TODO: Seleccione - Moneda Poliza
                Serenity.getDriver().findElement(By.id("policyCurrency")).click();
                Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='policyCurrency']//li)[1]")).click();
            }

            if (nuevoTipologia.equals("Duplicado de documento")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Seleccionar - Tipo filtro producto
                Serenity.getDriver().findElement(By.xpath("(//div[@id='productType'])[2]")).click();
                Serenity.getDriver().findElement(By.id("Credito")).click();
                //TODO: Ingresa - N° de tipo de producto
                Serenity.getDriver().findElement(By.id("productTypeNumber")).clear();
                Serenity.getDriver().findElement(By.id("productTypeNumber")).sendKeys("4213555322311541");
            }

            if (nuevoTipologia.equals("Informacion de movimientos")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());

                    }
                }

                //TODO: Ingresa - N° de tarjeta
                Serenity.getDriver().findElement(By.id("cardNumber")).clear();
                Serenity.getDriver().findElement(By.id("cardNumber")).sendKeys("4213555322311541");
                //TODO: Ingresa - N° de crédito
                Serenity.getDriver().findElement(By.id("creditNumber")).clear();
                Serenity.getDriver().findElement(By.id("creditNumber")).sendKeys("12312345678969");
                //TODO: Seleccionar - Canal utlizado / reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Bca Telef")).click();
            }

            if (nuevoTipologia.equals("Cobros Indebidos")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());

                    }
                }

                //TODO: Seleccionar - Tipo de producto
                Serenity.getDriver().findElement(By.xpath("(//div[@id='productType'])[2]")).click();
                Serenity.getDriver().findElement(By.id("Crédito")).click();
                //TODO: Ingresa - N° de tipo de producto
                Serenity.getDriver().findElement(By.id("productTypeNumber")).clear();
                Serenity.getDriver().findElement(By.id("productTypeNumber")).sendKeys("12345678");
                //TODO: Seleccionar - Motivo de Cobro
                Serenity.getDriver().findElement(By.id("reasonPayment")).click();
                Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='reasonPayment']//li)[1]")).click();
                //TODO: Seleccionar - Canal utlizado / reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Bca Telef")).click();
            }

        }

        if (newProducto.equals("CBME")) {

            if (nuevoTipologia.equals("Copia de boleta de depósito Lima")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());
                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                    }
                }
                //TODO: Ingresa - Nro. de cuenta
                Serenity.getDriver().findElement(By.id("accountNumber")).clear();
                Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("1007003469143");
            }
        }

        if (newProducto.equals("CARTA FIANZA/CERTIFICADO")) {
            if (nuevoTipologia.equals("Duplicado de documento")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Seleccionar - Tipo de documento
                Serenity.getDriver().findElement(By.id("documentType")).click();
                Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='documentType']//li)[1]")).click();
                //TODO: Ingresa - N° de cuenta
                Serenity.getDriver().findElement(By.id("accountNumber")).clear();
                Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("1007003469143");
            }

        }

        if (newProducto.equals("RAPPIBANK - CUENTAS")) {

            //TODO: *********************************************** TIPO DE TRAMITE RECLAMO ***********************************************
            if (nuevoTipologia.equals("Cobros Indebidos") || nuevoTipologia.equals("Transacción mal o no procesada") ||
                    nuevoTipologia.equals("Consumo/ Retiro No Efectuado") || nuevoTipologia.equals("Cuenta/Cancelación no reconocida") ||
                    nuevoTipologia.equals("Liberación de retenciones") || nuevoTipologia.equals("Operación Denegada") ||
                    nuevoTipologia.equals("Revisión de depósitos a terceros")) {

                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());
                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_" + i)).click();
                        Serenity.getDriver().findElement(By.xpath("(//*[@id='Reason_" + i + "']/div[2]/ul/li)[last()]")).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                    }
                }

                if (nuevoTipologia.equals("Cobros Indebidos") || nuevoTipologia.equals("Transacción mal o no procesada")) {
                    //TODO: Ingresa - N° de cuenta
                    Serenity.getDriver().findElement(By.id("accountNumber")).clear();
                    Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("1007003469143");
                    //TODO: Seleccionar - Canal utlizado / reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id("Whatsapp")).click();
                }

                if (nuevoTipologia.equals("Consumo/ Retiro No Efectuado") || nuevoTipologia.equals("Cuenta/Cancelación no reconocida") ||
                        nuevoTipologia.equals("Liberación de retenciones")) {
                    //TODO: Ingresa - N° de cuenta (Opcional)
                    Serenity.getDriver().findElement(By.id("accountNumber")).clear();
                    Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("1007003469143");
                    //TODO: Ingresa - N° de tarjeta de crédito (Opcional)
                    Serenity.getDriver().findElement(By.id("cardNumber")).clear();
                    Serenity.getDriver().findElement(By.id("cardNumber")).sendKeys("4213555322311541");
                    //TODO: Ingresa - N° de Retiros/Cobros/Consumos(Opcional)
                    Serenity.getDriver().findElement(By.id("numCollConsWithdraw")).clear();
                    Serenity.getDriver().findElement(By.id("numCollConsWithdraw")).sendKeys("306");

                    if (nuevoTipologia.equals("Consumo/ Retiro No Efectuado") || nuevoTipologia.equals("Cuenta/Cancelación no reconocida")) {
                        //TODO: Seleccionar - Canal utilizado/reclamado
                        Serenity.getDriver().findElement(By.id("channelUsed")).click();
                        Serenity.getDriver().findElement(By.id("Whatsapp")).click();
                    }

                    if (nuevoTipologia.equals("Liberación de retenciones")) {
                        //TODO: Seleccionar - Canal utilizado/reclamado
                        Serenity.getDriver().findElement(By.id("usedChannel")).click();
                        Serenity.getDriver().findElement(By.id("Whatsapp")).click();
                    }

                }

                if (nuevoTipologia.equals("Operación Denegada") || nuevoTipologia.equals("Revisión de depósitos a terceros")) {
                    //TODO: Ingresa - N° de cuenta (Opcional)
                    Serenity.getDriver().findElement(By.id("accountNumber")).clear();
                    Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("1007003469143");
                    //TODO: Ingresa - N° de tarjeta de crédito (Opcional)
                    Serenity.getDriver().findElement(By.id("cardNumber")).clear();
                    Serenity.getDriver().findElement(By.id("cardNumber")).sendKeys("4213555322311541");
                    //TODO: Ingresa - N° de Retiros/Cobros/Consumos(Opcional)
                    Serenity.getDriver().findElement(By.id("withdrawalNumbers")).clear();
                    Serenity.getDriver().findElement(By.id("withdrawalNumbers")).sendKeys("306");
                    //TODO: Seleccionar - Canal utilizado/reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id("Whatsapp")).click();
                }

            }

            if (nuevoTipologia.equals("Desacuerdo con el servicio")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Ingresa - N° de cuenta (Opcional)
                Serenity.getDriver().findElement(By.id("accountNumber")).clear();
                Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("1007003469143");
                //TODO: Ingresa - N° de tarjeta de crédito (Opcional)
                Serenity.getDriver().findElement(By.id("cardNumber")).clear();
                Serenity.getDriver().findElement(By.id("cardNumber")).sendKeys("4213555322311541");
                //TODO: Ingresa - Fecha de operación (Opcional)
                Serenity.getDriver().findElement(By.id("chargeDate")).clear();
                Serenity.getDriver().findElement(By.id("chargeDate")).sendKeys(getValueFecha());
                //TODO: Ingresa - Cantidad (Opcional)
                Serenity.getDriver().findElement(By.id("amount")).clear();
                Serenity.getDriver().findElement(By.id("amount")).sendKeys("350");
                //TODO: Seleccionar - Canal utilizado/reclamado
                Serenity.getDriver().findElement(By.id("channelUsed")).click();
                Serenity.getDriver().findElement(By.id("Whatsapp")).click();
            }

            if (nuevoTipologia.equals("Desacuerdo de Condiciones/Tasas/Tarifas") || nuevoTipologia.equals("Insatisfacción gestión de entrega de tarjeta")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Ingresa - N° de cuenta (Opcional)
                Serenity.getDriver().findElement(By.id("accountNumber")).clear();
                Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("1007003469143");
                //TODO: Ingresa - N° de tarjeta de crédito (Opcional)
                Serenity.getDriver().findElement(By.id("cardNumber")).clear();
                Serenity.getDriver().findElement(By.id("cardNumber")).sendKeys("4213555322311541");
                //TODO: Ingresa - Fecha de operación (Opcional)
                Serenity.getDriver().findElement(By.id("operationDate")).clear();
                Serenity.getDriver().findElement(By.id("operationDate")).sendKeys(getValueFecha());
                //TODO: Ingresa - Cantidad (Opcional)
                Serenity.getDriver().findElement(By.id("quantity")).clear();
                Serenity.getDriver().findElement(By.id("quantity")).sendKeys("350");
                //TODO: Seleccionar - Canal utilizado/reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Whatsapp")).click();
            }

            if (nuevoTipologia.equals("Sistema de Recompensas")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Ingresa - N° de cuenta (Opcional)
                Serenity.getDriver().findElement(By.id("accountNumber")).clear();
                Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("1007003469143");
                //TODO: Ingresa - N° de tarjeta (Opcional)
                Serenity.getDriver().findElement(By.id("cardNumber")).clear();
                Serenity.getDriver().findElement(By.id("cardNumber")).sendKeys("4213555322311541");
                //TODO: Ingresa - Fecha de operación (Opcional)
                Serenity.getDriver().findElement(By.id("date")).clear();
                Serenity.getDriver().findElement(By.id("date")).sendKeys(getValueFecha());
                //TODO: Ingresa - Cantidad (Opcional)
                Serenity.getDriver().findElement(By.id("amount")).clear();
                Serenity.getDriver().findElement(By.id("amount")).sendKeys("350");
                //TODO: Seleccionar - Canal utilizado/reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Whatsapp")).click();
            }

        }

        if (newProducto.equals("RAPPIBANK - TARJETA DE CRÉDITO")) {

            if (nuevoTipologia.equals("ATM de IB no entregó dinero")) {

                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());
                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Importe Solicitado
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ImporteSolicitado'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ImporteSolicitado'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Importe Reclamado
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("350");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                    }
                }

                //TODO: Ingresa - N° de Tarjeta
                Serenity.getDriver().findElement(By.id("cellPhoneNumber")).sendKeys("4213555322311541");
                //TODO: Ingresa - N° de cuenta
                Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("1007003469143");
                //TODO: Ingresa - Código Cajero
                Serenity.getDriver().findElement(By.id("codeATM")).sendKeys("COD124");
                //TODO: Seleccionar - Canal utilizado / reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Bca Telef")).click();
            }

            if (nuevoTipologia.equals("Correo/Teléfono Indebido de campañas")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Ingresa - Nombre del Titular
                Serenity.getDriver().findElement(By.id("Descripcion_")).clear();
                Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("JOSE LUIS");
                //TODO: Seleccionar - Canal utilizado / reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Bca Telef")).click();
            }

            if (nuevoTipologia.equals("Desacuerdo de Condiciones/Tasas/Tarifas")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Ingresa - N° de tarjeta
                Serenity.getDriver().findElement(By.id("cardNumber")).clear();
                Serenity.getDriver().findElement(By.id("cardNumber")).sendKeys("4213555322311541");
                //TODO: Ingresa - Monto
                Serenity.getDriver().findElement(By.id("amount")).clear();
                Serenity.getDriver().findElement(By.id("amount")).sendKeys("3500");
                //TODO: Seleccionar - Canal utilizado / reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Bca Telef")).click();
            }

            if (nuevoTipologia.equals("Dirección/Telf. indebidos de cobranza")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Ingresa - N° de crédito
                Serenity.getDriver().findElement(By.id("creditNumber")).clear();
                Serenity.getDriver().findElement(By.id("creditNumber")).sendKeys("13035489");
                //TODO: Ingresa - N° de tarjeta
                Serenity.getDriver().findElement(By.id("cardNumber")).clear();
                Serenity.getDriver().findElement(By.id("cardNumber")).sendKeys("4213555322311541");
                //TODO: Ingresa - Nombre del deudor
                Serenity.getDriver().findElement(By.id("debtorName")).clear();
                Serenity.getDriver().findElement(By.id("debtorName")).sendKeys("JUAN CARLOS");
                //TODO: Seleccionar - Canal utilizado/reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Bca Telef")).click();
            }

            if (nuevoTipologia.equals("Insatisfacción gestión de entrega de tarjeta") || nuevoTipologia.equals("Reporte indebido Centrales de Riesgo")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Ingresa - N° de tarjeta
                Serenity.getDriver().findElement(By.id("cardNumber")).clear();
                Serenity.getDriver().findElement(By.id("cardNumber")).sendKeys("4213555322311541");
                //TODO: Seleccionar - Canal utilizado/reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Bca Telef")).click();
            }

            if (nuevoTipologia.equals("Transacción mal o no procesada") || nuevoTipologia.equals("Cobros Indebidos") ||
                    nuevoTipologia.equals("Consumo/ Retiro No Efectuado") || nuevoTipologia.equals("Liberación de retenciones") ||
                    nuevoTipologia.equals("Operación Denegada")) {

                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());
                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_" + i)).click();
                        Serenity.getDriver().findElement(By.xpath("(//*[@id='Reason_" + i + "']/div[2]/ul/li)[last()]")).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        ((JavascriptExecutor) Serenity.getDriver()).executeScript("arguments[0].scrollIntoView(true)", Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")));
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                    }
                }

                if (nuevoTipologia.equals("Cobros Indebidos") || nuevoTipologia.equals("Consumo/ Retiro No Efectuado") ||
                        nuevoTipologia.equals("Liberación de retenciones") || nuevoTipologia.equals("Operación Denegada")) {
                    //TODO: Ingresa - N° de cuenta (Opcional)
                    Serenity.getDriver().findElement(By.id("accountNumber")).clear();
                    Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("1007003469143");
                    //TODO: Ingresa - N° de tarjeta de crédito (Opcional)
                    Serenity.getDriver().findElement(By.id("cardNumber")).clear();
                    Serenity.getDriver().findElement(By.id("cardNumber")).sendKeys("4213555322311541");

                    if (nuevoTipologia.equals("Cobros Indebidos") || nuevoTipologia.equals("Consumo/ Retiro No Efectuado") ||
                            nuevoTipologia.equals("Liberación de retenciones")) {
                        //TODO: Ingresa - N° de Retiros/Cobros/Consumos(Opcional)
                        Serenity.getDriver().findElement(By.id("numCollConsWithdraw")).clear();
                        Serenity.getDriver().findElement(By.id("numCollConsWithdraw")).sendKeys("306");

                    } else if (nuevoTipologia.equals("Operación Denegada")) {
                        //TODO: Ingresa - N° de Retiros/Cobros/Consumos(Opcional)
                        Serenity.getDriver().findElement(By.id("withdrawalNumbers")).clear();
                        Serenity.getDriver().findElement(By.id("withdrawalNumbers")).sendKeys("306");
                    }

                    if (nuevoTipologia.equals("Cobros Indebidos") || nuevoTipologia.equals("Consumo/ Retiro No Efectuado")) {
                        //TODO: Seleccionar - Canal utilizado/reclamado
                        Serenity.getDriver().findElement(By.id("channelUsed")).click();
                        Serenity.getDriver().findElement(By.id("Whatsapp")).click();
                    } else if (nuevoTipologia.equals("Liberación de retenciones") || nuevoTipologia.equals("Operación Denegada")) {
                        //TODO: Seleccionar - Canal utilizado/reclamado
                        Serenity.getDriver().findElement(By.id("usedChannel")).click();
                        Serenity.getDriver().findElement(By.id("Whatsapp")).click();
                    }

                }

                if (nuevoTipologia.equals("Transacción mal o no procesada")) {
                    //TODO: Ingresa - N° de Tarjeta
                    Serenity.getDriver().findElement(By.id("accountNumber")).clear();
                    Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("4213555322311541");
                    //TODO: Seleccionar - Canal utilizado / reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id("Bca Telef")).click();
                }

            }

            if (nuevoTipologia.equals("No recepción de documento")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Ingresa - N° de cuenta (Opcional)
                Serenity.getDriver().findElement(By.id("accountNumber")).clear();
                Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("1007003469143");
                //TODO: Ingresa - N° de tarjeta de crédito (Opcional)
                Serenity.getDriver().findElement(By.id("cardNumber")).clear();
                Serenity.getDriver().findElement(By.id("cardNumber")).sendKeys("4213555322311541");
                //TODO: Ingresa - Fecha de Operación (Opcional)
                Serenity.getDriver().findElement(By.id("operationDate")).clear();
                Serenity.getDriver().findElement(By.id("operationDate")).sendKeys(getValueFecha());
                //TODO: Ingresa - Cantidad (Opcional)
                Serenity.getDriver().findElement(By.id("quantity")).clear();
                Serenity.getDriver().findElement(By.id("quantity")).sendKeys("350");
                //TODO: Seleccionar - Canal utilizado/reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Whatsapp")).click();
            }

            if (nuevoTipologia.equals("Sistema de Recompensas")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Ingresa - N° de cuenta (Opcional)
                Serenity.getDriver().findElement(By.id("accountNumber")).clear();
                Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("1007003469143");
                //TODO: Ingresa - N° de tarjeta de crédito (Opcional)
                Serenity.getDriver().findElement(By.id("cardNumber")).clear();
                Serenity.getDriver().findElement(By.id("cardNumber")).sendKeys("4213555322311541");
                //TODO: Ingresa - Fecha de Operación (Opcional)
                Serenity.getDriver().findElement(By.id("date")).clear();
                Serenity.getDriver().findElement(By.id("date")).sendKeys(getValueFecha());
                //TODO: Ingresa - Cantidad (Opcional)
                Serenity.getDriver().findElement(By.id("amount")).clear();
                Serenity.getDriver().findElement(By.id("amount")).sendKeys("350");
                //TODO: Seleccionar - Canal utilizado/reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Whatsapp")).click();
            }


        }

        // TODO: OPERACIONES
        if (newProducto.equals("COBRANZAS")) {

            //TODO: *********************************************** TIPO DE TRAMITE RECLAMO ***********************************************
            if (nuevoTipologia.equals("Cobros Indebidos")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());
                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                    }
                }

                //TODO: Ingresa - Tipo de producto
                Serenity.getDriver().findElement(By.xpath("(//*[@id='productType'])[last()]")).click();
                Serenity.getDriver().findElement(By.id("Crédito")).click();

                if (ambiente.equals("UAT")) {
                    //TODO: Ingresa - N° de producto
                    Serenity.getDriver().findElement(By.id("productTypeNumber")).clear();
                    Serenity.getDriver().findElement(By.id("productTypeNumber")).sendKeys("12321321321321");
                } else {
                    //TODO: Ingresa - N° de producto
                    Serenity.getDriver().findElement(By.id("Descripcion_")).clear();
                    Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("12321321321321");
                }

                //TODO: Seleccionar - Canal utlizado / reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Bca Telef")).click();

            }

            if (nuevoTipologia.equals("Constancia de no adeudo")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                if (ambiente.equals("UAT")) {
                    //TODO: Ingresa - N° de crédito
                    Serenity.getDriver().findElement(By.id("creditNumber")).clear();
                    Serenity.getDriver().findElement(By.id("creditNumber")).sendKeys("87654321");
                } else {
                    //TODO: Ingresa - N° de crédito
                    Serenity.getDriver().findElement(By.id("Descripcion_")).clear();
                    Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("87654321");
                }
            }

            if (nuevoTipologia.equals("Dirección/Telf. indebidos de cobranza")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();

                //TODO: Ingresa - Tipo de producto
                Serenity.getDriver().findElement(By.xpath("(//*[@id='productType'])[last()]")).click();
                Serenity.getDriver().findElement(By.id("Credito")).click();
                //TODO: Ingresa - N° de tipo de producto
                Serenity.getDriver().findElement(By.id("productTypeNumber")).clear();
                Serenity.getDriver().findElement(By.id("productTypeNumber")).sendKeys("12321321321321");
                //TODO: Ingresa - Nombre del Deudor
                Serenity.getDriver().findElement(By.id("debtorName")).clear();
                Serenity.getDriver().findElement(By.id("debtorName")).sendKeys("Gustavo Campos");
                //TODO: Seleccionar - Canal utlizado / reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Bca Telef")).click();
            }

            if (nuevoTipologia.equals("Reporte indebido Centrales de Riesgo")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Ingresa - Tipo de producto
                Serenity.getDriver().findElement(By.xpath("(//*[@id='productType'])[last()]")).click();
                Serenity.getDriver().findElement(By.id("Crédito")).click();
                //TODO: Ingresa - N° de tipo de producto
                Serenity.getDriver().findElement(By.id("numberProductType")).clear();
                Serenity.getDriver().findElement(By.id("numberProductType")).sendKeys("12321321321321");
                //TODO: Seleccionar - Canal utlizado / reclamado
                Serenity.getDriver().findElement(By.id("channel")).click();
                Serenity.getDriver().findElement(By.id("Bca Telef")).click();
            }

        }


        if (newProducto.equals("REMESAS")) {

            //TODO: *********************************************** TIPO DE TRAMITE PEDIDO ***********************************************
            if (nuevoTipologia.equals("Constancia por transferencias recibidas")) {
                //TODO: Seleccionar - Tipo de Remesa
                Serenity.getDriver().findElement(By.id("remittanceCode")).click();
                Serenity.getDriver().findElement(By.id("Entregada")).clear();
                //TODO: Seleccionar - Periodo de Inicio
                Serenity.getDriver().findElement(By.id("startPeriod")).clear();
                Serenity.getDriver().findElement(By.id("startPeriod")).sendKeys("12/10/2022");
                //TODO: Seleccionar - Periodo de Fin
                Serenity.getDriver().findElement(By.id("endPeriod")).clear();
                Serenity.getDriver().findElement(By.id("endPeriod")).sendKeys("12/12/2022");
            }

            //TODO: *********************************************** TIPO DE TRAMITE RECLAMO ***********************************************
            if (nuevoTipologia.equals("Cobros Indebidos")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {

                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("125");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                    }
                }
                //TODO: Seleccionar - Tipo de Remesa
                Serenity.getDriver().findElement(By.id("remittanceType")).click();
                Serenity.getDriver().findElement(By.id("Entregada")).click();
                //TODO: Seleccionar - Código de Remesa
                Serenity.getDriver().findElement(By.id("remittanceCode")).clear();
                Serenity.getDriver().findElement(By.id("remittanceCode")).sendKeys("1234567");
            }

            if (nuevoTipologia.equals("Transacción mal o no procesada")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                    }
                }

                //TODO: Ingresa - Tipo de remesa
                Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).clear();
                Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).sendKeys("Recibida");
                //TODO: Seleccionar - Canal utlizado / reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Bca Telef")).click();
                //TODO: Ingresa - Código de remesa
                Serenity.getDriver().findElement(By.id("remittanceCode")).clear();
                Serenity.getDriver().findElement(By.id("remittanceCode")).sendKeys("20569105");
            }

            if (nuevoTipologia.equals("ATM de IB no entregó dinero")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Importe Solicitado
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ImporteSolicitado'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ImporteSolicitado'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Importe Reclamado
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='EstablecimientoTransaccion'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='EstablecimientoTransaccion'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                    }
                }

                //TODO: Ingresa - Nro de cuenta
                Serenity.getDriver().findElement(By.id("accountNumber")).clear();
                Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("1007003469143");
                //TODO: Ingresa - Ubicación del Cajero
                Serenity.getDriver().findElement(By.id("locationCashier")).clear();
                Serenity.getDriver().findElement(By.id("locationCashier")).sendKeys("Bloom Tower");
                //TODO: Ingresa - Código de remesa
                Serenity.getDriver().findElement(By.id("remittanceCode")).clear();
                Serenity.getDriver().findElement(By.id("remittanceCode")).sendKeys("1234567");
                //TODO: Seleccionar - Canal utlizado / reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Bca Telef")).click();
            }

            if (nuevoTipologia.equals("No recepción de transferencia IN/OUT")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {

                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("125");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                    }
                }

                //TODO: Ingresa - Nombre del Banco Emisor/Destino
                Serenity.getDriver().findElement(By.id("Descripcion_")).clear();
                Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("INTERBANK");
                //TODO: Ingresa - Número de cuenta
                Serenity.getDriver().findElement(By.id("accountNumber")).clear();
                Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("123123123123124");
            }

            if (nuevoTipologia.equals("Consumo/ Retiro No Efectuado")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("125");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/ Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                    }
                }

                //TODO: Ingresa - N° de cuenta
                Serenity.getDriver().findElement(By.id("accountNumber")).clear();
                Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("123123123123124");
                //TODO: Seleccionar - Canal utlizado / reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Entrega TC")).click();
            }

        }

        if (newProducto.equals("PODER")) {
            // TODO: NUEVO - EXISTE TIPO DE TRAMITE - SOLICITUD - NO SE RETIPIFICA
        }

        if (newProducto.equals("APP INTERBANK")) {
            //TODO: *********************************************** TIPO DE TRAMITE PEDIDO ***********************************************
            if (nuevoTipologia.equals("Constancia de pago realizado")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());
                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='EstablecimientoTransaccion'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='EstablecimientoTransaccion'])[" + (j + 1) + "]")).sendKeys("Tambo");
                        //TODO: Ingresa - Cod. Cliente/Suministro/Nro. Contrato
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ClienteSuministroContrato'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ClienteSuministroContrato'])[" + (j + 1) + "]")).sendKeys("105266");


                    }
                }

                if (ambiente.equals("UAT")) {
                    //TODO: Ingresa - Nro. de tarjeta
                    Serenity.getDriver().findElement(By.id("cardNumber")).clear();
                    Serenity.getDriver().findElement(By.id("cardNumber")).sendKeys("4213555322311541");
                    //TODO: Ingresa - Nro. de cuenta
                    Serenity.getDriver().findElement(By.id("accountNumber")).clear();
                    Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("1007003469143");
                } else {
                    //TODO: Ingresa - Nro. de tarjeta
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).clear();
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).sendKeys("4213555322311541");
                    //TODO: Ingresa - Nro. de cuenta
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[last()]")).clear();
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[last()]")).sendKeys("1007003469143");
                }

            }

            //TODO: *********************************************** TIPO DE TRAMITE RECLAMO ***********************************************
            if (nuevoTipologia.equals("Transacción mal o no procesada")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());

                    }
                }

                //TODO: Ingresa - Nro. de tarjeta
                Serenity.getDriver().findElement(By.id("cardNumber")).clear();
                Serenity.getDriver().findElement(By.id("cardNumber")).sendKeys("4213555322311541");
                //TODO: Ingresa - Nro. de cuenta
                Serenity.getDriver().findElement(By.id("accountNumber")).clear();
                Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("1007003469143");

                //TODO: Seleccionar - Canal utlizado / reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Bca Telef")).click();
            }

        }

        if (newProducto.equals("BANCA CELULAR")) {
            // TODO: NUEVO
            //TODO: *********************************************** TIPO DE TRAMITE RECLAMO ***********************************************
            if (nuevoTipologia.equals("Transacción mal o no procesada")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());

                    }
                }

                if (ambiente.equals("UAT")) {
                    //TODO: Ingresa - Celular
                    Serenity.getDriver().findElement(By.id("Descripcion_")).clear();
                    Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("941916927");
                } else {
                    //TODO: Ingresa - Celular
                    Serenity.getDriver().findElement(By.id("accountNumber")).clear();
                    Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("941916927");
                }

                //TODO: Seleccionar - Canal utlizado / reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Bca Telef")).click();
            }

        }

        if (newProducto.equals("DEBITO AUTOMATICO")) {
            //TODO: *********************************************** TIPO DE TRAMITE RECLAMO ***********************************************

            if (nuevoTipologia.equals("Transacción mal o no procesada") || nuevoTipologia.equals("Cobros Indebidos")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {

                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                        //TODO: Ingresa - Servicio/ Empresa
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ServicioEmpresa'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ServicioEmpresa'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                    }
                }

                //TODO: Seleccionar - Forma de pago
                Serenity.getDriver().findElement(By.id("paymentForm")).click();
                Serenity.getDriver().findElement(By.id("Cuenta")).click();

                if (nuevoTipologia.equals("Transacción mal o no procesada")) {
                    //TODO: Seleccionar - N° de forma de pago
                    Serenity.getDriver().findElement(By.xpath("//input[@formcontrolname='paymentNumber']")).clear();
                    Serenity.getDriver().findElement(By.xpath("//input[@formcontrolname='paymentNumber']")).sendKeys("123123123");
                }

                if (nuevoTipologia.equals("Cobros Indebidos")) {
                    //TODO: Seleccionar - N° de forma de pago
                    Serenity.getDriver().findElement(By.id("paymentNumber")).clear();
                    Serenity.getDriver().findElement(By.id("paymentNumber")).sendKeys("12345678");
                }

                //TODO: Seleccionar - Canal utlizado / reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='usedChannel']//li)[1]")).click();
            }


        }

        if (newProducto.equals("PAGO DE SERVICIO POR CANALES")) {
            //TODO: *********************************************** TIPO DE TRAMITE PEDIDO ************************************************
            if (nuevoTipologia.equals("Corrección de pago realizado")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("100");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                        //TODO: Ingresa - Servicio/Empresa
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Servicio'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Servicio'])[" + (j + 1) + "]")).sendKeys("IBK");
                        //TODO: Ingresa - Cod.Cliente/Sumnistro/N° Contrato
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Codigo'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Codigo'])[" + (j + 1) + "]")).sendKeys("123");

                    }
                }
                //TODO: Seleccionar - N° Cuenta
                Serenity.getDriver().findElement(By.id("accountNumber")).clear();
                Serenity.getDriver().findElement(By.xpath("accountNumber")).sendKeys("1234567891011");
            }

            //TODO: *********************************************** TIPO DE TRAMITE RECLAMO ***********************************************
            if (nuevoTipologia.equals("Transacción mal o no procesada") || nuevoTipologia.equals("Cobros Indebidos")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - IMonto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                        //TODO: Ingresa - Servicio/Empresa
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ServicioEmpresa'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ServicioEmpresa'])[" + (j + 1) + "]")).sendKeys("Pharmax S.A.C");

                    }
                }

                //TODO: Seleccionar - Forma de Pago
                Serenity.getDriver().findElement(By.id("paymentForm")).click();
                Serenity.getDriver().findElement(By.id("Cuenta")).click();
                //TODO: Seleccionar - N° Forma de Pago
                Serenity.getDriver().findElement(By.xpath("//input[@formcontrolname='paymentNumber']")).clear();
                Serenity.getDriver().findElement(By.xpath("//input[@formcontrolname='paymentNumber']")).sendKeys("12345678");
                //TODO: Seleccionar - Canal utlizado / reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Bca Telef")).click();
            }

            if (nuevoTipologia.equals("ATM de IB no depositó dinero")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Importe Solicitado
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ImporteSolicitado'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ImporteSolicitado'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Importe Reclamado
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("1500");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Hora de Operación
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Hora'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Hora'])[" + (j + 1) + "]")).sendKeys("12:12");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='EstablecimientoTransaccion'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='EstablecimientoTransaccion'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                        //TODO: Ingresa - Cod.Cliente/Sumnistro/N° Contrato
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ClienteSuministroContrato'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ClienteSuministroContrato'])[" + (j + 1) + "]")).sendKeys("123");

                    }
                }

                //TODO: Seleccionar - Ubicación del Cajero
                Serenity.getDriver().findElement(By.id("locationCashier")).clear();
                Serenity.getDriver().findElement(By.id("locationCashier")).sendKeys("Bloom Tower");
                //TODO: Seleccionar - Canal utlizado / reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Bca Telef")).click();
            }

        }

        if (newProducto.equals("TRANSF. NACIONALES")) {

            if (nuevoTipologia.equals("Transacción mal o no procesada")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("125");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());

                    }
                }

                //TODO: Seleccionar - N° Tarjeta
                Serenity.getDriver().findElement(By.id("cardNumber")).clear();
                Serenity.getDriver().findElement(By.id("cardNumber")).sendKeys("4213555322311541");
                //TODO: Seleccionar - N° Cuenta
                Serenity.getDriver().findElement(By.id("accountNumber")).clear();
                Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("1007003469143");
                //TODO: Seleccionar - Tipo de Transferencia
                Serenity.getDriver().findElement(By.id("typeOfTransfer")).click();
                Serenity.getDriver().findElement(By.id("Salida")).click();
                //TODO: Seleccionar - Canal utlizado / reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Bca Telef")).click();

            }

            if (nuevoTipologia.equals("Cobros Indebidos")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("125");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());

                    }
                }

                //TODO: Seleccionar - Tipo de transferencia
                Serenity.getDriver().findElement(By.id("typeOfTransfer")).click();
                Serenity.getDriver().findElement(By.id("Salida")).click();
                //TODO: Seleccionar - N° de cuenta
                Serenity.getDriver().findElement(By.id("accountNumber")).clear();
                Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("1007003469143");
                //TODO: Seleccionar - Canal utilizado/reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Bca Telef")).click();

            }

        }

        if (newProducto.equals("BANCA POR INTERNET")) {
            if (nuevoTipologia.equals("Transacción mal o no procesada")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());

                    }
                }

                if (ambiente.equals("UAT")) {
                    //TODO: Ingresa - N° de cuenta
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).clear();
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).sendKeys("1231231231231");
                    //TODO: Ingresa - N° de tarjeta
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[last()]")).clear();
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[last()]")).sendKeys("123123123123123");
                } else {
                    //TODO: Ingresa - N° de cuenta
                    Serenity.getDriver().findElement(By.id("accountNumber")).clear();
                    Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("1231231231231");
                    //TODO: Ingresa - N° de tarjeta
                    Serenity.getDriver().findElement(By.id("cardNumber")).clear();
                    Serenity.getDriver().findElement(By.id("cardNumber")).sendKeys("123123123123123");
                }

                //TODO: Seleccionar - Canal utlizado / reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Bca Telef")).click();
            }

        }

        if (newProducto.equals("BILLETERA ELECTRÓNICA")) {
            // TODO: NUEVO
            //TODO: *********************************************** TIPO DE TRAMITE RECLAMO ***********************************************
            if (nuevoTipologia.equals("Transacción mal o no procesada")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());

                    }
                }

                //TODO: Ingresa - N° de cuenta
                Serenity.getDriver().findElement(By.id("accountNumber")).clear();
                Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("1231231231231");
                //TODO: Ingresa - Celular
                Serenity.getDriver().findElement(By.id("cellPhoneNumber")).clear();
                Serenity.getDriver().findElement(By.id("cellPhoneNumber")).sendKeys("941916927");
                //TODO: Seleccionar - Canal utlizado / reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Bca Telef")).click();
            }

            if (nuevoTipologia.equals("Informacion de movimientos") || nuevoTipologia.equals("Consumo/ Retiro No Efectuado")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());

                    }
                }
                //TODO: Ingresa - N° de Cuenta
                Serenity.getDriver().findElement(By.id("accountNumber")).clear();
                Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("1010101010101");

                if (nuevoTipologia.equals("Informacion de movimientos")) {
                    //TODO: Seleccionar - Canal utlizado / reclamado
                    Serenity.getDriver().findElement(By.id("channelUsed")).click();
                    Serenity.getDriver().findElement(By.id("Bca Telef")).click();
                } else {
                    //TODO: Seleccionar - Canal utlizado / reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id("Bca Telef")).click();
                }
            }

        }

        if (newProducto.equals("IB AGENTE")) {
            // TODO: NUEVO
            //TODO: *********************************************** TIPO DE TRAMITE RECLAMO ***********************************************
            if (nuevoTipologia.equals("Transacción mal o no procesada")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                        //TODO: Ingresa - Cod.Cliente/Suministro/N° contrato
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='CodigoSuministroContrato'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='CodigoSuministroContrato'])[" + (j + 1) + "]")).sendKeys("COD124");
                    }
                }

                //TODO: Ingresa - N° de cuenta
                Serenity.getDriver().findElement(By.id("accountNumber")).clear();
                Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("1007003469143");
                //TODO: Ingresa - N° de tarjeta
                Serenity.getDriver().findElement(By.id("cardNumber")).clear();
                Serenity.getDriver().findElement(By.id("cardNumber")).sendKeys("123123123123123");
                //TODO: Seleccionar - Canal utlizado / reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Bca Telef")).click();
            }

            if (nuevoTipologia.equals("Cobros Indebidos")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                    }
                }

                //TODO: Ingresa - N° de cuenta
                Serenity.getDriver().findElement(By.id("accountNumber")).clear();
                Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("1007003469143");
                //TODO: Seleccionar - Canal utlizado / reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Bca Telef")).click();
            }


        }

        if (newProducto.equals("PAY PAL")) {
            // TODO: NUEVO
            //TODO: *********************************************** TIPO DE TRAMITE RECLAMO ***********************************************
            if (nuevoTipologia.equals("Transacción mal o no procesada")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                    }
                }

                if (ambiente.equals("UAT")) {
                    //TODO: Ingresa - N° de cuenta
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).clear();
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).sendKeys("1231231231231");
                } else {
                    //TODO: Ingresa - N° de cuenta
                    Serenity.getDriver().findElement(By.id("accountNumber")).clear();
                    Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("1231231231231");
                }

                //TODO: Seleccionar - Canal utlizado / reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Bca Telef")).click();
            }

        }

        if (newProducto.equals("PLIN")) {

            //TODO: *********************************************** TIPO DE TRAMITE RECLAMO ***********************************************
            if (nuevoTipologia.equals("Transacción mal o no procesada")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                    }
                }

                //TODO: Ingresa - N° de cuenta
                Serenity.getDriver().findElement(By.id("accountNumber")).clear();
                Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("1007003469143");
                //TODO: Ingresa - N° de tarjeta
                Serenity.getDriver().findElement(By.id("cardNumber")).clear();
                Serenity.getDriver().findElement(By.id("cardNumber")).sendKeys("123123123123123");
                //TODO: Seleccionar - Canal utlizado / reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Bca Telef")).click();
            }

            if (nuevoTipologia.equals("ATM de IB no entregó dinero")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Importe Solicitado
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ImporteSolicitado'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ImporteSolicitado'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Importe Reclamado
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("300");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Hora de Operación
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Hora'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Hora'])[" + (j + 1) + "]")).sendKeys("10:30");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                    }
                }

                //TODO: Ingresa - N° de celular
                Serenity.getDriver().findElement(By.id("cellPhoneNumber")).clear();
                Serenity.getDriver().findElement(By.id("cellPhoneNumber")).sendKeys("959149978");
                //TODO: Ingresa - N° de cuenta
                Serenity.getDriver().findElement(By.id("accountNumber")).clear();
                Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("1007003469143");
                //TODO: Ingresa - Cógido Cajero
                Serenity.getDriver().findElement(By.id("codeATM")).clear();
                Serenity.getDriver().findElement(By.id("codeATM")).sendKeys("COD124");
                //TODO: Seleccionar - Canal utlizado / reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Bca Telef")).click();
            }


        }

        if (newProducto.equals("TOKEN")) {
            // TODO: NUEVO
            //TODO: *********************************************** TIPO DE TRAMITE RECLAMO ***********************************************
            if (nuevoTipologia.equals("Transacción mal o no procesada")) {

                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();

                if (ambiente.equals("UAT")) {
                    //TODO: Ingresa - N° de serie token
                    Serenity.getDriver().findElement(By.id("tokenSerialNumber")).clear();
                    Serenity.getDriver().findElement(By.id("tokenSerialNumber")).sendKeys("AAAABBBBDDD");
                } else {
                    //TODO: Ingresa - N° de serie token
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).clear();
                    Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).sendKeys("AAAABBBBDDD");
                }

                //TODO: Ingresa - Teléfono de contacto
                Serenity.getDriver().findElement(By.id("contactPhone")).clear();
                Serenity.getDriver().findElement(By.id("contactPhone")).sendKeys("941916927");

                //TODO: Seleccionar - Canal utlizado / reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Bca Telef")).click();
            }

        }

        if (newProducto.equals("TRANSF. INTERNACIONALES")) {

            //TODO: *********************************************** TIPO DE TRAMITE RECLAMO ***********************************************
            if (nuevoTipologia.equals("Cobros Indebidos") || nuevoTipologia.equals("Transacción mal o no procesada")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                    }
                }

                //TODO: Ingresa - N° de cuenta
                Serenity.getDriver().findElement(By.id("accountNumber")).clear();
                Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("1231231231231");
                //TODO: Seleccionar - Canal utlizado / reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Bca Telef")).click();
            }

            if (nuevoTipologia.equals("Constancia por transferencias recibidas")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='EstablecimientoTransaccion'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='EstablecimientoTransaccion'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                    }
                }

                //TODO: Ingresa - N° de cuenta
                Serenity.getDriver().findElement(By.id("accountNumber")).clear();
                Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("1231231231231");
                //TODO: Ingresa - Fecha de operación
                Serenity.getDriver().findElement(By.id("operationDate")).clear();
                Serenity.getDriver().findElement(By.id("operationDate")).sendKeys(getValueFecha());
            }

            if (nuevoTipologia.equals("Informacion de movimientos")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                    }
                }

                //TODO: Ingresa - N° de cuenta
                Serenity.getDriver().findElement(By.id("accountNumber")).clear();
                Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("1231231231231");
                //TODO: Seleccionar - Tipo de transferencia
                Serenity.getDriver().findElement(By.id("typeOfTransfer")).click();
                Serenity.getDriver().findElement(By.id("ENTRADA")).click();
            }

            if (nuevoTipologia.equals("No recepción de transferencia IN/OUT")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='EstablecimientoTransaccion'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='EstablecimientoTransaccion'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                    }
                }

                //TODO: Ingresa - N° de cuenta
                Serenity.getDriver().findElement(By.id("accountNumber")).clear();
                Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("1231231231231");
                //TODO: Ingresa - Nombre del Banco emisor/destino
                Serenity.getDriver().findElement(By.id("destinationIssuingBank")).clear();
                Serenity.getDriver().findElement(By.id("destinationIssuingBank")).sendKeys("BANCO DE CREDITO DEL PERU");
                //TODO: Seleccionar - Canal utlizado / reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Whatsapp")).click();
            }

        }

        if (newProducto.equals("TUNKI")) {

            //TODO: *********************************************** TIPO DE TRAMITE RECLAMO ***********************************************
            if (nuevoTipologia.equals("Transacción mal o no procesada")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                    }
                }

                //TODO: Ingresa - N° de cuenta
                Serenity.getDriver().findElement(By.id("accountNumber")).clear();
                Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("1231231231231");
                //TODO: Ingresa - N° de tarjeta
                Serenity.getDriver().findElement(By.id("cardNumber")).clear();
                Serenity.getDriver().findElement(By.id("cardNumber")).sendKeys("123123123123123");
                //TODO: Seleccionar - Canal utlizado / reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Bca Telef")).click();
            }

            if (nuevoTipologia.equals("ATM de IB no entregó dinero")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Importe Solicitado
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ImporteSolicitado'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ImporteSolicitado'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Importe Reclamado
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("200");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys(getValueFecha());
                        //TODO: Ingresa - Hora de Operación
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Hora'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Hora'])[" + (j + 1) + "]")).sendKeys("03:10");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='EstablecimientoTransaccion'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='EstablecimientoTransaccion'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                    }
                }

                //TODO: Ingresa - Ubicación del cajero
                Serenity.getDriver().findElement(By.id("locationCashier")).sendKeys("Miraflores");
                //TODO: Seleccionar - Canal utilizado/reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Whatsapp")).click();
            }

        }

        if (newProducto.equals("PAGOS PLANILLAS/PROVEEDORES")) {

            if (nuevoTipologia.equals("Transacción mal o no procesada") || nuevoTipologia.equals("Informacion de movimientos")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                    }
                }

                //TODO: Ingresa - N° de cuenta
                Serenity.getDriver().findElement(By.id("accountNumber")).clear();
                Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("1231231231231");

                if (nuevoTipologia.equals("Transacción mal o no procesada")) {
                    //TODO: Seleccionar - Canal utilizado/reclamado
                    Serenity.getDriver().findElement(By.id("usedChannel")).click();
                    Serenity.getDriver().findElement(By.id("Whatsapp")).click();
                }
            }

            if (nuevoTipologia.equals("Activación de Ordenes de Pago")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Ingresa - Nombre Ordenante de la OP original
                Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).clear();
                Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[1]")).sendKeys("JUNIOR MARCOS");
                //TODO: Ingresa - Fecha de operación
                Serenity.getDriver().findElement(By.id("operationDate")).clear();
                Serenity.getDriver().findElement(By.id("operationDate")).sendKeys(getValueFecha());
                //TODO: Ingresa - N° OP original
                Serenity.getDriver().findElement(By.id("OPOriginal")).clear();
                Serenity.getDriver().findElement(By.id("OPOriginal")).sendKeys("ABCDETABCDEFABCDEFABCDEFABCDEFABCDEFABCDEFABCDEFABCDEFABCDEFABCDEFABCDEFABCDEFABCDEFABCDEFABCDEFABCD");
                //TODO: Ingresa - Nombre del Titular
                Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[2]")).clear();
                Serenity.getDriver().findElement(By.xpath("(//input[@id='Descripcion_'])[2]")).sendKeys("JUAN LUIS");
                //TODO: Ingresa - Monto
                Serenity.getDriver().findElement(By.id("amount")).clear();
                Serenity.getDriver().findElement(By.id("amount")).sendKeys("155,596.15");

            }

            if (nuevoTipologia.equals("Cobros Indebidos")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                        //TODO: Ingresa - Cod. cliente/Suministro/Nºcontrato
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ClienteSuministroContrato'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ClienteSuministroContrato'])[" + (j + 1) + "]")).sendKeys("Pharmax S.A.C");
                    }
                }

                //TODO: Ingresa - N° de cuenta
                Serenity.getDriver().findElement(By.id("accountNumber")).clear();
                Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("1007003469143");
                //TODO: Seleccionar - Canal utilizado/reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Whatsapp")).click();
            }

        }

        if (newProducto.equals("SUNAT")) {

            if (nuevoTipologia.equals("Constancia de pago realizado")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Selecciona - Medio de Pago
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='MedioPago'])[" + (j + 1) + "]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='Cargo en Cuenta'])[" + (j + 1) + "]")).click();
                        //TODO: Ingresa - Cod. cliente/Suministro/Nºcontrato
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ClienteSuministroContrato'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ClienteSuministroContrato'])[" + (j + 1) + "]")).sendKeys("255367");
                    }
                }

                //TODO: Ingresa - N° de tarjeta
                Serenity.getDriver().findElement(By.id("cardNumber")).clear();
                Serenity.getDriver().findElement(By.id("cardNumber")).sendKeys("1007003469143263");
                //TODO: Ingresa - N° de cuenta
                Serenity.getDriver().findElement(By.id("accountNumber")).clear();
                Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("1007003469143");
                //TODO: Seleccionar - Canal utilizado/reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Whatsapp")).click();
            }

            if (nuevoTipologia.equals("Corrección de pago realizado")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                        //TODO: Ingresa - Servicio/Empresa
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Servicio'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Servicio'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                        //TODO: Ingresa - Código/Suministro/Nro de contrato
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ClienteSuministroContrato'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ClienteSuministroContrato'])[" + (j + 1) + "]")).sendKeys("255366");
                    }
                }

                //TODO: Ingresa - N° de cuenta (Opcional)
                Serenity.getDriver().findElement(By.id("accountNumber")).clear();
                Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("1007003469143");
                //TODO: Seleccionar - Medio de pago
                Serenity.getDriver().findElement(By.id("paymentMethod")).click();
                Serenity.getDriver().findElement(By.id("Cargo en Cuenta")).click();
            }

            if (nuevoTipologia.equals("Transacción mal o no procesada")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Selecciona - Tipo de operación
                        Serenity.getDriver().findElement(By.xpath("(//div[@id='TipoOperacion'])[" + (j + 1) + "]")).click();
                        Serenity.getDriver().findElement(By.xpath("(//li[@id='Depósito'])[" + (j + 1) + "]")).click();
                        //TODO: Ingresa - Servicio/ Empresa
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ServicioEmpresa'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ServicioEmpresa'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                        //TODO: Ingresa - Cod. cliente/ Suministro/ Nºcontrato
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ClienteSuministroContrato'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='ClienteSuministroContrato'])[" + (j + 1) + "]")).sendKeys("255366");
                    }
                }

                //TODO: Ingresa - N° de cuenta (Opcional)
                Serenity.getDriver().findElement(By.id("accountNumber")).clear();
                Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("4213555322311541");
                //TODO: Seleccionar - Canal utilizado/reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Whatsapp")).click();
            }

            if (nuevoTipologia.equals("Cobros Indebidos")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                    }
                }

                //TODO: Ingresa - N° de cuenta
                Serenity.getDriver().findElement(By.id("accountNumber")).clear();
                Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("4213555322311541");
                //TODO: Seleccionar - Canal utilizado/reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Bca Telef")).click();
            }


        }

        if (newProducto.equals("MONEY EXCHANGE APP")) {

            if (nuevoTipologia.equals("Transacción mal o no procesada")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());

                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {
                        //TODO: Selecciona Motivo
                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                        //TODO: Ingresa - Hora de Operación
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Hora'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Hora'])[" + (j + 1) + "]")).sendKeys("10:30");
                    }
                }

                //TODO: Ingresa - N° de cuenta
                Serenity.getDriver().findElement(By.id("accountNumber")).clear();
                Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("1007003469143");
                //TODO: Seleccionar - Canal utilizado/reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.id("Whatsapp")).click();
            }

        }

        // TODO: ATENCION AL CLIENTE
        if (newProducto.equals("ATENCION AL CLIENTE")) {

            if (nuevoTipologia.equals("Modificación datos personales")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Departamento (*)
                Serenity.getDriver().findElement(By.id("department")).click();
                Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='department']//li)[1]")).click();
                //TODO: Provincia (*)
                Serenity.getDriver().findElement(By.id("province")).click();
                Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='province']//li)[1]")).click();
                //TODO: Distrito (*)
                Serenity.getDriver().findElement(By.id("district")).click();
                Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='district']//li)[1]")).click();
                //TODO: Tipo de via (*)
                Serenity.getDriver().findElement(By.id("streetType")).click();
                Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='streetType']//li)[14]")).click();
                //TODO: Nombre de vía (*)
                Serenity.getDriver().findElement(By.id("trackName")).sendKeys("Andromeda");
                //TODO: Urbanización
                Serenity.getDriver().findElement(By.xpath("//*[@id='urbanization ']")).sendKeys("La Paz");
                //TODO: Número
                Serenity.getDriver().findElement(By.id("streetNumber")).sendKeys("850");
                //TODO: Manzana
                Serenity.getDriver().findElement(By.id("block")).sendKeys("J");
                //TODO: Lote
                Serenity.getDriver().findElement(By.id("lot")).sendKeys("7B");
                //TODO: Interior
                Serenity.getDriver().findElement(By.id("apartment")).sendKeys("20");
                //TODO: Referencia
                Serenity.getDriver().findElement(By.xpath("//textarea[@formcontrolname='reference']")).sendKeys("GS4 Seguridad del Peru");
                //TODO: Numero Celular
                Serenity.getDriver().findElement(By.id("cellPhoneNumber")).sendKeys("923568122");
                //TODO: Selecciona Operador
                Serenity.getDriver().findElement(By.id("tab-aditional-MOVISTAR")).click();
                //TODO: Nuevo email
                Serenity.getDriver().findElement(By.id("personalEmail")).sendKeys("SILVAJAR@GMAIL.COM");
            }

            if (nuevoTipologia.equals("Solicitud de intérprete para usuario")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
            }

            if (nuevoTipologia.equals("Video Arco")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Ingresar Ubicación cajero / tienda
                Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("Torre Bloom");
                //TODO: Ingresar Fecha de operación
                Serenity.getDriver().findElement(By.id("operationDate")).sendKeys("12/12/2022");
                //TODO: Ingresar N° tienda utilizado
                Serenity.getDriver().findElement(By.id("numberStore")).sendKeys("123");
                //TODO: Ingresar Rango Horario
                Serenity.getDriver().findElement(By.id("sheduleRange")).sendKeys("8am");
                //TODO: Secuencia/Ubicación cámara de requerimiento
                Serenity.getDriver().findElement(By.id("suRequestChamber")).sendKeys("1011");
                //TODO: Secuencia/Ubicación cámara hoy
                Serenity.getDriver().findElement(By.id("suChamber")).sendKeys("1015");
                //TODO: Ingresa Medio para la Entrega
                Serenity.getDriver().findElement(By.id("mediumForDelivery")).click();
                Serenity.getDriver().findElement(By.id("Envío de Link por correo")).click();
            }

            if (nuevoTipologia.equals("Billete Falso")) {
                //TODO: VARIABLE GLOBAL - CANTIDAD MOTIVO Y MOVIMIENTO
                int motivoRetificar = Serenity.sessionVariableCalled("motivos").toString().split("-").length;
                int movimientoRetificar = 1;
                try {
                    movimientoRetificar = Integer.parseInt(Serenity.sessionVariableCalled("CantMovimientosPorMotivo").toString());
                } catch (Exception e) {
                    movimientoRetificar = 1;
                }
                for (int i = 0; i < motivoRetificar; i++) {
                    for (int j = 0; j < movimientoRetificar; j++) {

                        //TODO: Selecciona Motivo
                        //     try{Thread.sleep(5000000);}catch (Exception e){}
//                        Serenity.getDriver().findElement(By.id("Reason_0")).click();
                        Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                        Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                        //TODO: Agrega movimientos
                        //TODO: Ingresa - Monto
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Monto'])[" + (j + 1) + "]")).sendKeys("150");
                        //TODO: Ingresa - Fecha
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Fecha'])[" + (j + 1) + "]")).sendKeys("12/05/2022");
                        //TODO: Ingresa - Establecimiento/Transacción
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).clear();
                        Serenity.getDriver().findElement(By.xpath("(//input[@id='Establecimiento'])[" + (j + 1) + "]")).sendKeys(getValueDescripcion());
                    }
                }
                String valueCanal = "";
                try {
                    valueCanal = Serenity.sessionVariableCalled("canal").toString();
                } catch (Exception e) {
                    valueCanal = "App IBK";
                }
                //TODO: Ingresa - Nro. de cuenta
                Serenity.getDriver().findElement(By.id("accountNumber")).sendKeys("13131313131313");
                //TODO: Seleccionar - Canal utlizado / reclamado
                Serenity.getDriver().findElement(By.id("channelClaimed")).click();
                Serenity.getDriver().findElement(By.xpath("(//li[@id='" + valueCanal + "'])[last()]")).click();
            }

            if (nuevoTipologia.equals("Correo/Teléfono Indebido de campañas")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Ingresar - Nombre del Titular
                Serenity.getDriver().findElement(By.id("Descripcion_")).sendKeys("EDWARD");
                //TODO: Seleccionar - Canal utilizado/reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='usedChannel']//li)[1]")).click();
            }

            if (nuevoTipologia.equals("Insatisfacción gestión de entrega de tarjeta")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Ingresar - N° de tarjeta
                Serenity.getDriver().findElement(By.id("cardNumber")).sendKeys("1234567891011");
                //TODO: Seleccionar - Canal utilizado/reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='usedChannel']//li)[1]")).click();
            }

            if (nuevoTipologia.equals("Desacuerdo con el servicio")) {
                //TODO: Seleccionar motivo
                Serenity.getDriver().findElement(By.id("reasonClaim")).click();
                Serenity.getDriver().findElement(By.id(nuevoMotivo)).click();
                //TODO: Seleccionar - Canal utilizado/reclamado
                Serenity.getDriver().findElement(By.id("usedChannel")).click();
                Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='usedChannel']//li)[1]")).click();
            }

        }
    }

        Serenity.setSessionVariable("tipotramite").to(tipoTramiteRetificar);
        Serenity.setSessionVariable("tipologia").to(nuevoTipologia);
        Serenity.takeScreenshot();
        //TODO: BUTTON CONFIRMAR
        JavascriptExecutor js = (JavascriptExecutor) Serenity.getDriver();
        js.executeScript("arguments[0].scrollIntoView();", Serenity.getDriver().findElement(By.xpath("//button[normalize-space()='Confirmar']")));
        Serenity.getDriver().findElement(By.xpath("//button[normalize-space()='Confirmar']")).click();

        try {
            Thread.sleep(5000);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }


    public String getValueFecha() {
        //TODO: ADD FECHA AUTOGENERADO
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
        String valueDate = dateFormat.format(date);
        return valueDate;
    }

    public String getValueDescripcion() {
        //TODO: Descripcion y establecimiento
        String valueDescripcion = "";
        valueDescripcion = "SHOSPTAR";
        return valueDescripcion;
    }

}
