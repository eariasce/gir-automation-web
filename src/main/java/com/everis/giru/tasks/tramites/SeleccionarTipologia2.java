package com.everis.giru.tasks.tramites;

import com.everis.giru.userinterface.tramites.TramitesPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class SeleccionarTipologia2 implements Task {

    private final String tipologia;
    private final String perfil;
    private final String departamento;

    public SeleccionarTipologia2(String tipologia, String perfil, String departamento) {
        this.tipologia = tipologia;
        this.perfil = perfil;
        this.departamento = departamento;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        switch (departamento) {
            case "GPYR":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_TRAMITES_ESPECIALISTA_GPYR, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_TRAMITES_ESPECIALISTA_GPYR)
                                );
                                break;
                            case "CobrosIndebidos":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_COBROS_INDEBIDOS_ESPECIALISTA_GPYR, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_COBROS_INDEBIDOS_ESPECIALISTA_GPYR)
                                );
                                break;
                            case "ExoneracionCobros":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_EXO_COBROS_ESPECIALISTA_GPYR, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_EXO_COBROS_ESPECIALISTA_GPYR)
                                );
                                break;
                            case "AnulacionTC":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_ANULACION_TC_ESPECIALISTA_GPYR, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_ANULACION_TC_ESPECIALISTA_GPYR)
                                );
                                break;
                            case "FinalizacionTC":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_FINALIZ_TC_ESPECIALISTA_GPYR, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_FINALIZ_TC_ESPECIALISTA_GPYR)
                                );
                                break;
                            case "TransaccionMalNoProcesada":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_TRANSACCION_MAL_ONO_PROCESADA_ESPECIALISTA_GPYR, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_TRANSACCION_MAL_ONO_PROCESADA_ESPECIALISTA_GPYR)
                                );
                                break;
                            case "PagoAnticipadoAdelantoCuotas":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_PAGO_ANTICIPADO_ADELANTO_CUOTAS_ESPECIALISTA_GPYR, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_PAGO_ANTICIPADO_ADELANTO_CUOTAS_ESPECIALISTA_GPYR)
                                );
                                break;
                            case "DevolucionSaldoAcreedor":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_DEVOLUCION_SALDO_ACREEDOR_ESPECIALISTA_GPYR, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_DEVOLUCION_SALDO_ACREEDOR_ESPECIALISTA_GPYR)
                                );
                                break;
                            case "ProgramaRecompensas":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_PROGRAMA_RECOMPESAS_ESPECIALISTA_GPYR, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_PROGRAMA_RECOMPESAS_ESPECIALISTA_GPYR)
                                );
                                break;
                            case "SistemaRecompensas":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_SISTEMA_RECOMPENSAS_ESPECIALISTA_GPYR, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_SISTEMA_RECOMPENSAS_ESPECIALISTA_GPYR)
                                );
                                break;
                            case "InsuficienteInformacion":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_INSUFICIENTE_INFORMACION_ESPECIALISTA_GPYR, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_INSUFICIENTE_INFORMACION_ESPECIALISTA_GPYR)
                                );
                                break;
                            case "IncorrectaAplicacionCuotas":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_INCORRECTA_APLICACION_CD_CUOTAS_ESPECIALISTA_GPYR, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_INCORRECTA_APLICACION_CD_CUOTAS_ESPECIALISTA_GPYR)
                                );
                                break;
                            default:
                                System.out.println("No se encuentra tipología !!!");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_TRAMITES_SUPERVISOR_GPYR, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_TRAMITES_SUPERVISOR_GPYR)
                                );
                                break;
                            case "MiEquipo":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_MIEQUIPO_SUPERVISOR_GPYR, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_MIEQUIPO_SUPERVISOR_GPYR)
                                );
                                break;
                            case "Asignar":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_ASIGNAR_SUPERVISOR_GPYR, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_ASIGNAR_SUPERVISOR_GPYR)
                                );
                                break;
                            case "Resolver":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_RESOLVER_SUPERVISOR_GPYR, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_RESOLVER_SUPERVISOR_GPYR)
                                );
                                break;
                            default:
                                System.out.println("No se encuentra tipología en Supervisor GPYR !!!");
                        }
                        break;
                }
                break;
            case "Seguros":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_TRAMITES_ESPECIALISTA_SEGUROS, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_TRAMITES_ESPECIALISTA_SEGUROS)
                                );
                                break;
                            case "CobrosIndebidos":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_COBROS_INDEBIDOS_ESPECIALISTA_SEGUROS, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_COBROS_INDEBIDOS_ESPECIALISTA_SEGUROS)
                                );
                                break;
                            case "ExoneracionCobros":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_EXO_COBROS_ESPECIALISTA_SEGUROS, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_EXO_COBROS_ESPECIALISTA_SEGUROS)
                                );
                                break;
                            case "ConsumosNoReconocidos":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS)
                                );
                                break;
                            case "Monitorear":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_MONITOREAR_ESPECIALISTA_SEGUROS, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_MONITOREAR_ESPECIALISTA_SEGUROS)
                                );
                                break;
                            default:
                                System.out.println("No se encuentra tipología en especialista SEGUROS !!!");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_TRAMITES_SUPERVISOR_SEGUROS, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_TRAMITES_SUPERVISOR_SEGUROS)
                                );
                                break;
                            case "MiEquipo":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_MIEQUIPO_SUPERVISOR_SEGUROS, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_MIEQUIPO_SUPERVISOR_SEGUROS)
                                );
                                break;
                            case "Asignar":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_ASIGNAR_SUPERVISOR_SEGUROS, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_ASIGNAR_SUPERVISOR_SEGUROS)
                                );
                                break;
                            case "Resolver":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_RESOLVER_SUPERVISOR_SEGUROS, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_RESOLVER_SUPERVISOR_SEGUROS)
                                );
                                break;
                            case "Monitorear":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_MONITOREAR_SUPERVISOR_SEGUROS, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_MONITOREAR_SUPERVISOR_SEGUROS)
                                );
                                break;
                            default:
                                System.out.println("No se encuentra tipología en Supervisor SEGUROS !!!");
                        }
                        break;
                }
                break;
            case "Retenciones":
                break;
            case "Controversias":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_TRAMITES_ESPECIALISTA_CONTROVERSIAS, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_TRAMITES_ESPECIALISTA_CONTROVERSIAS)
                                );
                                break;
                            case "ConsumosNoReconocidos":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS)
                                );
                                break;
                            case "ConsumosMalProcesados":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS)
                                );
                                break;
                            case "Monitorear":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_MONITOREAR_ESPECIALISTA_CONTROVERSIAS, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_MONITOREAR_ESPECIALISTA_CONTROVERSIAS)
                                );
                                break;
                            case "TransaccionesEnProceso":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS)
                                );
                                break;
                            default:
                                System.out.println("No se encuentra tipología en especialista CONTROVERSIAS !!!");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_TRAMITES_SUPERVISOR_CONTROVERSIAS, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_TRAMITES_SUPERVISOR_CONTROVERSIAS)
                                );
                                break;
                            case "MiEquipo":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_MIEQUIPO_SUPERVISOR_CONTROVERSIAS, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_MIEQUIPO_SUPERVISOR_CONTROVERSIAS)
                                );
                                break;
                            case "Asignar":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_ASIGNAR_SUPERVISOR_CONTROVERSIAS, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_ASIGNAR_SUPERVISOR_CONTROVERSIAS)
                                );
                                break;
                            case "Resolver":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_RESOLVER_SUPERVISOR_CONTROVERSIAS, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_RESOLVER_SUPERVISOR_CONTROVERSIAS)
                                );
                                break;
                            case "Monitorear":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_MONITOREAR_SUPERVISOR_CONTROVERSIAS, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_MONITOREAR_SUPERVISOR_CONTROVERSIAS)
                                );
                                break;
                            case "TransaccionesEnProceso":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS)
                                );
                                break;
                            default:
                                System.out.println("No se encuentra tipología en Supervisor CONTROVERSIAS !!!");
                        }
                        break;
                }
                break;
            case "USPR":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_TRAMITES_ESPECIALISTA_USPR, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_TRAMITES_ESPECIALISTA_USPR)
                                );
                                break;
                            case "CobrosIndebidos":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_COBROS_INDEBIDOS_ESPECIALISTA_USPR, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_COBROS_INDEBIDOS_ESPECIALISTA_USPR)
                                );
                                break;
                            case "ExoneracionCobros":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_EXO_COBROS_ESPECIALISTA_USPR, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_EXO_COBROS_ESPECIALISTA_USPR)
                                );
                                break;
                            case "ConsumosMalProcesados":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR)
                                );
                                break;
                            case "ConsumosNoReconocidos":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR)
                                );
                                break;
                            case "TransaccionMalNoProcesada":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_TRANSACCION_MAL_ONO_PROCESADA_ESPECIALISTA_USPR, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_TRANSACCION_MAL_ONO_PROCESADA_ESPECIALISTA_USPR)
                                );
                                break;
                            case "DuplicadoEstadoCuentaSC":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_DUPLICADO_ESTADO_CUENTA_SC_ESPECIALISTA_USPR, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_DUPLICADO_ESTADO_CUENTA_SC_ESPECIALISTA_USPR)
                                );
                                break;
                            case "SistemaRecompensas":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_SISTEMA_DE_RECOMPENSAS_ESPECIALISTA_USPR, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_SISTEMA_DE_RECOMPENSAS_ESPECIALISTA_USPR)
                                );
                                break;
                            case "DesacuerdoCondicionesTT":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_DESACUERDO_CONDICIONES_TT_ESPECIALISTA_USPR, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_DESACUERDO_CONDICIONES_TT_ESPECIALISTA_USPR)
                                );
                                break;

                            case "Retipificar":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_CONSUMOS_RETIPIFICAR_ESPECIALISTA_USPR, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_CONSUMOS_RETIPIFICAR_ESPECIALISTA_USPR)
                                );
                                break;
                            default:
                                System.out.println("No se encuentra tipología en especialista USPR !!!");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_TRAMITES_SUPERVISOR_USPR, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_TRAMITES_SUPERVISOR_USPR)
                                );
                                break;
                            case "MiEquipo":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_MIEQUIPO_SUPERVISOR_USPR, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_MIEQUIPO_SUPERVISOR_USPR)
                                );
                                break;
                            case "Asignar":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_ASIGNAR_SUPERVISOR_USPR, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_ASIGNAR_SUPERVISOR_USPR)
                                );
                                break;
                            case "Resolver":
                                actor.attemptsTo(
                                        WaitUntil.the(TramitesPage.SIDENAV_MENU_RESOLVER_SUPERVISOR_USPR, isVisible()).forNoMoreThan(100).seconds(),
                                        Click.on(TramitesPage.SIDENAV_MENU_RESOLVER_SUPERVISOR_USPR)
                                );
                                break;
                            default:
                                System.out.println("No se encuentra tipología en Supervisor USPR !!!");
                        }
                        break;
                }
                break;
        }

    }

    public static Performable withData(String tipologia, String perfil, String departamento) {
        return instrumented(SeleccionarTipologia2.class, tipologia, perfil, departamento);
    }

}
