package com.everis.giru.tasks.tramites.filtros;

import com.everis.giru.userinterface.tramites.FiltrosBandejaPage;
import com.everis.giru.userinterface.tramites.filtrosPage.GetFiltroNivelServicioPage;
import com.everis.giru.userinterface.tramites.validaFiltros.ValidaFiltroNivelServicioPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class SeleccionarFiltroNivelServicio implements Task {

    private final String departamento;
    private final String perfil;
    private final String tipologia;
    private final String fNivelServicio;

    GetFiltroNivelServicioPage getFiltroNivelServicioPage;
    ValidaFiltroNivelServicioPage validaFiltroNivelServicioPage;

    public SeleccionarFiltroNivelServicio(String departamento, String perfil, String tipologia, String fNivelServicio) {
        this.departamento = departamento;
        this.perfil = perfil;
        this.tipologia = tipologia;
        this.fNivelServicio = fNivelServicio;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        if (!(fNivelServicio.equals("No"))) {
            actor.attemptsTo(
                    WaitUntil.the(FiltrosBandejaPage.BUTTOM_FILTRO_NIVELSERVICIO, isVisible()).forNoMoreThan(100).seconds(),
                    Click.on(FiltrosBandejaPage.BUTTOM_FILTRO_NIVELSERVICIO)
            );

            getFiltroNivelServicioPage.seleccionaFiltroNivelServicio(fNivelServicio);

            actor.attemptsTo(
                    WaitUntil.the(FiltrosBandejaPage.BUTTOM_FILTRO_APLICAR, isVisible()).forNoMoreThan(100).seconds(),
                    Click.on(FiltrosBandejaPage.BUTTOM_FILTRO_APLICAR)
            );

            validaFiltroNivelServicioPage.validaFiltroNivelServicio(departamento, perfil, tipologia, fNivelServicio);

        }

    }

    public static Performable withData(String departamento, String perfil, String tipologia, String fNivelServicio) {
        return instrumented(SeleccionarFiltroNivelServicio.class, departamento, perfil, tipologia, fNivelServicio);
    }

}
