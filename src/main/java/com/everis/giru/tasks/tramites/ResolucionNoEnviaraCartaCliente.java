package com.everis.giru.tasks.tramites;

import com.everis.giru.tasks.resolucion.ResolucionSiEnviaraCartaCliente;
import com.everis.giru.tasks.resolucion.ResolucionSeleccionaNoAdjuntarCarta;
import lombok.SneakyThrows;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.Objects;

public class ResolucionNoEnviaraCartaCliente implements Task {

    @SneakyThrows
    public <T extends Actor> void performAs(T actor) {

        ResolucionSiEnviaraCartaCliente resolucionSiEnviaraCartaCliente = new ResolucionSiEnviaraCartaCliente();
        ResolucionSeleccionaNoAdjuntarCarta resolucionSeleccionaNoAdjuntarCarta = new ResolucionSeleccionaNoAdjuntarCarta();

        String estoyEnResolutoria = Serenity.sessionVariableCalled("estoyEnResolutoria").toString();
        String tipoProducto = Serenity.sessionVariableCalled("tipoproducto").toString();
        String tipologia = Serenity.sessionVariableCalled("tipologia").toString();

        String carta = "";
        String tipoPrograma = "";

        try {
            carta = Serenity.sessionVariableCalled("carta").toString();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        try {
            tipoPrograma = Serenity.sessionVariableCalled("tipoPrograma").toString();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }


        if (Objects.equals(carta, "No")) {
            //TODO: LLEGA A AREA RESOLUTORIA - SI Y AGREGA LOS TIPOLOGIAS PARA RESOLUCION
            if (Objects.equals(estoyEnResolutoria, "Si") && Objects.equals(carta, "No")) {
                System.out.println("********************** SE INGRESA A MODO RESOLUCIÓN **********************");

                if (tipoProducto.equals("CAMBIOS DE TASAS")) {

                    if (tipologia.equals("NUEVA TASA")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                        JavascriptExecutor js = (JavascriptExecutor) Serenity.getDriver();
                        js.executeScript("arguments[0].scrollIntoView();", Serenity.getDriver().findElement(By.xpath("//label[@for='flagManualPayment']")));
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagManualPayment']")).click();
                    }

                }
                    if (tipoProducto.equals("CUENTA")) {

                    if (tipologia.equals("EXONERACIÓN DE COBROS") || tipologia.equals("COBROS INDEBIDOS") ||
                            tipologia.equals("CUENTA/CANCELACIÓN NO RECONOCIDA") || tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                        JavascriptExecutor js = (JavascriptExecutor) Serenity.getDriver();
                        js.executeScript("arguments[0].scrollIntoView();", Serenity.getDriver().findElement(By.xpath("//label[@for='flagManualPayment']")));
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagManualPayment']")).click();
                    }

                    if (tipologia.equals("DUPLICADO DE ESTADO DE CUENTA/SERVICIO DE COPIAS") || tipologia.equals("DESACUERDO DE CONDICIONES/TASAS/TARIFAS") ||
                            tipologia.equals("CANCELACIÓN DE CUENTA") || tipologia.equals("INFORMACION DE MOVIMIENTOS") ||
                            tipologia.equals("DUPLICADO DE DOCUMENTO") || tipologia.equals("NO RECEPCIÓN DE DOCUMENTO") ||
                            tipologia.equals("SOLICITUD DE ALCANCÍA") || tipologia.equals("PAGO CTS PESCA") ||
                            tipologia.equals("ATENCIÓN IBK AGENTE") || tipologia.equals("INCUMPLIMIENTO DE CAMPAÑA") ||
                            tipologia.equals("TRASLADOS DE CTS A OTRO BANCO")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                    }

                    if (tipologia.equals("SISTEMA DE RECOMPENSAS")) {
                        String importe = Serenity.sessionVariableCalled("importe").toString();
                        String cantidad = Serenity.sessionVariableCalled("cantidad").toString();

                        if (tipoPrograma.equals("Descuento cuenta sueldo") || tipoPrograma.equals("Descuento en Cuotas")) {
                            Serenity.getDriver().findElement(By.xpath("//input[@name='importeAprobado']")).sendKeys(importe);
                            //TODO: No Adjunta Carta
                            resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                        }

                        if (tipoPrograma.equals("Efectivo")) {
                            Serenity.getDriver().findElement(By.xpath("//input[@name='importeAprobado']")).sendKeys(importe);
                            //TODO: No Adjunta Carta
                            resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                            //TODO: TIPO DE PROGRAMA EFECTIVO TIENE FLAGS - flagAbonoManual
                            Serenity.getDriver().findElement(By.xpath("//label[@for='flagAbonoManual-']")).click();
                        }

                        if (tipoPrograma.equals("Millas")) {
                            Serenity.getDriver().findElement(By.xpath("//input[@name='cantidadAprobada']")).sendKeys("20");
                            //TODO: No Adjunta Carta
                            resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                        }

                        if (tipoPrograma.equals("Opciones sorteo millonario") || tipoPrograma.equals("Vales") || tipoPrograma.equals("Vales ruleta")) {
                            //TODO: Ingresa Importe aprobado
                            Serenity.getDriver().findElement(By.xpath("//input[@name='importeAprobado']")).sendKeys(importe);
                            //TODO: Ingresa Cantidad aprobada
                            Serenity.getDriver().findElement(By.xpath("//input[@name='cantidadAprobada']")).sendKeys(cantidad);
                            //TODO: No Adjunta Carta
                            resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                        }
                    }

                    if (tipologia.equals("ATM DE IB NO DEPOSITÓ DINERO") || tipologia.equals("LIBERACIÓN DE RETENCIONES JUDICIALES")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                        JavascriptExecutor js = (JavascriptExecutor) Serenity.getDriver();
                        js.executeScript("arguments[0].scrollIntoView();", Serenity.getDriver().findElement(By.xpath("//label[@for='flagAbonoManual-']")));

                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagAbonoManual-']")).click();
                    }

                }

                if (tipoProducto.equals("TARJETA DE CREDITO")) {

                    if (tipologia.equals("EXONERACIÓN DE COBROS") || tipologia.equals("COBROS INDEBIDOS") ||
                            tipologia.equals("CONSUMOS NO RECONOCIDOS POR FRAUDE") || tipologia.equals("CONSUMOS MAL PROCESADOS POR EL COMERCIO/MARCA") ||
                            tipologia.equals("MODIFICACION PLAZO") || tipologia.equals("PAGO ANTICIPADO Y ADELANTO DE CUOTAS") ||
                            tipologia.equals("DUPLICADO DE ESTADO DE CUENTA/SERVICIO DE COPIAS") || tipologia.equals("INSUFICIENTE INFORMACIÓN") ||
                            tipologia.equals("INCORRECTA APLICACIÓN CD/EC/C. CUOTAS") || tipologia.equals("ATENCIÓN POR PRENVENCIÓN Y FRAUDES") ||
                            tipologia.equals("DUPLICADO DE VOUCHER") || tipologia.equals("BAJA DE TASA") ||
                            tipologia.equals("MODIFICACIÓN DE GRUPO DE LIQUIDACIÓN") || tipologia.equals("MODIFICACIÓN LÍNEA TC ADICIONAL") ||
                            tipologia.equals("REDUCCIÓN DE LÍNEA TC") || tipologia.equals("TRASLADO DE SALDO ACREEDOR") ||
                            tipologia.equals("AFILIACIÓN / DESAFILIACIÓN DÉBITO AUTOMÁTICO") || tipologia.equals("DUPLICADO DE DOCUMENTO") ||
                            tipologia.equals("REGRABACIÓN DE PLÁSTICO") || tipologia.equals("NO RECEPCIÓN DE DOCUMENTO") ||
                            tipologia.equals("DESACUERDO DE CONDICIONES/TASAS/TARIFAS") || tipologia.equals("REPORTE INDEBIDO CENTRALES DE RIESGO") ||
                            tipologia.equals("UNIFICACIÓN DE MARCAS/TIPOS DE TARJETAS") || tipologia.equals("REPROGRAMACIÓN DE DEUDA") ||
                            tipologia.equals("DESBLOQUEO PREVENTIVO/SOBREENDEUDAMIENTO") || tipologia.equals("REPROGRAMACIÓN DE DEUDA TC SIN CAMPAÑA") ||
                            tipologia.equals("REPROGRAMACIÓN DE DEUDA TC CON CAMPAÑA") || tipologia.equals("COMPRA DE DEUDA INTERNA") ||
                            tipologia.equals("CORRECCIÓN DE PAGO REALIZADO") || tipologia.equals("MODIFICACIÓN DISPOSICIÓN DE EFECTIVO") ||
                            tipologia.equals("REVERSIÓN DE UPGRADE") || tipologia.equals("DEVOLUCION DE DOCUMENTOS") ||
                            tipologia.equals("ATENCIÓN IBK AGENTE") || tipologia.equals("COMPRA DE DEUDA SIN AMPLIACIÓN") ||
                            tipologia.equals("MODIFICACIÓN DE TASA COMPRA DE DEUDA TC") || tipologia.equals("ENDOSO SEGUROS DE DESGRAVAMEN") ||
                            tipologia.equals("REGULARIZACIÓN DE COMPRA DE DEUDA TC") || tipologia.equals("INFORMACIÓN DE ESTABLECIMIENTO/ RET.EFECTIVO") ||
                            tipologia.equals("INFORMACION DE MOVIMIENTOS") || tipologia.equals("INCUMPLIMIENTO DE CAMPAÑA") ||
                            tipologia.equals("CAMBIO DE FORMA DE ENVÍO DE CORRESPONDENCIA") || tipologia.equals("ABONO DE MILLAS") ||
                            tipologia.equals("CAMBIO DE TARJETA")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                    }

                    if (tipologia.equals("DEVOLUCION DE SALDO ACREEDOR") || tipologia.equals("LIBERACION DE RETENCIONES") ||
                            tipologia.equals("OPERACIÓN DENEGADA") || tipologia.equals("ATM DE IB NO DEPOSITÓ DINERO") ||
                            tipologia.equals("ATM DE IB NO ENTREGÓ DINERO") || tipologia.equals("PAGO ANTICIPADO Y ADELANTO DE CUOTAS") ||
                            tipologia.equals("ATM DE OTRO BANCO NO ENTREGÓ DINERO") || tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA")) {

                        if (tipologia.equals("DEVOLUCION DE SALDO ACREEDOR")) {
                            Serenity.getDriver().findElement(By.id("approvedAmount")).clear();
                            Serenity.getDriver().findElement(By.id("approvedAmount")).sendKeys("10");
                        }

                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                        //TODO: Acepto que el abono lo realizaré manualmente
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagAbonoManual-']")).click();
                    }

                    if (tipologia.equals("SISTEMA DE RECOMPENSAS")) {
                        String importe = Serenity.sessionVariableCalled("importe").toString();
                        String cantidad = Serenity.sessionVariableCalled("cantidad").toString();

                        if (tipoPrograma.equals("Descuento cuenta sueldo") || tipoPrograma.equals("Descuento en Cuotas")) {
                            Serenity.getDriver().findElement(By.xpath("//input[@name='importeAprobado']")).sendKeys(importe);
                            //TODO: No Adjunta Carta
                            resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                        }

                        if (tipoPrograma.equals("Efectivo")) {
                            Serenity.getDriver().findElement(By.xpath("//input[@name='importeAprobado']")).sendKeys(importe);
                            //TODO: No Adjunta Carta
                            resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                            //TODO: TIPO DE PROGRAMA EFECTIVO TIENE FLAGS - flagAbonoManual
                            Serenity.getDriver().findElement(By.xpath("//label[@for='flagAbonoManual-']")).click();
                        }

                        if (tipoPrograma.equals("Millas")) {
                            Serenity.getDriver().findElement(By.xpath("//input[@name='cantidadAprobada']")).sendKeys("20");
                            //TODO: No Adjunta Carta
                            resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                        }

                        if (tipoPrograma.equals("Opciones sorteo millonario") || tipoPrograma.equals("Vales") || tipoPrograma.equals("Vales ruleta")) {
                            //TODO: Ingresa Importe aprobado
                            Serenity.getDriver().findElement(By.xpath("//input[@name='importeAprobado']")).sendKeys(importe);
                            //TODO: Ingresa Cantidad aprobada
                            Serenity.getDriver().findElement(By.xpath("//input[@name='cantidadAprobada']")).sendKeys(cantidad);
                            //TODO: No Adjunta Carta
                            resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                        }
                    }

                    if (tipologia.equals("PROGRAMA DE RECOMPENSAS")) {
                        switch (tipoPrograma) {
                            case "Efectivo":
                                Serenity.getDriver().findElement(By.xpath("//input[@name='importeAprobado']")).sendKeys("10");
                                System.out.println("<< RESOLUCION >>" + " Importe Aprobado");
                                //TODO: No Adjunta Carta
                                resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                                //TODO: Acepto que el abono lo realizaré manualmente
                                Serenity.getDriver().findElement(By.xpath("//label[@for='flagAbonoManual-']")).click();
                                break;
                            case "Millas":
                                Serenity.getDriver().findElement(By.xpath("//input[@name='cantidadAprobada']")).sendKeys("20");
                                System.out.println("<< RESOLUCION >>" + " Cantidad Aprobada");
                                //TODO: No Adjunta Carta
                                resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                                break;
                            case "Vales":
                                Serenity.getDriver().findElement(By.xpath("//input[@name='cantidadAprobada']")).sendKeys("30");
                                Serenity.getDriver().findElement(By.xpath("//input[@name='importeAprobado']")).sendKeys("50");
                                System.out.println("<< RESOLUCION >>" + " Cantidad Aprobada y Importe Aprobado");
                                //TODO: No Adjunta Carta
                                resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                                break;

                            default:
                                System.out.println("No encuentra tipo de programa RESOLUCION !!!: ");
                        }
                    }

                }
                if (tipoProducto.equals("CUENTA CORRIENTE") ||tipoProducto.equals("CUENTA NEGOCIOS")) {

                    if ( tipologia.equals("COBROS INDEBIDOS")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                        //TODO: Acepto que el abono lo realizaré manualmente
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagManualPayment']")).click();
                    }

                }
                //**********************COMERCIAL******************************//


                if (tipoProducto.equals("TARJETA DE DÉBITO")) {

                    if (tipologia.equals("CONSUMOS NO RECONOCIDOS POR FRAUDE")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                        //TODO: Acepto que el abono lo realizaré manualmente
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagAbonoManual-']")).click();
                    }

                    if (tipologia.equals("CONSUMOS MAL PROCESADOS POR EL COMERCIO/MARCA")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                        //TODO: Acepto que el abono lo realizaré manualmente
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagManualPayment']")).click();
                    }
                    if (tipologia.equals("CONSUMO/ RETIRO NO EFECTUADO")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                    }
                }

                if (tipoProducto.equals("TARJETA DE CRÉDITO")) {

                    if (tipologia.equals("CONSUMOS NO RECONOCIDOS POR FRAUDE")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                        //TODO: Acepto que el abono lo realizaré manualmente
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagAbonoManual-']")).click();
                    }

                    if (tipologia.equals("CONSUMOS MAL PROCESADOS POR EL COMERCIO/MARCA")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                        //TODO: Acepto que el abono lo realizaré manualmente
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagManualPayment']")).click();
                    }
                }
                if (tipoProducto.equals("CRÉDITO BPE")) {
                    if (tipologia.equals("COBROS INDEBIDOS")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                        //TODO: Acepto que el abono lo realizaré manualmente
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagManualPayment']")).click();
                    }
                }
                if (tipoProducto.equals("SEGUROS RELACIONADOS")||tipoProducto.equals("SEGUROS")) {
                    if (tipologia.equals("DUPLICADO DE DOCUMENTO")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                    }
                    if (tipologia.equals("DESAFILIACIÓN DE SEGURO")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                    }
                }

                if (tipoProducto.equals("ATENCIÓN AL CLIENTE")) {
                    if (tipologia.equals("DESACUERDO CON LA ATENCIÓN")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                        //TODO: Acepto que el abono lo realizaré manualmente
                        //Serenity.getDriver().findElement(By.xpath("//label[@for='flagManualPayment']")).click();
                    }
                }
                if (tipoProducto.equals("CUENTA NEGOCIO")) {
                    if (tipologia.equals("COBROS INDEBIDOS")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                        //TODO: Acepto que el abono lo realizaré manualmente
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagManualPayment']")).click();
                    }
                }
                if (tipoProducto.equals("CUENTA CORRIENTE")) {
                    if (tipologia.equals("EXONERACIÓN DE COBROS")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                        //TODO: Acepto que el abono lo realizaré manualmente
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagManualPayment']")).click();
                    }
                }
            //**********************************************************************************************************************************//
                    if (tipoProducto.equals("TARJETA DE DEBITO")) {

                        if (tipologia.equals("EXONERACIÓN DE COBROS") || tipologia.equals("COBROS INDEBIDOS")) {
                            //TODO: No Adjunta Carta
                            resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                            //TODO: Acepto que el abono lo realizaré manualmente
                            Serenity.getDriver().findElement(By.xpath("//label[@for='flagManualPayment']")).click();
                        }

                    if (tipologia.equals("CONSUMOS NO RECONOCIDOS POR FRAUDE") || tipologia.equals("CONSUMOS MAL PROCESADOS POR EL COMERCIO/MARCA")) {
                        //TODO: Seleccionar : Tipo abono
                        try {
                            Thread.sleep(1000);
                        } catch (Exception e) {
                            System.out.println(e.getMessage());
                        }

                        Serenity.getDriver().findElement(By.id("Abono_0_0")).click();
                        Serenity.getDriver().findElement(By.id("DEFINITIVO")).click();

                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                    }

                    if (tipologia.equals("LIBERACION DE RETENCIONES") || tipologia.equals("OPERACIÓN DENEGADA") ||
                            tipologia.equals("ATM DE IB NO ENTREGÓ DINERO") || tipologia.equals("ATM DE OTRO BANCO NO ENTREGÓ DINERO")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                        //TODO: Acepto que el abono lo realizaré manualmente
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagAbonoManual-']")).click();
                    }

                    if (tipologia.equals("PROGRAMA DE RECOMPENSAS") || tipologia.equals("REVISIÓN DE DEPÓSITOS A TERCEROS") ||
                            tipologia.equals("REPOSICIÓN DE TARJETA DE DEBITO") || tipologia.equals("INFORMACIÓN DE ESTABLECIMIENTO/ RET.EFECTIVO") ||
                            tipologia.equals("ATENCIÓN POR PRENVENCIÓN Y FRAUDES") || tipologia.equals("ABONO DE MILLA")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                    }

                }

                if (tipoProducto.equals("SEGUROS")) {

                    if (tipologia.equals("SOLICITUD DE ANULACIÓN DE SEGURO") || tipologia.equals("DUPLICADO DE DOCUMENTO") ||
                            tipologia.equals("SEGURO DESGRAVAMEN FALLECIMIENTO") || tipologia.equals("PASE A CUOTAS SEGURO EXTRACASH") ||
                            tipologia.equals("DESAFILIACIÓN DE SEGURO") || tipologia.equals("INFORMACION DE MOVIMIENTOS") ||
                            tipologia.equals("NO RECEPCIÓN DE DOCUMENTO") || tipologia.equals("CAMBIO DE SEGURO EXTERNO A INTERNO") ||
                            tipologia.equals("MODIFICACIÓN DE CONDICIONES CONTRACTUALES")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                    }

                    if (tipologia.equals("COBROS INDEBIDOS")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                        //TODO: Acepto que el abono lo realizaré manualmente
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagAbonoManual-']")).click();
                    }

                }

                if (tipoProducto.equals("COBRANZAS")) {

                    if (tipologia.equals("COBROS INDEBIDOS")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                        //TODO: Acepto que el abono lo realizaré manualmente
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagAbonoManual-']")).click();
                    }

                    if (tipologia.equals("DIRECCIÓN/TELF. INDEBIDOS DE COBRANZA") || tipologia.equals("REPORTE INDEBIDO CENTRALES DE RIESGO") ||
                            tipologia.equals("CONSTANCIA DE NO ADEUDO")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                    }

                    if (tipologia.equals("RECTIFICACIÓN SBS")) {
                        System.out.println("La tipología no requiere carta ni otros datos adicionales");
                    }

                }

                if (tipoProducto.equals("APP INTERBANK")) {

                    if (tipologia.equals("CONSTANCIA DE PAGO REALIZADO")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                    }

                    if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                        //TODO: Acepto que el abono lo realizaré manualmente
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagAbonoManual-']")).click();
                    }
                }

                if (tipoProducto.equals("BANCA CELULAR")) {

                    if (tipologia.equals("DESAFILIACIÓN/ACTUALIZACIÓN DE NÚMERO CELULAR")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                    }

                    if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                        //TODO: Acepto que el abono lo realizaré manualmente
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagAbonoManual-']")).click();
                    }

                }

                if (tipoProducto.equals("ATENCION AL CLIENTE")) {

                    if (tipologia.equals("DESACUERDO CON EL SERVICIO") || tipologia.equals("SOLICITUD DE INTÉRPRETE PARA USUARIO") ||
                            tipologia.equals("MODIFICACIÓN DATOS PERSONALES") || tipologia.equals("VIDEO ARCO") ||
                            tipologia.equals("BILLETE FALSO") || tipologia.equals("PROTECCIÓN DE DATOS - DERECHO ARCO") ||
                            tipologia.equals("CORREO/TELÉFONO INDEBIDO DE CAMPAÑAS") || tipologia.equals("INSATISFACCIÓN GESTIÓN DE ENTREGA DE TARJETA")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                    }

                    if (tipologia.equals("DENUNCIAS")) {
                        System.out.println("La tipología no requiere carta ni otros datos adicionales");

                        //TODO: ******************* Producto y tipología asociado a la denuncia ************************
                        //TODO: Seleccione - Producto Denuncias
                        Serenity.getDriver().findElement(By.id("product")).click();
                        Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='product']//li)[1]")).click();
                        //TODO: Seleccione - Tipología Denuncias
                        Serenity.getDriver().findElement(By.id("typology")).click();
                        Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='typology']//li)[1]")).click();
                        //TODO: Seleccione - Motivo Denuncias
                        Serenity.getDriver().findElement(By.id("reason")).click();
                        Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='reason']//li)[1]")).click();
                        //TODO: Seleccione - Canal utilizado
                        Serenity.getDriver().findElement(By.id("usedChannel")).click();
                        Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='usedChannel']//li)[1]")).click();
                        //TODO: Seleccione - Tipo de operación
                        Serenity.getDriver().findElement(By.id("operationType")).click();
                        Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='operationType']//li)[1]")).click();

                        //TODO: ******************* Datos adicionales de la Denuncia ************************
                        //TODO: Ingresa - Fecha soluc. Interbank
                        Serenity.getDriver().findElement(By.id("Fecha__")).sendKeys("28/03/2023");
                        //TODO: Seleccione - Estado Indecopi/DCF
                        Serenity.getDriver().findElement(By.id("indecopiState")).click();
                        Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='indecopiState']//li)[1]")).click();
                        //TODO: Seleccione - ¿Audiencia?
                        Serenity.getDriver().findElement(By.xpath("//label[@for='tab-Si']")).click();
                        //TODO: Seleccione - Posible multa en UITS
                        Serenity.getDriver().findElement(By.xpath("//label[@for='tab-ticket-No']")).click();

                        //TODO: Soles
                        //TODO: Ingresa - Monto reclamado denuncias
                        Serenity.getDriver().findElement(By.xpath("//input[@formcontrolname='amountClaimedComplaintsPEN']")).sendKeys("500");
                        //TODO: Ingresa - Monto devuelto con Autonomía
                        Serenity.getDriver().findElement(By.xpath("//input[@formcontrolname='amountReturnedWithAutonomyPEN']")).sendKeys("450");
                        //TODO: Ingresa - Monto devuelto sin Autonomía
                        Serenity.getDriver().findElement(By.xpath("//input[@formcontrolname='amountReturnedWithoutAutonomyPEN']")).sendKeys("250");

                        //TODO: Dolar
                        //TODO: Ingresa - Monto reclamado denuncias
                        Serenity.getDriver().findElement(By.xpath("//input[@formcontrolname='amountClaimedComplaintsUSD']")).sendKeys("500");
                        //TODO: Ingresa - Monto devuelto con Autonomía
                        Serenity.getDriver().findElement(By.xpath("//input[@formcontrolname='amountReturnedWithAutonomyUSD']")).sendKeys("450");
                        //TODO: Ingresa - Monto devuelto sin Autonomía
                        Serenity.getDriver().findElement(By.xpath("//input[@formcontrolname='amountReturnedWithoutAutonomyUSD']")).sendKeys("250");

                        //TODO: Seleccione - Motivo devolución
                        Serenity.getDriver().findElement(By.id("reasonDevolution")).click();
                        Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='reasonDevolution']//li)[1]")).click();
                    }

                    if (tipologia.equals("DENUNCIAS LEGAL")) {
                        System.out.println("La tipología no requiere carta ni otros datos adicionales");
                    }

                }

                if (tipoProducto.equals("DEBITO AUTOMATICO")) {
                    //TODO: No Adjunta Carta
                    resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                    //TODO: Acepto que el abono lo realizaré manualmente
                    Serenity.getDriver().findElement(By.xpath("//label[@for='flagAbonoManual-']")).click();
                }

                if (tipoProducto.equals("PAGO DE SERVICIO POR CANALES")) {

                    if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA") || tipologia.equals("COBROS INDEBIDOS") ||
                            tipologia.equals("ATM DE IB NO DEPOSITÓ DINERO")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                        //TODO: Acepto que el abono lo realizaré manualmente
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagAbonoManual-']")).click();
                    }

                    if (tipologia.equals("CORRECCIÓN DE PAGO REALIZADO") || tipologia.equals("ATENCIÓN IBK AGENTE")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();


                    }

                }

                if (tipoProducto.equals("PODER")) {

                    if (tipologia.equals("SOLICITUD VALIDACIÓN DE PODERES LIMA")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                    }

                }

                if (tipoProducto.equals("REMESAS")) {

                    if (tipologia.equals("CONSTANCIA POR TRANSFERENCIAS RECIBIDAS") || tipologia.equals("NO RECEPCIÓN DE TRANSFERENCIA IN/OUT") ||
                            tipologia.equals("ATENCIÓN IBK AGENTE")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                    }

                    if (tipologia.equals("ATM DE IB NO ENTREGÓ DINERO") || tipologia.equals("COBROS INDEBIDOS") ||
                            tipologia.equals("CONSUMO/ RETIRO NO EFECTUADO")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                        //TODO: Acepto que el abono lo realizaré manualmente
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagAbonoManual-']")).click();
                    }

                }

                if (tipoProducto.equals("TRANSF. NACIONALES")) {

                    if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA") || tipologia.equals("COBROS INDEBIDOS")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                        //TODO: Acepto que el abono lo realizaré manualmente
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagAbonoManual-']")).click();
                    }

                }

                if (tipoProducto.equals("ADELANTO SUELDO")) {

                    if (tipologia.equals("ANULACIÓN DE CRÉDITO") || tipologia.equals("INFORMACION DE MOVIMIENTOS") ||
                            tipologia.equals("MODIFICACIÓN DE CUENTA SUELDO ASOCIADA") || tipologia.equals("DUPLICADO DE DOCUMENTO")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                    }

                    if (tipologia.equals("COBROS INDEBIDOS") || tipologia.equals("CONSUMO/ RETIRO NO EFECTUADO")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                        //TODO: Acepto que el abono lo realizaré manualmente
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagAbonoManual-']")).click();
                    }

                }

                if (tipoProducto.equals("LINEA CONVENIO")) {

                    if (tipologia.equals("BAJA DE TASA") || tipologia.equals("MODIFICACIÓN DE CONDICIONES CONTRACTUALES") ||
                            tipologia.equals("ANULACIÓN DE CRÉDITO") || tipologia.equals("DUPLICADO DE DOCUMENTO") ||
                            tipologia.equals("PAGO ANTICIPADO Y ADELANTO DE CUOTAS") || tipologia.equals("DUPLICADO DE VOUCHER") ||
                            tipologia.equals("INFORMACION DE MOVIMIENTOS") || tipologia.equals("DESACUERDO DE CONDICIONES/TASAS/TARIFAS") ||
                            tipologia.equals("REPORTE INDEBIDO CENTRALES DE RIESGO") || tipologia.equals("NO RECEPCIÓN DE DOCUMENTO") ||
                            tipologia.equals("ENDOSO SEGUROS DE DESGRAVAMEN")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                    }

                    if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA") || tipologia.equals("CONSUMO/ RETIRO NO EFECTUADO") ||
                            tipologia.equals("DEVOLUCIÓN CUOTAS EN TRÁNSITO")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                        //TODO: Acepto que el abono lo realizaré manualmente
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagAbonoManual-']")).click();
                    }

                }

                if (tipoProducto.equals("HIPOTECARIO")) {

                    if (tipologia.equals("BAJA DE TASA") || tipologia.equals("DUPLICADO DE CRONOGRAMA") ||
                            tipologia.equals("NO RECEPCIÓN DE DOCUMENTO") || tipologia.equals("DESACUERDO DE CONDICIONES/TASAS/TARIFA") ||
                            tipologia.equals("DEVOLUCIÓN DE DOCUMENTOS") || tipologia.equals("DUPLICADO DE DOCUMENTO") ||
                            tipologia.equals("PAGO ANTICIPADO Y ADELANTO DE CUOTAS") || tipologia.equals("CUOTA INICIAL HASTA 25% AFP") ||
                            tipologia.equals("EMISIÓN DE PRE CONFORMIDAD AFP 25%") || tipologia.equals("INFORMACION DE MOVIMIENTOS") ||
                            tipologia.equals("LIQUIDACIÓN DE DEUDA") || tipologia.equals("CAMBIO DE FORMA DE ENVÍO DE CORRESPONDENCIA") ||
                            tipologia.equals("CUOTA FLEXIBLE") || tipologia.equals("REPORTE INDEBIDO CENTRALES DE RIESGO") ||
                            tipologia.equals("REPROGRAMACIÓN DE DEUDA") || tipologia.equals("ENDOSO SEGUROS DE DESGRAVAMEN") ||
                            tipologia.equals("MEJORA DE CONDICIONES CHIP- RET") || tipologia.equals("SOLICITUD TRAMITACIÓN DE DOCUMENTOS")
                            || tipologia.equals("LEVANTAMIENTO DE GARANTÍA")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                    }

                    if (tipologia.equals("EMISIÓN PROACTIVA DE LEVANTAMIENTO DE GARANTÍA")) {
                        System.out.println("no tienen opciones carta");
                    }

                    if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA") || tipologia.equals("COBROS INDEBIDOS") ||
                            tipologia.equals("CONSUMO/ RETIRO NO EFECTUADO") || tipologia.equals("EXONERACIÓN DE COBROS")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                        //TODO: Acepto que el abono lo realizaré manualmente
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagAbonoManual-']")).click();
                    }

                }

                if (tipoProducto.equals("PRÉSTAMO PERSONAL")) {

                    if (tipologia.equals("BAJA DE TASA") || tipologia.equals("DUPLICADO DE CRONOGRAMA") ||
                            tipologia.equals("INFORMACION DE MOVIMIENTOS") || tipologia.equals("MODIFICACIÓN CONFIGURACIÓN CRÉDITO") ||
                            tipologia.equals("DESACUERDO DE CONDICIONES/TASAS/TARIFA") || tipologia.equals("REPORTE INDEBIDO CENTRALES DE RIESGO") ||
                            tipologia.equals("ANULACIÓN DE CRÉDITO") || tipologia.equals("DEVOLUCIÓN DE DOCUMENTOS") ||
                            tipologia.equals("DUPLICADO DE DOCUMENTO") || tipologia.equals("MODIFICACIÓN CONDICIONES CUOTAS CRÉDITO") ||
                            tipologia.equals("PAGO ANTICIPADO Y ADELANTO DE CUOTAS") || tipologia.equals("CAMBIO DE FORMA DE ENVÍO DE CORRESPONDENCIA") ||
                            tipologia.equals("CUOTA FLEXIBLE") || tipologia.equals("NO RECEPCIÓN DE DOCUMENTO") ||
                            tipologia.equals("ENDOSO SEGUROS DE DESGRAVAMEN") || tipologia.equals("REPROGRAMACIÓN DE DEUDA CON CAMPAÑA") ||
                            tipologia.equals("REPROGRAMACIÓN DE DEUDA SIN CAMPAÑA")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                    }

                    if (tipologia.equals("EXONERACIÓN DE COBROS") || tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA") ||
                            tipologia.equals("COBROS INDEBIDOS") || tipologia.equals("CONSUMO/ RETIRO NO EFECTUADO")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                        //TODO: Acepto que el abono lo realizaré manualmente
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagAbonoManual-']")).click();
                    }

                }

                if (tipoProducto.equals("REFINANCIADO")) {

                    if (tipologia.equals("BAJA DE TASA") || tipologia.equals("DUPLICADO DE CRONOGRAMA") ||
                            tipologia.equals("INFORMACION DE MOVIMIENTOS") || tipologia.equals("MODIFICACIÓN CONFIGURACIÓN CRÉDITO") ||
                            tipologia.equals("DESACUERDO DE CONDICIONES/TASAS/TARIFA") || tipologia.equals("REPORTE INDEBIDO CENTRALES DE RIESGO") ||
                            tipologia.equals("ANULACIÓN DE CRÉDITO") || tipologia.equals("DEVOLUCIÓN DE DOCUMENTOS") ||
                            tipologia.equals("DUPLICADO DE DOCUMENTO") || tipologia.equals("MODIFICACIÓN CONDICIONES CUOTAS CRÉDITO") ||
                            tipologia.equals("ENDOSO SEGUROS DE DESGRAVAMEN") || tipologia.equals("CAMBIO DE FORMA DE ENVÍO DE CORRESPONDENCIA") ||
                            tipologia.equals("CUOTA FLEXIBLE") || tipologia.equals("PAGO ANTICIPADO Y ADELANTO DE CUOTAS") ||
                            tipologia.equals("NO RECEPCIÓN DE DOCUMENTO") || tipologia.equals("REPROGRAMACIÓN DE DEUDA CON CAMPAÑA") ||
                            tipologia.equals("REPROGRAMACIÓN DE DEUDA SIN CAMPAÑA")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                    }

                    if (tipologia.equals("EXONERACIÓN DE COBROS") || tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA") ||
                            tipologia.equals("COBROS INDEBIDOS") || tipologia.equals("CONSUMO/ RETIRO NO EFECTUADO")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                        //TODO: Acepto que el abono lo realizaré manualmente
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagAbonoManual-']")).click();
                    }

                }

                if (tipoProducto.equals("EFECTIVO CON GARANTÍA HIPOTECARIO")) {

                    if (tipologia.equals("BAJA DE TASA") || tipologia.equals("DUPLICADO DE CRONOGRAMA") ||
                            tipologia.equals("INFORMACION DE MOVIMIENTOS") || tipologia.equals("MODIFICACIÓN CONFIGURACIÓN CRÉDITO") ||
                            tipologia.equals("DESACUERDO DE CONDICIONES/TASAS/TARIFA") || tipologia.equals("REPORTE INDEBIDO CENTRALES DE RIESGO") ||
                            tipologia.equals("DEVOLUCIÓN DE DOCUMENTOS") || tipologia.equals("DUPLICADO DE DOCUMENTO") ||
                            tipologia.equals("MODIFICACIÓN CONDICIONES CUOTAS CRÉDITO") || tipologia.equals("ENDOSO SEGUROS DE DESGRAVAMEN") ||
                            tipologia.equals("CAMBIO DE FORMA DE ENVÍO DE CORRESPONDENCIA") || tipologia.equals("CUOTA FLEXIBLE") ||
                            tipologia.equals("PAGO ANTICIPADO Y ADELANTO DE CUOTAS") || tipologia.equals("NO RECEPCIÓN DE DOCUMENTO") ||
                            tipologia.equals("EMISIÓN DE PRE CONFORMIDAD AFP 25%") || tipologia.equals("LIQUIDACIÓN DE DEUDA") ||
                            tipologia.equals("MEJORA DE CONDICIONES CHIP- RET") || tipologia.equals("CUOTA INICIAL HASTA 25% AFP") ||
                            tipologia.equals("REPROGRAMACIÓN DE DEUDA") || tipologia.equals("SOLICITUD TRAMITACIÓN DE DOCUMENTOS")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                    }

                    if (tipologia.equals("TRÁMITES DOCUMENTARIOS POST VENTA")) {
                        System.out.println("La tipología no requiere carta ni otros datos adicionales");
                    }

                    if (tipologia.equals("EXONERACIÓN DE COBROS") || tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA") ||
                            tipologia.equals("COBROS INDEBIDOS") || tipologia.equals("CONSUMO/ RETIRO NO EFECTUADO")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                        //TODO: Acepto que el abono lo realizaré manualmente
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagAbonoManual-']")).click();
                    }

                }

                if (tipoProducto.equals("VEHICULAR")) {

                    if (tipologia.equals("BAJA DE TASA") || tipologia.equals("DUPLICADO DE CRONOGRAMA") ||
                            tipologia.equals("INFORMACION DE MOVIMIENTOS") || tipologia.equals("MODIFICACIÓN CONFIGURACIÓN CRÉDITO") ||
                            tipologia.equals("DESACUERDO DE CONDICIONES/TASAS/TARIFA") || tipologia.equals("REPORTE INDEBIDO CENTRALES DE RIESGO") ||
                            tipologia.equals("ANULACIÓN DE CRÉDITO") || tipologia.equals("DEVOLUCIÓN DE DOCUMENTOS") ||
                            tipologia.equals("DUPLICADO DE DOCUMENTO") || tipologia.equals("MODIFICACIÓN CONDICIONES CUOTAS CRÉDITO") ||
                            tipologia.equals("PAGO ANTICIPADO Y ADELANTO DE CUOTAS") || tipologia.equals("CAMBIO DE FORMA DE ENVÍO DE CORRESPONDENCIA") ||
                            tipologia.equals("CUOTA FLEXIBLE") || tipologia.equals("APLICACIÓN DE FONDOS POR SINIESTROS") ||
                            tipologia.equals("REPROGRAMACIÓN DE DEUDA") || tipologia.equals("ENDOSO SEGUROS DE DESGRAVAMEN") ||
                            tipologia.equals("NO RECEPCIÓN DE DOCUMENTO") || tipologia.equals("LEVANTAMIENTO DE GARANTÍA")
                    ) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                    }

                    if (tipologia.equals("EMISIÓN PROACTIVA DE LEVANTAMIENTO DE GARANTÍA")) {
                        System.out.println("No tiene opciones de carta");
                    }

                    if (tipologia.equals("EXONERACIÓN DE COBROS") || tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA") ||
                            tipologia.equals("COBROS INDEBIDOS") || tipologia.equals("CONSUMO/ RETIRO NO EFECTUADO")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                        //TODO: Acepto que el abono lo realizaré manualmente
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagAbonoManual-']")).click();
                    }

                }

                if (tipoProducto.equals("BILLETERA ELECTRÓNICA")) {

                    if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA") || tipologia.equals("INFORMACION DE MOVIMIENTOS") ||
                            tipologia.equals("CONSUMO/ RETIRO NO EFECTUADO")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                        //TODO: Acepto que el abono lo realizaré manualmente
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagAbonoManual-']")).click();
                    }

                }

                if (tipoProducto.equals("TOKEN")) {

                    if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                    }

                }

                if (tipoProducto.equals("TUNKI")) {

                    if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA") || tipologia.equals("ATM DE IB NO ENTREGÓ DINERO")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                        //TODO: Acepto que el abono lo realizaré manualmente
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagAbonoManual-']")).click();
                    }

                }

                if (tipoProducto.equals("PAY PAL")) {

                    if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                        //TODO: Acepto que el abono lo realizaré manualmente
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagAbonoManual-']")).click();
                    }

                }

                if (tipoProducto.equals("TRANSF. INTERNACIONALES")) {

                    if (tipologia.equals("CONSTANCIA POR TRANSFERENCIAS RECIBIDAS") || tipologia.equals("INFORMACION DE MOVIMIENTOS") ||
                            tipologia.equals("NO RECEPCIÓN DE TRANSFERENCIA IN/OUT")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                    }

                    if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA") || tipologia.equals("COBROS INDEBIDOS")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                        //TODO: Acepto que el abono lo realizaré manualmente
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagAbonoManual-']")).click();
                    }

                }

                if (tipoProducto.equals("IB AGENTE")) {

                    if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA") || tipologia.equals("COBROS INDEBIDOS")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                        //TODO: Acepto que el abono lo realizaré manualmente
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagAbonoManual-']")).click();
                    }

                }

                if (tipoProducto.equals("PLIN")) {

                    if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA") || tipologia.equals("ATM DE IB NO ENTREGÓ DINERO")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                        //TODO: Acepto que el abono lo realizaré manualmente
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagAbonoManual-']")).click();
                    }

                }

                if (tipoProducto.equals("BANCA POR INTERNET")) {

                    if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                        //TODO: Acepto que el abono lo realizaré manualmente
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagAbonoManual-']")).click();
                    }

                }

                if (tipoProducto.equals("CBME")) {

                    if (tipologia.equals("COPIA DE BOLETA DE DEPÓSITO LIMA")) {
                        //TODO: Si adjunto Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                    }

                }

                if (tipoProducto.equals("CUENTA JUBILACIÓN")) {

                    if (tipologia.equals("DEPÓSITO A PLAZO - JUBILACIÓN")) {
                        //TODO: Si adjunto Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                    }

                }

                if (tipoProducto.equals("CUENTA CORRIENTE")) {

                    if (tipologia.equals("COPIA DE BOLETA DE DEPÓSITO LIMA") || tipologia.equals("REPORTE INDEBIDO CENTRALES DE RIESGO")) {
                        //TODO: Si adjunto Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                    }

                }

                if (tipoProducto.equals("SANEAMIENTO PROY. INMB")) {

                    if (tipologia.equals("LEVANTAMIENTO DE HIPOTECA MATRIZ") || tipologia.equals("SANEAMIENTO HIPOTECAS IBK")) {
                        //TODO: Si adjunto Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                    }

                }

                if (tipoProducto.equals("RETIRO SIN TARJETA")) {

                    if (tipologia.equals("ATENCIONES BOGOTÁ")) {
                        //TODO: Si adjunto Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                    }

                }

                if (tipoProducto.equals("PAGOS PLANILLAS/PROVEEDORES")) {

                    if (tipologia.equals("INFORMACION DE MOVIMIENTOS") || tipologia.equals("ACTIVACIÓN DE ORDENES DE PAGO")) {
                        //TODO: Si adjunto Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                    }

                    if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA") || tipologia.equals("COBROS INDEBIDOS")) {
                        //TODO: Si adjunto Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                        //TODO: Acepto que el abono lo realizaré manualmente
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagAbonoManual-']")).click();
                    }

                }

                if (tipoProducto.equals("PODERES")) {

                    if (tipologia.equals("SOLICITUD DE OP. DE PERSONAS SIN DISCERNIMIENTO")) {
                        //TODO: Si adjunto Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                    }

                }

                if (tipoProducto.equals("SEGUROS RELACIONADOS")) {

                    if (tipologia.equals("ENDOSO SEGUROS DEL BIEN") || tipologia.equals("APLICACIÓN DE FONDOS POR SINIESTROS/DESGRAVAMEN") ||
                            tipologia.equals("SOLICITUD DE INFORMACIÓN DE SINIESTROS") || tipologia.equals("LEVANTAMIENTO DE ENDOSO") ||
                            tipologia.equals("DUPLICADO DE DOCUMENTO") || tipologia.equals("INFORMACION DE MOVIMIENTOS")) {
                        //TODO: Si adjunto Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                    }

                    if (tipologia.equals("COBROS INDEBIDOS")) {
                        //TODO: Si adjunto Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                        //TODO: Acepto que el abono lo realizaré manualmente
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagAbonoManual-']")).click();
                    }

                }

                if (tipoProducto.equals("CARTA FIANZA/CERTIFICADO")) {

                    if (tipologia.equals("LIBERACIÓN CARTA FIANZA Y CERTIFICADO BANCARIO")) {
                        System.out.println("La tipología no requiere carta ni otros datos adicionales");
                    }

                    if (tipologia.equals("DUPLICADO DE DOCUMENTO")) {
                        //TODO: Si adjunto Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                    }

                }

                if (tipoProducto.equals("RAPPIBANK - TARJETA DE CRÉDITO")) {

                    if (tipologia.equals("CORREO/TELÉFONO INDEBIDO DE CAMPAÑAS") || tipologia.equals("DESACUERDO DE CONDICIONES/TASAS/TARIFAS") ||
                            tipologia.equals("DIRECCIÓN/TELF. INDEBIDOS DE COBRANZA") || tipologia.equals("INSATISFACCIÓN GESTIÓN DE ENTREGA DE TARJETA") ||
                            tipologia.equals("REPORTE INDEBIDO CENTRALES DE RIESGO") || tipologia.equals("NO RECEPCIÓN DE DOCUMENTO") ||
                            tipologia.equals("SISTEMA DE RECOMPENSAS")) {
                        //TODO: Si adjunto Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                    }

                    if (tipologia.equals("ATM DE IB NO ENTREGÓ DINERO") || tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA") ||
                            tipologia.equals("COBROS INDEBIDOS") || tipologia.equals("CONSUMO/ RETIRO NO EFECTUADO") ||
                            tipologia.equals("LIBERACIÓN DE RETENCIONES") || tipologia.equals("OPERACIÓN DENEGADA")) {
                        //TODO: Si adjunto Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                        //TODO: Acepto que el abono lo realizaré manualmente
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagAbonoManual-']")).click();
                    }

                }

                if (tipoProducto.equals("RAPPIBANK - CUENTAS")) {

                    if (tipologia.equals("CUENTA/CANCELACIÓN NO RECONOCIDA") || tipologia.equals("DESACUERDO CON EL SERVICIO") ||
                            tipologia.equals("DESACUERDO DE CONDICIONES/TASAS/TARIFAS") || tipologia.equals("INSATISFACCIÓN GESTIÓN DE ENTREGA DE TARJETA") ||
                            tipologia.equals("SISTEMA DE RECOMPENSAS")) {
                        //TODO: Si adjunto Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                    }

                    if (tipologia.equals("COBROS INDEBIDOS") || tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA") ||
                            tipologia.equals("CONSUMO/ RETIRO NO EFECTUADO") || tipologia.equals("LIBERACIÓN DE RETENCIONES") ||
                            tipologia.equals("OPERACIÓN DENEGADA") || tipologia.equals("REVISIÓN DE DEPÓSITOS A TERCEROS")) {
                        //TODO: Si adjunto Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                        //TODO: Acepto que el abono lo realizaré manualmente
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagAbonoManual-']")).click();
                    }

                }

                if (tipoProducto.equals("SUNAT")) {

                    if (tipologia.equals("CORRECCIÓN DE PAGO REALIZADO")) {
                        //TODO: Si adjunto Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                    }

                    if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA") || tipologia.equals("CONSTANCIA DE PAGO REALIZADO") ||
                            tipologia.equals("COBROS INDEBIDOS")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                        //TODO: Acepto que el abono lo realizaré manualmente
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagAbonoManual-']")).click();
                    }

                }

                if (tipoProducto.equals("SERVICIOS")) {

                    if (tipologia.equals("ATENCIÓN IBK AGENTE")) {
                        System.out.println("La tipología no requiere carta ni otros datos adicionales");
                    }

                }

                if (tipoProducto.equals("MONEY EXCHANGE APP")) {

                    if (tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                        //TODO: Acepto que el abono lo realizaré manualmente
                        Serenity.getDriver().findElement(By.xpath("//label[@for='flagAbonoManual-']")).click();
                    }

                }

                if (tipoProducto.equals("TRASLADOS CTS")) {

                    if (tipologia.equals("TRASLADO DE CTS FÍSICO A INTERBANK") || tipologia.equals("TRASLADO DE CTS DIGITAL A INTERBANK")) {
                        //TODO: No Adjunta Carta
                        resolucionSeleccionaNoAdjuntarCarta.resolucionNoAdjuntarCarta();
                    }

                }


                //TODO: SELECCIONA BUTTON - CONFIRMAR RESOLUCION
                WebElement btnConfirmar = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(15))
                        .until(ExpectedConditions.elementToBeClickable(By.id("add-email__btn-add")));
                btnConfirmar.click();

                Serenity.takeScreenshot();

                //TODO: SE ESPERA A PROCESAR TRAMITE
                try {
                    Thread.sleep(10000);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }

            } else {

                //TODO: SELECCIONA BUTTON - CONFIRMAR - PROCEDE VALIDACIÓN
                WebElement btnConfirmar = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(30))
                        .until(ExpectedConditions.elementToBeClickable(By.id("add-email__btn-add")));
                btnConfirmar.click();

                Serenity.takeScreenshot();

                //TODO: SE ESPERA A PROCESAR TRAMITE
                try {
                    Thread.sleep(30000);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }

            }


        } else {
            resolucionSiEnviaraCartaCliente.performAs(actor);

        }
    }


}
