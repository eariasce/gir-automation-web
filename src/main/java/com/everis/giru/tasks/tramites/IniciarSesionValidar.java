package com.everis.giru.tasks.tramites;

import com.everis.giru.tasks.login.CerrarGiru;
import com.everis.giru.userinterface.login.LoginPage;
import com.everis.giru.utils.WriteToFile;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.openqa.selenium.By;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;
import static org.junit.Assert.fail;

public class IniciarSesionValidar implements Task {
    private final String user;
    private final String area;
    private final String perfil;

    public IniciarSesionValidar(String razon, String area,String perfil) {
        this.user = razon;
        this.area = area;
        this.perfil = perfil;
    }
    @Override
    public <T extends Actor> void performAs(T actor) {

        String resultado = "fallo";
        actor.attemptsTo(
                WaitUntil.the(LoginPage.INP_USUARIO, isVisible()).forNoMoreThan(30).seconds(),
                Enter.theValue(user).into(LoginPage.INP_USUARIO),
                Enter.theValue("Abc12345$").into(LoginPage.INP_PASSWORD)
        );
        actor.attemptsTo(
                WaitUntil.the(LoginPage.BTN_INICIAR_SESION, isVisible()).forNoMoreThan(30).seconds(),
                Click.on(LoginPage.BTN_INICIAR_SESION)
        );

        try {
            Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(6));
            if (Serenity.getDriver().findElements(By.xpath("//p[text()=' *EL ID O CONTRASEÑA NO COINCIDEN ']")).size() >0) {
                WriteToFile.writeLinesToFileValidaciones(user+","+area+","+perfil+","+resultado);
                System.out.println("ERROR! --> EL ID O CONTRASEÑA NO COINCIDEN ");
            }
            else
            {
                resultado = "ok";
                WriteToFile.writeLinesToFileValidaciones(user+","+area+","+perfil+","+resultado);
                Thread.sleep(3000);
                theActorInTheSpotlight().attemptsTo(
                        new CerrarGiru());

            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }


    }

    public static Performable withData(String razon, String area, String perfil) {
        return instrumented(IniciarSesionValidar.class, razon, area, perfil);
    }

}
