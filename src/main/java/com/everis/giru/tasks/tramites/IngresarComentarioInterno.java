package com.everis.giru.tasks.tramites;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;

import java.time.Duration;
import java.util.Objects;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class IngresarComentarioInterno implements Task {
    private final String comentarioInterno;

    public IngresarComentarioInterno(String comentarioInterno) {
        this.comentarioInterno = comentarioInterno;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        String estoyEnResolutoria = Serenity.sessionVariableCalled("estoyEnResolutoria").toString();

        if (Objects.equals(estoyEnResolutoria, "Si")) {
            JavascriptExecutor js = (JavascriptExecutor) Serenity.getDriver();
            js.executeScript("arguments[0].scrollIntoView();", Serenity.getDriver().findElement(By.id("areaComments")));
            //TODO: TextArea Comentario Interno Procede
            Serenity.getDriver().findElement(By.id("areaComments")).sendKeys(comentarioInterno);

        } else {
            Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(1));
            boolean comentarioInternoValidacion = Serenity.getDriver().findElements(By.id("validationComments")).size() > 0;
            if (comentarioInternoValidacion) {
                JavascriptExecutor js = (JavascriptExecutor) Serenity.getDriver();
                js.executeScript("arguments[0].scrollIntoView();", Serenity.getDriver().findElement(By.id("validationComments")));

                //TODO: TextArea Comentario Validacion
                Serenity.getDriver().findElement(By.id("validationComments")).sendKeys(comentarioInterno);
            } else {
                JavascriptExecutor js = (JavascriptExecutor) Serenity.getDriver();
                js.executeScript("arguments[0].scrollIntoView();", Serenity.getDriver().findElement(By.id("areaComments")));

                //TODO: TextArea Comentario Validacion
                Serenity.getDriver().findElement(By.id("areaComments")).sendKeys(comentarioInterno);
            }
        }
    }

    public static Performable withData(String comentarioInterno) {
        return instrumented(IngresarComentarioInterno.class, comentarioInterno);
    }

}


