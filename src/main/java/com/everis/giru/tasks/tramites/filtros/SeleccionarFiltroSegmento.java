package com.everis.giru.tasks.tramites.filtros;

import com.everis.giru.userinterface.tramites.FiltrosBandejaPage;
import com.everis.giru.userinterface.tramites.filtrosPage.GetFiltroSegmentoPage;
import com.everis.giru.userinterface.tramites.validaFiltros.ValidaFiltroSegmentoPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class SeleccionarFiltroSegmento implements Task {

    private final String departamento;
    private final String perfil;
    private final String tipologia;
    private final String fSegmento;

    GetFiltroSegmentoPage getFiltroSegmentoPage;
    ValidaFiltroSegmentoPage validaFiltroSegmentoPage;

    public SeleccionarFiltroSegmento(String departamento, String perfil, String tipologia, String fSegmento) {
        this.departamento = departamento;
        this.perfil = perfil;
        this.tipologia = tipologia;
        this.fSegmento = fSegmento;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        if (!(fSegmento.equals("No"))) {
            actor.attemptsTo(
                    WaitUntil.the(FiltrosBandejaPage.BUTTOM_FILTRO_SEGMENTO, isVisible()).forNoMoreThan(100).seconds(),
                    Click.on(FiltrosBandejaPage.BUTTOM_FILTRO_SEGMENTO)
            );

            getFiltroSegmentoPage.seleccionaFiltroSegmento(fSegmento);

            actor.attemptsTo(
                    WaitUntil.the(FiltrosBandejaPage.BUTTOM_FILTRO_APLICAR, isVisible()).forNoMoreThan(100).seconds(),
                    Click.on(FiltrosBandejaPage.BUTTOM_FILTRO_APLICAR)
            );

            validaFiltroSegmentoPage.validaFiltroSegmento(departamento, perfil, tipologia, fSegmento);

        }

    }

    public static Performable withData(String departamento, String perfil, String tipologia, String fSegmento) {
        return instrumented(SeleccionarFiltroSegmento.class, departamento, perfil, tipologia, fSegmento);
    }

}
