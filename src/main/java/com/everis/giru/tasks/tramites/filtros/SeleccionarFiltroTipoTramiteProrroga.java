package com.everis.giru.tasks.tramites.filtros;

import com.everis.giru.userinterface.tramites.FiltrosBandejaPage;
import com.everis.giru.userinterface.tramites.filtrosPage.GetFiltroTipoTramitePage;
import com.everis.giru.userinterface.tramites.validaFiltros.ValidaFiltroTipoTramitePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class SeleccionarFiltroTipoTramiteProrroga implements Task {

    private final String departamento;
    private final String perfil;
    private final String tipologia;
    private final String fTipoTramite;

    GetFiltroTipoTramitePage getFiltroTipoTramitePage;


    public SeleccionarFiltroTipoTramiteProrroga(String departamento, String perfil, String tipologia, String fTipoTramite) {
        this.departamento = departamento;
        this.perfil = perfil;
        this.tipologia = tipologia;
        this.fTipoTramite = fTipoTramite;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        if (!(fTipoTramite.equals("No"))) {
            actor.attemptsTo(
                    WaitUntil.the(FiltrosBandejaPage.BUTTOM_FILTRO_TIPO_TRAMITE, isVisible()).forNoMoreThan(100).seconds(),
                    Click.on(FiltrosBandejaPage.BUTTOM_FILTRO_TIPO_TRAMITE)
            );

            getFiltroTipoTramitePage.seleccionaFiltroTipoTramite(fTipoTramite, perfil, departamento);

            actor.attemptsTo(
                    WaitUntil.the(FiltrosBandejaPage.BUTTOM_FILTRO_APLICAR, isVisible()).forNoMoreThan(100).seconds(),
                    Click.on(FiltrosBandejaPage.BUTTOM_FILTRO_APLICAR)
            );



        }

    }

    public static Performable withData(String departamento, String perfil, String tipologia, String fTipoTramite) {
        return instrumented(SeleccionarFiltroTipoTramiteProrroga.class, departamento, perfil, tipologia, fTipoTramite);
    }

}
