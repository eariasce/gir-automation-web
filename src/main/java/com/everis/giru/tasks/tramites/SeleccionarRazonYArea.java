package com.everis.giru.tasks.tramites;

import com.everis.giru.userinterface.tramites.ResolucionTramitePage;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.targets.Target;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.openqa.selenium.By;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class SeleccionarRazonYArea implements Task {
    private final String razon;
    private final String area;

    public SeleccionarRazonYArea(String razon, String area) {
        this.razon = razon;
        this.area = area;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        switch (razon) {
            case "NecesitaValidacionDeAfilacion":
                actor.attemptsTo(WaitUntil.the(ResolucionTramitePage.CBX_SELECCIONAR_RAZON, isVisible()).forNoMoreThan(100).seconds(),
                        Click.on(ResolucionTramitePage.CBX_SELECCIONAR_RAZON),
                        Click.on(Target.the("Seleccionas la razon del tramite ").located(By.id("R0001"))));
                switch (area) {
                    case "Dpto-Seguros":
                        actor.attemptsTo(WaitUntil.the(ResolucionTramitePage.CBX_SELECCIONAR_AREA, isVisible()).forNoMoreThan(100).seconds(),
                                Click.on(ResolucionTramitePage.CBX_SELECCIONAR_AREA),
                                Click.on(Target.the("Seleccionas Dpto-Seguros ").located(By.id("000006"))));
                        break;
                    case "EECC-TC":
                        actor.attemptsTo(WaitUntil.the(ResolucionTramitePage.CBX_SELECCIONAR_AREA, isVisible()).forNoMoreThan(100).seconds(),
                                Click.on(ResolucionTramitePage.CBX_SELECCIONAR_AREA),
                                Click.on(Target.the("Seleccionas EECC-TC ").located(By.id("000014"))));
                        break;
                    case "CustodiaDeDocumentos":
                        actor.attemptsTo(WaitUntil.the(ResolucionTramitePage.CBX_SELECCIONAR_AREA, isVisible()).forNoMoreThan(100).seconds(),
                                Click.on(ResolucionTramitePage.CBX_SELECCIONAR_AREA),
                                Click.on(Target.the("Seleccionas Custodia de documentos ").located(By.id("000015"))));
                        break;
                    case "Prod.YDistribucionTC":
                        actor.attemptsTo(WaitUntil.the(ResolucionTramitePage.CBX_SELECCIONAR_AREA, isVisible()).forNoMoreThan(100).seconds(),
                                Click.on(ResolucionTramitePage.CBX_SELECCIONAR_AREA),
                                Click.on(Target.the("Seleccionas Prod. Y Distribución TC ").located(By.id("000016"))));
                        break;
                    case "MensajeriaIE":
                        actor.attemptsTo(WaitUntil.the(ResolucionTramitePage.CBX_SELECCIONAR_AREA, isVisible()).forNoMoreThan(100).seconds(),
                                Click.on(ResolucionTramitePage.CBX_SELECCIONAR_AREA),
                                Click.on(Target.the("Seleccionas Mensajeria IE ").located(By.id("000017"))));
                        break;
                }
                break;
            case "NecesitaInformacionDeDocumentos":
                actor.attemptsTo(WaitUntil.the(ResolucionTramitePage.CBX_SELECCIONAR_RAZON, isVisible()).forNoMoreThan(100).seconds(),
                        Click.on(ResolucionTramitePage.CBX_SELECCIONAR_RAZON),
                        Click.on(Target.the("Seleccionas la razon: Necesitas información de documentos  ").located(By.id("R0002"))));
                switch (area) {

                    case "Gestión De Pedidos Y Reclamos TC - GPYR-Seguros":
                        actor.attemptsTo(WaitUntil.the(ResolucionTramitePage.CBX_SELECCIONAR_AREA, isVisible()).forNoMoreThan(100).seconds(),
                                Click.on(ResolucionTramitePage.CBX_SELECCIONAR_AREA),
                                Click.on(Target.the("Seleccionas estión De Pedidos Y Reclamos TC - GPYR-Seguros").located(By.id("000009-1"))));
                        break;

                    case "Gestión De Pedidos Y Reclamos TC":
                        actor.attemptsTo(WaitUntil.the(ResolucionTramitePage.CBX_SELECCIONAR_AREA, isVisible()).forNoMoreThan(100).seconds(),
                                Click.on(ResolucionTramitePage.CBX_SELECCIONAR_AREA),
                                Click.on(Target.the("Seleccionas Gestión De Pedidos Y Reclamos TC").located(By.id("000009"))));
                        break;
                    case "Gestión De Reclamos Regulatorios":
                        actor.attemptsTo(WaitUntil.the(ResolucionTramitePage.CBX_SELECCIONAR_AREA, isVisible()).forNoMoreThan(100).seconds(),
                                Click.on(ResolucionTramitePage.CBX_SELECCIONAR_AREA),
                                Click.on(Target.the("Seleccionas Proceso de Captaciones").located(By.id("000010"))));
                        break;
                    case "U-Segto-Pedidos Recl":
                        actor.attemptsTo(WaitUntil.the(ResolucionTramitePage.CBX_SELECCIONAR_AREA, isVisible()).forNoMoreThan(100).seconds(),
                                Click.on(ResolucionTramitePage.CBX_SELECCIONAR_AREA),
                                Click.on(Target.the("Seleccionas Proceso de Captaciones").located(By.id("000011"))));
                        break;
                    case "Controversias":
                        actor.attemptsTo(WaitUntil.the(ResolucionTramitePage.CBX_SELECCIONAR_AREA, isVisible()).forNoMoreThan(100).seconds(),
                                Click.on(ResolucionTramitePage.CBX_SELECCIONAR_AREA),
                                Click.on(Target.the("Seleccionas Proceso de Captaciones").located(By.id("000018"))));
                        break;
                    case "Retenciones Judiciales":
                        actor.attemptsTo(WaitUntil.the(ResolucionTramitePage.CBX_SELECCIONAR_AREA, isVisible()).forNoMoreThan(100).seconds(),
                                Click.on(ResolucionTramitePage.CBX_SELECCIONAR_AREA),
                                Click.on(Target.the("Seleccionas Proceso de Captaciones").located(By.id("000024"))));
                        break;
                    case "Hipotecario Post Venta":
                        actor.attemptsTo(WaitUntil.the(ResolucionTramitePage.CBX_SELECCIONAR_AREA, isVisible()).forNoMoreThan(100).seconds(),
                                Click.on(ResolucionTramitePage.CBX_SELECCIONAR_AREA),
                                Click.on(Target.the("Seleccionas Proceso de Captaciones").located(By.id("000030"))));
                        break;
                    case "Gestión de la Información COB":
                        actor.attemptsTo(WaitUntil.the(ResolucionTramitePage.CBX_SELECCIONAR_AREA, isVisible()).forNoMoreThan(100).seconds(),
                                Click.on(ResolucionTramitePage.CBX_SELECCIONAR_AREA),
                                Click.on(Target.the("Seleccionas Proceso de Captaciones").located(By.id("000042"))));
                        break;
                    case "Seguros Relacionados":
                        actor.attemptsTo(WaitUntil.the(ResolucionTramitePage.CBX_SELECCIONAR_AREA, isVisible()).forNoMoreThan(100).seconds(),
                                Click.on(ResolucionTramitePage.CBX_SELECCIONAR_AREA),
                                Click.on(Target.the("Seleccionas Proceso de Captaciones").located(By.id("000048"))));
                        break;
                    case "Nóminas":
                        actor.attemptsTo(WaitUntil.the(ResolucionTramitePage.CBX_SELECCIONAR_AREA, isVisible()).forNoMoreThan(100).seconds(),
                                Click.on(ResolucionTramitePage.CBX_SELECCIONAR_AREA),
                                Click.on(Target.the("Seleccionas Proceso de Captaciones").located(By.id("000055"))));
                        break;
                    case "Convenios Post Venta":
                        actor.attemptsTo(WaitUntil.the(ResolucionTramitePage.CBX_SELECCIONAR_AREA, isVisible()).forNoMoreThan(100).seconds(),
                                Click.on(ResolucionTramitePage.CBX_SELECCIONAR_AREA),
                                Click.on(Target.the("Seleccionas Proceso de Captaciones").located(By.id("000056"))));
                        break;
                    case "Procesos de Captaciones":
                        actor.attemptsTo(WaitUntil.the(ResolucionTramitePage.CBX_SELECCIONAR_AREA, isVisible()).forNoMoreThan(100).seconds(),
                                Click.on(ResolucionTramitePage.CBX_SELECCIONAR_AREA),
                                Click.on(Target.the("Seleccionas Proceso de Captaciones").located(By.id("000023"))));
                        break;
                    case "Legal":
                        actor.attemptsTo(WaitUntil.the(ResolucionTramitePage.CBX_SELECCIONAR_AREA, isVisible()).forNoMoreThan(100).seconds(),
                                Click.on(ResolucionTramitePage.CBX_SELECCIONAR_AREA),
                                Click.on(Target.the("Seleccionas Proceso de Captaciones").located(By.id("000058"))));
                        break;
                    case "Dpto-Seguros":
                        actor.attemptsTo(WaitUntil.the(ResolucionTramitePage.CBX_SELECCIONAR_AREA, isVisible()).forNoMoreThan(100).seconds(),
                                Click.on(ResolucionTramitePage.CBX_SELECCIONAR_AREA),
                                Click.on(Target.the("Seleccionas Dpto-Seguros ").located(By.id("000006"))));
                        break;
                    case "EECC-TC":
                        actor.attemptsTo(WaitUntil.the(ResolucionTramitePage.CBX_SELECCIONAR_AREA, isVisible()).forNoMoreThan(100).seconds(),
                                Click.on(ResolucionTramitePage.CBX_SELECCIONAR_AREA),
                                Click.on(Target.the("Seleccionas EECC-TC").located(By.id("000014"))));
                        break;
                    case "CustodiaDeDocumentos":
                        actor.attemptsTo(WaitUntil.the(ResolucionTramitePage.CBX_SELECCIONAR_AREA, isVisible()).forNoMoreThan(100).seconds(),
                                Click.on(ResolucionTramitePage.CBX_SELECCIONAR_AREA),
                                Click.on(Target.the("Seleccionas Custodia de Documentos ").located(By.id("000015"))));
                        break;
                    case "Prod.YDistribucionTC":
                        actor.attemptsTo(WaitUntil.the(ResolucionTramitePage.CBX_SELECCIONAR_AREA, isVisible()).forNoMoreThan(100).seconds(),
                                Click.on(ResolucionTramitePage.CBX_SELECCIONAR_AREA),
                                Click.on(Target.the("Seleccionas Prod. Y Distribución ").located(By.id("000016"))));
                        break;
                    case "MensajeriaIE":
                        actor.attemptsTo(WaitUntil.the(ResolucionTramitePage.CBX_SELECCIONAR_AREA, isVisible()).forNoMoreThan(100).seconds(),
                                Click.on(ResolucionTramitePage.CBX_SELECCIONAR_AREA),
                                Click.on(Target.the("Seleccionas mensajería IE ").located(By.id("000017"))));
                        break;
                    case "Poderes PJ":
                        actor.attemptsTo(WaitUntil.the(ResolucionTramitePage.CBX_SELECCIONAR_AREA, isVisible()).forNoMoreThan(100).seconds(),
                                Click.on(ResolucionTramitePage.CBX_SELECCIONAR_AREA));
                                //Click.on(Target.the("Seleccionas Poderes PJ ").located(By.id("000035-2")
                                Serenity.getDriver().findElement(By.xpath("//*[contains(text(), '"+area+"')]")).click();
                        break;
                }
                break;
            case "RequiereSerCerradoPorOtraAreaResolutora":
                System.out.println("aqui");
                actor.attemptsTo(WaitUntil.the(ResolucionTramitePage.CBX_SELECCIONAR_RAZON, isVisible()).forNoMoreThan(100).seconds(),
                        Click.on(ResolucionTramitePage.CBX_SELECCIONAR_RAZON),
                        Click.on(Target.the("Seleccionas la razon: Requiere ser Retipificado  ").located(By.id("R0008"))));
//                switch (area) {
//                    case "U-Segto-Pedidos Recl":
//                        actor.attemptsTo(WaitUntil.the(ResolucionTramitePage.CBX_SELECCIONAR_AREA, isVisible()).forNoMoreThan(100).seconds(),
//                                Click.on(ResolucionTramitePage.CBX_SELECCIONAR_AREA),
//                                Click.on(Target.the("Procesos de Captaciones").located(By.id("undefined"))));
//                        break;
//                }
                break;
            case "NecesitaSerAnalizadoSegundoEspecialistaEnMiArea":
                System.out.println("aqui");
                actor.attemptsTo(WaitUntil.the(ResolucionTramitePage.CBX_SELECCIONAR_RAZON, isVisible()).forNoMoreThan(100).seconds(),
                        Click.on(ResolucionTramitePage.CBX_SELECCIONAR_RAZON),
                        Click.on(Target.the("Seleccionas la razon: Requiere ser Retipificado  ").located(By.id("R0007"))));
                switch (area) {
                    case "Procesos de Captaciones":
                        actor.attemptsTo(WaitUntil.the(ResolucionTramitePage.CBX_SELECCIONAR_AREA, isVisible()).forNoMoreThan(100).seconds(),
                                Click.on(ResolucionTramitePage.CBX_SELECCIONAR_AREA),
                                Click.on(Target.the("Procesos de Captaciones").located(By.id("000023"))));
                        break;
                    case "Controversias":
                        System.out.println("Controversias seleccion");
                        // actor.attemptsTo(WaitUntil.the(ResolucionTramitePage.CBX_SELECCIONAR_AREA, isVisible()).forNoMoreThan(100).seconds(),
                        //          Click.on(ResolucionTramitePage.CBX_SELECCIONAR_AREA),
                        //          Click.on(Target.the("Seleccionas Controversias").located(By.id("000018"))));
                        break;
                }
                break;
            case "RequiereSerDevueltoAreaAnterior":
                actor.attemptsTo(WaitUntil.the(ResolucionTramitePage.CBX_SELECCIONAR_RAZON, isVisible()).forNoMoreThan(100).seconds(),
                        Click.on(ResolucionTramitePage.CBX_SELECCIONAR_RAZON),
                        Click.on(Target.the("Seleccionas la razon: Requiere ser devuelt a area anterior  ").located(By.id("R0006"))));
                switch (area) {
                    case "Procesos de Captaciones":
                        actor.attemptsTo(WaitUntil.the(ResolucionTramitePage.CBX_SELECCIONAR_AREA, isVisible()).forNoMoreThan(100).seconds(),
                                Click.on(ResolucionTramitePage.CBX_SELECCIONAR_AREA),
                                Click.on(Target.the("Procesos de Captaciones").located(By.id("000023"))));
                        break;
                    case "Controversias":
                        actor.attemptsTo(WaitUntil.the(ResolucionTramitePage.CBX_SELECCIONAR_AREA, isVisible()).forNoMoreThan(100).seconds(),
                                Click.on(ResolucionTramitePage.CBX_SELECCIONAR_AREA),
                                Click.on(Target.the("Seleccionas Controversias").located(By.id("000018"))));
                        break;
                    case "U-Segto-Pedidos Recl":
                        actor.attemptsTo(WaitUntil.the(ResolucionTramitePage.CBX_SELECCIONAR_AREA, isVisible()).forNoMoreThan(100).seconds(),
                                Click.on(ResolucionTramitePage.CBX_SELECCIONAR_AREA),
                                Click.on(Target.the("Seleccionas U-Segto-Pedidos Recl").located(By.id("0"))));
                        break;
                    case "Gestión De Pedidos Y Reclamos TC":
                        actor.attemptsTo(WaitUntil.the(ResolucionTramitePage.CBX_SELECCIONAR_AREA, isVisible()).forNoMoreThan(100).seconds(),
                                Click.on(ResolucionTramitePage.CBX_SELECCIONAR_AREA),
                                Click.on(Target.the("Seleccionas U-Segto-Pedidos Recl").located(By.id("000009"))));
                        break;
                }
                break;
            case "RequiereSerRetipificado":
                actor.attemptsTo(WaitUntil.the(ResolucionTramitePage.CBX_SELECCIONAR_RAZON, isVisible()).forNoMoreThan(100).seconds(),
                        Click.on(ResolucionTramitePage.CBX_SELECCIONAR_RAZON),
                        Click.on(Target.the("Seleccionas la razon: Requiere ser Retipificado  ").located(By.id("R0004"))));
                break;
            case "NecesitaValidacionDeAfilaciondeAreaCorrespondiente":
                actor.attemptsTo(WaitUntil.the(ResolucionTramitePage.CBX_SELECCIONAR_RAZON, isVisible()).forNoMoreThan(100).seconds(),
                        Click.on(ResolucionTramitePage.CBX_SELECCIONAR_RAZON),
                        Click.on(Target.the("Seleccionas la razon del tramite ").located(By.id("R0003"))));
                switch (area) {
                    case "Dpto-Seguros":
                        actor.attemptsTo(WaitUntil.the(ResolucionTramitePage.CBX_SELECCIONAR_AREA, isVisible()).forNoMoreThan(100).seconds(),
                                Click.on(ResolucionTramitePage.CBX_SELECCIONAR_AREA),
                                Click.on(Target.the("Seleccionas Dpto-Seguros ").located(By.id("000006"))));
                        break;
                    case "EECC-TC":
                        actor.attemptsTo(WaitUntil.the(ResolucionTramitePage.CBX_SELECCIONAR_AREA, isVisible()).forNoMoreThan(100).seconds(),
                                Click.on(ResolucionTramitePage.CBX_SELECCIONAR_AREA),
                                Click.on(Target.the("Seleccionas EECC-TC ").located(By.id("000014"))));
                        break;
                    case "CustodiaDeDocumentos":
                        actor.attemptsTo(WaitUntil.the(ResolucionTramitePage.CBX_SELECCIONAR_AREA, isVisible()).forNoMoreThan(100).seconds(),
                                Click.on(ResolucionTramitePage.CBX_SELECCIONAR_AREA),
                                Click.on(Target.the("Seleccionas Custodia de documentos ").located(By.id("000015"))));
                        break;
                }
                break;

            case "RequiereSerRevisadoPorOtraAreaValidadora":
                actor.attemptsTo(WaitUntil.the(ResolucionTramitePage.CBX_SELECCIONAR_RAZON, isVisible()).forNoMoreThan(100).seconds(),
                        Click.on(ResolucionTramitePage.CBX_SELECCIONAR_RAZON),
                        Click.on(Target.the("Seleccionas la razon del tramite ").located(By.id("R0009"))));
                switch (area) {
                    case "GestiónDePedidosYReclamosTC":
                        actor.attemptsTo(WaitUntil.the(ResolucionTramitePage.CBX_SELECCIONAR_AREA, isVisible()).forNoMoreThan(100).seconds(),
                                Click.on(ResolucionTramitePage.CBX_SELECCIONAR_AREA),
                                Click.on(Target.the("Seleccionas Gestión De Pedidos Y Reclamos TC ").located(By.id("000009"))));
                        break;

                    case "Gestión De Pedidos Y Reclamos TC - GPYR-Seguros":
                        actor.attemptsTo(WaitUntil.the(ResolucionTramitePage.CBX_SELECCIONAR_AREA, isVisible()).forNoMoreThan(100).seconds(),
                                Click.on(ResolucionTramitePage.CBX_SELECCIONAR_AREA),
                                Click.on(Target.the("Seleccionas Gestión De Pedidos Y Reclamos TC ").located(By.id("000009-2"))));
                        break;

                    case "Procesos de Captaciones":
                        actor.attemptsTo(WaitUntil.the(ResolucionTramitePage.CBX_SELECCIONAR_AREA, isVisible()).forNoMoreThan(100).seconds(),
                                Click.on(ResolucionTramitePage.CBX_SELECCIONAR_AREA),
                                Click.on(Target.the("Seleccionas Proceso de Captaciones").located(By.id("000023"))));
                        break;
                    case "Poderes PJ":
                        actor.attemptsTo(WaitUntil.the(ResolucionTramitePage.CBX_SELECCIONAR_AREA, isVisible()).forNoMoreThan(100).seconds(),
                                Click.on(ResolucionTramitePage.CBX_SELECCIONAR_AREA));
//                                Click.on(Target.the("Seleccionas Poderes PJ").located(By.id("000035-2")
                              //  Click.on(Target.the("Seleccionas Poderes PJ").located(By.xpath("//*[contains(text(), ' Poderes PJ')]")
                                        Serenity.getDriver().findElement(By.xpath("//*[contains(text(), '"+area+"')]")).click();
                               // )));
                        break;

                }
                break;



        }

//        actor.attemptsTo(
//                WaitUntil.the(ResolucionTramitePage.CBX_SELECCIONAR_RAZON, isVisible()).forNoMoreThan(100).seconds(),
//                Click.on(ResolucionTramitePage.CBX_SELECCIONAR_RAZON),
//                Click.on(Target.the("Seleccionas la razón del tramite ").located(By.id("R000"+razon)))
//        );
//
//        actor.attemptsTo(
//                WaitUntil.the(ResolucionTramitePage.CBX_SELECCIONAR_AREA, isVisible()).forNoMoreThan(100).seconds(),
//                Click.on(ResolucionTramitePage.CBX_SELECCIONAR_AREA),
//                Click.on(Target.the("Seleccionas el área del tramite ").located(By.id("0000"+area)))
//        );


    }

    public static Performable withData(String razon, String area) {
        return instrumented(SeleccionarRazonYArea.class, razon, area);
    }
}

