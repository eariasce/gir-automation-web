package com.everis.giru.tasks.tramites;

import com.everis.giru.userinterface.tramites.TramitesPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class SeleccionarTipoTramites implements Task {

    private final String tipoTramite;
    private final String numeroTramite;

    public SeleccionarTipoTramites(String tipoTramite, String numeroTramite) {
        this.tipoTramite = tipoTramite;
        this.numeroTramite = numeroTramite;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(
                WaitUntil.the(TramitesPage.CBX_TIPO_DOC, isVisible()).forNoMoreThan(30).seconds(),
                Click.on(TramitesPage.CBX_TIPO_DOC)
        );

        switch (tipoTramite) {
            case "NRO TRAMITE":
                actor.attemptsTo(
                        Click.on(TramitesPage.DIV_SELECT_NRO_TRAMITE),
                        Enter.theValue(numeroTramite).into(TramitesPage.INP_NUMERO_DOC)

                );
                break;
            case "DNI":
                actor.attemptsTo(
                        Click.on(TramitesPage.DIV_SELECT_DNI),
                        Enter.theValue(numeroTramite).into(TramitesPage.INP_NUMERO_DOC)
                );
                break;
            case "CE":
                actor.attemptsTo(
                        Click.on(TramitesPage.DIV_SELECT_CE),
                        Enter.theValue(numeroTramite).into(TramitesPage.INP_NUMERO_DOC)
                );
                break;

            default:
                System.out.println("No se encuentra ningun tramite !!!");
        }

    }

    public static Performable withData(String tipoTramite, String numeroTramite) {
        return instrumented(SeleccionarTipoTramites.class, tipoTramite, numeroTramite);
    }

}
