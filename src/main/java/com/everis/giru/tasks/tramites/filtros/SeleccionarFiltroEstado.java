package com.everis.giru.tasks.tramites.filtros;

import com.everis.giru.userinterface.tramites.FiltrosBandejaPage;
import com.everis.giru.userinterface.tramites.filtrosPage.GetFiltroEstadoPage;
import com.everis.giru.userinterface.tramites.validaFiltros.ValidaFiltroEstadoPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class SeleccionarFiltroEstado implements Task {

    private final String departamento;
    private final String perfil;
    private final String tipologia;
    private final String fEstado;

    GetFiltroEstadoPage getFiltroEstadoPage;
    ValidaFiltroEstadoPage validaFiltroEstadoPage;

    public SeleccionarFiltroEstado(String departamento, String perfil, String tipologia, String fEstado) {
        this.departamento = departamento;
        this.perfil = perfil;
        this.tipologia = tipologia;
        this.fEstado = fEstado;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        if (!(fEstado.equals("No"))) {
            actor.attemptsTo(
                    WaitUntil.the(FiltrosBandejaPage.BUTTOM_FILTRO_ESTADO, isVisible()).forNoMoreThan(100).seconds(),
                    Click.on(FiltrosBandejaPage.BUTTOM_FILTRO_ESTADO)
            );

            getFiltroEstadoPage.seleccionaFiltroEstado(fEstado);

            actor.attemptsTo(
                    WaitUntil.the(FiltrosBandejaPage.BUTTOM_FILTRO_APLICAR, isVisible()).forNoMoreThan(100).seconds(),
                    Click.on(FiltrosBandejaPage.BUTTOM_FILTRO_APLICAR)
            );

            validaFiltroEstadoPage.validaFiltroEstado(departamento, perfil, tipologia, fEstado);

        }

    }

    public static Performable withData(String departamento, String perfil, String tipologia, String fEstado) {
        return instrumented(SeleccionarFiltroEstado.class, departamento, perfil, tipologia, fEstado);
    }

}
