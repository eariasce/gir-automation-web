package com.everis.giru.tasks.tramites;

import com.everis.giru.userinterface.tramites.contadorPage.GetContadorIzquierdoPage;
import com.everis.giru.userinterface.tramites.contadorPage.GetContadorDerechoPage;
import com.everis.giru.userinterface.tramites.paginacionPage.GetDropdownPaginacionPage;
import com.everis.giru.userinterface.tramites.paginacionPage.GetSeleccionarPaginacionPage;
import com.everis.giru.userinterface.tramites.paginacionPage.ValidaPaginacionPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static org.assertj.core.api.Assertions.assertThat;

public class ValidaPaginacion implements Task {

    private final String departamento;
    private final String perfil;
    private final String tipologia;
    private final String registros;

    GetDropdownPaginacionPage getDropdownPaginacionPage;
    GetSeleccionarPaginacionPage getSeleccionarPaginacionPage;
    ValidaPaginacionPage validaPaginacionPage;
    GetContadorDerechoPage getContadorDerechoPage;

    GetContadorIzquierdoPage getContadorIzquierdoPage = new GetContadorIzquierdoPage();

    public ValidaPaginacion(String departamento, String perfil, String tipologia, String registros) {
        this.departamento = departamento;
        this.perfil = perfil;
        this.tipologia = tipologia;
        this.registros = registros;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("") || getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("0")) {
            getContadorDerechoPage.EsCeroTotalTramite();
            if (tipologia.equals("TodosMisTramites") || tipologia.equals("MiEquipo") || tipologia.equals("Asignar") || tipologia.equals("Resolver") || tipologia.equals("Monitorear") || tipologia.equals("TransaccionesEnProceso") || tipologia.equals("Retipificar")) {
                if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("Ingresa una búsqueda para ver resultados")) {
                    assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                } else {
                    assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                }

            } else {

                if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("No se encontraron coincidencias")) {
                    assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                } else {
                    assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                }

            }

        } else {
            //TODO: seleccionar Dropdown Paginacion
            getDropdownPaginacionPage.seleccionarDropdownPaginacion(departamento, perfil, tipologia);
            //TODO: seleccionar Paginacion
            getSeleccionarPaginacionPage.seleccionarPaginacion(departamento, perfil, tipologia, registros);

            if (validaPaginacionPage.validaPaginacion(departamento, perfil, tipologia, registros)) {
                assertThat(registros).isEqualTo(registros);
            } else {
                assertThat(registros).isEqualTo(validaPaginacionPage.traerContador());
            }

        }

        getContadorIzquierdoPage.SetContadorANull();

    }

    public static Performable withData(String departamento, String perfil, String tipologia, String registros) {
        return instrumented(ValidaPaginacion.class, departamento, perfil, tipologia, registros);
    }

}
