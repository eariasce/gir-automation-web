package com.everis.giru.tasks.tramites;

import com.everis.giru.userinterface.login.LoginPage;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.openqa.selenium.By;

import java.util.concurrent.TimeUnit;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;
import static org.junit.Assert.fail;

public class IniciarSesionReasignado implements Task {

    String usuarioreasignado = Serenity.sessionVariableCalled("nroAsesorReasignado").toString();
    String ambiente = Serenity.sessionVariableCalled("ambiente");
    String tipoProducto = Serenity.sessionVariableCalled("tipoproducto").toString();
    String tipologia = Serenity.sessionVariableCalled("tipologia").toString();

    @Override
    public <T extends Actor> void performAs(T actor) {

        //TODO: TIPOLOGIA ATENCIÓN POR PRENVENCIÓN Y FRAUDES - TARJETA DE CREDITO - TARJETA DE DEBITO
        if (tipoProducto.equals("TARJETA DE CREDITO") || tipoProducto.equals("TARJETA DE DEBITO")) {

            if (tipologia.equals("ATENCIÓN POR PRENVENCIÓN Y FRAUDES")) {
                //TODO: Supervisor - UNIDAD DE PREVENCION DE FRAUDES
                usuarioreasignado = "B7706";
            }

            //TODO: TIPOLOGIA PA - TARJETA DE CREDITO - TARJETA DE DEBITO
            if (tipologia.equals("CAMBIO DE FORMA DE ENVÍO DE CORRESPONDENCIA") || tipologia.equals("ABONO DE MILLAS") ||
                    tipologia.equals("CAMBIO DE TARJETA")) {
                //TODO: Supervisor - Gestión De Pedidos Y Reclamos TC
                usuarioreasignado = "B10951";
            }

        }

        //TODO: COBRANZAS - TIPOLOGIA PA
        if (tipoProducto.equals("COBRANZAS")) {
            if (tipologia.equals("CONSTANCIA DE NO ADEUDO")) {
                //TODO: Supervisor - U-Segto-Pedidos Recl
                usuarioreasignado = "B10181";
            }
        }

        //TODO: TRASLADOS CTS
        if (tipoProducto.equals("TRASLADOS CTS") || tipoProducto.equals("CUENTA")) {
            if (tipologia.equals("TRASLADO DE CTS DIGITAL A INTERBANK") || tipologia.equals("TRASLADO DE CTS FÍSICO A INTERBANK") ||
                    tipologia.equals("TRASLADOS DE CTS A OTRO BANCO")) {
                usuarioreasignado = "B40286";
            }
        }

        //TODO: EFECTIVO CON GARANTÍA HIPOTECARIO - TRÁMITES DOCUMENTARIOS POST VENTA
        if (tipoProducto.equals("EFECTIVO CON GARANTÍA HIPOTECARIO")) {
            if (tipologia.equals("TRÁMITES DOCUMENTARIOS POST VENTA")) {
                //TODO: Supervisor - HIPOTECARIO - LEGAL INMOBILIARIO
                if (ambiente.equals("UAT")) {
                    usuarioreasignado = "B12842";
                } else if (ambiente.equals("STG")) {
                    usuarioreasignado = "B40640";
                }
                
            }
        }

        System.out.println("=======================================\nENDPOINT: Username de la Consulta: " + usuarioreasignado + "\n=======================================");

        actor.attemptsTo(
                WaitUntil.the(LoginPage.INP_USUARIO, isVisible()).forNoMoreThan(30).seconds(),
                Enter.theValue(usuarioreasignado).into(LoginPage.INP_USUARIO),
                Enter.theValue("Abc12345$").into(LoginPage.INP_PASSWORD)
        );
        actor.attemptsTo(
                WaitUntil.the(LoginPage.BTN_INICIAR_SESION, isVisible()).forNoMoreThan(30).seconds(),
                Click.on(LoginPage.BTN_INICIAR_SESION)
        );

        try {
            Serenity.getDriver().manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
            if (Serenity.getDriver().findElement(By.xpath("//p[text()=' *EL ID O CONTRASEÑA NO COINCIDEN ']")).isDisplayed())
                fail("ERROR! --> EL ID O CONTRASEÑA NO COINCIDEN ");
        } catch (Exception e) {
            //
        } finally {
            Serenity.getDriver().manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        }
    }

}
