package com.everis.giru.tasks.tramites;

import com.everis.giru.userinterface.tramites.ResolucionTramitePage;

import com.everis.giru.utils.SubirArchivo;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;

import java.awt.*;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class SubirCartaResolucion implements Task {
    private final String carta;
    private final String tipoCarta;

    String resolCarta;
    ResolucionTramitePage resolucionTramitePage;
    String tipoResolucion = Serenity.sessionVariableCalled("tipoResol").toString();
    String numeroTramite = Serenity.sessionVariableCalled("numeroTramite").toString();
    String idCarta;
    String tipologiaresol;

    public SubirCartaResolucion(String carta, String tipoCarta) {
        this.carta = carta;
        this.tipoCarta = tipoCarta;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        if (!carta.equals("No")) {
            Serenity.setSessionVariable("nombreCarta").to(carta);
            switch (tipoResolucion) {
                case "Procede":
                    switch (tipoCarta) {
                        case "CartaProcedeCI":
                            idCarta = "2";
                            Serenity.setSessionVariable("tipoCarta").to(idCarta);
                            resolCarta = "P";
                            Serenity.setSessionVariable("tipoCartaDer").to(resolCarta);
                            tipologiaresol = "ProcedeCI";
                            Serenity.setSessionVariable("tipologiaResol").to(tipologiaresol);
                            break;
                        case "CartaProcedeEXO":
                            idCarta = "4";
                            Serenity.setSessionVariable("tipoCarta").to(idCarta);
                            resolCarta = "P";
                            Serenity.setSessionVariable("tipoCartaDer").to(resolCarta);
                            tipologiaresol = "ProcedeExo";
                            Serenity.setSessionVariable("tipologiaResol").to(tipologiaresol);
                            break;
                        case "CartaProcedeCI_Remesa":
                            idCarta = "4";
                            Serenity.setSessionVariable("tipoCarta").to(idCarta);
                            resolCarta = "R";
                            Serenity.setSessionVariable("tipoCartaDer").to(resolCarta);
                            tipologiaresol = "ProcedeCI";
                            Serenity.setSessionVariable("tipologiaResol").to(tipologiaresol);
                            break;
                    }
                    break;
                case "No Procede":
                    switch (tipoCarta) {
                        case "CartaNoProcedeCI":
                            idCarta = "1";
                            Serenity.setSessionVariable("tipoCarta").to(idCarta);
                            resolCarta = "R";
                            Serenity.setSessionVariable("tipoCartaDer").to(resolCarta);
                            tipologiaresol = "NoProcedeCI";
                            Serenity.setSessionVariable("tipologiaResol").to(tipologiaresol);
                            break;
                        case "CartaNoProcedeEXO":
                            idCarta = "3";
                            Serenity.setSessionVariable("tipoCarta").to(idCarta);
                            resolCarta = "R";
                            Serenity.setSessionVariable("tipoCartaDer").to(resolCarta);
                            tipologiaresol = "NoProcedeExo";
                            Serenity.setSessionVariable("tipologiaResol").to(tipologiaresol);
                            break;
                        case "CartaProcedeCI_Remesa":
                            idCarta = "4";
                            Serenity.setSessionVariable("tipoCarta").to(idCarta);
                            resolCarta = "R";
                            Serenity.setSessionVariable("tipoCartaDer").to(resolCarta);
                            tipologiaresol = "NoProcedeCI";
                            Serenity.setSessionVariable("tipologiaResol").to(tipologiaresol);
                            break;
                    }
                    break;
            }
            //resolucionTramitePage.seleccionarMedioCIYEXO();
            //resolucionTramitePage.descargarCarta();

            try {

                new SubirArchivo().subirCarta2(tipoResolucion, numeroTramite, resolCarta);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (AWTException e) {
                e.printStackTrace();
            }
            try {
                Thread.sleep(4000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } else {
            if (carta.equals("No")) {
                System.out.println("No Adjunta la carta");
            }
        }

    }

    public static Performable withData(String carta, String tipoCarta) {
        return instrumented(SubirCartaResolucion.class, carta, tipoCarta);
    }
}