package com.everis.giru.tasks.tramites.filtros;

import com.everis.giru.userinterface.tramites.FiltrosBandejaPage;
import com.everis.giru.userinterface.tramites.filtrosPage.GetFiltroMedioRespuestaPage;
import com.everis.giru.userinterface.tramites.validaFiltros.ValidaFiltroMedioRespuestaPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class SeleccionarFiltroMedioRespuesta implements Task {

    private final String departamento;
    private final String perfil;
    private final String tipologia;
    private final String fMedioRespuesta;

    GetFiltroMedioRespuestaPage getFiltroMedioRespuestaPage;
    ValidaFiltroMedioRespuestaPage validaFiltroMedioRespuestaPage;

    public SeleccionarFiltroMedioRespuesta(String departamento, String perfil, String tipologia, String fMedioRespuesta) {
        this.departamento = departamento;
        this.perfil = perfil;
        this.tipologia = tipologia;
        this.fMedioRespuesta = fMedioRespuesta;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        if (!(fMedioRespuesta.equals("No"))) {
            actor.attemptsTo(
                    WaitUntil.the(FiltrosBandejaPage.BUTTOM_FILTRO_MASFILTROS, isVisible()).forNoMoreThan(100).seconds(),
                    Click.on(FiltrosBandejaPage.BUTTOM_FILTRO_MASFILTROS)
            );

            getFiltroMedioRespuestaPage.seleccionaFiltroMedioRespuesta(fMedioRespuesta);

            actor.attemptsTo(
                    WaitUntil.the(FiltrosBandejaPage.BUTTOM_FILTRO_APLICAR, isVisible()).forNoMoreThan(100).seconds(),
                    Click.on(FiltrosBandejaPage.BUTTOM_FILTRO_APLICAR)
            );

            validaFiltroMedioRespuestaPage.validaFiltroMedioRespuesta(departamento, perfil, tipologia, fMedioRespuesta);

        }

    }

    public static Performable withData(String departamento, String perfil, String tipologia, String fMedioRespuesta) {
        return instrumented(SeleccionarFiltroMedioRespuesta.class, departamento, perfil, tipologia, fMedioRespuesta);
    }

}
