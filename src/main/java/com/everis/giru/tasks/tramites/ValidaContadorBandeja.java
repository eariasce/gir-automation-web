package com.everis.giru.tasks.tramites;

import com.everis.giru.userinterface.tramites.contadorPage.GetContadorIzquierdoPage;
import com.everis.giru.userinterface.tramites.contadorPage.GetContadorDerechoPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static org.assertj.core.api.Assertions.assertThat;

public class ValidaContadorBandeja implements Task {

    GetContadorIzquierdoPage getContadorIzquierdoPage = new GetContadorIzquierdoPage();
    GetContadorDerechoPage getContadorDerechoPage = new GetContadorDerechoPage();

    private final String departamento;
    private final String perfil;
    private final String tipologia;

    public ValidaContadorBandeja(String departamento, String perfil, String tipologia) {
        this.departamento = departamento;
        this.perfil = perfil;
        this.tipologia = tipologia;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        switch (departamento) {
            case "GPYR":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));
                                }

                                break;
                            case "CobrosIndebidos":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("0")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("No se encontraron coincidencias")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));

                                }

                                break;
                            case "ExoneracionCobros":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("0")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("No se encontraron coincidencias")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));

                                }

                                break;
                            case "AnulacionTC":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("0")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("No se encontraron coincidencias")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));

                                }
                                break;
                            case "FinalizacionTC":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("0")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("No se encontraron coincidencias")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));

                                }
                                break;
                            case "TransaccionMalNoProcesada":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("0")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("No se encontraron coincidencias")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));

                                }
                                break;
                            case "PagoAnticipadoAdelantoCuotas":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("0")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("No se encontraron coincidencias")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));

                                }
                                break;
                            case "DevolucionSaldoAcreedor":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("0")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("No se encontraron coincidencias")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));

                                }
                                break;
                            case "ProgramaRecompensas":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("0")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("No se encontraron coincidencias")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));

                                }
                                break;
                            case "SistemaRecompensas":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("0")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("No se encontraron coincidencias")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));

                                }
                                break;
                            case "InsuficienteInformacion":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("0")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("No se encontraron coincidencias")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));

                                }
                                break;
                            case "IncorrectaAplicacionCuotas":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("0")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("No se encontraron coincidencias")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));

                                }

                                break;

                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA VALIDAR CONTADOR BANDEJA EN: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));

                                }

                                break;
                            case "MiEquipo":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("0")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));
                                }
                                break;
                            case "Asignar":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("0")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));

                                }

                                break;
                            case "Resolver":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("0")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));
                                }

                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA VALIDAR CONTADOR BANDEJA EN: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                }
                break;
            case "Seguros":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));
                                }

                                break;
                            case "CobrosIndebidos":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("0")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("No se encontraron coincidencias")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));
                                }

                                break;
                            case "ExoneracionCobros":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("0")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("No se encontraron coincidencias")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));
                                }

                                break;
                            case "ConsumosNoReconocidos":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("0")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("No se encontraron coincidencias")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));
                                }
                                break;
                            case "Monitorear":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("0")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));
                                }

                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA VALIDAR CONTADOR BANDEJA EN: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));
                                }

                                break;
                            case "MiEquipo":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("0")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));
                                }

                                break;
                            case "Asignar":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("0")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));
                                }

                                break;
                            case "Resolver":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("0")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));
                                }

                                break;
                            case "Monitorear":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("0")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));
                                    break;
                                }

                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA VALIDAR CONTADOR BANDEJA EN: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                }
                break;
            case "Retenciones":
                break;
            case "Controversias":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));

                                }

                                break;
                            case "ConsumosNoReconocidos":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("0")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("No se encontraron coincidencias")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));

                                }

                                break;
                            case "ConsumosMalProcesados":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("0")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("No se encontraron coincidencias")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));
                                }

                                break;
                            case "Monitorear":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("0")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));

                                }

                                break;
                            case "TransaccionesEnProceso":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("0")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));

                                }

                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA VALIDAR CONTADOR BANDEJA EN: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));

                                }

                                break;
                            case "MiEquipo":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("0")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));

                                }

                                break;
                            case "Asignar":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("0")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));

                                }

                                break;
                            case "Resolver":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("0")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));

                                }

                                break;
                            case "Monitorear":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("0")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));

                                }

                                break;
                            case "TransaccionesEnProceso":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("0")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));

                                }

                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA VALIDAR CONTADOR BANDEJA EN: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                }
                break;
            case "USPR":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));

                                }

                                break;
                            case "CobrosIndebidos":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("0")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("No se encontraron coincidencias")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));

                                }

                                break;
                            case "ExoneracionCobros":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("0")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("No se encontraron coincidencias")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));

                                }
                                break;
                            case "ConsumosMalProcesados":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("0")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("No se encontraron coincidencias")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));

                                }
                                break;
                            case "ConsumosNoReconocidos":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("0")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("No se encontraron coincidencias")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));
                                }
                                break;
                            case "TransaccionMalNoProcesada":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("0")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("No se encontraron coincidencias")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));
                                }
                                break;
                            case "DuplicadoEstadoCuentaSC":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("0")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("No se encontraron coincidencias")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));
                                }
                                break;
                            case "SistemaRecompensas":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("0")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("No se encontraron coincidencias")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));
                                }
                                break;
                            case "DesacuerdoCondicionesTT":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("0")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("No se encontraron coincidencias")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("No se encontraron coincidencias");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));
                                }
                                break;
                            case "Retipificar":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("0")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));
                                }

                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA VALIDAR CONTADOR BANDEJA EN: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));

                                }

                                break;
                            case "MiEquipo":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("0")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));

                                }

                                break;
                            case "Asignar":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("0")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));

                                }
                                break;
                            case "Resolver":
                                if (getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia).equals("0")) {
                                    getContadorDerechoPage.EsCeroTotalTramite();
                                    if (getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia).equals("Ingresa una búsqueda para ver resultados")) {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    } else {
                                        assertThat(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia)).isEqualTo("Ingresa una búsqueda para ver resultados");
                                    }

                                } else {
                                    assertThat(getContadorIzquierdoPage.getValueContadorIzquierdo(departamento, perfil, tipologia)).isEqualTo(getContadorDerechoPage.getContadorDerecho(departamento, perfil, tipologia));
                                }

                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA VALIDAR CONTADOR BANDEJA EN: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                }
                break;
        }

        getContadorIzquierdoPage.SetContadorANull();

    }

    public static Performable withData(String departamento, String perfil, String tipologia) {
        return instrumented(ValidaContadorBandeja.class, departamento, perfil, tipologia);
    }

}
