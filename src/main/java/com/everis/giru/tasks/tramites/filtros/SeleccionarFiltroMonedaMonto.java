package com.everis.giru.tasks.tramites.filtros;

import com.everis.giru.userinterface.tramites.FiltrosBandejaPage;
import com.everis.giru.userinterface.tramites.filtrosPage.GetFiltroMonedaMontoPage;
import com.everis.giru.userinterface.tramites.validaFiltros.ValidaFiltroMonedaMontoPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class SeleccionarFiltroMonedaMonto implements Task {

    private final String departamento;
    private final String perfil;
    private final String tipologia;
    private final String fMoneda;
    private final String fMontoDesde;
    private final String fMontoHasta;

    GetFiltroMonedaMontoPage getFiltroMonedaMontoPage;
    ValidaFiltroMonedaMontoPage validaFiltroMonedaMontoPage;

    public SeleccionarFiltroMonedaMonto(String departamento, String perfil, String tipologia, String fMoneda, String fMontoDesde, String fMontoHasta) {
        this.departamento = departamento;
        this.perfil = perfil;
        this.tipologia = tipologia;
        this.fMoneda = fMoneda;
        this.fMontoDesde = fMontoDesde;
        this.fMontoHasta = fMontoHasta;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        if (!(fMoneda.equals("No"))) {
            actor.attemptsTo(
                    WaitUntil.the(FiltrosBandejaPage.BUTTOM_FILTRO_MONEDAyMONTO, isVisible()).forNoMoreThan(30).seconds(),
                    Click.on(FiltrosBandejaPage.BUTTOM_FILTRO_MONEDAyMONTO)
            );

            getFiltroMonedaMontoPage.seleccionaFiltroMonedaMonto(fMoneda, fMontoDesde, fMontoHasta);

            actor.attemptsTo(
                    WaitUntil.the(FiltrosBandejaPage.BUTTOM_FILTRO_APLICAR, isVisible()).forNoMoreThan(30).seconds(),
                    Click.on(FiltrosBandejaPage.BUTTOM_FILTRO_APLICAR)
            );

            validaFiltroMonedaMontoPage.validaFiltroMonedaMonto(departamento, perfil, tipologia, fMoneda, fMontoDesde, fMontoHasta);

        }

    }

    public static Performable withData(String departamento, String perfil, String tipologia, String fMoneda, String fMontoDesde, String fMontoHasta) {
        return instrumented(SeleccionarFiltroMonedaMonto.class, departamento, perfil, tipologia, fMoneda, fMontoDesde, fMontoHasta);
    }

}
