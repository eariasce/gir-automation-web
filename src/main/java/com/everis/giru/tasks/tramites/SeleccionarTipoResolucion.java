package com.everis.giru.tasks.tramites;

import com.everis.giru.tasks.atencionCliente.SubirArchivo;
import com.everis.giru.tasks.login.SeleccionarMenuPrincipal;
import com.everis.giru.userinterface.atencionCliente.ValidacionesRegistroPage;
import com.everis.giru.userinterface.tramites.historial.ValidaNumeroTramiteHistorialPage;
import com.everis.giru.utils.WriteToFile;
import com.everis.tdm.Do;
import com.everis.tdm.model.giru.EscenarioValidacion;
import com.everis.tdm.model.giru.resolucionComercial;
import lombok.SneakyThrows;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.Locale;
import java.util.Objects;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class SeleccionarTipoResolucion implements Task {

    private String tipoResol;
    String tipoResolTramite;
    ValidaNumeroTramiteHistorialPage validaNumeroTramiteHistorialPage = new ValidaNumeroTramiteHistorialPage();
    ValidacionesRegistroPage validacionRegistroPage = new ValidacionesRegistroPage();

    public SeleccionarTipoResolucion(String tipoResol) {
        this.tipoResol = tipoResol;
    }

    @SneakyThrows
    @Override
    public <T extends Actor> void performAs(T actor) {

        Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
        WebElement btnClaimDetailTabs = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(60)).until(ExpectedConditions.elementToBeClickable(By.xpath("//ibk-giru-action-bar[@id='claim-detail-tabs__buttons']//div[@class='toggle-action hide']")));
        btnClaimDetailTabs.click();


        String tipoproducto = Serenity.sessionVariableCalled("tipoproducto");

        //TDM

        String tipotramite = Serenity.sessionVariableCalled("tipotramite");
        String tipologia = Serenity.sessionVariableCalled("tipologia");
        String areaVal = Serenity.sessionVariableCalled("areaValidacionOpcional");
        String comercial = Serenity.sessionVariableCalled("comercial");

        String fueDerivado = "No";
        String estoyEnResolutoria = "No";
        try {
            fueDerivado = Serenity.sessionVariableCalled("fueDerivado");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        try {
            estoyEnResolutoria = Serenity.sessionVariableCalled("estoyEnResolutoria");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        EscenarioValidacion escenarioValidacion;
        resolucionComercial resolucionComercialr;
        String areaInicial = "";
        String areaResolutora = "";
        String areaValidadoraOpcional = "";
        String resolutora = "No";

        if(tipoResol.equals("En Proceso"))
        {
            String numeroTramite = Serenity.sessionVariableCalled("numeroTramite").toString();

            String tiporesol = Serenity.sessionVariableCalled("tipoResol");
            WriteToFile.writeLinesToFileResol(numeroTramite +",omitido,"+tiporesol);

            throw new AssertionError("En proceso, no se resolvera tramite");
        }

        if (!tipoResol.equals("Derivar-2")&&!tipoResol.equals("Derivar-2-Procede")&&!tipoResol.equals("Derivar-2-NoProcede")) {
            try {
                System.out.println("TIPO PRODUCTO: " + tipoproducto.toUpperCase(Locale.ROOT) + " TIPO TRAMITE: " + tipotramite + " TIPOLOGIA: " + tipologia + " AREA VAL: " + areaVal);
                String areaValidadora = Serenity.sessionVariableCalled("areaValidacionOpcional");
                if (!areaValidadora.equals("")) {
                    if (comercial.equals("si")) {
                        resolucionComercialr = Do.getResolucionComercial(tipoproducto.toUpperCase(Locale.ROOT), tipotramite, tipologia, "");

                        areaInicial = resolucionComercialr.getAreaInicial();
                        areaResolutora = resolucionComercialr.getAreaResolutora();
                        //areaValidadoraOpcional = resolucionComercialr.getAreaValidadoraOpcional();
                        areaValidadoraOpcional = "TENGO AREA VALIDADORA!";
                    }else {
                        escenarioValidacion = Do.getEscenarioValidacion(tipoproducto.toUpperCase(Locale.ROOT), tipotramite, tipologia, "");

                        areaInicial = escenarioValidacion.getAreaInicial();
                        areaResolutora = escenarioValidacion.getAreaResolutora();
                        areaValidadoraOpcional = "TENGO AREA VALIDADORA!";
                    }
                } else if (comercial.equals("si")) {
                        resolucionComercialr = Do.getResolucionComercial(tipoproducto.toUpperCase(Locale.ROOT), tipotramite, tipologia, "");

                        areaInicial = resolucionComercialr.getAreaInicial();
                        areaResolutora = resolucionComercialr.getAreaResolutora();
                        areaValidadoraOpcional = "";
                    //   areaInicial = "AREA";
                 //   areaResolutora = "AREA";
                 //   areaValidadoraOpcional = "";
                }else {
                    escenarioValidacion = Do.getEscenarioValidacion(tipoproducto.toUpperCase(Locale.ROOT), tipotramite, tipologia, areaVal);

                    areaInicial = escenarioValidacion.getAreaInicial();
                    areaResolutora = escenarioValidacion.getAreaResolutora();
                    areaValidadoraOpcional = escenarioValidacion.getAreaValidadoraOpcional();
                }
            } catch (Exception e) {

                System.out.println(e.getMessage());
                throw new AssertionError("No se encuentra escenario de validación resolución" + e.getMessage());
            }

            //TODO: OBSERVACION - REVISAR
            System.err.println("++++++++++++++++++++ AREA RESOLUTORA: " + areaResolutora + areaValidadoraOpcional);

            if (tipoResol.equals("Retipificar")) {
                resolutora = "Si";
            }

            if (!areaInicial.equals(areaResolutora) && !Objects.equals(areaValidadoraOpcional, "")) {

                if (estoyEnResolutoria.equals("EnProceso")) {
                    resolutora = "Si";
                    Serenity.setSessionVariable("estoyEnResolutoria").to("Si");
                }

                if (estoyEnResolutoria.equals("EnDerivacionOpcional")) {
                    resolutora = "No";
                    Serenity.setSessionVariable("estoyEnResolutoria").to("EnProceso");
                }

                if (estoyEnResolutoria.equals("EnDerivacion")) {
                    resolutora = "No";
                    Serenity.setSessionVariable("estoyEnResolutoria").to("EnDerivacionOpcional");
                }

                if (estoyEnResolutoria.equals("No")) {
                    resolutora = "No";
                    Serenity.setSessionVariable("estoyEnResolutoria").to("EnDerivacion");
                }

            }

            if (!areaInicial.equals(areaResolutora) && Objects.equals(areaValidadoraOpcional, "")) {

                if (estoyEnResolutoria.equals("EnProceso")) {
                    resolutora = "Si";
                    Serenity.setSessionVariable("estoyEnResolutoria").to("Si");
                }

                if (estoyEnResolutoria.equals("No")) {
                    resolutora = "No";
                    Serenity.setSessionVariable("estoyEnResolutoria").to("EnProceso");
                }

            }

            if (areaInicial.equals(areaResolutora) && Objects.equals(areaValidadoraOpcional, "")) {
                resolutora = "Si";
                Serenity.setSessionVariable("estoyEnResolutoria").to("Si");
            }

            if (areaInicial.equals(areaResolutora) && !Objects.equals(areaValidadoraOpcional, "")) {

                if (Objects.equals(fueDerivado, "Si")) {
                    Serenity.setSessionVariable("fueDerivado").to("Si");
                    resolutora = "Si";
                    Serenity.setSessionVariable("estoyEnResolutoria").to("Si");
                }

                if (Objects.equals(fueDerivado, "EnProceso")) {
                    Serenity.setSessionVariable("fueDerivado").to("Si");
                    resolutora = "No";
                }

                if (Objects.equals(fueDerivado, "No")) {
                    resolutora = "No";
                    Serenity.setSessionVariable("fueDerivado").to("EnProceso");
                }
                String der = Serenity.sessionVariableCalled("fueDerivado");
                System.out.println("fueDerivado: " + der +" resolutora: "+resolutora);
            }
            //TDM
        } else {
            if(fueDerivado.equals("En Proceso")) {
                resolutora = "Si";
                Serenity.setSessionVariable("estoyEnResolutoria").to("Si");
                Serenity.setSessionVariable("fueDerivado").to("DerivadoOpcional");
                if(tipoResol.equals("Derivar-2-Procede")) {
                    tipoResol = "Procede";
                }else
                    tipoResol = "No Procede";
            }
            if(fueDerivado.equals("No")){
                resolutora = "No";
            Serenity.setSessionVariable("estoyEnResolutoria").to("No");
            Serenity.setSessionVariable("fueDerivado").to("En Proceso");
                if(tipoResol.equals("Derivar-2-Procede")) {
                    tipoResol = "Procede";
                }else
                    tipoResol = "No Procede";

            }



        }

        if (resolutora.equals("Si")) {
            switch (tipoResol) {
                case "Procede":
                    //TODO: SELECCIONA BUTTON - VERDE - PROCEDE
                    WebElement btnPF = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(30))
                            .until(ExpectedConditions.elementToBeClickable(By.id("PF")));
                    btnPF.click();

                    System.out.println("AREA RESOLUTORA - PROCEDE: " + "<< PF >>");
                    tipoResolTramite = "P";
                    break;

                case "No Procede":
                    //TODO: SELECCIONA BUTTON - ROJO - NO PROCEDE
                    WebElement btnNF = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(30))
                            .until(ExpectedConditions.elementToBeClickable(By.id("NF")));
                    btnNF.click();

                    System.out.println("AREA RESOLUTORA - NO PROCEDE: " + "<< NF >>");
                    tipoResolTramite = "NP";
                    break;

                case "Derivar":
                    //TODO: SELECCIONA BUTTON - AMBAR - DERIVAR - FLUJO RESOLUCIÓN
                    WebElement btnDerivar = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(30))
                            .until(ExpectedConditions.elementToBeClickable(By.id("DE")));
                    btnDerivar.click();

                    System.out.println("AREA RESOLUTORA - DERIVAR: " + "<< DE >>");
                    tipoResolTramite = "D";
                    break;
                case "Derivar-2":
                    //TODO: SELECCIONA BUTTON - AMBAR - DERIVAR - - FLUJO RETIPIFICACIÓN
                    WebElement btnDerivarOne = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(30))
                            .until(ExpectedConditions.elementToBeClickable(By.id("DE")));
                    btnDerivarOne.click();
                    break;

                case "Derivar-2-Procede":
                    //TODO: SELECCIONA BUTTON - VERDE - DERIVA- PROCEDE
                    WebElement btnDeriPF = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(30))
                            .until(ExpectedConditions.elementToBeClickable(By.id("PF")));
                    btnDeriPF.click();

                    System.out.println("AREA RESOLUTORA - DERIVA Y LUEGO PROCEDE: " + "<< PF >>");
                    tipoResolTramite = "P";

                    break;

                case "Derivar-2-NoProcede":
                    //TODO: SELECCIONA BUTTON - ROJO - NO PROCEDE
                    WebElement btnDeriNF = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(30))
                            .until(ExpectedConditions.elementToBeClickable(By.id("NF")));
                    btnDeriNF.click();

                    System.out.println("AREA RESOLUTORA - DERIVA Y LUEGO NO PROCEDE: " + "<< NF >>");
                    tipoResolTramite = "NP";



                    try {
                        Thread.sleep(5000);
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }

                    System.out.println("INGRESA TIPO RESOLUCION A TRAMITE - DERIVAR: " + "<< D >>");
                    tipoResolTramite = "D";

                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select[1]//div[@id='undefined']//div)[1]")).click();
                    WebElement actividad1 = Serenity.getDriver().findElement(By.xpath("(//app-ibk-select[1]//div[@id='undefined']//li)[1]"));
                    String actividad1Text = actividad1.getText();
                    actividad1.click();

                    Serenity.getDriver().findElement(By.xpath("(//app-ibk-select[1]//div[@id='undefined']/div[1])[2]")).click();
                    WebElement area1 = Serenity.getDriver().findElement(By.xpath("((//app-ibk-select)[3])//li"));
                    String area1Text = area1.getText();
                    area1.click();

                    Serenity.getDriver().findElement(By.xpath("//textarea[@formcontrolname='areaComments']")).sendKeys("Derivar Prueba");
                    String subirarchivo = Serenity.sessionVariableCalled("subirarchivo").toString();

                    theActorInTheSpotlight().attemptsTo(
                            SubirArchivo.withData(subirarchivo)
                    );

                    actor.attemptsTo(
                            WaitUntil.the(By.xpath("//button[contains(text(), 'Confirmar')]"), isVisible()).forNoMoreThan(20).seconds(),
                            Click.on(By.xpath("//button[contains(text(), 'Confirmar')]"))
                    );

                    try {
                        Thread.sleep(10000);
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }

                    //TODO: VALIDACION HISTORIAL
                    String numeroTramite = Serenity.sessionVariableCalled("numeroTramite").toString();

                    theActorInTheSpotlight().attemptsTo(
                            SeleccionarMenuPrincipal.withData("Historial")
                    );

                    validaNumeroTramiteHistorialPage.validaNumeroTramiteHistorial(numeroTramite);
                    validaNumeroTramiteHistorialPage.validaSeguimientoTramiteDerivado("Derivar Prueba", area1Text, actividad1Text);
                    validacionRegistroPage.SalirDetalle();

            }
        } else {
            switch (tipoResol) {
                case "Procede":
                    //TODO: SELECCIONA BUTTON - VERDE - PROCEDE VALIDACIÓN
                    WebElement btnPV = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(30))
                            .until(ExpectedConditions.elementToBeClickable(By.id("PV")));
                    btnPV.click();

                    System.out.println("AREA VALIDADORA - PROCEDE VALIDACIÓN: " + "<< PV >>");
                    tipoResolTramite = "PV";
                    break;

                case "No Procede":
                    //TODO: SELECCIONA BUTTON - ROJO - NO PROCEDE VALIDACIÓN
                    WebElement btnNV = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(30))
                            .until(ExpectedConditions.elementToBeClickable(By.id("NV")));
                    btnNV.click();

                    System.out.println("AREA VALIDADORA - NO PROCEDE VALIDACIÓN: " + "<< NV >>");
                    tipoResolTramite = "NV";
                    break;

                case "Derivar":
                    //TODO: SELECCIONA BUTTON - AMBAR - DERIVAR
                    WebElement btnDerivar = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(30))
                            .until(ExpectedConditions.elementToBeClickable(By.id("DE")));
                    btnDerivar.click();

                    System.out.println("AREA VALIDADORA - DERIVAR: " + "<< D >>");
                    tipoResolTramite = "D";
                    break;


            }


        }


    }

    public static Performable withData(String tipoResol) {
        return instrumented(SeleccionarTipoResolucion.class, tipoResol);
    }

}


