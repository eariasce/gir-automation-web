package com.everis.giru.tasks.tramites.filtros;

import com.everis.giru.userinterface.tramites.FiltrosBandejaPage;
import com.everis.giru.userinterface.tramites.filtrosPage.GetFiltroMotivosPage;
import com.everis.giru.userinterface.tramites.validaFiltros.ValidaFiltroMotivosPage;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Scroll;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class SeleccionarFiltroMotivos implements Task {

    private final String departamento;
    private final String perfil;
    private String fTipoTramite;
    private String fTipologia;
    private final String fMotivos;
    
    GetFiltroMotivosPage getFiltroMotivosPage;
    ValidaFiltroMotivosPage validaFiltroMotivosPage;

    String tipologia = Serenity.sessionVariableCalled("Tipologia").toString();

    public SeleccionarFiltroMotivos(String departamento, String perfil, String fTipoTramite, String fTipologia, String fMotivos) {
        this.departamento = departamento;
        this.perfil = perfil;
        this.fTipoTramite = fTipoTramite;
        this.fTipologia = fTipologia;
        this.fMotivos = fMotivos;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        if (!(fMotivos.equals("No"))) {
            if (tipologia.equals("TodosMisTramites") || tipologia.equals("MiEquipo") || tipologia.equals("Asignar") || tipologia.equals("Resolver") || tipologia.equals("Monitorear") || tipologia.equals("TransaccionesEnProceso") || tipologia.equals("Retipificar")) {
                actor.attemptsTo(
                        WaitUntil.the(FiltrosBandejaPage.BUTTOM_FILTRO_MOTIVOS, isVisible()).forNoMoreThan(100).seconds(),
                        Click.on(FiltrosBandejaPage.BUTTOM_FILTRO_MOTIVOS)
                );

                getFiltroMotivosPage.SeleccionaFiltroMotivos(departamento, perfil, fTipoTramite, fTipologia, fMotivos);

                actor.attemptsTo(
                        WaitUntil.the(FiltrosBandejaPage.BUTTOM_FILTRO_APLICAR, isVisible()).forNoMoreThan(100).seconds(),
                        Scroll.to(FiltrosBandejaPage.BUTTOM_FILTRO_APLICAR),
                        Click.on(FiltrosBandejaPage.BUTTOM_FILTRO_APLICAR)
                );

                validaFiltroMotivosPage.validaFiltroMotivos(departamento, perfil, tipologia, fTipoTramite, fTipologia, fMotivos);

            } else {
                if (tipologia.equals("CobrosIndebidos") || tipologia.equals("ConsumosNoReconocidos") || tipologia.equals("ConsumosMalProcesados")) {
                    fTipoTramite = "Reclamo";
                    fTipologia = tipologia;
                } else if (tipologia.equals("ExoneracionCobros")) {
                    fTipoTramite = "Pedido";
                    fTipologia = tipologia;
                } else if (tipologia.equals("AnulacionTC") || tipologia.equals("FinalizacionTC")) {
                    fTipoTramite = "Solicitud";
                    fTipologia = tipologia;
                }

                actor.attemptsTo(
                        WaitUntil.the(FiltrosBandejaPage.BUTTOM_FILTRO_MOTIVOS, isVisible()).forNoMoreThan(100).seconds(),
                        Click.on(FiltrosBandejaPage.BUTTOM_FILTRO_MOTIVOS)
                );

                getFiltroMotivosPage.SeleccionaFiltroMotivos(departamento, perfil, fTipoTramite, fTipologia, fMotivos);

                actor.attemptsTo(
                        WaitUntil.the(FiltrosBandejaPage.BUTTOM_FILTRO_APLICAR, isVisible()).forNoMoreThan(100).seconds(),
                        Scroll.to(FiltrosBandejaPage.BUTTOM_FILTRO_APLICAR),
                        Click.on(FiltrosBandejaPage.BUTTOM_FILTRO_APLICAR)
                );

                validaFiltroMotivosPage.validaFiltroMotivos(departamento, perfil, tipologia, fTipoTramite, fTipologia, fMotivos);


            }

        }

    }

    public static Performable withData(String departamento, String perfil, String fTipoTramite, String fTipologia, String fMotivos) {
        return instrumented(SeleccionarFiltroMotivos.class, departamento, perfil, fTipoTramite, fTipologia, fMotivos);
    }

}
