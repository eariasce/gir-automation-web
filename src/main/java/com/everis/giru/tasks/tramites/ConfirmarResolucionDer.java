package com.everis.giru.tasks.tramites;

import com.everis.giru.userinterface.tramites.ResolucionTramitePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

public class ConfirmarResolucionDer implements Task {

    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(ResolucionTramitePage.BTN_CONFIRMAR_DERIVACION)
        );
        try {
            Thread.sleep(8000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
