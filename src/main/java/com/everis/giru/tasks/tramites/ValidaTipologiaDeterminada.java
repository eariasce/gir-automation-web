package com.everis.giru.tasks.tramites;

import com.everis.giru.questions.tramites.PerfilTipologialQuestions;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.Matchers.equalTo;


public class ValidaTipologiaDeterminada implements Task {

    private final String perfil;
    private final String tipologia;
    private final String departamento;

    public ValidaTipologiaDeterminada(String perfil, String tipologia, String departamento) {
        this.perfil = perfil;
        this.tipologia = tipologia;
        this.departamento = departamento;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        switch (departamento) {
            case "GPYR":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("Mis trámites"))
                                );
                                break;
                            case "CobrosIndebidos":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("Cobros Indebidos"))
                                );
                                break;
                            case "ExoneracionCobros":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("Exoneración de cobros"))
                                );
                                break;
                            case "AnulacionTC":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("Anulación de tarjeta de crédito"))
                                );
                                break;
                            case "FinalizacionTC":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("Finalización de tarjeta de crédito"))
                                );
                                break;
                            case "TransaccionMalNoProcesada":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("Transacción mal o no procesada"))
                                );
                                break;
                            case "PagoAnticipadoAdelantoCuotas":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("Pago Anticipado y Adelanto de Cuotas"))
                                );
                                break;
                            case "DevolucionSaldoAcreedor":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("Devolucion de saldo acreedor"))
                                );
                                break;
                            case "ProgramaRecompensas":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("Programa de Recompensas"))
                                );
                                break;
                            case "SistemaRecompensas":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("Sistema de Recompensas"))
                                );
                                break;
                            case "InsuficienteInformacion":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("Insuficiente información"))
                                );
                                break;
                            case "IncorrectaAplicacionCuotas":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("Incorrecta aplicación CD/EC/C. cuotas"))
                                );
                                break;
                            default:
                                System.out.println("Título equivocado según Bandeja de la tipología !!!");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("Todos los trámites"))
                                );
                                break;
                            case "MiEquipo":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("Trámites de mi equipo"))
                                );
                                break;
                            case "Asignar":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("Trámites por asignar"))
                                );
                                break;
                            case "Resolver":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("Trámites por resolver"))
                                );
                                break;
                            default:
                                System.out.println("No se encuentra tipología en Supervisor GPYR !!!");
                        }

                        break;
                }
                break;
            case "Seguros":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("Mis trámites"))
                                );
                                break;
                            case "CobrosIndebidos":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("Cobros Indebidos"))
                                );
                                break;
                            case "ExoneracionCobros":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("Exoneración de cobros"))
                                );
                                break;
                            case "ConsumosNoReconocidos":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("C.N.R. por fraude"))
                                );
                                break;
                            case "Monitorear":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("Monitorear"))
                                );
                                break;
                            default:
                                System.out.println("No se encuentra tipología en especialista SEGUROS !!!");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("Todos los trámites"))
                                );
                                break;
                            case "MiEquipo":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("Trámites de mi equipo"))
                                );
                                break;
                            case "Asignar":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("Trámites por asignar"))
                                );
                                break;
                            case "Resolver":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("Trámites por resolver"))
                                );
                                break;
                            case "Monitorear":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("Monitorear"))
                                );
                                break;
                            default:
                                System.out.println("No se encuentra tipología en Supervisor SEGUROS !!!");
                        }
                        break;
                }
                break;
            case "Retenciones":
                break;
            case "Controversias":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("Mis trámites"))
                                );
                                break;
                            case "ConsumosNoReconocidos":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("C.N.R. por fraude"))
                                );
                                break;
                            case "ConsumosMalProcesados":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("Consumo mal procesado por comercio/marca"))
                                );
                                break;
                            case "Monitorear":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("Monitorear"))
                                );
                                break;
                            case "TransaccionesEnProceso":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("Transacciones en Proceso"))
                                );
                                break;
                            default:
                                System.out.println("No se encuentra tipología en especialista CONTROVERSIAS !!!");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("Todos los trámites"))
                                );
                                break;
                            case "MiEquipo":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("Trámites de mi equipo"))
                                );
                                break;
                            case "Asignar":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("Trámites por asignar"))
                                );
                                break;
                            case "Resolver":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("Trámites por resolver"))
                                );
                                break;
                            case "Monitorear":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("Monitorear"))
                                );
                                break;
                            case "TransaccionesEnProceso":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("Transacciones en Proceso"))
                                );
                                break;
                            default:
                                System.out.println("No se encuentra tipología en Supervisor CONTROVERSIAS !!!");
                        }
                        break;
                }
                break;
            case "USPR":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("Mis trámites"))
                                );
                                break;
                            case "CobrosIndebidos":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("Cobros Indebidos"))
                                );
                                break;
                            case "ExoneracionCobros":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("Exoneración de cobros"))
                                );
                                break;
                            case "ConsumosMalProcesados":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("Consumo mal procesado por comercio/marca"))
                                );
                                break;
                            case "ConsumosNoReconocidos":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("C.N.R. por fraude"))
                                );
                                break;
                            case "TransaccionMalNoProcesada":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("Transacción mal o no procesada"))
                                );
                                break;
                            case "DuplicadoEstadoCuentaSC":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("Duplicado de Estado de Cuenta/Servicio de Copias"))
                                );
                                break;
                            case "SistemaRecompensas":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("Sistema de Recompensas"))
                                );
                                break;
                            case "DesacuerdoCondicionesTT":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("Desacuerdo de Condiciones/Tasas/Tarifas"))
                                );
                                break;
                            case "Retipificar":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("Retipificar"))
                                );
                                break;
                            default:
                                System.out.println("No se encuentra tipología en especialista USPR !!!");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("Todos los trámites"))
                                );
                                break;
                            case "MiEquipo":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("Trámites de mi equipo"))
                                );
                                break;
                            case "Asignar":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("Trámites por asignar"))
                                );
                                break;
                            case "Resolver":
                                theActorInTheSpotlight().should(
                                        seeThat(PerfilTipologialQuestions.perfilTipologiaEspecialista(), equalTo("Trámites por resolver"))
                                );
                                break;
                            default:
                                System.out.println("No se encuentra tipología en Supervisor USPR !!!");
                        }
                        break;
                }
                break;
        }

    }

    public static Performable withData(String perfil, String tipologia, String departamento) {
        return instrumented(ValidaTipologiaDeterminada.class, perfil, tipologia, departamento);
    }
}
