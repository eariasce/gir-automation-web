package com.everis.giru.tasks.tramites.filtros;

import com.everis.giru.userinterface.tramites.FiltrosBandejaPage;
import com.everis.giru.userinterface.tramites.filtrosPage.GetFiltroProductosPage;
import com.everis.giru.userinterface.tramites.validaFiltros.ValidaFiltroProductosPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class SeleccionarFiltroProducto implements Task {

    private final String departamento;
    private final String perfil;
    private final String tipologia;
    private final String fProductos;

    GetFiltroProductosPage getFiltroProductosPage;
    ValidaFiltroProductosPage validaFiltroProductosPage;

    public SeleccionarFiltroProducto(String departamento, String perfil, String tipologia, String fProductos) {
        this.departamento = departamento;
        this.perfil = perfil;
        this.tipologia = tipologia;
        this.fProductos = fProductos;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        if (!(fProductos.equals("No"))) {
            actor.attemptsTo(
                    WaitUntil.the(FiltrosBandejaPage.BUTTOM_FILTRO_PRODUCTO, isVisible()).forNoMoreThan(100).seconds(),
                    Click.on(FiltrosBandejaPage.BUTTOM_FILTRO_PRODUCTO)
            );

            getFiltroProductosPage.seleccionaFiltroProductos(fProductos);

            actor.attemptsTo(
                    WaitUntil.the(FiltrosBandejaPage.BUTTOM_FILTRO_APLICAR, isVisible()).forNoMoreThan(100).seconds(),
                    Click.on(FiltrosBandejaPage.BUTTOM_FILTRO_APLICAR)
            );

            validaFiltroProductosPage.validaFiltroProductos(departamento, perfil, tipologia, fProductos);

        }

    }

    public static Performable withData(String departamento, String perfil, String tipologia, String fProductos) {
        return instrumented(SeleccionarFiltroProducto.class, departamento, perfil, tipologia, fProductos);
    }

}
