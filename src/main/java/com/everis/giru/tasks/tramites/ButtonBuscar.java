package com.everis.giru.tasks.tramites;

import com.everis.giru.userinterface.tramites.TramitesPage;
import net.serenitybdd.annotations.Step;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

public class ButtonBuscar implements Task {

    @Override
    @Step("Click button buscar")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(TramitesPage.BTN_BUSCAR)
        );
    }

}
