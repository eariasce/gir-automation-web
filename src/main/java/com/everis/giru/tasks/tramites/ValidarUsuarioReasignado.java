package com.everis.giru.tasks.tramites;

import com.everis.giru.rest.steps.ConsultaFilterInboxSteps;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class ValidarUsuarioReasignado implements Task {
    ConsultaFilterInboxSteps consultaFilterInboxSteps;
    String nroAsesorReasignado = "";
    private final String snumerotramite;

    public ValidarUsuarioReasignado(String snumerotramite) {
        this.snumerotramite = snumerotramite;
    }

    public <T extends Actor> void performAs(T actor) {
        consultaFilterInboxSteps = new ConsultaFilterInboxSteps();
        consultaFilterInboxSteps.seArmaElCuerpoDePeticionCreacionCliente(snumerotramite);

        for(int i=1; i<4; i++) {
            if (consultaFilterInboxSteps.agregoLosEncabezadosEnLaPeticion2() == 200){
                System.out.println("Nro de veces que consulta al servicio de Derivación: " + i);
                break;
            }
        }

        consultaFilterInboxSteps.validoLaRespuestaObtenida();
        nroAsesorReasignado = consultaFilterInboxSteps.validoCamposObtenidos();

        Serenity.setSessionVariable("nrotramite").to(snumerotramite);
        Serenity.setSessionVariable("nroAsesorReasignado").to(nroAsesorReasignado);
    }

    public static Performable withData(String snumerotramite) {
        return instrumented(ValidarUsuarioReasignado.class, snumerotramite);
    }
}
