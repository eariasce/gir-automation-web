package com.everis.giru.tasks.tramites;

import com.everis.giru.userinterface.atencionCliente.IdentificarClientePage;
import com.everis.giru.userinterface.atencionCliente.NoClientePage;
import com.everis.giru.userinterface.atencionCliente.ValidacionesRegistroPage;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import org.openqa.selenium.By;

import java.time.Duration;

public class IngresaDatosComercio implements Task {
    NoClientePage ingresarDatosNoCliente;

    private final String nomComercio;
    ValidacionesRegistroPage validacionesRegistroPage = new ValidacionesRegistroPage();

    public IngresaDatosComercio(String nomComercio) {
        this.nomComercio = nomComercio;
    }

       @Override
    public <T extends Actor> void performAs(T actor) {
        Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
        ingresarDatosNoCliente.ingresarNombreComercio(nomComercio);
        //TODO: SELECCIONA OPCION - REGISTRAR
        Serenity.getDriver().findElement(By.id("search-form__btn-validate")).click();
        Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        Serenity.getDriver().findElement(By.id("confirmation__btn-confirm")).click();
    }
}
