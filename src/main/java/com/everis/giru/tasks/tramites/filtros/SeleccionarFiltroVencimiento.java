package com.everis.giru.tasks.tramites.filtros;

import com.everis.giru.userinterface.tramites.FiltrosBandejaPage;
import com.everis.giru.userinterface.tramites.filtrosPage.GetFiltroVencimientoPage;
import com.everis.giru.userinterface.tramites.validaFiltros.ValidaFiltroVencimientoPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class SeleccionarFiltroVencimiento implements Task {

    private final String departamento;
    private final String perfil;
    private final String tipologia;
    private final String fVencimiento;

    GetFiltroVencimientoPage getFiltroVencimientoPage;
    ValidaFiltroVencimientoPage validaFiltroVencimientoPage;

    public SeleccionarFiltroVencimiento(String departamento, String perfil, String tipologia, String fVencimiento) {
        this.departamento = departamento;
        this.perfil = perfil;
        this.tipologia = tipologia;
        this.fVencimiento = fVencimiento;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        if (!(fVencimiento.equals("No"))) {

            actor.attemptsTo(
                    WaitUntil.the(FiltrosBandejaPage.BUTTOM_FILTRO_VENCIMIENTO, isVisible()).forNoMoreThan(100).seconds(),
                    Click.on(FiltrosBandejaPage.BUTTOM_FILTRO_VENCIMIENTO)
            );

            getFiltroVencimientoPage.seleccionaFiltroVencimiento(fVencimiento);

            actor.attemptsTo(
                    WaitUntil.the(FiltrosBandejaPage.BUTTOM_FILTRO_APLICAR, isVisible()).forNoMoreThan(100).seconds(),
                    Click.on(FiltrosBandejaPage.BUTTOM_FILTRO_APLICAR)
            );

            validaFiltroVencimientoPage.validaFiltroVencimiento(departamento, perfil, tipologia, fVencimiento);

        }

    }

    public static Performable withData(String departamento, String perfil, String tipologia, String fVencimiento) {
        return instrumented(SeleccionarFiltroVencimiento.class, departamento, perfil, tipologia, fVencimiento);
    }

}
