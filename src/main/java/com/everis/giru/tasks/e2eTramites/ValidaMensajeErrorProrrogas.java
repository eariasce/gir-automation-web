package com.everis.giru.tasks.e2eTramites;

import com.everis.giru.userinterface.gestion.ProrrogasPage;
import lombok.SneakyThrows;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Upload;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.openqa.selenium.By;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class ValidaMensajeErrorProrrogas implements Task {
    @SneakyThrows
    @Override
    public <T extends Actor> void performAs(T actor) {

        Thread.sleep(5000);
        actor.attemptsTo(
                WaitUntil.the(ProrrogasPage.BTN_ERROR, isVisible()).forNoMoreThan(30).seconds(),
                Click.on(ProrrogasPage.BTN_ERROR)
        );


    }


}
