package com.everis.giru.tasks.e2eTramites;

import com.everis.giru.userinterface.atencionCliente.ValidacionesRegistroPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;

public class IngresaSegmentoComercial implements Task {
    ValidacionesRegistroPage validacionesRegistroPage = new ValidacionesRegistroPage();

    @Override
    public <T extends Actor> void performAs(T actor) {
        validacionesRegistroPage.seleccionarTipoSegmentoComercio();
    }

}
