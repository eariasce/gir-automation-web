package com.everis.giru.tasks.e2eTramites;

import com.everis.giru.utils.ApiCommons;
import com.everis.giru.utils.WebServiceEndPoints;
import io.restassured.RestAssured;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import net.serenitybdd.core.Serenity;
import org.awaitility.Awaitility;

import java.util.concurrent.TimeUnit;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

/**
 * @author Nilo Carrion
 */
public class GetHistoricalInbox {

    //private static final String URL_BASE_HISTORICAL_INBOX_STG = "https://eu2-ibk-apm-stg-ext-001.azure-api.net/giru-stg-inbox/stg";
    private static final String URL_BASE_HISTORICAL_INBOX_UAT = "https://eu2-ibk-apm-uat-ext-001.azure-api.net/giru-inbox/uat";
    //public static final String NPS_SECURITY_SUBSCRIPTION_KEY_STG = "c99a61f28e87494c91b1f49d39854d0d";
    public static final String NPS_SECURITY_SUBSCRIPTION_KEY_UAT = "f954d6fcccd34705a727a228c6bc772e";
    private static final String TEMPLATE_GET_HISTORICAL_INBOX = "/templates/getHistoricalInbox-input.json";
    //public static int contadorIntentosConsultaTramite;

//    GetAssigneeMasiveTask getAssigneeMasiveTask = new GetAssigneeMasiveTask();

    static String departamentoGlobal = null;
    static String tipologiaGlobal = null;

    static String tipologiaGlobalConsultaServicio = null;


    static String idInstance = null;
    static String claimNumber = null;
    static String claimStatusCode = null;
    static String claimStatusDescription = null;
    static String currentAreaCode = null;
    static String currentAreaDescription = null;
    static String currentAreaUserCode = null;
    static String currentAreaUserName = null;
    static String activityDescription = null;
    static String bpmType = null;

    static String cardNumber = null;
//    static String userNameAssigned = null;

    static String numeroTramiteFront = null;

    public static void consultarTramite() {

        RestAssured.baseURI = URL_BASE_HISTORICAL_INBOX_UAT;
        System.out.println(numeroTramiteFront + " .....................................................");

        try {

            Response response = given()
                    .log().all()
                    .contentType(ContentType.JSON)
                    .relaxedHTTPSValidation()
                    .header("Authorization", GetOauthToken.generarAccessTokenGiru())
                    .header("Ocp-Apim-Subscription-Key", NPS_SECURITY_SUBSCRIPTION_KEY_UAT)
                    .queryParam("page", "0")
                    .queryParam("size", "4")
                    .queryParam("sortBy", "FLAG_SUPERVISOR")
                    .queryParam("direction", "DESCENDING")
                    .when()
                    .body(ApiCommons.getTemplate(TEMPLATE_GET_HISTORICAL_INBOX)
                            .replace("{numeroTramite}", numeroTramiteFront)
                    )
                    .post(WebServiceEndPoints.API_HISTORICAL_INBOX.getUrl())
                    .then()
                    .log().all()
                    .assertThat().statusCode(is(equalTo(200)))
                    .extract().response();

            //contadorIntentosConsultaTramite++;
            Awaitility.await().atMost(20, TimeUnit.SECONDS).until(() -> response.statusCode() == 200);
            //System.out.println("Cantida de intentos CONSULTA POST : " + contadorIntentosConsultaTramite + "\n");

            idInstance = response.path("content[0].idInstance").toString();
            claimNumber = response.path("content[0].claimNumber").toString();
            claimStatusCode = response.path("content[0].claimStatusCode").toString();
            claimStatusDescription = response.path("content[0].claimStatusDescription").toString();


            currentAreaCode = response.path("content[0].assignedSpecialistJson[0].currentAreaCode").toString();
            currentAreaDescription = response.path("content[0].assignedSpecialistJson[0].currentAreaDescription").toString();
            currentAreaUserCode = response.path("content[0].assignedSpecialistJson[0].currentAreaUserCode").toString();

            currentAreaUserName = response.path("content[0].assignedSpecialistJson[0].currentAreaUserName").toString();

            activityDescription = response.path("content[0].assignedSpecialistJson[0].activityDescription").toString();
            bpmType = response.path("content[0].bpmType").toString();

            tipologiaGlobal = response.path("content[0].transactionSubtype").toString();

            cardNumber = response.path("content[0].cardNumber").toString();


            Serenity.setSessionVariable("userCode").to(currentAreaUserCode);
            System.out.println("\n" + "*****************************************************************");
            System.out.println("idInstance : " + idInstance);
            System.out.println("claimNumber : " + claimNumber);
            System.out.println("claimStatusCode : " + claimStatusCode);
            System.out.println("claimStatusDescription : " + claimStatusDescription);

            System.out.println("currentAreaCode : " + currentAreaCode);
            System.out.println("currentAreaDescription : " + currentAreaDescription);
            System.out.println("currentAreaUserCode : " + currentAreaUserCode);

            System.out.println("currentAreaUserName : " + currentAreaUserName);

            System.out.println("activityDescription : " + activityDescription);
            System.out.println("bpmType : " + bpmType);
            System.out.println("Tipologia : " + tipologiaGlobal);

            System.out.println("CardNumer : " + cardNumber);

        } catch (Exception e) {
            //if (contadorIntentosConsultaTramite == 5) {
            throw new RuntimeException(e);
            //} else {
//                System.out.println("Cantida de intentos CONSULTA POST : " + contadorIntentosConsultaTramite + "\n");
//                consultarTramite();
//            }

        }


    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setContadorIntentosConsultaTramiteACero() {
        // contadorIntentosConsultaTramite = 0;
    }

    public void tipologiaGlobalTramite() {

        tipologiaGlobalConsultaServicio = tipologiaGlobal;
        switch (tipologiaGlobal) {
            case "Exoneración de Cobros":
                tipologiaGlobal = "ExoneracionCobros";
                break;
            case "Cobros Indebidos":
                tipologiaGlobal = "CobrosIndebidos";

                break;
            case "Anulación de tarjeta de crédito":
                tipologiaGlobal = "AnulacionTC";

                break;
            case "Consumos no reconocidos por fraude":
                tipologiaGlobal = "ConsumosNoReconocidos";

                break;
            case "Consumos mal procesados por el comercio/marca":
                tipologiaGlobal = "ConsumosMalProcesados";
                break;

            default:
                System.out.println("Departamento no encontrado !!!: ");
        }
    }

    public String getTipologiaGlobalConsultaServicio() {
        return tipologiaGlobalConsultaServicio;
    }

    public String getTipologiaGlobal() {
        return tipologiaGlobal;
    }

    public void departamentoGlobalTramite() {

        //TODO: MAPEA LISTA DE USUARIO
//  STG
//        String[] listaUserCode = {"B10951", "B26569", "B4753", "B7889", "B12999"};
//        String[] listaUserName = {"Allecaco Llerena, Juan Pablo", "Mendoza Garcia, Miriam Soledad", "Centurión Mostacero, Juddi  Amnie", "Luna Victoria Weyder, Johanna Catherine", "Reyna Rossi, Monica Mirella"};
//  UAT
//        String[] listaUserCode = {"XT9833", "B26569", "B4753", "B7889", "B12999"};
//        String[] listaUserName = {"Leslie Hugo", "Mendoza Garcia, Miriam Soledad", "Centurión Mostacero, Juddi  Amnie", "Luna Victoria Weyder, Johanna Catherine", "Reyna Rossi, Monica Mirella"};

        switch (currentAreaDescription) {
            case "Gestión De Pedidos Y Reclamos TC":
                departamentoGlobal = "GPYR";

//                for (int i = 0; i < listaUserCode.length; i++) {
//                    for (int j = 0; j < listaUserName.length; j++) {
//
//                        if (listaUserCode[i].equals("XT9833") && listaUserName[j].equals("Leslie Hugo")) {
//                            String userCode = listaUserCode[i];
//                            String userName = listaUserName[j];
//                            userNameAssigned = userName;
//
//                            System.out.println("UserCode : " + i + " = " + userCode + " VS " + "userName : " + j + " = " + userName);
//                            System.out.println(idInstance + " " + claimNumber + " " + claimStatusCode + " " + currentAreaCode + " " + currentAreaDescription + " " + userCode + " " + userName + " " + currentAreaUserCode + " " + activityDescription + " " + bpmType);
//                            getAssigneeMasiveTask.reasignarTramite(idInstance, claimNumber, claimStatusCode, currentAreaCode, currentAreaDescription, userCode, userName, currentAreaUserCode, activityDescription, bpmType);
//                            Serenity.setSessionVariable("userCode").to(userCode);
//                            System.out.println("VALIDACIÓN CORRECTO ");
//
//                        } else {
//                            System.out.println("USUARIO NO VALIDO !!!");
//                        }
//
//                    }
//
//                }
                break;
            case "Dpto-Seguros":
                departamentoGlobal = "Seguros";
//                for (int i = 0; i < listaUserCode.length; i++) {
//                    for (int j = 0; j < listaUserName.length; j++) {
//
//                        if (listaUserCode[i].equals("B26569") && listaUserName[j].equals("Mendoza Garcia, Miriam Soledad")) {
//                            String userCode = listaUserCode[i];
//                            String userName = listaUserName[j];
//                            userNameAssigned = userName;
//                            System.out.println("UserCode : " + i + " = " + userCode + " VS " + "userName : " + j + " = " + userName);
//                            getAssigneeMasiveTask.reasignarTramite(idInstance, claimNumber, claimStatusCode, currentAreaCode, currentAreaDescription, userCode, userName, currentAreaUserCode, activityDescription, bpmType);
//                            Serenity.setSessionVariable("userCode").to(userCode);
//                            System.out.println("VALIDACIÓN CORRECTO ");
//
//                        } else {
//                            System.out.println("USUARIO NO VALIDO !!!");
//                        }
//
//                    }
//
//                }
                break;
            case "Controversias":
                departamentoGlobal = "Controversias";
//                for (int i = 0; i < listaUserCode.length; i++) {
//                    for (int j = 0; j < listaUserName.length; j++) {
//
//                        if (listaUserCode[i].equals("B4753") && listaUserName[j].equals("Centurión Mostacero, Juddi  Amnie")) {
//                            String userCode = listaUserCode[i];
//                            String userName = listaUserName[j];
//                            userNameAssigned = userName;
//                            System.out.println("UserCode : " + i + " = " + userCode + " VS " + "userName : " + j + " = " + userName);
//                            getAssigneeMasiveTask.reasignarTramite(idInstance, claimNumber, claimStatusCode, currentAreaCode, currentAreaDescription, userCode, userName, currentAreaUserCode, activityDescription, bpmType);
//                            Serenity.setSessionVariable("userCode").to(userCode);
//                            System.out.println("VALIDACIÓN CORRECTO ");
//
//                        } else {
//                            System.out.println("USUARIO NO VALIDO !!!");
//                        }
//
//                    }
//
//                }
                break;
            case "U-Segto-Pedidos Recl":
                departamentoGlobal = "USPR";
//                for (int i = 0; i < listaUserCode.length; i++) {
//                    for (int j = 0; j < listaUserName.length; j++) {
//
//                        if (listaUserCode[i].equals("B7889") && listaUserName[j].equals("Luna Victoria Weyder, Johanna Catherine")) {
//                            String userCode = listaUserCode[i];
//                            String userName = listaUserName[j];
//                            userNameAssigned = userName;
//                            System.out.println("UserCode : " + i + " = " + userCode + " VS " + "userName : " + j + " = " + userName);
//                            getAssigneeMasiveTask.reasignarTramite(idInstance, claimNumber, claimStatusCode, currentAreaCode, currentAreaDescription, userCode, userName, currentAreaUserCode, activityDescription, bpmType);
//                            Serenity.setSessionVariable("userCode").to(userCode);
//                            System.out.println("VALIDACIÓN CORRECTO ");
//
//                        } else {
//                            System.out.println("USUARIO NO VALIDO !!!");
//                        }
//
//                    }
//
//                }
                break;
            case "Retención Multiproducto TC":
                departamentoGlobal = "Retenciones";
//                for (int i = 0; i < listaUserCode.length; i++) {
//                    for (int j = 0; j < listaUserName.length; j++) {
//
//                        if (listaUserCode[i].equals("B12999") && listaUserName[j].equals("Reyna Rossi, Monica Mirella")) {
//                            String userCode = listaUserCode[i];
//                            String userName = listaUserName[j];
//                            userNameAssigned = userName;
//                            System.out.println("UserCode : " + i + " = " + userCode + " VS " + "userName : " + j + " = " + userName);
//                            getAssigneeMasiveTask.reasignarTramite(idInstance, claimNumber, claimStatusCode, currentAreaCode, currentAreaDescription, userCode, userName, currentAreaUserCode, activityDescription, bpmType);
//                            Serenity.setSessionVariable("userCode").to(userCode);
//                            System.out.println("VALIDACIÓN CORRECTO ");
//
//                        } else {
//                            System.out.println("USUARIO NO VALIDO !!!");
//                        }
//
//                    }
//
//                }
                break;

            default:
                System.out.println("Departamento no encontrado !!!: ");
        }


    }

    public void setNumeroTramiteFront(String NumTramite) {
        numeroTramiteFront = NumTramite;
    }

    public String getNumeroTramiteFront() {
        return numeroTramiteFront;
    }

    public String getDepartamentoGlobal() {
        return departamentoGlobal;
    }

    public String getClainStatusDescription() {
        return claimStatusDescription;
    }

    public String getClaimNumber() {
        return claimNumber;
    }

//    public String getUserNameAssigned() {
//        return userNameAssigned;
//    }

    public String getcurrentAreaUserName() {
        return currentAreaUserName;
    }

}
