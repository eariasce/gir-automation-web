package com.everis.giru.tasks.e2eTramites;

import com.everis.giru.userinterface.e2eTramite.resolucionTramite.ResumenPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class RealizarAtencionE2E implements Task {

    private final String atencion;

    public RealizarAtencionE2E(String atencion) {
        this.atencion = atencion;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(
                Click.on(ResumenPage.BTN_GESTIONAR_TRAMITE)
        );

        switch (atencion) {
            case "PROCEDE":
                actor.attemptsTo(
                        WaitUntil.the(ResumenPage.BTN_PROCEDE, isVisible()).forNoMoreThan(2).seconds(),
                        Click.on(ResumenPage.BTN_PROCEDE)
                );
                actor.attemptsTo(
                        WaitUntil.the(ResumenPage.BTN_PROCEDE_CONFIRMAR, isVisible()).forNoMoreThan(2).seconds(),
                        Click.on(ResumenPage.BTN_PROCEDE_CONFIRMAR)
                );

                break;
            case "NO PROCEDE":
                actor.attemptsTo(
                        WaitUntil.the(ResumenPage.BTN_NOPROCEDE, isVisible()).forNoMoreThan(2).seconds(),
                        Click.on(ResumenPage.BTN_NOPROCEDE)
                );
                actor.attemptsTo(
                        WaitUntil.the(ResumenPage.BTN_NOPROCEDE_CONFIRMAR, isVisible()).forNoMoreThan(2).seconds(),
                        Click.on(ResumenPage.BTN_NOPROCEDE_CONFIRMAR)
                );

                break;
            case "DERIVAR":
                actor.attemptsTo(
                        WaitUntil.the(ResumenPage.BTN_DERIVAR, isVisible()).forNoMoreThan(2).seconds(),
                        Click.on(ResumenPage.BTN_DERIVAR)

                );

                break;

            default:
                System.out.println("No se encuentra ningun tramite !!!");
        }


    }

    public static Performable withData(String atencion) {
        return instrumented(RealizarAtencionE2E.class, atencion);
    }

}
