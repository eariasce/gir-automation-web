package com.everis.giru.tasks.e2eTramites;

import com.everis.giru.userinterface.e2eTramite.validaBusquedaHistorico.HistorialPage;
import com.everis.giru.userinterface.e2eTramite.validaBusquedaHistorico.IngresaTramiteEnHistorialPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class SeleccionarTipoTramitesHistorial implements Task {

    private final String tipoTramite;
    private final String numeroTramite;
    IngresaTramiteEnHistorialPage ingresaTramiteEnHistorialPage = new IngresaTramiteEnHistorialPage();

    public SeleccionarTipoTramitesHistorial(String tipoTramite, String numeroTramite) {
        this.tipoTramite = tipoTramite;
        this.numeroTramite = numeroTramite;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(
                WaitUntil.the(HistorialPage.CBX_TIPO_DOC_HISTORIAL, isVisible()).forNoMoreThan(30).seconds(),
                Click.on(HistorialPage.CBX_TIPO_DOC_HISTORIAL)
        );

        switch (tipoTramite) {
            case "NRO TRAMITE":
                actor.attemptsTo(
                        WaitUntil.the(HistorialPage.DIV_SELECT_NRO_TRAMITE, isVisible()).forNoMoreThan(100).seconds(),
                        Click.on(HistorialPage.DIV_SELECT_NRO_TRAMITE)
                );
                ingresaTramiteEnHistorialPage.ingresarTramite(numeroTramite);
                break;
            case "DNI":
                actor.attemptsTo(
                        Click.on(HistorialPage.DIV_SELECT_DNI)
                );
                ingresaTramiteEnHistorialPage.ingresarTramite(numeroTramite);
                break;
            case "CE":
                actor.attemptsTo(
                        Click.on(HistorialPage.DIV_SELECT_CE)
                );
                ingresaTramiteEnHistorialPage.ingresarTramite(numeroTramite);
                break;

            default:
                System.out.println("No se encuentra ningun tramite !!!");
        }

    }

    public static Performable withData(String tipoTramite, String numeroTramite) {
        return instrumented(SeleccionarTipoTramitesHistorial.class, tipoTramite, numeroTramite);
    }

}
