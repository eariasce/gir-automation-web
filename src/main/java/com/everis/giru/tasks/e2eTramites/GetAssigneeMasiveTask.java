package com.everis.giru.tasks.e2eTramites;

import com.everis.giru.utils.ApiCommons;
import com.everis.giru.utils.WebServiceEndPoints;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import net.serenitybdd.core.Serenity;
import org.awaitility.Awaitility;

import java.util.concurrent.TimeUnit;

public class GetAssigneeMasiveTask {

    //    private static final String URL_BASE_ASSIGNEE_MASIVE_TASK_STG = "https://eu2-ibk-apm-stg-ext-001.azure-api.net/giru-stg-tasks/stg";
    private static final String URL_BASE_ASSIGNEE_MASIVE_TASK_UAT = "https://eu2-ibk-apm-uat-ext-001.azure-api.net/giru-bpm-tasks/uat";
    //    public static final String NPS_SECURITY_SUBSCRIPTION_KEY_STG = "c99a61f28e87494c91b1f49d39854d0d";
    public static final String NPS_SECURITY_SUBSCRIPTION_KEY_UAT = "f954d6fcccd34705a727a228c6bc772e";
    private static final String TEMPLATE_GET_ASSIGNEE_MASIVE_TASK = "/templates/getAssigneeMasiveTask-input.json";

    public void reasignarTramite(String idInstance, String claimNumber, String claimStatusCode, String currentAreaCode, String currentAreaDescription, String userCode, String userName, String currentAreaUserCode, String activityDescription, String bpmType) {

        RestAssured.baseURI = URL_BASE_ASSIGNEE_MASIVE_TASK_UAT;

        try {

            Response response = RestAssured
                    .given()
                    .log()
                    .all()
                    .relaxedHTTPSValidation()
                    .contentType(ContentType.JSON)
                    .header("Authorization", GetOauthToken.generarAccessTokenGiru())
                    .header("Ocp-Apim-Subscription-Key", NPS_SECURITY_SUBSCRIPTION_KEY_UAT)
                    .body(ApiCommons.getTemplate(TEMPLATE_GET_ASSIGNEE_MASIVE_TASK)

                            .replace("{AreaCode}", currentAreaCode)
                            .replace("{AreaDescription}", currentAreaDescription)

                            .replace("{UserCode}", userCode)
                            .replace("{UserName}", userName)

                            .replace("{idInstance}", idInstance)
                            .replace("{claimNumber}", claimNumber)
                            .replace("{activityDescription}", activityDescription)
                            .replace("{claimStatusCode}", claimStatusCode)
                            .replace("{bpmType}", bpmType)

                            .replace("{currentAreaUserCode}", currentAreaUserCode)

                    )
                    .post(WebServiceEndPoints.API_ASSIGNEE_MASIVE_TASK.getUrl());

            //.then()
            //.log()
            //.all()
            //.extract().asString();
            Awaitility.await().atMost(30, TimeUnit.SECONDS).until(() -> response.statusCode() == 200);
            int statusCode = response.then().extract().path("status");
            String statusCodeOne = Integer.toString(statusCode);
            String message = response.then().extract().path("message");

            //TODO: OBTENER VALORES DEL CUNSULTA INBOX
            //Serenity.setSessionVariable("idInstance").to(idInstance);

            System.out.println("\n" + "*****************************************************************");
            System.out.println("statusCode: " + statusCodeOne);
            System.out.println("message: " + message);

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


}
