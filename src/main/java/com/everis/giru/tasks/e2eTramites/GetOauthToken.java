package com.everis.giru.tasks.e2eTramites;

import com.everis.giru.utils.ApiCommons;
import com.everis.giru.utils.WebServiceEndPoints;
import io.restassured.RestAssured;
import io.restassured.response.Response;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

/**
 * @author Nilo Carrion
 */
public class GetOauthToken {

    //private static final String URL_BASE_LOGIN_STG = "https://eu2-ibk-apm-stg-ext-001.azure-api.net/giru-stg-security/stg";
    private static final String URL_BASE_LOGIN_UAT = "https://eu2-ibk-apm-uat-ext-001.azure-api.net/giru-security/uat";
    //public static final String NPS_SECURITY_SUBSCRIPTION_KEY_SGT = "a71bcdbfc27346059097d8e47bdb51e6";
    public static final String NPS_SECURITY_SUBSCRIPTION_KEY_UAT = "2e1d1dd1d85c4d65a14fc9c1b1f3ebae";

    private static final String TEMPLATE_GET_LOGIN = "/templates/getLogin-input.json";

    public static String generarAccessTokenGiru() {

        RestAssured.baseURI = URL_BASE_LOGIN_UAT;

        try {

            Response response = given()
                    .log().all()
                    .relaxedHTTPSValidation()
                    .contentType("application/json")
                    .header("Authorization", "Basic Z2lydWZyb250OnMyazM0bzRwNXBrbA==")
                    .header("Ocp-Apim-Subscription-Key", NPS_SECURITY_SUBSCRIPTION_KEY_UAT)
                    .when()
                    .body(ApiCommons.getTemplate(TEMPLATE_GET_LOGIN)
                            .replace("{username}", "B22079")
                            .replace("{password}", "Abc12345$")
                    )
                    .post(WebServiceEndPoints.URL_SECURITY_LOGIN.getUrl())
                    .then()
                    .log().all()
                    .assertThat().statusCode(is(equalTo(200)))
                    .assertThat().body("userinfo.username", notNullValue())
                    .extract().response();

            String access_token = "Bearer " + response.path("access_token");
            System.out.println("GIRU GENERAR TOKEN: " + access_token);

            return access_token;

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


}
