package com.everis.giru.tasks.e2eTramites;

import com.everis.giru.questions.e2eTramites.DatosResumenQuestions;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.Matchers.containsStringIgnoringCase;
import static org.hamcrest.Matchers.equalTo;

public class ValidaDatosResumenBandeja implements Task {

    GetHistoricalInbox getHistoricalInbox = new GetHistoricalInbox();

    static String cardNumberServicioConsulta = null;
    static String tipologiaServicioConsulta = null;
    static String numeroTramiteServicioConsulta = null;

    @Override
    public <T extends Actor> void performAs(T actor) {

        cardNumberServicioConsulta = getHistoricalInbox.getCardNumber();
        tipologiaServicioConsulta = getHistoricalInbox.getTipologiaGlobalConsultaServicio();
        numeroTramiteServicioConsulta = getHistoricalInbox.getNumeroTramiteFront();
        
        theActorInTheSpotlight().should(
                seeThat("Valida numero de tarjeta", DatosResumenQuestions.numeroTarjetaResumen(), equalTo(cardNumberServicioConsulta)),
                seeThat("Valida tipología", DatosResumenQuestions.tipologiaResumen(), containsStringIgnoringCase(tipologiaServicioConsulta)),
                seeThat("Valida numero de trámite", DatosResumenQuestions.numeroTramiteResumen(), equalTo(numeroTramiteServicioConsulta))
        );

    }


}
