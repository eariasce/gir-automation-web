package com.everis.giru.tasks.e2eTramites;

import com.everis.giru.userinterface.gestion.ProrrogasPage;
import lombok.SneakyThrows;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Upload;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.openqa.selenium.By;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class EnvioCartasAdjuntaArchivo implements Task {
    @SneakyThrows
    @Override
    public <T extends Actor> void performAs(T actor) {

        Serenity.getDriver().findElement(By.xpath("//*[text()=' Actualizar envío de carta ']")).click();
        //TODO: Adjunta la carta aquí
        actor.attemptsTo(
                Upload.theClasspathResource("archivoReclamo/Carga_EstadoCartas.xlsx").to(Serenity.getDriver().findElement(By.xpath("//input[@id='files_uploader']")))
        );

        //TODO: SE ESPERA A ADJUNTAR CARTA

        actor.attemptsTo(
                WaitUntil.the(ProrrogasPage.BTN_PROCESAR, isVisible()).forNoMoreThan(30).seconds(),
                Click.on(ProrrogasPage.BTN_PROCESAR)
        );
        actor.attemptsTo(
                WaitUntil.the(ProrrogasPage.BTN_CONFIRMAR_PROCESAR, isVisible()).forNoMoreThan(30).seconds(),
                Click.on(ProrrogasPage.BTN_CONFIRMAR_PROCESAR)
        );
        try {
            Thread.sleep(10000);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        actor.attemptsTo(
                WaitUntil.the(ProrrogasPage.BTN_DESCARGAR_ARCHIVO, isVisible()).forNoMoreThan(30).seconds(),
                Click.on(ProrrogasPage.BTN_DESCARGAR_ARCHIVO)
        );
        try {
            Thread.sleep(5000);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        actor.attemptsTo(
                WaitUntil.the(ProrrogasPage.BTN_TERMINAR, isVisible()).forNoMoreThan(30).seconds(),
                Click.on(ProrrogasPage.BTN_TERMINAR)
        );

        try {
            Thread.sleep(5000);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }


}
