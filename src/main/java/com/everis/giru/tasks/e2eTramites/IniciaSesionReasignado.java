package com.everis.giru.tasks.e2eTramites;

import com.everis.giru.userinterface.login.LoginPage;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class IniciaSesionReasignado implements Task {

    String usuarioReasignado = Serenity.sessionVariableCalled("userCode").toString();

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(
                WaitUntil.the(LoginPage.INP_USUARIO, isVisible()).forNoMoreThan(30).seconds(),
                Enter.theValue(usuarioReasignado).into(LoginPage.INP_USUARIO),
                Enter.theValue("Abc12345$").into(LoginPage.INP_PASSWORD)
        );

    }

}
