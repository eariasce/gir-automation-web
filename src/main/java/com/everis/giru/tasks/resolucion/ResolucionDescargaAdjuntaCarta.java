package com.everis.giru.tasks.resolucion;

import lombok.SneakyThrows;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Upload;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;

public class ResolucionDescargaAdjuntaCarta implements Task {
    @SneakyThrows
    @Override
    public <T extends Actor> void performAs(T actor) {
        JavascriptExecutor je = (JavascriptExecutor) Serenity.getDriver();
        je.executeScript("arguments[0].scrollIntoView();", Serenity.getDriver().findElement(By.xpath("//label[@for='sendLetter-1']")));

        //TODO: ¿Enviarás carta al cliente? - SI
        Serenity.getDriver().findElement(By.xpath("//label[@for='sendLetter-1']")).click();
        //TODO: Selecciona - Elige la plantilla que necesites usar
        Serenity.getDriver().findElement(By.id("template")).click();
        Serenity.getDriver().findElement(By.xpath("(//app-ibk-select//div[@id='template']//li)[1]")).click();
        //TODO: Selecciona - Descargar CARTA
        Serenity.getDriver().findElement(By.id("claim-proceed__download")).click();

        try {
            Thread.sleep(3000);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        //TODO: Adjunta la carta aquí
        actor.attemptsTo(
                Upload.theClasspathResource("archivoReclamo/CartaGenerica.docx").to(Serenity.getDriver().findElement(By.xpath("//input[@id='file-upload']")))
        );

        //TODO: SE ESPERA A ADJUNTAR CARTA
        try {
            Thread.sleep(10000);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }


}
