package com.everis.giru.tasks.resolucion;

import net.serenitybdd.core.Serenity;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;

public class ResolucionSeleccionaNoAdjuntarCarta {

    public void resolucionNoAdjuntarCarta(){
        //TODO: ¿Enviarás carta al cliente? - NO
        JavascriptExecutor je = (JavascriptExecutor) Serenity.getDriver();
        je.executeScript("arguments[0].scrollIntoView();", Serenity.getDriver().findElement(By.xpath("//label[@for='sendLetter-0']")));

        Serenity.getDriver().findElement(By.xpath("//label[@for='sendLetter-0']")).click();
    }
}
