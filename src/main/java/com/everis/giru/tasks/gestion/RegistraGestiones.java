package com.everis.giru.tasks.gestion;

import com.everis.giru.userinterface.gestion.GestionesPage;
import com.everis.giru.utils.ApiCommons;
import com.everis.tdm.Do;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.openqa.selenium.By;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class RegistraGestiones implements Task {

    private final String gestionDescripcion;
    private final String comentario;
    private final String llamada;

    public RegistraGestiones(String gestionDescripcion, String comentario, String llamada) {
                this.gestionDescripcion = gestionDescripcion;
                this.comentario = comentario;
            this.llamada = llamada;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {



        actor.attemptsTo(WaitUntil.the(GestionesPage.BTN_REGISTRAR_GESTION, isVisible()).forNoMoreThan(100).seconds(),
                Click.on(GestionesPage.BTN_REGISTRAR_GESTION));


        ApiCommons.tipoGestion = Do.spGetTipoGestionByGestionPadre(gestionDescripcion);

        actor.attemptsTo(
                Click.on(GestionesPage.CBX_TIPOGESTIONES)
        );


        if(Serenity.getDriver().findElements(By.xpath("//span[text() = '" + ApiCommons.tipoGestion.get(0).getDescripcionGestion() + "']")).size() > 0)
            {
                Serenity.getDriver().findElement(By.xpath("//span[text() = '" + ApiCommons.tipoGestion.get(0).getDescripcionGestion() + "']")).click();

            }
            else
            {
                Serenity.getDriver().findElement(By.xpath("//li[@id='" + ApiCommons.tipoGestion.get(0).getIdTipoGestion() + "']")).click();

            }


            actor.attemptsTo(
                Click.on(GestionesPage.CBX_GESTIONES)
        );

        if(Serenity.getDriver().findElements(By.xpath("(//span[contains(text(), '" + ApiCommons.tipoGestion.get(0).getDescripcionGestionHija() + "')])[last()]")).size() > 0) {
            Serenity.getDriver().findElement(By.xpath("(//span[contains(text(), '" + ApiCommons.tipoGestion.get(0).getDescripcionGestionHija() + "')])[last()]")).click();
        }else {
            Serenity.getDriver().findElement(By.xpath("//li[@id='" + ApiCommons.tipoGestion.get(0).getIdTipoGestionHija() + "']")).click();
        }

        actor.attemptsTo(
                Click.on(GestionesPage.CBX_TIPOLLAMADA)
        );
        switch (llamada) {
            case "In":
                actor.attemptsTo(
                        Click.on(GestionesPage.CBX_TIPOLLAMADA_IN)
                );
                break;
            case "Out":
                actor.attemptsTo(
                        Click.on(GestionesPage.CBX_TIPOLLAMADA_OUT)
                );
                break;
            default:
                System.out.println("No se encuentra el tipo de llamada!!!");
        }
        Enter.theValue(comentario).into(GestionesPage.CBX_COMENTARIOS);
        actor.attemptsTo(Click.on(GestionesPage.BTN_COMPLETAR_REGISTRO_GESTION));

        ApiCommons.GESTIONES_NRO_TRAMITE = Serenity.getDriver().findElement(By.xpath("//*[contains(text(), 'NÚMERO DE GESTIÓN')]/..//span[2]")).getText();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        Serenity.getDriver().findElement(By.xpath("//*[contains(text(), ' Terminar atención ')]")).click();


    }

    public static Performable withData(String gestionDescripcion, String comentario, String llamada) {
        return instrumented(RegistraGestiones.class, gestionDescripcion, comentario,llamada);
    }
}
