package com.everis.giru.tasks.gestion;

import com.everis.giru.questions.login.PerfilQuestions;
import com.everis.giru.userinterface.e2eTramite.resolucionTramite.ResumenPage;
import com.everis.giru.userinterface.gestion.GestionesPage;
import com.everis.giru.userinterface.tramites.TramitesPage;
import com.everis.giru.utils.ApiCommons;
import com.google.protobuf.Api;
import com.ibm.icu.impl.UResource;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;
import static org.hamcrest.Matchers.equalTo;

public class IngresaBusquedaGestiones implements Task {

    private final String tipoDocumento;
    private final String numeroDocumento;

    public IngresaBusquedaGestiones(String tipoDocumento, String numeroDocumento) {
        this.tipoDocumento = tipoDocumento;
        this.numeroDocumento = numeroDocumento;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        switch (ApiCommons.GESTIONES_FUENTE_DATA) {
            case "Giru":
                actor.attemptsTo(
                        Click.on(GestionesPage.BTN_FILTRO_GIRU));
                break;
            case "Telesoft":
                actor.attemptsTo(
                        Click.on(GestionesPage.BTN_FILTRO_TELESOFT));
                break;
           }

           if(!ApiCommons.GESTIONES_FUENTE_DATA.equals("Telesoft")) {
               actor.attemptsTo(
                       Click.on(GestionesPage.BTN_CLIENTE_GESTIONES)
               );
           }
        actor.attemptsTo(
                Click.on(GestionesPage.CBX_TIPO_DOCUMENTO)
        );

        switch (tipoDocumento) {
            case "DNI":
                actor.attemptsTo(
                        Click.on(GestionesPage.CBX_TIPO_DOCUMENTO_DNI));
                break;
            case "CE":
                actor.attemptsTo(
                        Click.on(GestionesPage.CBX_TIPO_DOCUMENTO_CE));
                break;
            case "PTP":
                actor.attemptsTo(
                        Click.on(GestionesPage.CBX_TIPO_DOCUMENTO_PTP));
                break;
            case "PASAPORTE":
                actor.attemptsTo(
                        Click.on(GestionesPage.CBX_TIPO_DOCUMENTO_PASAPORTE));
                break;
            case "RUC":
                actor.attemptsTo(
                        Click.on(GestionesPage.CBX_TIPO_DOCUMENTO_RUC));
                break;
            default:
                System.out.println("No se encuentra el documento!!!");
        }

        Serenity.getDriver().findElement(By.xpath("//ibk-inputgroup-text[@formcontrolname='document']")).sendKeys(numeroDocumento);

        Serenity.getDriver().findElement(By.xpath("//ibk-inputgroup-text[@formcontrolname='document']")).sendKeys(Keys.ENTER);

//        actor.attemptsTo(
//        Enter.theValue(numeroDocumento).into(GestionesPage.TXT_BUSCAR_GESTION)
//        );
//        actor.attemptsTo(
//        Enter.theValue(Keys.ENTER).into(GestionesPage.TXT_BUSCAR_GESTION));

    }

    public static Performable withData(String tipoDocumento, String numeroDocumento) {
        return instrumented(IngresaBusquedaGestiones.class, tipoDocumento, numeroDocumento);
    }

}
