package com.everis.giru.tasks.gestion;

import com.everis.giru.userinterface.gestion.GestionesPage;
import com.everis.giru.userinterface.tramites.historial.ValidaNumeroTramiteHistorialPage;
import com.everis.giru.utils.ApiCommons;
import com.everis.tdm.Do;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class ValidaGestiones implements Task {
    ValidaNumeroTramiteHistorialPage validaNumeroTramiteHistorialPage;
    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(WaitUntil.the(GestionesPage.BTN_USUARIO_GESTIONES, isVisible()).forNoMoreThan(100).seconds(),
                Click.on(GestionesPage.BTN_USUARIO_GESTIONES));

       // actor.attemptsTo(Enter.theValue(ApiCommons.NOMBRE_USUARIO).into(GestionesPage.CBX_COMENTARIOS));
        Serenity.getDriver().findElement(By.xpath("//ibk-inputgroup-text[@type='icon']")).sendKeys(ApiCommons.NOMBRE_USUARIO.replace("EVERIS.",""));
        try{  Thread.sleep(2000);}catch (Exception e){}

        Serenity.getDriver().findElement(By.xpath("//div[@class='search-input flex']//li[1]")).click();
        try{  Thread.sleep(1000);}catch (Exception e){}

        Serenity.getDriver().findElement(By.xpath("//div[@class='search-input flex']//li[1]")).click();
        try{  Thread.sleep(1000);}catch (Exception e){}

        validaNumeroTramiteHistorialPage.clickBusquedaGestiones();

    }

}
