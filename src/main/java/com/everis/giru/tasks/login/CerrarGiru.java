package com.everis.giru.tasks.login;

import com.everis.giru.userinterface.login.ButtonSalirPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;

public class CerrarGiru implements Task {
    
    ButtonSalirPage buttonSalirPage;

    public <T extends Actor> void performAs(T actor) {

        buttonSalirPage.cerrarSesion();

    }

}
