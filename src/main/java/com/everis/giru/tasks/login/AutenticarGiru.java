package com.everis.giru.tasks.login;

import com.everis.giru.userinterface.login.LoginPage;
import net.serenitybdd.annotations.Step;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

/**
 * @author Nilo Carrion
 */
public class AutenticarGiru implements Task {

    private final String user;
    private final String password;

    public AutenticarGiru(String user, String password) {
        this.user = user;
        this.password = password;
    }

    @Override
    @Step("{0} {0} Ingresamos datos de usuario")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                WaitUntil.the(LoginPage.INP_USUARIO, isVisible()).forNoMoreThan(150).seconds(),
                Enter.theValue(user).into(LoginPage.INP_USUARIO),
                WaitUntil.the(LoginPage.INP_PASSWORD, isVisible()).forNoMoreThan(150).seconds(),
                Enter.theValue(password).into(LoginPage.INP_PASSWORD)
        );
    }

    public static Performable withData(String user, String password) {
        return instrumented(AutenticarGiru.class, user, password);
    }

}
