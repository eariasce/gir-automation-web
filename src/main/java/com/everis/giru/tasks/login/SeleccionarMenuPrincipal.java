package com.everis.giru.tasks.login;

import com.everis.giru.userinterface.login.MenuPrincipalPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class SeleccionarMenuPrincipal implements Task {

    private final String menuPrincipal;

    public SeleccionarMenuPrincipal(String menuPrincipal) {
        this.menuPrincipal = menuPrincipal;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        
        switch (menuPrincipal) {
            case "Administrador":
                actor.attemptsTo(
                        WaitUntil.the(MenuPrincipalPage.NAV_ADMINISTRADOR, isVisible()).forNoMoreThan(100).seconds(),
                        Click.on(MenuPrincipalPage.NAV_ADMINISTRADOR)
                );
                break;
            case "Dashboard":
                actor.attemptsTo(
                        WaitUntil.the(MenuPrincipalPage.NAV_DASHBOARD, isVisible()).forNoMoreThan(100).seconds(),
                        Click.on(MenuPrincipalPage.NAV_DASHBOARD)
                );
                break;
            case "Tramites":
                actor.attemptsTo(
                        WaitUntil.the(MenuPrincipalPage.NAV_TRAMITES, isVisible()).forNoMoreThan(300).seconds(),
                        Click.on(MenuPrincipalPage.NAV_TRAMITES)
                );
                break;
            case "AtencionCliente":
                actor.attemptsTo(
                        WaitUntil.the(MenuPrincipalPage.NAV_ATENCIONALCLIENTE, isVisible()).forNoMoreThan(300).seconds(),
                        Click.on(MenuPrincipalPage.NAV_ATENCIONALCLIENTE)
                );
                break;
            case "Historial":
                actor.attemptsTo(
                        WaitUntil.the(MenuPrincipalPage.NAV_HISTORIAL, isVisible()).forNoMoreThan(100).seconds(),
                        Click.on(MenuPrincipalPage.NAV_HISTORIAL)
                );
                break;
            case "Pure Connect":
                actor.attemptsTo(
                        WaitUntil.the(MenuPrincipalPage.NAV_PURECONNECT, isVisible()).forNoMoreThan(100).seconds(),
                        Click.on(MenuPrincipalPage.NAV_PURECONNECT)
                );
                break;
            case "Gestiones":
                actor.attemptsTo(
                        WaitUntil.the(MenuPrincipalPage.NAV_HISTORIAL, isVisible()).forNoMoreThan(100).seconds(),
                        Click.on(MenuPrincipalPage.NAV_GESTIONES)
                );
                break;
            case "Procesos Masivos":
                actor.attemptsTo(
                        WaitUntil.the(MenuPrincipalPage.NAV_HISTORIAL, isVisible()).forNoMoreThan(100).seconds(),
                        Click.on(MenuPrincipalPage.NAV_PROCESOS_MASIVOS)
                );
                break;
            default:
                System.out.println("No se encuentra ningún Menú Principal !!!");
        }
    }

    public static Performable withData(String menuPrincipal) {
        return instrumented(SeleccionarMenuPrincipal.class, menuPrincipal);
    }
}
