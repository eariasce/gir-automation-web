package com.everis.giru.tasks.login;

import com.everis.giru.utils.ApiCommons;
import com.google.protobuf.Api;
import net.serenitybdd.annotations.Step;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.model.environment.EnvironmentSpecificConfiguration;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;
import net.thucydides.model.util.EnvironmentVariables;

import static net.serenitybdd.screenplay.Tasks.instrumented;

/**
 * @author Nilo Carrion
 */
public class AbrirGiru implements Task {

    EnvironmentVariables environmentVariables;

    private final String url;

    public AbrirGiru(String url) {
        this.url = url;
    }

    @Override
    @Step("{0} Inicia la página #url")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Open.url(EnvironmentSpecificConfiguration.from(environmentVariables).getProperty(url))
        );
        ApiCommons.AMBIENTE = EnvironmentSpecificConfiguration.from(environmentVariables).getProperty("giru.enviroment");
        Serenity.setSessionVariable("ambiente").to(EnvironmentSpecificConfiguration.from(environmentVariables).getProperty("giru.enviroment"));
        Serenity.setSessionVariable("urlgiru").to(EnvironmentSpecificConfiguration.from(environmentVariables).getProperty("giru.page"));
       ApiCommons.documentType = EnvironmentSpecificConfiguration.from(environmentVariables).getProperty("giru.documento");

    }

    public static Task loginGiruPage() {
        String url = "giru.page";
        return instrumented(AbrirGiru.class, url);
    }

}
