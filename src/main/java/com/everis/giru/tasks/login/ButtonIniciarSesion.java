package com.everis.giru.tasks.login;

import com.everis.giru.userinterface.login.LoginPage;
import net.serenitybdd.annotations.Step;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

/**
 * @author Nilo Carrion
 */
public class ButtonIniciarSesion implements Task {

    @Override
    @Step("Click en Iniciar Sesion")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                WaitUntil.the(LoginPage.BTN_INICIAR_SESION, isVisible()).forNoMoreThan(100).seconds(),
                Click.on(LoginPage.BTN_INICIAR_SESION)
        );
    }

}
