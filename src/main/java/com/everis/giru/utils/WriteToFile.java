package com.everis.giru.utils;


import java.io.*;
public class WriteToFile {

    public void write(String args) throws IOException {

        PrintWriter writer= new PrintWriter("src/test/java/ImporteYFechaPago/session.data","UTF-8");
        writer.println(args);
        //writer.println("{'sesioncode':'"+ args+"'}");

        writer.flush();
        writer.close();
    }


    public String read() throws IOException {
        String line = "";
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(
                    "src/test/java/ImporteYFechaPago/session.data"));
            line = reader.readLine();
            //while (() != null) {
                System.out.println("session" + line);
            //}
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return line;
    }
    public static void writeLinesToFile(String msg)  {
        String fileName = "src/test/java/resultados.csv";
        PrintWriter printWriter = null;
        File file = new File(fileName);
        try {
            if (!file.exists()) file.createNewFile();
            printWriter = new PrintWriter(new FileOutputStream(fileName, true));


            printWriter.write(msg + "\r\n");
        } catch (IOException ioex) {
            ioex.printStackTrace();
        } finally {
            if (printWriter != null) {
                printWriter.flush();
                printWriter.close();
            }
        }


    }

    public static void writeLinesToFileGestion(String msg) {
        String fileName = "src/test/java/gestiones.csv";
        PrintWriter printWriter = null;
        File file = new File(fileName);
        try {
            if (!file.exists()) file.createNewFile();
            printWriter = new PrintWriter(new FileOutputStream(fileName, true));


            printWriter.write(msg + "\r\n");
        } catch (IOException ioex) {
            ioex.printStackTrace();
        } finally {
            if (printWriter != null) {
                printWriter.flush();
                printWriter.close();
            }
        }

    }
    public static void writeLinesToFileResol(String msg) {
        String fileName = "src/test/java/resultados2daInstancia.csv";
        PrintWriter printWriter = null;
        File file = new File(fileName);
        try {
            if (!file.exists()) file.createNewFile();
            printWriter = new PrintWriter(new FileOutputStream(fileName, true));


            printWriter.write(msg + "\r\n");
        } catch (IOException ioex) {
            ioex.printStackTrace();
        } finally {
            if (printWriter != null) {
                printWriter.flush();
                printWriter.close();
            }
        }

    }

    public static void writeLinesToFileCartaValidados(String msg) {
        String fileName = "src/test/java/cartaValidados.csv";
        PrintWriter printWriter = null;
        File file = new File(fileName);
        try {
            if (!file.exists()) file.createNewFile();
            printWriter = new PrintWriter(new FileOutputStream(fileName, true));


            printWriter.write(msg + "\r\n");
        } catch (IOException ioex) {
            ioex.printStackTrace();
        } finally {
            if (printWriter != null) {
                printWriter.flush();
                printWriter.close();
            }
        }
    }


    public static void writeLinesToFileProrrogaValidados(String msg) {
        String fileName = "src/test/java/prorrogaValidados.csv";
        PrintWriter printWriter = null;
        File file = new File(fileName);
        try {
            if (!file.exists()) file.createNewFile();
            printWriter = new PrintWriter(new FileOutputStream(fileName, true));


            printWriter.write(msg + "\r\n");
        } catch (IOException ioex) {
            ioex.printStackTrace();
        } finally {
            if (printWriter != null) {
                printWriter.flush();
                printWriter.close();
            }
        }
    }

    public static void writeLinesToFileProrroga(String msg) {
        String fileName = "src/test/java/prorroga.csv";
        PrintWriter printWriter = null;
        File file = new File(fileName);
        try {
            if (!file.exists()) file.createNewFile();
            printWriter = new PrintWriter(new FileOutputStream(fileName, true));


            printWriter.write(msg + "\r\n");
        } catch (IOException ioex) {
            ioex.printStackTrace();
        } finally {
            if (printWriter != null) {
                printWriter.flush();
                printWriter.close();
            }
        }
    }



    public static void writeLinesToFileValidaciones(String msg) {
        String fileName = "src/test/java/resolutadosUsuarios.csv";
        PrintWriter printWriter = null;
        File file = new File(fileName);
        try {
            if (!file.exists()) file.createNewFile();
            printWriter = new PrintWriter(new FileOutputStream(fileName, true));


            printWriter.write(msg + "\r\n");
        } catch (IOException ioex) {
            ioex.printStackTrace();
        } finally {
            if (printWriter != null) {
                printWriter.flush();
                printWriter.close();
            }
        }
    }


    public static void convertProrrogaCSVtoXLSX() throws Exception {
          ExcelFileService.csvToXLSX();
        Thread.sleep(5000);

    }



}