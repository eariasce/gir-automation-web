package com.everis.giru.utils;

import com.github.seratch.jslack.Slack;
import com.github.seratch.jslack.api.webhook.Payload;
import net.serenitybdd.core.Serenity;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class NotificacionSlack {

    public static final String URL = "https://hooks.slack.com/services/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";
    //public static final String TOKEN = "xoxb-4965901863828-4976091311761-19cu6vEGvjuc6xBnEYe8Pyvr";
    public static final String CHANNEL = "notificaciones";//"certificación";**"aadi-webhook";

    public String enviarMensajeSlack(String enviarMensajeSlack){
        try{
            String ambiente = Serenity.sessionVariableCalled("ambiente").toString();
            String urlgiru = Serenity.sessionVariableCalled("urlgiru").toString();
            String escenario = Serenity.sessionVariableCalled("scenario");
            StringBuilder msjBuilder = new StringBuilder();
            msjBuilder.append(ambiente + " - " + urlgiru + " - " + escenario + "\n" +enviarMensajeSlack);
            Payload payload = Payload.builder().channel(CHANNEL).text(msjBuilder.toString()).build();

            //WebhookResponse webhookResponse =
            Slack.getInstance().send(URL, payload);

        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public static void enviarmensajeSlack(Exception e)
    {
       // StringWriter errors = new StringWriter();
       // e.printStackTrace(new PrintWriter(errors));
        //SLACK
        String imageName = "";
        String filename = "." + File.separator + "screenshots";
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH_mm_ss");
        LocalDateTime now = LocalDateTime.now();
        String fecha = dtf.format(now);

        String filename2 = "." + File.separator + fecha;

        String rutacompleta = "http://jenkins-server2.ddns.net:8081/errorcaptures/" + fecha+".png";
        File f = ((TakesScreenshot)  Serenity.getDriver()).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(f, new File(filename + ".png"));
            FileUtils.copyFile(f, new File(System.getProperty("user.dir") + "\\errorcaptures\\"+ filename2 + ".png"));

            File source2 = new File("C:\\jenkins\\dummy");
            File dest2 = new File("C:\\jenkins\\dummy2");

            File source = new File(System.getProperty("user.dir") + "\\errorcaptures");
            File dest = new File("C:\\Users\\Public\\tomcat\\webapps\\errorcaptures");

            FileUtils.copyDirectory(source2, dest2);
            FileUtils.copyDirectory(source, dest);


        } catch (IOException g) {
            System.out.println("ruta se enviara con slack");
            rutacompleta = "";
            //g.printStackTrace();
        }
        imageName = filename + ".png";
        try {
            if(rutacompleta.equals("")) {
                SlackIncomingWebhook.slackWebhookPostRequest("notificaciones", ImageUploadImgur.uploadImageToImgur(imageName), "Error!");
            }
        } catch (Exception h) {
            h.printStackTrace();
        }

        NotificacionSlack slack = new NotificacionSlack();
        String texto = rutacompleta +" Se obtuvo el siguiente error: " + e.getMessage() + "\n";// + errors;
        slack.enviarMensajeSlack(texto);
        //SLACK END
    }

}
