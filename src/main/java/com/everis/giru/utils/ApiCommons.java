package com.everis.giru.utils;

import net.serenitybdd.model.environment.EnvironmentSpecificConfiguration;
import net.thucydides.model.util.EnvironmentVariables;
import org.apache.commons.io.IOUtils;
import org.springframework.core.io.ClassPathResource;

import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Properties;
import com.everis.tdm.model.giru.*;


/**
 * @author Nilo Carrion
 */
public class ApiCommons {

    public static String AMBIENTE;
    public static String SUBIARCHIVO;
    EnvironmentVariables variables;
    public static String USUARIO;
    public static String PERFIL_USUARIO;

    public static String NOMBRE_USUARIO;
    public static String GESTIONES_FUENTE_DATA;
    public static String GESTIONES_TIPO_BUSQUEDA;
    public static String GESTIONES_NRO_TRAMITE;
    public static String GESTIONES_TIPO_DOC;
    public static String ID_GESTION;
    public static List<ClientData> clientData;
    public static List<TipoGestion> tipoGestion;
    public static RegistroGestion registroGestion;

    public static ClienteTarjeta clientes;
    public static String documentType;

    public String getVariableSerenity(String tipoVariable) {
        return EnvironmentSpecificConfiguration.from(variables).getProperty(tipoVariable);
    }
    public Properties properties() {

        Properties prop = null;
        try {
            prop = new Properties();
            String propFileName = "serenity.properties";

            prop.load(new FileReader(propFileName));

        } catch (Exception e) {
            System.out.println("Exception: " + e);
        }

        return prop;
    }

    public static String getTemplate(String templatePath) {
        try {
            return IOUtils.toString(new ClassPathResource(templatePath).getInputStream(), StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
