package com.everis.giru.utils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ExcelFileService {

    public static void removeRow(Sheet sheet, int rowIndex) {
        int lastRowNum = sheet.getLastRowNum();
        if (rowIndex >= 0 && rowIndex < lastRowNum) {
            sheet.shiftRows(rowIndex + 1, lastRowNum, -1);
        }
        if (rowIndex == lastRowNum) {
            Row removingRow = sheet.getRow(rowIndex);
            if (removingRow != null) {
                sheet.removeRow(removingRow);
            }
        }
    }

    public static void eliminoTramiteValidado(String tramite) throws IOException {
        String excelFilePath = "src/test/resources/archivoReclamo/Carga_Prorrogas.xlsx";


        FileInputStream inputStream = new FileInputStream(new File(excelFilePath));
        Workbook workbook = WorkbookFactory.create(inputStream);

        //VALIDATE
        Sheet sheet = workbook.getSheetAt(0);
        String text1 = "";

        FileInputStream file = new FileInputStream(new File("src/test/resources/archivoReclamo/Carga_Prorrogas.xlsx"));
        XSSFWorkbook workbook2 = new XSSFWorkbook(file);

        Sheet sheet2 = workbook2.getSheetAt(0);


        for (Row row : sheet2) {

            Cell cell = row.getCell(0);

            try {
                text1 = String.valueOf(cell.getNumericCellValue()).substring(0,6);
            }
            catch (Exception e){
                text1 = String.valueOf(cell.getStringCellValue());
            }
            if(!text1.contains("Nro trámite"))
            {
                if(text1.equals(tramite))
                {
                    removeRow(sheet,row.getRowNum());
                }
            }

        }

        inputStream.close();

        FileOutputStream outputStream = new FileOutputStream(excelFilePath);
        workbook.write(outputStream);
        workbook.close();
        outputStream.close();


    }


    public static void eliminoTramiteValidadoCartas(String tramite) throws IOException {
        String excelFilePath = "src/test/resources/archivoReclamo/Carga_EstadoCartas.xlsx";


        FileInputStream inputStream = new FileInputStream(new File(excelFilePath));
        Workbook workbook = WorkbookFactory.create(inputStream);

        //VALIDATE
        Sheet sheet = workbook.getSheetAt(0);
        String text1 = "";

        FileInputStream file = new FileInputStream(new File("src/test/resources/archivoReclamo/Carga_EstadoCartas.xlsx"));
        XSSFWorkbook workbook2 = new XSSFWorkbook(file);

        Sheet sheet2 = workbook2.getSheetAt(0);


        for (Row row : sheet2) {

            Cell cell = row.getCell(0);

            try {
                text1 = String.valueOf(cell.getNumericCellValue()).substring(0,6);
            }
            catch (Exception e){
                text1 = String.valueOf(cell.getStringCellValue());
            }
            if(!text1.contains("Nro trámite"))
            {
                if(text1.equals(tramite))
                {
                    removeRow(sheet,row.getRowNum());
                }
            }

        }

        inputStream.close();

        FileOutputStream outputStream = new FileOutputStream(excelFilePath);
        workbook.write(outputStream);
        workbook.close();
        outputStream.close();


    }
    public static void csvToXLSX() {

        try {
            String csvFileAddress = "src/test/java/prorroga.csv"; //csv file address
            String xlsxFileAddress = "src/test/resources/archivoReclamo/Prorroga.xlsx"; //xlsx file address
            XSSFWorkbook workBook = new XSSFWorkbook();
            XSSFSheet sheet = workBook.createSheet("sheet1");
            String currentLine = null;
            int RowNum = -1;
            BufferedReader br = new BufferedReader(new FileReader(csvFileAddress));
            while ((currentLine = br.readLine()) != null) {
                String str[] = currentLine.split(",");
                RowNum++;
                XSSFRow currentRow = sheet.createRow(RowNum);
                for (int i = 0; i < str.length; i++) {
                    currentRow.createCell(i).setCellValue(str[i]);
                }
            }

            FileOutputStream fileOutputStream = new FileOutputStream(xlsxFileAddress);
            workBook.write(fileOutputStream);
            fileOutputStream.close();
            System.out.println("Done");
        } catch (Exception ex) {
            System.out.println(ex.getMessage() + "Exception in try");
        }
    }

    public static List<String> ProrrogasTramiteList() throws IOException {
        List<String> listadoProrrogas = new ArrayList<>();
        FileInputStream file = new FileInputStream(new File("src/test/resources/archivoReclamo/Carga_Prorrogas.xlsx"));


        XSSFWorkbook workbook = new XSSFWorkbook(file);

        Sheet sheet = workbook.getSheetAt(0);
        String text1 = "";

        for (Row row : sheet) {

            Cell cell = row.getCell(0);

            try {
                text1 = String.valueOf(cell.getNumericCellValue()).substring(0,6);
            }
            catch (Exception e){
                text1 = String.valueOf(cell.getStringCellValue());
            }
            if(!text1.contains("Nro trámite"))
            listadoProrrogas.add(text1);

        }

        return listadoProrrogas;
    }

    public static List<String> ProrrogasDiasList() throws IOException {
        List<String> listadoProrrogas = new ArrayList<>();
        FileInputStream file = new FileInputStream(new File("src/test/resources/archivoReclamo/Carga_Prorrogas.xlsx"));

//Create Workbook instance holding reference to .xlsx file
        XSSFWorkbook workbook = new XSSFWorkbook(file);

        Sheet sheet = workbook.getSheetAt(0); // Get Your Sheet.
        String text2 = "";

        for (Row row : sheet) { // For each Row.
            Cell cell2 = row.getCell(1); // Get the Cell at the Index / Column you want.

            try {
                text2 = String.valueOf(cell2.getNumericCellValue()).substring(0,2);
            }
            catch (Exception e){
                text2 = String.valueOf(cell2.getStringCellValue());
            }
            if(!text2.contains("Días de prórroga"))
                listadoProrrogas.add(text2);
        }

        return listadoProrrogas;
    }


    public static List<String> EnvioCartasTramiteList() throws IOException {
        List<String> listadoProrrogas = new ArrayList<>();
        FileInputStream file = new FileInputStream(new File("src/test/resources/archivoReclamo/Carga_EstadoCartas.xlsx"));
        XSSFWorkbook workbook = new XSSFWorkbook(file);

        Sheet sheet = workbook.getSheetAt(0);
        String text1 = "";

        for (Row row : sheet) {

            Cell cell = row.getCell(0);

            try {
                text1 = String.valueOf(cell.getNumericCellValue()).substring(0,6);
            }
            catch (Exception e){
                text1 = String.valueOf(cell.getStringCellValue());
            }
            if(!text1.contains("Nro trámite"))
                listadoProrrogas.add(text1);

        }

        return listadoProrrogas;
    }


    public static List<String> EnvioCartasFechaList() throws IOException {
        List<String> listadoProrrogas = new ArrayList<>();
        FileInputStream file = new FileInputStream(new File("src/test/resources/archivoReclamo/Carga_EstadoCartas.xlsx"));
        XSSFWorkbook workbook = new XSSFWorkbook(file);

        Sheet sheet = workbook.getSheetAt(0);
        String text1 = "";

        for (Row row : sheet) {

            Cell cell = row.getCell(1);

            try {
                text1 = String.valueOf(cell.getNumericCellValue()).substring(0,6);
            }
            catch (Exception e){
                text1 = String.valueOf(cell.getStringCellValue());
            }
            if(!text1.contains("Fecha de recepción"))
                listadoProrrogas.add(text1);

        }

        return listadoProrrogas;
    }

    public static List<String> EnvioCartasEstadoEnvioList() throws IOException {
        List<String> listadoProrrogas = new ArrayList<>();
        FileInputStream file = new FileInputStream(new File("src/test/resources/archivoReclamo/Carga_EstadoCartas.xlsx"));
        XSSFWorkbook workbook = new XSSFWorkbook(file);

        Sheet sheet = workbook.getSheetAt(0);
        String text1 = "";

        for (Row row : sheet) {

            Cell cell = row.getCell(2);

            try {
                text1 = String.valueOf(cell.getNumericCellValue()).substring(0,6);
            }
            catch (Exception e){
                text1 = String.valueOf(cell.getStringCellValue());
            }
            if(!text1.contains("Estado de envío"))
                listadoProrrogas.add(text1);

        }

        return listadoProrrogas;
    }


    public static List<String> EnvioCartasComentarioList() throws IOException {
        List<String> listadoProrrogas = new ArrayList<>();
        FileInputStream file = new FileInputStream(new File("src/test/resources/archivoReclamo/Carga_EstadoCartas.xlsx"));
        XSSFWorkbook workbook = new XSSFWorkbook(file);

        Sheet sheet = workbook.getSheetAt(0);
        String text1 = "";

        for (Row row : sheet) {

            Cell cell = row.getCell(3);

            try {
                text1 = String.valueOf(cell.getNumericCellValue()).substring(0,6);
            }
            catch (Exception e){
                text1 = String.valueOf(cell.getStringCellValue());
            }
            if(!text1.contains("Comentario"))
                listadoProrrogas.add(text1);
        }

        return listadoProrrogas;
    }


    public static void addTramitesEnvioCartas(String tramite, String fecharecepcion, String estadoenvio, String comentariocarta) throws IOException {
        String excelFilePath = "src/test/resources/archivoReclamo/Carga_EstadoCartas.xlsx";


        FileInputStream inputStream = new FileInputStream(new File(excelFilePath));
        Workbook workbook = WorkbookFactory.create(inputStream);

        Sheet sheet = workbook.getSheetAt(0);
        int tramitenum = Integer.parseInt(tramite);



        Object[][] bookData = {
                {tramitenum, fecharecepcion,estadoenvio,comentariocarta}};

        int rowCount = sheet.getLastRowNum();

        for (Object[] aBook : bookData) {
            Row row = sheet.createRow(++rowCount);

            int columnCount = 0;

            Cell cell;// row.createCell(columnCount);
            //cell.setCellValue(rowCount);

            for (Object field : aBook) {

                cell = row.createCell(columnCount);

                if(field instanceof Integer)
                {
                    cell.setCellValue((Integer) field);
                }
               else
                {
                    String value = (String) field;

                    if(value.contains("/"))
                    {
                      try {
                          CellStyle cellStyle = workbook.createCellStyle();
                          CreationHelper createHelper = workbook.getCreationHelper();
                          cellStyle.setDataFormat(
                                  createHelper.createDataFormat().getFormat("dd/mm/yyyy"));

                          SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);

                          Date date = formatter.parse((String) field);

                          cell.setCellValue((date));
                          cell.setCellStyle(cellStyle);
                      }catch (Exception e)
                      {
                          System.out.println(e);
                      }
                    }
                    else {
                        cell.setCellValue((String) field);
                    }
                }
                columnCount++;
            }


        }

        inputStream.close();

        FileOutputStream outputStream = new FileOutputStream(excelFilePath);
        workbook.write(outputStream);
        workbook.close();
        outputStream.close();


    }

    public static void addTramitesProrroga(String tramite, String prorroga) throws IOException {
        String excelFilePath = "src/test/resources/archivoReclamo/Carga_Prorrogas.xlsx";


            FileInputStream inputStream = new FileInputStream(new File(excelFilePath));
            Workbook workbook = WorkbookFactory.create(inputStream);

            Sheet sheet = workbook.getSheetAt(0);
            int tramitenum = Integer.parseInt(tramite);
            int prorroganum = Integer.parseInt(prorroga);

            Object[][] bookData = {
                    {tramitenum, prorroganum}
            };

            int rowCount = sheet.getLastRowNum();

            for (Object[] aBook : bookData) {
                Row row = sheet.createRow(++rowCount);

                int columnCount = 0;

                Cell cell;// row.createCell(columnCount);
                //cell.setCellValue(rowCount);

                for (Object field : aBook) {
                    cell = row.createCell(columnCount);
                        cell.setCellValue((Integer) field);

                    columnCount++;
                }


            }

            inputStream.close();

            FileOutputStream outputStream = new FileOutputStream(excelFilePath);
            workbook.write(outputStream);
            workbook.close();
            outputStream.close();


    }

}


