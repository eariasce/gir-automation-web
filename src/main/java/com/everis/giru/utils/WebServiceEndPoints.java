package com.everis.giru.utils;

public enum WebServiceEndPoints {

    URL_SECURITY_LOGIN("/login"),
    API_HISTORICAL_INBOX("/inbox/search/filterHistorical"),
    API_ASSIGNEE_MASIVE_TASK("/bpm-tasks/assignee-masive-task");

    private final String url;

    WebServiceEndPoints(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

}
