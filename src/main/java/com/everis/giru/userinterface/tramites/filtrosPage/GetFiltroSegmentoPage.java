package com.everis.giru.userinterface.tramites.filtrosPage;

import com.everis.giru.utils.Shadow;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class GetFiltroSegmentoPage extends PageObject {

    Shadow general = new Shadow();

    @FindBy(xpath = "//*[@id='radio-list__radio-option']")
    WebElement LBL_ID_RADIOBUTTOM_SEGMENTO;

    public void seleccionaFiltroSegmento(String fSegmento) {

        switch (fSegmento) {
            case "Segmento Joven":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SEGMENTO));
                WebElement btnJoven = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SEGMENTO, "div > div:nth-child(1) > label", "", "", "");
                btnJoven.click();
                break;
            case "Segmento Premium":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SEGMENTO));
                WebElement btnPorPremium = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SEGMENTO, "div > div:nth-child(2) > label", "", "", "");
                btnPorPremium.click();
                break;
            case "Segmento Preferente":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SEGMENTO));
                WebElement btnPreferente = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SEGMENTO, "div > div:nth-child(3) > label", "", "", "");
                btnPreferente.click();
                break;
            case "Segmento Emprendedor":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SEGMENTO));
                WebElement btnEmprendedor = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SEGMENTO, "div > div:nth-child(4) > label", "", "", "");
                btnEmprendedor.click();
                break;
            case "Empresario":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SEGMENTO));
                WebElement btnEmpresario = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SEGMENTO, "div > div:nth-child(5) > label", "", "", "");
                btnEmpresario.click();
                break;
            case "Pequeño Empresario":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SEGMENTO));
                WebElement btnPequenoEmpresario = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SEGMENTO, "div > div:nth-child(6) > label", "", "", "");
                btnPequenoEmpresario.click();
                break;
            case "Microempresario":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SEGMENTO));
                WebElement btnMicroempresario = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SEGMENTO, "div > div:nth-child(7) > label", "", "", "");
                btnMicroempresario.click();
                break;
            case "Segmento Persona Natural sin Negocio (PNSN)":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SEGMENTO));
                WebElement btnPersonaNaturalsinNegocio = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SEGMENTO, "div > div:nth-child(8) > label", "", "", "");
                btnPersonaNaturalsinNegocio.click();
                break;
            case "Segmento Informal":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SEGMENTO));
                WebElement btnInformal = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SEGMENTO, "div > div:nth-child(9) > label", "", "", "");
                btnInformal.click();
                break;
            case "Segmento Select":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SEGMENTO));
                WebElement btnSelect = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SEGMENTO, "div > div:nth-child(10) > label", "", "", "");
                btnSelect.click();
                break;
            case "Segmento Consumo Inicial":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SEGMENTO));
                WebElement btnConsumoInicial = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SEGMENTO, "div > div:nth-child(11) > label", "", "", "");
                btnConsumoInicial.click();
                break;
            case "Cliente Nuevo":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SEGMENTO));
                WebElement btnClienteNuevo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SEGMENTO, "div > div:nth-child(12) > label", "", "", "");
                btnClienteNuevo.click();
                break;
            case "No Segmentado":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SEGMENTO));
                WebElement btnNoSegmentado = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SEGMENTO, "div > div:nth-child(13) > label", "", "", "");
                btnNoSegmentado.click();
                break;

            default:
                System.out.println("No se encuentra Filtro Segmento!!!");
        }

    }


}
