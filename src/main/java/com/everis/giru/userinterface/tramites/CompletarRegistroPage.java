package com.everis.giru.userinterface.tramites;

import com.everis.giru.utils.Shadow;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import org.junit.Assert;
import org.openqa.selenium.WebElement;

public class CompletarRegistroPage extends PageObject {

    private Shadow general = new Shadow();
    @FindBy(xpath = "//*[@id=\"header__giru\"]")
    WebElement nombreArea;

    public void validoArea(String area) {


       // String nombreAsesorValidar = general.expandContainer("2css", nombreArea, "div > div.usuario > ibk-breadcrum:nth-child(1)", "div > p", "").getText();

//        Serenity.setSessionVariable("nombreAsesorValidacion").to(nombreAsesorValidar);

        if (area.equals("USPR")) {
            WebElement texto = general.expandContainer("2css", nombreArea, "div > div.usuario > ibk-breadcrum:nth-child(2)", "div > p", "","");

            if (texto.getText().equals("U-SEGTO-PEDIDOS RECL")) {
                //Bien

            } else {
                Assert.assertEquals(area, texto.getText());
            }


        }

        if (area.equals("GDPR")) {
            WebElement texto = general.expandContainer("2css", nombreArea, "div > div.usuario > ibk-breadcrum:nth-child(2)", "div > p", "","");

            if (texto.getText().equals("GESTIÓN DE PEDIDOS Y RECLAMOS TC")) {
                //Bien

            } else {
                Assert.assertEquals(area, texto.getText());
            }


        }

        if (area.equals("RMTC")) {
            WebElement texto = general.expandContainer("2css", nombreArea, "div > div.usuario > ibk-breadcrum:nth-child(2)", "div > p", "","");

            if (texto.getText().equals("RETENCIÓN MULTIPRODUCTO TC")) {
                //Bien

            } else {
                Assert.assertEquals(area, texto.getText());
            }


        }


        if (area.equals("GDPR")) {
            WebElement texto = general.expandContainer("2css", nombreArea, "div > div.usuario > ibk-breadcrum:nth-child(2)", "div > p", "", "");

            if (texto.getText().equals("GESTIÓN DE PEDIDOS Y RECLAMOS TC")) {
                //Bien

            } else {
                Assert.assertEquals(area, texto.getText());
            }


        }

        if (area.equals("Seguros")) {
            WebElement texto = general.expandContainer("2css", nombreArea, "div > div.usuario > ibk-breadcrum:nth-child(2)", "div > p", "","");

            if (texto.getText().equals("DPTO-SEGUROS")) {
                //Bien

            } else {
                Assert.assertEquals(area, texto.getText());
            }



        }

        if (area.equals("CONTROVERSIAS")) {
            WebElement texto = general.expandContainer("2css", nombreArea, "div > div.usuario > ibk-breadcrum:nth-child(2)", "div > p", "", "");

            if (texto.getText().equals("CONTROVERSIAS")) {
                //Bien

            } else {
                Assert.assertEquals(area, texto.getText());
            }
        }

    }


}
