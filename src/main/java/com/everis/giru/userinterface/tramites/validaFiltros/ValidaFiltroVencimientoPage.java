package com.everis.giru.userinterface.tramites.validaFiltros;

import com.everis.giru.utils.Shadow;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.WebElement;

import static org.assertj.core.api.Assertions.assertThat;

public class ValidaFiltroVencimientoPage extends PageObject {

    Shadow general = new Shadow();

    public int contador = 0;

    //MAPEO DE ELEMENTOS - VALIDACIÓN FILTROS
    @FindBy(xpath = "//*[@id='grid']/giru-specialist-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR;

    @FindBy(xpath = "//*[@id='grid']/giru-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SUPERVISOR_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-my-team/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-to-assigned/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-to-resolve/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR;

    @FindBy(xpath = "//*[@id='grid']/giru-specialist-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ESPECIALISTA_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-specialist-to-monitor/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS;

    @FindBy(xpath = "//*[@id='grid']/giru-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SUPERVISOR_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-my-team/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-assigned/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-resolve/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-monitor/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS;

    @FindBy(xpath = "//*[@id='grid']/giru-specialist-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-specialist-to-monitor/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-specialist-to-transactions-in-process/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS;

    @FindBy(xpath = "//*[@id='grid']/giru-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-my-team/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-assigned/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-resolve/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-monitor/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-supervisor-trx/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS;

    @FindBy(xpath = "//*[@id='grid']/giru-specialist-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-task/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR;

    @FindBy(xpath = "//*[@id='grid']/giru-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SUPERVISOR_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-my-team/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-to-assigned/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-to-resolve/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR;

    //TODO: Validacion de filtro - Vencimiento
    public boolean validaFiltroVencimiento(String departamento, String perfil, String tipologia, String fVencimiento) {
        boolean validadorFiltroVencimiento = false;
        char[] guion = {'-'};

        switch (departamento) {
            case "GPYR":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_GPYR, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validadorFiltroVencimiento = true;
                                    }
                                } catch (Exception e) {
                                    if (fVencimiento.equals("Nuevos")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueVencimiento = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                WebElement valueVencimiento2 = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");

                                                String getVencimiento = valueVencimiento.getText();
                                                String getVencimiento2 = valueVencimiento2.getText();

                                                System.out.println("Vencimiento encontrado: " + getVencimiento + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getVencimiento.toCharArray();
                                                char[] array_vencimiento2 = getVencimiento2.toCharArray();

                                                String numeroString = "";
                                                String numeroString2 = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                if (array_vencimiento2[0] == guion[0]) {
                                                    numeroString2 += guion[0];
                                                }

                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                for (int k = 0; k < array_vencimiento2.length; ++k) {
                                                    if (Character.isDigit(array_vencimiento2[k])) {
                                                        numeroString2 += array_vencimiento2[k];
                                                    }
                                                }

                                                System.out.println("Numero Cliente: " + numeroString + " Numero Area: " + numeroString2);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                int numeroInt2 = Integer.parseInt(numeroString2);

                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                assertThat(numeroInt2).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("PorVencer")) {

                                    } else if (fVencimiento.equals("VencidosArea")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosCliente")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    }

                                }
                                break;
                            case "CobrosIndebidos":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validadorFiltroVencimiento = true;
                                    }
                                } catch (Exception e) {
                                    if (fVencimiento.equals("Nuevos")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueVencimiento = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                WebElement valueVencimiento2 = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");

                                                String getVencimiento = valueVencimiento.getText();
                                                String getVencimiento2 = valueVencimiento2.getText();

                                                System.out.println("Vencimiento encontrado: " + getVencimiento + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getVencimiento.toCharArray();
                                                char[] array_vencimiento2 = getVencimiento2.toCharArray();

                                                String numeroString = "";
                                                String numeroString2 = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                if (array_vencimiento2[0] == guion[0]) {
                                                    numeroString2 += guion[0];
                                                }

                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                for (int k = 0; k < array_vencimiento2.length; ++k) {
                                                    if (Character.isDigit(array_vencimiento2[k])) {
                                                        numeroString2 += array_vencimiento2[k];
                                                    }
                                                }

                                                System.out.println("Numero Cliente: " + numeroString + " Numero Area: " + numeroString2);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                int numeroInt2 = Integer.parseInt(numeroString2);

                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                assertThat(numeroInt2).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("PorVencer")) {

                                    } else if (fVencimiento.equals("VencidosArea")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosCliente")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    }

                                }
                                break;
                            case "ExoneracionCobros":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validadorFiltroVencimiento = true;
                                    }
                                } catch (Exception e) {
                                    if (fVencimiento.equals("Nuevos")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueVencimiento = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                WebElement valueVencimiento2 = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");

                                                String getVencimiento = valueVencimiento.getText();
                                                String getVencimiento2 = valueVencimiento2.getText();

                                                System.out.println("Vencimiento encontrado: " + getVencimiento + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getVencimiento.toCharArray();
                                                char[] array_vencimiento2 = getVencimiento2.toCharArray();

                                                String numeroString = "";
                                                String numeroString2 = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                if (array_vencimiento2[0] == guion[0]) {
                                                    numeroString2 += guion[0];
                                                }

                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                for (int k = 0; k < array_vencimiento2.length; ++k) {
                                                    if (Character.isDigit(array_vencimiento2[k])) {
                                                        numeroString2 += array_vencimiento2[k];
                                                    }
                                                }

                                                System.out.println("Numero Cliente: " + numeroString + " Numero Area: " + numeroString2);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                int numeroInt2 = Integer.parseInt(numeroString2);

                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                assertThat(numeroInt2).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("PorVencer")) {


                                    } else if (fVencimiento.equals("VencidosArea")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosCliente")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    }

                                }
                                break;
                            case "AnulacionTC":

                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validadorFiltroVencimiento = true;
                                    }
                                } catch (Exception e) {
                                    if (fVencimiento.equals("Nuevos")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueVencimiento = general.expandContainer("2css", LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                WebElement valueVencimiento2 = general.expandContainer("2css", LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");

                                                String getVencimiento = valueVencimiento.getText();
                                                String getVencimiento2 = valueVencimiento2.getText();

                                                System.out.println("Vencimiento encontrado: " + getVencimiento + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getVencimiento.toCharArray();
                                                char[] array_vencimiento2 = getVencimiento2.toCharArray();

                                                String numeroString = "";
                                                String numeroString2 = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                if (array_vencimiento2[0] == guion[0]) {
                                                    numeroString2 += guion[0];
                                                }

                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                for (int k = 0; k < array_vencimiento2.length; ++k) {
                                                    if (Character.isDigit(array_vencimiento2[k])) {
                                                        numeroString2 += array_vencimiento2[k];
                                                    }
                                                }

                                                System.out.println("Numero Cliente: " + numeroString + " Numero Area: " + numeroString2);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                int numeroInt2 = Integer.parseInt(numeroString2);

                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                assertThat(numeroInt2).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("PorVencer")) {

                                    } else if (fVencimiento.equals("VencidosArea")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosCliente")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    }
                                }
                                break;
                            case "FinalizacionTC":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validadorFiltroVencimiento = true;
                                    }
                                } catch (Exception e) {
                                    if (fVencimiento.equals("Nuevos")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueVencimiento = general.expandContainer("2css", LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                WebElement valueVencimiento2 = general.expandContainer("2css", LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");

                                                String getVencimiento = valueVencimiento.getText();
                                                String getVencimiento2 = valueVencimiento2.getText();

                                                System.out.println("Vencimiento encontrado: " + getVencimiento + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getVencimiento.toCharArray();
                                                char[] array_vencimiento2 = getVencimiento2.toCharArray();

                                                String numeroString = "";
                                                String numeroString2 = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                if (array_vencimiento2[0] == guion[0]) {
                                                    numeroString2 += guion[0];
                                                }

                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                for (int k = 0; k < array_vencimiento2.length; ++k) {
                                                    if (Character.isDigit(array_vencimiento2[k])) {
                                                        numeroString2 += array_vencimiento2[k];
                                                    }
                                                }

                                                System.out.println("Numero Cliente: " + numeroString + " Numero Area: " + numeroString2);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                int numeroInt2 = Integer.parseInt(numeroString2);

                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                assertThat(numeroInt2).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("PorVencer")) {

                                    } else if (fVencimiento.equals("VencidosArea")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosCliente")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    }

                                }
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA VALIDAR FILTRO VENCIMIENTO: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                    //TODO: Supervisor GPYR
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_GPYR, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validadorFiltroVencimiento = true;
                                    }
                                } catch (Exception e) {
                                    if (fVencimiento.equals("Nuevos")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueVencimiento = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                WebElement valueVencimiento2 = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");

                                                String getVencimiento = valueVencimiento.getText();
                                                String getVencimiento2 = valueVencimiento2.getText();

                                                System.out.println("Vencimiento encontrado: " + getVencimiento + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getVencimiento.toCharArray();
                                                char[] array_vencimiento2 = getVencimiento2.toCharArray();

                                                String numeroString = "";
                                                String numeroString2 = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                if (array_vencimiento2[0] == guion[0]) {
                                                    numeroString2 += guion[0];
                                                }

                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                for (int k = 0; k < array_vencimiento2.length; ++k) {
                                                    if (Character.isDigit(array_vencimiento2[k])) {
                                                        numeroString2 += array_vencimiento2[k];
                                                    }
                                                }

                                                System.out.println("Numero Cliente: " + numeroString + " Numero Area: " + numeroString2);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                int numeroInt2 = Integer.parseInt(numeroString2);

                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                assertThat(numeroInt2).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("PorVencer")) {

                                    } else if (fVencimiento.equals("VencidosArea")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosCliente")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    }
                                }
                                break;
                            case "MiEquipo":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validadorFiltroVencimiento = true;
                                    }
                                } catch (Exception e) {
                                    if (fVencimiento.equals("Nuevos")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueVencimiento = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                WebElement valueVencimiento2 = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4) > ibk-label-badge", "", "");

                                                String getVencimiento = valueVencimiento.getText();
                                                String getVencimiento2 = valueVencimiento2.getText();

                                                System.out.println("Vencimiento encontrado: " + getVencimiento + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getVencimiento.toCharArray();
                                                char[] array_vencimiento2 = getVencimiento2.toCharArray();

                                                String numeroString = "";
                                                String numeroString2 = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                if (array_vencimiento2[0] == guion[0]) {
                                                    numeroString2 += guion[0];
                                                }

                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                for (int k = 0; k < array_vencimiento2.length; ++k) {
                                                    if (Character.isDigit(array_vencimiento2[k])) {
                                                        numeroString2 += array_vencimiento2[k];
                                                    }
                                                }

                                                System.out.println("Numero Cliente: " + numeroString + " Numero Area: " + numeroString2);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                int numeroInt2 = Integer.parseInt(numeroString2);

                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                assertThat(numeroInt2).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("PorVencer")) {

                                    } else if (fVencimiento.equals("VencidosArea")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosCliente")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    }
                                }
                                break;
                            case "Asignar":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validadorFiltroVencimiento = true;
                                    }
                                } catch (Exception e) {
                                    if (fVencimiento.equals("Nuevos")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueVencimiento = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                WebElement valueVencimiento2 = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4) > ibk-label-badge", "", "");

                                                String getVencimiento = valueVencimiento.getText();
                                                String getVencimiento2 = valueVencimiento2.getText();

                                                System.out.println("Vencimiento encontrado: " + getVencimiento + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getVencimiento.toCharArray();
                                                char[] array_vencimiento2 = getVencimiento2.toCharArray();

                                                String numeroString = "";
                                                String numeroString2 = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                if (array_vencimiento2[0] == guion[0]) {
                                                    numeroString2 += guion[0];
                                                }

                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                for (int k = 0; k < array_vencimiento2.length; ++k) {
                                                    if (Character.isDigit(array_vencimiento2[k])) {
                                                        numeroString2 += array_vencimiento2[k];
                                                    }
                                                }

                                                System.out.println("Numero Cliente: " + numeroString + " Numero Area: " + numeroString2);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                int numeroInt2 = Integer.parseInt(numeroString2);

                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                assertThat(numeroInt2).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("PorVencer")) {

                                    } else if (fVencimiento.equals("VencidosArea")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosCliente")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    }
                                }
                                break;
                            case "Resolver":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validadorFiltroVencimiento = true;
                                    }
                                } catch (Exception e) {
                                    if (fVencimiento.equals("Nuevos")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueVencimiento = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                WebElement valueVencimiento2 = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4) > ibk-label-badge", "", "");

                                                String getVencimiento = valueVencimiento.getText();
                                                String getVencimiento2 = valueVencimiento2.getText();

                                                System.out.println("Vencimiento encontrado: " + getVencimiento + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getVencimiento.toCharArray();
                                                char[] array_vencimiento2 = getVencimiento2.toCharArray();

                                                String numeroString = "";
                                                String numeroString2 = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                if (array_vencimiento2[0] == guion[0]) {
                                                    numeroString2 += guion[0];
                                                }

                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                for (int k = 0; k < array_vencimiento2.length; ++k) {
                                                    if (Character.isDigit(array_vencimiento2[k])) {
                                                        numeroString2 += array_vencimiento2[k];
                                                    }
                                                }

                                                System.out.println("Numero Cliente: " + numeroString + " Numero Area: " + numeroString2);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                int numeroInt2 = Integer.parseInt(numeroString2);

                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                assertThat(numeroInt2).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("PorVencer")) {

                                    } else if (fVencimiento.equals("VencidosArea")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosCliente")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    }
                                }

                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA VALIDAR FILTRO VENCIMIENTO: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                }
                break;
            case "Seguros":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validadorFiltroVencimiento = true;
                                    }
                                } catch (Exception e) {
                                    if (fVencimiento.equals("Nuevos")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueVencimiento = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                WebElement valueVencimiento2 = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");

                                                String getVencimiento = valueVencimiento.getText();
                                                String getVencimiento2 = valueVencimiento2.getText();

                                                System.out.println("Vencimiento encontrado: " + getVencimiento + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getVencimiento.toCharArray();
                                                char[] array_vencimiento2 = getVencimiento2.toCharArray();

                                                String numeroString = "";
                                                String numeroString2 = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                if (array_vencimiento2[0] == guion[0]) {
                                                    numeroString2 += guion[0];
                                                }

                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                for (int k = 0; k < array_vencimiento2.length; ++k) {
                                                    if (Character.isDigit(array_vencimiento2[k])) {
                                                        numeroString2 += array_vencimiento2[k];
                                                    }
                                                }

                                                System.out.println("Numero Cliente: " + numeroString + " Numero Area: " + numeroString2);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                int numeroInt2 = Integer.parseInt(numeroString2);

                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                assertThat(numeroInt2).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("PorVencer")) {

                                    } else if (fVencimiento.equals("VencidosArea")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosCliente")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    }

                                }
                                break;
                            case "CobrosIndebidos":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validadorFiltroVencimiento = true;
                                    }
                                } catch (Exception e) {
                                    if (fVencimiento.equals("Nuevos")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueVencimiento = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                WebElement valueVencimiento2 = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");

                                                String getVencimiento = valueVencimiento.getText();
                                                String getVencimiento2 = valueVencimiento2.getText();

                                                System.out.println("Vencimiento encontrado: " + getVencimiento + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getVencimiento.toCharArray();
                                                char[] array_vencimiento2 = getVencimiento2.toCharArray();

                                                String numeroString = "";
                                                String numeroString2 = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                if (array_vencimiento2[0] == guion[0]) {
                                                    numeroString2 += guion[0];
                                                }

                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                for (int k = 0; k < array_vencimiento2.length; ++k) {
                                                    if (Character.isDigit(array_vencimiento2[k])) {
                                                        numeroString2 += array_vencimiento2[k];
                                                    }
                                                }

                                                System.out.println("Numero Cliente: " + numeroString + " Numero Area: " + numeroString2);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                int numeroInt2 = Integer.parseInt(numeroString2);

                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                assertThat(numeroInt2).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("PorVencer")) {

                                    } else if (fVencimiento.equals("VencidosArea")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosCliente")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    }

                                }
                                break;
                            case "ExoneracionCobros":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validadorFiltroVencimiento = true;
                                    }
                                } catch (Exception e) {
                                    if (fVencimiento.equals("Nuevos")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueVencimiento = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                WebElement valueVencimiento2 = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");

                                                String getVencimiento = valueVencimiento.getText();
                                                String getVencimiento2 = valueVencimiento2.getText();

                                                System.out.println("Vencimiento encontrado: " + getVencimiento + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getVencimiento.toCharArray();
                                                char[] array_vencimiento2 = getVencimiento2.toCharArray();

                                                String numeroString = "";
                                                String numeroString2 = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                if (array_vencimiento2[0] == guion[0]) {
                                                    numeroString2 += guion[0];
                                                }

                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                for (int k = 0; k < array_vencimiento2.length; ++k) {
                                                    if (Character.isDigit(array_vencimiento2[k])) {
                                                        numeroString2 += array_vencimiento2[k];
                                                    }
                                                }

                                                System.out.println("Numero Cliente: " + numeroString + " Numero Area: " + numeroString2);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                int numeroInt2 = Integer.parseInt(numeroString2);

                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                assertThat(numeroInt2).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("PorVencer")) {

                                    } else if (fVencimiento.equals("VencidosArea")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosCliente")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    }
                                }
                                break;
                            case "ConsumosNoReconocidos":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validadorFiltroVencimiento = true;
                                    }
                                } catch (Exception e) {
                                    if (fVencimiento.equals("Nuevos")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueVencimiento = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                WebElement valueVencimiento2 = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");

                                                String getVencimiento = valueVencimiento.getText();
                                                String getVencimiento2 = valueVencimiento2.getText();

                                                System.out.println("Vencimiento encontrado: " + getVencimiento + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getVencimiento.toCharArray();
                                                char[] array_vencimiento2 = getVencimiento2.toCharArray();

                                                String numeroString = "";
                                                String numeroString2 = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                if (array_vencimiento2[0] == guion[0]) {
                                                    numeroString2 += guion[0];
                                                }

                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                for (int k = 0; k < array_vencimiento2.length; ++k) {
                                                    if (Character.isDigit(array_vencimiento2[k])) {
                                                        numeroString2 += array_vencimiento2[k];
                                                    }
                                                }

                                                System.out.println("Numero Cliente: " + numeroString + " Numero Area: " + numeroString2);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                int numeroInt2 = Integer.parseInt(numeroString2);

                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                assertThat(numeroInt2).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("PorVencer")) {

                                    } else if (fVencimiento.equals("VencidosArea")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosCliente")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    }

                                }
                                break;
                            case "Monitorear":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validadorFiltroVencimiento = true;
                                    }
                                } catch (Exception e) {
                                    if (fVencimiento.equals("Nuevos")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueVencimiento = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                WebElement valueVencimiento2 = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");

                                                String getVencimiento = valueVencimiento.getText();
                                                String getVencimiento2 = valueVencimiento2.getText();

                                                System.out.println("Vencimiento encontrado: " + getVencimiento + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getVencimiento.toCharArray();
                                                char[] array_vencimiento2 = getVencimiento2.toCharArray();

                                                String numeroString = "";
                                                String numeroString2 = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                if (array_vencimiento2[0] == guion[0]) {
                                                    numeroString2 += guion[0];
                                                }

                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                for (int k = 0; k < array_vencimiento2.length; ++k) {
                                                    if (Character.isDigit(array_vencimiento2[k])) {
                                                        numeroString2 += array_vencimiento2[k];
                                                    }
                                                }

                                                System.out.println("Numero Cliente: " + numeroString + " Numero Area: " + numeroString2);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                int numeroInt2 = Integer.parseInt(numeroString2);

                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                assertThat(numeroInt2).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("PorVencer")) {

                                    } else if (fVencimiento.equals("VencidosArea")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosCliente")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    }

                                }

                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA VALIDAR FILTRO VENCIMIENTO: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validadorFiltroVencimiento = true;
                                    }
                                } catch (Exception e) {
                                    if (fVencimiento.equals("Nuevos")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueVencimiento = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                WebElement valueVencimiento2 = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");

                                                String getVencimiento = valueVencimiento.getText();
                                                String getVencimiento2 = valueVencimiento2.getText();

                                                System.out.println("Vencimiento encontrado: " + getVencimiento + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getVencimiento.toCharArray();
                                                char[] array_vencimiento2 = getVencimiento2.toCharArray();

                                                String numeroString = "";
                                                String numeroString2 = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                if (array_vencimiento2[0] == guion[0]) {
                                                    numeroString2 += guion[0];
                                                }

                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                for (int k = 0; k < array_vencimiento2.length; ++k) {
                                                    if (Character.isDigit(array_vencimiento2[k])) {
                                                        numeroString2 += array_vencimiento2[k];
                                                    }
                                                }

                                                System.out.println("Numero Cliente: " + numeroString + " Numero Area: " + numeroString2);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                int numeroInt2 = Integer.parseInt(numeroString2);

                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                assertThat(numeroInt2).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("PorVencer")) {

                                    } else if (fVencimiento.equals("VencidosArea")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosCliente")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    }

                                }

                                break;
                            case "MiEquipo":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validadorFiltroVencimiento = true;
                                    }
                                } catch (Exception e) {
                                    if (fVencimiento.equals("Nuevos")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueVencimiento = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                WebElement valueVencimiento2 = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4) > ibk-label-badge", "", "");

                                                String getVencimiento = valueVencimiento.getText();
                                                String getVencimiento2 = valueVencimiento2.getText();

                                                System.out.println("Vencimiento encontrado: " + getVencimiento + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getVencimiento.toCharArray();
                                                char[] array_vencimiento2 = getVencimiento2.toCharArray();

                                                String numeroString = "";
                                                String numeroString2 = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                if (array_vencimiento2[0] == guion[0]) {
                                                    numeroString2 += guion[0];
                                                }

                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                for (int k = 0; k < array_vencimiento2.length; ++k) {
                                                    if (Character.isDigit(array_vencimiento2[k])) {
                                                        numeroString2 += array_vencimiento2[k];
                                                    }
                                                }

                                                System.out.println("Numero Cliente: " + numeroString + " Numero Area: " + numeroString2);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                int numeroInt2 = Integer.parseInt(numeroString2);

                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                assertThat(numeroInt2).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("PorVencer")) {

                                    } else if (fVencimiento.equals("VencidosArea")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosCliente")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    }
                                }

                                break;
                            case "Asignar":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validadorFiltroVencimiento = true;
                                    }
                                } catch (Exception e) {
                                    if (fVencimiento.equals("Nuevos")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueVencimiento = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                WebElement valueVencimiento2 = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4) > ibk-label-badge", "", "");

                                                String getVencimiento = valueVencimiento.getText();
                                                String getVencimiento2 = valueVencimiento2.getText();

                                                System.out.println("Vencimiento encontrado: " + getVencimiento + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getVencimiento.toCharArray();
                                                char[] array_vencimiento2 = getVencimiento2.toCharArray();

                                                String numeroString = "";
                                                String numeroString2 = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                if (array_vencimiento2[0] == guion[0]) {
                                                    numeroString2 += guion[0];
                                                }

                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                for (int k = 0; k < array_vencimiento2.length; ++k) {
                                                    if (Character.isDigit(array_vencimiento2[k])) {
                                                        numeroString2 += array_vencimiento2[k];
                                                    }
                                                }

                                                System.out.println("Numero Cliente: " + numeroString + " Numero Area: " + numeroString2);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                int numeroInt2 = Integer.parseInt(numeroString2);

                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                assertThat(numeroInt2).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("PorVencer")) {

                                    } else if (fVencimiento.equals("VencidosArea")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosCliente")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    }
                                }

                                break;
                            case "Resolver":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validadorFiltroVencimiento = true;
                                    }
                                } catch (Exception e) {
                                    if (fVencimiento.equals("Nuevos")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueVencimiento = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                WebElement valueVencimiento2 = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4) > ibk-label-badge", "", "");

                                                String getVencimiento = valueVencimiento.getText();
                                                String getVencimiento2 = valueVencimiento2.getText();

                                                System.out.println("Vencimiento encontrado: " + getVencimiento + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getVencimiento.toCharArray();
                                                char[] array_vencimiento2 = getVencimiento2.toCharArray();

                                                String numeroString = "";
                                                String numeroString2 = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                if (array_vencimiento2[0] == guion[0]) {
                                                    numeroString2 += guion[0];
                                                }

                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                for (int k = 0; k < array_vencimiento2.length; ++k) {
                                                    if (Character.isDigit(array_vencimiento2[k])) {
                                                        numeroString2 += array_vencimiento2[k];
                                                    }
                                                }

                                                System.out.println("Numero Cliente: " + numeroString + " Numero Area: " + numeroString2);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                int numeroInt2 = Integer.parseInt(numeroString2);

                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                assertThat(numeroInt2).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("PorVencer")) {

                                    } else if (fVencimiento.equals("VencidosArea")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosCliente")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    }
                                }

                                break;
                            case "Monitorear":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validadorFiltroVencimiento = true;
                                    }
                                } catch (Exception e) {
                                    if (fVencimiento.equals("Nuevos")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueVencimiento = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                WebElement valueVencimiento2 = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4) > ibk-label-badge", "", "");

                                                String getVencimiento = valueVencimiento.getText();
                                                String getVencimiento2 = valueVencimiento2.getText();

                                                System.out.println("Vencimiento encontrado: " + getVencimiento + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getVencimiento.toCharArray();
                                                char[] array_vencimiento2 = getVencimiento2.toCharArray();

                                                String numeroString = "";
                                                String numeroString2 = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                if (array_vencimiento2[0] == guion[0]) {
                                                    numeroString2 += guion[0];
                                                }

                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                for (int k = 0; k < array_vencimiento2.length; ++k) {
                                                    if (Character.isDigit(array_vencimiento2[k])) {
                                                        numeroString2 += array_vencimiento2[k];
                                                    }
                                                }

                                                System.out.println("Numero Cliente: " + numeroString + " Numero Area: " + numeroString2);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                int numeroInt2 = Integer.parseInt(numeroString2);

                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                assertThat(numeroInt2).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("PorVencer")) {

                                    } else if (fVencimiento.equals("VencidosArea")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosCliente")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    }
                                }

                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA VALIDAR FILTRO VENCIMIENTO: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                }
                break;
            case "Retenciones":
                break;
            case "Controversias":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validadorFiltroVencimiento = true;
                                    }
                                } catch (Exception e) {
                                    if (fVencimiento.equals("Nuevos")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueVencimiento = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                WebElement valueVencimiento2 = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");

                                                String getVencimiento = valueVencimiento.getText();
                                                String getVencimiento2 = valueVencimiento2.getText();

                                                System.out.println("Vencimiento encontrado: " + getVencimiento + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getVencimiento.toCharArray();
                                                char[] array_vencimiento2 = getVencimiento2.toCharArray();

                                                String numeroString = "";
                                                String numeroString2 = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                if (array_vencimiento2[0] == guion[0]) {
                                                    numeroString2 += guion[0];
                                                }

                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                for (int k = 0; k < array_vencimiento2.length; ++k) {
                                                    if (Character.isDigit(array_vencimiento2[k])) {
                                                        numeroString2 += array_vencimiento2[k];
                                                    }
                                                }

                                                System.out.println("Numero Cliente: " + numeroString + " Numero Area: " + numeroString2);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                int numeroInt2 = Integer.parseInt(numeroString2);

                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                assertThat(numeroInt2).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("PorVencer")) {

                                    } else if (fVencimiento.equals("VencidosArea")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosCliente")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    }
                                }
                                break;
                            case "ConsumosNoReconocidos":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validadorFiltroVencimiento = true;
                                    }
                                } catch (Exception e) {
                                    if (fVencimiento.equals("Nuevos")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueVencimiento = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                WebElement valueVencimiento2 = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");

                                                String getVencimiento = valueVencimiento.getText();
                                                String getVencimiento2 = valueVencimiento2.getText();

                                                System.out.println("Vencimiento encontrado: " + getVencimiento + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getVencimiento.toCharArray();
                                                char[] array_vencimiento2 = getVencimiento2.toCharArray();

                                                String numeroString = "";
                                                String numeroString2 = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                if (array_vencimiento2[0] == guion[0]) {
                                                    numeroString2 += guion[0];
                                                }

                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                for (int k = 0; k < array_vencimiento2.length; ++k) {
                                                    if (Character.isDigit(array_vencimiento2[k])) {
                                                        numeroString2 += array_vencimiento2[k];
                                                    }
                                                }

                                                System.out.println("Numero Cliente: " + numeroString + " Numero Area: " + numeroString2);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                int numeroInt2 = Integer.parseInt(numeroString2);

                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                assertThat(numeroInt2).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("PorVencer")) {

                                    } else if (fVencimiento.equals("VencidosArea")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosCliente")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    }
                                }

                                break;
                            case "ConsumosMalProcesados":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validadorFiltroVencimiento = true;
                                    }
                                } catch (Exception e) {
                                    if (fVencimiento.equals("Nuevos")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueVencimiento = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                WebElement valueVencimiento2 = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");

                                                String getVencimiento = valueVencimiento.getText();
                                                String getVencimiento2 = valueVencimiento2.getText();

                                                System.out.println("Vencimiento encontrado: " + getVencimiento + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getVencimiento.toCharArray();
                                                char[] array_vencimiento2 = getVencimiento2.toCharArray();

                                                String numeroString = "";
                                                String numeroString2 = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                if (array_vencimiento2[0] == guion[0]) {
                                                    numeroString2 += guion[0];
                                                }

                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                for (int k = 0; k < array_vencimiento2.length; ++k) {
                                                    if (Character.isDigit(array_vencimiento2[k])) {
                                                        numeroString2 += array_vencimiento2[k];
                                                    }
                                                }

                                                System.out.println("Numero Cliente: " + numeroString + " Numero Area: " + numeroString2);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                int numeroInt2 = Integer.parseInt(numeroString2);

                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                assertThat(numeroInt2).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("PorVencer")) {

                                    } else if (fVencimiento.equals("VencidosArea")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosCliente")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    }
                                }

                                break;
                            case "Monitorear":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validadorFiltroVencimiento = true;
                                    }
                                } catch (Exception e) {
                                    if (fVencimiento.equals("Nuevos")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueVencimiento = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                WebElement valueVencimiento2 = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");

                                                String getVencimiento = valueVencimiento.getText();
                                                String getVencimiento2 = valueVencimiento2.getText();

                                                System.out.println("Vencimiento encontrado: " + getVencimiento + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getVencimiento.toCharArray();
                                                char[] array_vencimiento2 = getVencimiento2.toCharArray();

                                                String numeroString = "";
                                                String numeroString2 = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                if (array_vencimiento2[0] == guion[0]) {
                                                    numeroString2 += guion[0];
                                                }

                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                for (int k = 0; k < array_vencimiento2.length; ++k) {
                                                    if (Character.isDigit(array_vencimiento2[k])) {
                                                        numeroString2 += array_vencimiento2[k];
                                                    }
                                                }

                                                System.out.println("Numero Cliente: " + numeroString + " Numero Area: " + numeroString2);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                int numeroInt2 = Integer.parseInt(numeroString2);

                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                assertThat(numeroInt2).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("PorVencer")) {

                                    } else if (fVencimiento.equals("VencidosArea")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosCliente")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    }
                                }

                                break;
                            case "TransaccionesEnProceso":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validadorFiltroVencimiento = true;
                                    }
                                } catch (Exception e) {
                                    if (fVencimiento.equals("Nuevos")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueVencimiento = general.expandContainer("2css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                WebElement valueVencimiento2 = general.expandContainer("2css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");

                                                String getVencimiento = valueVencimiento.getText();
                                                String getVencimiento2 = valueVencimiento2.getText();

                                                System.out.println("Vencimiento encontrado: " + getVencimiento + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getVencimiento.toCharArray();
                                                char[] array_vencimiento2 = getVencimiento2.toCharArray();

                                                String numeroString = "";
                                                String numeroString2 = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                if (array_vencimiento2[0] == guion[0]) {
                                                    numeroString2 += guion[0];
                                                }

                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                for (int k = 0; k < array_vencimiento2.length; ++k) {
                                                    if (Character.isDigit(array_vencimiento2[k])) {
                                                        numeroString2 += array_vencimiento2[k];
                                                    }
                                                }

                                                System.out.println("Numero Cliente: " + numeroString + " Numero Area: " + numeroString2);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                int numeroInt2 = Integer.parseInt(numeroString2);

                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                assertThat(numeroInt2).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("PorVencer")) {

                                    } else if (fVencimiento.equals("VencidosArea")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosCliente")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    }
                                }

                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA VALIDAR FILTRO VENCIMIENTO: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validadorFiltroVencimiento = true;
                                    }
                                } catch (Exception e) {
                                    if (fVencimiento.equals("Nuevos")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueVencimiento = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                WebElement valueVencimiento2 = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");

                                                String getVencimiento = valueVencimiento.getText();
                                                String getVencimiento2 = valueVencimiento2.getText();

                                                System.out.println("Vencimiento encontrado: " + getVencimiento + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getVencimiento.toCharArray();
                                                char[] array_vencimiento2 = getVencimiento2.toCharArray();

                                                String numeroString = "";
                                                String numeroString2 = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                if (array_vencimiento2[0] == guion[0]) {
                                                    numeroString2 += guion[0];
                                                }

                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                for (int k = 0; k < array_vencimiento2.length; ++k) {
                                                    if (Character.isDigit(array_vencimiento2[k])) {
                                                        numeroString2 += array_vencimiento2[k];
                                                    }
                                                }

                                                System.out.println("Numero Cliente: " + numeroString + " Numero Area: " + numeroString2);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                int numeroInt2 = Integer.parseInt(numeroString2);

                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                assertThat(numeroInt2).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("PorVencer")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosArea")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosCliente")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    }

                                }
                                break;
                            case "MiEquipo":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validadorFiltroVencimiento = true;
                                    }
                                } catch (Exception e) {
                                    if (fVencimiento.equals("Nuevos")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueVencimiento = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                WebElement valueVencimiento2 = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4) > ibk-label-badge", "", "");

                                                String getVencimiento = valueVencimiento.getText();
                                                String getVencimiento2 = valueVencimiento2.getText();

                                                System.out.println("Vencimiento encontrado: " + getVencimiento + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getVencimiento.toCharArray();
                                                char[] array_vencimiento2 = getVencimiento2.toCharArray();

                                                String numeroString = "";
                                                String numeroString2 = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                if (array_vencimiento2[0] == guion[0]) {
                                                    numeroString2 += guion[0];
                                                }

                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                for (int k = 0; k < array_vencimiento2.length; ++k) {
                                                    if (Character.isDigit(array_vencimiento2[k])) {
                                                        numeroString2 += array_vencimiento2[k];
                                                    }
                                                }

                                                System.out.println("Numero Cliente: " + numeroString + " Numero Area: " + numeroString2);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                int numeroInt2 = Integer.parseInt(numeroString2);

                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                assertThat(numeroInt2).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("PorVencer")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosArea")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosCliente")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    }
                                }
                                break;
                            case "Asignar":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validadorFiltroVencimiento = true;
                                    }
                                } catch (Exception e) {
                                    if (fVencimiento.equals("Nuevos")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueVencimiento = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                WebElement valueVencimiento2 = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4) > ibk-label-badge", "", "");

                                                String getVencimiento = valueVencimiento.getText();
                                                String getVencimiento2 = valueVencimiento2.getText();

                                                System.out.println("Vencimiento encontrado: " + getVencimiento + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getVencimiento.toCharArray();
                                                char[] array_vencimiento2 = getVencimiento2.toCharArray();

                                                String numeroString = "";
                                                String numeroString2 = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                if (array_vencimiento2[0] == guion[0]) {
                                                    numeroString2 += guion[0];
                                                }

                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                for (int k = 0; k < array_vencimiento2.length; ++k) {
                                                    if (Character.isDigit(array_vencimiento2[k])) {
                                                        numeroString2 += array_vencimiento2[k];
                                                    }
                                                }

                                                System.out.println("Numero Cliente: " + numeroString + " Numero Area: " + numeroString2);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                int numeroInt2 = Integer.parseInt(numeroString2);

                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                assertThat(numeroInt2).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("PorVencer")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosArea")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosCliente")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    }
                                }
                                break;
                            case "Resolver":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validadorFiltroVencimiento = true;
                                    }
                                } catch (Exception e) {
                                    if (fVencimiento.equals("Nuevos")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueVencimiento = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                WebElement valueVencimiento2 = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4) > ibk-label-badge", "", "");

                                                String getVencimiento = valueVencimiento.getText();
                                                String getVencimiento2 = valueVencimiento2.getText();

                                                System.out.println("Vencimiento encontrado: " + getVencimiento + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getVencimiento.toCharArray();
                                                char[] array_vencimiento2 = getVencimiento2.toCharArray();

                                                String numeroString = "";
                                                String numeroString2 = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                if (array_vencimiento2[0] == guion[0]) {
                                                    numeroString2 += guion[0];
                                                }

                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                for (int k = 0; k < array_vencimiento2.length; ++k) {
                                                    if (Character.isDigit(array_vencimiento2[k])) {
                                                        numeroString2 += array_vencimiento2[k];
                                                    }
                                                }

                                                System.out.println("Numero Cliente: " + numeroString + " Numero Area: " + numeroString2);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                int numeroInt2 = Integer.parseInt(numeroString2);

                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                assertThat(numeroInt2).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("PorVencer")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosArea")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosCliente")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    }
                                }
                                break;
                            case "Monitorear":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validadorFiltroVencimiento = true;
                                    }
                                } catch (Exception e) {
                                    if (fVencimiento.equals("Nuevos")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueVencimiento = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                WebElement valueVencimiento2 = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4) > ibk-label-badge", "", "");

                                                String getVencimiento = valueVencimiento.getText();
                                                String getVencimiento2 = valueVencimiento2.getText();

                                                System.out.println("Vencimiento encontrado: " + getVencimiento + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getVencimiento.toCharArray();
                                                char[] array_vencimiento2 = getVencimiento2.toCharArray();

                                                String numeroString = "";
                                                String numeroString2 = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                if (array_vencimiento2[0] == guion[0]) {
                                                    numeroString2 += guion[0];
                                                }

                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                for (int k = 0; k < array_vencimiento2.length; ++k) {
                                                    if (Character.isDigit(array_vencimiento2[k])) {
                                                        numeroString2 += array_vencimiento2[k];
                                                    }
                                                }

                                                System.out.println("Numero Cliente: " + numeroString + " Numero Area: " + numeroString2);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                int numeroInt2 = Integer.parseInt(numeroString2);

                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                assertThat(numeroInt2).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("PorVencer")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosArea")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosCliente")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    }
                                }
                                break;
                            case "TransaccionesEnProceso":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validadorFiltroVencimiento = true;
                                    }
                                } catch (Exception e) {
                                    if (fVencimiento.equals("Nuevos")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueVencimiento = general.expandContainer("2css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                WebElement valueVencimiento2 = general.expandContainer("2css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4) > ibk-label-badge", "", "");

                                                String getVencimiento = valueVencimiento.getText();
                                                String getVencimiento2 = valueVencimiento2.getText();

                                                System.out.println("Vencimiento encontrado: " + getVencimiento + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getVencimiento.toCharArray();
                                                char[] array_vencimiento2 = getVencimiento2.toCharArray();

                                                String numeroString = "";
                                                String numeroString2 = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                if (array_vencimiento2[0] == guion[0]) {
                                                    numeroString2 += guion[0];
                                                }

                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                for (int k = 0; k < array_vencimiento2.length; ++k) {
                                                    if (Character.isDigit(array_vencimiento2[k])) {
                                                        numeroString2 += array_vencimiento2[k];
                                                    }
                                                }

                                                System.out.println("Numero Cliente: " + numeroString + " Numero Area: " + numeroString2);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                int numeroInt2 = Integer.parseInt(numeroString2);

                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                assertThat(numeroInt2).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("PorVencer")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosArea")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosCliente")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    }
                                }
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA VALIDAR FILTRO VENCIMIENTO: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                }
                break;
            case "USPR":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validadorFiltroVencimiento = true;
                                    }
                                } catch (Exception e) {
                                    if (fVencimiento.equals("Nuevos")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueVencimiento = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                WebElement valueVencimiento2 = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");

                                                String getVencimiento = valueVencimiento.getText();
                                                String getVencimiento2 = valueVencimiento2.getText();

                                                System.out.println("Vencimiento encontrado: " + getVencimiento + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getVencimiento.toCharArray();
                                                char[] array_vencimiento2 = getVencimiento2.toCharArray();

                                                String numeroString = "";
                                                String numeroString2 = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                if (array_vencimiento2[0] == guion[0]) {
                                                    numeroString2 += guion[0];
                                                }

                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                for (int k = 0; k < array_vencimiento2.length; ++k) {
                                                    if (Character.isDigit(array_vencimiento2[k])) {
                                                        numeroString2 += array_vencimiento2[k];
                                                    }
                                                }

                                                System.out.println("Numero Cliente: " + numeroString + " Numero Area: " + numeroString2);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                int numeroInt2 = Integer.parseInt(numeroString2);

                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                assertThat(numeroInt2).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("PorVencer")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosArea")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosCliente")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    }
                                }
                                break;
                            case "CobrosIndebidos":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validadorFiltroVencimiento = true;
                                    }
                                } catch (Exception e) {
                                    if (fVencimiento.equals("Nuevos")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueVencimiento = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                WebElement valueVencimiento2 = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");

                                                String getVencimiento = valueVencimiento.getText();
                                                String getVencimiento2 = valueVencimiento2.getText();

                                                System.out.println("Vencimiento encontrado: " + getVencimiento + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getVencimiento.toCharArray();
                                                char[] array_vencimiento2 = getVencimiento2.toCharArray();

                                                String numeroString = "";
                                                String numeroString2 = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                if (array_vencimiento2[0] == guion[0]) {
                                                    numeroString2 += guion[0];
                                                }

                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                for (int k = 0; k < array_vencimiento2.length; ++k) {
                                                    if (Character.isDigit(array_vencimiento2[k])) {
                                                        numeroString2 += array_vencimiento2[k];
                                                    }
                                                }

                                                System.out.println("Numero Cliente: " + numeroString + " Numero Area: " + numeroString2);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                int numeroInt2 = Integer.parseInt(numeroString2);

                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                assertThat(numeroInt2).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("PorVencer")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosArea")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosCliente")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    }
                                }
                                break;
                            case "ExoneracionCobros":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validadorFiltroVencimiento = true;
                                    }
                                } catch (Exception e) {
                                    if (fVencimiento.equals("Nuevos")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueVencimiento = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                WebElement valueVencimiento2 = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");

                                                String getVencimiento = valueVencimiento.getText();
                                                String getVencimiento2 = valueVencimiento2.getText();

                                                System.out.println("Vencimiento encontrado: " + getVencimiento + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getVencimiento.toCharArray();
                                                char[] array_vencimiento2 = getVencimiento2.toCharArray();

                                                String numeroString = "";
                                                String numeroString2 = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                if (array_vencimiento2[0] == guion[0]) {
                                                    numeroString2 += guion[0];
                                                }

                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                for (int k = 0; k < array_vencimiento2.length; ++k) {
                                                    if (Character.isDigit(array_vencimiento2[k])) {
                                                        numeroString2 += array_vencimiento2[k];
                                                    }
                                                }

                                                System.out.println("Numero Cliente: " + numeroString + " Numero Area: " + numeroString2);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                int numeroInt2 = Integer.parseInt(numeroString2);

                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                assertThat(numeroInt2).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("PorVencer")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosArea")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosCliente")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    }
                                }
                                break;
                            case "ConsumosMalProcesados":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validadorFiltroVencimiento = true;
                                    }
                                } catch (Exception e) {
                                    if (fVencimiento.equals("Nuevos")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueVencimiento = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                WebElement valueVencimiento2 = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");

                                                String getVencimiento = valueVencimiento.getText();
                                                String getVencimiento2 = valueVencimiento2.getText();

                                                System.out.println("Vencimiento encontrado: " + getVencimiento + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getVencimiento.toCharArray();
                                                char[] array_vencimiento2 = getVencimiento2.toCharArray();

                                                String numeroString = "";
                                                String numeroString2 = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                if (array_vencimiento2[0] == guion[0]) {
                                                    numeroString2 += guion[0];
                                                }

                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                for (int k = 0; k < array_vencimiento2.length; ++k) {
                                                    if (Character.isDigit(array_vencimiento2[k])) {
                                                        numeroString2 += array_vencimiento2[k];
                                                    }
                                                }

                                                System.out.println("Numero Cliente: " + numeroString + " Numero Area: " + numeroString2);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                int numeroInt2 = Integer.parseInt(numeroString2);

                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                assertThat(numeroInt2).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("PorVencer")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosArea")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosCliente")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    }
                                }
                                break;
                            case "ConsumosNoReconocidos":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validadorFiltroVencimiento = true;
                                    }
                                } catch (Exception e) {
                                    if (fVencimiento.equals("Nuevos")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueVencimiento = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                WebElement valueVencimiento2 = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");

                                                String getVencimiento = valueVencimiento.getText();
                                                String getVencimiento2 = valueVencimiento2.getText();

                                                System.out.println("Vencimiento encontrado: " + getVencimiento + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getVencimiento.toCharArray();
                                                char[] array_vencimiento2 = getVencimiento2.toCharArray();

                                                String numeroString = "";
                                                String numeroString2 = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                if (array_vencimiento2[0] == guion[0]) {
                                                    numeroString2 += guion[0];
                                                }

                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                for (int k = 0; k < array_vencimiento2.length; ++k) {
                                                    if (Character.isDigit(array_vencimiento2[k])) {
                                                        numeroString2 += array_vencimiento2[k];
                                                    }
                                                }

                                                System.out.println("Numero Cliente: " + numeroString + " Numero Area: " + numeroString2);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                int numeroInt2 = Integer.parseInt(numeroString2);

                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                assertThat(numeroInt2).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("PorVencer")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosArea")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosCliente")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    }
                                }
                                break;
                            case "Retipificar":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validadorFiltroVencimiento = true;
                                    }
                                } catch (Exception e) {
                                    if (fVencimiento.equals("Nuevos")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueVencimiento = general.expandContainer("2css", LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                WebElement valueVencimiento2 = general.expandContainer("2css", LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");

                                                String getVencimiento = valueVencimiento.getText();
                                                String getVencimiento2 = valueVencimiento2.getText();

                                                System.out.println("Vencimiento encontrado: " + getVencimiento + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getVencimiento.toCharArray();
                                                char[] array_vencimiento2 = getVencimiento2.toCharArray();

                                                String numeroString = "";
                                                String numeroString2 = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                if (array_vencimiento2[0] == guion[0]) {
                                                    numeroString2 += guion[0];
                                                }

                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                for (int k = 0; k < array_vencimiento2.length; ++k) {
                                                    if (Character.isDigit(array_vencimiento2[k])) {
                                                        numeroString2 += array_vencimiento2[k];
                                                    }
                                                }

                                                System.out.println("Numero Cliente: " + numeroString + " Numero Area: " + numeroString2);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                int numeroInt2 = Integer.parseInt(numeroString2);

                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                assertThat(numeroInt2).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("PorVencer")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosArea")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosCliente")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    }
                                }
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA VALIDAR FILTRO VENCIMIENTO: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_USPR, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validadorFiltroVencimiento = true;
                                    }
                                } catch (Exception e) {
                                    if (fVencimiento.equals("Nuevos")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueVencimiento = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                WebElement valueVencimiento2 = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");

                                                String getVencimiento = valueVencimiento.getText();
                                                String getVencimiento2 = valueVencimiento2.getText();

                                                System.out.println("Vencimiento encontrado: " + getVencimiento + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getVencimiento.toCharArray();
                                                char[] array_vencimiento2 = getVencimiento2.toCharArray();

                                                String numeroString = "";
                                                String numeroString2 = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                if (array_vencimiento2[0] == guion[0]) {
                                                    numeroString2 += guion[0];
                                                }

                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                for (int k = 0; k < array_vencimiento2.length; ++k) {
                                                    if (Character.isDigit(array_vencimiento2[k])) {
                                                        numeroString2 += array_vencimiento2[k];
                                                    }
                                                }

                                                System.out.println("Numero Cliente: " + numeroString + " Numero Area: " + numeroString2);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                int numeroInt2 = Integer.parseInt(numeroString2);

                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                assertThat(numeroInt2).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("PorVencer")) {

                                    } else if (fVencimiento.equals("VencidosArea")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosCliente")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception q1) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    }
                                }
                                break;
                            case "MiEquipo":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validadorFiltroVencimiento = true;
                                    }
                                } catch (Exception e) {
                                    if (fVencimiento.equals("Nuevos")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueVencimiento = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                WebElement valueVencimiento2 = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4) > ibk-label-badge", "", "");

                                                String getVencimiento = valueVencimiento.getText();
                                                String getVencimiento2 = valueVencimiento2.getText();

                                                System.out.println("Vencimiento encontrado: " + getVencimiento + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getVencimiento.toCharArray();
                                                char[] array_vencimiento2 = getVencimiento2.toCharArray();

                                                String numeroString = "";
                                                String numeroString2 = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                if (array_vencimiento2[0] == guion[0]) {
                                                    numeroString2 += guion[0];
                                                }

                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                for (int k = 0; k < array_vencimiento2.length; ++k) {
                                                    if (Character.isDigit(array_vencimiento2[k])) {
                                                        numeroString2 += array_vencimiento2[k];
                                                    }
                                                }

                                                System.out.println("Numero Cliente: " + numeroString + " Numero Area: " + numeroString2);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                int numeroInt2 = Integer.parseInt(numeroString2);

                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                assertThat(numeroInt2).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("PorVencer")) {

                                    } else if (fVencimiento.equals("VencidosArea")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosCliente")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    }
                                }
                                break;
                            case "Asignar":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validadorFiltroVencimiento = true;
                                    }
                                } catch (Exception e) {
                                    if (fVencimiento.equals("Nuevos")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueVencimiento = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                WebElement valueVencimiento2 = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4) > ibk-label-badge", "", "");

                                                String getVencimiento = valueVencimiento.getText();
                                                String getVencimiento2 = valueVencimiento2.getText();

                                                System.out.println("Vencimiento encontrado: " + getVencimiento + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getVencimiento.toCharArray();
                                                char[] array_vencimiento2 = getVencimiento2.toCharArray();

                                                String numeroString = "";
                                                String numeroString2 = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                if (array_vencimiento2[0] == guion[0]) {
                                                    numeroString2 += guion[0];
                                                }

                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                for (int k = 0; k < array_vencimiento2.length; ++k) {
                                                    if (Character.isDigit(array_vencimiento2[k])) {
                                                        numeroString2 += array_vencimiento2[k];
                                                    }
                                                }

                                                System.out.println("Numero Cliente: " + numeroString + " Numero Area: " + numeroString2);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                int numeroInt2 = Integer.parseInt(numeroString2);

                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                assertThat(numeroInt2).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("PorVencer")) {

                                    } else if (fVencimiento.equals("VencidosArea")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosCliente")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    }
                                }
                                break;
                            case "Resolver":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validadorFiltroVencimiento = true;
                                    }
                                } catch (Exception e) {
                                    if (fVencimiento.equals("Nuevos")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueVencimiento = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                WebElement valueVencimiento2 = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4) > ibk-label-badge", "", "");

                                                String getVencimiento = valueVencimiento.getText();
                                                String getVencimiento2 = valueVencimiento2.getText();

                                                System.out.println("Vencimiento encontrado: " + getVencimiento + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getVencimiento.toCharArray();
                                                char[] array_vencimiento2 = getVencimiento2.toCharArray();

                                                String numeroString = "";
                                                String numeroString2 = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                if (array_vencimiento2[0] == guion[0]) {
                                                    numeroString2 += guion[0];
                                                }

                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                for (int k = 0; k < array_vencimiento2.length; ++k) {
                                                    if (Character.isDigit(array_vencimiento2[k])) {
                                                        numeroString2 += array_vencimiento2[k];
                                                    }
                                                }

                                                System.out.println("Numero Cliente: " + numeroString + " Numero Area: " + numeroString2);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                int numeroInt2 = Integer.parseInt(numeroString2);

                                                assertThat(numeroInt).isGreaterThanOrEqualTo(0);
                                                assertThat(numeroInt2).isGreaterThanOrEqualTo(0);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("PorVencer")) {

                                    } else if (fVencimiento.equals("VencidosArea")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    } else if (fVencimiento.equals("VencidosCliente")) {
                                        try {
                                            for (int i = 1; i < 100; ++i) {
                                                WebElement valueMasFiltros = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3) > ibk-label-badge", "", "");
                                                String getMasFiltros = valueMasFiltros.getText();
                                                System.out.println("Vencimiento encontrado: " + getMasFiltros + " vs Del Vencimiento: " + fVencimiento);
                                                char[] array_vencimiento = getMasFiltros.toCharArray();
                                                String numeroString = "";
                                                if (array_vencimiento[0] == guion[0]) {
                                                    numeroString += guion[0];
                                                }
                                                for (int j = 0; j < array_vencimiento.length; ++j) {
                                                    if (Character.isDigit(array_vencimiento[j])) {
                                                        numeroString += array_vencimiento[j];
                                                    }
                                                }
                                                System.out.println("Numero: " + numeroString);
                                                int numeroInt = Integer.parseInt(numeroString);
                                                assertThat(numeroInt).isLessThanOrEqualTo(-1);
                                                System.out.println("\nEL NUMERO ES: " + numeroInt + "\n");
                                                contador++;
                                                numeroString = null;
                                            }

                                        } catch (Exception i) {
                                            validadorFiltroVencimiento = true;
                                        }
                                    }
                                }
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA VALIDAR FILTRO VENCIMIENTO: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                }
                break;
        }
        return validadorFiltroVencimiento;
    }


}
