package com.everis.giru.userinterface.tramites;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class FiltrosBandejaPage {

    public static final Target BUTTOM_FILTRO_APLICAR = Target.the("Filtro Boton Aplicar").located(By.linkText("Aplicar"));

    //Filtro Vencimiento
    public static final Target BUTTOM_FILTRO_VENCIMIENTO = Target.the("Filtro Vencimiento").located(By.id("EXPIRATION_INDICATOR"));
    //Filtro Productos
    public static final Target BUTTOM_FILTRO_PRODUCTO = Target.the("Filtro Producto").located(By.id("SERVICE"));
    
    //Filtro Tipo de Tramite
    public static final Target BUTTOM_FILTRO_TIPO_TRAMITE = Target.the("Filtro Tipo de Tramite").located(By.id("PROCEDURE"));

    //Filtro Tipologia
    public static final Target BUTTOM_FILTRO_TIPOLOGIA = Target.the("Filtro Tipologia").located(By.id("TYPE"));

    //Filtro Motivos
    public static final Target BUTTOM_FILTRO_MOTIVOS = Target.the("Filtro Motivos").located(By.id("REASON"));

    //Filtro Segmento
    public static final Target BUTTOM_FILTRO_SEGMENTO = Target.the("Filtro Segmento").located(By.id("SEGMENT"));

    //Filtro Estado
    public static final Target BUTTOM_FILTRO_ESTADO = Target.the("Filtro Estado").located(By.id("CLAIM_STATUS_FILTER"));

    //Filtro Nivel de Servicio
    public static final Target BUTTOM_FILTRO_NIVELSERVICIO = Target.the("Filtro Nivel de Servicio").located(By.id("SERVICE_LEVEL"));

    //Filtro Moneda y Monto
    public static final Target BUTTOM_FILTRO_MONEDAyMONTO = Target.the("Filtro Moneda y Monto").located(By.id("CURRENCY"));

    //Filtro Mas Filtros
    public static final Target BUTTOM_FILTRO_MASFILTROS = Target.the("Filtro Mas Filtros").located(By.id("MORE_FILTERS"));

}
