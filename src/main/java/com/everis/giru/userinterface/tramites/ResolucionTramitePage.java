package com.everis.giru.userinterface.tramites;

import com.everis.giru.utils.Shadow;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class ResolucionTramitePage extends PageObject {

    Shadow general = new Shadow();

    public static final Target CBX_SELECCIONAR_RAZON = Target.the("Seleccionar razon para derivar CIEXO").located(By.xpath("/html/body/app-assi-modal[2]/div/div/giru-claim-derive/div/div/div[2]/form/div/div[1]/div/app-ibk-select"));
    public static final Target CBX_SELECCIONAR_AREA = Target.the("Seleccionar area para derivar  CIEXO").located(By.xpath("/html/body/app-assi-modal[2]/div/div/giru-claim-derive/div/div/div[2]/form/div/div[2]/div/app-ibk-select"));

    public static final Target CBX_SELECCIONAR_AREA_C = Target.the("Seleccionar area para derivar  CIEXO").located(By.xpath("/html/body/app-assi-modal[2]/div/div/giru-claim-derive/div/div/div[2]/form/div/div[2]/div/ibk-select"));

    public static final Target BTN_CONFIRMAR_DERIVACION = Target.the("Botón confirmar derivación").located(By.xpath("//*[@class='btn btn-primary main-button-s']"));


}
