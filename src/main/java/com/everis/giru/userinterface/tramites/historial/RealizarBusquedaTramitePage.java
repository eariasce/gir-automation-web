package com.everis.giru.userinterface.tramites.historial;

import com.everis.giru.utils.ApiCommons;
import com.everis.giru.utils.Shadow;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.io.File;
import java.time.Duration;
import java.util.Locale;

public class RealizarBusquedaTramitePage extends PageObject {

    Shadow general = new Shadow();

    @FindBy(xpath = "//ibk-select")
    WebElement tramiteCombo;

    @FindBy(xpath = "(//ibk-giru-historic-process)[1]")
    WebElement DetallesEnProceso;

    @FindBy(xpath = "//*[@id='claim-detail-tabs__tracking']")
    WebElement LBL_NOMBRE_AREA;

    @FindBy(xpath = "//ibk-inputgroup-text[@formcontrolname='document']")
    WebElement LBL_INPUT_BUSCAR_TRAMITE;

    @FindBy(xpath = "//ibk-grid-group")
    WebElement LBL_ID_TABLA_HISTORIAL;

    @FindBy(id = "claim-detail-tabs__tracking")
    WebElement lblArchivo;

    @FindBy(xpath = "//*[@id='claim-detail-tabs__tracking']")
    WebElement seccionSegumiento;

    @FindBy(id = "claim-detail-tabs__tab-detail")
    WebElement btnSeguimiento;

    public void validaSeguimientoTramiteDerivado(String comentario, String areaDerivada, String actividadDerivada) {

        try {
            Thread.sleep(3000);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        //TODO: VALIDACIÓN EN HISTORIAL - SEGUIMIENTO
        WebElement btnseguimiento = general.expandContainer("1css", btnSeguimiento, "ibk-tab:nth-child(2)", "", "", "");
        btnseguimiento.click();

        WebElement areaFront = general.expandContainer("1css", LBL_NOMBRE_AREA, "div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1)", "", "", "");

        if (areaFront.getText().toUpperCase(Locale.ROOT).contains(areaDerivada.toUpperCase(Locale.ROOT))) {
            System.out.println("VALIDA AREA DERIVADA ES: " + areaDerivada.toUpperCase(Locale.ROOT));
            System.out.println("HISTORIAL SEGUIMIENTO - VALIDA AREA ES: " + areaFront.getText().toUpperCase(Locale.ROOT));
        } else {
            throw new AssertionError("No coincide nombres de area");
        }

        //TODO: CLICK EN TRAMITE FLUJOS
        tramiteCombo.click();

        //TODO: SELECCIONAR TRAMITE FLUJO INICIAL
        WebElement selectTramiteFlujoInicial = general.expandContainer("1css", tramiteCombo, "div:nth-child(1) > ul:nth-child(2) > li:nth-child(2)", "", "", "");
        selectTramiteFlujoInicial.click();

        try {
            Thread.sleep(3000);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        WebElement lblActividadDer = general.expandContainer("2css", DetallesEnProceso, "div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(2) > ibk-giru-text-info:nth-child(1)", ".collapsible-text.notranslate", "", "");
        WebElement lblComentario = general.expandContainer("2css", DetallesEnProceso, "div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(3) > ibk-giru-text-info:nth-child(1)", ".collapsible-text.notranslate", "", "");

        String nuevoActividad = "";
        switch (actividadDerivada.trim()) {
            case "Requiere ser enviado a nuevo flujo":
                nuevoActividad = "Derivación a nuevo flujo";
                break;
            case "Caso 2":
                nuevoActividad = "Texto caso 2";
                break;
        }

        Assert.assertEquals(nuevoActividad, lblActividadDer.getText());
        System.out.println("VALIDA ACTIVIDAD DERIVACION A NUEVO FLUJO: " + lblActividadDer.getText());
        Assert.assertEquals(comentario, lblComentario.getText());
        System.out.println("VALIDA TEXTO COMENTARIO: " + lblComentario.getText());
    }

    public void validaNumeroTramiteHistorial(String numeroTramite) {

        String ambiente = Serenity.sessionVariableCalled("ambiente");

        WebElement valueClaimNumber = general.expandContainer("2css", LBL_INPUT_BUSCAR_TRAMITE, "ibk-input", "div > input", "", "");
        valueClaimNumber.sendKeys(numeroTramite);

        if (ambiente.equals("UAT")) {
            try {
                Thread.sleep(8000);
            } catch (Exception e) {
                System.out.println(e);
            }
        } else if (ambiente.equals("STG")) {
            try {
                Thread.sleep(5000);
            } catch (Exception e) {
                System.out.println(e);
            }
        }

        valueClaimNumber.sendKeys(Keys.ENTER);

        try {
            Thread.sleep(3000);
        } catch (Exception e) {
            System.out.println(e);
        }

        //TODO: VALIDA HISTORIAL NUMERO DE TRAMITE
        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_HISTORIAL, "ibk-grid", "div:nth-child(1) > table:nth-child(1) > tbody:nth-child(3) > tr:nth-child(1) > td:nth-child(1)", "", "");
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("arguments[0].scrollIntoView();", valueTramite);
        String getTramite = valueTramite.getText();
        String sSubTramite = removerCeros(getTramite);
        System.out.println("\n * VALIDA HISTORIAL NUMERO DE TRAMITE: " + "Numero tramite Original: " + getTramite + " Numero tramite Con Metodo: " + sSubTramite + "\n***************************************");
        Assert.assertEquals(sSubTramite, numeroTramite);

        try {
            Thread.sleep(1000);
        } catch (Exception e) {
            System.out.println(e);
        }

        //TODO: VALIDO AUTONOMIA - ACTIVIDAD EN RESOLUCION AUTOMATICA
        String autonomia = "";
        try {
            autonomia = Serenity.sessionVariableCalled("autonomia").toString();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        if (autonomia.equals("si")) {
            WebElement valueTipoResolucion = general.expandContainer("2css", LBL_ID_TABLA_HISTORIAL, "div > div.ibkTableGroup__table > div.grid-wrapper > ibk-grid", "div > table > tbody > tr > td:nth-child(8)", "", "");
            String getTipoResolucion = valueTipoResolucion.getText();
            if (getTipoResolucion.equals(" Resolución Automática")) {
                System.out.println("Texto validado con resolución automática");
            } else {
                throw new AssertionError("No se validó correctamente en historial texto resolución automatica, se obtuvo: " + getTipoResolucion);
            }
        }

        valueTramite.click();
    }

    public String removerCeros(String cadena) {
        cadena = cadena.replaceAll("^0+", "");
        return cadena;
    }

    public void validarDatosResumenHistorial() {
        String ambiente = Serenity.sessionVariableCalled("ambiente");
        String tipoProducto = Serenity.sessionVariableCalled("tipoproducto");
        String tipoTramite = Serenity.sessionVariableCalled("tipotramite");
        String tipologia = Serenity.sessionVariableCalled("tipologia");
        String nrotarjeta = ApiCommons.clientes.getNumerotarjeta();

        String subirarchivo = Serenity.sessionVariableCalled("subirarchivo");


        //TODO: VALIDA EN RESUMEN - TIPO TRAMITE Y TIPOLOGIA
        Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        boolean labelTipoTramite = Serenity.getDriver().findElements(By.xpath("//label[text()='Tipo de trámite:']/following-sibling::label")).size() > 0;
        if (labelTipoTramite) {
            System.err.println("VALIDA EN RESUMEN - TIPO TRAMITE Y TIPOLOGIA - ENTRA LABEL");
            String tipoTramiteFrontSpan = Serenity.getDriver().findElement(By.xpath("//label[text()='Tipo de trámite:']/following-sibling::label")).getText();
            String tipologiaFrontSpan = Serenity.getDriver().findElement(By.xpath("//label[text()='Tipología:']/following-sibling::label")).getText();
            Assert.assertEquals(tipoTramite.toUpperCase(new Locale("es", "Peru")), tipoTramiteFrontSpan.toUpperCase(new Locale("es", "Peru")));
            Assert.assertEquals(tipologia.toUpperCase(new Locale("es", "Peru")), tipologiaFrontSpan.toUpperCase(new Locale("es", "Peru")));
        } else {
            System.err.println("VALIDA EN RESUMEN - TIPO TRAMITE Y TIPOLOGIA - ENTRA SPAN");
            String tipoTramiteFrontSpan = Serenity.getDriver().findElement(By.xpath("//label[text()='Tipo de trámite:']/following-sibling::span")).getText();
            String tipologiaFrontSpan = Serenity.getDriver().findElement(By.xpath("//label[text()='Tipología:']/following-sibling::span")).getText();
            Assert.assertEquals(tipoTramite.toUpperCase(new Locale("es", "Peru")), tipoTramiteFrontSpan.toUpperCase(new Locale("es", "Peru")));
            Assert.assertEquals(tipologia.toUpperCase(new Locale("es", "Peru")), tipologiaFrontSpan.toUpperCase(new Locale("es", "Peru")));

        }


        //TODO: VALIDA EN HISTORIAL - NUMERO TARJETA
        if (tipoProducto.equals("TARJETA DE CREDITO") || tipoProducto.equals("TARJETA DE DEBITO")) {

            Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(3));
            boolean labelNumeroTarjeta = Serenity.getDriver().findElements(By.xpath("//label[text()='Número de tarjeta']/following-sibling::span")).size() > 0;

            if (labelNumeroTarjeta) {
                System.err.println("VALIDA EN RESUMEN - NUMERO TARJETA - ENTRA SPAN");
                String numeroTarjeta = Serenity.getDriver().findElement(By.xpath("//label[text()='Número de tarjeta']/following-sibling::span")).getText();
                Assert.assertEquals(nrotarjeta.replace(" ", ""), numeroTarjeta.replace(" ", ""));

            } else {
                System.err.println("VALIDA EN RESUMEN - NUMERO TARJETA - ENTRA LABEL");
                String numeroTarjeta = Serenity.getDriver().findElement(By.xpath("//label[text()='Número de tarjeta:']/following-sibling::label")).getText();
                Assert.assertEquals(nrotarjeta.replace(" ", ""), numeroTarjeta.replace(" ", ""));
            }
        }

        //TODO: BUTTON SEGUIMIENTO
        WebElement btnseguimiento = general.expandContainer("1css", btnSeguimiento, "ibk-tab:nth-child(2)", "", "", "");
        waitForCondition().until(ExpectedConditions.visibilityOf(btnseguimiento));
        btnseguimiento.click();

        if (subirarchivo.equals("Si")) {
            //Validar archivo
            Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
            WebElement actividadArchivoSeguimientoHistorial = general.expandContainer("2css", lblArchivo, "ibk-giru-link-icon", "div > div.link > a", "", "");

            if (actividadArchivoSeguimientoHistorial.getText().contains("pdf") || actividadArchivoSeguimientoHistorial.getText().contains("txt")) {
                System.out.println("Se encuentra el archivo");
            } else {
                throw new AssertionError("No se encuentra archivo");
            }

        }

        //TODO: VALIDO AUTONOMIA EN SEGUIMIENTO
        String autonomia = "";
        try {
            autonomia = Serenity.sessionVariableCalled("autonomia").toString();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        if (autonomia.equals("si")) {
            Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(2));
            WebElement actividadTipoResolucion = general.expandContainer("2css", seccionSegumiento, "#stage_procede > div.detail-process-container > div > div.body-detail-process > div > div.container-subdetail-process > div.content-subdetail-process.detail_actividad.false > ibk-giru-text-info", "div > div.name-value > div", "", "");

            if (actividadTipoResolucion.getText().contains("Resolución Automática")) {
                System.out.println("validado en seguimiento Resolución Automática");
            } else {
                throw new AssertionError("No se validó el texto de resolución automatica, se obtuvo: " + actividadTipoResolucion.getText());
            }
        }

    }


    //TODO: AGREGAR VALIDACION - DESPUES DE RETIFICAR - HISTORIAL
    public void validoDatosTramiteRetipificado() {
        WebElement btnseguimiento = general.expandContainer("1css", btnSeguimiento, "ibk-tab:nth-child(2)", "", "", "");
        btnseguimiento.click();
        WebElement prueba = general.expandContainer("1css", lblArchivo, "span.process-description", "", "", "");
        System.out.println("Se encuentra en proceso new: " + prueba.getText());
        String tipoResol = Serenity.sessionVariableCalled("tipoResol").toString();
        //String estoyEnResolutoria = Serenity.sessionVariableCalled("estoyEnResolutoria").toString();

//        Assert.assertEquals("EN PROCESO", prueba.getText());

    }

    public void validoDatosTramiteResol() {
        String tipoResolucion = Serenity.sessionVariableCalled("tipoResol").toString();
        //Validar estado del tramite
//        WebElement estadoTramite = general.expandContainer("2css", lblResolTramite, "div > div.ibkTableGroup__table > div.grid-wrapper > ibk-grid", "div > table > tbody > tr > td.no-overflow > div > span:nth-child(1)", "","");
//        System.err.println(estadoTramite.getText());
        WebElement btnseguimiento = general.expandContainer("1css", btnSeguimiento, "ibk-tab:nth-child(2)", "", "", "");
        btnseguimiento.click();
        //Se revisara ya que cae en este paso
        WebElement prueba = general.expandContainer("1css", lblArchivo, "span.process-description", "", "", "");
        System.err.println(prueba.getText());
        String tipoResol = Serenity.sessionVariableCalled("tipoResol").toString();
        String estoyEnResolutoria = Serenity.sessionVariableCalled("estoyEnResolutoria").toString();

        if (estoyEnResolutoria.equals("Si") && tipoResol.equals("Procede")) {
            Assert.assertEquals("PROCEDE", prueba.getText());
            System.out.println("<<< PROCEDE >>>" + " OK ");
        } else if (estoyEnResolutoria.equals("Si") && tipoResol.equals("No Procede")) {
            Assert.assertEquals("NO PROCEDE", prueba.getText());
            System.out.println("<<< NO PROCEDE >>>" + " OK ");
        } else {
            System.out.println("escenarios prodece derivacion");
        }

    }

    public void validoHRDescargado() {

        WebElement actividadArchivoSeguimientoHistorial = general.expandContainer("2css", lblArchivo, "#stage_registro > div.detail-process-container > div > div.body-detail-process > div > div > div.content-subdetail-process.detail_hoja-de-tramite-original.w-100 > ibk-giru-link-icon", "div > div.link > a", "", "");
        waitForCondition().until(ExpectedConditions.visibilityOf(actividadArchivoSeguimientoHistorial));

        JavascriptExecutor js = (JavascriptExecutor) Serenity.getDriver();
        js.executeScript("arguments[0].scrollIntoView();", actividadArchivoSeguimientoHistorial);

        actividadArchivoSeguimientoHistorial.click();

        try {
            Thread.sleep(2000);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        String ambiente = Serenity.sessionVariableCalled("ambiente");
        if (ambiente.equals("UAT")) {
            try {
                Thread.sleep(4000);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

        String numeroTramite = Serenity.sessionVariableCalled("numeroTramite").toString();
        System.out.println("SE DESCARGO EL HR DEL TRAMITE");
        String home = System.getProperty("user.home");
        String file_with_location = home + "\\Downloads\\" + "HR_" + numeroTramite + ".pdf";
        File file = new File(file_with_location);

        if (file.exists()) {
            System.out.println(file_with_location + "  SE DESCARGO EL HR CON EXITO");
        } else {
            System.err.println(file_with_location + "  NO SE ENCUENTRA DESCARGADO EL HR - REVISAR POR FRONT");
        }

    }


}


