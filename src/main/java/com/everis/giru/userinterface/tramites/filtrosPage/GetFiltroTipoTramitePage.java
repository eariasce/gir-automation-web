package com.everis.giru.userinterface.tramites.filtrosPage;

import com.everis.giru.utils.Shadow;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class GetFiltroTipoTramitePage extends PageObject {

    Shadow general = new Shadow();

    @FindBy(xpath = "//*[@id='radio-list__radio-option']")
    WebElement LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='radio-list__radio-option']")
    WebElement LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS;
    @FindBy(xpath = "//*[@id='radio-list__radio-option']")
    WebElement LBL_ID_RADIOBUTTOM_ESPECIALISTA_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='radio-list__radio-option']")
    WebElement LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR;

    @FindBy(xpath = "//*[@id='radio-list__radio-option']")
    WebElement LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR;
    @FindBy(xpath = "//*[@id='radio-list__radio-option']")
    WebElement LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS;
    @FindBy(xpath = "//*[@id='radio-list__radio-option']")
    WebElement LBL_ID_RADIOBUTTOM_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='radio-list__radio-option']")
    WebElement LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR;

    public void seleccionaFiltroTipoTramite(String fTipoTramite, String perfil, String departamento) {

        switch (departamento) {
            case "GPYR":
                switch (perfil) {
                    case "Especialista":
                        switch (fTipoTramite) {
                            case "Reclamo":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                WebElement btnReclamo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(1) > label", "", "", "");
                                btnReclamo.click();
                                break;
                            case "Pedido":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                WebElement btnPedido = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(2) > label", "", "", "");
                                btnPedido.click();
                                break;
                            case "Solicitud":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                WebElement btnSolicitud = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(3) > label", "", "", "");
                                btnSolicitud.click();
                                break;
                            default:
                                System.out.println("No se encuentra Filtro Tipo Tramite mapeado como: " + fTipoTramite + "  en perfil Especialista del departamento GPYR!!!");
                        }
                        break;
                    case "Supervisor":
                        switch (fTipoTramite) {
                            case "Reclamo":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                WebElement btnReclamo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(1) > label", "", "", "");
                                btnReclamo.click();
                                break;
                            case "Pedido":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                WebElement btnPedido = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(2) > label", "", "", "");
                                btnPedido.click();
                                break;
                            case "Solicitud":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                WebElement btnSolicitud = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(3) > label", "", "", "");
                                btnSolicitud.click();
                                break;
                            default:
                                System.out.println("No se encuentra Filtro Tipo Tramite mapeado como: " + fTipoTramite + "  en perfil Supervisor del departamento GPYR!!!");
                        }
                        break;
                }
                break;
            case "Seguros":
                switch (perfil) {
                    case "Especialista":
                        switch (fTipoTramite) {
                            case "Reclamo":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                WebElement btnReclamo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(1) > label", "", "", "");
                                btnReclamo.click();
                                break;
                            case "Pedido":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                WebElement btnPedido = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(2) > label", "", "", "");
                                btnPedido.click();
                                break;
                            default:
                                System.out.println("No se encuentra Filtro Tipo Tramite mapeado como: " + fTipoTramite + "  en perfil Especialista del departamento SEGUROS!!!");
                        }
                        break;
                    case "Supervisor":
                        switch (fTipoTramite) {
                            case "Reclamo":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                WebElement btnReclamo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(1) > label", "", "", "");
                                btnReclamo.click();
                                break;
                            case "Pedido":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                WebElement btnPedido = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(2) > label", "", "", "");
                                btnPedido.click();
                                break;
                            default:
                                System.out.println("No se encuentra Filtro Tipo Tramite mapeado como: " + fTipoTramite + "  en perfil Supervisor del departamento SEGUROS!!!");
                        }
                        break;
                }
                break;
            case "Controversias":
                switch (perfil) {
                    case "Especialista":
                        switch (fTipoTramite) {
                            case "Reclamo":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_CONTROVERSIAS));
                                WebElement btnReclamo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_CONTROVERSIAS, "div > div > label", "", "", "");
                                btnReclamo.click();
                                break;
                            default:
                                System.out.println("No se encuentra Filtro Tipo Tramite mapeado como: " + fTipoTramite + "  en perfil Especialista del departamento CONTROVERSIAS!!!");
                        }
                        break;
                    case "Supervisor":
                        switch (fTipoTramite) {
                            case "Reclamo":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_CONTROVERSIAS));
                                WebElement btnReclamo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_CONTROVERSIAS, "div > div > label", "", "", "");
                                btnReclamo.click();
                                break;
                            default:
                                System.out.println("No se encuentra Filtro Tipo Tramite mapeado como: " + fTipoTramite + "  en perfil Supervisor del departamento CONTROVERSIAS!!!");
                        }
                        break;
                }
                break;
            case "USPR":
                switch (perfil) {
                    case "Especialista":
                        switch (fTipoTramite) {
                            case "Reclamo":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                WebElement btnReclamo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(1) > label", "", "", "");
                                btnReclamo.click();
                                break;
                            case "Pedido":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                WebElement btnPedido = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(2) > label", "", "", "");
                                btnPedido.click();
                                break;
                            default:
                                System.out.println("No se encuentra Filtro Tipo Tramite mapeado como: " + fTipoTramite + "  en perfil Especialista del departamento USPR!!!");
                        }
                        break;
                    case "Supervisor":
                        switch (fTipoTramite) {
                            case "Reclamo":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                WebElement btnReclamo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(1) > label", "", "", "");
                                btnReclamo.click();
                                break;
                            case "Pedido":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                WebElement btnPedido = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(2) > label", "", "", "");
                                btnPedido.click();
                                break;
                            default:
                                System.out.println("No se encuentra Filtro Tipo Tramite mapeado como: " + fTipoTramite + "  en perfil Supervisor del departamento USPR!!!");
                        }
                        break;
                }

                break;
            default:
                System.out.println("No existe filtro TipoDeTramite en el departamento ingresado: " + departamento + " !!!");
        }
    }

}
