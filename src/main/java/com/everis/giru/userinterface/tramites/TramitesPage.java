package com.everis.giru.userinterface.tramites;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class TramitesPage {
    
    //Combo tipo de documento
    public static final Target CBX_TIPO_DOC = Target.the("Combo tramites por tipo de documento").located(By.id("search-claim-form__document-type"));
    public static final Target DIV_SELECT_NRO_TRAMITE = Target.the("Select N° de tramite").located(By.id("TRAMITE"));
    public static final Target DIV_SELECT_DNI = Target.the("Select DNI").located(By.id("DNI"));
    public static final Target DIV_SELECT_CE = Target.the("Select CE").located(By.id("CE"));
    //Button Buscar
    public static final Target INP_NUMERO_DOC = Target.the("Número de documento").located(By.id("search-claim-form__txt-document-number"));
    public static final Target BTN_BUSCAR = Target.the("Botón buscar").located(By.xpath("//img[@alt='buscar']"));

    ///////////////////////////////////////////////////////////////Departamento GPYR//////////////////////////////////////////////////////////////////////////////
    //Especialista GPYR
    //Clic en Menú
    public static final Target SIDENAV_MENU_TRAMITES_ESPECIALISTA_GPYR = Target.the("Menu todos mis tramites ESPECIALISTA_GPYR").located(By.id("item-5"));
    public static final Target SIDENAV_MENU_COBROS_INDEBIDOS_ESPECIALISTA_GPYR = Target.the("Menu Cobros Indebidos ESPECIALISTA_GPYR").located(By.id("item-7"));
    public static final Target SIDENAV_MENU_EXO_COBROS_ESPECIALISTA_GPYR = Target.the("Menu Exoneracion de Cobros ESPECIALISTA_GPYR").located(By.id("item-8"));
    public static final Target SIDENAV_MENU_ANULACION_TC_ESPECIALISTA_GPYR = Target.the("Menu Anulacion de TC ESPECIALISTA_GPYR").located(By.id("item-15"));
    public static final Target SIDENAV_MENU_FINALIZ_TC_ESPECIALISTA_GPYR = Target.the("Menu Finalizacion de TC ESPECIALISTA_GPYR").located(By.id("item-16"));
    //TODO: ADD NUEVAS TIPOLOGIAS GPYR
    public static final Target SIDENAV_MENU_TRANSACCION_MAL_ONO_PROCESADA_ESPECIALISTA_GPYR = Target.the("Menu Transacción mal o no procesada").located(By.id("item-19"));
    public static final Target SIDENAV_MENU_PAGO_ANTICIPADO_ADELANTO_CUOTAS_ESPECIALISTA_GPYR = Target.the("Menu Pago Anticipado y Adelanto de Cuotas").located(By.id("item-20"));
    public static final Target SIDENAV_MENU_DEVOLUCION_SALDO_ACREEDOR_ESPECIALISTA_GPYR = Target.the("Menu Devolucion de saldo acreedor").located(By.id("item-22"));
    public static final Target SIDENAV_MENU_PROGRAMA_RECOMPESAS_ESPECIALISTA_GPYR = Target.the("Menu Programa de Recompensas").located(By.id("item-31"));
    public static final Target SIDENAV_MENU_SISTEMA_RECOMPENSAS_ESPECIALISTA_GPYR = Target.the("Menu Sistema de Recompensas").located(By.id("item-32"));
    public static final Target SIDENAV_MENU_INSUFICIENTE_INFORMACION_ESPECIALISTA_GPYR = Target.the("Menu Insuficiente información").located(By.id("item-33"));
    public static final Target SIDENAV_MENU_INCORRECTA_APLICACION_CD_CUOTAS_ESPECIALISTA_GPYR = Target.the("Menu Incorrecta aplicación CD/EC/C. cuotas").located(By.id("item-37"));

    //Supervisor GPYR
    public static final Target SIDENAV_MENU_TRAMITES_SUPERVISOR_GPYR = Target.the("MENU_TRAMITES_SUPERVISOR_GPYR").located(By.id("item-1"));
    public static final Target SIDENAV_MENU_MIEQUIPO_SUPERVISOR_GPYR = Target.the("MENU_MIEQUIPO_SUPERVISOR_GPYR").located(By.id("item-4"));
    public static final Target SIDENAV_MENU_ASIGNAR_SUPERVISOR_GPYR = Target.the("MENU_ASIGNAR_SUPERVISOR_GPYR ").located(By.id("item-2"));
    public static final Target SIDENAV_MENU_RESOLVER_SUPERVISOR_GPYR = Target.the("MENU_RESOLVER_SUPERVISOR_GPYR").located(By.id("item-3"));

    ///////////////////////////////////////////////////////////////Departamento SEGUROS//////////////////////////////////////////////////////////////////////////////
    //Especialista Seguros
    public static final Target SIDENAV_MENU_TRAMITES_ESPECIALISTA_SEGUROS = Target.the("Menu todos mis tramites ESPECIALISTA_SEGUROS").located(By.id("item-5"));
    public static final Target SIDENAV_MENU_COBROS_INDEBIDOS_ESPECIALISTA_SEGUROS = Target.the("Menu Cobros Indebidos ESPECIALISTA_SEGUROS").located(By.id("item-7"));
    public static final Target SIDENAV_MENU_EXO_COBROS_ESPECIALISTA_SEGUROS = Target.the("Menu Exoneracion de Cobros ESPECIALISTA_SEGUROS").located(By.id("item-8"));
    public static final Target SIDENAV_MENU_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS = Target.the("Menu CNR ESPECIALISTA_SEGUROS").located(By.id("item-11"));
    public static final Target SIDENAV_MENU_MONITOREAR_ESPECIALISTA_SEGUROS = Target.the("Menu Monitorear ESPECIALISTA_SEGUROS").located(By.id("item-12"));

    //Supervisor Seguros
    public static final Target SIDENAV_MENU_TRAMITES_SUPERVISOR_SEGUROS = Target.the("MENU_TRAMITES_SUPERVISOR_SEGUROS").located(By.id("item-1"));
    public static final Target SIDENAV_MENU_MIEQUIPO_SUPERVISOR_SEGUROS = Target.the("MENU_MIEQUIPO_SUPERVISOR_SEGUROS").located(By.id("item-4"));
    public static final Target SIDENAV_MENU_ASIGNAR_SUPERVISOR_SEGUROS = Target.the("MENU_ASIGNAR_SUPERVISOR_SEGUROS").located(By.id("item-2"));
    public static final Target SIDENAV_MENU_RESOLVER_SUPERVISOR_SEGUROS = Target.the("MENU_RESOLVER_SUPERVISOR_SEGUROS").located(By.id("item-3"));
    public static final Target SIDENAV_MENU_MONITOREAR_SUPERVISOR_SEGUROS = Target.the("MENU_MONITOREAR_SUPERVISOR_SEGUR").located(By.id("item-12"));

    ////////////////////////////////////////////////////////////Departamento CONTROVERSIAS//////////////////////////////////////////////////////////////////////////////
    //Especialista Controversias
    public static final Target SIDENAV_MENU_TRAMITES_ESPECIALISTA_CONTROVERSIAS = Target.the("Menu todos mis tramites ESPECIALISTA_CONTROVERSIAS").located(By.id("item-5"));
    public static final Target SIDENAV_MENU_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS = Target.the("MENU_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS").located(By.id("item-11"));
    public static final Target SIDENAV_MENU_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS = Target.the("MENU_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS").located(By.id("item-13"));
    public static final Target SIDENAV_MENU_MONITOREAR_ESPECIALISTA_CONTROVERSIAS = Target.the("MENU_MONITOREAR_ESPECIALISTA_CONTROVERSIAS").located(By.id("item-12"));
    public static final Target SIDENAV_MENU_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS = Target.the("MENU_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS").located(By.id("item-17"));

    //Supervisor Controversias
    public static final Target SIDENAV_MENU_TRAMITES_SUPERVISOR_CONTROVERSIAS = Target.the("Menu todos mis tramites SUPERVISOR_CONTROVERSIAS").located(By.id("item-1"));
    public static final Target SIDENAV_MENU_MIEQUIPO_SUPERVISOR_CONTROVERSIAS = Target.the("Menu MIEQUIPO_SUPERVISOR_CONTROVERSIAS").located(By.id("item-4"));
    public static final Target SIDENAV_MENU_ASIGNAR_SUPERVISOR_CONTROVERSIAS = Target.the("Menu ASIGNAR_SUPERVISOR_CONTROVERSIAS").located(By.id("item-2"));
    public static final Target SIDENAV_MENU_RESOLVER_SUPERVISOR_CONTROVERSIAS = Target.the("Menu RESOLVER_SUPERVISOR_CONTROVERSIAS").located(By.id("item-3"));
    public static final Target SIDENAV_MENU_MONITOREAR_SUPERVISOR_CONTROVERSIAS = Target.the("Menu MONITOREAR_SUPERVISOR_CONTROVERSIAS").located(By.id("item-12"));
    public static final Target SIDENAV_MENU_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS = Target.the("Menu TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS").located(By.id("item-17"));

    //////////////////////////////////////////////////////////Departamento U SEGTO. PEDIDOS RECLAMOS//////////////////////////////////////////////////////////////////////////////
    //Especialista USPR
    public static final Target SIDENAV_MENU_TRAMITES_ESPECIALISTA_USPR = Target.the("Menu todos mis tramites").located(By.id("item-5"));
    public static final Target SIDENAV_MENU_COBROS_INDEBIDOS_ESPECIALISTA_USPR = Target.the("Menu Cobros Indebidos").located(By.id("item-7"));
    public static final Target SIDENAV_MENU_EXO_COBROS_ESPECIALISTA_USPR = Target.the("Menu Exoneracion de Cobros").located(By.id("item-8"));
    public static final Target SIDENAV_MENU_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR = Target.the("Menu Consumos Mal Procesados").located(By.id("item-13"));
    public static final Target SIDENAV_MENU_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR = Target.the("Menu Consumos No Reconocidos").located(By.id("item-11"));
    //TODO: ADD NUEVAS TIPOLOGIAS USPR
    public static final Target SIDENAV_MENU_TRANSACCION_MAL_ONO_PROCESADA_ESPECIALISTA_USPR = Target.the("Menu Transacción mal o no procesada").located(By.id("item-19"));
    public static final Target SIDENAV_MENU_DUPLICADO_ESTADO_CUENTA_SC_ESPECIALISTA_USPR = Target.the("Menu Duplicado de Estado de Cuenta/Servicio de Copias").located(By.id("item-27"));
    public static final Target SIDENAV_MENU_SISTEMA_DE_RECOMPENSAS_ESPECIALISTA_USPR = Target.the("Menu Sistema de Recompensas").located(By.id("item-32"));
    public static final Target SIDENAV_MENU_DESACUERDO_CONDICIONES_TT_ESPECIALISTA_USPR = Target.the("Menu Desacuerdo de Condiciones/Tasas/Tarifas").located(By.id("item-36"));
    public static final Target SIDENAV_MENU_CONSUMOS_RETIPIFICAR_ESPECIALISTA_USPR = Target.the("Menu Por retipificar").located(By.id("item-10"));

    //Supervisor USPR
    public static final Target SIDENAV_MENU_TRAMITES_SUPERVISOR_USPR = Target.the("Menu todos mis tramites SUPERVISOR_USPR").located(By.id("item-1"));
    public static final Target SIDENAV_MENU_MIEQUIPO_SUPERVISOR_USPR = Target.the("Menu MIEQUIPO_SUPERVISOR_USPR").located(By.id("item-4"));
    public static final Target SIDENAV_MENU_ASIGNAR_SUPERVISOR_USPR = Target.the("Menu ASIGNAR_SUPERVISOR_USPR").located(By.id("item-2"));
    public static final Target SIDENAV_MENU_RESOLVER_SUPERVISOR_USPR = Target.the("Menu RESOLVER_SUPERVISOR_USPR").located(By.id("item-3"));


}
