package com.everis.giru.userinterface.tramites.paginacionPage;

import com.everis.giru.utils.ApiCommons;
import com.everis.giru.utils.Shadow;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.WebElement;

public class ValidaPaginacionPage extends PageObject {

    Shadow general = new Shadow();

    public int contador = 0;
    public String Scontador;
    public int numTotalRegistros;
    String usuarioSesion = ApiCommons.USUARIO;

    //MAPEO DE ELEMENTOS - VALIDACIÓN FILTROS
    @FindBy(xpath = "//*[@id='grid']/giru-specialist-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR;
    //TODO ADD NUEVAS TIPOLOGIAS - GPYR
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_TRANSACCION_MAL_ONO_PROCESADA_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_PAGO_ANTICIPADO_ADELANTO_CUOTAS_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_DEVOLUCION_SALDO_ACREEDOR_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_PROGRAMA_RECOMPESAS_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SISTEMA_RECOMPENSAS_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_INSUFICIENTE_INFORMACION_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_INCORRECTA_APLICACION_CD_CUOTAS_ESPECIALISTA_GPYR;

    @FindBy(xpath = "//*[@id='grid']/giru-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SUPERVISOR_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-my-team/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-to-assigned/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-to-resolve/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR;

    @FindBy(xpath = "//*[@id='grid']/giru-specialist-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ESPECIALISTA_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-specialist-to-monitor/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS;

    @FindBy(xpath = "//*[@id='grid']/giru-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SUPERVISOR_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-my-team/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-assigned/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-resolve/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-monitor/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS;

    @FindBy(xpath = "//*[@id='grid']/giru-specialist-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-specialist-to-monitor/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-specialist-to-transactions-in-process/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS;

    @FindBy(xpath = "//*[@id='grid']/giru-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-my-team/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-assigned/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-resolve/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-monitor/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-supervisor-trx/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS;

    @FindBy(xpath = "//*[@id='grid']/giru-specialist-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR;
    //TODO ADD NUEVAS TIPOLOGIAS - USPR
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_TRANSACCION_MAL_NO_PROCESADA_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_DUPLICADO_ESTADO_CUENTA_SC_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SISTEMA_RECOMPENSAS_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_DESACUERDO_CONDICIONES_TT_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-task/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR;

    @FindBy(xpath = "//*[@id='grid']/giru-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SUPERVISOR_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-my-team/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-to-assigned/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-to-resolve/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR;

    //TODO: Validacion de filtros
    public boolean validaPaginacion(String departamento, String perfil, String tipologia, String registros) {

        boolean validaPaginacion = true;

        switch (departamento) {
            case "GPYR":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }

                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");

                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }

                                }
                                break;
                            case "CobrosIndebidos":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "ExoneracionCobros":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "AnulacionTC":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "FinalizacionTC":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "TransaccionMalNoProcesada":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_TRANSACCION_MAL_ONO_PROCESADA_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "PagoAnticipadoAdelantoCuotas":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_PAGO_ANTICIPADO_ADELANTO_CUOTAS_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "DevolucionSaldoAcreedor":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_DEVOLUCION_SALDO_ACREEDOR_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "ProgramaRecompensas":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_PROGRAMA_RECOMPESAS_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "SistemaRecompensas":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_SISTEMA_RECOMPENSAS_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "InsuficienteInformacion":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_INSUFICIENTE_INFORMACION_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "IncorrectaAplicacionCuotas":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_INCORRECTA_APLICACION_CD_CUOTAS_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;

                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA VALIDAR PAGINACION: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "MiEquipo":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "Asignar":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "Resolver":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA VALIDAR PAGINACION: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                }
                break;
            case "Seguros":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "CobrosIndebidos":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "ExoneracionCobros":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "ConsumosNoReconocidos":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "Monitorear":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA VALIDAR PAGINACION: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "MiEquipo":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "Asignar":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "Resolver":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "Monitorear":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA VALIDAR PAGINACION: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                }
                break;
            case "Retenciones":
                break;
            case "Controversias":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "ConsumosNoReconocidos":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "ConsumosMalProcesados":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "Monitorear":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "TransaccionesEnProceso":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA VALIDAR PAGINACION: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "MiEquipo":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "Asignar":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "Resolver":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "Monitorear":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "TransaccionesEnProceso":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA VALIDAR PAGINACION: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                }
                break;
            case "USPR":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "CobrosIndebidos":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "ExoneracionCobros":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "ConsumosMalProcesados":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "ConsumosNoReconocidos":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;

                            case "TransaccionMalNoProcesada":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_TRANSACCION_MAL_NO_PROCESADA_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;

                            case "DuplicadoEstadoCuentaSC":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_DUPLICADO_ESTADO_CUENTA_SC_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;

                            case "SistemaRecompensas":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_SISTEMA_RECOMPENSAS_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;

                            case "DesacuerdoCondicionesTT":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_DESACUERDO_CONDICIONES_TT_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;

                            case "Retipificar":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA VALIDAR PAGINACION: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "MiEquipo":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "Asignar":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            case "Resolver":
                                try {
                                    for (int i = 1; i < 100; ++i) {
                                        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)", "", "");
                                        String getTramite = valueTramite.getText();
                                        contador++;
                                        System.out.println("N² de tramite: " + getTramite + " y Contador es: " + contador);
                                    }
                                } catch (Exception e) {
                                    Scontador = String.valueOf(contador);
                                    numTotalRegistros = Integer.parseInt(registros);
                                    if (contador <= numTotalRegistros) {
                                        validaPaginacion = true;
                                        System.out.println("\n**************************************************************\n\tValidación correcta con: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    } else {
                                        validaPaginacion = false;
                                        System.out.println("\n**************************************************************\n\tValidación incorrecta, existen: " + Scontador + " registros con Tabulación de: " + registros + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                    }
                                }
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA VALIDAR PAGINACION: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                }
                break;
        }

        return validaPaginacion;
    }

    public String traerContador() {
        Scontador = String.valueOf(numTotalRegistros);
        return Scontador;
    }


}
