package com.everis.giru.userinterface.tramites.filtrosPage;

import com.everis.giru.utils.Shadow;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class GetFiltroMedioRespuestaPage extends PageObject {

    Shadow general = new Shadow();

    @FindBy(xpath = "//*[@id='radio-list__radio-domain-options']")
    WebElement LBL_ID_RADIOBUTTOM_MASFILTROS;

    public void seleccionaFiltroMedioRespuesta(String fMedioRespuesta) {

        switch (fMedioRespuesta) {
            case "Email":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_MASFILTROS));
                WebElement btnEmail = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_MASFILTROS, "div > div:nth-child(2) > label", "", "", "");
                btnEmail.click();
                break;
            case "Domicilio":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_MASFILTROS));
                WebElement btnDomicilio = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_MASFILTROS, "div > div:nth-child(3) > label", "", "", "");
                btnDomicilio.click();
                break;
            default:
                System.out.println("No se encuentra ningún filtro MEDIO DE RESPUESTA !!!");
        }
    }


}
