package com.everis.giru.userinterface.tramites.filtrosPage;

import com.everis.giru.utils.Shadow;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class GetFiltroProductosPage extends PageObject {

    Shadow general = new Shadow();

    @FindBy(xpath = "//*[@id='radio-list__radio-option']")
    WebElement LBL_ID_RADIOBUTTOM_PRODUCTO;

    public void seleccionaFiltroProductos(String fProductos) {

        switch (fProductos) {
            case "Tarjeta de Crédito":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_PRODUCTO));
                WebElement btnTC = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_PRODUCTO, "div > div:nth-child(1) > label", "", "", "");
                btnTC.click();
                break;
            case "Tarjeta de Débito":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_PRODUCTO));
                WebElement btnTD = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_PRODUCTO, "div > div:nth-child(2) > label", "", "", "");
                btnTD.click();
                break;
//            case "Cuentas":
//                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_PRODUCTO));
//                WebElement btnCuentas = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_PRODUCTO, "div > div:nth-child(3) > label", "", "", "");
//                btnCuentas.click();
//                break;

            default:
                System.out.println("No se encuentra Filtro Producto !!!");
        }

    }


}
