package com.everis.giru.userinterface.tramites.paginacionPage;

import com.everis.giru.utils.Shadow;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.WebElement;

public class GetSeleccionarPaginacionPage extends PageObject {

    Shadow general = new Shadow();

    //MAPEO DE ELEMENTOS - SELECCIONAR PAGINACION
    @FindBy(xpath = "//*[@id='grid']/giru-specialist-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR;
    //TODO ADD NUEVAS TIPOLOGIAS - GPYR
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_TRANSACCION_MAL_ONO_PROCESADA_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_PAGO_ANTICIPADO_ADELANTO_CUOTAS_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_DEVOLUCION_SALDO_ACREEDOR_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_PROGRAMA_RECOMPESAS_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SISTEMA_RECOMPENSAS_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_INSUFICIENTE_INFORMACION_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_INCORRECTA_APLICACION_CD_CUOTAS_ESPECIALISTA_GPYR;

    @FindBy(xpath = "//*[@id='grid']/giru-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SUPERVISOR_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-my-team/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-to-assigned/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-to-resolve/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR;

    @FindBy(xpath = "//*[@id='grid']/giru-specialist-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ESPECIALISTA_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-specialist-to-monitor/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS;

    @FindBy(xpath = "//*[@id='grid']/giru-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SUPERVISOR_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-my-team/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-assigned/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-resolve/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-monitor/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS;

    @FindBy(xpath = "//*[@id='grid']/giru-specialist-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-specialist-to-monitor/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-specialist-to-transactions-in-process/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS;

    @FindBy(xpath = "//*[@id='grid']/giru-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-my-team/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-assigned/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-resolve/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-monitor/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-supervisor-trx/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS;

    @FindBy(xpath = "//*[@id='grid']/giru-specialist-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR;
    //TODO ADD NUEVAS TIPOLOGIAS - USPR
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_TRANSACCION_MAL_NO_PROCESADA_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_DUPLICADO_ESTADO_CUENTA_SC_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SISTEMA_RECOMPENSAS_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_DESACUERDO_CONDICIONES_TT_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-task/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR;

    @FindBy(xpath = "//*[@id='grid']/giru-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SUPERVISOR_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-my-team/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-to-assigned/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-to-resolve/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR;

    //TODO: seleccionar Paginacion
    public void seleccionarPaginacion(String departamento, String perfil, String tipologia, String registros) {

        switch (departamento) {
            case "GPYR":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "CobrosIndebidos":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "ExoneracionCobros":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "AnulacionTC":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "FinalizacionTC":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "TransaccionMalNoProcesada":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_TRANSACCION_MAL_ONO_PROCESADA_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_TRANSACCION_MAL_ONO_PROCESADA_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_TRANSACCION_MAL_ONO_PROCESADA_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "PagoAnticipadoAdelantoCuotas":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_PAGO_ANTICIPADO_ADELANTO_CUOTAS_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_PAGO_ANTICIPADO_ADELANTO_CUOTAS_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_PAGO_ANTICIPADO_ADELANTO_CUOTAS_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "DevolucionSaldoAcreedor":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_DEVOLUCION_SALDO_ACREEDOR_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_DEVOLUCION_SALDO_ACREEDOR_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_DEVOLUCION_SALDO_ACREEDOR_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "ProgramaRecompensas":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_PROGRAMA_RECOMPESAS_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_PROGRAMA_RECOMPESAS_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_PROGRAMA_RECOMPESAS_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "SistemaRecompensas":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_SISTEMA_RECOMPENSAS_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_SISTEMA_RECOMPENSAS_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_SISTEMA_RECOMPENSAS_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "InsuficienteInformacion":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_INSUFICIENTE_INFORMACION_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_INSUFICIENTE_INFORMACION_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_INSUFICIENTE_INFORMACION_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "IncorrectaAplicacionCuotas":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_INCORRECTA_APLICACION_CD_CUOTAS_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_INCORRECTA_APLICACION_CD_CUOTAS_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_INCORRECTA_APLICACION_CD_CUOTAS_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;

                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA SELECCIONAR PAGINACION: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_GPYR, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_GPYR, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_GPYR, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "MiEquipo":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "Asignar":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "Resolver":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA SELECCIONAR PAGINACION: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                }
                break;
            case "Seguros":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "CobrosIndebidos":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "ExoneracionCobros":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "ConsumosNoReconocidos":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "Monitorear":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA SELECCIONAR PAGINACION: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "MiEquipo":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "Asignar":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "Resolver":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "Monitorear":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA SELECCIONAR PAGINACION: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                }
                break;
            case "Retenciones":
                break;
            case "Controversias":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "ConsumosNoReconocidos":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }

                                break;
                            case "ConsumosMalProcesados":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "Monitorear":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "TransaccionesEnProceso":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA SELECCIONAR PAGINACION: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "MiEquipo":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "Asignar":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "Resolver":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "Monitorear":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "TransaccionesEnProceso":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA SELECCIONAR PAGINACION: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                }
                break;
            case "USPR":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "CobrosIndebidos":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "ExoneracionCobros":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "ConsumosMalProcesados":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "ConsumosNoReconocidos":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "TransaccionMalNoProcesada":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_TRANSACCION_MAL_NO_PROCESADA_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_TRANSACCION_MAL_NO_PROCESADA_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_TRANSACCION_MAL_NO_PROCESADA_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "DuplicadoEstadoCuentaSC":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_DUPLICADO_ESTADO_CUENTA_SC_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_DUPLICADO_ESTADO_CUENTA_SC_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_DUPLICADO_ESTADO_CUENTA_SC_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "SistemaRecompensas":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_SISTEMA_RECOMPENSAS_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_SISTEMA_RECOMPENSAS_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_SISTEMA_RECOMPENSAS_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "DesacuerdoCondicionesTT":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_DESACUERDO_CONDICIONES_TT_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_DESACUERDO_CONDICIONES_TT_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_DESACUERDO_CONDICIONES_TT_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;

                            case "Retipificar":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA SELECCIONAR PAGINACION: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_USPR, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_USPR, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_USPR, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "MiEquipo":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "Asignar":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            case "Resolver":
                                switch (registros) {
                                    case "20":
                                        WebElement LI_FILTRO_PAGINACION_VEINTE = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR, "ibk-pagination-group2", "ibk-select", "#\\32 0", "");
                                        LI_FILTRO_PAGINACION_VEINTE.click();
                                        break;
                                    case "30":
                                        WebElement LI_FILTRO_PAGINACION_TREINTA = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR, "ibk-pagination-group2", "ibk-select", "#\\33 0", "");
                                        LI_FILTRO_PAGINACION_TREINTA.click();
                                        break;
                                    case "50":
                                        WebElement LI_FILTRO_PAGINACION_CINCUENTA = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR, "ibk-pagination-group2", "ibk-select", "#\\35 0", "");
                                        LI_FILTRO_PAGINACION_CINCUENTA.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra ningun paginacion!!!");
                                }
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA SELECCIONAR PAGINACION: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                }
                break;
        }

    }


}
