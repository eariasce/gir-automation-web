package com.everis.giru.userinterface.tramites.paginacionPage;

import com.everis.giru.utils.Shadow;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class GetDropdownPaginacionPage extends PageObject {

    Shadow general = new Shadow();

    //MAPEO DE ELEMENTOS - SELECT DROPDOWN
    @FindBy(xpath = "//*[@id='grid']/giru-specialist-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR;
    //TODO ADD NUEVAS TIPOLOGIAS - GPYR
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_TRANSACCION_MAL_ONO_PROCESADA_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_PAGO_ANTICIPADO_ADELANTO_CUOTAS_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_DEVOLUCION_SALDO_ACREEDOR_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_PROGRAMA_RECOMPESAS_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SISTEMA_RECOMPENSAS_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_INSUFICIENTE_INFORMACION_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_INCORRECTA_APLICACION_CD_CUOTAS_ESPECIALISTA_GPYR;

    @FindBy(xpath = "//*[@id='grid']/giru-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SUPERVISOR_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-my-team/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-to-assigned/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-to-resolve/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR;

    @FindBy(xpath = "//*[@id='grid']/giru-specialist-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ESPECIALISTA_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-specialist-to-monitor/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS;

    @FindBy(xpath = "//*[@id='grid']/giru-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SUPERVISOR_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-my-team/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-assigned/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-resolve/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-monitor/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS;

    @FindBy(xpath = "//*[@id='grid']/giru-specialist-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-specialist-to-monitor/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-specialist-to-transactions-in-process/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS;

    @FindBy(xpath = "//*[@id='grid']/giru-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-my-team/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-assigned/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-resolve/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-monitor/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-supervisor-trx/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS;

    @FindBy(xpath = "//*[@id='grid']/giru-specialist-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR;
    //TODO ADD NUEVAS TIPOLOGIAS - USPR
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_TRANSACCION_MAL_NO_PROCESADA_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_DUPLICADO_ESTADO_CUENTA_SC_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SISTEMA_RECOMPENSAS_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_DESACUERDO_CONDICIONES_TT_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-task/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR;

    @FindBy(xpath = "//*[@id='grid']/giru-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SUPERVISOR_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-my-team/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-to-assigned/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-to-resolve/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR;

    //TODO: seleccionar Dropdown Paginacion
    public void seleccionarDropdownPaginacion(String departamento, String perfil, String tipologia) {

        switch (departamento) {
            case "GPYR":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ESPECIALISTA_GPYR));
                                WebElement selectDropdown = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdown.click();
                                break;
                            case "CobrosIndebidos":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR));
                                WebElement selectDropdownCI = general.expandContainer("3css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdownCI.click();
                                break;
                            case "ExoneracionCobros":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR));
                                WebElement selectDropdownEX = general.expandContainer("3css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdownEX.click();
                                break;
                            case "AnulacionTC":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR));
                                WebElement selectDropdownAnulTC = general.expandContainer("3css", LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdownAnulTC.click();
                                break;
                            case "FinalizacionTC":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR));
                                WebElement selectDropdownFinalTC = general.expandContainer("3css", LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdownFinalTC.click();
                                break;

                            case "TransaccionMalNoProcesada":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_TRANSACCION_MAL_ONO_PROCESADA_ESPECIALISTA_GPYR));
                                WebElement selectDropdownTMalNoProcesada = general.expandContainer("3css", LBL_ID_TABLA_TRANSACCION_MAL_ONO_PROCESADA_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdownTMalNoProcesada.click();
                                break;
                            case "PagoAnticipadoAdelantoCuotas":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_PAGO_ANTICIPADO_ADELANTO_CUOTAS_ESPECIALISTA_GPYR));
                                WebElement selectDropdownPAACuotas = general.expandContainer("3css", LBL_ID_TABLA_PAGO_ANTICIPADO_ADELANTO_CUOTAS_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdownPAACuotas.click();
                                break;
                            case "DevolucionSaldoAcreedor":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_DEVOLUCION_SALDO_ACREEDOR_ESPECIALISTA_GPYR));
                                WebElement selectDropdownDSaldoAcreedor = general.expandContainer("3css", LBL_ID_TABLA_DEVOLUCION_SALDO_ACREEDOR_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdownDSaldoAcreedor.click();
                                break;
                            case "ProgramaRecompensas":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_PROGRAMA_RECOMPESAS_ESPECIALISTA_GPYR));
                                WebElement selectDropdownPRecompensas = general.expandContainer("3css", LBL_ID_TABLA_PROGRAMA_RECOMPESAS_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdownPRecompensas.click();
                                break;
                            case "SistemaRecompensas":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SISTEMA_RECOMPENSAS_ESPECIALISTA_GPYR));
                                WebElement selectDropdownSRecompensas = general.expandContainer("3css", LBL_ID_TABLA_SISTEMA_RECOMPENSAS_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdownSRecompensas.click();
                                break;
                            case "InsuficienteInformacion":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_INSUFICIENTE_INFORMACION_ESPECIALISTA_GPYR));
                                WebElement selectDropdownIInformacion = general.expandContainer("3css", LBL_ID_TABLA_INSUFICIENTE_INFORMACION_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdownIInformacion.click();
                                break;
                            case "IncorrectaAplicacionCuotas":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_INCORRECTA_APLICACION_CD_CUOTAS_ESPECIALISTA_GPYR));
                                WebElement selectDropdownIACuotas = general.expandContainer("3css", LBL_ID_TABLA_INCORRECTA_APLICACION_CD_CUOTAS_ESPECIALISTA_GPYR, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdownIACuotas.click();
                                break;

                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA SELECCIONAR DROPDOWN PAGINACION: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SUPERVISOR_GPYR));
                                WebElement selectDropdown = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_GPYR, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdown.click();
                                break;
                            case "MiEquipo":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR));
                                WebElement selectDropdownMiEquipo = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdownMiEquipo.click();
                                break;
                            case "Asignar":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR));
                                WebElement selectDropdownAsignar = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdownAsignar.click();
                                break;
                            case "Resolver":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR));
                                WebElement selectDropdownResolver = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdownResolver.click();
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA SELECCIONAR DROPDOWN PAGINACION: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                }
                break;
            case "Seguros":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ESPECIALISTA_SEGUROS));
                                WebElement selectDropdown = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdown.click();
                                break;
                            case "CobrosIndebidos":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS));
                                WebElement selectDropdownCI = general.expandContainer("3css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdownCI.click();
                                break;
                            case "ExoneracionCobros":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS));
                                WebElement selectDropdownEX = general.expandContainer("3css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdownEX.click();
                                break;
                            case "ConsumosNoReconocidos":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS));
                                WebElement selectDropdownCNR = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdownCNR.click();
                                break;
                            case "Monitorear":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS));
                                WebElement selectDropdownMonitorear = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdownMonitorear.click();
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA SELECCIONAR DROPDOWN PAGINACION: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SUPERVISOR_SEGUROS));
                                WebElement selectDropdown = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdown.click();
                                break;
                            case "MiEquipo":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS));
                                WebElement selectDropdownMiEquipo = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdownMiEquipo.click();
                                break;
                            case "Asignar":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS));
                                WebElement selectDropdownAsignar = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdownAsignar.click();
                                break;
                            case "Resolver":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS));
                                WebElement selectDropdownResolver = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdownResolver.click();
                                break;
                            case "Monitorear":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS));
                                WebElement selectDropdownMonitorear = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdownMonitorear.click();
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA SELECCIONAR DROPDOWN PAGINACION: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                }
                break;
            case "Retenciones":
                break;
            case "Controversias":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS));
                                WebElement selectDropdown = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdown.click();
                                break;
                            case "ConsumosNoReconocidos":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS));
                                WebElement selectDropdownCNR = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdownCNR.click();

                                break;
                            case "ConsumosMalProcesados":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS));
                                WebElement selectDropdownCMP = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdownCMP.click();
                                break;
                            case "Monitorear":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS));
                                WebElement selectDropdownMonitorear = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdownMonitorear.click();
                                break;
                            case "TransaccionesEnProceso":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS));
                                WebElement selectDropdownTEP = general.expandContainer("3css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdownTEP.click();
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA SELECCIONAR DROPDOWN PAGINACION: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS));
                                WebElement selectDropdown = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdown.click();
                                break;
                            case "MiEquipo":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS));
                                WebElement selectDropdownMiEquipo = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdownMiEquipo.click();
                                break;
                            case "Asignar":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS));
                                WebElement selectDropdownAsignar = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdownAsignar.click();
                                break;
                            case "Resolver":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS));
                                WebElement selectDropdownResolver = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdownResolver.click();
                                break;
                            case "Monitorear":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS));
                                WebElement selectDropdownMonitorear = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdownMonitorear.click();
                                break;
                            case "TransaccionesEnProceso":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS));
                                WebElement selectDropdownTEP = general.expandContainer("3css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdownTEP.click();
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA SELECCIONAR DROPDOWN PAGINACION: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                }
                break;
            case "USPR":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ESPECIALISTA_USPR));
                                WebElement selectDropdown = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdown.click();
                                break;
                            case "CobrosIndebidos":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR));
                                WebElement selectDropdownCI = general.expandContainer("3css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdownCI.click();
                                break;
                            case "ExoneracionCobros":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR));
                                WebElement selectDropdownEXC = general.expandContainer("3css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdownEXC.click();
                                break;
                            case "ConsumosMalProcesados":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR));
                                WebElement selectDropdownCMP = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdownCMP.click();
                                break;
                            case "ConsumosNoReconocidos":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR));
                                WebElement selectDropdownCNR = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdownCNR.click();
                                break;
                            case "TransaccionMalNoProcesada":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_TRANSACCION_MAL_NO_PROCESADA_ESPECIALISTA_USPR));
                                WebElement selectDropdownTMalNoProcesada = general.expandContainer("3css", LBL_ID_TABLA_TRANSACCION_MAL_NO_PROCESADA_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdownTMalNoProcesada.click();
                                break;
                            case "DuplicadoEstadoCuentaSC":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_DUPLICADO_ESTADO_CUENTA_SC_ESPECIALISTA_USPR));
                                WebElement selectDropdownDEstadoCuentaSC = general.expandContainer("3css", LBL_ID_TABLA_DUPLICADO_ESTADO_CUENTA_SC_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdownDEstadoCuentaSC.click();
                                break;
                            case "SistemaRecompensas":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SISTEMA_RECOMPENSAS_ESPECIALISTA_USPR));
                                WebElement selectDropdownSRecompensas = general.expandContainer("3css", LBL_ID_TABLA_SISTEMA_RECOMPENSAS_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdownSRecompensas.click();
                                break;
                            case "DesacuerdoCondicionesTT":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_DESACUERDO_CONDICIONES_TT_ESPECIALISTA_USPR));
                                WebElement selectDropdownDCondicionesTT = general.expandContainer("3css", LBL_ID_TABLA_DESACUERDO_CONDICIONES_TT_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdownDCondicionesTT.click();
                                break;
                                
                            case "Retipificar":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR));
                                WebElement selectDropdownRetificar = general.expandContainer("3css", LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdownRetificar.click();
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA SELECCIONAR DROPDOWN PAGINACION: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SUPERVISOR_USPR));
                                WebElement selectDropdown = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_USPR, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdown.click();
                                break;
                            case "MiEquipo":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR));
                                WebElement selectDropdownMiEquipo = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdownMiEquipo.click();
                                break;
                            case "Asignar":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR));
                                WebElement selectDropdownAsignar = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdownAsignar.click();
                                break;
                            case "Resolver":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR));
                                WebElement selectDropdownResolver = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR, "ibk-pagination-group2", "ibk-select", "#dropdown-select", "");
                                selectDropdownResolver.click();
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA SELECCIONAR DROPDOWN PAGINACION: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                }
                break;
        }


    }


}
