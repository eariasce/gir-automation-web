package com.everis.giru.userinterface.tramites.validaFiltros;

import com.everis.giru.utils.Shadow;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.WebElement;

import static org.assertj.core.api.Assertions.assertThat;

public class ValidaFiltroTipoTramitePage extends PageObject {

    Shadow general = new Shadow();

    public int contador = 0;

    //MAPEO DE ELEMENTOS - VALIDACIÓN FILTROS
    @FindBy(xpath = "//*[@id='grid']/giru-specialist-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR;

    @FindBy(xpath = "//*[@id='grid']/giru-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SUPERVISOR_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-my-team/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-to-assigned/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-to-resolve/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR;

    @FindBy(xpath = "//*[@id='grid']/giru-specialist-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ESPECIALISTA_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-specialist-to-monitor/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS;

    @FindBy(xpath = "//*[@id='grid']/giru-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SUPERVISOR_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-my-team/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-assigned/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-resolve/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-monitor/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS;

    @FindBy(xpath = "//*[@id='grid']/giru-specialist-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-specialist-to-monitor/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-specialist-to-transactions-in-process/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS;

    @FindBy(xpath = "//*[@id='grid']/giru-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-my-team/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-assigned/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-resolve/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-monitor/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-supervisor-trx/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS;

    @FindBy(xpath = "//*[@id='grid']/giru-specialist-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-task/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR;

    @FindBy(xpath = "//*[@id='grid']/giru-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SUPERVISOR_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-my-team/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-to-assigned/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-to-resolve/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR;

    //TODO: Validacion de filtro - Tipo Tramite
    public boolean validaFiltroTipoTramite(String departamento, String perfil, String tipologia, String fTipoTramite) {

        boolean validaTipoTramite = false;

        switch (departamento) {

            case "GPYR":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_GPYR, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validaTipoTramite = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro < Tipo Tramite Front > encontrado: " + getTipoTramite + " vs Filtro < Tipo Tramite > Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaTipoTramite = true;

                                    }
                                }
                                break;
                            case "CobrosIndebidos":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validaTipoTramite = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro < Tipo Tramite Front > encontrado: " + getTipoTramite + " vs Filtro < Tipo Tramite > Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaTipoTramite = true;

                                    }
                                }
                                break;
                            case "ExoneracionCobros":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validaTipoTramite = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro < Tipo Tramite Front > encontrado: " + getTipoTramite + " vs Filtro < Tipo Tramite > Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaTipoTramite = true;

                                    }
                                }
                                break;
                            case "AnulacionTC":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validaTipoTramite = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro < Tipo Tramite Front > encontrado: " + getTipoTramite + " vs Filtro < Tipo Tramite > Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaTipoTramite = true;

                                    }
                                }
                                break;
                            case "FinalizacionTC":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validaTipoTramite = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro < Tipo Tramite Front > encontrado: " + getTipoTramite + " vs Filtro < Tipo Tramite > Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaTipoTramite = true;

                                    }
                                }
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA VALIDAR FILTRO TIPO TRAMITE: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                    //TODO: Tipo Tramite Supervisor GPYR - No muestra en tabla
                    case "Supervisor":
                        /*switch (tipologia) {
                            case "TodosMisTramites":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_GPYR, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validaTipoTramite = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro < Tipo Tramite Front > encontrado: " + getTipoTramite + " vs Filtro < Tipo Tramite > Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaTipoTramite = true;

                                    }
                                }
                                break;
                            case "MiEquipo":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validaTipoTramite = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(12)", "", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro < Tipo Tramite Front > encontrado: " + getTipoTramite + " vs Filtro < Tipo Tramite > Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaTipoTramite = true;

                                    }
                                }
                                break;
                            case "Asignar":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validaTipoTramite = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(12)", "", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro < Tipo Tramite Front > encontrado: " + getTipoTramite + " vs Filtro < Tipo Tramite > Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaTipoTramite = true;

                                    }
                                }
                                break;
                            case "Resolver":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validaTipoTramite = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(12)", "", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro < Tipo Tramite Front > encontrado: " + getTipoTramite + " vs Filtro < Tipo Tramite > Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaTipoTramite = true;

                                    }
                                }
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA VALIDAR FILTRO TIPO TRAMITE: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }*/
                        break;
                }
                break;

            case "Seguros":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validaTipoTramite = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro < Tipo Tramite Front > encontrado: " + getTipoTramite + " vs Filtro < Tipo Tramite > Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaTipoTramite = true;

                                    }
                                }
                                break;
                            case "CobrosIndebidos":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validaTipoTramite = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro < Tipo Tramite Front > encontrado: " + getTipoTramite + " vs Filtro < Tipo Tramite > Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaTipoTramite = true;

                                    }
                                }
                                break;
                            case "ExoneracionCobros":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validaTipoTramite = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro < Tipo Tramite Front > encontrado: " + getTipoTramite + " vs Filtro < Tipo Tramite > Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaTipoTramite = true;

                                    }
                                }
                                break;
                            case "ConsumosNoReconocidos":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validaTipoTramite = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro < Tipo Tramite Front > encontrado: " + getTipoTramite + " vs Filtro < Tipo Tramite > Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaTipoTramite = true;

                                    }
                                }
                                break;
                            case "Monitorear":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validaTipoTramite = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro < Tipo Tramite Front > encontrado: " + getTipoTramite + " vs Filtro < Tipo Tramite > Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaTipoTramite = true;

                                    }
                                }
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA VALIDAR FILTRO TIPO TRAMITE: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                    //TODO: Tipo Tramite Supervisor Seguros - No muestra en tabla
                    case "Supervisor":
                        /*switch (tipologia) {
                            case "TodosMisTramites":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validaTipoTramite = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro < Tipo Tramite Front > encontrado: " + getTipoTramite + " vs Filtro < Tipo Tramite > Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaTipoTramite = true;

                                    }
                                }
                                break;
                            case "MiEquipo":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validaTipoTramite = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro < Tipo Tramite Front > encontrado: " + getTipoTramite + " vs Filtro < Tipo Tramite > Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaTipoTramite = true;

                                    }
                                }
                                break;
                            case "Asignar":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validaTipoTramite = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro < Tipo Tramite Front > encontrado: " + getTipoTramite + " vs Filtro < Tipo Tramite > Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaTipoTramite = true;

                                    }
                                }
                                break;
                            case "Resolver":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                   String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validaTipoTramite = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro < Tipo Tramite Front > encontrado: " + getTipoTramite + " vs Filtro < Tipo Tramite > Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaTipoTramite = true;

                                    }
                                }
                                break;
                            case "Monitorear":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validaTipoTramite = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro < Tipo Tramite Front > encontrado: " + getTipoTramite + " vs Filtro < Tipo Tramite > Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaTipoTramite = true;

                                    }
                                }
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA VALIDAR FILTRO TIPO TRAMITE: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }*/
                        break;
                }
                break;

            case "Retenciones":
                break;

            case "Controversias":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validaTipoTramite = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro < Tipo Tramite Front > encontrado: " + getTipoTramite + " vs Filtro < Tipo Tramite > Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaTipoTramite = true;

                                    }
                                }
                                break;
                            case "ConsumosNoReconocidos":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validaTipoTramite = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro < Tipo Tramite Front > encontrado: " + getTipoTramite + " vs Filtro < Tipo Tramite > Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaTipoTramite = true;

                                    }
                                }
                                break;
                            case "ConsumosMalProcesados":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validaTipoTramite = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro < Tipo Tramite Front > encontrado: " + getTipoTramite + " vs Filtro < Tipo Tramite > Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaTipoTramite = true;

                                    }
                                }
                                break;
                            case "Monitorear":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validaTipoTramite = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro < Tipo Tramite Front > encontrado: " + getTipoTramite + " vs Filtro < Tipo Tramite > Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaTipoTramite = true;

                                    }
                                }
                                break;
                            case "TransaccionesEnProceso":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validaTipoTramite = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro < Tipo Tramite Front > encontrado: " + getTipoTramite + " vs Filtro < Tipo Tramite > Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaTipoTramite = true;

                                    }
                                }
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA VALIDAR FILTRO TIPO TRAMITE: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                    //TODO: Tipo Tramite Supervisor Controversias - No muestra en tabla
                    case "Supervisor":
                       /* switch (tipologia) {
                            case "TodosMisTramites":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validaTipoTramite = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro < Tipo Tramite Front > encontrado: " + getTipoTramite + " vs Filtro < Tipo Tramite > Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaTipoTramite = true;

                                    }
                                }
                                break;
                            case "MiEquipo":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validaTipoTramite = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro < Tipo Tramite Front > encontrado: " + getTipoTramite + " vs Filtro < Tipo Tramite > Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaTipoTramite = true;

                                    }
                                }
                                break;
                            case "Asignar":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validaTipoTramite = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro < Tipo Tramite Front > encontrado: " + getTipoTramite + " vs Filtro < Tipo Tramite > Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaTipoTramite = true;

                                    }
                                }
                                break;
                            case "Resolver":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validaTipoTramite = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro < Tipo Tramite Front > encontrado: " + getTipoTramite + " vs Filtro < Tipo Tramite > Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaTipoTramite = true;

                                    }
                                }
                                break;
                            case "Monitorear":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validaTipoTramite = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro < Tipo Tramite Front > encontrado: " + getTipoTramite + " vs Filtro < Tipo Tramite > Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaTipoTramite = true;

                                    }
                                }
                                break;
                            case "TransaccionesEnProceso":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validaTipoTramite = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro < Tipo Tramite Front > encontrado: " + getTipoTramite + " vs Filtro < Tipo Tramite > Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaTipoTramite = true;

                                    }
                                }
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA VALIDAR FILTRO TIPO TRAMITE: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }*/
                        break;
                }
                break;

            case "USPR":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validaTipoTramite = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro < Tipo Tramite Front > encontrado: " + getTipoTramite + " vs Filtro < Tipo Tramite > Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaTipoTramite = true;

                                    }
                                }
                                break;
                            case "CobrosIndebidos":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validaTipoTramite = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro < Tipo Tramite Front > encontrado: " + getTipoTramite + " vs Filtro < Tipo Tramite > Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaTipoTramite = true;

                                    }
                                }
                                break;
                            case "ExoneracionCobros":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validaTipoTramite = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro < Tipo Tramite Front > encontrado: " + getTipoTramite + " vs Filtro < Tipo Tramite > Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaTipoTramite = true;

                                    }
                                }
                                break;
                            case "ConsumosMalProcesados":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validaTipoTramite = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro < Tipo Tramite Front > encontrado: " + getTipoTramite + " vs Filtro < Tipo Tramite > Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaTipoTramite = true;

                                    }
                                }
                                break;
                            case "ConsumosNoReconocidos":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validaTipoTramite = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro < Tipo Tramite Front > encontrado: " + getTipoTramite + " vs Filtro < Tipo Tramite > Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaTipoTramite = true;

                                    }
                                }
                                break;
                            case "Retipificar":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validaTipoTramite = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro < Tipo Tramite Front > encontrado: " + getTipoTramite + " vs Filtro < Tipo Tramite > Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaTipoTramite = true;

                                    }
                                }
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA VALIDAR FILTRO TIPO TRAMITE: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                    //TODO: Tipo Tramite Supervisor USPR - No muestra en tabla
                    case "Supervisor":
                        /*switch (tipologia) {
                            case "TodosMisTramites":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_USPR, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validaTipoTramite = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro < Tipo Tramite Front > encontrado: " + getTipoTramite + " vs Filtro < Tipo Tramite > Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaTipoTramite = true;

                                    }
                                }
                                break;
                            case "MiEquipo":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validaTipoTramite = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro < Tipo Tramite Front > encontrado: " + getTipoTramite + " vs Filtro < Tipo Tramite > Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaTipoTramite = true;

                                    }
                                }
                                break;
                            case "Asignar":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR, "ibk-grid", "ibk-h", "p", "");
                                    String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validaTipoTramite = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro < Tipo Tramite Front > encontrado: " + getTipoTramite + " vs Filtro < Tipo Tramite > Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaTipoTramite = true;

                                    }
                                }
                                break;
                            case "Resolver":
                                try {
                                    WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR, "ibk-grid", "ibk-h", "p", "");
                                     String getMessage = valueMessage.getText();
                                    if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                        System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                        validaTipoTramite = true;
                                    }
                                } catch (Exception e) {
                                    try {
                                        for (int i = 1; i < 100; ++i) {
                                            WebElement valueTipoTramite = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");
                                            String getTipoTramite = valueTipoTramite.getText();
                                            System.out.println("Filtro < Tipo Tramite Front > encontrado: " + getTipoTramite + " vs Filtro < Tipo Tramite > Deaseado: " + fTipoTramite);
                                            assertThat(getTipoTramite).isEqualTo(fTipoTramite);
                                            contador++;
                                        }

                                    } catch (Exception i) {
                                        validaTipoTramite = true;

                                    }
                                }
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA VALIDAR FILTRO TIPO TRAMITE: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }*/
                        break;
                }

                break;
            default:
                System.out.println("No existe filtro tipo tramite en el departamento ingresado: " + departamento + " !!!");
        }

        return validaTipoTramite;
    }


}
