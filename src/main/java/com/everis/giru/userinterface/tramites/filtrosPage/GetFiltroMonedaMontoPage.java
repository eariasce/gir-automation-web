package com.everis.giru.userinterface.tramites.filtrosPage;

import com.everis.giru.utils.Shadow;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class GetFiltroMonedaMontoPage extends PageObject {

    Shadow general = new Shadow();

    @FindBy(xpath = "//*[@id='radio-list__radio-currency']")
    WebElement LBL_ID_RADIOBUTTOM_MONEDA_MONTO;

    public void seleccionaFiltroMonedaMonto(String fMoneda, String fMontoDesde, String fMontoHasta) {

        switch (fMoneda) {
            case "PEN":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_MONEDA_MONTO));
                WebElement btnSoles = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_MONEDA_MONTO, "div > div:nth-child(1) > label", "", "", "");
                btnSoles.click();

                WebElement inpFiltroAmountMin = getDriver().findElement(By.cssSelector("input[formcontrolname='amountMin']"));
                inpFiltroAmountMin.sendKeys(fMontoDesde);

                WebElement inpFiltroAmountMax = getDriver().findElement(By.cssSelector("input[formcontrolname='amountMax']"));
                inpFiltroAmountMax.sendKeys(fMontoHasta);

                break;
            case "USD":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_MONEDA_MONTO));
                WebElement btnDolares = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_MONEDA_MONTO, "div > div:nth-child(2) > label", "", "", "");
                btnDolares.click();

                WebElement txtFiltroAmountMin = getDriver().findElement(By.cssSelector("input[formcontrolname='amountMin']"));
                txtFiltroAmountMin.sendKeys(fMontoDesde);

                WebElement txtFiltroAmountMax = getDriver().findElement(By.cssSelector("input[formcontrolname='amountMax']"));
                txtFiltroAmountMax.sendKeys(fMontoHasta);
                break;
            default:
                System.out.println("No se encuentra ningún tipo de moneda !!!");
        }

    }

}
