package com.everis.giru.userinterface.tramites.validaBusqueda;

import com.everis.giru.tasks.e2eTramites.GetHistoricalInbox;
import com.everis.giru.utils.Shadow;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class ValidaBuscarTramitePage extends PageObject {

    Shadow general = new Shadow();

    GetHistoricalInbox getHistoricalInbox = new GetHistoricalInbox();

    //TODO: MAPEO DE ELEMENTOS - VALIDACIÓN FILTROS
    @FindBy(xpath = "//*[@id='grid']/giru-specialist-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR;
    //TODO ADD NUEVAS TIPOLOGIAS - GPYR
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_TRANSACCION_MAL_ONO_PROCESADA_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_PAGO_ANTICIPADO_ADELANTO_CUOTAS_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_DEVOLUCION_SALDO_ACREEDOR_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_PROGRAMA_RECOMPESAS_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SISTEMA_RECOMPENSAS_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_INSUFICIENTE_INFORMACION_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_INCORRECTA_APLICACION_CD_CUOTAS_ESPECIALISTA_GPYR;

    @FindBy(xpath = "//*[@id='grid']/giru-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SUPERVISOR_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-my-team/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-to-assigned/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-to-resolve/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR;

    @FindBy(xpath = "//*[@id='grid']/giru-specialist-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ESPECIALISTA_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-specialist-to-monitor/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS;

    @FindBy(xpath = "//*[@id='grid']/giru-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SUPERVISOR_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-my-team/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-assigned/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-resolve/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-monitor/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS;

    @FindBy(xpath = "//*[@id='grid']/giru-specialist-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-specialist-to-monitor/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-specialist-to-transactions-in-process/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS;

    @FindBy(xpath = "//*[@id='grid']/giru-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-my-team/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-assigned/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-resolve/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-monitor/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-supervisor-trx/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS;

    @FindBy(xpath = "//*[@id='grid']/giru-specialist-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR;
    //TODO ADD NUEVAS TIPOLOGIAS - USPR
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_TRANSACCION_MAL_NO_PROCESADA_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_DUPLICADO_ESTADO_CUENTA_SC_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SISTEMA_RECOMPENSAS_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_DESACUERDO_CONDICIONES_TT_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-task/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR;
    
    @FindBy(xpath = "//*[@id='grid']/giru-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SUPERVISOR_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-my-team/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-to-assigned/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-to-resolve/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR;

    public void validaBuscarTramite() throws InterruptedException {
        String nroTramite = "NRO TRAMITE";
        String numeroDNI = "DNI";
        String numeroCE = "CE";
        String numeroTramite = null;
        String tipoTramite = null;

        String departamento = null;
        String perfil = null;
        String tipologia = null;

        try {
            //PARA VALIDACION DE BANDJEA
            numeroTramite = Serenity.sessionVariableCalled("numeroTramite").toString();
            tipoTramite = Serenity.sessionVariableCalled("tipoTramite").toString();
            departamento = Serenity.sessionVariableCalled("departamentoBuscar").toString();
            perfil = Serenity.sessionVariableCalled("perfilBuscar").toString();
            tipologia = Serenity.sessionVariableCalled("tipologiaBuscar").toString();
        } catch (Exception e) {
            // PARA FLUJO E2E
            numeroTramite = getHistoricalInbox.getNumeroTramiteFront();
            tipoTramite = "NRO TRAMITE";
            departamento = Serenity.sessionVariableCalled("departamentoBuscar").toString();
            perfil = Serenity.sessionVariableCalled("perfilBuscar").toString();
            tipologia = Serenity.sessionVariableCalled("tipologiaBuscar").toString();
        }
        Thread.sleep(1000);
        switch (departamento) {
            case "GPYR":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ESPECIALISTA_GPYR));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ESPECIALISTA_GPYR));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr > td:nth-child(17)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ESPECIALISTA_GPYR));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(17)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            case "CobrosIndebidos":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr > td:nth-child(17)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(17)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            case "ExoneracionCobros":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr > td:nth-child(17)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(17)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            case "AnulacionTC":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr > td:nth-child(17)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(17)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            case "FinalizacionTC":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr > td:nth-child(17)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(17)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            case "TransaccionMalNoProcesada":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_TRANSACCION_MAL_ONO_PROCESADA_ESPECIALISTA_GPYR));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_TRANSACCION_MAL_ONO_PROCESADA_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_TRANSACCION_MAL_ONO_PROCESADA_ESPECIALISTA_GPYR));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_TRANSACCION_MAL_ONO_PROCESADA_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr > td:nth-child(17)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_TRANSACCION_MAL_ONO_PROCESADA_ESPECIALISTA_GPYR));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_TRANSACCION_MAL_ONO_PROCESADA_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(17)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            case "PagoAnticipadoAdelantoCuotas":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_PAGO_ANTICIPADO_ADELANTO_CUOTAS_ESPECIALISTA_GPYR));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_PAGO_ANTICIPADO_ADELANTO_CUOTAS_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_PAGO_ANTICIPADO_ADELANTO_CUOTAS_ESPECIALISTA_GPYR));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_PAGO_ANTICIPADO_ADELANTO_CUOTAS_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr > td:nth-child(17)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_PAGO_ANTICIPADO_ADELANTO_CUOTAS_ESPECIALISTA_GPYR));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_PAGO_ANTICIPADO_ADELANTO_CUOTAS_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(17)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            case "DevolucionSaldoAcreedor":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_DEVOLUCION_SALDO_ACREEDOR_ESPECIALISTA_GPYR));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_DEVOLUCION_SALDO_ACREEDOR_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_DEVOLUCION_SALDO_ACREEDOR_ESPECIALISTA_GPYR));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_DEVOLUCION_SALDO_ACREEDOR_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr > td:nth-child(17)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_DEVOLUCION_SALDO_ACREEDOR_ESPECIALISTA_GPYR));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_DEVOLUCION_SALDO_ACREEDOR_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(17)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            case "ProgramaRecompensas":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_PROGRAMA_RECOMPESAS_ESPECIALISTA_GPYR));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_PROGRAMA_RECOMPESAS_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_PROGRAMA_RECOMPESAS_ESPECIALISTA_GPYR));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_PROGRAMA_RECOMPESAS_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr > td:nth-child(17)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_PROGRAMA_RECOMPESAS_ESPECIALISTA_GPYR));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_PROGRAMA_RECOMPESAS_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(17)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            case "SistemaRecompensas":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SISTEMA_RECOMPENSAS_ESPECIALISTA_GPYR));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_SISTEMA_RECOMPENSAS_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SISTEMA_RECOMPENSAS_ESPECIALISTA_GPYR));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_SISTEMA_RECOMPENSAS_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr > td:nth-child(17)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SISTEMA_RECOMPENSAS_ESPECIALISTA_GPYR));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_SISTEMA_RECOMPENSAS_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(17)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            case "InsuficienteInformacion":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_INSUFICIENTE_INFORMACION_ESPECIALISTA_GPYR));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_INSUFICIENTE_INFORMACION_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_INSUFICIENTE_INFORMACION_ESPECIALISTA_GPYR));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_INSUFICIENTE_INFORMACION_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr > td:nth-child(17)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_INSUFICIENTE_INFORMACION_ESPECIALISTA_GPYR));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_INSUFICIENTE_INFORMACION_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(17)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            case "IncorrectaAplicacionCuotas":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_INCORRECTA_APLICACION_CD_CUOTAS_ESPECIALISTA_GPYR));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_INCORRECTA_APLICACION_CD_CUOTAS_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_INCORRECTA_APLICACION_CD_CUOTAS_ESPECIALISTA_GPYR));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_INCORRECTA_APLICACION_CD_CUOTAS_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr > td:nth-child(17)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_INCORRECTA_APLICACION_CD_CUOTAS_ESPECIALISTA_GPYR));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_INCORRECTA_APLICACION_CD_CUOTAS_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(17)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA VALIDAR TRAMITE BANDEJA EN: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='grid']/giru-all-procedures/div[3]/ibk-grid-group")));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SUPERVISOR_GPYR));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr > td:nth-child(17)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SUPERVISOR_GPYR));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(17)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            case "MiEquipo":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr > td:nth-child(2)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr > td:nth-child(18)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(18)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            case "Asignar":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr > td:nth-child(2)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr > td:nth-child(18)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(18)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            case "Resolver":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr > td:nth-child(2)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr > td:nth-child(18)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(18)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA VALIDAR TRAMITE BANDEJA EN: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                }
                break;
            case "Seguros":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ESPECIALISTA_SEGUROS));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ESPECIALISTA_SEGUROS));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr > td:nth-child(17)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ESPECIALISTA_SEGUROS));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(17)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            case "CobrosIndebidos":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr > td:nth-child(17)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(17)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            case "ExoneracionCobros":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr > td:nth-child(17)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(17)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            case "ConsumosNoReconocidos":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr > td:nth-child(17)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(17)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            case "Monitorear":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr > td:nth-child(17)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(17)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA VALIDAR TRAMITE BANDEJA EN: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SUPERVISOR_SEGUROS));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SUPERVISOR_SEGUROS));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr > td:nth-child(17)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SUPERVISOR_SEGUROS));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(17)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            case "MiEquipo":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr > td:nth-child(2)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(18)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(18)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            case "Asignar":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr > td:nth-child(2)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(18)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(18)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            case "Resolver":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr > td:nth-child(2)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr > td:nth-child(18)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(18)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            case "Monitorear":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr > td:nth-child(2)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr > td:nth-child(18)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(18)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA VALIDAR TRAMITE BANDEJA EN: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                }
                break;
            case "Retenciones":
                break;
            case "Controversias":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr > td:nth-child(17)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(17)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            case "ConsumosNoReconocidos":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr > td:nth-child(17)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(17)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            case "ConsumosMalProcesados":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr > td:nth-child(17)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(17)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            case "Monitorear":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr > td:nth-child(17)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(17)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            case "TransaccionesEnProceso":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr > td:nth-child(17)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(17)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA VALIDAR TRAMITE BANDEJA EN: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr > td:nth-child(17)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(17)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            case "MiEquipo":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr > td:nth-child(2)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr > td:nth-child(18)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(18)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            case "Asignar":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr > td:nth-child(2)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr > td:nth-child(18)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(18)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            case "Resolver":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr > td:nth-child(2)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr > td:nth-child(18)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(18)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            case "Monitorear":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr > td:nth-child(2)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr > td:nth-child(18)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(18)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            case "TransaccionesEnProceso":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr > td:nth-child(17)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(17)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA VALIDAR TRAMITE BANDEJA EN: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                }
                break;
            case "USPR":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ESPECIALISTA_USPR));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ESPECIALISTA_USPR));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr > td:nth-child(17)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ESPECIALISTA_USPR));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(17)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            case "CobrosIndebidos":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr > td:nth-child(17)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(17)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            case "ExoneracionCobros":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr > td:nth-child(17)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(17)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            case "ConsumosMalProcesados":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr > td:nth-child(17)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(17)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            case "ConsumosNoReconocidos":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr > td:nth-child(17)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(17)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            case "TransaccionMalNoProcesada":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_TRANSACCION_MAL_NO_PROCESADA_ESPECIALISTA_USPR));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_TRANSACCION_MAL_NO_PROCESADA_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_TRANSACCION_MAL_NO_PROCESADA_ESPECIALISTA_USPR));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_TRANSACCION_MAL_NO_PROCESADA_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr > td:nth-child(17)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_TRANSACCION_MAL_NO_PROCESADA_ESPECIALISTA_USPR));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_TRANSACCION_MAL_NO_PROCESADA_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(17)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            case "DuplicadoEstadoCuentaSC":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_DUPLICADO_ESTADO_CUENTA_SC_ESPECIALISTA_USPR));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_DUPLICADO_ESTADO_CUENTA_SC_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_DUPLICADO_ESTADO_CUENTA_SC_ESPECIALISTA_USPR));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_DUPLICADO_ESTADO_CUENTA_SC_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr > td:nth-child(17)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_DUPLICADO_ESTADO_CUENTA_SC_ESPECIALISTA_USPR));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_DUPLICADO_ESTADO_CUENTA_SC_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(17)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            case "SistemaRecompensas":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SISTEMA_RECOMPENSAS_ESPECIALISTA_USPR));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_SISTEMA_RECOMPENSAS_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SISTEMA_RECOMPENSAS_ESPECIALISTA_USPR));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_SISTEMA_RECOMPENSAS_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr > td:nth-child(17)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SISTEMA_RECOMPENSAS_ESPECIALISTA_USPR));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_SISTEMA_RECOMPENSAS_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(17)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            case "DesacuerdoCondicionesTT":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_DESACUERDO_CONDICIONES_TT_ESPECIALISTA_USPR));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_DESACUERDO_CONDICIONES_TT_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_DESACUERDO_CONDICIONES_TT_ESPECIALISTA_USPR));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_DESACUERDO_CONDICIONES_TT_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr > td:nth-child(17)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_DESACUERDO_CONDICIONES_TT_ESPECIALISTA_USPR));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_DESACUERDO_CONDICIONES_TT_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(17)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            case "Retipificar":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr > td:nth-child(17)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(17)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA VALIDAR TRAMITE BANDEJA EN: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SUPERVISOR_USPR));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SUPERVISOR_USPR));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr > td:nth-child(17)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SUPERVISOR_USPR));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(17)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            case "MiEquipo":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr > td:nth-child(2)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr > td:nth-child(18)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(18)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            case "Asignar":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr > td:nth-child(2)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr > td:nth-child(18)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(18)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            case "Resolver":
                                if (tipoTramite.equals(nroTramite)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR));
                                    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr > td:nth-child(2)", "", "");
                                    String getTramite = valueTramite.getText();
                                    String sSubTramite = removerCeros(getTramite);
                                    System.out.println("Numero tramite Original: " + getTramite);
                                    System.out.println("Numero tramite Con Metodo: " + sSubTramite);
                                    Assert.assertEquals(sSubTramite, numeroTramite);
                                } else if (tipoTramite.equals(numeroDNI)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR));
                                    WebElement valueNumeroDNI = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr > td:nth-child(18)", "", "");
                                    String getNumeroDNI = valueNumeroDNI.getText();
                                    System.out.println("Numero DNI: " + getNumeroDNI);
                                    Assert.assertEquals(getNumeroDNI, numeroTramite);
                                } else if (tipoTramite.equals(numeroCE)) {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR));
                                    WebElement valueNumeroCE = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(1) > td:nth-child(18)", "", "");
                                    String getNumeroCE = valueNumeroCE.getText();
                                    System.out.println("Numero CE: " + getNumeroCE);
                                    Assert.assertEquals(getNumeroCE, numeroTramite);
                                }
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA VALIDAR TRAMITE BANDEJA EN: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                }
                break;
        }


    }


    public String removerCeros(String cadena) {
        cadena = cadena.replaceAll("^0+", "");
        return cadena;
    }


}
