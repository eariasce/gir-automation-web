package com.everis.giru.userinterface.tramites.filtrosPage;

import com.everis.giru.utils.Shadow;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class GetFiltroNivelServicioPage extends PageObject {

    Shadow general = new Shadow();

    @FindBy(xpath = "//*[@id='radio-list__radio-option']")
    WebElement LBL_ID_RADIOBUTTOM_NIVELSERVICIO;


    public void seleccionaFiltroNivelServicio(String fNivelServicio) {

        switch (fNivelServicio) {
            case "Platino":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_NIVELSERVICIO));
                WebElement btnPlatino = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_NIVELSERVICIO, "div > div:nth-child(1) > label", "", "", "");
                btnPlatino.click();
                break;
            case "Oro":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_NIVELSERVICIO));
                WebElement btnOro = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_NIVELSERVICIO, "div > div:nth-child(2) > label", "", "", "");
                btnOro.click();
                break;
            case "Plata":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_NIVELSERVICIO));
                WebElement btnPlata = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_NIVELSERVICIO, "div > div:nth-child(3) > label", "", "", "");
                btnPlata.click();
                break;
            case "Bronce":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_NIVELSERVICIO));
                WebElement btnBronce = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_NIVELSERVICIO, "div > div:nth-child(4) > label", "", "", "");
                btnBronce.click();
                break;
            case "Sin Especificar":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_NIVELSERVICIO));
                WebElement btnSinEspecificar = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_NIVELSERVICIO, "div > div:nth-child(5) > label", "", "", "");
                btnSinEspecificar.click();
                break;
            case "Premium":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_NIVELSERVICIO));
                WebElement btnPremium = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_NIVELSERVICIO, "div > div:nth-child(6) > label", "", "", "");
                btnPremium.click();
                break;

            default:
                System.out.println("No se encuentra ningún filtro Nivel Servicio !!!");
        }

    }

}
