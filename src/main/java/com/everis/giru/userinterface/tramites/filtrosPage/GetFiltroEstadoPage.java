package com.everis.giru.userinterface.tramites.filtrosPage;

import com.everis.giru.utils.Shadow;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class GetFiltroEstadoPage extends PageObject {

    Shadow general = new Shadow();

    @FindBy(xpath = "//*[@id='radio-list__radio-option']")
    WebElement LBL_ID_RADIOBUTTOM_ESTADO;

    public void seleccionaFiltroEstado(String fEstado) {

        switch (fEstado) {
            case "EN PROCESO":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESTADO));
                WebElement btnEnProceso = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESTADO, "div > div:nth-child(1) > label", "", "", "");
                btnEnProceso.click();
                break;
            case "PROCEDE VALIDACIÓN":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESTADO));
                WebElement btnProcedeValidacion = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESTADO, "div > div:nth-child(2) > label", "", "", "");
                btnProcedeValidacion.click();
                break;
            case "NO PROCEDE VALIDACIÓN":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESTADO));
                WebElement btnNoProcedeValidacion = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESTADO, "div > div:nth-child(3) > label", "", "", "");
                btnNoProcedeValidacion.click();
                break;

            default:
                System.out.println("No se encuentra Filtro Estado!!!");
        }

    }


}
