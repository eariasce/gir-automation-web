package com.everis.giru.userinterface.tramites.historial;

import com.everis.giru.userinterface.tramites.ValidarTramiteBandejaPage;
import com.everis.giru.utils.ApiCommons;
import com.everis.giru.utils.ExcelFileService;
import com.everis.giru.utils.Shadow;
import com.everis.giru.utils.WriteToFile;
import io.cucumber.java.eo.Se;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class ValidaNumeroTramiteHistorialPage extends PageObject {

    Shadow general = new Shadow();

    @FindBy(xpath = "//*[@id='claim-detail-tabs__tracking']")
    WebElement historialSeguimientoUser;

    @FindBy(xpath = "//ibk-select")
    WebElement tramiteCombo;

    @FindBy(xpath = "(//ibk-giru-historic-process)[1]")
    WebElement DetallesEnProceso;

    @FindBy(xpath = "//*[@id='claim-detail-tabs__tracking']")
    WebElement LBL_NOMBRE_AREA;
    @FindBy(xpath = "//ibk-inputgroup-text[@type='icon']")
    WebElement LBL_INPUT_BUSCAR_GESTION;

    @FindBy(xpath = "//ibk-inputgroup-text[@formcontrolname='document']")
    WebElement LBL_INPUT_BUSCAR_TRAMITE;

    @FindBy(xpath = "//ibk-grid-group")
    WebElement LBL_ID_TABLA_HISTORIAL;

    @FindBy(id = "claim-detail-tabs__tracking")
    WebElement lblArchivo;

    @FindBy(xpath = "//*[@id='claim-detail-tabs__tracking']")
    WebElement seccionSegumiento;

    @FindBy(id = "claim-detail-tabs__tab-detail")
    WebElement btnSeguimiento;
    public void clickBusquedaGestiones() {
     WebElement buscadorGestion = general.expandContainer("2css", LBL_INPUT_BUSCAR_GESTION, "div > ibk-input", "div > span", "", "");
    buscadorGestion.click();

       /* try{Thread.sleep(4000);}catch (Exception e){};
        //TODO: VALIDA HISTORIAL NUMERO DE TRAMITE EN USUARIO, SE COMENTA YA QUE NO MUESTRA ULTIMO TRAMITE
        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_HISTORIAL, "ibk-grid", "div:nth-child(1) > table:nth-child(1) > tbody:nth-child(3) > tr:nth-child(1) > td:nth-child(1)", "", "");
        WebElement documentoE = general.expandContainer("2css", LBL_ID_TABLA_HISTORIAL, "ibk-grid", "div:nth-child(1) > table:nth-child(1) > tbody:nth-child(3) > tr:nth-child(1) > td:nth-child(4)", "", "");
        WebElement nrodocumentoE = general.expandContainer("2css", LBL_ID_TABLA_HISTORIAL, "ibk-grid", "div:nth-child(1) > table:nth-child(1) > tbody:nth-child(3) > tr:nth-child(1) > td:nth-child(5)", "", "");
        WebElement nombreUsuarioE = general.expandContainer("2css", LBL_ID_TABLA_HISTORIAL, "ibk-grid", "div:nth-child(1) > table:nth-child(1) > tbody:nth-child(3) > tr:nth-child(1) > td:nth-child(8)", "", "");

        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("arguments[0].scrollIntoView();", valueTramite);
        String getTramite = valueTramite.getText();
        String document = documentoE.getText();
        String nroDocumento = nrodocumentoE.getText();
        String nombreUsuario = nombreUsuarioE.getText();
        String sSubTramite = removerCeros(getTramite);
        System.out.println("\n * VALIDA HISTORIAL NUMERO DE TRAMITE: " + "Numero tramite Original: " + getTramite + " Numero tramite Con Metodo: " + sSubTramite + " Estado:  \n***************************************");

        ApiCommons.NOMBRE_USUARIO = nombreUsuario;
        Assert.assertEquals(getTramite, ApiCommons.GESTIONES_NRO_TRAMITE);
        Assert.assertEquals(document, ApiCommons.GESTIONES_TIPO_DOC);
        Assert.assertEquals(nroDocumento, ApiCommons.clientData.get(0).getDocumentClient());*/

    }
    public void gestionesTramites() {
        try{Thread.sleep(4000);}catch (Exception e){};
        //TODO: VALIDA HISTORIAL NUMERO DE TRAMITE
        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_HISTORIAL, "ibk-grid", "div:nth-child(1) > table:nth-child(1) > tbody:nth-child(3) > tr:nth-child(1) > td:nth-child(1)", "", "");
        WebElement documentoE = general.expandContainer("2css", LBL_ID_TABLA_HISTORIAL, "ibk-grid", "div:nth-child(1) > table:nth-child(1) > tbody:nth-child(3) > tr:nth-child(1) > td:nth-child(4)", "", "");
        WebElement nrodocumentoE = general.expandContainer("2css", LBL_ID_TABLA_HISTORIAL, "ibk-grid", "div:nth-child(1) > table:nth-child(1) > tbody:nth-child(3) > tr:nth-child(1) > td:nth-child(5)", "", "");
        WebElement nombreUsuarioE = general.expandContainer("2css", LBL_ID_TABLA_HISTORIAL, "ibk-grid", "div:nth-child(1) > table:nth-child(1) > tbody:nth-child(3) > tr:nth-child(1) > td:nth-child(8)", "", "");

        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("arguments[0].scrollIntoView();", valueTramite);
        String getTramite = valueTramite.getText();
        String document = documentoE.getText();
        String nroDocumento = nrodocumentoE.getText();
        String nombreUsuario = nombreUsuarioE.getText();
        String sSubTramite = removerCeros(getTramite);
        System.out.println("\n * VALIDA HISTORIAL NUMERO DE TRAMITE: " + "Numero tramite Original: " + getTramite + " Numero tramite Con Metodo: " + sSubTramite + " Estado:  \n***************************************");

        ApiCommons.NOMBRE_USUARIO = nombreUsuario;
        Assert.assertEquals(getTramite, ApiCommons.GESTIONES_NRO_TRAMITE);
        Assert.assertEquals(document, ApiCommons.GESTIONES_TIPO_DOC);
        Assert.assertEquals(nroDocumento, ApiCommons.clientData.get(0).getDocumentClient());
    }
    public void validaSeguimientoTramiteDerivado(String comentario, String areaDerivada, String actividadDerivada) {

        try {
            Thread.sleep(3000);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        //TODO: VALIDACIÓN EN HISTORIAL - SEGUIMIENTO
        WebElement btnseguimiento = general.expandContainer("1css", btnSeguimiento, "ibk-tab:nth-child(2)", "", "", "");
        btnseguimiento.click();

        WebElement areaFront = general.expandContainer("1css", LBL_NOMBRE_AREA, "div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1)", "", "", "");

        //SE COMENTA PORQUE FALLA EN RETIPIFICACION
        /*if (areaFront.getText().toUpperCase(Locale.ROOT).contains(areaDerivada.toUpperCase(Locale.ROOT))) {
            System.out.println("VALIDA AREA DERIVADA ES: " + areaDerivada.toUpperCase(Locale.ROOT));
            System.out.println("HISTORIAL SEGUIMIENTO - VALIDA AREA ES: " + areaFront.getText().toUpperCase(Locale.ROOT));
        } else {
            throw new AssertionError("No coincide nombres de area");
        }*/

        //TODO: CLICK EN TRAMITE FLUJOS
        tramiteCombo.click();

        //TODO: SELECCIONAR TRAMITE FLUJO INICIAL
        WebElement selectTramiteFlujoInicial = general.expandContainer("1css", tramiteCombo, "div:nth-child(1) > ul:nth-child(2) > li:nth-child(2)", "", "", "");
        selectTramiteFlujoInicial.click();

        try {
            Thread.sleep(3000);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        WebElement lblActividadDer = general.expandContainer("2css", DetallesEnProceso, "div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(2) > ibk-giru-text-info:nth-child(1)", ".collapsible-text.notranslate", "", "");
        WebElement lblComentario = general.expandContainer("2css", DetallesEnProceso, "div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(3) > ibk-giru-text-info:nth-child(1)", ".collapsible-text.notranslate", "", "");

        String nuevoActividad = "";
        switch (actividadDerivada.trim()) {
            case "Requiere ser enviado a nuevo flujo":
                nuevoActividad = "Derivación a nuevo flujo";
                break;
            case "Caso 2":
                nuevoActividad = "Texto caso 2";
                break;
        }

        Assert.assertEquals(nuevoActividad, lblActividadDer.getText());
        System.out.println("VALIDA ACTIVIDAD DERIVACION A NUEVO FLUJO: " + lblActividadDer.getText());
        Assert.assertEquals(comentario, lblComentario.getText());
        System.out.println("VALIDA TEXTO COMENTARIO: " + lblComentario.getText());
    }


    public void validaNumeroTramiteHistorialVerEstados(String numeroTramite) {
        try {
            String ambiente = Serenity.sessionVariableCalled("ambiente");


            WebElement valueClaimNumber = general.expandContainer("2css", LBL_INPUT_BUSCAR_TRAMITE, "ibk-input", "div > input", "", "");

            valueClaimNumber.clear();
            valueClaimNumber.sendKeys(numeroTramite);


            try {
                Thread.sleep(3000);
            } catch (Exception e) {
                System.out.println(e);
            }

            valueClaimNumber.sendKeys(Keys.ENTER);


            try {
                Thread.sleep(1000);
            } catch (Exception e) {
                System.out.println(e);
            }

            //TODO: VALIDA HISTORIAL NUMERO DE TRAMITE
            WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_HISTORIAL, "ibk-grid", "div:nth-child(1) > table:nth-child(1) > tbody:nth-child(3) > tr:nth-child(1) > td:nth-child(1)", "", "");
            WebElement estadojs = general.expandContainer("2css", LBL_ID_TABLA_HISTORIAL, "ibk-grid", "div:nth-child(1) > table:nth-child(1) > tbody:nth-child(3) > tr:nth-child(1) > td:nth-child(7)", "", "");

            JavascriptExecutor js = (JavascriptExecutor) getDriver();
            js.executeScript("arguments[0].scrollIntoView();", valueTramite);
            String getTramite = valueTramite.getText();
            String estado = estadojs.getText();
            String sSubTramite = removerCeros(getTramite);
            System.out.println("\n * VALIDA HISTORIAL NUMERO DE TRAMITE: " + "Numero tramite Original: " + getTramite + " Numero tramite Con Metodo: " + sSubTramite + " Estado: " + estado + " \n***************************************");


            WriteToFile.writeLinesToFile(estado + "," + sSubTramite);
            try {
                Thread.sleep(1000);
            } catch (Exception e) {
                System.out.println(e);
            }
        }catch (Exception e){}
    }


    public void validaNumeroTramiteHistorial(String numeroTramite) {

        String ambiente = Serenity.sessionVariableCalled("ambiente");
        String perfil = ApiCommons.PERFIL_USUARIO;

        if (perfil.equals("ASESOR")) {
            WebElement valueClaimNumberAsesor = Serenity.getDriver().findElement(By.xpath("//input[@id='search-claim-form__txt-document-number']"));
            valueClaimNumberAsesor.clear();
            valueClaimNumberAsesor.sendKeys(numeroTramite);

            if (ambiente.equals("UAT")) {
                try {
                    Thread.sleep(8000);
                } catch (Exception e) {
                    System.out.println(e);
                }
            } else if (ambiente.equals("STG")) {
                try {
                    Thread.sleep(5000);
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
            valueClaimNumberAsesor.sendKeys(Keys.ENTER);
        } else {
            WebElement valueClaimNumber = general.expandContainer("2css", LBL_INPUT_BUSCAR_TRAMITE, "ibk-input", "div > input", "", "");
            valueClaimNumber.clear();
            valueClaimNumber.sendKeys(numeroTramite);

            if (ambiente.equals("UAT")) {
                try {
                    Thread.sleep(8000);
                } catch (Exception e) {
                    System.out.println(e);
                }
            } else if (ambiente.equals("STG")) {
                try {
                    Thread.sleep(5000);
                } catch (Exception e) {
                    System.out.println(e);
                }
            }

            valueClaimNumber.sendKeys(Keys.ENTER);
        }

        try {
            Thread.sleep(3000);
        } catch (Exception e) {
            System.out.println(e);
        }

        //TODO: VALIDA HISTORIAL NUMERO DE TRAMITE
        WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_HISTORIAL, "ibk-grid", "div:nth-child(1) > table:nth-child(1) > tbody:nth-child(3) > tr:nth-child(1) > td:nth-child(1)", "", "");

        //SI ES DE Comercial, tomar el estado de la 8va columna
        String esComercial = Serenity.sessionVariableCalled("COMERCIAL").toString();
        WebElement estadojs;
        if(esComercial.equals("COMERCIAL")) {
             estadojs = general.expandContainer("2css", LBL_ID_TABLA_HISTORIAL, "ibk-grid", "div:nth-child(1) > table:nth-child(1) > tbody:nth-child(3) > tr:nth-child(1) > td:nth-child(8)", "", "");
        }else {
             estadojs = general.expandContainer("2css", LBL_ID_TABLA_HISTORIAL, "ibk-grid", "div:nth-child(1) > table:nth-child(1) > tbody:nth-child(3) > tr:nth-child(1) > td:nth-child(7)", "", "");
        }

        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("arguments[0].scrollIntoView();", valueTramite);
        String getTramite = valueTramite.getText();
        String estado = estadojs.getText();
        String sSubTramite = removerCeros(getTramite);
        System.out.println("\n * VALIDA HISTORIAL NUMERO DE TRAMITE: " + "Numero tramite Original: " + getTramite + " Numero tramite Con Metodo: " + sSubTramite + " Estado: "+estado+" \n***************************************");
        Assert.assertEquals(sSubTramite, numeroTramite);

      Serenity.setSessionVariable("finalcsv").to(estado+ ","+sSubTramite);
    try {
            Thread.sleep(1000);
        } catch (Exception e) {
            System.out.println(e);
        }

        //TODO: VALIDO AUTONOMIA - ACTIVIDAD EN RESOLUCION AUTOMATICA
        String autonomia = "";
        try {
            autonomia = Serenity.sessionVariableCalled("autonomia").toString();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        if (autonomia.equals("si")) {
            WebElement valueActividadFront = general.expandContainer("2css", LBL_ID_TABLA_HISTORIAL, "ibk-grid", "div:nth-child(1) > table:nth-child(1) > tbody:nth-child(3) > tr:nth-child(1) > td:nth-child(8)", "", "");
            String getActividadFront = valueActividadFront.getText();
            if (getActividadFront.equals("Resolución Automática")) {
                Assert.assertEquals(getActividadFront, "Resolución Automática");
            } else {
                throw new AssertionError("No se validó correctamente en historial texto resolución automatica, se obtuvo: " + getActividadFront);
            }
        }

        valueTramite.click();



    }
public String validoDiasSinFinde(String diaVencimiento, String diaProrroga) throws ParseException {
    DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    Date date1 = df.parse(diaVencimiento);
    Date date2 = df.parse(diaProrroga);
    Calendar cal1 = Calendar.getInstance();
    Calendar cal2 = Calendar.getInstance();
    cal1.setTime(date1);
    cal2.setTime(date2);

    int numberOfDays = 0;
    while (cal1.before(cal2)) {
        if ((Calendar.SATURDAY != cal1.get(Calendar.DAY_OF_WEEK))
                &&(Calendar.SUNDAY != cal1.get(Calendar.DAY_OF_WEEK))) {
            numberOfDays++;
        }
        cal1.add(Calendar.DATE,1);
    }
    System.out.println(numberOfDays);

    return String.valueOf(numberOfDays);
}
    public void validoProrroga(String tramite, String dias) throws ParseException, IOException {
        //TODO: VALIDO PRORROGA
            String prorroga = dias;

            String FechaVencimiento = "";
            String FechaProrroga = "";
        Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
        String diasDiferencia = "0";
            try {
                try {
                    FechaVencimiento = Serenity.getDriver().findElement(By.xpath("//label[contains(text(), 'Fecha de vencimiento:')]/../label[2]")).getText();
                }catch (Exception e)
                {
                    System.out.println(e.getMessage() + "se realizara con otro xpath");
                    FechaVencimiento = Serenity.getDriver().findElement(By.xpath("//label[contains(text(), 'Fecha de vencimiento:')]/../span")).getText();
                }
                FechaProrroga = Serenity.getDriver().findElement(By.xpath("//label[contains(text(), 'Fecha de prórroga:')]/../label[2]")).getText();
                SimpleDateFormat sdformat = new SimpleDateFormat("dd/MM/yyyy");

                Date FechaVencimientoDate = sdformat.parse(FechaVencimiento);
                Date FechaProrrogaDate = sdformat.parse(FechaProrroga);

                diasDiferencia = validoDiasSinFinde(FechaVencimiento,FechaProrroga);
            }catch (Exception e)
            {
                System.out.println(e.getMessage());

            }


     /*   System.out.println("prorroga: " + FechaProrrogaDate.getTime() + " vencimiento: "+ FechaVencimientoDate.getTime());
            long diff = FechaProrrogaDate.getTime() - FechaVencimientoDate.getTime();
            TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
            String diffstr = Long.toString(diff);
            int diasreal = (Integer.parseInt(diffstr))/86400000;
            String diasRealS = String.valueOf(diasreal);*/

            if(prorroga.contains(diasDiferencia))
            {
                System.out.println("comparacion de dias correcto: Se obtuvo "+diasDiferencia + " y se ingresaron "+ prorroga + " dias de prorroga");
                ExcelFileService.eliminoTramiteValidado(tramite);
                WriteToFile.writeLinesToFileProrrogaValidados(tramite + "," + dias + "," +"validado pendiente carta" );

            }
            else
            {
               // throw new AssertionError("Hubo error en validar los dias de prorroga: Se obtuvo "+diffstr + " y se ingresaron "+ prorroga + " dias de prorroga");
                System.out.println("Hubo error en validar los dias de prorroga: Se obtuvo "+diasDiferencia + " y se ingresaron "+ prorroga + " dias de prorroga");

            }

        Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(20));
    }
public void validoEnvioCartaHistorial(String numeroTramite,String fechaRecepcion, String estadoEnvio, String comentario) throws IOException {
    String ambiente = Serenity.sessionVariableCalled("ambiente");
    String perfil = ApiCommons.PERFIL_USUARIO;

    if (perfil.equals("ASESOR")) {
        WebElement valueClaimNumberAsesor = Serenity.getDriver().findElement(By.xpath("//input[@id='search-claim-form__txt-document-number']"));
        valueClaimNumberAsesor.clear();
        valueClaimNumberAsesor.sendKeys(numeroTramite);

        if (ambiente.equals("UAT")) {
            try {
                Thread.sleep(8000);
            } catch (Exception e) {
                System.out.println(e);
            }
        } else if (ambiente.equals("STG")) {
            try {
                Thread.sleep(5000);
            } catch (Exception e) {
                System.out.println(e);
            }
        }
        valueClaimNumberAsesor.sendKeys(Keys.ENTER);
    } else {
        WebElement valueClaimNumber = general.expandContainer("2css", LBL_INPUT_BUSCAR_TRAMITE, "ibk-input", "div > input", "", "");
        valueClaimNumber.clear();
        valueClaimNumber.sendKeys(numeroTramite);

        if (ambiente.equals("UAT")) {
            try {
                Thread.sleep(8000);
            } catch (Exception e) {
                System.out.println(e);
            }
        } else if (ambiente.equals("STG")) {
            try {
                Thread.sleep(5000);
            } catch (Exception e) {
                System.out.println(e);
            }
        }

        valueClaimNumber.sendKeys(Keys.ENTER);
    }

    try {
        Thread.sleep(3000);
    } catch (Exception e) {
        System.out.println(e);
    }

    //TODO: VALIDA HISTORIAL NUMERO DE TRAMITE
    WebElement valueTramite = general.expandContainer("2css", LBL_ID_TABLA_HISTORIAL, "ibk-grid", "div:nth-child(1) > table:nth-child(1) > tbody:nth-child(3) > tr:nth-child(1) > td:nth-child(1)", "", "");

    valueTramite.click();
    try {
        Thread.sleep(2000);
    } catch (Exception e) {
        System.out.println(e);
    }
    //INGRESO SEGUIMIENTO PARA VALIDAR CARTA
    WebElement btnseguimiento = general.expandContainer("1css", btnSeguimiento, "ibk-tab:nth-child(2)", "", "", "");
    waitForCondition().until(ExpectedConditions.visibilityOf(btnseguimiento));
    btnseguimiento.click();

    if( Serenity.getDriver().findElements(By.xpath("//ibk-container[@collapsable='true']")).size()>0) {
        Serenity.getDriver().findElement(By.xpath("//ibk-container[@collapsable='true']")).click();


        String FechaEnvio = Serenity.getDriver().findElement(By.xpath("//label[text()='Fecha de recepción de carta:']/../span")).getText();

        String EstadoCarta = Serenity.getDriver().findElement(By.xpath("//label[text()='Estado de carta:']/../span")).getText();

        String Comentario = Serenity.getDriver().findElement(By.xpath("//label[text()='Comentario']/../span")).getText();


        if (fechaRecepcion.contains(FechaEnvio)) {
            System.out.println("comparacion de texto correcto: Se obtuvo " + FechaEnvio + " y se ingreso en excel " + fechaRecepcion + " en fecha de recepcion");
        }
        if (estadoEnvio.contains(EstadoCarta)) {
            System.out.println("comparacion de texto correcto: Se obtuvo " + EstadoCarta + " y se ingreso en excel " + estadoEnvio + " en estado de carta");
        }
        if (comentario.contains(Comentario)) {
            System.out.println("comparacion de texto correcto: Se obtuvo " + Comentario + " y se ingreso en excel " + comentario + " en comentario de carta");

            ExcelFileService.eliminoTramiteValidadoCartas(numeroTramite);
            WriteToFile.writeLinesToFileCartaValidados(numeroTramite + "," + fechaRecepcion + "," + estadoEnvio + "," + comentario + "," + "validado completo con carta");

        }
    }
}
    public String removerCeros(String cadena) {
        cadena = cadena.replaceAll("^0+", "");
        return cadena;
    }
    public void validoProrrogaHistorial(String numeroTramite){

        String ambiente = Serenity.sessionVariableCalled("ambiente");
        String perfil = ApiCommons.PERFIL_USUARIO;

        if (perfil.equals("ASESOR")) {
            WebElement valueClaimNumberAsesor = Serenity.getDriver().findElement(By.xpath("//input[@id='search-claim-form__txt-document-number']"));
            valueClaimNumberAsesor.clear();
            valueClaimNumberAsesor.sendKeys(numeroTramite);

            if (ambiente.equals("UAT")) {
                try {
                    Thread.sleep(8000);
                } catch (Exception e) {
                    System.out.println(e);
                }
            } else if (ambiente.equals("STG")) {
                try {
                    Thread.sleep(5000);
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
            valueClaimNumberAsesor.sendKeys(Keys.ENTER);
        } else {
            WebElement valueClaimNumber = general.expandContainer("2css", LBL_INPUT_BUSCAR_TRAMITE, "ibk-input", "div > input", "", "");
            valueClaimNumber.clear();
            valueClaimNumber.sendKeys(numeroTramite);

            if (ambiente.equals("UAT")) {
                try {
                    Thread.sleep(8000);
                } catch (Exception e) {
                    System.out.println(e);
                }
            } else if (ambiente.equals("STG")) {
                try {
                    Thread.sleep(5000);
                } catch (Exception e) {
                    System.out.println(e);
                }
            }

            valueClaimNumber.sendKeys(Keys.ENTER);
        }

    }
    public void validaDatosResumenHistorial() {
        String ambiente = Serenity.sessionVariableCalled("ambiente");
        String tipoDocumento = ApiCommons.clientes.getTipodocumento();
        String numeroDocumento = ApiCommons.clientes.getNumerodocumento();

        String tipoProducto = Serenity.sessionVariableCalled("tipoproducto");
        String tipoTramite = Serenity.sessionVariableCalled("tipotramite");
        String tipologia = Serenity.sessionVariableCalled("tipologia");
        String numeroTramite = Serenity.sessionVariableCalled("numeroTramite").toString();
        String nrotarjeta = ApiCommons.clientes.getNumerotarjeta();

        String subirarchivo = Serenity.sessionVariableCalled("subirarchivo");
        String esComercial = Serenity.sessionVariableCalled("COMERCIAL").toString();

        if (esComercial.equals("COMERCIAL")) {
            if(tipoProducto.equals("TARJETA DE CRÉDITO")||tipoProducto.equals("TARJETA DE DÉBITO")) {
                if (tipologia.equals("CONSUMOS NO RECONOCIDOS POR FRAUDE")) {
                    tipologia = "CONSUMOS NO RECONOCIDOS POR FRAUDE [C]";
                } else if (tipologia.equals("CONSUMOS MAL PROCESADOS POR EL COMERCIO/MARCA")) {
                    tipologia = "CONSUMOS MAL PROCESADOS POR EL COMERCIO/MARCA [C]";
                }
            }
            else
            if(tipoProducto.equals("SEGUROS RELACIONADOS")||tipoProducto.equals("SEGUROS")){
                if(tipologia.equals("COBROS INDEBIDOS")){
                    tipologia="COBROS INDEBIDOS[C]";
                }
                else if(tipologia.equals("DESAFILIACIÓN DE SEGURO")){
                    tipologia="DESAFILIACIÓN DE SEGURO [C]";
                }
                else if(tipologia.equals("DUPLICADO DE DOCUMENTO")){
                    tipologia="DUPLICADO DE DOCUMENTO [C]";
                }
            }


        }



        if (tipoDocumento.equals("PASAPORTE") || tipoDocumento.equals("PTP") || tipoDocumento.equals("DNI") || tipoDocumento.equals("CE")) {
            String tipoDocumentoFront = Serenity.getDriver().findElement(By.cssSelector("div[class='flex middle-xs mr-28p'] label[class='parrafo h-bold']")).getText();
            String valueDocumentType = tipoDocumentoFront.replace(":", "");

            String numeroDocumentoFront = Serenity.getDriver().findElement(By.xpath("(//span[@class='parrafo_todter h-regular'])[1]")).getText();
            String valueDocumentNumber = numeroDocumentoFront.trim();

            Assert.assertEquals(tipoDocumento, valueDocumentType);
            Assert.assertEquals(numeroDocumento, valueDocumentNumber);
        }

        //TODO: VALIDA EN RESUMEN - TIPO TRAMITE Y TIPOLOGIA
        Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        boolean labelTipoTramite = Serenity.getDriver().findElements(By.xpath("//label[text()='Tipo de trámite:']/following-sibling::label")).size() > 0;
        if (labelTipoTramite) {
            System.err.println("VALIDA EN RESUMEN - TIPO TRAMITE Y TIPOLOGIA - ENTRA LABEL");
            String tipoTramiteLabel = Serenity.getDriver().findElement(By.xpath("//label[text()='Tipo de trámite:']/following-sibling::label")).getText();
            String tipologiaLabel = Serenity.getDriver().findElement(By.xpath("//label[text()='Tipología:']/following-sibling::label")).getText();

//            String numeroTramiteLabel = "";
//            if (tipoProducto.equals("PAGOS PLANILLAS/PROVEEDORES") && tipologia.equals("COBROS INDEBIDOS")) {
//                numeroTramiteLabel = Serenity.getDriver().findElement(By.xpath("//label[text()='N° de trámite:']/following-sibling::label")).getText();
//            } else if (tipoProducto.equals("SUNAT") && tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA")) {
//                numeroTramiteLabel = Serenity.getDriver().findElement(By.xpath("//label[text()='N° de trámite:']/following-sibling::label")).getText();
//            } else if (tipoProducto.equals("EFECTIVO CON GARANTÍA HIPOTECARIO") || tipoProducto.equals("HIPOTECARIO") ||
//                    tipoProducto.equals("REFINANCIADO") || tipoProducto.equals("VEHICULAR")) {
//                numeroTramiteLabel = Serenity.getDriver().findElement(By.xpath("//label[text()='N° de trámite:']/following-sibling::label")).getText();
//            } else {
//                numeroTramiteLabel = Serenity.getDriver().findElement(By.xpath("//label[text()='Número de trámite:']/following-sibling::label")).getText();
//            }

            Assert.assertEquals(tipoTramite.toUpperCase(new Locale("es", "Peru")), tipoTramiteLabel.toUpperCase(new Locale("es", "Peru")));
            Assert.assertEquals(tipologia.toUpperCase(new Locale("es", "Peru")), tipologiaLabel.toUpperCase(new Locale("es", "Peru")));
            //Assert.assertEquals(numeroTramite, numeroTramiteLabel);
        } else {
            System.err.println("VALIDA EN RESUMEN - TIPO TRAMITE Y TIPOLOGIA - ENTRA SPAN");
            String tipoTramiteSpan = Serenity.getDriver().findElement(By.xpath("//label[text()='Tipo de trámite:']/following-sibling::span")).getText();
            String tipologiaSpan = Serenity.getDriver().findElement(By.xpath("//label[text()='Tipología:']/following-sibling::span")).getText();

//            String numeroTramiteSpan = "";
//            if (tipoProducto.equals("PAGOS PLANILLAS/PROVEEDORES") && tipologia.equals("COBROS INDEBIDOS")) {
//                numeroTramiteSpan = Serenity.getDriver().findElement(By.xpath("//label[text()='N° de trámite:']/following-sibling::span")).getText();
//            } else if (tipoProducto.equals("SUNAT") && tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA")) {
//                numeroTramiteSpan = Serenity.getDriver().findElement(By.xpath("//label[text()='N° de trámite:']/following-sibling::span")).getText();
//            } else if (tipoProducto.equals("EFECTIVO CON GARANTÍA HIPOTECARIO") || tipoProducto.equals("HIPOTECARIO") ||
//                    tipoProducto.equals("REFINANCIADO") || tipoProducto.equals("VEHICULAR")) {
//                numeroTramiteSpan = Serenity.getDriver().findElement(By.xpath("//label[text()='N° de trámite:']/following-sibling::span")).getText();
//            } else {
//                numeroTramiteSpan = Serenity.getDriver().findElement(By.xpath("//label[text()='Número de trámite:']/following-sibling::span")).getText();
//            }

            Assert.assertEquals(tipoTramite.toUpperCase(new Locale("es", "Peru")), tipoTramiteSpan.toUpperCase(new Locale("es", "Peru")));
            Assert.assertEquals(tipologia.toUpperCase(new Locale("es", "Peru")), tipologiaSpan.toUpperCase(new Locale("es", "Peru")));
            //Assert.assertEquals(numeroTramite, numeroTramiteSpan);
        }

        //TODO: VALIDA EN HISTORIAL - NUMERO TARJETA
        if (tipoProducto.equals("TARJETA DE CREDITO") || tipoProducto.equals("TARJETA DE DEBITO")) {

            Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(3));
            boolean labelNumeroTarjeta = Serenity.getDriver().findElements(By.xpath("//label[text()='Número de tarjeta']/following-sibling::span")).size() > 0;

            if (labelNumeroTarjeta) {
                System.err.println("VALIDA EN RESUMEN - NUMERO TARJETA - ENTRA SPAN");
                String numeroTarjeta = Serenity.getDriver().findElement(By.xpath("//label[text()='Número de tarjeta']/following-sibling::span")).getText();
                Assert.assertEquals(nrotarjeta.replace(" ", ""), numeroTarjeta.replace(" ", ""));

            } else {
                System.err.println("VALIDA EN RESUMEN - NUMERO TARJETA - ENTRA LABEL");
                String numeroTarjeta = Serenity.getDriver().findElement(By.xpath("//label[text()='Número de tarjeta:']/following-sibling::label")).getText();
                Assert.assertEquals(nrotarjeta.replace(" ", ""), numeroTarjeta.replace(" ", ""));
            }
        }

        //TODO: BUTTON SEGUIMIENTO
        WebElement btnseguimiento = general.expandContainer("1css", btnSeguimiento, "ibk-tab:nth-child(2)", "", "", "");
        waitForCondition().until(ExpectedConditions.visibilityOf(btnseguimiento));
        btnseguimiento.click();

        if (subirarchivo.equals("Si")) {
            //Validar archivo
            Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
            WebElement actividadArchivoSeguimientoHistorial = general.expandContainer("2css", lblArchivo, "ibk-giru-link-icon", "div > div.link > a", "", "");

            if (actividadArchivoSeguimientoHistorial.getText().contains("pdf") || actividadArchivoSeguimientoHistorial.getText().contains("txt")) {
                System.out.println("Se encuentra el archivo");
            } else {
                throw new AssertionError("No se encuentra archivo");
            }

        }

        //TODO: COMERCIAL-VALIDO ESPECIALISTA ASIGNADO Y WORKGROUP EN EL REGISTRO
        if (esComercial.equals("COMERCIAL")) {

            String textoUsuario;

            WebElement valueUser = general.expandContainer("1css", historialSeguimientoUser, "#stage_en-proceso > div.detail-process-container > div > div.body-detail-process > div > div.title-subdetail-process", "", "", "");
            String labelEspecialista= valueUser.getText();
            WebElement valueAreaWork = general.expandContainer("1css", historialSeguimientoUser, "#stage_en-proceso > div.detail-process-container > div > div.header-detail-process > div.title-detail-section", "", "", "");
            String labelWorkgroup=(valueAreaWork.getText()).replace("\n", "");
            //String labelWorkgroup=("Controversias -<br>" +"Canal digital (app/bpi) y otros").replace("<br>", "");

            try{
                String[] fragmentos = labelWorkgroup.split("-");
                labelWorkgroup=fragmentos[1].trim();
            }catch (Exception e){
                System.out.println("No se encontro Workgroup");
            }



            //tring labelEspecialista = Serenity.getDriver().findElement(By.xpath("(//div[@class='title-subdetail-process'])[last()]")).getText();
            String motivos = Serenity.sessionVariableCalled("motivos").toString();
            String[] arrMotivos = motivos.split("-");
            boolean encontrado = false;


            if (tipoProducto.equals("TARJETA DE DÉBITO")) {
                if (tipologia.equals("CONSUMOS NO RECONOCIDOS POR FRAUDE [C]")) {
                    String valorBuscado1="3";// Si uno de los motivos seleccionados es Canales Digitales
                    String valorBuscado2="2";// Si uno de los motivos seleccionados es Fraude por medios fisicos
                    String motivoEncontrado=buscarValor(arrMotivos, valorBuscado1, valorBuscado2);


                    if(motivoEncontrado.equals("3")){
                        //Canal digital (app/bpi) y otros
                        Assert.assertEquals("Workgroup coincide", "Canal digital (app/bpi) y otros", labelWorkgroup);
                        String[] arrEspecialistas_CanalDigitales= {"Kiara Sabrina Dangelo Gamarra","Milagros Joanna Ibañez Saji","Jose Alejandro Ganoza Lopéz","Andrea Soley Garcia Zambrano","Vania Fiorella Yenque Acuña","Yessica Chavez Juárez","Allison Nicoll Cambar Peceros"};
                        buscaValidaEspecialista(arrEspecialistas_CanalDigitales,labelEspecialista);
                    }else if(motivoEncontrado.equals("2")){
                        //Robo, perdida y operativos
                        Assert.assertEquals("Workgroup coincide", "Robo, perdida y operativos", labelWorkgroup);
                        String[] arrEspecialistas_Robo_Perdida= {"Nelly Melitza Chuquisuta Collao","Luis Eduardo Baca Poma"};
                        buscaValidaEspecialista(arrEspecialistas_Robo_Perdida,labelEspecialista);
                    }else{
                        //Fraude virtual (ecommerce)
                        Assert.assertEquals("Workgroup coincide", "Fraude virtual (ecommerce)", labelWorkgroup);
                        String[] arrEspecialistas_Fraude= {"Karla Mercedes Isla Arbulu","Emanuel Felix Panduro Paredes","Cristian Alfredo Pichihua Vallejos"};
                        buscaValidaEspecialista(arrEspecialistas_Fraude,labelEspecialista);
                    }

                } else if (tipologia.equals("CONSUMOS MAL PROCESADOS POR EL COMERCIO/MARCA [C]")) {
                    //Robo, perdida y operativos
                    Assert.assertEquals("Workgroup coincide", "Robo, perdida y operativos", labelWorkgroup);
                    String[] arrEspecialistas_Robo_Perdida= {"Heyden Jesus Romero Chocano"};
                    buscaValidaEspecialista(arrEspecialistas_Robo_Perdida,labelEspecialista);

                }else if (tipologia.equals("CONSUMO/ RETIRO NO EFECTUADO [C]")) {
                    //Canales Digitales
                    Assert.assertEquals("Workgroup coincide", "Canal digital (app/bpi) y otros", labelWorkgroup);
                    String[] arrEspecialistas_CanalDigitales= {"Kiara Sabrina Dangelo Gamarra","Milagros Joanna Ibañez Saji","Jose Alejandro Ganoza Lopéz","Andrea Soley Garcia Zambrano","Vania Fiorella Yenque Acuña","Yessica Chavez Juárez","Allison Nicoll Cambar Peceros"};
                    buscaValidaEspecialista(arrEspecialistas_CanalDigitales,labelEspecialista);
                }

            }
            if (tipoProducto.equals("TARJETA DE CRÉDITO")) {
                if (tipologia.equals("CONSUMOS NO RECONOCIDOS POR FRAUDE [C]")) {
                    String valorBuscado1="2";// Si uno de los motivos seleccionados es Fraude por medios fisicos
                    String valorBuscado2="1";// Si uno de los motivos seleccionados es Fraude por medios fisicos
                    String motivoEncontrado=buscarValor(arrMotivos, valorBuscado1, valorBuscado2);
                    if(motivoEncontrado.equals("2")){
                        //Robo, perdida y operativos
                        Assert.assertEquals("Workgroup coincide", "Robo, perdida y operativos", labelWorkgroup);
                        String[] arrEspecialistas_Robo_Perdida= {"Allison Amalia Silva Gonzales","Juan Miguel Llaja Fernandez","Ana Lisset Cotrina Melendez"};
                        buscaValidaEspecialista(arrEspecialistas_Robo_Perdida,labelEspecialista);
                    }else{
                        //Fraude virtual (ecommerce)
                        Assert.assertEquals("Workgroup coincide", "Fraude virtual (ecommerce)", labelWorkgroup);
                        String[] arrEspecialistas_Fraude= {"Ivo Gastulo Sanchez","Carlos Enrique Llanto Cueto","Gabriela Del Pilar Gastelu Montoya","Lorena Estefany Cardenas Romero","Daniela Julia Solano Vilchez"};
                        buscaValidaEspecialista(arrEspecialistas_Fraude,labelEspecialista);
                    }


                }else if (tipologia.equals("CONSUMOS MAL PROCESADOS POR EL COMERCIO/MARCA [C]")) {
                    //Robo, perdida y operativos
                    Assert.assertEquals("Workgroup coincide", "Robo, perdida y operativos", labelWorkgroup);
                    String[] arrEspecialistas_Robo_Perdida= {"Elizabeth Consuelo Gallardo Palomino","Anabel Cordova Bacilio","Roberto Carlos Flores Martinez"};
                    buscaValidaEspecialista(arrEspecialistas_Robo_Perdida,labelEspecialista);
                }
            }



        }

        //TODO: VALIDO AUTONOMIA EN SEGUIMIENTO
        String autonomia = "";
        try {
            autonomia = Serenity.sessionVariableCalled("autonomia").toString();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        if (autonomia.equals("si")) {
            Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(2));
            WebElement actividadTipoResolucion = general.expandContainer("2css", seccionSegumiento, "#stage_procede > div.detail-process-container > div > div.body-detail-process > div > div.container-subdetail-process > div.content-subdetail-process.detail_actividad.false > ibk-giru-text-info", "div > div.name-value > div", "", "");

            if (actividadTipoResolucion.getText().contains("Resolución Automática")) {
                System.out.println("validado en seguimiento Resolución Automática");
            } else {
                throw new AssertionError("No se validó el texto de resolución automatica, se obtuvo: " + actividadTipoResolucion.getText());
            }
        }

    }

    // Función para buscar un valor en un array y devolver otro si no se encuentra
    public static String buscarValor(String[] array, String valorBuscado1, String valorBuscado2) {

        if(array.length>1)
        {
        // Recorrer el array y buscar el valor 1
        for (String elemento : array) {
            if (elemento.equals(valorBuscado1)) {
                return valorBuscado1;  // Devolver el valor 1 si se encuentra
            }
        }
        // Si no se encuentra el valor 1, devolver el valor 2
        for (String elemento : array) {
            if (elemento.equals(valorBuscado2)) {
                return valorBuscado2;  // Devolver el valor 2 si se encuentra
            }
        }
    }else
        if(array.length==1){
            if (array[0].equals(valorBuscado1)) {
                return valorBuscado1;  // Devolver el valor 1 si se encuentra
            }
            if (array[0].equals(valorBuscado2)) {
                return valorBuscado2;  // Devolver el valor 2 si se encuentra
            }
            // Si ninguno de los valores se encuentra, devolver un mensaje o null
            return "Ninguno de los valores encontrados";
        }

        // Si ninguno de los valores se encuentra, devolver un mensaje o null
        return "Ninguno de los valores encontrados";
    }

    // Función para buscar un especialista en el grupo t1
    public static void buscaValidaEspecialista(String[] array, String especialistaBuscado) {
        // Buscar el valor en el array
        boolean encontrado = false;
        for (String elemento : array) {
            if (elemento.equals(especialistaBuscado)) {
                encontrado = true;
                break;
            }
        }
        // Ejecutar un assert utilizando Serenity
        Assert.assertTrue("Especialista encontrado: " + especialistaBuscado, encontrado);
        // También puedes utilizar Serenity para registrar información en el informe
        Serenity.recordReportData().withTitle("Resultado de la búsqueda").andContents("Especialista encontrado: " + especialistaBuscado);
    }
    public void validaDatosResumenBandeja() {
        String ambiente = Serenity.sessionVariableCalled("ambiente");
        String tipoDocumento = ApiCommons.clientes.getTipodocumento();
        String numeroDocumento = ApiCommons.clientes.getNumerodocumento();

        String tipoProducto = Serenity.sessionVariableCalled("tipoproducto");
        String tipoTramite = Serenity.sessionVariableCalled("tipotramite");
        String tipologia = Serenity.sessionVariableCalled("tipologia");
        String numeroTramite = Serenity.sessionVariableCalled("numeroTramite").toString();
        String esComercial = Serenity.sessionVariableCalled("COMERCIAL").toString();
        String nrotarjeta = "";
        try {

             nrotarjeta =ApiCommons.clientes.getNumerotarjeta();
        }catch (Exception e)
        {
            System.out.println(e);
        }
        if (esComercial.equals("COMERCIAL")) {
            if(tipoProducto.equals("TARJETA DE CRÉDITO")||tipoProducto.equals("TARJETA DE DÉBITO")) {
                if (tipologia.toUpperCase().equals("CONSUMOS NO RECONOCIDOS POR FRAUDE".toUpperCase())) {
                    tipologia = "CONSUMOS NO RECONOCIDOS POR FRAUDE [C]";

                } else if (tipologia.toUpperCase().equals("CONSUMOS MAL PROCESADOS POR EL COMERCIO/MARCA".toUpperCase())) {
                    tipologia = "CONSUMOS MAL PROCESADOS POR EL COMERCIO/MARCA [C]";
                }
            }
            else
            if(tipoProducto.equals("SEGUROS RELACIONADOS")||tipoProducto.equals("SEGUROS")){
                if(tipologia.equals("COBROS INDEBIDOS")){
                    tipologia="COBROS INDEBIDOS[C]";
                }
                else if(tipologia.equals("DESAFILIACIÓN DE SEGURO")){
                    tipologia="DESAFILIACIÓN DE SEGURO [C]";
                }
                else if(tipologia.equals("DUPLICADO DE DOCUMENTO")){
                    tipologia="DUPLICADO DE DOCUMENTO [C]";
                }
            }


        }

        if (tipoDocumento.equals("PASAPORTE") || tipoDocumento.equals("PTP") || tipoDocumento.equals("DNI") || tipoDocumento.equals("CE")) {
            String tipoDocumentoFront = Serenity.getDriver().findElement(By.cssSelector("div[class='flex middle-xs mr-28p'] label[class='parrafo h-bold']")).getText();
            String valueDocumentType = tipoDocumentoFront.replace(":", "");

            String numeroDocumentoFront = Serenity.getDriver().findElement(By.xpath("(//span[@class='parrafo_todter h-regular'])[1]")).getText();
            String valueDocumentNumber = numeroDocumentoFront.trim();

            Assert.assertEquals(tipoDocumento, valueDocumentType);
            Assert.assertEquals(numeroDocumento, valueDocumentNumber);
        }

        //TODO: VALIDA EN RESUMEN - TIPO TRAMITE Y TIPOLOGIA
        Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        boolean labelTipoTramite = Serenity.getDriver().findElements(By.xpath("//label[text()='Tipo de trámite:']/following-sibling::label")).size() > 0;
        if (labelTipoTramite) {
            System.err.println("VALIDA EN RESUMEN - TIPO TRAMITE Y TIPOLOGIA - ENTRA LABEL");
            String tipoTramiteLabel = Serenity.getDriver().findElement(By.xpath("//label[text()='Tipo de trámite:']/following-sibling::label")).getText();
            String tipologiaLabel = Serenity.getDriver().findElement(By.xpath("//label[text()='Tipología:']/following-sibling::label")).getText();

//            String numeroTramiteLabel = "";
//            if (tipoProducto.equals("PAGOS PLANILLAS/PROVEEDORES") && tipologia.equals("COBROS INDEBIDOS")) {
//                numeroTramiteLabel = Serenity.getDriver().findElement(By.xpath("//label[text()='N° de trámite:']/following-sibling::label")).getText();
//            } else if (tipoProducto.equals("SUNAT") && tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA")) {
//                numeroTramiteLabel = Serenity.getDriver().findElement(By.xpath("//label[text()='N° de trámite:']/following-sibling::label")).getText();
//            } else if (tipoProducto.equals("EFECTIVO CON GARANTÍA HIPOTECARIO") || tipoProducto.equals("HIPOTECARIO") ||
//                    tipoProducto.equals("REFINANCIADO") || tipoProducto.equals("VEHICULAR")) {
//                numeroTramiteLabel = Serenity.getDriver().findElement(By.xpath("//label[text()='N° de trámite:']/following-sibling::label")).getText();
//            } else {
//                numeroTramiteLabel = Serenity.getDriver().findElement(By.xpath("//label[text()='Número de trámite:']/following-sibling::label")).getText();
//            }

            Assert.assertEquals(tipoTramite.toUpperCase(new Locale("es", "Peru")), tipoTramiteLabel.toUpperCase(new Locale("es", "Peru")));
            Assert.assertEquals(tipologia.toUpperCase(new Locale("es", "Peru")), tipologiaLabel.toUpperCase(new Locale("es", "Peru")));
            //Assert.assertEquals(numeroTramite, numeroTramiteLabel);
        } else {
            System.err.println("VALIDA EN RESUMEN - TIPO TRAMITE Y TIPOLOGIA - ENTRA SPAN");
            String tipoTramiteSpan = Serenity.getDriver().findElement(By.xpath("//label[text()='Tipo de trámite:']/following-sibling::span")).getText();
            String tipologiaSpan = Serenity.getDriver().findElement(By.xpath("//label[text()='Tipología:']/following-sibling::span")).getText();

//            String numeroTramiteSpan = "";
//            if (tipoProducto.equals("PAGOS PLANILLAS/PROVEEDORES") && tipologia.equals("COBROS INDEBIDOS")) {
//                numeroTramiteSpan = Serenity.getDriver().findElement(By.xpath("//label[text()='N° de trámite:']/following-sibling::span")).getText();
//            } else if (tipoProducto.equals("SUNAT") && tipologia.equals("TRANSACCIÓN MAL O NO PROCESADA")) {
//                numeroTramiteSpan = Serenity.getDriver().findElement(By.xpath("//label[text()='N° de trámite:']/following-sibling::span")).getText();
//            } else if (tipoProducto.equals("EFECTIVO CON GARANTÍA HIPOTECARIO") || tipoProducto.equals("HIPOTECARIO") ||
//                    tipoProducto.equals("REFINANCIADO") || tipoProducto.equals("VEHICULAR")) {
//                numeroTramiteSpan = Serenity.getDriver().findElement(By.xpath("//label[text()='N° de trámite:']/following-sibling::span")).getText();
//            } else {
//                numeroTramiteSpan = Serenity.getDriver().findElement(By.xpath("//label[text()='Número de trámite:']/following-sibling::span")).getText();
//            }
           // String nuevotipologia = Serenity.sessionVariableCalled("nuevoTipologia");
            Assert.assertEquals(tipoTramite.toUpperCase(new Locale("es", "Peru")), tipoTramiteSpan.toUpperCase(new Locale("es", "Peru")));
            Assert.assertEquals(tipologia.toUpperCase(new Locale("es", "Peru")), tipologiaSpan.toUpperCase(new Locale("es", "Peru")));
            //Assert.assertEquals(numeroTramite, numeroTramiteSpan);
        }

        //TODO: VALIDA EN HISTORIAL - NUMERO TARJETA
        if (tipoProducto.equals("TARJETA DE CREDITO") || tipoProducto.equals("TARJETA DE DEBITO")) {

            Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(3));
            boolean labelNumeroTarjeta = Serenity.getDriver().findElements(By.xpath("//label[text()='Número de tarjeta']/following-sibling::span")).size() > 0;

            if (labelNumeroTarjeta) {
                System.err.println("VALIDA EN RESUMEN - NUMERO TARJETA - ENTRA SPAN");
                String numeroTarjeta = Serenity.getDriver().findElement(By.xpath("//label[text()='Número de tarjeta']/following-sibling::span")).getText();
                try{Assert.assertEquals(nrotarjeta.replace(" ", ""), numeroTarjeta.replace(" ", ""));} catch (Exception e){System.out.println(e);}
            } else {
                System.err.println("VALIDA EN RESUMEN - NUMERO TARJETA - ENTRA LABEL");
                String numeroTarjeta = Serenity.getDriver().findElement(By.xpath("//label[text()='Número de tarjeta:']/following-sibling::label")).getText();
                try{Assert.assertEquals(nrotarjeta.replace(" ", ""), numeroTarjeta.replace(" ", ""));} catch (Exception e){System.out.println(e);}
            }
        }

    }


    //TODO: AGREGAR VALIDACION - DESPUES DE RETIFICAR - HISTORIAL
    public void validoDatosTramiteRetipificado() {
        WebElement btnseguimiento = general.expandContainer("1css", btnSeguimiento, "ibk-tab:nth-child(2)", "", "", "");
        btnseguimiento.click();
        WebElement prueba = general.expandContainer("1css", lblArchivo, "span.process-description", "", "", "");
        System.out.println("Se encuentra en proceso new: " + prueba.getText());
        String tipoResol = Serenity.sessionVariableCalled("tipoResol").toString();
        //String estoyEnResolutoria = Serenity.sessionVariableCalled("estoyEnResolutoria").toString();

//        Assert.assertEquals("EN PROCESO", prueba.getText());

    }

    public void validoDatosTramiteResol() {
        String tipoResolucion = Serenity.sessionVariableCalled("tipoResol").toString();
        //Validar estado del tramite
//        WebElement estadoTramite = general.expandContainer("2css", lblResolTramite, "div > div.ibkTableGroup__table > div.grid-wrapper > ibk-grid", "div > table > tbody > tr > td.no-overflow > div > span:nth-child(1)", "","");
//        System.err.println(estadoTramite.getText());
        WebElement btnseguimiento = general.expandContainer("1css", btnSeguimiento, "ibk-tab:nth-child(2)", "", "", "");
        btnseguimiento.click();
        //Se revisara ya que cae en este paso
        WebElement prueba = general.expandContainer("1css", lblArchivo, "span.process-description", "", "", "");
        System.err.println(prueba.getText());
        String tipoResol = Serenity.sessionVariableCalled("tipoResol").toString();
        String estoyEnResolutoria = Serenity.sessionVariableCalled("estoyEnResolutoria").toString();

        if (estoyEnResolutoria.equals("Si") && tipoResol.equals("Procede")) {
            Assert.assertEquals("PROCEDE", prueba.getText());
            System.out.println("<<< PROCEDE >>>" + " OK ");
        } else if (estoyEnResolutoria.equals("Si") && tipoResol.equals("No Procede")) {
            Assert.assertEquals("NO PROCEDE", prueba.getText());
            System.out.println("<<< NO PROCEDE >>>" + " OK ");
        } else {
            System.out.println("escenarios prodece derivacion");
        }

    }

    public void validoHRDescargado() {

        WebElement actividadArchivoSeguimientoHistorial = general.expandContainer("2css", lblArchivo, "#stage_registro > div.detail-process-container > div > div.body-detail-process > div > div > div.content-subdetail-process.detail_hoja-de-tramite-original.w-100 > ibk-giru-link-icon", "div > div.link > a", "", "");
        waitForCondition().until(ExpectedConditions.visibilityOf(actividadArchivoSeguimientoHistorial));

        JavascriptExecutor js = (JavascriptExecutor) Serenity.getDriver();
        js.executeScript("arguments[0].scrollIntoView();", actividadArchivoSeguimientoHistorial);

        actividadArchivoSeguimientoHistorial.click();

        try {
            Thread.sleep(2000);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        String ambiente = Serenity.sessionVariableCalled("ambiente");
        if (ambiente.equals("UAT")) {
            try {
                Thread.sleep(4000);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

        String numeroTramite = Serenity.sessionVariableCalled("numeroTramite").toString();
        System.out.println("SE DESCARGO EL HR DEL TRAMITE");
        String home = System.getProperty("user.home");
        String file_with_location = home + "\\Downloads\\" + "HR_" + numeroTramite + ".pdf";
        File file = new File(file_with_location);

        if (file.exists()) {
            System.out.println(file_with_location + "  SE DESCARGO EL HR CON EXITO");
        } else {
            System.err.println(file_with_location + "  NO SE ENCUENTRA DESCARGADO EL HR - REVISAR POR FRONT");
        }

    }


}


