package com.everis.giru.userinterface.tramites.paginacionPage;

import com.everis.giru.utils.Shadow;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

public class CambiarPaginaPage extends PageObject {

    Shadow general = new Shadow();
    @FindBy(xpath = "//*[@id='grid']/giru-all-procedures/div[3]/ibk-grid-group")
    WebElement PAGINACION_BASE;

    @FindBy(xpath = "//ibk-grid-group")
    WebElement txtNumeroReclamoAll;

    public String removerCeros(String cadena) {
        cadena = cadena.replaceAll("^0+", "");
        return cadena;
    }
    public String ValidarYObtenerPrimerTramiteRegistroAntiguo() {

        try {
            Thread.sleep(10000);
        }catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
        WebElement totalResultados = general.expandContainer("2cssSize", txtNumeroReclamoAll, "ibk-grid", "div > table > tbody > tr", "", "");

       int totalRestNum = Serenity.sessionVariableCalled("cantidadTable");
        String tramite = "";
        System.out.println("Cantidad elementos tabla " + totalRestNum);
       for(int i =1; i<totalRestNum;i++) {
           //TODO: VALIDACION - NUMERO DE TRAMITE - BANDEJA
           WebElement valueTramite = general.expandContainer("2css", txtNumeroReclamoAll, "ibk-grid", "div > table > tbody > tr:nth-child("+i+") > td:nth-child(2)", "", "");
           WebElement diasVencido = general.expandContainer("3css", txtNumeroReclamoAll, "ibk-grid", "div > table > tbody > tr:nth-child("+i+") > td:nth-child(3) > ibk-label-badge", "div > span", "");

           WebElement check = general.expandContainer("2css", txtNumeroReclamoAll, "ibk-grid", "div > table > tbody > tr:nth-child("+i+") > td:nth-child(1)", "", "");


           ((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true)", valueTramite);
           String getTramite = valueTramite.getText();
           String sSubTramite = removerCeros(getTramite);
           System.out.println("Numero tramite Original: " + getTramite);
           System.out.println("Numero tramite Con Metodo: " + sSubTramite);

           String textDiasVencido = diasVencido.getText();
           if (textDiasVencido.contains("-")) {
               tramite = sSubTramite;
               check.click();
               break;
           }
       }

        return tramite;

    }
    public void cambiarPaginacionAnteriorUltima()
    {
        try {
            Thread.sleep(15000);
        }catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
        WebElement labelPaginacion = general.expandContainer("3css", PAGINACION_BASE, "div > div.ibkTableGroup__options > div.lg.sm-p > ibk-pagination-group2", "div > div.ibkPaginationGroup__pagination > ibk-pagination2", "div > span > b", "");
        String textPage = labelPaginacion.getText();
        int textoI = Integer.parseInt(textPage);

        System.out.println(textPage);


        WebElement inputPaginacion = general.expandContainer("3css", PAGINACION_BASE, "div > div.ibkTableGroup__options > div.lg.sm-p > ibk-pagination-group2", "div > div.ibkPaginationGroup__pagination > ibk-pagination2", "#page", "");
        inputPaginacion.sendKeys(Keys.BACK_SPACE);
        try {
            Thread.sleep(5000);

        Robot robo = new Robot();

        StringSelection str = new StringSelection(textPage);
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(str, null);
        Thread.sleep(3000);


            for (char ch: textPage.toCharArray()) {
                if(ch == '1') {robo.keyPress(KeyEvent.VK_NUMPAD1);robo.keyRelease(KeyEvent.VK_NUMPAD1);}
                if(ch == '2') {robo.keyPress(KeyEvent.VK_NUMPAD2);robo.keyRelease(KeyEvent.VK_NUMPAD2);}
                if(ch == '3') {robo.keyPress(KeyEvent.VK_NUMPAD3);robo.keyRelease(KeyEvent.VK_NUMPAD3);}
                if(ch == '4') {robo.keyPress(KeyEvent.VK_NUMPAD4);robo.keyRelease(KeyEvent.VK_NUMPAD4);}
                if(ch == '5') {robo.keyPress(KeyEvent.VK_NUMPAD5);robo.keyRelease(KeyEvent.VK_NUMPAD5);}
                if(ch == '6') {robo.keyPress(KeyEvent.VK_NUMPAD6);robo.keyRelease(KeyEvent.VK_NUMPAD6);}
                if(ch == '7') {robo.keyPress(KeyEvent.VK_NUMPAD7);robo.keyRelease(KeyEvent.VK_NUMPAD7);}
                if(ch == '8') {robo.keyPress(KeyEvent.VK_NUMPAD8);robo.keyRelease(KeyEvent.VK_NUMPAD8);}
                if(ch == '9') {robo.keyPress(KeyEvent.VK_NUMPAD9);robo.keyRelease(KeyEvent.VK_NUMPAD9);}
                if(ch == '0') {robo.keyPress(KeyEvent.VK_NUMPAD0);robo.keyRelease(KeyEvent.VK_NUMPAD0);}
            }



        Thread.sleep(5000);
        }catch (Exception e)
        {
            System.out.println(e);
        }
        try {
            Thread.sleep(5000);
            Robot robo2 = new Robot();
            robo2.keyPress(KeyEvent.VK_ENTER);robo2.keyRelease(KeyEvent.VK_ENTER);

        }catch (Exception e)
        {
            System.out.println(e);
        }

        try {
            Thread.sleep(5000);
        }catch (Exception e)
        {
            System.out.println(e);
        }
    }
}
