package com.everis.giru.userinterface.tramites.contadorPage;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class GetContadorIzquierdoPage extends PageObject {

    public String contador = null;
    public WebElement getcontador;

    public void SetContadorANull() {
        contador = null;
    }

    public String getValueContadorIzquierdo(String departamento, String perfil, String tipologia) {
        switch (departamento) {
            case "GPYR":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-5']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);
                                break;
                            case "CobrosIndebidos":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-7']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);
                                break;
                            case "ExoneracionCobros":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-8']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);
                                break;
                            case "AnulacionTC":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-15']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);
                                break;
                            case "FinalizacionTC":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-16']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);
                                break;
                            case "TransaccionMalNoProcesada":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-19']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);
                                break;
                            case "PagoAnticipadoAdelantoCuotas":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-20']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);
                                break;
                            case "DevolucionSaldoAcreedor":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-22']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);
                                break;
                            case "ProgramaRecompensas":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-31']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);
                                break;
                            case "SistemaRecompensas":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-32']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);
                                break;
                            case "InsuficienteInformacion":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-33']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);
                                break;
                            case "IncorrectaAplicacionCuotas":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-37']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);
                                break;

                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA LADO IZQUIERDO BANDEJA EN: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-1']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);

                                break;
                            case "MiEquipo":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-4']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);

                                break;
                            case "Asignar":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-2']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);

                                break;
                            case "Resolver":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-3']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);

                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA LADO IZQUIERDO BANDEJA EN: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                }
                break;
            case "Seguros":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-5']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);


                                break;
                            case "CobrosIndebidos":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-7']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);


                                break;
                            case "ExoneracionCobros":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-8']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);

                                break;
                            case "ConsumosNoReconocidos":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-11']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);

                                break;
                            case "Monitorear":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-12']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);

                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA LADO IZQUIERDO BANDEJA EN: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-1']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);

                                break;
                            case "MiEquipo":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-4']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);

                                break;
                            case "Asignar":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-2']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);

                                break;
                            case "Resolver":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-3']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);

                                break;
                            case "Monitorear":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-12']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + "tramites del perfil " + perfil + " +  perteneciente al departamento de " + departamento);

                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA LADO IZQUIERDO BANDEJA EN: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                }
                break;
            case "Retenciones":
                break;
            case "Controversias":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-5']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);

                                break;
                            case "ConsumosNoReconocidos":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-11']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);

                                break;
                            case "ConsumosMalProcesados":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-13']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);

                                break;
                            case "Monitorear":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-12']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);

                                break;
                            case "TransaccionesEnProceso":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-17']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);

                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA LADO IZQUIERDO BANDEJA EN: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-1']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);

                                break;
                            case "MiEquipo":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-4']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);

                                break;
                            case "Asignar":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-2']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);

                                break;
                            case "Resolver":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-3']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);

                                break;
                            case "Monitorear":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-12']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);

                                break;
                            case "TransaccionesEnProceso":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-17']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA LADO IZQUIERDO BANDEJA EN: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                }
                break;
            case "USPR":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-5']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);
                                break;
                            case "CobrosIndebidos":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-7']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);
                                break;
                            case "ExoneracionCobros":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-8']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);
                                break;
                            case "ConsumosMalProcesados":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-13']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);
                                break;
                            case "ConsumosNoReconocidos":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-11']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);
                                break;
                            case "TransaccionMalNoProcesada":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-19']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);
                                break;
                            case "DuplicadoEstadoCuentaSC":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-27']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);
                                break;
                            case "SistemaRecompensas":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-32']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);
                                break;
                            case "DesacuerdoCondicionesTT":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-36']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);
                                break;
                            case "Retipificar":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-10']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);

                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA LADO IZQUIERDO BANDEJA EN: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-1']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);

                                break;
                            case "MiEquipo":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-4']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);

                                break;
                            case "Asignar":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-2']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);

                                break;
                            case "Resolver":
                                getcontador = getDriver().findElement(By.xpath("//*[@id='item-3']/div[2]/div[2]/span"));
                                contador = getcontador.getText();
                                System.out.println("Tipologia: " + tipologia + " tiene " + contador + " tramites del perfil " + perfil + " perteneciente al departamento de " + departamento);

                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA LADO IZQUIERDO BANDEJA EN: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                }
                break;
        }


        return contador;
    }

}
