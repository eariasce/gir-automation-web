package com.everis.giru.userinterface.tramites.filtrosPage;

import com.everis.giru.utils.Shadow;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class GetFiltroTipologiaPage extends PageObject {

    Shadow general = new Shadow();

    @FindBy(xpath = "//*[@id='radio-list__radio-option']")
    WebElement LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='radio-list__radio-option']")
    WebElement LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS;
    @FindBy(xpath = "//*[@id='radio-list__radio-option']")
    WebElement LBL_ID_RADIOBUTTOM_ESPECIALISTA_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='radio-list__radio-option']")
    WebElement LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR;

    @FindBy(xpath = "//*[@id='radio-list__radio-option']")
    WebElement LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR;
    @FindBy(xpath = "//*[@id='radio-list__radio-option']")
    WebElement LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS;
    @FindBy(xpath = "//*[@id='radio-list__radio-option']")
    WebElement LBL_ID_RADIOBUTTOM_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='radio-list__radio-option']")
    WebElement LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR;


    public void seleccionaFiltroTipologia(String fTipologia, String perfil, String departamento, String fTipoTramite) {

        switch (departamento) {
            case "GPYR":
                switch (perfil) {
                    case "Especialista":
                        switch (fTipoTramite) {
                            case "Reclamo":
                                switch (fTipologia) {
                                    case "CobrosIndebidos":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                        WebElement btnCobrosIndebidos = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(1) > label", "", "", "");
                                        btnCobrosIndebidos.click();
                                        System.out.println("\n" + "Select filtro tipo tramite: " + fTipoTramite + " " + fTipologia);
                                        break;
                                    case "TransaccionMalNoProcesada":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                        WebElement btnTransaccionMalNoProcesada = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(2) > label", "", "", "");
                                        btnTransaccionMalNoProcesada.click();
                                        System.out.println("\n" + "Select filtro tipo tramite: " + fTipoTramite + " " + fTipologia);
                                        break;
                                    case "SistemaRecompensas":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                        WebElement btnSistemaRecompensas = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(3) > label", "", "", "");
                                        btnSistemaRecompensas.click();
                                        System.out.println("\n" + "Select filtro tipo tramite: " + fTipoTramite + " " + fTipologia);
                                        break;
                                    case "InsuficienteInformacion":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                        WebElement btnInsuficienteInformacion = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(4) > label", "", "", "");
                                        btnInsuficienteInformacion.click();
                                        System.out.println("\n" + "Select filtro tipo tramite: " + fTipoTramite + " " + fTipologia);
                                        break;
                                    case "IncorrectaAplicacionCuotas":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                        WebElement btnIncorrectaAplicacionCuotas = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(5) > label", "", "", "");
                                        btnIncorrectaAplicacionCuotas.click();
                                        System.out.println("\n" + "Select filtro tipo tramite: " + fTipoTramite + " " + fTipologia);
                                        break;

                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento GPYR!!!");
                                }
                                break;
                            case "Pedido":
                                switch (fTipologia) {
                                    case "ExoneracionCobros":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                        WebElement btnExoneracionCobros = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(1) > label", "", "", "");
                                        btnExoneracionCobros.click();
                                        System.out.println("\n" + "Select filtro tipo tramite: " + fTipoTramite + " " + fTipologia);
                                        break;
                                    case "ModificacionPlazo":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                        WebElement btnModificacionPlazo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(2) > label", "", "", "");
                                        btnModificacionPlazo.click();
                                        System.out.println("\n" + "Select filtro tipo tramite: " + fTipoTramite + " " + fTipologia);
                                        break;
                                    case "ProgramaRecompensas":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                        WebElement btnProgramaRecompensas = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(3) > label", "", "", "");
                                        btnProgramaRecompensas.click();
                                        System.out.println("\n" + "Select filtro tipo tramite: " + fTipoTramite + " " + fTipologia);
                                        break;
                                    case "PagoAnticipadoAdelantoCuotas":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                        WebElement btnPagoAnticipadoAdelantoCuotas = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(4) > label", "", "", "");
                                        btnPagoAnticipadoAdelantoCuotas.click();
                                        System.out.println("\n" + "Select filtro tipo tramite: " + fTipoTramite + " " + fTipologia);
                                        break;
                                    case "DevolucionSaldoAcreedor":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                        WebElement btnDevolucionSaldoAcreedor = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(5) > label", "", "", "");
                                        btnDevolucionSaldoAcreedor.click();
                                        System.out.println("\n" + "Select filtro tipo tramite: " + fTipoTramite + " " + fTipologia);
                                        break;

                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento GPYR!!!");
                                }
                                break;
                            case "Solicitud":
                                switch (fTipologia) {
                                    case "AnulacionTC":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                        WebElement btnAnulacionTC = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(1) > label", "", "", "");
                                        btnAnulacionTC.click();
                                        System.out.println("\n" + "Select filtro tipo tramite: " + fTipoTramite + " " + fTipologia);
                                        break;
                                    case "FinalizacionTC":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                        WebElement btnFinalizacionTC = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(2) > label", "", "", "");
                                        btnFinalizacionTC.click();
                                        System.out.println("\n" + "Select filtro tipo tramite: " + fTipoTramite + " " + fTipologia);
                                        break;
                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento GPYR!!!");
                                }
                                break;
                            default:
                                System.out.println("No se encuentra TipoTramite mapeado como: " + fTipoTramite + "  en perfil Especialista del departamento GPYR!!!");
                        }
                        break;
                    case "Supervisor":
                        switch (fTipoTramite) {
                            case "Reclamo":
                                switch (fTipologia) {
                                    case "CobrosIndebidos":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                        WebElement btnCobrosIndebidos = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(1) > label", "", "", "");
                                        btnCobrosIndebidos.click();
                                        System.out.println("\n" + "Select filtro tipo tramite: " + fTipoTramite + " " + fTipologia);
                                        break;
                                    case "TransaccionMalNoProcesada":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                        WebElement btnTransaccionMalNoProcesada = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(2) > label", "", "", "");
                                        btnTransaccionMalNoProcesada.click();
                                        System.out.println("\n" + "Select filtro tipo tramite: " + fTipoTramite + " " + fTipologia);
                                        break;
                                    case "SistemaRecompensas":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                        WebElement btnSistemaRecompensas = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(3) > label", "", "", "");
                                        btnSistemaRecompensas.click();
                                        System.out.println("\n" + "Select filtro tipo tramite: " + fTipoTramite + " " + fTipologia);
                                        break;
                                    case "InsuficienteInformacion":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                        WebElement btnInsuficienteInformacion = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(4) > label", "", "", "");
                                        btnInsuficienteInformacion.click();
                                        System.out.println("\n" + "Select filtro tipo tramite: " + fTipoTramite + " " + fTipologia);
                                        break;
                                    case "IncorrectaAplicacionCuotas":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                        WebElement btnIncorrectaAplicacionCuotas = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(5) > label", "", "", "");
                                        btnIncorrectaAplicacionCuotas.click();
                                        System.out.println("\n" + "Select filtro tipo tramite: " + fTipoTramite + " " + fTipologia);
                                        break;

                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento GPYR!!!");
                                }
                                break;
                            case "Pedido":
                                switch (fTipologia) {
                                    case "ExoneracionCobros":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                        WebElement btnExoneracionCobros = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(1) > label", "", "", "");
                                        btnExoneracionCobros.click();
                                        System.out.println("\n" + "Select filtro tipo tramite: " + fTipoTramite + " " + fTipologia);
                                        break;
                                    case "ModificacionPlazo":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                        WebElement btnModificacionPlazo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(2) > label", "", "", "");
                                        btnModificacionPlazo.click();
                                        System.out.println("\n" + "Select filtro tipo tramite: " + fTipoTramite + " " + fTipologia);
                                        break;
                                    case "ProgramaRecompensas":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                        WebElement btnProgramaRecompensas = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(3) > label", "", "", "");
                                        btnProgramaRecompensas.click();
                                        System.out.println("\n" + "Select filtro tipo tramite: " + fTipoTramite + " " + fTipologia);
                                        break;
                                    case "PagoAnticipadoAdelantoCuotas":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                        WebElement btnPagoAnticipadoAdelantoCuotas = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(4) > label", "", "", "");
                                        btnPagoAnticipadoAdelantoCuotas.click();
                                        System.out.println("\n" + "Select filtro tipo tramite: " + fTipoTramite + " " + fTipologia);
                                        break;
                                    case "DevolucionSaldoAcreedor":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                        WebElement btnDevolucionSaldoAcreedor = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(5) > label", "", "", "");
                                        btnDevolucionSaldoAcreedor.click();
                                        System.out.println("\n" + "Select filtro tipo tramite: " + fTipoTramite + " " + fTipologia);
                                        break;

                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento GPYR!!!");
                                }
                                break;
                            case "Solicitud":
                                switch (fTipologia) {
                                    case "AnulacionTC":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                        WebElement btnAnulacionTC = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(1) > label", "", "", "");
                                        btnAnulacionTC.click();
                                        System.out.println("\n" + "Select filtro tipo tramite: " + fTipoTramite + " " + fTipologia);
                                        break;
                                    case "FinalizacionTC":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                        WebElement btnFinalizacionTC = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(2) > label", "", "", "");
                                        btnFinalizacionTC.click();
                                        System.out.println("\n" + "Select filtro tipo tramite: " + fTipoTramite + " " + fTipologia);
                                        break;
                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento GPYR!!!");
                                }
                                break;
                            default:
                                System.out.println("No se encuentra TipoTramite mapeado como: " + fTipoTramite + "  en perfil Especialista del departamento GPYR!!!");
                        }
                        break;
                }
                break;

            case "Seguros":
                switch (perfil) {
                    case "Especialista":
                        switch (fTipoTramite) {
                            case "Reclamo":
                                switch (fTipologia) {
                                    case "CobrosIndebidos":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                        WebElement btnCobroIndebido = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(1) > label", "", "", "");
                                        btnCobroIndebido.click();
                                        System.out.println("\n" + "Select filtro tipo tramite: " + fTipoTramite + " " + fTipologia);
                                        break;
                                    case "ConsumosNoReconocidos":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                        WebElement btnConsumoNoReconocido = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(2) > label", "", "", "");
                                        btnConsumoNoReconocido.click();
                                        System.out.println("\n" + "Select filtro tipo tramite: " + fTipoTramite + " " + fTipologia);
                                        break;
                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento SEGUROS!!!");
                                }
                                break;
                            case "Pedido":
                                switch (fTipologia) {
                                    case "ExoneracionCobros":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                        WebElement btnExoneracionCobros = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(1) > label", "", "", "");
                                        btnExoneracionCobros.click();
                                        System.out.println("\n" + "Select filtro tipo tramite: " + fTipoTramite + " " + fTipologia);
                                        break;
                                    case "ModificacionPlazo":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                        WebElement btnModificacionPlazo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(2) > label", "", "", "");
                                        btnModificacionPlazo.click();
                                        System.out.println("\n" + "Select filtro tipo tramite: " + fTipoTramite + " " + fTipologia);
                                        break;

                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento GPYR!!!");
                                }
                                break;
                            default:
                                System.out.println("No se encuentra TipoTramite mapeado como: " + fTipoTramite + "  en perfil Especialista del departamento GPYR!!!");
                        }
                        break;
                    case "Supervisor":
                        switch (fTipoTramite) {
                            case "Reclamo":
                                switch (fTipologia) {
                                    case "CobrosIndebidos":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                        WebElement btnCobroIndebido = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(1) > label", "", "", "");
                                        btnCobroIndebido.click();
                                        System.out.println("\n" + "Select filtro tipo tramite: " + fTipoTramite + " " + fTipologia);
                                        break;
                                    case "ConsumosNoReconocidos":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                        WebElement btnConsumoNoReconocido = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(2) > label", "", "", "");
                                        btnConsumoNoReconocido.click();
                                        System.out.println("\n" + "Select filtro tipo tramite: " + fTipoTramite + " " + fTipologia);
                                        break;
                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento SEGUROS!!!");
                                }
                                break;
                            case "Pedido":
                                switch (fTipologia) {
                                    case "ExoneracionCobros":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                        WebElement btnExoneracionCobros = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(1) > label", "", "", "");
                                        btnExoneracionCobros.click();
                                        System.out.println("\n" + "Select filtro tipo tramite: " + fTipoTramite + " " + fTipologia);
                                        break;
                                    case "ModificacionPlazo":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                        WebElement btnModificacionPlazo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(2) > label", "", "", "");
                                        btnModificacionPlazo.click();
                                        System.out.println("\n" + "Select filtro tipo tramite: " + fTipoTramite + " " + fTipologia);
                                        break;

                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento GPYR!!!");
                                }
                                break;
                            default:
                                System.out.println("No se encuentra TipoTramite mapeado como: " + fTipoTramite + "  en perfil Especialista del departamento GPYR!!!");
                        }
                        break;
                }
                break;

            case "Controversias":
                switch (perfil) {
                    case "Especialista":
                        switch (fTipoTramite) {
                            case "Reclamo":
                                switch (fTipologia) {
                                    case "ConsumosNoReconocidos":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_CONTROVERSIAS));
                                        WebElement btnConsumosNoReconocidos = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_CONTROVERSIAS, "div > div:nth-child(1) > label", "", "", "");
                                        btnConsumosNoReconocidos.click();
                                        break;
                                    case "ConsumosMalProcesados":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_CONTROVERSIAS));
                                        WebElement btnConsumosMalProcesados = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_CONTROVERSIAS, "div > div:nth-child(2) > label", "", "", "");
                                        btnConsumosMalProcesados.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento CONTROVERSIAS!!!");
                                }
                                break;
                            default:
                                System.out.println("No se encuentra TipoTramite mapeado como: " + fTipoTramite + "  en perfil Especialista del departamento GPYR!!!");
                        }
                        break;
                    case "Supervisor":
                        switch (fTipoTramite) {
                            case "Reclamo":
                                switch (fTipologia) {
                                    case "ConsumosNoReconocidos":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_CONTROVERSIAS));
                                        WebElement btnConsumosNoReconocidos = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_CONTROVERSIAS, "div > div:nth-child(1) > label", "", "", "");
                                        btnConsumosNoReconocidos.click();
                                        break;
                                    case "ConsumosMalProcesados":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_CONTROVERSIAS));
                                        WebElement btnConsumosMalProcesados = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_CONTROVERSIAS, "div > div:nth-child(2) > label", "", "", "");
                                        btnConsumosMalProcesados.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento CONTROVERSIAS!!!");
                                }
                                break;
                            default:
                                System.out.println("No se encuentra TipoTramite mapeado como: " + fTipoTramite + "  en perfil Especialista del departamento GPYR!!!");
                        }
                        break;
                }
                break;

            case "USPR":
                switch (perfil) {
                    case "Especialista":
                        switch (fTipoTramite) {
                            case "Reclamo":
                                switch (fTipologia) {
                                    case "CobrosIndebidos":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                        WebElement btnCobrosIndebidos = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(1) > label", "", "", "");
                                        btnCobrosIndebidos.click();
                                        break;
                                    case "ConsumosNoReconocidos":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                        WebElement btnConsumosNoReconocidos = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(2) > label", "", "", "");
                                        btnConsumosNoReconocidos.click();
                                        break;
                                    case "ConsumosMalProcesados":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                        WebElement btnConsumosMalProcesados = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(3) > label", "", "", "");
                                        btnConsumosMalProcesados.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento USPR!!!");
                                }
                                break;
                            case "Pedido":
                                switch (fTipologia) {
                                    case "ExoneracionCobros":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                        WebElement btnExoneracionCobros = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div > label", "", "", "");
                                        btnExoneracionCobros.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento GPYR!!!");
                                }
                                break;

                            default:
                                System.out.println("No se encuentra TipoTramite mapeado como: " + fTipoTramite + "  en perfil Especialista del departamento GPYR!!!");
                        }
                        break;
                    case "Supervisor":
                        switch (fTipoTramite) {
                            case "Reclamo":
                                switch (fTipologia) {
                                    case "CobrosIndebidos":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                        WebElement btnCobrosIndebidos = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(1) > label", "", "", "");
                                        btnCobrosIndebidos.click();
                                        break;
                                    case "ConsumosNoReconocidos":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                        WebElement btnConsumosNoReconocidos = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(2) > label", "", "", "");
                                        btnConsumosNoReconocidos.click();
                                        break;
                                    case "ConsumosMalProcesados":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                        WebElement btnConsumosMalProcesados = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(3) > label", "", "", "");
                                        btnConsumosMalProcesados.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento USPR!!!");
                                }
                                break;
                            case "Pedido":
                                switch (fTipologia) {
                                    case "ExoneracionCobros":
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                        WebElement btnExoneracionCobros = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div > label", "", "", "");
                                        btnExoneracionCobros.click();
                                        break;
                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento USPR!!!");
                                }
                                break;

                            default:
                                System.out.println("No se encuentra TipoTramite mapeado como: " + fTipoTramite + "  en perfil Especialista del departamento USPR!!!");
                        }
                        break;
                }

                break;
            default:
                System.out.println("No existe filtro TipoDeTramite en el departamento ingresado: " + departamento + " !!!");
        }

    }


}
