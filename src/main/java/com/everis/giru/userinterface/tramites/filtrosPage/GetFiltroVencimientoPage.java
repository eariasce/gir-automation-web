package com.everis.giru.userinterface.tramites.filtrosPage;

import com.everis.giru.utils.Shadow;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class GetFiltroVencimientoPage extends PageObject {

    Shadow general = new Shadow();

    @FindBy(xpath = "//*[@id='radio-list__radio-option']")
    WebElement LBL_ID_RADIOBUTTOM_VENCIMIENTO;

    public void seleccionaFiltroVencimiento(String fVencimiento) {

        switch (fVencimiento) {
            case "Nuevos":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_VENCIMIENTO));
                WebElement btnNuevo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_VENCIMIENTO, "div > div:nth-child(1) > label", "", "", "");
                btnNuevo.click();
                break;
            case "PorVencer":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_VENCIMIENTO));
                WebElement btnPorVencer = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_VENCIMIENTO, "div > div:nth-child(2) > label", "", "", "");
                btnPorVencer.click();
                break;
            case "VencidosArea":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_VENCIMIENTO));
                WebElement btnVencidosArea = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_VENCIMIENTO, "div > div:nth-child(3) > label", "", "", "");
                btnVencidosArea.click();
                break;
            case "VencidosCliente":
                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_VENCIMIENTO));
                WebElement btnVencidosCliente = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_VENCIMIENTO, "div > div:nth-child(4) > label", "", "", "");
                btnVencidosCliente.click();
                break;

            default:
                System.out.println("No se encuentra Filtro Vencimiento!!!");
        }
    }


}
