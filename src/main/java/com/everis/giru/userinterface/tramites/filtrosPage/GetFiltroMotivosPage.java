package com.everis.giru.userinterface.tramites.filtrosPage;

import com.everis.giru.utils.Shadow;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class GetFiltroMotivosPage extends PageObject {

    Shadow general = new Shadow();

    @FindBy(xpath = "//*[@id='radio-list__radio-option']")
    WebElement LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='radio-list__radio-option']")
    WebElement LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS;
    @FindBy(xpath = "//*[@id='radio-list__radio-option']")
    WebElement LBL_ID_RADIOBUTTOM_ESPECIALISTA_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='radio-list__radio-option']")
    WebElement LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR;

    @FindBy(xpath = "//*[@id='radio-list__radio-option']")
    WebElement LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR;
    @FindBy(xpath = "//*[@id='radio-list__radio-option']")
    WebElement LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS;
    @FindBy(xpath = "//*[@id='radio-list__radio-option']")
    WebElement LBL_ID_RADIOBUTTOM_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='radio-list__radio-option']")
    WebElement LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR;


    public void SeleccionaFiltroMotivos(String departamento, String perfil, String fTipoTramite, String fTipologia, String fMotivos) {

        switch (departamento) {
            case "GPYR":
                switch (perfil) {
                    case "Especialista":
                        switch (fTipoTramite) {
                            case "Reclamo":
                                switch (fTipologia) {
                                    case "CobrosIndebidos":
                                        switch (fMotivos) {
                                            case "PenalidadIncumplimientoPago":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                                WebElement btnPenalidadIncumplimientoPago = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(1) > label", "", "", "");
                                                btnPenalidadIncumplimientoPago.click();
                                                break;
                                            case "ComisionMembresia":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                                WebElement btnComisionMembresia = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(2) > label", "", "", "");
                                                btnComisionMembresia.click();
                                                break;
                                            case "InteresCreditoNormalFinanciar":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                                WebElement btnInteresCreditoNormalFinanciar = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(3) > label", "", "", "");
                                                btnInteresCreditoNormalFinanciar.click();
                                                break;
                                            case "InteresDisposicionEfectivo":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                                WebElement btnInteresDisposicionEfectivo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(4) > label", "", "", "");
                                                btnInteresDisposicionEfectivo.click();
                                                break;
                                            case "ComisionUsoCanales":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                                WebElement btnComisionUsoCanales = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(5) > label", "", "", "");
                                                btnComisionUsoCanales.click();
                                                break;
                                            case "ComisionEnvioFisicoEECC":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                                WebElement btnComisionEnvioFisicoEECC = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(6) > label", "", "", "");
                                                btnComisionEnvioFisicoEECC.click();
                                                break;
                                            case "ComisionReposicionPlastico":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                                WebElement btnComisionReposicionPlastico = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(7) > label", "", "", "");
                                                btnComisionReposicionPlastico.click();
                                                break;
                                            case "SeguroDesgravamen":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                                WebElement btnSeguroDesgravamen = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(8) > label", "", "", "");
                                                btnSeguroDesgravamen.click();
                                                break;
                                            case "TipoCambio":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                                WebElement btnTipoCambio = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(9) > label", "", "", "");
                                                btnTipoCambio.click();
                                                break;
                                            case "InteresMoratorio":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                                WebElement btnInteresMoratorio = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(10) > label", "", "", "");
                                                btnInteresMoratorio.click();
                                                break;

                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento GPYR!!!");
                                        }
                                        break;
                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento GPYR!!!");
                                }
                                break;

                            case "Pedido":
                                switch (fTipologia) {
                                    case "ExoneracionCobros":
                                        switch (fMotivos) {
                                            case "PenalidadIncumplimientoPago":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                                WebElement btnPenalidadIncumplimientoPago = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(1) > label", "", "", "");
                                                btnPenalidadIncumplimientoPago.click();
                                                break;
                                            case "ComisionMembresia":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                                WebElement btnComisionMembresia = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(2) > label", "", "", "");
                                                btnComisionMembresia.click();
                                                break;
                                            case "InteresCreditoNormalFinanciar":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                                WebElement btnInteresCreditoNormalFinanciar = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(3) > label", "", "", "");
                                                btnInteresCreditoNormalFinanciar.click();
                                                break;
                                            case "InteresDisposicionEfectivo":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                                WebElement btnInteresDisposicionEfectivo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(4) > label", "", "", "");
                                                btnInteresDisposicionEfectivo.click();
                                                break;
                                            case "ComisionUsoCanales":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                                WebElement btnComisionUsoCanales = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(5) > label", "", "", "");
                                                btnComisionUsoCanales.click();
                                                break;
                                            case "ComisionEnvioFisicoEECC":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                                WebElement btnComisionEnvioFisicoEECC = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(6) > label", "", "", "");
                                                btnComisionEnvioFisicoEECC.click();
                                                break;
                                            case "ComisionReposicionPlastico":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                                WebElement btnComisionReposicionPlastico = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(7) > label", "", "", "");
                                                btnComisionReposicionPlastico.click();
                                                break;
                                            case "SeguroDesgravamen":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                                WebElement btnSeguroDesgravamen = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(8) > label", "", "", "");
                                                btnSeguroDesgravamen.click();
                                                break;
                                            case "ComisionRetiroATM":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                                WebElement btnComisionRetiroATMBancoLocalExtranjero = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(9) > label", "", "", "");
                                                btnComisionRetiroATMBancoLocalExtranjero.click();
                                                break;
                                            case "InteresMoratorio":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                                WebElement btnInteresMoratorio = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div:nth-child(10) > label", "", "", "");
                                                btnInteresMoratorio.click();
                                                break;

                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento GPYR!!!");
                                        }
                                        break;
                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento GPYR!!!");
                                }

                                break;
                            case "Solicitud":
                                switch (fTipologia) {
                                    case "AnulacionTC":
                                        switch (fMotivos) {
                                            case "SinMotivo":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                                WebElement btnSinMotivo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div > label", "", "", "");
                                                btnSinMotivo.click();
                                                break;
                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento GPYR!!!");
                                        }
                                        break;
                                    case "FinalizacionTC":
                                        switch (fMotivos) {
                                            case "SinMotivo":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                                WebElement btnSinMotivo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div > label", "", "", "");
                                                btnSinMotivo.click();
                                                break;
                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento GPYR!!!");
                                        }
                                        break;
                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento GPYR!!!");
                                }
                                break;
                            default:
                                System.out.println("No se encuentra TipoTramite mapeado como: " + fTipoTramite + "  en perfil Especialista del departamento GPYR!!!");
                        }
                        break;
                    case "Supervisor":
                        switch (fTipoTramite) {
                            case "Reclamo":
                                switch (fTipologia) {
                                    case "CobrosIndebidos":
                                        switch (fMotivos) {
                                            case "PenalidadIncumplimientoPago":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                                WebElement btnPenalidadIncumplimientoPago = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(1) > label", "", "", "");
                                                btnPenalidadIncumplimientoPago.click();
                                                break;
                                            case "ComisionMembresia":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                                WebElement btnComisionMembresia = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(2) > label", "", "", "");
                                                btnComisionMembresia.click();
                                                break;
                                            case "InteresCreditoNormalFinanciar":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                                WebElement btnInteresCreditoNormalFinanciar = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(3) > label", "", "", "");
                                                btnInteresCreditoNormalFinanciar.click();
                                                break;
                                            case "InteresDisposicionEfectivo":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                                WebElement btnInteresDisposicionEfectivo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(4) > label", "", "", "");
                                                btnInteresDisposicionEfectivo.click();
                                                break;
                                            case "ComisionUsoCanales":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                                WebElement btnComisionUsoCanales = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(5) > label", "", "", "");
                                                btnComisionUsoCanales.click();
                                                break;
                                            case "ComisionEnvioFisicoEECC":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                                WebElement btnComisionEnvioFisicoEECC = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(6) > label", "", "", "");
                                                btnComisionEnvioFisicoEECC.click();
                                                break;
                                            case "ComisionReposicionPlastico":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                                WebElement btnComisionReposicionPlastico = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(7) > label", "", "", "");
                                                btnComisionReposicionPlastico.click();
                                                break;
                                            case "SeguroDesgravamen":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                                WebElement btnSeguroDesgravamen = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(8) > label", "", "", "");
                                                btnSeguroDesgravamen.click();
                                                break;
                                            case "TipoCambio":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                                WebElement btnTipoCambio = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(9) > label", "", "", "");
                                                btnTipoCambio.click();
                                                break;
                                            case "InteresMoratorio":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                                WebElement btnInteresMoratorio = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(10) > label", "", "", "");
                                                btnInteresMoratorio.click();
                                                break;

                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento GPYR!!!");
                                        }
                                        break;
                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento GPYR!!!");
                                }
                                break;

                            case "Pedido":
                                switch (fTipologia) {
                                    case "ExoneracionCobros":
                                        switch (fMotivos) {
                                            case "PenalidadIncumplimientoPago":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                                WebElement btnPenalidadIncumplimientoPago = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(1) > label", "", "", "");
                                                btnPenalidadIncumplimientoPago.click();
                                                break;
                                            case "ComisionMembresia":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                                WebElement btnComisionMembresia = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(2) > label", "", "", "");
                                                btnComisionMembresia.click();
                                                break;
                                            case "InteresCreditoNormalFinanciar":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                                WebElement btnInteresCreditoNormalFinanciar = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(3) > label", "", "", "");
                                                btnInteresCreditoNormalFinanciar.click();
                                                break;
                                            case "InteresDisposicionEfectivo":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                                WebElement btnInteresDisposicionEfectivo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(4) > label", "", "", "");
                                                btnInteresDisposicionEfectivo.click();
                                                break;
                                            case "ComisionUsoCanales":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                                WebElement btnComisionUsoCanales = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(5) > label", "", "", "");
                                                btnComisionUsoCanales.click();
                                                break;
                                            case "ComisionEnvioFisicoEECC":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                                WebElement btnComisionEnvioFisicoEECC = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(6) > label", "", "", "");
                                                btnComisionEnvioFisicoEECC.click();
                                                break;
                                            case "ComisionReposicionPlastico":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                                WebElement btnComisionReposicionPlastico = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(7) > label", "", "", "");
                                                btnComisionReposicionPlastico.click();
                                                break;
                                            case "SeguroDesgravamen":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                                WebElement btnSeguroDesgravamen = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(8) > label", "", "", "");
                                                btnSeguroDesgravamen.click();
                                                break;
                                            case "ComisionRetiroATM":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                                WebElement btnComisionRetiroATMBancoLocalExtranjero = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(9) > label", "", "", "");
                                                btnComisionRetiroATMBancoLocalExtranjero.click();
                                                break;
                                            case "InteresMoratorio":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                                WebElement btnInteresMoratorio = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div:nth-child(10) > label", "", "", "");
                                                btnInteresMoratorio.click();
                                                break;

                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento GPYR!!!");
                                        }
                                        break;
                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento GPYR!!!");
                                }

                                break;
                            case "Solicitud":
                                switch (fTipologia) {
                                    case "AnulacionTC":
                                        switch (fMotivos) {
                                            case "SinMotivo":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR));
                                                WebElement btnSinMotivo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_GPYR, "div > div > label", "", "", "");
                                                btnSinMotivo.click();
                                                break;
                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento GPYR!!!");
                                        }
                                        break;
                                    case "FinalizacionTC":
                                        switch (fMotivos) {
                                            case "SinMotivo":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR));
                                                WebElement btnSinMotivo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_GPYR, "div > div > label", "", "", "");
                                                btnSinMotivo.click();
                                                break;
                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento GPYR!!!");
                                        }
                                        break;
                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento GPYR!!!");
                                }
                                break;
                            default:
                                System.out.println("No se encuentra TipoTramite mapeado como: " + fTipoTramite + "  en perfil Especialista del departamento GPYR!!!");
                        }
                        break;
                }
                break;

            case "Seguros":
                switch (perfil) {
                    case "Especialista":
                        switch (fTipoTramite) {
                            case "Reclamo":
                                switch (fTipologia) {
                                    case "CobrosIndebidos":
                                        switch (fMotivos) {
                                            case "PenalidadIncumplimientoPago":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                                WebElement btnPenalidadIncumplimientoPago = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(1) > label", "", "", "");
                                                btnPenalidadIncumplimientoPago.click();
                                                break;
                                            case "ComisionMembresia":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                                WebElement btnComisionMembresia = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(2) > label", "", "", "");
                                                btnComisionMembresia.click();
                                                break;
                                            case "InteresCreditoNormalFinanciar":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                                WebElement btnInteresCreditoNormalFinanciar = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(3) > label", "", "", "");
                                                btnInteresCreditoNormalFinanciar.click();
                                                break;
                                            case "InteresDisposicionEfectivo":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                                WebElement btnInteresDisposicionEfectivo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(4) > label", "", "", "");
                                                btnInteresDisposicionEfectivo.click();
                                                break;
                                            case "ComisionUsoCanales":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                                WebElement btnComisionUsoCanales = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(5) > label", "", "", "");
                                                btnComisionUsoCanales.click();
                                                break;
                                            case "ComisionEnvioFisicoEECC":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                                WebElement btnComisionEnvioFisicoEECC = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(6) > label", "", "", "");
                                                btnComisionEnvioFisicoEECC.click();
                                                break;
                                            case "ComisionReposicionPlastico":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                                WebElement btnComisionReposicionPlastico = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(7) > label", "", "", "");
                                                btnComisionReposicionPlastico.click();
                                                break;
                                            case "SeguroDesgravamen":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                                WebElement btnSeguroDesgravamen = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(8) > label", "", "", "");
                                                btnSeguroDesgravamen.click();
                                                break;
                                            case "TipoCambio":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                                WebElement btnTipoCambio = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(9) > label", "", "", "");
                                                btnTipoCambio.click();
                                                break;
                                            case "InteresMoratorio":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                                WebElement btnInteresMoratorio = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(10) > label", "", "", "");
                                                btnInteresMoratorio.click();
                                                break;

                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento SEGUROS!!!");
                                        }
                                        break;
                                    case "ConsumosNoReconocidos":
                                        switch (fMotivos) {
                                            case "FraudeMediosVirtuales":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                                WebElement btnFraudeMediosVirtuales = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(1) > label", "", "", "");
                                                btnFraudeMediosVirtuales.click();
                                                break;
                                            case "FraudeMediosFisicos":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                                WebElement btnFraudeMediosFisicos = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(2) > label", "", "", "");
                                                btnFraudeMediosFisicos.click();
                                                break;

                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento SEGUROS!!!");
                                        }
                                        break;

                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento SEGUROS!!!");
                                }
                                break;

                            case "Pedido":
                                switch (fTipologia) {
                                    case "ExoneracionCobros":
                                        switch (fMotivos) {
                                            case "PenalidadIncumplimientoPago":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                                WebElement btnPenalidadIncumplimientoPago = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(1) > label", "", "", "");
                                                btnPenalidadIncumplimientoPago.click();
                                                break;
                                            case "ComisionMembresia":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                                WebElement btnComisionMembresia = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(2) > label", "", "", "");
                                                btnComisionMembresia.click();
                                                break;
                                            case "InteresCreditoNormalFinanciar":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                                WebElement btnInteresCreditoNormalFinanciar = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(3) > label", "", "", "");
                                                btnInteresCreditoNormalFinanciar.click();
                                                break;
                                            case "InteresDisposicionEfectivo":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                                WebElement btnInteresDisposicionEfectivo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(4) > label", "", "", "");
                                                btnInteresDisposicionEfectivo.click();
                                                break;
                                            case "ComisionUsoCanales":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                                WebElement btnComisionUsoCanales = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(5) > label", "", "", "");
                                                btnComisionUsoCanales.click();
                                                break;
                                            case "ComisionEnvioFisicoEECC":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                                WebElement btnComisionEnvioFisicoEECC = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(6) > label", "", "", "");
                                                btnComisionEnvioFisicoEECC.click();
                                                break;
                                            case "ComisionReposicionPlastico":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                                WebElement btnComisionReposicionPlastico = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(7) > label", "", "", "");
                                                btnComisionReposicionPlastico.click();
                                                break;
                                            case "SeguroDesgravamen":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                                WebElement btnSeguroDesgravamen = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(8) > label", "", "", "");
                                                btnSeguroDesgravamen.click();
                                                break;
                                            case "ComisionRetiroATM":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                                WebElement btnComisionRetiroATMBancoLocalExtranjero = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(9) > label", "", "", "");
                                                btnComisionRetiroATMBancoLocalExtranjero.click();
                                                break;
                                            case "InteresMoratorio":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS));
                                                WebElement btnInteresMoratorio = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_SEGUROS, "div > div:nth-child(10) > label", "", "", "");
                                                btnInteresMoratorio.click();
                                                break;

                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento SEGUROS!!!");
                                        }
                                        break;

                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento SEGUROS!!!");
                                }
                                break;

                            default:
                                System.out.println("No se encuentra TipoTramite mapeado como: " + fTipoTramite + "  en perfil Especialista del departamento SEGUROS!!!");
                        }
                        break;
                    case "Supervisor":
                        switch (fTipoTramite) {
                            case "Reclamo":
                                switch (fTipologia) {
                                    case "CobrosIndebidos":
                                        switch (fMotivos) {
                                            case "PenalidadIncumplimientoPago":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                                WebElement btnPenalidadIncumplimientoPago = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(1) > label", "", "", "");
                                                btnPenalidadIncumplimientoPago.click();
                                                break;
                                            case "ComisionMembresia":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                                WebElement btnComisionMembresia = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(2) > label", "", "", "");
                                                btnComisionMembresia.click();
                                                break;
                                            case "InteresCreditoNormalFinanciar":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                                WebElement btnInteresCreditoNormalFinanciar = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(3) > label", "", "", "");
                                                btnInteresCreditoNormalFinanciar.click();
                                                break;
                                            case "InteresDisposicionEfectivo":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                                WebElement btnInteresDisposicionEfectivo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(4) > label", "", "", "");
                                                btnInteresDisposicionEfectivo.click();
                                                break;
                                            case "ComisionUsoCanales":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                                WebElement btnComisionUsoCanales = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(5) > label", "", "", "");
                                                btnComisionUsoCanales.click();
                                                break;
                                            case "ComisionEnvioFisicoEECC":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                                WebElement btnComisionEnvioFisicoEECC = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(6) > label", "", "", "");
                                                btnComisionEnvioFisicoEECC.click();
                                                break;
                                            case "ComisionReposicionPlastico":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                                WebElement btnComisionReposicionPlastico = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(7) > label", "", "", "");
                                                btnComisionReposicionPlastico.click();
                                                break;
                                            case "SeguroDesgravamen":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                                WebElement btnSeguroDesgravamen = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(8) > label", "", "", "");
                                                btnSeguroDesgravamen.click();
                                                break;
                                            case "TipoCambio":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                                WebElement btnTipoCambio = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(9) > label", "", "", "");
                                                btnTipoCambio.click();
                                                break;
                                            case "InteresMoratorio":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                                WebElement btnInteresMoratorio = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(10) > label", "", "", "");
                                                btnInteresMoratorio.click();
                                                break;

                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento GPYR!!!");
                                        }
                                        break;
                                    case "ConsumosNoReconocidos":
                                        switch (fMotivos) {
                                            case "FraudeMediosVirtuales":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                                WebElement btnFraudeMediosVirtuales = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(1) > label", "", "", "");
                                                btnFraudeMediosVirtuales.click();
                                                break;
                                            case "FraudeMediosFisicos":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                                WebElement btnFraudeMediosFisicos = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(2) > label", "", "", "");
                                                btnFraudeMediosFisicos.click();
                                                break;

                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento GPYR!!!");
                                        }
                                        break;

                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento SEGUROS!!!");
                                }
                                break;

                            case "Pedido":
                                switch (fTipologia) {
                                    case "ExoneracionCobros":
                                        switch (fMotivos) {
                                            case "PenalidadIncumplimientoPago":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                                WebElement btnPenalidadIncumplimientoPago = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(1) > label", "", "", "");
                                                btnPenalidadIncumplimientoPago.click();
                                                break;
                                            case "ComisionMembresia":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                                WebElement btnComisionMembresia = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(2) > label", "", "", "");
                                                btnComisionMembresia.click();
                                                break;
                                            case "InteresCreditoNormalFinanciar":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                                WebElement btnInteresCreditoNormalFinanciar = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(3) > label", "", "", "");
                                                btnInteresCreditoNormalFinanciar.click();
                                                break;
                                            case "InteresDisposicionEfectivo":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                                WebElement btnInteresDisposicionEfectivo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(4) > label", "", "", "");
                                                btnInteresDisposicionEfectivo.click();
                                                break;
                                            case "ComisionUsoCanales":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                                WebElement btnComisionUsoCanales = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(5) > label", "", "", "");
                                                btnComisionUsoCanales.click();
                                                break;
                                            case "ComisionEnvioFisicoEECC":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                                WebElement btnComisionEnvioFisicoEECC = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(6) > label", "", "", "");
                                                btnComisionEnvioFisicoEECC.click();
                                                break;
                                            case "ComisionReposicionPlastico":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                                WebElement btnComisionReposicionPlastico = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(7) > label", "", "", "");
                                                btnComisionReposicionPlastico.click();
                                                break;
                                            case "SeguroDesgravamen":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                                WebElement btnSeguroDesgravamen = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(8) > label", "", "", "");
                                                btnSeguroDesgravamen.click();
                                                break;
                                            case "ComisionRetiroATM":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                                WebElement btnComisionRetiroATMBancoLocalExtranjero = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(9) > label", "", "", "");
                                                btnComisionRetiroATMBancoLocalExtranjero.click();
                                                break;
                                            case "InteresMoratorio":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS));
                                                WebElement btnInteresMoratorio = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_SEGUROS, "div > div:nth-child(10) > label", "", "", "");
                                                btnInteresMoratorio.click();
                                                break;

                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento GPYR!!!");
                                        }
                                        break;

                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento GPYR!!!");
                                }
                                break;

                            default:
                                System.out.println("No se encuentra TipoTramite mapeado como: " + fTipoTramite + "  en perfil Especialista del departamento GPYR!!!");
                        }
                        break;
                }
                break;

            case "Controversias":
                switch (perfil) {
                    case "Especialista":
                        switch (fTipoTramite) {
                            case "Reclamo":
                                switch (fTipologia) {
                                    case "ConsumosNoReconocidos":
                                        switch (fMotivos) {
                                            case "FraudeMediosVirtuales":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_CONTROVERSIAS));
                                                WebElement btnFraudeMediosVirtuales = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_CONTROVERSIAS, "div > div:nth-child(1) > label", "", "", "");
                                                btnFraudeMediosVirtuales.click();
                                                break;
                                            case "FraudeMediosFisicos":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_CONTROVERSIAS));
                                                WebElement btnFraudeMediosFisicos = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_CONTROVERSIAS, "div > div:nth-child(2) > label", "", "", "");
                                                btnFraudeMediosFisicos.click();
                                                break;

                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento CONTROVERSIAS!!!");
                                        }
                                        break;

                                    case "ConsumosMalProcesados":
                                        switch (fMotivos) {
                                            case "ConsumoCargoMasUnaVezDuplico":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_CONTROVERSIAS));
                                                WebElement btnConsumoCargoMasUnaVezDuplico = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_CONTROVERSIAS, "div > div:nth-child(1) > label", "", "", "");
                                                btnConsumoCargoMasUnaVezDuplico.click();
                                                break;
                                            case "CargoConsumoNoConcretadoDesistioCompra":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_CONTROVERSIAS));
                                                WebElement btnCargoConsumoNoConcretadoDesistioCompra = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_CONTROVERSIAS, "div > div:nth-child(2) > label", "", "", "");
                                                btnCargoConsumoNoConcretadoDesistioCompra.click();
                                                break;
                                            case "CargoConsumoNoConcretadoPagoOtraTarjetaEfectivo":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_CONTROVERSIAS));
                                                WebElement btnCargoConsumoNoConcretadoPagoOtraTarjetaEfectivo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_CONTROVERSIAS, "div > div:nth-child(3) > label", "", "", "");
                                                btnCargoConsumoNoConcretadoPagoOtraTarjetaEfectivo.click();
                                                break;
                                            case "AnuloConsumoDevolvioDinero":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_CONTROVERSIAS));
                                                WebElement btnAnuloConsumoDevolvioDinero = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_CONTROVERSIAS, "div > div:nth-child(4) > label", "", "", "");
                                                btnAnuloConsumoDevolvioDinero.click();
                                                break;
                                            case "ProductoServicioRecibioAcordeOfrecido":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_CONTROVERSIAS));
                                                WebElement btnProductoServicioRecibioAcordeOfrecido = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_CONTROVERSIAS, "div > div:nth-child(5) > label", "", "", "");
                                                btnProductoServicioRecibioAcordeOfrecido.click();
                                                break;
                                            case "ImporteDifiereOriginalMismaMoneda":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_CONTROVERSIAS));
                                                WebElement btnImporteDifiereOriginalMismaMoneda = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_CONTROVERSIAS, "div > div:nth-child(6) > label", "", "", "");
                                                btnImporteDifiereOriginalMismaMoneda.click();
                                                break;

                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento CONTROVERSIAS!!!");
                                        }
                                        break;

                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento CONTROVERSIAS!!!");
                                }
                                break;
                            default:
                                System.out.println("No se encuentra TipoTramite mapeado como: " + fTipoTramite + "  en perfil Especialista del departamento CONTROVERSIAS!!!");
                        }
                        break;
                    case "Supervisor":
                        switch (fTipoTramite) {
                            case "Reclamo":
                                switch (fTipologia) {
                                    case "ConsumosNoReconocidos":
                                        switch (fMotivos) {
                                            case "FraudeMediosVirtuales":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_CONTROVERSIAS));
                                                WebElement btnFraudeMediosVirtuales = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_CONTROVERSIAS, "div > div:nth-child(1) > label", "", "", "");
                                                btnFraudeMediosVirtuales.click();
                                                break;
                                            case "FraudeMediosFisicos":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_CONTROVERSIAS));
                                                WebElement btnFraudeMediosFisicos = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_CONTROVERSIAS, "div > div:nth-child(2) > label", "", "", "");
                                                btnFraudeMediosFisicos.click();
                                                break;

                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento GPYR!!!");
                                        }
                                        break;

                                    case "ConsumosMalProcesados":
                                        switch (fMotivos) {
                                            case "ConsumoCargoMasUnaVezDuplico":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_CONTROVERSIAS));
                                                WebElement btnConsumoCargoMasUnaVezDuplico = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_CONTROVERSIAS, "div > div:nth-child(1) > label", "", "", "");
                                                btnConsumoCargoMasUnaVezDuplico.click();
                                                break;
                                            case "CargoConsumoNoConcretadoDesistioCompra":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_CONTROVERSIAS));
                                                WebElement btnCargoConsumoNoConcretadoDesistioCompra = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_CONTROVERSIAS, "div > div:nth-child(2) > label", "", "", "");
                                                btnCargoConsumoNoConcretadoDesistioCompra.click();
                                                break;
                                            case "CargoConsumoNoConcretadoPagoOtraTarjetaEfectivo":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_CONTROVERSIAS));
                                                WebElement btnCargoConsumoNoConcretadoPagoOtraTarjetaEfectivo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_CONTROVERSIAS, "div > div:nth-child(3) > label", "", "", "");
                                                btnCargoConsumoNoConcretadoPagoOtraTarjetaEfectivo.click();
                                                break;
                                            case "AnuloConsumoDevolvioDinero":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_CONTROVERSIAS));
                                                WebElement btnAnuloConsumoDevolvioDinero = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_CONTROVERSIAS, "div > div:nth-child(4) > label", "", "", "");
                                                btnAnuloConsumoDevolvioDinero.click();
                                                break;
                                            case "ProductoServicioRecibioAcordeOfrecido":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_CONTROVERSIAS));
                                                WebElement btnProductoServicioRecibioAcordeOfrecido = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_CONTROVERSIAS, "div > div:nth-child(5) > label", "", "", "");
                                                btnProductoServicioRecibioAcordeOfrecido.click();
                                                break;
                                            case "ImporteDifiereOriginalMismaMoneda":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_CONTROVERSIAS));
                                                WebElement btnImporteDifiereOriginalMismaMoneda = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_CONTROVERSIAS, "div > div:nth-child(6) > label", "", "", "");
                                                btnImporteDifiereOriginalMismaMoneda.click();
                                                break;

                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento GPYR!!!");
                                        }
                                        break;

                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento CONTROVERSIAS!!!");
                                }
                                break;
                            default:
                                System.out.println("No se encuentra TipoTramite mapeado como: " + fTipoTramite + "  en perfil Especialista del departamento GPYR!!!");
                        }
                        break;
                }
                break;

            case "USPR":
                switch (perfil) {
                    case "Especialista":
                        switch (fTipoTramite) {
                            case "Reclamo":
                                switch (fTipologia) {
                                    case "CobrosIndebidos":
                                        switch (fMotivos) {
                                            case "PenalidadIncumplimientoPago":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnPenalidadIncumplimientoPago = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(1) > label", "", "", "");
                                                btnPenalidadIncumplimientoPago.click();
                                                break;
                                            case "ComisionMembresia":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnComisionMembresia = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(2) > label", "", "", "");
                                                btnComisionMembresia.click();
                                                break;
                                            case "InteresCreditoNormalFinanciar":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnInteresCreditoNormalFinanciar = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(3) > label", "", "", "");
                                                btnInteresCreditoNormalFinanciar.click();
                                                break;
                                            case "InteresDisposicionEfectivo":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnInteresDisposicionEfectivo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(4) > label", "", "", "");
                                                btnInteresDisposicionEfectivo.click();
                                                break;
                                            case "ComisionUsoCanales":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnComisionUsoCanales = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(5) > label", "", "", "");
                                                btnComisionUsoCanales.click();
                                                break;
                                            case "ComisionEnvioFisicoEECC":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnComisionEnvioFisicoEECC = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(6) > label", "", "", "");
                                                btnComisionEnvioFisicoEECC.click();
                                                break;
                                            case "ComisionReposicionPlastico":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnComisionReposicionPlastico = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(7) > label", "", "", "");
                                                btnComisionReposicionPlastico.click();
                                                break;
                                            case "SeguroDesgravamen":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnSeguroDesgravamen = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(8) > label", "", "", "");
                                                btnSeguroDesgravamen.click();
                                                break;
                                            case "TipoCambio":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnTipoCambio = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(9) > label", "", "", "");
                                                btnTipoCambio.click();
                                                break;
                                            case "InteresMoratorio":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnInteresMoratorio = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(10) > label", "", "", "");
                                                btnInteresMoratorio.click();
                                                break;

                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento USPR!!!");
                                        }
                                        break;

                                    case "ConsumosNoReconocidos":
                                        switch (fMotivos) {
                                            case "FraudeMediosVirtuales":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnFraudeMediosVirtuales = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(1) > label", "", "", "");
                                                btnFraudeMediosVirtuales.click();
                                                break;
                                            case "FraudeMediosFisicos":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnFraudeMediosFisicos = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(2) > label", "", "", "");
                                                btnFraudeMediosFisicos.click();
                                                break;

                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento USPR!!!");
                                        }
                                        break;

                                    case "ConsumosMalProcesados":
                                        switch (fMotivos) {
                                            case "ConsumoCargoMasUnaVezDuplico":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnConsumoCargoMasUnaVezDuplico = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(1) > label", "", "", "");
                                                btnConsumoCargoMasUnaVezDuplico.click();
                                                break;
                                            case "CargoConsumoNoConcretadoDesistioCompra":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnCargoConsumoNoConcretadoDesistioCompra = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(2) > label", "", "", "");
                                                btnCargoConsumoNoConcretadoDesistioCompra.click();
                                                break;
                                            case "CargoConsumoNoConcretadoPagoOtraTarjetaEfectivo":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnCargoConsumoNoConcretadoPagoOtraTarjetaEfectivo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(3) > label", "", "", "");
                                                btnCargoConsumoNoConcretadoPagoOtraTarjetaEfectivo.click();
                                                break;
                                            case "AnuloConsumoDevolvioDinero":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnAnuloConsumoDevolvioDinero = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(4) > label", "", "", "");
                                                btnAnuloConsumoDevolvioDinero.click();
                                                break;
                                            case "ProductoServicioRecibioAcordeOfrecido":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnProductoServicioRecibioAcordeOfrecido = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(5) > label", "", "", "");
                                                btnProductoServicioRecibioAcordeOfrecido.click();
                                                break;
                                            case "ImporteDifiereOriginalMismaMoneda":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnImporteDifiereOriginalMismaMoneda = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(6) > label", "", "", "");
                                                btnImporteDifiereOriginalMismaMoneda.click();
                                                break;

                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento USPR!!!");
                                        }
                                        break;

                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento USPR!!!");
                                }
                                break;

                            case "Pedido":
                                switch (fTipologia) {
                                    case "ExoneracionCobros":
                                        switch (fMotivos) {
                                            case "PenalidadIncumplimientoPago":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnPenalidadIncumplimientoPago = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(1) > label", "", "", "");
                                                btnPenalidadIncumplimientoPago.click();
                                                break;
                                            case "ComisionMembresia":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnComisionMembresia = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(2) > label", "", "", "");
                                                btnComisionMembresia.click();
                                                break;
                                            case "InteresCreditoNormalFinanciar":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnInteresCreditoNormalFinanciar = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(3) > label", "", "", "");
                                                btnInteresCreditoNormalFinanciar.click();
                                                break;
                                            case "InteresDisposicionEfectivo":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnInteresDisposicionEfectivo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(4) > label", "", "", "");
                                                btnInteresDisposicionEfectivo.click();
                                                break;
                                            case "ComisionUsoCanales":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnComisionUsoCanales = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(5) > label", "", "", "");
                                                btnComisionUsoCanales.click();
                                                break;
                                            case "ComisionEnvioFisicoEECC":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnComisionEnvioFisicoEECC = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(6) > label", "", "", "");
                                                btnComisionEnvioFisicoEECC.click();
                                                break;
                                            case "ComisionReposicionPlastico":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnComisionReposicionPlastico = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(7) > label", "", "", "");
                                                btnComisionReposicionPlastico.click();
                                                break;
                                            case "SeguroDesgravamen":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnSeguroDesgravamen = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(8) > label", "", "", "");
                                                btnSeguroDesgravamen.click();
                                                break;
                                            case "ComisionRetiroATM":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnComisionRetiroATMBancoLocalExtranjero = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(9) > label", "", "", "");
                                                btnComisionRetiroATMBancoLocalExtranjero.click();
                                                break;
                                            case "InteresMoratorio":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR));
                                                WebElement btnInteresMoratorio = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_ESPECIALISTA_USPR, "div > div:nth-child(10) > label", "", "", "");
                                                btnInteresMoratorio.click();
                                                break;

                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento USPR!!!");
                                        }
                                        break;

                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento USPR!!!");
                                }
                                break;

                            default:
                                System.out.println("No se encuentra TipoTramite mapeado como: " + fTipoTramite + "  en perfil Especialista del departamento USPR!!!");
                        }
                        break;
                    case "Supervisor":
                        switch (fTipoTramite) {

                            case "Reclamo":
                                switch (fTipologia) {
                                    case "CobrosIndebidos":
                                        switch (fMotivos) {
                                            case "PenalidadIncumplimientoPago":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnPenalidadIncumplimientoPago = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(1) > label", "", "", "");
                                                btnPenalidadIncumplimientoPago.click();
                                                break;
                                            case "ComisionMembresia":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnComisionMembresia = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(2) > label", "", "", "");
                                                btnComisionMembresia.click();
                                                break;
                                            case "InteresCreditoNormalFinanciar":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnInteresCreditoNormalFinanciar = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(3) > label", "", "", "");
                                                btnInteresCreditoNormalFinanciar.click();
                                                break;
                                            case "InteresDisposicionEfectivo":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnInteresDisposicionEfectivo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(4) > label", "", "", "");
                                                btnInteresDisposicionEfectivo.click();
                                                break;
                                            case "ComisionUsoCanales":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnComisionUsoCanales = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(5) > label", "", "", "");
                                                btnComisionUsoCanales.click();
                                                break;
                                            case "ComisionEnvioFisicoEECC":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnComisionEnvioFisicoEECC = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(6) > label", "", "", "");
                                                btnComisionEnvioFisicoEECC.click();
                                                break;
                                            case "ComisionReposicionPlastico":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnComisionReposicionPlastico = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(7) > label", "", "", "");
                                                btnComisionReposicionPlastico.click();
                                                break;
                                            case "SeguroDesgravamen":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnSeguroDesgravamen = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(8) > label", "", "", "");
                                                btnSeguroDesgravamen.click();
                                                break;
                                            case "TipoCambio":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnTipoCambio = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(9) > label", "", "", "");
                                                btnTipoCambio.click();
                                                break;
                                            case "InteresMoratorio":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnInteresMoratorio = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(10) > label", "", "", "");
                                                btnInteresMoratorio.click();
                                                break;

                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento USPR!!!");
                                        }
                                        break;

                                    case "ConsumosNoReconocidos":
                                        switch (fMotivos) {
                                            case "FraudeMediosVirtuales":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnFraudeMediosVirtuales = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(1) > label", "", "", "");
                                                btnFraudeMediosVirtuales.click();
                                                break;
                                            case "FraudeMediosFisicos":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnFraudeMediosFisicos = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(2) > label", "", "", "");
                                                btnFraudeMediosFisicos.click();
                                                break;

                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento USPR!!!");
                                        }
                                        break;

                                    case "ConsumosMalProcesados":
                                        switch (fMotivos) {
                                            case "ConsumoCargoMasUnaVezDuplico":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnConsumoCargoMasUnaVezDuplico = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(1) > label", "", "", "");
                                                btnConsumoCargoMasUnaVezDuplico.click();
                                                break;
                                            case "CargoConsumoNoConcretadoDesistioCompra":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnCargoConsumoNoConcretadoDesistioCompra = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(2) > label", "", "", "");
                                                btnCargoConsumoNoConcretadoDesistioCompra.click();
                                                break;
                                            case "CargoConsumoNoConcretadoPagoOtraTarjetaEfectivo":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnCargoConsumoNoConcretadoPagoOtraTarjetaEfectivo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(3) > label", "", "", "");
                                                btnCargoConsumoNoConcretadoPagoOtraTarjetaEfectivo.click();
                                                break;
                                            case "AnuloConsumoDevolvioDinero":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnAnuloConsumoDevolvioDinero = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(4) > label", "", "", "");
                                                btnAnuloConsumoDevolvioDinero.click();
                                                break;
                                            case "ProductoServicioRecibioAcordeOfrecido":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnProductoServicioRecibioAcordeOfrecido = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(5) > label", "", "", "");
                                                btnProductoServicioRecibioAcordeOfrecido.click();
                                                break;
                                            case "ImporteDifiereOriginalMismaMoneda":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnImporteDifiereOriginalMismaMoneda = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(6) > label", "", "", "");
                                                btnImporteDifiereOriginalMismaMoneda.click();
                                                break;

                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento USPR!!!");
                                        }
                                        break;

                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento USPR!!!");
                                }
                                break;

                            case "Pedido":
                                switch (fTipologia) {
                                    case "ExoneracionCobros":
                                        switch (fMotivos) {
                                            case "PenalidadIncumplimientoPago":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnPenalidadIncumplimientoPago = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(1) > label", "", "", "");
                                                btnPenalidadIncumplimientoPago.click();
                                                break;
                                            case "ComisionMembresia":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnComisionMembresia = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(2) > label", "", "", "");
                                                btnComisionMembresia.click();
                                                break;
                                            case "InteresCreditoNormalFinanciar":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnInteresCreditoNormalFinanciar = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(3) > label", "", "", "");
                                                btnInteresCreditoNormalFinanciar.click();
                                                break;
                                            case "InteresDisposicionEfectivo":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnInteresDisposicionEfectivo = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(4) > label", "", "", "");
                                                btnInteresDisposicionEfectivo.click();
                                                break;
                                            case "ComisionUsoCanales":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnComisionUsoCanales = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(5) > label", "", "", "");
                                                btnComisionUsoCanales.click();
                                                break;
                                            case "ComisionEnvioFisicoEECC":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnComisionEnvioFisicoEECC = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(6) > label", "", "", "");
                                                btnComisionEnvioFisicoEECC.click();
                                                break;
                                            case "ComisionReposicionPlastico":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnComisionReposicionPlastico = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(7) > label", "", "", "");
                                                btnComisionReposicionPlastico.click();
                                                break;
                                            case "SeguroDesgravamen":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnSeguroDesgravamen = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(8) > label", "", "", "");
                                                btnSeguroDesgravamen.click();
                                                break;
                                            case "ComisionRetiroATM":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnComisionRetiroATMBancoLocalExtranjero = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(9) > label", "", "", "");
                                                btnComisionRetiroATMBancoLocalExtranjero.click();
                                                break;
                                            case "InteresMoratorio":
                                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR));
                                                WebElement btnInteresMoratorio = general.expandContainer("1css", LBL_ID_RADIOBUTTOM_SUPERVISOR_USPR, "div > div:nth-child(10) > label", "", "", "");
                                                btnInteresMoratorio.click();
                                                break;

                                            default:
                                                System.out.println("No se encuentra Motivo mapeado como: " + fMotivos + "  en perfil Especialista del departamento USPR!!!");
                                        }
                                        break;

                                    default:
                                        System.out.println("No se encuentra Tipologia mapeado como: " + fTipologia + "  en perfil Especialista del departamento USPR!!!");
                                }
                                break;

                            default:
                                System.out.println("No se encuentra TipoTramite mapeado como: " + fTipoTramite + "  en perfil Especialista del departamento USPR!!!");
                        }
                        break;
                }

                break;
            default:
                System.out.println("No existe filtro TipoDeTramite en el departamento ingresado: " + departamento + " !!!");
        }

    }


}
