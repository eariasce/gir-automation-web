package com.everis.giru.userinterface.tramites.validaFiltros;

import com.everis.giru.utils.Shadow;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.WebElement;

public class ValidaFiltroMonedaMontoPage extends PageObject {

    Shadow general = new Shadow();

    public int contador = 0;

    //MAPEO DE ELEMENTOS - VALIDACIÓN FILTROS
    @FindBy(xpath = "//*[@id='grid']/giru-specialist-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR;

    @FindBy(xpath = "//*[@id='grid']/giru-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SUPERVISOR_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-my-team/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-to-assigned/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-to-resolve/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR;

    @FindBy(xpath = "//*[@id='grid']/giru-specialist-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ESPECIALISTA_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-specialist-to-monitor/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS;

    @FindBy(xpath = "//*[@id='grid']/giru-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SUPERVISOR_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-my-team/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-assigned/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-resolve/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-monitor/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS;

    @FindBy(xpath = "//*[@id='grid']/giru-specialist-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-specialist-to-monitor/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-specialist-to-transactions-in-process/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS;

    @FindBy(xpath = "//*[@id='grid']/giru-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-my-team/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-assigned/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-resolve/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-monitor/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-supervisor-trx/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS;

    @FindBy(xpath = "//*[@id='grid']/giru-specialist-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-task/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR;

    @FindBy(xpath = "//*[@id='grid']/giru-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SUPERVISOR_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-my-team/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-to-assigned/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-to-resolve/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR;

    //TODO: Validacion de filtro - Moneda y Monto
    public boolean validaFiltroMonedaMonto(String departamento, String perfil, String tipologia, String fMoneda, String fMontoDesde, String fMontoHasta) {

        boolean validaMonedaMonto = false;

        switch (departamento) {

            case "GPYR":
                //TODO: Especialista GPYR
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                switch (fMoneda) {
                                    case "PEN":
                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_GPYR, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoSoles = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(9)", "", "");

                                                    double montoSolesFront = Double.parseDouble(valueMontoSoles.getText());
                                                    System.out.println("Filtro < Monto Soles Front > : " + montoSolesFront);

                                                    double amountMinSoles = Double.parseDouble(fMontoDesde);
                                                    double amountMaxSoles = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Soles > Desde: " + amountMinSoles + " Hasta: " + amountMaxSoles);

                                                    try {
                                                        if ((amountMinSoles >= montoSolesFront) && (amountMaxSoles <= montoSolesFront)) {
                                                            System.out.println("Filtro < Monto Soles Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Soles todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Soles No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;
                                    case "USD":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_GPYR, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoDolares = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(10)", "", "");

                                                    double montoDolaresFront = Double.parseDouble(valueMontoDolares.getText());
                                                    System.out.println("Filtro < Monto Dolares Front > : " + montoDolaresFront);

                                                    double amountMinDolares = Double.parseDouble(fMontoDesde);
                                                    double amountMaxDolares = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Dolares > Desde: " + amountMinDolares + " Hasta: " + amountMaxDolares);

                                                    try {
                                                        if ((amountMinDolares >= montoDolaresFront) && (amountMaxDolares <= montoDolaresFront)) {
                                                            System.out.println("Filtro < Monto Dolares Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Dolares todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Dolares No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;

                                    default:
                                        System.out.println("No se encuentra ningún tipo de moneda !!!");
                                }

                                break;
                            case "CobrosIndebidos":
                                switch (fMoneda) {
                                    case "PEN":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoSoles = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(9)", "", "");

                                                    double montoSolesFront = Double.parseDouble(valueMontoSoles.getText());
                                                    System.out.println("Filtro < Monto Soles Front > : " + montoSolesFront);

                                                    double amountMinSoles = Double.parseDouble(fMontoDesde);
                                                    double amountMaxSoles = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Soles > Desde: " + amountMinSoles + " Hasta: " + amountMaxSoles);

                                                    try {
                                                        if ((amountMinSoles >= montoSolesFront) && (amountMaxSoles <= montoSolesFront)) {
                                                            System.out.println("Filtro < Monto Soles Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Soles todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Soles No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;
                                    case "USD":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoDolares = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(10)", "", "");

                                                    double montoDolaresFront = Double.parseDouble(valueMontoDolares.getText());
                                                    System.out.println("Filtro < Monto Dolares Front > : " + montoDolaresFront);

                                                    double amountMinDolares = Double.parseDouble(fMontoDesde);
                                                    double amountMaxDolares = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Dolares > Desde: " + amountMinDolares + " Hasta: " + amountMaxDolares);

                                                    try {
                                                        if ((amountMinDolares >= montoDolaresFront) && (amountMaxDolares <= montoDolaresFront)) {
                                                            System.out.println("Filtro < Monto Dolares Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Dolares todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Dolares No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;

                                    default:
                                        System.out.println("No se encuentra ningún tipo de moneda !!!");
                                }

                                break;
                            case "ExoneracionCobros":
                                switch (fMoneda) {
                                    case "PEN":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoSoles = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(9)", "", "");

                                                    double montoSolesFront = Double.parseDouble(valueMontoSoles.getText());
                                                    System.out.println("Filtro < Monto Soles Front > : " + montoSolesFront);

                                                    double amountMinSoles = Double.parseDouble(fMontoDesde);
                                                    double amountMaxSoles = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Soles > Desde: " + amountMinSoles + " Hasta: " + amountMaxSoles);

                                                    try {
                                                        if ((amountMinSoles >= montoSolesFront) && (amountMaxSoles <= montoSolesFront)) {
                                                            System.out.println("Filtro < Monto Soles Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Soles todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Soles No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;
                                    case "USD":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoDolares = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(10)", "", "");

                                                    double montoDolaresFront = Double.parseDouble(valueMontoDolares.getText());
                                                    System.out.println("Filtro < Monto Dolares Front > : " + montoDolaresFront);

                                                    double amountMinDolares = Double.parseDouble(fMontoDesde);
                                                    double amountMaxDolares = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Dolares > Desde: " + amountMinDolares + " Hasta: " + amountMaxDolares);

                                                    try {
                                                        if ((amountMinDolares >= montoDolaresFront) && (amountMaxDolares <= montoDolaresFront)) {
                                                            System.out.println("Filtro < Monto Dolares Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Dolares todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Dolares No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;

                                    default:
                                        System.out.println("No se encuentra ningún tipo de moneda !!!");
                                }

                                break;
                            case "AnulacionTC":
                                switch (fMoneda) {
                                    case "PEN":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoSoles = general.expandContainer("2css", LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(9)", "", "");

                                                    double montoSolesFront = Double.parseDouble(valueMontoSoles.getText());
                                                    System.out.println("Filtro < Monto Soles Front > : " + montoSolesFront);

                                                    double amountMinSoles = Double.parseDouble(fMontoDesde);
                                                    double amountMaxSoles = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Soles > Desde: " + amountMinSoles + " Hasta: " + amountMaxSoles);

                                                    try {
                                                        if ((amountMinSoles >= montoSolesFront) && (amountMaxSoles <= montoSolesFront)) {
                                                            System.out.println("Filtro < Monto Soles Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Soles todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Soles No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;
                                    case "USD":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoDolares = general.expandContainer("2css", LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(10)", "", "");

                                                    double montoDolaresFront = Double.parseDouble(valueMontoDolares.getText());
                                                    System.out.println("Filtro < Monto Dolares Front > : " + montoDolaresFront);

                                                    double amountMinDolares = Double.parseDouble(fMontoDesde);
                                                    double amountMaxDolares = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Dolares > Desde: " + amountMinDolares + " Hasta: " + amountMaxDolares);

                                                    try {
                                                        if ((amountMinDolares >= montoDolaresFront) && (amountMaxDolares <= montoDolaresFront)) {
                                                            System.out.println("Filtro < Monto Dolares Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Dolares todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Dolares No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;

                                    default:
                                        System.out.println("No se encuentra ningún tipo de moneda !!!");
                                }

                                break;
                            case "FinalizacionTC":
                                switch (fMoneda) {
                                    case "PEN":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoSoles = general.expandContainer("2css", LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(9)", "", "");

                                                    double montoSolesFront = Double.parseDouble(valueMontoSoles.getText());
                                                    System.out.println("Filtro < Monto Soles Front > : " + montoSolesFront);

                                                    double amountMinSoles = Double.parseDouble(fMontoDesde);
                                                    double amountMaxSoles = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Soles > Desde: " + amountMinSoles + " Hasta: " + amountMaxSoles);

                                                    try {
                                                        if ((amountMinSoles >= montoSolesFront) && (amountMaxSoles <= montoSolesFront)) {
                                                            System.out.println("Filtro < Monto Soles Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Soles todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Soles No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;
                                    case "USD":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoDolares = general.expandContainer("2css", LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(10)", "", "");

                                                    double montoDolaresFront = Double.parseDouble(valueMontoDolares.getText());
                                                    System.out.println("Filtro < Monto Dolares Front > : " + montoDolaresFront);

                                                    double amountMinDolares = Double.parseDouble(fMontoDesde);
                                                    double amountMaxDolares = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Dolares > Desde: " + amountMinDolares + " Hasta: " + amountMaxDolares);

                                                    try {
                                                        if ((amountMinDolares >= montoDolaresFront) && (amountMaxDolares <= montoDolaresFront)) {
                                                            System.out.println("Filtro < Monto Dolares Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Dolares todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Dolares No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;

                                    default:
                                        System.out.println("No se encuentra ningún tipo de moneda !!!");
                                }

                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA VALIDAR FILTRO MONEDA Y MONTO: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;

                    //TODO: Supervisor GPYR
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                switch (fMoneda) {
                                    case "PEN":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_GPYR, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoSoles = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(9)", "", "");

                                                    double montoSolesFront = Double.parseDouble(valueMontoSoles.getText());
                                                    System.out.println("Filtro < Monto Soles Front > : " + montoSolesFront);

                                                    double amountMinSoles = Double.parseDouble(fMontoDesde);
                                                    double amountMaxSoles = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Soles > Desde: " + amountMinSoles + " Hasta: " + amountMaxSoles);

                                                    try {
                                                        if ((amountMinSoles >= montoSolesFront) && (amountMaxSoles <= montoSolesFront)) {
                                                            System.out.println("Filtro < Monto Soles Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Soles todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Soles No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;
                                    case "USD":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_GPYR, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoDolares = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(10)", "", "");

                                                    double montoDolaresFront = Double.parseDouble(valueMontoDolares.getText());
                                                    System.out.println("Filtro < Monto Dolares Front > : " + montoDolaresFront);

                                                    double amountMinDolares = Double.parseDouble(fMontoDesde);
                                                    double amountMaxDolares = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Dolares > Desde: " + amountMinDolares + " Hasta: " + amountMaxDolares);

                                                    try {
                                                        if ((amountMinDolares >= montoDolaresFront) && (amountMaxDolares <= montoDolaresFront)) {
                                                            System.out.println("Filtro < Monto Dolares Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Dolares todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Dolares No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;

                                    default:
                                        System.out.println("No se encuentra ningún tipo de moneda !!!");
                                }

                                break;
                            case "MiEquipo":
                                switch (fMoneda) {
                                    case "PEN":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoSoles = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(10)", "", "");

                                                    double montoSolesFront = Double.parseDouble(valueMontoSoles.getText());
                                                    System.out.println("Filtro < Monto Soles Front > : " + montoSolesFront);

                                                    double amountMinSoles = Double.parseDouble(fMontoDesde);
                                                    double amountMaxSoles = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Soles > Desde: " + amountMinSoles + " Hasta: " + amountMaxSoles);

                                                    try {
                                                        if ((amountMinSoles >= montoSolesFront) && (amountMaxSoles <= montoSolesFront)) {
                                                            System.out.println("Filtro < Monto Soles Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Soles todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Soles No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;
                                    case "USD":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoDolares = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");

                                                    double montoDolaresFront = Double.parseDouble(valueMontoDolares.getText());
                                                    System.out.println("Filtro < Monto Dolares Front > : " + montoDolaresFront);

                                                    double amountMinDolares = Double.parseDouble(fMontoDesde);
                                                    double amountMaxDolares = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Dolares > Desde: " + amountMinDolares + " Hasta: " + amountMaxDolares);

                                                    try {
                                                        if ((amountMinDolares >= montoDolaresFront) && (amountMaxDolares <= montoDolaresFront)) {
                                                            System.out.println("Filtro < Monto Dolares Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Dolares todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Dolares No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;

                                    default:
                                        System.out.println("No se encuentra ningún tipo de moneda !!!");
                                }

                                break;
                            case "Asignar":
                                switch (fMoneda) {
                                    case "PEN":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoSoles = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(10)", "", "");

                                                    double montoSolesFront = Double.parseDouble(valueMontoSoles.getText());
                                                    System.out.println("Filtro < Monto Soles Front > : " + montoSolesFront);

                                                    double amountMinSoles = Double.parseDouble(fMontoDesde);
                                                    double amountMaxSoles = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Soles > Desde: " + amountMinSoles + " Hasta: " + amountMaxSoles);

                                                    try {
                                                        if ((amountMinSoles >= montoSolesFront) && (amountMaxSoles <= montoSolesFront)) {
                                                            System.out.println("Filtro < Monto Soles Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Soles todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Soles No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;
                                    case "USD":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoDolares = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");

                                                    double montoDolaresFront = Double.parseDouble(valueMontoDolares.getText());
                                                    System.out.println("Filtro < Monto Dolares Front > : " + montoDolaresFront);

                                                    double amountMinDolares = Double.parseDouble(fMontoDesde);
                                                    double amountMaxDolares = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Dolares > Desde: " + amountMinDolares + " Hasta: " + amountMaxDolares);

                                                    try {
                                                        if ((amountMinDolares >= montoDolaresFront) && (amountMaxDolares <= montoDolaresFront)) {
                                                            System.out.println("Filtro < Monto Dolares Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Dolares todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Dolares No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;

                                    default:
                                        System.out.println("No se encuentra ningún tipo de moneda !!!");
                                }

                                break;
                            case "Resolver":
                                switch (fMoneda) {
                                    case "PEN":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoSoles = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(10)", "", "");

                                                    double montoSolesFront = Double.parseDouble(valueMontoSoles.getText());
                                                    System.out.println("Filtro < Monto Soles Front > : " + montoSolesFront);

                                                    double amountMinSoles = Double.parseDouble(fMontoDesde);
                                                    double amountMaxSoles = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Soles > Desde: " + amountMinSoles + " Hasta: " + amountMaxSoles);

                                                    try {
                                                        if ((amountMinSoles >= montoSolesFront) && (amountMaxSoles <= montoSolesFront)) {
                                                            System.out.println("Filtro < Monto Soles Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Soles todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Soles No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;
                                    case "USD":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoDolares = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");

                                                    double montoDolaresFront = Double.parseDouble(valueMontoDolares.getText());
                                                    System.out.println("Filtro < Monto Dolares Front > : " + montoDolaresFront);

                                                    double amountMinDolares = Double.parseDouble(fMontoDesde);
                                                    double amountMaxDolares = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Dolares > Desde: " + amountMinDolares + " Hasta: " + amountMaxDolares);

                                                    try {
                                                        if ((amountMinDolares >= montoDolaresFront) && (amountMaxDolares <= montoDolaresFront)) {
                                                            System.out.println("Filtro < Monto Dolares Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Dolares todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Dolares No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;

                                    default:
                                        System.out.println("No se encuentra ningún tipo de moneda !!!");
                                }

                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA VALIDAR FILTRO MONEDA Y MONTO: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                }
                break;

            case "Seguros":

                switch (perfil) {
                    //TODO: Especialista Seguros
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                switch (fMoneda) {
                                    case "PEN":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoSoles = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(9)", "", "");

                                                    double montoSolesFront = Double.parseDouble(valueMontoSoles.getText());
                                                    System.out.println("Filtro < Monto Soles Front > : " + montoSolesFront);

                                                    double amountMinSoles = Double.parseDouble(fMontoDesde);
                                                    double amountMaxSoles = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Soles > Desde: " + amountMinSoles + " Hasta: " + amountMaxSoles);

                                                    try {
                                                        if ((amountMinSoles >= montoSolesFront) && (amountMaxSoles <= montoSolesFront)) {
                                                            System.out.println("Filtro < Monto Soles Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Soles todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Soles No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;
                                    case "USD":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoDolares = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(10)", "", "");

                                                    double montoDolaresFront = Double.parseDouble(valueMontoDolares.getText());
                                                    System.out.println("Filtro < Monto Dolares Front > : " + montoDolaresFront);

                                                    double amountMinDolares = Double.parseDouble(fMontoDesde);
                                                    double amountMaxDolares = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Dolares > Desde: " + amountMinDolares + " Hasta: " + amountMaxDolares);

                                                    try {
                                                        if ((amountMinDolares >= montoDolaresFront) && (amountMaxDolares <= montoDolaresFront)) {
                                                            System.out.println("Filtro < Monto Dolares Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Dolares todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Dolares No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;

                                    default:
                                        System.out.println("No se encuentra ningún tipo de moneda !!!");
                                }

                                break;
                            case "CobrosIndebidos":
                                switch (fMoneda) {
                                    case "PEN":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoSoles = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(9)", "", "");

                                                    double montoSolesFront = Double.parseDouble(valueMontoSoles.getText());
                                                    System.out.println("Filtro < Monto Soles Front > : " + montoSolesFront);

                                                    double amountMinSoles = Double.parseDouble(fMontoDesde);
                                                    double amountMaxSoles = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Soles > Desde: " + amountMinSoles + " Hasta: " + amountMaxSoles);

                                                    try {
                                                        if ((amountMinSoles >= montoSolesFront) && (amountMaxSoles <= montoSolesFront)) {
                                                            System.out.println("Filtro < Monto Soles Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Soles todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Soles No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;
                                    case "USD":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoDolares = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(10)", "", "");

                                                    double montoDolaresFront = Double.parseDouble(valueMontoDolares.getText());
                                                    System.out.println("Filtro < Monto Dolares Front > : " + montoDolaresFront);

                                                    double amountMinDolares = Double.parseDouble(fMontoDesde);
                                                    double amountMaxDolares = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Dolares > Desde: " + amountMinDolares + " Hasta: " + amountMaxDolares);

                                                    try {
                                                        if ((amountMinDolares >= montoDolaresFront) && (amountMaxDolares <= montoDolaresFront)) {
                                                            System.out.println("Filtro < Monto Dolares Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Dolares todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Dolares No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;

                                    default:
                                        System.out.println("No se encuentra ningún tipo de moneda !!!");
                                }

                                break;
                            case "ExoneracionCobros":
                                switch (fMoneda) {
                                    case "PEN":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoSoles = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(9)", "", "");

                                                    double montoSolesFront = Double.parseDouble(valueMontoSoles.getText());
                                                    System.out.println("Filtro < Monto Soles Front > : " + montoSolesFront);

                                                    double amountMinSoles = Double.parseDouble(fMontoDesde);
                                                    double amountMaxSoles = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Soles > Desde: " + amountMinSoles + " Hasta: " + amountMaxSoles);

                                                    try {
                                                        if ((amountMinSoles >= montoSolesFront) && (amountMaxSoles <= montoSolesFront)) {
                                                            System.out.println("Filtro < Monto Soles Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Soles todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Soles No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;
                                    case "USD":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoDolares = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(10)", "", "");

                                                    double montoDolaresFront = Double.parseDouble(valueMontoDolares.getText());
                                                    System.out.println("Filtro < Monto Dolares Front > : " + montoDolaresFront);

                                                    double amountMinDolares = Double.parseDouble(fMontoDesde);
                                                    double amountMaxDolares = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Dolares > Desde: " + amountMinDolares + " Hasta: " + amountMaxDolares);

                                                    try {
                                                        if ((amountMinDolares >= montoDolaresFront) && (amountMaxDolares <= montoDolaresFront)) {
                                                            System.out.println("Filtro < Monto Dolares Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Dolares todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Dolares No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;

                                    default:
                                        System.out.println("No se encuentra ningún tipo de moneda !!!");
                                }

                                break;
                            case "ConsumosNoReconocidos":
                                switch (fMoneda) {
                                    case "PEN":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoSoles = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(9)", "", "");

                                                    double montoSolesFront = Double.parseDouble(valueMontoSoles.getText());
                                                    System.out.println("Filtro < Monto Soles Front > : " + montoSolesFront);

                                                    double amountMinSoles = Double.parseDouble(fMontoDesde);
                                                    double amountMaxSoles = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Soles > Desde: " + amountMinSoles + " Hasta: " + amountMaxSoles);

                                                    try {
                                                        if ((amountMinSoles >= montoSolesFront) && (amountMaxSoles <= montoSolesFront)) {
                                                            System.out.println("Filtro < Monto Soles Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Soles todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Soles No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;
                                    case "USD":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoDolares = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(10)", "", "");

                                                    double montoDolaresFront = Double.parseDouble(valueMontoDolares.getText());
                                                    System.out.println("Filtro < Monto Dolares Front > : " + montoDolaresFront);

                                                    double amountMinDolares = Double.parseDouble(fMontoDesde);
                                                    double amountMaxDolares = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Dolares > Desde: " + amountMinDolares + " Hasta: " + amountMaxDolares);

                                                    try {
                                                        if ((amountMinDolares >= montoDolaresFront) && (amountMaxDolares <= montoDolaresFront)) {
                                                            System.out.println("Filtro < Monto Dolares Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Dolares todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Dolares No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;

                                    default:
                                        System.out.println("No se encuentra ningún tipo de moneda !!!");
                                }

                                break;
                            case "Monitorear":
                                switch (fMoneda) {
                                    case "PEN":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoSoles = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(9)", "", "");

                                                    double montoSolesFront = Double.parseDouble(valueMontoSoles.getText());
                                                    System.out.println("Filtro < Monto Soles Front > : " + montoSolesFront);

                                                    double amountMinSoles = Double.parseDouble(fMontoDesde);
                                                    double amountMaxSoles = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Soles > Desde: " + amountMinSoles + " Hasta: " + amountMaxSoles);

                                                    try {
                                                        if ((amountMinSoles >= montoSolesFront) && (amountMaxSoles <= montoSolesFront)) {
                                                            System.out.println("Filtro < Monto Soles Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Soles todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Soles No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;
                                    case "USD":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoDolares = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(10)", "", "");

                                                    double montoDolaresFront = Double.parseDouble(valueMontoDolares.getText());
                                                    System.out.println("Filtro < Monto Dolares Front > : " + montoDolaresFront);

                                                    double amountMinDolares = Double.parseDouble(fMontoDesde);
                                                    double amountMaxDolares = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Dolares > Desde: " + amountMinDolares + " Hasta: " + amountMaxDolares);

                                                    try {
                                                        if ((amountMinDolares >= montoDolaresFront) && (amountMaxDolares <= montoDolaresFront)) {
                                                            System.out.println("Filtro < Monto Dolares Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Dolares todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Dolares No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;

                                    default:
                                        System.out.println("No se encuentra ningún tipo de moneda !!!");
                                }

                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA VALIDAR FILTRO MONEDA Y MONTO: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;

                    //TODO: Supervisor Seguros
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                switch (fMoneda) {
                                    case "PEN":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoSoles = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(9)", "", "");

                                                    double montoSolesFront = Double.parseDouble(valueMontoSoles.getText());
                                                    System.out.println("Filtro < Monto Soles Front > : " + montoSolesFront);

                                                    double amountMinSoles = Double.parseDouble(fMontoDesde);
                                                    double amountMaxSoles = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Soles > Desde: " + amountMinSoles + " Hasta: " + amountMaxSoles);

                                                    try {
                                                        if ((amountMinSoles >= montoSolesFront) && (amountMaxSoles <= montoSolesFront)) {
                                                            System.out.println("Filtro < Monto Soles Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Soles todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Soles No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;
                                    case "USD":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoDolares = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(10)", "", "");

                                                    double montoDolaresFront = Double.parseDouble(valueMontoDolares.getText());
                                                    System.out.println("Filtro < Monto Dolares Front > : " + montoDolaresFront);

                                                    double amountMinDolares = Double.parseDouble(fMontoDesde);
                                                    double amountMaxDolares = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Dolares > Desde: " + amountMinDolares + " Hasta: " + amountMaxDolares);

                                                    try {
                                                        if ((amountMinDolares >= montoDolaresFront) && (amountMaxDolares <= montoDolaresFront)) {
                                                            System.out.println("Filtro < Monto Dolares Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Dolares todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Dolares No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;

                                    default:
                                        System.out.println("No se encuentra ningún tipo de moneda !!!");
                                }

                                break;
                            case "MiEquipo":
                                switch (fMoneda) {
                                    case "PEN":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoSoles = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(10)", "", "");

                                                    double montoSolesFront = Double.parseDouble(valueMontoSoles.getText());
                                                    System.out.println("Filtro < Monto Soles Front > : " + montoSolesFront);

                                                    double amountMinSoles = Double.parseDouble(fMontoDesde);
                                                    double amountMaxSoles = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Soles > Desde: " + amountMinSoles + " Hasta: " + amountMaxSoles);

                                                    try {
                                                        if ((amountMinSoles >= montoSolesFront) && (amountMaxSoles <= montoSolesFront)) {
                                                            System.out.println("Filtro < Monto Soles Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Soles todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Soles No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;
                                    case "USD":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoDolares = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");

                                                    double montoDolaresFront = Double.parseDouble(valueMontoDolares.getText());
                                                    System.out.println("Filtro < Monto Dolares Front > : " + montoDolaresFront);

                                                    double amountMinDolares = Double.parseDouble(fMontoDesde);
                                                    double amountMaxDolares = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Dolares > Desde: " + amountMinDolares + " Hasta: " + amountMaxDolares);

                                                    try {
                                                        if ((amountMinDolares >= montoDolaresFront) && (amountMaxDolares <= montoDolaresFront)) {
                                                            System.out.println("Filtro < Monto Dolares Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Dolares todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Dolares No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;

                                    default:
                                        System.out.println("No se encuentra ningún tipo de moneda !!!");
                                }

                                break;
                            case "Asignar":
                                switch (fMoneda) {
                                    case "PEN":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoSoles = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(10)", "", "");

                                                    double montoSolesFront = Double.parseDouble(valueMontoSoles.getText());
                                                    System.out.println("Filtro < Monto Soles Front > : " + montoSolesFront);

                                                    double amountMinSoles = Double.parseDouble(fMontoDesde);
                                                    double amountMaxSoles = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Soles > Desde: " + amountMinSoles + " Hasta: " + amountMaxSoles);

                                                    try {
                                                        if ((amountMinSoles >= montoSolesFront) && (amountMaxSoles <= montoSolesFront)) {
                                                            System.out.println("Filtro < Monto Soles Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Soles todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Soles No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;
                                    case "USD":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoDolares = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");

                                                    double montoDolaresFront = Double.parseDouble(valueMontoDolares.getText());
                                                    System.out.println("Filtro < Monto Dolares Front > : " + montoDolaresFront);

                                                    double amountMinDolares = Double.parseDouble(fMontoDesde);
                                                    double amountMaxDolares = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Dolares > Desde: " + amountMinDolares + " Hasta: " + amountMaxDolares);

                                                    try {
                                                        if ((amountMinDolares >= montoDolaresFront) && (amountMaxDolares <= montoDolaresFront)) {
                                                            System.out.println("Filtro < Monto Dolares Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Dolares todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Dolares No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;

                                    default:
                                        System.out.println("No se encuentra ningún tipo de moneda !!!");
                                }

                                break;
                            case "Resolver":
                                switch (fMoneda) {
                                    case "PEN":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoSoles = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(10)", "", "");

                                                    double montoSolesFront = Double.parseDouble(valueMontoSoles.getText());
                                                    System.out.println("Filtro < Monto Soles Front > : " + montoSolesFront);

                                                    double amountMinSoles = Double.parseDouble(fMontoDesde);
                                                    double amountMaxSoles = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Soles > Desde: " + amountMinSoles + " Hasta: " + amountMaxSoles);

                                                    try {
                                                        if ((amountMinSoles >= montoSolesFront) && (amountMaxSoles <= montoSolesFront)) {
                                                            System.out.println("Filtro < Monto Soles Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Soles todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Soles No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;
                                    case "USD":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoDolares = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");

                                                    double montoDolaresFront = Double.parseDouble(valueMontoDolares.getText());
                                                    System.out.println("Filtro < Monto Dolares Front > : " + montoDolaresFront);

                                                    double amountMinDolares = Double.parseDouble(fMontoDesde);
                                                    double amountMaxDolares = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Dolares > Desde: " + amountMinDolares + " Hasta: " + amountMaxDolares);

                                                    try {
                                                        if ((amountMinDolares >= montoDolaresFront) && (amountMaxDolares <= montoDolaresFront)) {
                                                            System.out.println("Filtro < Monto Dolares Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Dolares todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Dolares No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;

                                    default:
                                        System.out.println("No se encuentra ningún tipo de moneda !!!");
                                }

                                break;
                            case "Monitorear":
                                switch (fMoneda) {
                                    case "PEN":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoSoles = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(10)", "", "");

                                                    double montoSolesFront = Double.parseDouble(valueMontoSoles.getText());
                                                    System.out.println("Filtro < Monto Soles Front > : " + montoSolesFront);

                                                    double amountMinSoles = Double.parseDouble(fMontoDesde);
                                                    double amountMaxSoles = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Soles > Desde: " + amountMinSoles + " Hasta: " + amountMaxSoles);

                                                    try {
                                                        if ((amountMinSoles >= montoSolesFront) && (amountMaxSoles <= montoSolesFront)) {
                                                            System.out.println("Filtro < Monto Soles Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Soles todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Soles No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;
                                    case "USD":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoDolares = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");

                                                    double montoDolaresFront = Double.parseDouble(valueMontoDolares.getText());
                                                    System.out.println("Filtro < Monto Dolares Front > : " + montoDolaresFront);

                                                    double amountMinDolares = Double.parseDouble(fMontoDesde);
                                                    double amountMaxDolares = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Dolares > Desde: " + amountMinDolares + " Hasta: " + amountMaxDolares);

                                                    try {
                                                        if ((amountMinDolares >= montoDolaresFront) && (amountMaxDolares <= montoDolaresFront)) {
                                                            System.out.println("Filtro < Monto Dolares Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Dolares todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Dolares No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;

                                    default:
                                        System.out.println("No se encuentra ningún tipo de moneda !!!");
                                }

                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA VALIDAR FILTRO MONEDA Y MONTO: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                }
                break;

            case "Retenciones":
                break;

            case "Controversias":
                switch (perfil) {
                    //TODO: Especialista Controversias
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                switch (fMoneda) {
                                    case "PEN":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoSoles = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(9)", "", "");

                                                    double montoSolesFront = Double.parseDouble(valueMontoSoles.getText());
                                                    System.out.println("Filtro < Monto Soles Front > : " + montoSolesFront);

                                                    double amountMinSoles = Double.parseDouble(fMontoDesde);
                                                    double amountMaxSoles = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Soles > Desde: " + amountMinSoles + " Hasta: " + amountMaxSoles);

                                                    try {
                                                        if ((amountMinSoles >= montoSolesFront) && (amountMaxSoles <= montoSolesFront)) {
                                                            System.out.println("Filtro < Monto Soles Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Soles todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Soles No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;
                                    case "USD":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoDolares = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(10)", "", "");

                                                    double montoDolaresFront = Double.parseDouble(valueMontoDolares.getText());
                                                    System.out.println("Filtro < Monto Dolares Front > : " + montoDolaresFront);

                                                    double amountMinDolares = Double.parseDouble(fMontoDesde);
                                                    double amountMaxDolares = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Dolares > Desde: " + amountMinDolares + " Hasta: " + amountMaxDolares);

                                                    try {
                                                        if ((amountMinDolares >= montoDolaresFront) && (amountMaxDolares <= montoDolaresFront)) {
                                                            System.out.println("Filtro < Monto Dolares Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Dolares todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Dolares No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;

                                    default:
                                        System.out.println("No se encuentra ningún tipo de moneda !!!");
                                }

                                break;
                            case "ConsumosNoReconocidos":
                                switch (fMoneda) {
                                    case "PEN":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoSoles = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(9)", "", "");

                                                    double montoSolesFront = Double.parseDouble(valueMontoSoles.getText());
                                                    System.out.println("Filtro < Monto Soles Front > : " + montoSolesFront);

                                                    double amountMinSoles = Double.parseDouble(fMontoDesde);
                                                    double amountMaxSoles = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Soles > Desde: " + amountMinSoles + " Hasta: " + amountMaxSoles);

                                                    try {
                                                        if ((amountMinSoles >= montoSolesFront) && (amountMaxSoles <= montoSolesFront)) {
                                                            System.out.println("Filtro < Monto Soles Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Soles todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Soles No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;
                                    case "USD":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoDolares = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(10)", "", "");

                                                    double montoDolaresFront = Double.parseDouble(valueMontoDolares.getText());
                                                    System.out.println("Filtro < Monto Dolares Front > : " + montoDolaresFront);

                                                    double amountMinDolares = Double.parseDouble(fMontoDesde);
                                                    double amountMaxDolares = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Dolares > Desde: " + amountMinDolares + " Hasta: " + amountMaxDolares);

                                                    try {
                                                        if ((amountMinDolares >= montoDolaresFront) && (amountMaxDolares <= montoDolaresFront)) {
                                                            System.out.println("Filtro < Monto Dolares Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Dolares todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Dolares No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;

                                    default:
                                        System.out.println("No se encuentra ningún tipo de moneda !!!");
                                }

                                break;
                            case "ConsumosMalProcesados":
                                switch (fMoneda) {
                                    case "PEN":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoSoles = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(9)", "", "");

                                                    double montoSolesFront = Double.parseDouble(valueMontoSoles.getText());
                                                    System.out.println("Filtro < Monto Soles Front > : " + montoSolesFront);

                                                    double amountMinSoles = Double.parseDouble(fMontoDesde);
                                                    double amountMaxSoles = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Soles > Desde: " + amountMinSoles + " Hasta: " + amountMaxSoles);

                                                    try {
                                                        if ((amountMinSoles >= montoSolesFront) && (amountMaxSoles <= montoSolesFront)) {
                                                            System.out.println("Filtro < Monto Soles Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Soles todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Soles No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;
                                    case "USD":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoDolares = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(10)", "", "");

                                                    double montoDolaresFront = Double.parseDouble(valueMontoDolares.getText());
                                                    System.out.println("Filtro < Monto Dolares Front > : " + montoDolaresFront);

                                                    double amountMinDolares = Double.parseDouble(fMontoDesde);
                                                    double amountMaxDolares = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Dolares > Desde: " + amountMinDolares + " Hasta: " + amountMaxDolares);

                                                    try {
                                                        if ((amountMinDolares >= montoDolaresFront) && (amountMaxDolares <= montoDolaresFront)) {
                                                            System.out.println("Filtro < Monto Dolares Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Dolares todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Dolares No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;

                                    default:
                                        System.out.println("No se encuentra ningún tipo de moneda !!!");
                                }

                                break;
                            case "Monitorear":
                                switch (fMoneda) {
                                    case "PEN":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoSoles = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(9)", "", "");

                                                    double montoSolesFront = Double.parseDouble(valueMontoSoles.getText());
                                                    System.out.println("Filtro < Monto Soles Front > : " + montoSolesFront);

                                                    double amountMinSoles = Double.parseDouble(fMontoDesde);
                                                    double amountMaxSoles = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Soles > Desde: " + amountMinSoles + " Hasta: " + amountMaxSoles);

                                                    try {
                                                        if ((amountMinSoles >= montoSolesFront) && (amountMaxSoles <= montoSolesFront)) {
                                                            System.out.println("Filtro < Monto Soles Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Soles todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Soles No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;
                                    case "USD":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoDolares = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(10)", "", "");

                                                    double montoDolaresFront = Double.parseDouble(valueMontoDolares.getText());
                                                    System.out.println("Filtro < Monto Dolares Front > : " + montoDolaresFront);

                                                    double amountMinDolares = Double.parseDouble(fMontoDesde);
                                                    double amountMaxDolares = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Dolares > Desde: " + amountMinDolares + " Hasta: " + amountMaxDolares);

                                                    try {
                                                        if ((amountMinDolares >= montoDolaresFront) && (amountMaxDolares <= montoDolaresFront)) {
                                                            System.out.println("Filtro < Monto Dolares Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Dolares todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Dolares No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;

                                    default:
                                        System.out.println("No se encuentra ningún tipo de moneda !!!");
                                }

                                break;
                            case "TransaccionesEnProceso":
                                switch (fMoneda) {
                                    case "PEN":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoSoles = general.expandContainer("2css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(9)", "", "");

                                                    double montoSolesFront = Double.parseDouble(valueMontoSoles.getText());
                                                    System.out.println("Filtro < Monto Soles Front > : " + montoSolesFront);

                                                    double amountMinSoles = Double.parseDouble(fMontoDesde);
                                                    double amountMaxSoles = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Soles > Desde: " + amountMinSoles + " Hasta: " + amountMaxSoles);

                                                    try {
                                                        if ((amountMinSoles >= montoSolesFront) && (amountMaxSoles <= montoSolesFront)) {
                                                            System.out.println("Filtro < Monto Soles Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Soles todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Soles No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;
                                    case "USD":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoDolares = general.expandContainer("2css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(10)", "", "");

                                                    double montoDolaresFront = Double.parseDouble(valueMontoDolares.getText());
                                                    System.out.println("Filtro < Monto Dolares Front > : " + montoDolaresFront);

                                                    double amountMinDolares = Double.parseDouble(fMontoDesde);
                                                    double amountMaxDolares = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Dolares > Desde: " + amountMinDolares + " Hasta: " + amountMaxDolares);

                                                    try {
                                                        if ((amountMinDolares >= montoDolaresFront) && (amountMaxDolares <= montoDolaresFront)) {
                                                            System.out.println("Filtro < Monto Dolares Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Dolares todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Dolares No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;

                                    default:
                                        System.out.println("No se encuentra ningún tipo de moneda !!!");
                                }

                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA VALIDAR FILTRO MONEDA Y MONTO: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;

                    //TODO: Supervisor Controversias
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                switch (fMoneda) {
                                    case "PEN":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoSoles = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(9)", "", "");

                                                    double montoSolesFront = Double.parseDouble(valueMontoSoles.getText());
                                                    System.out.println("Filtro < Monto Soles Front > : " + montoSolesFront);

                                                    double amountMinSoles = Double.parseDouble(fMontoDesde);
                                                    double amountMaxSoles = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Soles > Desde: " + amountMinSoles + " Hasta: " + amountMaxSoles);

                                                    try {
                                                        if ((amountMinSoles >= montoSolesFront) && (amountMaxSoles <= montoSolesFront)) {
                                                            System.out.println("Filtro < Monto Soles Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Soles todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Soles No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;
                                    case "USD":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoDolares = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(10)", "", "");

                                                    double montoDolaresFront = Double.parseDouble(valueMontoDolares.getText());
                                                    System.out.println("Filtro < Monto Dolares Front > : " + montoDolaresFront);

                                                    double amountMinDolares = Double.parseDouble(fMontoDesde);
                                                    double amountMaxDolares = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Dolares > Desde: " + amountMinDolares + " Hasta: " + amountMaxDolares);

                                                    try {
                                                        if ((amountMinDolares >= montoDolaresFront) && (amountMaxDolares <= montoDolaresFront)) {
                                                            System.out.println("Filtro < Monto Dolares Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Dolares todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Dolares No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;

                                    default:
                                        System.out.println("No se encuentra ningún tipo de moneda !!!");
                                }

                                break;
                            case "MiEquipo":
                                switch (fMoneda) {
                                    case "PEN":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoSoles = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(10)", "", "");

                                                    double montoSolesFront = Double.parseDouble(valueMontoSoles.getText());
                                                    System.out.println("Filtro < Monto Soles Front > : " + montoSolesFront);

                                                    double amountMinSoles = Double.parseDouble(fMontoDesde);
                                                    double amountMaxSoles = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Soles > Desde: " + amountMinSoles + " Hasta: " + amountMaxSoles);

                                                    try {
                                                        if ((amountMinSoles >= montoSolesFront) && (amountMaxSoles <= montoSolesFront)) {
                                                            System.out.println("Filtro < Monto Soles Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Soles todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Soles No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;
                                    case "USD":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoDolares = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");

                                                    double montoDolaresFront = Double.parseDouble(valueMontoDolares.getText());
                                                    System.out.println("Filtro < Monto Dolares Front > : " + montoDolaresFront);

                                                    double amountMinDolares = Double.parseDouble(fMontoDesde);
                                                    double amountMaxDolares = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Dolares > Desde: " + amountMinDolares + " Hasta: " + amountMaxDolares);

                                                    try {
                                                        if ((amountMinDolares >= montoDolaresFront) && (amountMaxDolares <= montoDolaresFront)) {
                                                            System.out.println("Filtro < Monto Dolares Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Dolares todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Dolares No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;

                                    default:
                                        System.out.println("No se encuentra ningún tipo de moneda !!!");
                                }

                                break;
                            case "Asignar":
                                switch (fMoneda) {
                                    case "PEN":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoSoles = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(10)", "", "");

                                                    double montoSolesFront = Double.parseDouble(valueMontoSoles.getText());
                                                    System.out.println("Filtro < Monto Soles Front > : " + montoSolesFront);

                                                    double amountMinSoles = Double.parseDouble(fMontoDesde);
                                                    double amountMaxSoles = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Soles > Desde: " + amountMinSoles + " Hasta: " + amountMaxSoles);

                                                    try {
                                                        if ((amountMinSoles >= montoSolesFront) && (amountMaxSoles <= montoSolesFront)) {
                                                            System.out.println("Filtro < Monto Soles Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Soles todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Soles No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;
                                    case "USD":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoDolares = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");

                                                    double montoDolaresFront = Double.parseDouble(valueMontoDolares.getText());
                                                    System.out.println("Filtro < Monto Dolares Front > : " + montoDolaresFront);

                                                    double amountMinDolares = Double.parseDouble(fMontoDesde);
                                                    double amountMaxDolares = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Dolares > Desde: " + amountMinDolares + " Hasta: " + amountMaxDolares);

                                                    try {
                                                        if ((amountMinDolares >= montoDolaresFront) && (amountMaxDolares <= montoDolaresFront)) {
                                                            System.out.println("Filtro < Monto Dolares Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Dolares todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Dolares No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;

                                    default:
                                        System.out.println("No se encuentra ningún tipo de moneda !!!");
                                }

                                break;
                            case "Resolver":
                                switch (fMoneda) {
                                    case "PEN":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoSoles = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(10)", "", "");

                                                    double montoSolesFront = Double.parseDouble(valueMontoSoles.getText());
                                                    System.out.println("Filtro < Monto Soles Front > : " + montoSolesFront);

                                                    double amountMinSoles = Double.parseDouble(fMontoDesde);
                                                    double amountMaxSoles = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Soles > Desde: " + amountMinSoles + " Hasta: " + amountMaxSoles);

                                                    try {
                                                        if ((amountMinSoles >= montoSolesFront) && (amountMaxSoles <= montoSolesFront)) {
                                                            System.out.println("Filtro < Monto Soles Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Soles todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Soles No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;
                                    case "USD":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoDolares = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");

                                                    double montoDolaresFront = Double.parseDouble(valueMontoDolares.getText());
                                                    System.out.println("Filtro < Monto Dolares Front > : " + montoDolaresFront);

                                                    double amountMinDolares = Double.parseDouble(fMontoDesde);
                                                    double amountMaxDolares = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Dolares > Desde: " + amountMinDolares + " Hasta: " + amountMaxDolares);

                                                    try {
                                                        if ((amountMinDolares >= montoDolaresFront) && (amountMaxDolares <= montoDolaresFront)) {
                                                            System.out.println("Filtro < Monto Dolares Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Dolares todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Dolares No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;

                                    default:
                                        System.out.println("No se encuentra ningún tipo de moneda !!!");
                                }

                                break;
                            case "Monitorear":
                                switch (fMoneda) {
                                    case "PEN":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoSoles = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(10)", "", "");

                                                    double montoSolesFront = Double.parseDouble(valueMontoSoles.getText());
                                                    System.out.println("Filtro < Monto Soles Front > : " + montoSolesFront);

                                                    double amountMinSoles = Double.parseDouble(fMontoDesde);
                                                    double amountMaxSoles = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Soles > Desde: " + amountMinSoles + " Hasta: " + amountMaxSoles);

                                                    try {
                                                        if ((amountMinSoles >= montoSolesFront) && (amountMaxSoles <= montoSolesFront)) {
                                                            System.out.println("Filtro < Monto Soles Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Soles todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Soles No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;
                                    case "USD":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoDolares = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");

                                                    double montoDolaresFront = Double.parseDouble(valueMontoDolares.getText());
                                                    System.out.println("Filtro < Monto Dolares Front > : " + montoDolaresFront);

                                                    double amountMinDolares = Double.parseDouble(fMontoDesde);
                                                    double amountMaxDolares = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Dolares > Desde: " + amountMinDolares + " Hasta: " + amountMaxDolares);

                                                    try {
                                                        if ((amountMinDolares >= montoDolaresFront) && (amountMaxDolares <= montoDolaresFront)) {
                                                            System.out.println("Filtro < Monto Dolares Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Dolares todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Dolares No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;

                                    default:
                                        System.out.println("No se encuentra ningún tipo de moneda !!!");
                                }

                                break;
                            case "TransaccionesEnProceso":
                                switch (fMoneda) {
                                    case "PEN":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoSoles = general.expandContainer("2css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(9)", "", "");

                                                    double montoSolesFront = Double.parseDouble(valueMontoSoles.getText());
                                                    System.out.println("Filtro < Monto Soles Front > : " + montoSolesFront);

                                                    double amountMinSoles = Double.parseDouble(fMontoDesde);
                                                    double amountMaxSoles = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Soles > Desde: " + amountMinSoles + " Hasta: " + amountMaxSoles);

                                                    try {
                                                        if ((amountMinSoles >= montoSolesFront) && (amountMaxSoles <= montoSolesFront)) {
                                                            System.out.println("Filtro < Monto Soles Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Soles todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Soles No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;
                                    case "USD":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoDolares = general.expandContainer("2css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(10)", "", "");

                                                    double montoDolaresFront = Double.parseDouble(valueMontoDolares.getText());
                                                    System.out.println("Filtro < Monto Dolares Front > : " + montoDolaresFront);

                                                    double amountMinDolares = Double.parseDouble(fMontoDesde);
                                                    double amountMaxDolares = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Dolares > Desde: " + amountMinDolares + " Hasta: " + amountMaxDolares);

                                                    try {
                                                        if ((amountMinDolares >= montoDolaresFront) && (amountMaxDolares <= montoDolaresFront)) {
                                                            System.out.println("Filtro < Monto Dolares Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Dolares todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Dolares No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;

                                    default:
                                        System.out.println("No se encuentra ningún tipo de moneda !!!");
                                }

                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA VALIDAR FILTRO MONEDA Y MONTO: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                }

                break;

            case "USPR":
                switch (perfil) {
                    //TODO: Especialista USPR
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                switch (fMoneda) {
                                    case "PEN":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoSoles = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(9)", "", "");

                                                    double montoSolesFront = Double.parseDouble(valueMontoSoles.getText());
                                                    System.out.println("Filtro < Monto Soles Front > : " + montoSolesFront);

                                                    double amountMinSoles = Double.parseDouble(fMontoDesde);
                                                    double amountMaxSoles = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Soles > Desde: " + amountMinSoles + " Hasta: " + amountMaxSoles);

                                                    try {
                                                        if ((amountMinSoles >= montoSolesFront) && (amountMaxSoles <= montoSolesFront)) {
                                                            System.out.println("Filtro < Monto Soles Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Soles todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Soles No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;
                                    case "USD":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoDolares = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(10)", "", "");

                                                    double montoDolaresFront = Double.parseDouble(valueMontoDolares.getText());
                                                    System.out.println("Filtro < Monto Dolares Front > : " + montoDolaresFront);

                                                    double amountMinDolares = Double.parseDouble(fMontoDesde);
                                                    double amountMaxDolares = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Dolares > Desde: " + amountMinDolares + " Hasta: " + amountMaxDolares);

                                                    try {
                                                        if ((amountMinDolares >= montoDolaresFront) && (amountMaxDolares <= montoDolaresFront)) {
                                                            System.out.println("Filtro < Monto Dolares Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Dolares todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Dolares No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;

                                    default:
                                        System.out.println("No se encuentra ningún tipo de moneda !!!");
                                }

                                break;
                            case "CobrosIndebidos":
                                switch (fMoneda) {
                                    case "PEN":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoSoles = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(9)", "", "");

                                                    double montoSolesFront = Double.parseDouble(valueMontoSoles.getText());
                                                    System.out.println("Filtro < Monto Soles Front > : " + montoSolesFront);

                                                    double amountMinSoles = Double.parseDouble(fMontoDesde);
                                                    double amountMaxSoles = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Soles > Desde: " + amountMinSoles + " Hasta: " + amountMaxSoles);

                                                    try {
                                                        if ((amountMinSoles >= montoSolesFront) && (amountMaxSoles <= montoSolesFront)) {
                                                            System.out.println("Filtro < Monto Soles Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Soles todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Soles No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;
                                    case "USD":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoDolares = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(10)", "", "");

                                                    double montoDolaresFront = Double.parseDouble(valueMontoDolares.getText());
                                                    System.out.println("Filtro < Monto Dolares Front > : " + montoDolaresFront);

                                                    double amountMinDolares = Double.parseDouble(fMontoDesde);
                                                    double amountMaxDolares = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Dolares > Desde: " + amountMinDolares + " Hasta: " + amountMaxDolares);

                                                    try {
                                                        if ((amountMinDolares >= montoDolaresFront) && (amountMaxDolares <= montoDolaresFront)) {
                                                            System.out.println("Filtro < Monto Dolares Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Dolares todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Dolares No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;

                                    default:
                                        System.out.println("No se encuentra ningún tipo de moneda !!!");
                                }

                                break;
                            case "ExoneracionCobros":
                                switch (fMoneda) {
                                    case "PEN":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoSoles = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(9)", "", "");

                                                    double montoSolesFront = Double.parseDouble(valueMontoSoles.getText());
                                                    System.out.println("Filtro < Monto Soles Front > : " + montoSolesFront);

                                                    double amountMinSoles = Double.parseDouble(fMontoDesde);
                                                    double amountMaxSoles = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Soles > Desde: " + amountMinSoles + " Hasta: " + amountMaxSoles);

                                                    try {
                                                        if ((amountMinSoles >= montoSolesFront) && (amountMaxSoles <= montoSolesFront)) {
                                                            System.out.println("Filtro < Monto Soles Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Soles todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Soles No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;
                                    case "USD":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoDolares = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(10)", "", "");

                                                    double montoDolaresFront = Double.parseDouble(valueMontoDolares.getText());
                                                    System.out.println("Filtro < Monto Dolares Front > : " + montoDolaresFront);

                                                    double amountMinDolares = Double.parseDouble(fMontoDesde);
                                                    double amountMaxDolares = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Dolares > Desde: " + amountMinDolares + " Hasta: " + amountMaxDolares);

                                                    try {
                                                        if ((amountMinDolares >= montoDolaresFront) && (amountMaxDolares <= montoDolaresFront)) {
                                                            System.out.println("Filtro < Monto Dolares Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Dolares todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Dolares No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;

                                    default:
                                        System.out.println("No se encuentra ningún tipo de moneda !!!");
                                }

                                break;
                            case "ConsumosMalProcesados":
                                switch (fMoneda) {
                                    case "PEN":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoSoles = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(9)", "", "");

                                                    double montoSolesFront = Double.parseDouble(valueMontoSoles.getText());
                                                    System.out.println("Filtro < Monto Soles Front > : " + montoSolesFront);

                                                    double amountMinSoles = Double.parseDouble(fMontoDesde);
                                                    double amountMaxSoles = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Soles > Desde: " + amountMinSoles + " Hasta: " + amountMaxSoles);

                                                    try {
                                                        if ((amountMinSoles >= montoSolesFront) && (amountMaxSoles <= montoSolesFront)) {
                                                            System.out.println("Filtro < Monto Soles Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Soles todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Soles No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;
                                    case "USD":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoDolares = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(10)", "", "");

                                                    double montoDolaresFront = Double.parseDouble(valueMontoDolares.getText());
                                                    System.out.println("Filtro < Monto Dolares Front > : " + montoDolaresFront);

                                                    double amountMinDolares = Double.parseDouble(fMontoDesde);
                                                    double amountMaxDolares = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Dolares > Desde: " + amountMinDolares + " Hasta: " + amountMaxDolares);

                                                    try {
                                                        if ((amountMinDolares >= montoDolaresFront) && (amountMaxDolares <= montoDolaresFront)) {
                                                            System.out.println("Filtro < Monto Dolares Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Dolares todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Dolares No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;

                                    default:
                                        System.out.println("No se encuentra ningún tipo de moneda !!!");
                                }

                                break;
                            case "ConsumosNoReconocidos":
                                switch (fMoneda) {
                                    case "PEN":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoSoles = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(9)", "", "");

                                                    double montoSolesFront = Double.parseDouble(valueMontoSoles.getText());
                                                    System.out.println("Filtro < Monto Soles Front > : " + montoSolesFront);

                                                    double amountMinSoles = Double.parseDouble(fMontoDesde);
                                                    double amountMaxSoles = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Soles > Desde: " + amountMinSoles + " Hasta: " + amountMaxSoles);

                                                    try {
                                                        if ((amountMinSoles >= montoSolesFront) && (amountMaxSoles <= montoSolesFront)) {
                                                            System.out.println("Filtro < Monto Soles Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Soles todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Soles No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;
                                    case "USD":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoDolares = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(10)", "", "");

                                                    double montoDolaresFront = Double.parseDouble(valueMontoDolares.getText());
                                                    System.out.println("Filtro < Monto Dolares Front > : " + montoDolaresFront);

                                                    double amountMinDolares = Double.parseDouble(fMontoDesde);
                                                    double amountMaxDolares = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Dolares > Desde: " + amountMinDolares + " Hasta: " + amountMaxDolares);

                                                    try {
                                                        if ((amountMinDolares >= montoDolaresFront) && (amountMaxDolares <= montoDolaresFront)) {
                                                            System.out.println("Filtro < Monto Dolares Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Dolares todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Dolares No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;

                                    default:
                                        System.out.println("No se encuentra ningún tipo de moneda !!!");
                                }

                                break;
                            case "Retipificar":
                                switch (fMoneda) {
                                    case "PEN":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoSoles = general.expandContainer("2css", LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(9)", "", "");

                                                    double montoSolesFront = Double.parseDouble(valueMontoSoles.getText());
                                                    System.out.println("Filtro < Monto Soles Front > : " + montoSolesFront);

                                                    double amountMinSoles = Double.parseDouble(fMontoDesde);
                                                    double amountMaxSoles = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Soles > Desde: " + amountMinSoles + " Hasta: " + amountMaxSoles);

                                                    try {
                                                        if ((amountMinSoles >= montoSolesFront) && (amountMaxSoles <= montoSolesFront)) {
                                                            System.out.println("Filtro < Monto Soles Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Soles todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Soles No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;
                                    case "USD":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoDolares = general.expandContainer("2css", LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(10)", "", "");

                                                    double montoDolaresFront = Double.parseDouble(valueMontoDolares.getText());
                                                    System.out.println("Filtro < Monto Dolares Front > : " + montoDolaresFront);

                                                    double amountMinDolares = Double.parseDouble(fMontoDesde);
                                                    double amountMaxDolares = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Dolares > Desde: " + amountMinDolares + " Hasta: " + amountMaxDolares);

                                                    try {
                                                        if ((amountMinDolares >= montoDolaresFront) && (amountMaxDolares <= montoDolaresFront)) {
                                                            System.out.println("Filtro < Monto Dolares Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Dolares todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Dolares No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;

                                    default:
                                        System.out.println("No se encuentra ningún tipo de moneda !!!");
                                }

                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA VALIDAR FILTRO MONEDA Y MONTO: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;

                    //TODO: Supervisor USPR
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                switch (fMoneda) {
                                    case "PEN":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_USPR, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoSoles = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(9)", "", "");

                                                    double montoSolesFront = Double.parseDouble(valueMontoSoles.getText());
                                                    System.out.println("Filtro < Monto Soles Front > : " + montoSolesFront);

                                                    double amountMinSoles = Double.parseDouble(fMontoDesde);
                                                    double amountMaxSoles = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Soles > Desde: " + amountMinSoles + " Hasta: " + amountMaxSoles);

                                                    try {
                                                        if ((amountMinSoles >= montoSolesFront) && (amountMaxSoles <= montoSolesFront)) {
                                                            System.out.println("Filtro < Monto Soles Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Soles todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Soles No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;
                                    case "USD":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_USPR, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoDolares = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(10)", "", "");

                                                    double montoDolaresFront = Double.parseDouble(valueMontoDolares.getText());
                                                    System.out.println("Filtro < Monto Dolares Front > : " + montoDolaresFront);

                                                    double amountMinDolares = Double.parseDouble(fMontoDesde);
                                                    double amountMaxDolares = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Dolares > Desde: " + amountMinDolares + " Hasta: " + amountMaxDolares);

                                                    try {
                                                        if ((amountMinDolares >= montoDolaresFront) && (amountMaxDolares <= montoDolaresFront)) {
                                                            System.out.println("Filtro < Monto Dolares Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Dolares todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Dolares No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;

                                    default:
                                        System.out.println("No se encuentra ningún tipo de moneda !!!");
                                }

                                break;
                            case "MiEquipo":
                                switch (fMoneda) {
                                    case "PEN":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoSoles = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(10)", "", "");

                                                    double montoSolesFront = Double.parseDouble(valueMontoSoles.getText());
                                                    System.out.println("Filtro < Monto Soles Front > : " + montoSolesFront);

                                                    double amountMinSoles = Double.parseDouble(fMontoDesde);
                                                    double amountMaxSoles = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Soles > Desde: " + amountMinSoles + " Hasta: " + amountMaxSoles);

                                                    try {
                                                        if ((amountMinSoles >= montoSolesFront) && (amountMaxSoles <= montoSolesFront)) {
                                                            System.out.println("Filtro < Monto Soles Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Soles todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Soles No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;
                                    case "USD":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoDolares = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");

                                                    double montoDolaresFront = Double.parseDouble(valueMontoDolares.getText());
                                                    System.out.println("Filtro < Monto Dolares Front > : " + montoDolaresFront);

                                                    double amountMinDolares = Double.parseDouble(fMontoDesde);
                                                    double amountMaxDolares = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Dolares > Desde: " + amountMinDolares + " Hasta: " + amountMaxDolares);

                                                    try {
                                                        if ((amountMinDolares >= montoDolaresFront) && (amountMaxDolares <= montoDolaresFront)) {
                                                            System.out.println("Filtro < Monto Dolares Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Dolares todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Dolares No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;

                                    default:
                                        System.out.println("No se encuentra ningún tipo de moneda !!!");
                                }

                                break;
                            case "Asignar":
                                switch (fMoneda) {
                                    case "PEN":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoSoles = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(10)", "", "");

                                                    double montoSolesFront = Double.parseDouble(valueMontoSoles.getText());
                                                    System.out.println("Filtro < Monto Soles Front > : " + montoSolesFront);

                                                    double amountMinSoles = Double.parseDouble(fMontoDesde);
                                                    double amountMaxSoles = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Soles > Desde: " + amountMinSoles + " Hasta: " + amountMaxSoles);

                                                    try {
                                                        if ((amountMinSoles >= montoSolesFront) && (amountMaxSoles <= montoSolesFront)) {
                                                            System.out.println("Filtro < Monto Soles Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Soles todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Soles No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;
                                    case "USD":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoDolares = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");

                                                    double montoDolaresFront = Double.parseDouble(valueMontoDolares.getText());
                                                    System.out.println("Filtro < Monto Dolares Front > : " + montoDolaresFront);

                                                    double amountMinDolares = Double.parseDouble(fMontoDesde);
                                                    double amountMaxDolares = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Dolares > Desde: " + amountMinDolares + " Hasta: " + amountMaxDolares);

                                                    try {
                                                        if ((amountMinDolares >= montoDolaresFront) && (amountMaxDolares <= montoDolaresFront)) {
                                                            System.out.println("Filtro < Monto Dolares Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Dolares todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Dolares No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;

                                    default:
                                        System.out.println("No se encuentra ningún tipo de moneda !!!");
                                }

                                break;
                            case "Resolver":
                                switch (fMoneda) {
                                    case "PEN":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoSoles = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(10)", "", "");

                                                    double montoSolesFront = Double.parseDouble(valueMontoSoles.getText());
                                                    System.out.println("Filtro < Monto Soles Front > : " + montoSolesFront);

                                                    double amountMinSoles = Double.parseDouble(fMontoDesde);
                                                    double amountMaxSoles = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Soles > Desde: " + amountMinSoles + " Hasta: " + amountMaxSoles);

                                                    try {
                                                        if ((amountMinSoles >= montoSolesFront) && (amountMaxSoles <= montoSolesFront)) {
                                                            System.out.println("Filtro < Monto Soles Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Soles todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Soles No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;
                                    case "USD":

                                        try {
                                            WebElement valueMessage = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR, "ibk-grid", "ibk-h", "p", "");
                                            String getMessage = valueMessage.getText();
                                            if (getMessage.equals("No se encontraron coincidencias") || getMessage.equals("Ingresa una búsqueda para ver resultados")) {
                                                System.out.println("Este filtro no tiene resultado, se muestra mensaje: " + getMessage);
                                                validaMonedaMonto = true;
                                            }
                                        } catch (Exception e) {
                                            try {
                                                for (int i = 1; i < 100; ++i) {
                                                    WebElement valueMontoDolares = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr:nth-child(" + i + ") > td:nth-child(11)", "", "");

                                                    double montoDolaresFront = Double.parseDouble(valueMontoDolares.getText());
                                                    System.out.println("Filtro < Monto Dolares Front > : " + montoDolaresFront);

                                                    double amountMinDolares = Double.parseDouble(fMontoDesde);
                                                    double amountMaxDolares = Double.parseDouble(fMontoHasta);
                                                    System.out.println("Ingresa un rango para el monto: < Dolares > Desde: " + amountMinDolares + " Hasta: " + amountMaxDolares);

                                                    try {
                                                        if ((amountMinDolares >= montoDolaresFront) && (amountMaxDolares <= montoDolaresFront)) {
                                                            System.out.println("Filtro < Monto Dolares Correcto > Bien !!!");
                                                        } else {
                                                            System.out.println("Filtro < Monto Dolares todo normal >  !!!");
                                                        }

                                                    } catch (Exception msg) {
                                                        System.out.println("Filtro < Monto Dolares No Coincide > Incorrecto !!! " + msg);
                                                    }

                                                    contador++;
                                                }

                                            } catch (Exception i) {
                                                validaMonedaMonto = true;

                                            }
                                        }

                                        break;

                                    default:
                                        System.out.println("No se encuentra ningún tipo de moneda !!!");
                                }

                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA PARA VALIDAR FILTRO MONEDA Y MONTO: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                }

                break;
            default:
                System.out.println("No existe filtro estado en el departamento ingresado: " + departamento + " !!!");
        }

        return validaMonedaMonto;
    }


}
