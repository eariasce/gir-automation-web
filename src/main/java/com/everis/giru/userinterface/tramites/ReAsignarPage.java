package com.everis.giru.userinterface.tramites;

import com.everis.giru.utils.Shadow;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class ReAsignarPage extends PageObject {

    Shadow general = new Shadow();

    @FindBy(xpath = "//ibk-grid-group")
    WebElement txtNumeroReclamoAll;

    @FindBy(xpath = "//ibk-inputgroup-tabs")
    WebElement txtSeguimiento;

    @FindBy(xpath = "//*[@id='header__giru']")
    WebElement nombreArea;

    @FindBy(xpath = "//*[@id='grid']/giru-to-assigned/div[3]/ibk-grid-group")
    WebElement tablaBusquedaCTS;


    public void buscarTramiteBandejaRegistro(String numeroTramite) {
        Serenity.getDriver().findElement(By.id("search-claim-form__txt-document-number")).sendKeys(numeroTramite);
        try {
            Thread.sleep(5000);
        } catch (Exception e) {
            System.out.println(e);
        }

        Serenity.getDriver().findElement(By.id("search-claim-form__txt-document-number")).sendKeys(Keys.ENTER);

        try {
            Thread.sleep(3000);
        } catch (Exception e) {
            System.out.println(e);
        }

        //TODO: VALIDACION - NUMERO DE TRAMITE - BANDEJA
        WebElement valueTramite = general.expandContainer("2css", txtNumeroReclamoAll, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true)", valueTramite);
        String getTramite = valueTramite.getText();
        String sSubTramite = removerCeros(getTramite);
        System.out.println("***************************************\n * VALIDA BANDEJA NUMERO DE TRAMITE: " + "Numero tramite Original: " + getTramite + " Numero tramite Con Metodo: " + sSubTramite);
        Assert.assertEquals(sSubTramite, numeroTramite);
    }

    public String removerCeros(String cadena) {
        cadena = cadena.replaceAll("^0+", "");
        return cadena;
    }

    public void buscarTramiteBandejaResolucion(String numeroTramite) {
        Serenity.getDriver().findElement(By.id("search-claim-form__txt-document-number")).sendKeys(numeroTramite);
        try {
            Thread.sleep(5000);
        } catch (Exception e) {
            System.out.println(e);
        }

        Serenity.getDriver().findElement(By.id("search-claim-form__txt-document-number")).sendKeys(Keys.ENTER);

        try {
            Thread.sleep(3000);
        } catch (Exception e) {
            System.out.println(e);
        }

        //TODO: VALIDACION - NUMERO DE TRAMITE - BANDEJA
        WebElement valueTramite = general.expandContainer("2css", txtNumeroReclamoAll, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true)", valueTramite);
        String getTramite = valueTramite.getText();
        String sSubTramite = removerCeros(getTramite);
        System.out.println("Numero tramite Original: " + getTramite);
        System.out.println("Numero tramite Con Metodo: " + sSubTramite);
        Assert.assertEquals(sSubTramite, numeroTramite);

        valueTramite.click();
        Serenity.takeScreenshot();

        //TODO: BOTÓN SEGUIMIENTO
        WebElement valueSeguimiento = general.expandContainer("1css", txtSeguimiento, "div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > ibk-tab:nth-child(2)", "", "", "");
        valueSeguimiento.click();

        try {
            Thread.sleep(5000);
        } catch (Exception e) {
            System.out.println(e);
        }

        //TODO: BOTÓN RESUMEN
        WebElement valueResumen = general.expandContainer("1css", txtSeguimiento, "div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > ibk-tab:nth-child(1)", "", "", "");
        valueResumen.click();

    }

    public void buscarTramiteBandejaResolucionPA(String numeroTramite) {

        String ambiente = Serenity.sessionVariableCalled("ambiente");
        String tipoProducto = Serenity.sessionVariableCalled("tipoproducto").toString();
        String tipologia = Serenity.sessionVariableCalled("tipologia").toString();

        try {
            Thread.sleep(3000);
        } catch (Exception e) {
            System.out.println(e);
        }
        
        WebElement btnBuscarClaimNumber = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(60)).until(ExpectedConditions.elementToBeClickable(By.id("search-claim-form__txt-document-number")));
        btnBuscarClaimNumber.sendKeys(numeroTramite);
        btnBuscarClaimNumber.sendKeys(Keys.ENTER);

        try {
            Thread.sleep(3000);
        } catch (Exception e) {
            System.out.println(e);
        }

        //TODO : SELECCIONAR RADIO BUTON
        //VALIDAR ESTE ELEMENTO
        WebElement checkTramite = general.expandContainer("3css", txtNumeroReclamoAll, "ibk-grid", "div:nth-child(1) > table:nth-child(1) > tbody:nth-child(3) > tr:nth-child(1) > td:nth-child(1) > div:nth-child(1) > ibk-checkbox:nth-child(1)", "div > label", "");
        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true)", checkTramite);
        checkTramite.click();

        //TODO : SELECCIONAR BUTON ASIGNAR TRAMITE
        WebElement btnAsignar = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(20)).until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='grid']/giru-all-procedures/div[3]/div[1]/giru-render-buttons/div/div/button")));
        btnAsignar.click();

        try {
            Thread.sleep(3000);
        } catch (Exception e) {
            System.out.println(e);
        }

        if (tipoProducto.equals("TARJETA DE CREDITO") || tipoProducto.equals("TARJETA DE DEBITO") || tipoProducto.equals("COBRANZAS")) {

            //TODO: COBRANZAS - TIPOLOGIA PA
            if (tipologia.equals("CONSTANCIA DE NO ADEUDO")) {
                WebElement btnBuscarUser = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(30)).until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@formcontrolname='name']")));
                btnBuscarUser.sendKeys("Hanny");
                btnBuscarUser.sendKeys(Keys.ENTER);
                try {
                    Thread.sleep(3000);
                } catch (Exception e) {
                    System.out.println(e);
                }

                //TODO : SELECCIONAR USUARIO - ASIGNAR TRAMITE
                WebElement btnUserAsignarTramite = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(30)).until(ExpectedConditions.elementToBeClickable(By.xpath("//ibk-button-user-check[@id='815']")));
                btnUserAsignarTramite.click();

            }

            //TODO: TIPOLOGIA PA - TARJETA DE CREDITO - TARJETA DE DEBITO
            if (tipologia.equals("CAMBIO DE FORMA DE ENVÍO DE CORRESPONDENCIA") || tipologia.equals("ABONO DE MILLAS") ||
                    tipologia.equals("CAMBIO DE TARJETA")) {

                if (ambiente.equals("STG")) {
                    //TODO : SELECCIONAR USUARIO - ASIGNAR TRAMITE
                    WebElement btnUserAsignarTramite = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(30)).until(ExpectedConditions.elementToBeClickable(By.xpath("//ibk-button-user-check[@id='490']")));
                    btnUserAsignarTramite.click();
                } else {
                    WebElement btnUserAsignarTramite = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(30)).until(ExpectedConditions.elementToBeClickable(By.xpath("//ibk-button-user-check[@id='430']")));
                    btnUserAsignarTramite.click();
                }

            }


        }


        //TODO : SELECCIONAR USUARIO - ASIGNAR TRAMITE
        WebElement btnAsignarTramite = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(30)).until(ExpectedConditions.elementToBeClickable(By.id("assign-claim__btn-assign")));
        btnAsignarTramite.click();

        try {
            Thread.sleep(3000);
        } catch (Exception e) {
            System.out.println(e);
        }

        //TODO : SELECCIONAR TRAMITE BANDEJA
        WebElement btnBuscarClaimNumberAsignado = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(60)).until(ExpectedConditions.elementToBeClickable(By.id("search-claim-form__txt-document-number")));
        btnBuscarClaimNumberAsignado.sendKeys(numeroTramite);
        btnBuscarClaimNumberAsignado.sendKeys(Keys.ENTER);

        try {
            Thread.sleep(3000);
        } catch (Exception e) {
            System.out.println(e);
        }

        //TODO: VALIDACION - NUMERO DE TRAMITE - BANDEJA
        WebElement valueTramite = general.expandContainer("2css", txtNumeroReclamoAll, "ibk-grid", "div > table > tbody > tr > td:nth-child(2)", "", "");
        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true)", valueTramite);
        String getTramite = valueTramite.getText();
        String sSubTramite = removerCeros(getTramite);
        System.out.println("Numero tramite Original: " + getTramite);
        System.out.println("Numero tramite Con Metodo: " + sSubTramite);
        Assert.assertEquals(sSubTramite, numeroTramite);

        valueTramite.click();
        Serenity.takeScreenshot();

        //TODO: BOTÓN SEGUIMIENTO
        WebElement valueSeguimiento = general.expandContainer("1css", txtSeguimiento, "div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > ibk-tab:nth-child(2)", "", "", "");
        valueSeguimiento.click();

        try {
            Thread.sleep(5000);
        } catch (Exception e) {
            System.out.println(e);
        }

        //TODO: BOTÓN RESUMEN
        WebElement valueResumen = general.expandContainer("1css", txtSeguimiento, "div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > ibk-tab:nth-child(1)", "", "", "");
        valueResumen.click();

    }

    public void buscarTramiteBandejaResolucionTrasladoCTS(String numeroTramite) {

        WebElement btnBandejaAsignar = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(60)).until(ExpectedConditions.elementToBeClickable(By.id("item-2")));
        btnBandejaAsignar.click();

        try {
            Thread.sleep(3000);
        } catch (Exception e) {
            System.out.println(e);
        }
        Serenity.getDriver().findElement(By.id("search-claim-form__txt-document-number")).sendKeys(numeroTramite);
        Serenity.getDriver().findElement(By.id("search-claim-form__txt-document-number")).sendKeys(Keys.ENTER);

        try {
            Thread.sleep(5000);
        } catch (Exception e) {
            System.out.println(e);
        }

        WebElement checkTramite = general.expandContainer("2css", tablaBusquedaCTS, "div > div.ibkTableGroup__table > div.grid-wrapper > ibk-grid", "div > table > tbody > tr > td.index-column > div > ibk-checkbox", "", "");
        checkTramite.click();

        //TODO : SELECCIONAR BUTON ASIGNAR TRAMITE
        WebElement btnAsignar = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(20)).until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"grid\"]/giru-to-assigned/div[3]/div[1]/giru-render-buttons/div/div/button")));
        btnAsignar.click();

        try {
            Thread.sleep(3000);
        } catch (Exception e) {
            System.out.println(e);
        }

        //TODO : SELECCIONAR USUARIO - ASIGNAR TRAMITE
        WebElement btnSelectPerfilAsignarTramite = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(20)).until(ExpectedConditions.elementToBeClickable(By.xpath("//ibk-button-check[@id='5112']")));
        btnSelectPerfilAsignarTramite.click();

        //TODO : SELECCIONAR USUARIO - ASIGNAR TRAMITE
        WebElement btnAsignarTramite = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(20)).until(ExpectedConditions.elementToBeClickable(By.id("assign-claim__btn-assign")));
        btnAsignarTramite.click();

        //TODO : SELECCIONAR TODOS LOS TRAMITES
        WebElement btnTodosMisTramite = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(30)).until(ExpectedConditions.elementToBeClickable(By.id("item-1")));
        btnTodosMisTramite.click();

        try {
            Thread.sleep(5000);
        } catch (Exception e) {
            System.out.println(e);
        }

        Serenity.getDriver().findElement(By.id("search-claim-form__txt-document-number")).sendKeys(numeroTramite);
        Serenity.getDriver().findElement(By.id("search-claim-form__txt-document-number")).sendKeys(Keys.ENTER);

        //TODO: VALIDACION - NUMERO DE TRAMITE - BANDEJA
        WebElement valueTramite = general.expandContainer("2css", txtNumeroReclamoAll, "ibk-grid", "div > table > tbody > tr > td:nth-child(2)", "", "");
        String getTramite = valueTramite.getText();
        String sSubTramite = removerCeros(getTramite);
        System.out.println("Numero tramite Original: " + getTramite);
        System.out.println("Numero tramite Con Metodo: " + sSubTramite);
        Assert.assertEquals(sSubTramite, numeroTramite);

        valueTramite.click();
        Serenity.takeScreenshot();

        //TODO: BOTÓN SEGUIMIENTO
        WebElement valueSeguimiento = general.expandContainer("1css", txtSeguimiento, "div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > ibk-tab:nth-child(2)", "", "", "");
        valueSeguimiento.click();

        try {
            Thread.sleep(5000);
        } catch (Exception e) {
            System.out.println(e);
        }

        //TODO: BOTÓN RESUMEN
        WebElement valueResumen = general.expandContainer("1css", txtSeguimiento, "div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > ibk-tab:nth-child(1)", "", "", "");
        valueResumen.click();

    }


    public void validoNombreYAreaAsesor(String area, String asesorNombre) {
        String nombreAsesorValidar = Serenity.sessionVariableCalled("nombreAsesor");
        String areaAsesorValidar = Serenity.sessionVariableCalled("nombreArea");

        //  WebElement texto = general.expandContainer("2css", nombreArea, "div > div.usuario > ibk-breadcrum:nth-child(2)", "div > p", "");

        if (area.toUpperCase().equals(areaAsesorValidar.toUpperCase())) {
            //Bien

        } else {
            Assert.assertEquals(areaAsesorValidar, area);
        }

        String nombres = nombreAsesorValidar.replace(",", "");

        WebElement texto2 = general.expandContainer("2css", nombreArea, "div > div.usuario > ibk-breadcrum:nth-child(1)", "div > p", "", "");
        System.out.println("UNO " + nombreAsesorValidar + " OTRO " + texto2.getText());
        String texto[] = texto2.getText().split(" ");

        for (int i = 0; i < texto.length; i++) {
            if (nombres.toUpperCase().contains(texto[i].toUpperCase())) {

            } else {
                Assert.assertEquals(nombres.toUpperCase(), texto[i].toUpperCase());
            }
        }


    }

}
