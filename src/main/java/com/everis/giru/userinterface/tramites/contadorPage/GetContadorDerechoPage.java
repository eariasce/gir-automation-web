package com.everis.giru.userinterface.tramites.contadorPage;

import com.everis.giru.utils.ApiCommons;
import com.everis.giru.utils.Shadow;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class GetContadorDerechoPage extends PageObject {

    Shadow general = new Shadow();
    public String getValueContadorDerecho;
    public int contador = 0;
    public boolean TotalTramitesEsCERO = false;
    String usuarioSesion = ApiCommons.USUARIO;

    //MAPEO DE ELEMENTOS - VALIDA TOTAL TRAMITE POR TIPOLOGIA
    @FindBy(xpath = "//*[@id='grid']/giru-specialist-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR;
    //TODO ADD NUEVAS TIPOLOGIAS - GPYR
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_TRANSACCION_MAL_ONO_PROCESADA_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_PAGO_ANTICIPADO_ADELANTO_CUOTAS_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_DEVOLUCION_SALDO_ACREEDOR_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_PROGRAMA_RECOMPESAS_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SISTEMA_RECOMPENSAS_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_INSUFICIENTE_INFORMACION_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_INCORRECTA_APLICACION_CD_CUOTAS_ESPECIALISTA_GPYR;

    @FindBy(xpath = "//*[@id='grid']/giru-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SUPERVISOR_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-my-team/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-to-assigned/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-to-resolve/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR;

    @FindBy(xpath = "//*[@id='grid']/giru-specialist-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ESPECIALISTA_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-specialist-to-monitor/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS;

    @FindBy(xpath = "//*[@id='grid']/giru-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SUPERVISOR_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-my-team/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-assigned/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-resolve/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-monitor/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS;

    @FindBy(xpath = "//*[@id='grid']/giru-specialist-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-specialist-to-monitor/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-specialist-to-transactions-in-process/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS;

    @FindBy(xpath = "//*[@id='grid']/giru-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-my-team/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-assigned/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-resolve/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-monitor/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-supervisor-trx/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS;

    @FindBy(xpath = "//*[@id='grid']/giru-specialist-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR;
    //TODO ADD NUEVAS TIPOLOGIAS - USPR
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_TRANSACCION_MAL_NO_PROCESADA_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_DUPLICADO_ESTADO_CUENTA_SC_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SISTEMA_RECOMPENSAS_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_DESACUERDO_CONDICIONES_TT_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-task/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR;

    @FindBy(xpath = "//*[@id='grid']/giru-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SUPERVISOR_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-my-team/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-to-assigned/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-to-resolve/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR;


    //TODO: Validacion Total Tramite Por Tipologia
    public void EsCeroTotalTramite() {
        TotalTramitesEsCERO = true;
    }

    public String getContadorDerecho(String departamento, String perfil, String tipologia) {

        getValueContadorDerecho = "";

        switch (departamento) {
            case "GPYR":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ESPECIALISTA_GPYR));
                                        WebElement valueTotalTramite = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_GPYR, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueTotalTramite.getText();
                                        System.out.println("Contador tipologia TodosMisTramites = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");


                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en TodosMisTramites para contador 0 del Perfil Especialista Departamento GPYR");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ESPECIALISTA_GPYR));
                                    WebElement valueTotalTramite = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_GPYR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueTotalTramite.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "CobrosIndebidos":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR));
                                        WebElement valueTotalTramite_CobrosIndebidos = general.expandContainer("3css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueTotalTramite_CobrosIndebidos.getText();
                                        System.out.println("Contador tipologia CobrosIndebidos = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en CobrosIndebidos para contador 0 del Perfil Especialista Departamento GPYR");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR));
                                    WebElement valueTotalTramite_CobrosIndebidos = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueTotalTramite_CobrosIndebidos.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }

                                break;
                            case "ExoneracionCobros":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR));
                                        WebElement valueTotalTramite_ExoneracionCobros = general.expandContainer("3css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueTotalTramite_ExoneracionCobros.getText();
                                        System.out.println("Contador tipologia Exoneracion = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");
                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en Exoneracion de Cobros para contador 0 del Perfil Especialista Departamento GPYR");
                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR));
                                    WebElement valueTotalTramite_ExoneracionCobros = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueTotalTramite_ExoneracionCobros.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }

                                break;
                            case "AnulacionTC":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR));
                                        WebElement valueTotalTramite_AnulacionTC = general.expandContainer("3css", LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueTotalTramite_AnulacionTC.getText();
                                        System.out.println("Contador tipologia AnulacionTC = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");
                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en AnulacionTC para contador 0 del Perfil Especialista Departamento GPYR");
                                    }

                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR));
                                    WebElement valueTotalTramite_AnulacionTC = general.expandContainer("2css", LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueTotalTramite_AnulacionTC.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "FinalizacionTC":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR));
                                        WebElement valueTotalTramite_FinalizacionTC = general.expandContainer("3css", LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueTotalTramite_FinalizacionTC.getText();
                                        System.out.println("Contador tipologia Finalización = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> para contador 0");

                                    }

                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR));
                                    WebElement valueTotalTramite_FinalizacionTC = general.expandContainer("2css", LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueTotalTramite_FinalizacionTC.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "TransaccionMalNoProcesada":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_TRANSACCION_MAL_ONO_PROCESADA_ESPECIALISTA_GPYR));
                                        WebElement valueTotalTramite_TMalNoProcesada = general.expandContainer("3css", LBL_ID_TABLA_TRANSACCION_MAL_ONO_PROCESADA_ESPECIALISTA_GPYR, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueTotalTramite_TMalNoProcesada.getText();
                                        System.out.println("Contador tipologia Finalización = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> para contador 0");

                                    }

                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_TRANSACCION_MAL_ONO_PROCESADA_ESPECIALISTA_GPYR));
                                    WebElement valueTotalTramite_TMalNoProcesada = general.expandContainer("2css", LBL_ID_TABLA_TRANSACCION_MAL_ONO_PROCESADA_ESPECIALISTA_GPYR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueTotalTramite_TMalNoProcesada.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "PagoAnticipadoAdelantoCuotas":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_PAGO_ANTICIPADO_ADELANTO_CUOTAS_ESPECIALISTA_GPYR));
                                        WebElement valueTotalTramite_PagoAnticipadoAdelantoCuotas = general.expandContainer("3css", LBL_ID_TABLA_PAGO_ANTICIPADO_ADELANTO_CUOTAS_ESPECIALISTA_GPYR, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueTotalTramite_PagoAnticipadoAdelantoCuotas.getText();
                                        System.out.println("Contador tipologia Finalización = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> para contador 0");

                                    }

                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_PAGO_ANTICIPADO_ADELANTO_CUOTAS_ESPECIALISTA_GPYR));
                                    WebElement valueTotalTramite_PagoAnticipadoAdelantoCuotas = general.expandContainer("2css", LBL_ID_TABLA_PAGO_ANTICIPADO_ADELANTO_CUOTAS_ESPECIALISTA_GPYR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueTotalTramite_PagoAnticipadoAdelantoCuotas.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "DevolucionSaldoAcreedor":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_DEVOLUCION_SALDO_ACREEDOR_ESPECIALISTA_GPYR));
                                        WebElement valueTotalTramite_DevolucionSaldoAcreedor = general.expandContainer("3css", LBL_ID_TABLA_DEVOLUCION_SALDO_ACREEDOR_ESPECIALISTA_GPYR, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueTotalTramite_DevolucionSaldoAcreedor.getText();
                                        System.out.println("Contador tipologia Finalización = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> para contador 0");

                                    }

                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_DEVOLUCION_SALDO_ACREEDOR_ESPECIALISTA_GPYR));
                                    WebElement valueTotalTramite_DevolucionSaldoAcreedor = general.expandContainer("2css", LBL_ID_TABLA_DEVOLUCION_SALDO_ACREEDOR_ESPECIALISTA_GPYR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueTotalTramite_DevolucionSaldoAcreedor.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "ProgramaRecompensas":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_PROGRAMA_RECOMPESAS_ESPECIALISTA_GPYR));
                                        WebElement valueTotalTramite_ProgramaRecompensas = general.expandContainer("3css", LBL_ID_TABLA_PROGRAMA_RECOMPESAS_ESPECIALISTA_GPYR, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueTotalTramite_ProgramaRecompensas.getText();
                                        System.out.println("Contador tipologia Finalización = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> para contador 0");

                                    }

                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_PROGRAMA_RECOMPESAS_ESPECIALISTA_GPYR));
                                    WebElement valueTotalTramite_ProgramaRecompensas = general.expandContainer("2css", LBL_ID_TABLA_PROGRAMA_RECOMPESAS_ESPECIALISTA_GPYR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueTotalTramite_ProgramaRecompensas.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "SistemaRecompensas":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SISTEMA_RECOMPENSAS_ESPECIALISTA_GPYR));
                                        WebElement valueTotalTramite_SistemaRecompensas = general.expandContainer("3css", LBL_ID_TABLA_SISTEMA_RECOMPENSAS_ESPECIALISTA_GPYR, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueTotalTramite_SistemaRecompensas.getText();
                                        System.out.println("Contador tipologia Finalización = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> para contador 0");

                                    }

                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SISTEMA_RECOMPENSAS_ESPECIALISTA_GPYR));
                                    WebElement valueTotalTramite_SistemaRecompensas = general.expandContainer("2css", LBL_ID_TABLA_SISTEMA_RECOMPENSAS_ESPECIALISTA_GPYR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueTotalTramite_SistemaRecompensas.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;

                            case "InsuficienteInformacion":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_INSUFICIENTE_INFORMACION_ESPECIALISTA_GPYR));
                                        WebElement valueTotalTramite_InsuficienteInformacion = general.expandContainer("3css", LBL_ID_TABLA_INSUFICIENTE_INFORMACION_ESPECIALISTA_GPYR, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueTotalTramite_InsuficienteInformacion.getText();
                                        System.out.println("Contador tipologia Finalización = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> para contador 0");

                                    }

                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_INSUFICIENTE_INFORMACION_ESPECIALISTA_GPYR));
                                    WebElement valueTotalTramite_InsuficienteInformacion = general.expandContainer("2css", LBL_ID_TABLA_INSUFICIENTE_INFORMACION_ESPECIALISTA_GPYR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueTotalTramite_InsuficienteInformacion.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }

                                break;
                            case "IncorrectaAplicacionCuotas":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_INCORRECTA_APLICACION_CD_CUOTAS_ESPECIALISTA_GPYR));
                                        WebElement valueTotalTramite_IncorrectaAplicacionCuotas = general.expandContainer("3css", LBL_ID_TABLA_INCORRECTA_APLICACION_CD_CUOTAS_ESPECIALISTA_GPYR, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueTotalTramite_IncorrectaAplicacionCuotas.getText();
                                        System.out.println("Contador tipologia Finalización = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> para contador 0");

                                    }

                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_INCORRECTA_APLICACION_CD_CUOTAS_ESPECIALISTA_GPYR));
                                    WebElement valueTotalTramite_IncorrectaAplicacionCuotas = general.expandContainer("2css", LBL_ID_TABLA_INCORRECTA_APLICACION_CD_CUOTAS_ESPECIALISTA_GPYR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueTotalTramite_IncorrectaAplicacionCuotas.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }

                                break;

                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA BANDEJA EN: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SUPERVISOR_GPYR));
                                        WebElement valueTotalTramite = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_GPYR, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueTotalTramite.getText();
                                        System.out.println("Contador tipologia TodosMisTramites = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en TodosMisTramites para contador 0 del Perfil Supervisor Departamento GPYR");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SUPERVISOR_GPYR));
                                    WebElement valueTotalTramite = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_GPYR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueTotalTramite.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "MiEquipo":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR));
                                        WebElement valueMiEquipo = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueMiEquipo.getText();
                                        System.out.println("Contador tipologia MiEquipo = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en MiEquipo para contador 0 del Perfil Supervisor Departamento GPYR");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR));
                                    WebElement valueMiEquipo = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueMiEquipo.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "Asignar":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR));
                                        WebElement valueAsignar = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueAsignar.getText();
                                        System.out.println("Contador tipologia Asignar = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en Asignar para contador 0 del Perfil Supervisor Departamento GPYR");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR));
                                    WebElement valueAsignar = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueAsignar.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "Resolver":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR));
                                        WebElement valueResolver = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueResolver.getText();
                                        System.out.println("Contador tipologia Resolver = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en Resolver para contador 0 del Perfil Supervisor Departamento GPYR");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR));
                                    WebElement valueResolver = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueResolver.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA BANDEJA EN: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                }
                break;
            case "Seguros":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ESPECIALISTA_SEGUROS));
                                        WebElement valueTotalTramite = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueTotalTramite.getText();
                                        System.out.println("Contador tipologia TodosMisTramites = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en TodosMisTramites para contador 0 del Perfil Especialista Departamento Seguros");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ESPECIALISTA_SEGUROS));
                                    WebElement valueTotalTramite = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueTotalTramite.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");

                                }
                                break;
                            case "CobrosIndebidos":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS));
                                        WebElement valueCobrosIndebidos = general.expandContainer("3css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueCobrosIndebidos.getText();
                                        System.out.println("Contador tipologia CobrosIndebidos = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en CobrosIndebidos  para contador 0 del Perfil Especialista Departamento Seguros");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS));
                                    WebElement valueCobrosIndebidos = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueCobrosIndebidos.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "ExoneracionCobros":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS));
                                        WebElement valueExoneracionCobros = general.expandContainer("3css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueExoneracionCobros.getText();
                                        System.out.println("Contador tipologia ExoneracionCobros = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en ExoneracionCobros  para contador 0 del Perfil Especialista Departamento Seguros");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS));
                                    WebElement valueExoneracionCobros = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueExoneracionCobros.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "ConsumosNoReconocidos":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS));
                                        WebElement valueConsumoNoReconocidos = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueConsumoNoReconocidos.getText();
                                        System.out.println("Contador tipologia ConsumoNoReconocidos = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en ConsumoNoReconocidos  para contador 0 del Perfil Especialista Departamento Seguros");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS));
                                    WebElement valueConsumoNoReconocidos = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueConsumoNoReconocidos.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "Monitorear":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS));
                                        WebElement valueMonitorear = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueMonitorear.getText();
                                        System.out.println("Contador tipologia Monitorear = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en Monitorear  para contador 0 del Perfil Especialista Departamento Seguros");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS));
                                    WebElement valueMonitorear = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueMonitorear.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA BANDEJA EN: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SUPERVISOR_SEGUROS));
                                        WebElement valueTotalTramite = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueTotalTramite.getText();
                                        System.out.println("Contador tipologia TodosMisTramites = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en TodosMisTramites para contador 0 del Perfil Supervisor Departamento Seguros");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SUPERVISOR_SEGUROS));
                                    WebElement valueTotalTramite = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueTotalTramite.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "MiEquipo":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS));
                                        WebElement valueMiEquipo = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueMiEquipo.getText();
                                        System.out.println("Contador tipologia MiEquipo = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en MiEquipo para contador 0 del Perfil Supervisor Departamento Seguros");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS));
                                    WebElement valueMiEquipo = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueMiEquipo.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "Asignar":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS));
                                        WebElement valueAsignar = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueAsignar.getText();
                                        System.out.println("Contador tipologia Asignar = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en Asignar para contador 0 del Perfil Supervisor Departamento Seguros");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS));
                                    WebElement valueAsignar = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueAsignar.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "Resolver":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS));
                                        WebElement valueResolver = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueResolver.getText();
                                        System.out.println("Contador tipologia Resolver = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en Resolver para contador 0 del Perfil Supervisor Departamento Seguros");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS));
                                    WebElement valueResolver = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueResolver.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "Monitorear":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS));
                                        WebElement valueMonitorear = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueMonitorear.getText();
                                        System.out.println("Contador tipologia Monitorear = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en Monitorear para contador 0 del Perfil Supervisor Departamento Seguros");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS));
                                    WebElement valueMonitorear = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueMonitorear.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA BANDEJA EN: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                }
                break;
            case "Retenciones":
                break;
            case "Controversias":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS));
                                        WebElement valueTotalTramite = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueTotalTramite.getText();
                                        System.out.println("Contador tipologia TotalTramite = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en TotalTramite para contador 0 del Perfil Especialista Departamento Controversias");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS));
                                    WebElement valueTotalTramite = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueTotalTramite.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "ConsumosNoReconocidos":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS));
                                        WebElement valueConsumosNoReconocidos = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueConsumosNoReconocidos.getText();
                                        System.out.println("Contador tipologia ConsumosNoReconocidos = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en ConsumosNoReconocidos para contador 0 del Perfil Especialista Departamento Controversias");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS));
                                    WebElement valueConsumosNoReconocidos = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueConsumosNoReconocidos.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "ConsumosMalProcesados":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS));
                                        WebElement valueConsumosMalProcesados = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueConsumosMalProcesados.getText();
                                        System.out.println("Contador tipologia ConsumosMalProcesados = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en ConsumosMalProcesados para contador 0 del Perfil Especialista Departamento Controversias");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS));
                                    WebElement valueConsumosMalProcesados = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueConsumosMalProcesados.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "Monitorear":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS));
                                        WebElement valueMonitorear = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueMonitorear.getText();
                                        System.out.println("Contador tipologia Monitorear = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en Monitorear para contador 0 del Perfil Especialista Departamento Controversias");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS));
                                    WebElement valueMonitorear = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueMonitorear.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "TransaccionesEnProceso":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS));
                                        WebElement valueTransaccionesEnProceso = general.expandContainer("3css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueTransaccionesEnProceso.getText();
                                        System.out.println("Contador tipologia TransaccionesEnProceso = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en TransaccionesEnProceso para contador 0 del Perfil Especialista Departamento Controversias");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS));
                                    WebElement valueTransaccionesEnProceso = general.expandContainer("2css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueTransaccionesEnProceso.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA BANDEJA EN: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS));
                                        WebElement valueTotalTramite = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueTotalTramite.getText();
                                        System.out.println("Contador tipologia TotalTramite = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en TotalTramite para contador 0 del Perfil Supervisor Departamento Controversias");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS));
                                    WebElement valueTotalTramite = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueTotalTramite.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "MiEquipo":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS));
                                        WebElement valueMiEquipo = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueMiEquipo.getText();
                                        System.out.println("Contador tipologia MiEquipo = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en MiEquipo para contador 0 del Perfil Supervisor Departamento Controversias");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS));
                                    WebElement valueMiEquipo = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueMiEquipo.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "Asignar":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS));
                                        WebElement valueAsignar = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueAsignar.getText();
                                        System.out.println("Contador tipologia Asignar = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en Asignar para contador 0 del Perfil Supervisor Departamento Controversias");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS));
                                    WebElement valueAsignar = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueAsignar.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "Resolver":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS));
                                        WebElement valueResolver = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueResolver.getText();
                                        System.out.println("Contador tipologia Resolver = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en Resolver para contador 0 del Perfil Supervisor Departamento Controversias");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS));
                                    WebElement valueResolver = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueResolver.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "Monitorear":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS));
                                        WebElement valueMonitorear = general.expandContainer("3css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueMonitorear.getText();
                                        System.out.println("Contador tipologia Monitorear = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en Monitorear para contador 0 del Perfil Supervisor Departamento Controversias");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS));
                                    WebElement valueMonitorear = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueMonitorear.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "TransaccionesEnProceso":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS));
                                        WebElement valueTransaccionesEnProceso = general.expandContainer("3css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueTransaccionesEnProceso.getText();
                                        System.out.println("Contador tipologia TransaccionesEnProceso = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en TransaccionesEnProceso para contador 0 del Perfil Supervisor Departamento Controversias");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS));
                                    WebElement valueTransaccionesEnProceso = general.expandContainer("2css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueTransaccionesEnProceso.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA BANDEJA EN: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                }
                break;
            case "USPR":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ESPECIALISTA_USPR));
                                        WebElement valueTotalTramite = general.expandContainer("3css", LBL_ID_TABLA_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueTotalTramite.getText();
                                        System.out.println("Contador tipologia TotalTramite = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en TotalTramite para contador 0 del Perfil Especialista Departamento USPR");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ESPECIALISTA_USPR));
                                    WebElement valueTotalTramite = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_USPR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueTotalTramite.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "CobrosIndebidos":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR));
                                        WebElement valueCobrosIndebidos = general.expandContainer("3css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueCobrosIndebidos.getText();
                                        System.out.println("Contador tipologia CobrosIndebidos = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en CobrosIndebidos para contador 0 del Perfil Especialista Departamento USPR");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR));
                                    WebElement valueCobrosIndebidos = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueCobrosIndebidos.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "ExoneracionCobros":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR));
                                        WebElement valueExoneracionCobros = general.expandContainer("3css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueExoneracionCobros.getText();
                                        System.out.println("Contador tipologia ExoneracionCobros = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en ExoneracionCobros para contador 0 del Perfil Especialista Departamento USPR");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR));
                                    WebElement valueExoneracionCobros = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueExoneracionCobros.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "ConsumosMalProcesados":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR));
                                        WebElement valueConsumosMalProcesados = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueConsumosMalProcesados.getText();
                                        System.out.println("Contador tipologia ConsumosMalProcesados = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en ConsumosMalProcesados para contador 0 del Perfil Especialista Departamento USPR");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR));
                                    WebElement valueConsumosMalProcesados = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueConsumosMalProcesados.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;

                            case "ConsumosNoReconocidos":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR));
                                        WebElement valueConsumosNoReconocidos = general.expandContainer("3css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueConsumosNoReconocidos.getText();
                                        System.out.println("Contador tipologia ConsumosNoReconocidos = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en ConsumosNoReconocidos para contador 0 del Perfil Especialista Departamento USPR");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR));
                                    WebElement valueConsumosNoReconocidos = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueConsumosNoReconocidos.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "TransaccionMalNoProcesada":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_TRANSACCION_MAL_NO_PROCESADA_ESPECIALISTA_USPR));
                                        WebElement valueTransaccionMalNoProcesada = general.expandContainer("3css", LBL_ID_TABLA_TRANSACCION_MAL_NO_PROCESADA_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueTransaccionMalNoProcesada.getText();
                                        System.out.println("Contador tipologia ConsumosNoReconocidos = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en ConsumosNoReconocidos para contador 0 del Perfil Especialista Departamento USPR");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_TRANSACCION_MAL_NO_PROCESADA_ESPECIALISTA_USPR));
                                    WebElement valueTransaccionMalNoProcesada = general.expandContainer("2css", LBL_ID_TABLA_TRANSACCION_MAL_NO_PROCESADA_ESPECIALISTA_USPR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueTransaccionMalNoProcesada.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "DuplicadoEstadoCuentaSC":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_DUPLICADO_ESTADO_CUENTA_SC_ESPECIALISTA_USPR));
                                        WebElement valueDuplicadoEstadoCuentaSC = general.expandContainer("3css", LBL_ID_TABLA_DUPLICADO_ESTADO_CUENTA_SC_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueDuplicadoEstadoCuentaSC.getText();
                                        System.out.println("Contador tipologia ConsumosNoReconocidos = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en ConsumosNoReconocidos para contador 0 del Perfil Especialista Departamento USPR");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_DUPLICADO_ESTADO_CUENTA_SC_ESPECIALISTA_USPR));
                                    WebElement valueDuplicadoEstadoCuentaSC = general.expandContainer("2css", LBL_ID_TABLA_DUPLICADO_ESTADO_CUENTA_SC_ESPECIALISTA_USPR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueDuplicadoEstadoCuentaSC.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "SistemaRecompensas":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SISTEMA_RECOMPENSAS_ESPECIALISTA_USPR));
                                        WebElement valueSistemaRecompensas = general.expandContainer("3css", LBL_ID_TABLA_SISTEMA_RECOMPENSAS_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueSistemaRecompensas.getText();
                                        System.out.println("Contador tipologia ConsumosNoReconocidos = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en ConsumosNoReconocidos para contador 0 del Perfil Especialista Departamento USPR");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SISTEMA_RECOMPENSAS_ESPECIALISTA_USPR));
                                    WebElement valueSistemaRecompensas = general.expandContainer("2css", LBL_ID_TABLA_SISTEMA_RECOMPENSAS_ESPECIALISTA_USPR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueSistemaRecompensas.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "DesacuerdoCondicionesTT":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_DESACUERDO_CONDICIONES_TT_ESPECIALISTA_USPR));
                                        WebElement valueDesacuerdoCondicionesTT = general.expandContainer("3css", LBL_ID_TABLA_DESACUERDO_CONDICIONES_TT_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueDesacuerdoCondicionesTT.getText();
                                        System.out.println("Contador tipologia ConsumosNoReconocidos = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en ConsumosNoReconocidos para contador 0 del Perfil Especialista Departamento USPR");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_DESACUERDO_CONDICIONES_TT_ESPECIALISTA_USPR));
                                    WebElement valueDesacuerdoCondicionesTT = general.expandContainer("2css", LBL_ID_TABLA_DESACUERDO_CONDICIONES_TT_ESPECIALISTA_USPR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueDesacuerdoCondicionesTT.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;

                            case "Retipificar":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR));
                                        WebElement valueRetipificar = general.expandContainer("3css", LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueRetipificar.getText();
                                        System.out.println("Contador tipologia Retipificar = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en Retipificar para contador 0 del Perfil Especialista Departamento USPR");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR));
                                    WebElement valueRetipificar = general.expandContainer("2css", LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueRetipificar.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA BANDEJA EN: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SUPERVISOR_USPR));
                                        WebElement valueTotalTramite = general.expandContainer("3css", LBL_ID_TABLA_SUPERVISOR_USPR, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueTotalTramite.getText();
                                        System.out.println("Contador tipologia TotalTramite = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en TotalTramite para contador 0 del Perfil Supervisor Departamento USPR");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SUPERVISOR_USPR));
                                    WebElement valueTotalTramite = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_USPR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueTotalTramite.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "MiEquipo":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR));
                                        WebElement valueMiEquipo = general.expandContainer("3css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueMiEquipo.getText();
                                        System.out.println("Contador tipologia MiEquipo = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en MiEquipo para contador 0 del Perfil Supervisor Departamento USPR");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR));
                                    WebElement valueMiEquipo = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueMiEquipo.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "Asignar":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR));
                                        WebElement valueAsignar = general.expandContainer("3css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueAsignar.getText();
                                        System.out.println("Contador tipologia Asignar = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en Asignar para contador 0 del Perfil Supervisor Departamento USPR");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR));
                                    WebElement valueAsignar = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueAsignar.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            case "Resolver":
                                if (TotalTramitesEsCERO == true) {
                                    try {
                                        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR));
                                        WebElement valueResolver = general.expandContainer("3css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR, "ibk-grid", "ibk-h", "p", "");
                                        getValueContadorDerecho = valueResolver.getText();
                                        System.out.println("Contador tipologia Resolver = 0 muestra mensaje satisfactorio " + getValueContadorDerecho + " del Perfil: < " + perfil + " > Departamento: < " + departamento + " >");

                                    } catch (Exception e) {
                                        getValueContadorDerecho = "FALLO";
                                        System.out.println("No se encontró mensaje <No se encontraron coincidencias> en Resolver para contador 0 del Perfil Supervisor Departamento USPR");

                                    }
                                } else {
                                    waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR));
                                    WebElement valueResolver = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR, "ibk-pagination-group2", "div > span > b:nth-child(2)", "", "");
                                    getValueContadorDerecho = valueResolver.getText();
                                    System.out.println("\n**************************************************************\n\t\t\t\t\tNumero total tramite: " + getValueContadorDerecho + " \n**************************************************************\nDel Usuario: " + usuarioSesion + " Perfil: " + perfil + " Departamento: " + departamento + "\n**************************************************************\n\n");
                                }
                                break;
                            default:
                                System.out.println("NO ENCUENTRA TIPOLOGIA BANDEJA EN: " + " Departamento: < " + departamento + " > Perfil: < " + perfil + " > Tipologia: < " + tipologia + " >");
                        }
                        break;
                }
                break;
        }

        return getValueContadorDerecho;
    }


}
