package com.everis.giru.userinterface.login;

import com.everis.giru.utils.Shadow;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class ButtonSalirPage extends PageObject {

    Shadow general = new Shadow();

    @FindBy(xpath = "//ibk-giru-header[@id='header__giru']")
    WebElement BTN_PERFIL;

    public void cerrarSesion() {
        waitForCondition().until(ExpectedConditions.visibilityOf(BTN_PERFIL));
        WebElement btnPerfil = general.expandContainer("2css", BTN_PERFIL, "ibk-menu", "div", "", "");
        btnPerfil.click();

        waitForCondition().until(ExpectedConditions.visibilityOf(BTN_PERFIL));
        WebElement btnSalir = general.expandContainer("2css", BTN_PERFIL, "ibk-menu", "ibk-link", "", "");
        btnSalir.click();
        
    }


}
