package com.everis.giru.userinterface.login;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

/**
 * @author Nilo Carrion
 * @author Renato Paco
 */
public class LoginPage {

    public static final Target INP_USUARIO = Target.the("Usuario").located(By.id("username"));
    public static final Target INP_PASSWORD = Target.the("Contraseña").located(By.id("password"));
    public static final Target BTN_INICIAR_SESION = Target.the("Botón iniciar sesion").located(By.cssSelector("ibk-button[class='hydrated']"));

    public static final Target LBL_ASESOR = Target.the("Perfil asesor").located(By.cssSelector(".title_section.o-light"));
    public static final Target LBL_ASESOR_RETENCION = Target.the("Perfil asesor retencion").located(By.cssSelector(".title"));
    public static final Target LBL_ESPECIALISTA = Target.the("Perfil especialista").located(By.cssSelector(".title"));

}
