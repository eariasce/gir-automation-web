package com.everis.giru.userinterface.atencionCliente;

import com.everis.giru.rest.steps.ConsultaFilterInboxSteps;
import com.everis.giru.tasks.login.CerrarGiru;
import com.everis.giru.tasks.tramites.IniciarSesionReasignado;
import com.everis.giru.utils.Shadow;
import net.serenitybdd.core.Serenity;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import java.time.Duration;

import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class ValidacionesRegistroPage extends PageObject {

    Shadow general = new Shadow();

    @FindBy(xpath = "//ibk-grid-group")
    WebElement elementoBaseMensajeTramites;

    @FindBy(xpath = "//ibk-select")
    WebElement elementoBaseServicios;

    @FindBy(xpath = "//*[@formcontrolname='originBalance']")
    WebElement elementoBaseOrigenSaldo;

    @FindBy(xpath = "//ibk-giru-header")
    WebElement botonComercial;

    @FindBy(xpath = "//ibk-inputgroup-select[@formcontrolname='segment']")
    WebElement cmbSegmento;

    ConsultaFilterInboxSteps consultaFilterInboxSteps;

    public void cambioSeccionComercial() {
        WebElement seccionComercial = general.expandContainer("1css", botonComercial, "div > div.options > button.center", "", "", "");
        seccionComercial.click();
    }

    public void seleccionarTipoSegmentoComercio() {

        try {
            Thread.sleep(3000);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        //TODO: SELECCIONA CBO - SEGMENTO
        WebElement cmb = general.expandContainer("2css", cmbSegmento, "ibk-select", "#dropdown-select", "", "");
        cmb.click();
        //TODO: SELECCIONA OPCION - BANCA CORPORATIVA
        WebElement cmbBancaCorporativa = general.expandContainer("2css", cmbSegmento, "ibk-select", "div:nth-child(1) > ul:nth-child(2) > li:nth-child(1)", "", "");
        cmbBancaCorporativa.click();
        //TODO: SELECCIONA OPCION - REGISTRAR
        Serenity.getDriver().findElement(By.id("search-form__btn-validate")).click();

    }

    public void consultarServicioDeAreaAsignada() {
        //Se espera a procesar tramite
        try {
            Thread.sleep(2000);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        String numeroTramite = Serenity.sessionVariableCalled("numeroTramite").toString();

        consultaFilterInboxSteps = new ConsultaFilterInboxSteps();
        consultaFilterInboxSteps.seArmaElCuerpoDePeticionCreacionCliente(numeroTramite);

        for (int i = 1; i < 6; i++) {
            if (consultaFilterInboxSteps.agregoLosEncabezadosEnLaPeticion2() == 200) {
                System.out.println("Nro de intentos que se consulta el servicio de Derivación: " + i);
                break;
            }
        }
        consultaFilterInboxSteps.validoLaRespuestaObtenida();
        consultaFilterInboxSteps.validoAreaAsignada();

        String nroAsesorReasignado = consultaFilterInboxSteps.validoCamposObtenidos();

        Serenity.setSessionVariable("nrotramite").to(numeroTramite);
        Serenity.setSessionVariable("nroAsesorReasignado").to(nroAsesorReasignado);
    }

    public void seleccionarListadoOrigenSaldo(String origenSaldo) {

        String ambiente = Serenity.sessionVariableCalled("ambiente").toString();
        general.expandContainer("2css", elementoBaseOrigenSaldo, "div > ibk-select", "#dropdown-select", "", "").click();

        if (ambiente.equals("STG")) {
            String[] seleccion = origenSaldo.split(" ");
            general.expandContainer("2css", elementoBaseOrigenSaldo, "div > ibk-select", "#" + seleccion[0] + "\\ " + seleccion[1], "", "").click();
        } else {
            if (origenSaldo.equals("Transferencia Interbancaria")) {
                String[] seleccion = "Transferencia Interbancaria".split(" ");
                general.expandContainer("2css", elementoBaseOrigenSaldo, "div > ibk-select", "#" + seleccion[0] + "\\ " + seleccion[1], "", "").click();
            }
            if (origenSaldo.equals("Pago canales")) {
                String[] seleccion = "Pago canales".split(" ");
                general.expandContainer("2css", elementoBaseOrigenSaldo, "div > ibk-select", "#" + seleccion[0] + "\\ " + seleccion[1], "", "").click();
            }
            if (origenSaldo.equals("Devolución Comisión")) {
                String[] seleccion = "Devolución Comisión".split(" ");
                general.expandContainer("2css", elementoBaseOrigenSaldo, "div > ibk-select", "#" + seleccion[0] + "\\ " + seleccion[1], "", "").click();
            }
            if (origenSaldo.equals("Devolución Establecimiento")) {
                String[] seleccion = "Devolución Establecimiento".split(" ");
                general.expandContainer("2css", elementoBaseOrigenSaldo, "div > ibk-select", "#" + seleccion[0] + "\\ " + seleccion[1], "", "").click();
            }
        }


    }

    public void SeleccionarListadoServicios(String servicio) {
        switch (servicio) {
            case "Hotel":
                general.expandContainer("1css", elementoBaseServicios, "#TSVC001", "", "", "").click();
                break;

            case "Tour":
                general.expandContainer("1css", elementoBaseServicios, "#TSVC002", "", "", "").click();
                break;

            case "Vuelos":
                general.expandContainer("1css", elementoBaseServicios, "#TSVC003", "", "", "").click();
                break;

            case "Otros":
                general.expandContainer("1css", elementoBaseServicios, "#TSVC004", "", "", "").click();
                break;

        }
    }


    public void SalirDetalle() {

        try {
            Thread.sleep(3000);

            Actions action = new Actions(getDriver());
            action.moveByOffset(10, 10).click().build().perform();

//            Serenity.getDriver().findElement(By.xpath("//body")).click();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public void cierroSesionEIngresoNuevoUser() {
        //CIERRO SESION
        theActorInTheSpotlight().attemptsTo(new CerrarGiru());

        //INGRESO CON USUARIO REASIGNADO
        theActorInTheSpotlight().attemptsTo(new IniciarSesionReasignado());

    }

    public String guardarNumeroTramite() {
        String valueNumeroTramiteFrontOne;

        //TODO: BUTTON REGISTRAR - TRAMITE
        WebElement btnRegistrar = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(20)).until(ExpectedConditions.elementToBeClickable(By.id("claim-form__btn-submit")));
        btnRegistrar.click();


        Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(60));
        boolean labelTipoTramite = Serenity.getDriver().findElements(By.xpath("//*[@id='no-proceed-section']/div[2]/div[1]/span[2]")).size() > 0;
        if (labelTipoTramite) {
            System.err.println("OBTIENE NUMERO - TRAMITE - ENTRA ID no-proceed-section");
            //TODO: OBTIENE NUMERO TRAMITE DESDE FRONT
            WebElement lblNumeroTramiteFront = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(120)).until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='no-proceed-section']/div[2]/div[1]/span[2]")));
            String valueNumeroTramiteFront = lblNumeroTramiteFront.getText();
            valueNumeroTramiteFrontOne = valueNumeroTramiteFront;
            System.out.println("=======================================\nFRONT REGISTRO: Nº TRÁMITE: " + valueNumeroTramiteFront + "\n=======================================");
            Serenity.takeScreenshot();
        } else {
            System.err.println("OBTIENE NUMERO - TRAMITE - ENTRA ID proceed-section");
            //TODO: OBTIENE NUMERO TRAMITE DESDE FRONT
            WebElement lblNumeroTramiteFront = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(120)).until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='proceed-section']/div[2]/div[1]/span[2]")));
            String valueNumeroTramiteFront = lblNumeroTramiteFront.getText();
            valueNumeroTramiteFrontOne = valueNumeroTramiteFront;
            System.out.println("=======================================\nFRONT REGISTRO: Nº TRÁMITE: " + valueNumeroTramiteFront + "\n=======================================");
            Serenity.takeScreenshot();
        }

        //TODO: SECCIÓN AUTONOMIA
        String autonomia = "";
        try {
            autonomia = Serenity.sessionVariableCalled("autonomia").toString();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        if (autonomia.equals("si")) {
            //TODO: VALIDO TEXTO ABONO DE AUTONOMIA
            Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
            boolean mensajeAutonomia = Serenity.getDriver().findElements(By.xpath("//*[contains(text(), ' . El abono se realizará dentro de 48 horas.')]")).size() > 0;
            if (mensajeAutonomia) {
                System.err.println("OBTIENE MENSAJE 1 REGISTRO AUTONOMIA");
                //TODO: OBTIENE VALOR TEXTO ABONO 1
                WebElement lblAbonoFront = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(120)).until(ExpectedConditions.elementToBeClickable(By.xpath("//*[contains(text(), ' . El abono se realizará dentro de 48 horas.')]")));
                String valueAbonoFront = lblAbonoFront.getText();
                Assert.assertEquals(valueAbonoFront, ". El abono se realizará dentro de 48 horas.");
                System.out.println("MENSAJE ABONO: " + valueAbonoFront);
                Serenity.takeScreenshot();
            } else {
                System.err.println("OBTIENE MENSAJE 2 REGISTRO AUTONOMIA");
                //TODO: OBTIENE VALOR TEXTO ABONO 2
                WebElement lblAbonoFront = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(120)).until(ExpectedConditions.elementToBeClickable(By.xpath("//*[contains(text(), 'El abono se realizará en 48 horas')]")));
                String valueAbonoFront = lblAbonoFront.getText();
                Assert.assertEquals(valueAbonoFront, "· El abono se realizará en 48 horas");
                System.out.println("MENSAJE ABONO: " + valueAbonoFront);
                Serenity.takeScreenshot();
            }
        }

        //TODO: BUTTON TERMINAR - ATENCIÓN
        WebElement btnTerminarAtencion = new WebDriverWait(Serenity.getDriver(), Duration.ofSeconds(120)).until(ExpectedConditions.elementToBeClickable(By.id("end-atention")));
        btnTerminarAtencion.click();

        return valueNumeroTramiteFrontOne;
    }



}
