package com.everis.giru.userinterface.atencionCliente;

import com.everis.giru.rest.steps.ConsultaFilterInboxSteps;
import com.everis.giru.utils.ApiCommons;
import com.everis.giru.utils.Shadow;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class GetListCreditCardsPage extends PageObject {

    ConsultaFilterInboxSteps consultaFilterInboxSteps = new ConsultaFilterInboxSteps();
    Shadow general = new Shadow();

    @FindBy(xpath = "//*[@id='credit-cards-list']/ibk-container[1]")
    WebElement BTN_LIST_CREDIT_CARDS;

    @FindBy(xpath = "//*[@id='Debit-cards']/section/ibk-container")
    WebElement BTN_LIST_DEBIT_CARDS;

    @FindBy(xpath = "//*[@id='Debit-cards']/section/giru-account-client/ibk-container[1]")
    WebElement BTN_LIST_DEBIT_ACTIVA;

    @FindBy(xpath = "//*[@id='account-list']/ibk-container[1]")
    WebElement BTN_LIST_CUENTA;

    @FindBy(xpath = "//*[@id='credit-cards']/div[1]/div[2]/ibk-inputgroup-select")
    WebElement CBOX_CREDITO_PERSONALE;

    @FindBy(xpath = "//*[@id='credit-cards-list']/giru-credit-personal/ibk-container[1]")
    WebElement SECTION_CREDIT_PERSONAL;

    @FindBy(xpath = "//*[@id='credit-cards-list']/giru-credit-personal/ibk-container[2]")
    WebElement SECTION_OTHER_CREDITS;

    @FindBy(xpath = "//*[@id='claim-derive__btn-confirm']")
    WebElement BUTTON_REGISTER;



    @FindBy(xpath = "//*[@id='credit-cards-list']/giru-credit-agreements/ibk-container[1]")
    WebElement SECTION_CREDIT_AGREEMENTS;

    @FindBy(xpath = "//*[@id='credit-cards-list']/giru-insurance-step/ibk-container[1]")
    WebElement SECTION_CREDIT_SEGUROS;

    @FindBy(xpath = "//ibk-inputgroup-text[@id='creditNumber']")
    WebElement TXT_CREDIT_NUMBER;

    @FindBy(xpath = "//ibk-inputgroup-text[@id='productType']")
    WebElement TXT_PRODUCT_TYPE;


    String tipoProducto = Serenity.sessionVariableCalled("tipoproducto").toString();
    String numeroDni = ApiCommons.clientes.getNumerodocumento();

    // TODO: Selecciona una tarjeta de crédito
    public void selectListCardsCredit() {
        String numeroCardsCredit = ApiCommons.clientes.getNumerotarjeta();

        //TODO: SECCIÓN AUTONOMIA
        String autonomia = "";
        try {
            autonomia = Serenity.sessionVariableCalled("autonomia").toString();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(20));
        WebElement CardsCreditTitular = general.expandContainer("2css", BTN_LIST_CREDIT_CARDS, "div > div > ibk-p", "div > p", "", "");
        CardsCreditTitular.click();

        Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
        WebElement getNumeroTarjeta = general.expandContainer("2css", BTN_LIST_CREDIT_CARDS, "div.container-body.collapsed.open > div:nth-child(1) > div.container-detail > ibk-row > ibk-col:nth-child(2) > ibk-giru-text-info", "div > div.name-value > div", "", "");
        String valueNumeroTarjeta = getNumeroTarjeta.getText();
        System.out.println("NUMERO TARJETA CREDITO FRONT: " + valueNumeroTarjeta + " NUMERO TARJETA CREDITO TDM: " + numeroCardsCredit);
        Assert.assertEquals(valueNumeroTarjeta, numeroCardsCredit);

        if (autonomia.equals("si")) {
            try {
                Thread.sleep(1000);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

            // TODO: Validacion por bandeja tarjeta antigua a 6 meses
            consultaFilterInboxSteps.seArmaElCuerpoDePeticionClienteActual(numeroDni);

            for (int i = 1; i < 4; i++) {
                if (consultaFilterInboxSteps.agregoLosEncabezadosEnLaPeticionClientePorDNI(numeroDni) == 200) {
                    System.out.println("Nro de veces que consulta al servicio de Derivación: " + i);
                    break;
                }
            }

            consultaFilterInboxSteps.validoLaRespuestaObtenida();
            String customerId = consultaFilterInboxSteps.obtengoCampoCodigoUnico();

            //Query actualize el segmento y nivel de servicio

            try {
                Thread.sleep(2000);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

            // TODO: Obtener tarjeta cliente
            consultaFilterInboxSteps.agregoLosEncabezadosEnLaPeticionClientePorTarjeta(customerId);
            String fechaRegistroTarjeta = consultaFilterInboxSteps.obtengoFechaTarjeta();

            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Date fechaTarjeta = sdf.parse(fechaRegistroTarjeta);
                System.out.println("Fecha tarjeta: " + fechaRegistroTarjeta);
                DateUtils.addMonths(fechaTarjeta, 6);

                LocalDate dateObj = LocalDate.now();
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                String date = dateObj.format(formatter);
                Date fechaActual = sdf.parse(date);
                System.out.println("Fecha Actual: " + date);


                if (fechaTarjeta.after(fechaActual)) {
                    throw new AssertionError("La tarjeta tiene menos de 6 meses de antiguedad, no aplica autonomia");
                } else {
                    System.out.println("Tarjeta es mayor a 6 meses, procede autonomia");
                }
            } catch (Exception e) {
                throw new AssertionError(e.getMessage());
            }

        }

        WebElement btnCardsCredit = general.expandContainer("2css", BTN_LIST_CREDIT_CARDS, "ibk-button", "button", "", "");
        btnCardsCredit.click();
    }

    // TODO: Seleccione una tarjeta de débito
    public void selectListCardsDebit() {
        String numeroCardsCredit = ApiCommons.clientes.getNumerotarjeta();
        String numeroCuenta = ApiCommons.clientes.getNumerocuenta();

        try {
            Thread.sleep(3000);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        WebElement CardsDebitTitular = general.expandContainer("2css", BTN_LIST_DEBIT_CARDS, "div > div > ibk-p", "div > p", "", "");
        CardsDebitTitular.click();

        Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
        WebElement getNumeroTarjeta = general.expandContainer("2css", BTN_LIST_DEBIT_CARDS, "div.container-body.collapsed.open > div > div.container-detail > ibk-row > ibk-col:nth-child(1) > ibk-giru-text-info", "div > div.name-value > div", "", "");
        String valueNumeroTarjeta = getNumeroTarjeta.getText();
        System.out.println("NUMERO TARJETA DEBITO FRONT: " + valueNumeroTarjeta + " NUMERO TARJETA DEBITO TDM: " + numeroCardsCredit);
        Assert.assertEquals(valueNumeroTarjeta, numeroCardsCredit);


        //////
        //TODO: SECCIÓN AUTONOMIA
        String autonomia = "";
        try {
            autonomia = Serenity.sessionVariableCalled("autonomia").toString();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        if (autonomia.equals("si")) {
            try {
                Thread.sleep(1000);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

            // TODO: Validacion por bandeja tarjeta antigua a 6 meses
            consultaFilterInboxSteps.seArmaElCuerpoDePeticionClienteActual(numeroDni);

            for (int i = 1; i < 4; i++) {
                if (consultaFilterInboxSteps.agregoLosEncabezadosEnLaPeticionClientePorDNI(numeroDni) == 200) {
                    System.out.println("Nro de veces que consulta al servicio de Derivación: " + i);
                    break;
                }
            }

            consultaFilterInboxSteps.validoLaRespuestaObtenida();
            String customerId = consultaFilterInboxSteps.obtengoCampoCodigoUnico();
            try {
                Thread.sleep(2000);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

            // TODO: Obtener tarjeta cliente
            consultaFilterInboxSteps.agregoLosEncabezadosEnLaPeticionClientePorTarjeta(customerId);
            String fechaRegistroTarjeta = consultaFilterInboxSteps.obtengoFechaTarjeta();

            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Date fechaTarjeta = sdf.parse(fechaRegistroTarjeta);
                System.out.println("Fecha tarjeta: " + fechaRegistroTarjeta);
                DateUtils.addMonths(fechaTarjeta, 6);

                LocalDate dateObj = LocalDate.now();
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                String date = dateObj.format(formatter);
                Date fechaActual = sdf.parse(date);
                System.out.println("Fecha Actual: " + date);


                if (fechaTarjeta.after(fechaActual)) {
                    throw new AssertionError("La tarjeta tiene menos de 6 meses de antiguedad, no aplica autonomia");
                } else {
                    System.out.println("Tarjeta es mayor a 6 meses, procede autonomia");
                }
            } catch (Exception e) {
                throw new AssertionError(e.getMessage());
            }

        }
        //TODO: Seleccione una tarjeta de débito
        WebElement btnSeleccionar = general.expandContainer("2css", BTN_LIST_DEBIT_CARDS, "ibk-button", "button", "", "");
        btnSeleccionar.click();
        //TODO: Selecciona una cuenta
        Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
        WebElement cuentasActiva = general.expandContainer("2css", BTN_LIST_DEBIT_ACTIVA, "div > div > ibk-p", "div > p", "", "");
        cuentasActiva.click();

        Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
        WebElement getNumeroCuenta = general.expandContainer("2css", BTN_LIST_DEBIT_ACTIVA, "div.container-body.collapsed.open > div:nth-child(1) > div.container-detail > ibk-row > ibk-col:nth-child(4) > ibk-giru-text-info", "div > div.name-value > div", "", "");
        String valueNumeroCuenta = getNumeroCuenta.getText();
        System.out.println("NUMERO CUENTA FRONT: " + valueNumeroCuenta + " NUMERO CUENTA TDM: " + numeroCuenta);
        Assert.assertEquals(valueNumeroCuenta, numeroCuenta);
        //TODO: Registrar Tramite
        WebElement btnRegistrar = general.expandContainer("2css", BTN_LIST_DEBIT_ACTIVA, "ibk-button", "button", "", "");
        btnRegistrar.click();
    }

    // TODO: Selecciona una cuenta
    public void selectListCuenta() {
        String estadocuenta = ApiCommons.clientes.getEstadoproducto();
        String numeroCuenta = ApiCommons.clientes.getNumerocuenta();

        // TODO: Cuentas Activas
        if (estadocuenta.equals("ACTIVA")) {
            Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(20));
            WebElement cuentasActiva = general.expandContainer("2css", BTN_LIST_CUENTA, "div > div > ibk-p", "div > p", "", "");
            cuentasActiva.click();

            Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
            WebElement getNumeroCuenta = general.expandContainer("2css", BTN_LIST_CUENTA, "div.container-body.collapsed.open > div:nth-child(1) > div.container-detail > ibk-row > ibk-col:nth-child(3) > ibk-giru-text-info", "div > div.name-value > div", "", "");
            String valueNumeroCuenta = getNumeroCuenta.getText();
            System.out.println("NUMERO CUENTA FRONT: " + valueNumeroCuenta + " NUMERO CUENTA TDM: " + numeroCuenta);
            Assert.assertEquals(valueNumeroCuenta, numeroCuenta);

            WebElement btnCuenta = general.expandContainer("2css", BTN_LIST_CUENTA, "ibk-button", "button", "", "");
            btnCuenta.click();
        }

    }

    // TODO: Selecciona un producto de seguro
    public void selectListSeguro() {
        String estadocuenta = ApiCommons.clientes.getEstadoproducto();
        String numeroCuenta =ApiCommons.clientes.getNumerocuenta();

        //TODO: Selecciona - Seguros
        Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        WebElement selectSeguros = general.expandContainer("1css", SECTION_CREDIT_SEGUROS, "div > div", "", "", "");
        selectSeguros.click();

        if (estadocuenta.equals("ACTIVA")) {
            Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(20));
            WebElement getNumeroPoliza = general.expandContainer("2css", SECTION_CREDIT_SEGUROS, "div > div > div.container-detail > ibk-row > ibk-col:nth-child(2) > ibk-giru-text-info", "div > div.name-value > div", "", "");
            String valueNumeroPoliza = getNumeroPoliza.getText();
            System.out.println("NUMERO POLIZA FRONT: " + valueNumeroPoliza + " NUMERO TARJETA TDM: " + numeroCuenta);
            Assert.assertEquals(valueNumeroPoliza, numeroCuenta);

            //TODO: Selecciona - Buton Registrar
            WebElement btnSeguros = general.expandContainer("2css", SECTION_CREDIT_SEGUROS, "ibk-button", "button", "", "");
            btnSeguros.click();
        }

    }

    public void selectListCardAdelantoSueldo() {
        //TODO: Selecciona - Créditos
        Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        WebElement selectAdelantoSueldo = general.expandContainer("1css", SECTION_CREDIT_AGREEMENTS, "div > div", "", "", "");
        selectAdelantoSueldo.click();
        //TODO: Selecciona - Buton Registrar
        Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(20));
        WebElement btnAdelantoSueldo = general.expandContainer("2css", SECTION_CREDIT_AGREEMENTS, "div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > ibk-button:nth-child(1)", "button", "", "");
        btnAdelantoSueldo.click();
    }

    public void selectListCardsLineaConvenio() {
        //TODO: Selecciona - Créditos
        Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        WebElement selectLineaConvenio = general.expandContainer("1css", SECTION_CREDIT_AGREEMENTS, "div > div", "", "", "");
        selectLineaConvenio.click();
        //TODO: Selecciona - Buton Registrar
        Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(20));
        WebElement btnLineaConvenio = general.expandContainer("2css", SECTION_CREDIT_AGREEMENTS, "div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > ibk-button:nth-child(1)", "button", "", "");
        btnLineaConvenio.click();
    }

    public void selectListCreditosPersonales() {

        try {
            Thread.sleep(3000);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        WebElement selectCredito = general.expandContainer("2css", CBOX_CREDITO_PERSONALE, "div > ibk-select", "#dropdown-select > div", "", "");
        selectCredito.click();

        if (tipoProducto.equals("HIPOTECARIO")) {
            //TODO: Selecciona - dropdown
            WebElement selectHipotecario = general.expandContainer("2css", CBOX_CREDITO_PERSONALE, "div > ibk-select", "div:nth-child(1) > ul:nth-child(2) > li:nth-child(1)", "", "");
            selectHipotecario.click();
           /* //TODO: Selecciona - Créditos
            Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
            WebElement selectCreditos = general.expandContainer("1css", SECTION_CREDIT_PERSONAL, "div > div", "", "", "");
            selectCreditos.click();
            //TODO: Selecciona - Buton Registrar
            Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
            WebElement btnHipotecario = general.expandContainer("2css", SECTION_CREDIT_PERSONAL, "#claim-derive__btn-confirm", "button", "", "");
            btnHipotecario.click();*/
            Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
            WebElement selectCreditos = general.expandContainer("1css", SECTION_OTHER_CREDITS, "div > div", "", "", "");
            selectCreditos.click();

            WebElement textCreditos = general.expandContainer("2css", TXT_CREDIT_NUMBER, "div > ibk-input", "#Nº\\ DE\\ CRÉDITO", "", "");
            textCreditos.sendKeys("0004521");


            WebElement txtTipoProducto = general.expandContainer("2css", TXT_PRODUCT_TYPE, "div > ibk-input", "#TIPO\\ DE\\ PRODUCTO", "", "");
            txtTipoProducto.sendKeys("MICROPRESTAMOS");

            Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
            WebElement btnVehicular = general.expandContainer("1css", BUTTON_REGISTER, "button", "", "", "");
            btnVehicular.click();
        }

        if (tipoProducto.equals("EFECTIVO CON GARANTÍA HIPOTECARIO")) {
            //TODO: Selecciona - dropdown
            WebElement selectEfectivoGarantiaHipotecario = general.expandContainer("2css", CBOX_CREDITO_PERSONALE, "div > ibk-select", "div:nth-child(1) > ul:nth-child(2) > li:nth-child(2)", "", "");
            selectEfectivoGarantiaHipotecario.click();
          /*  //TODO: Selecciona - Créditos
            Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
            WebElement selectCreditos = general.expandContainer("1css", SECTION_CREDIT_PERSONAL, "div > div", "", "", "");
            selectCreditos.click();
            //TODO: Selecciona - Buton Registrar
            Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
            WebElement btnEfectivoGarantiaHipotecario = general.expandContainer("2css", SECTION_CREDIT_PERSONAL, "#claim-derive__btn-confirm", "button", "", "");
            btnEfectivoGarantiaHipotecario.click();*/

            Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
            WebElement selectCreditos = general.expandContainer("1css", SECTION_OTHER_CREDITS, "div > div", "", "", "");
            selectCreditos.click();

            WebElement textCreditos = general.expandContainer("2css", TXT_CREDIT_NUMBER, "div > ibk-input", "#Nº\\ DE\\ CRÉDITO", "", "");
            textCreditos.sendKeys("0004521");


            WebElement txtTipoProducto = general.expandContainer("2css", TXT_PRODUCT_TYPE, "div > ibk-input", "#TIPO\\ DE\\ PRODUCTO", "", "");
            txtTipoProducto.sendKeys("MICROPRESTAMOS");

            Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
            WebElement btnVehicular = general.expandContainer("1css", BUTTON_REGISTER, "button", "", "", "");
            btnVehicular.click();
        }

        if (tipoProducto.equals("PRÉSTAMO PERSONAL")) {
            //TODO: Selecciona - dropdown
            WebElement selectPrestamoPersonal = general.expandContainer("2css", CBOX_CREDITO_PERSONALE, "div > ibk-select", "div:nth-child(1) > ul:nth-child(2) > li:nth-child(3)", "", "");
            selectPrestamoPersonal.click();
          /*  //TODO: Selecciona - Créditos
            Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
            WebElement selectCreditos = general.expandContainer("1css", SECTION_CREDIT_PERSONAL, "div > div", "", "", "");
            selectCreditos.click();
            //TODO: Selecciona - Buton Registrar
            Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
            WebElement btnPrestamoPersonal = general.expandContainer("2css", SECTION_CREDIT_PERSONAL, "#claim-derive__btn-confirm", "button", "", "");
            btnPrestamoPersonal.click();*/

            Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
            WebElement selectCreditos = general.expandContainer("1css", SECTION_OTHER_CREDITS, "div > div", "", "", "");
            selectCreditos.click();

            WebElement textCreditos = general.expandContainer("2css", TXT_CREDIT_NUMBER, "div > ibk-input", "#Nº\\ DE\\ CRÉDITO", "", "");
            textCreditos.sendKeys("0004521");


            WebElement txtTipoProducto = general.expandContainer("2css", TXT_PRODUCT_TYPE, "div > ibk-input", "#TIPO\\ DE\\ PRODUCTO", "", "");
            txtTipoProducto.sendKeys("MICROPRESTAMOS");

            Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
            WebElement btnVehicular = general.expandContainer("1css", BUTTON_REGISTER, "button", "", "", "");
            btnVehicular.click();
        }

        if (tipoProducto.equals("VEHICULAR")) {
            //TODO: Selecciona - dropdown
            WebElement selectVehicular = general.expandContainer("2css", CBOX_CREDITO_PERSONALE, "div > ibk-select", "div:nth-child(1) > ul:nth-child(2) > li:nth-child(4)", "", "");
            selectVehicular.click();
           /*
            //TODO: Selecciona - Créditos
            Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
            WebElement selectCreditos = general.expandContainer("1css", SECTION_CREDIT_PERSONAL, "div > div", "", "", "");
            selectCreditos.click();

            //TODO: Selecciona - Buton Registrar
            Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
            WebElement btnVehicular = general.expandContainer("2css", SECTION_CREDIT_PERSONAL, "#claim-derive__btn-confirm", "button", "", "");
            btnVehicular.click();*/

            Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
            WebElement selectCreditos = general.expandContainer("1css", SECTION_OTHER_CREDITS, "div > div", "", "", "");
            selectCreditos.click();

            WebElement textCreditos = general.expandContainer("2css", TXT_CREDIT_NUMBER, "div > ibk-input", "#Nº\\ DE\\ CRÉDITO", "", "");
            textCreditos.sendKeys("0004521");


            WebElement txtTipoProducto = general.expandContainer("2css", TXT_PRODUCT_TYPE, "div > ibk-input", "#TIPO\\ DE\\ PRODUCTO", "", "");
            txtTipoProducto.sendKeys("MICROPRESTAMOS");

            Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
            WebElement btnVehicular = general.expandContainer("1css", BUTTON_REGISTER, "button", "", "", "");
            btnVehicular.click();

           // Actions actionobj = new Actions(Serenity.getDriver());
          //  actionobj.moveByOffset(btnVehicular.getLocation().getX()+5,btnVehicular.getLocation().getY()+5).build().perform();


        }

        if (tipoProducto.equals("REFINANCIADO")) {
            //TODO: Selecciona - dropdown
            WebElement selectRefinanciado = general.expandContainer("2css", CBOX_CREDITO_PERSONALE, "div > ibk-select", "div:nth-child(1) > ul:nth-child(2) > li:nth-child(5)", "", "");
            selectRefinanciado.click();
            /*//TODO: Selecciona - Créditos
            Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
            WebElement selectCreditos = general.expandContainer("1css", SECTION_CREDIT_PERSONAL, "div > div", "", "", "");
            selectCreditos.click();
            //TODO: Selecciona - Buton Registrar
            Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
            WebElement btnRefinanciado = general.expandContainer("2css", SECTION_CREDIT_PERSONAL, "#claim-derive__btn-confirm", "button", "", "");
            btnRefinanciado.click();*/

            Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
            WebElement selectCreditos = general.expandContainer("1css", SECTION_OTHER_CREDITS, "div > div", "", "", "");
            selectCreditos.click();

            WebElement textCreditos = general.expandContainer("2css", TXT_CREDIT_NUMBER, "div > ibk-input", "#Nº\\ DE\\ CRÉDITO", "", "");
            textCreditos.sendKeys("0004521");


            WebElement txtTipoProducto = general.expandContainer("2css", TXT_PRODUCT_TYPE, "div > ibk-input", "#TIPO\\ DE\\ PRODUCTO", "", "");
            txtTipoProducto.sendKeys("MICROPRESTAMOS");

            Serenity.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
            WebElement btnVehicular = general.expandContainer("1css", BUTTON_REGISTER, "button", "", "", "");
            btnVehicular.click();
        }

    }


}
