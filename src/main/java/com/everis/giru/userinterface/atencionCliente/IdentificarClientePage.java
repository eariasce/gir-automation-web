package com.everis.giru.userinterface.atencionCliente;

import com.everis.giru.utils.Shadow;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class IdentificarClientePage extends PageObject {

    private Shadow general = new Shadow();

    @FindBy(xpath = "(//ibk-inputgroup-select[@formcontrolname='documentType'])")
    WebElement CBX_TIPO_DOCUMENTO;

    @FindBy(xpath = "(//ibk-inputgroup-text[@formcontrolname='numberDocument'])")
    WebElement INPUT_NUMERO_DOCUMENTO;



    public void seleccionarTipoDocumento(String tipoDocumento) {
        waitForCondition().until(ExpectedConditions.visibilityOf(CBX_TIPO_DOCUMENTO));
        WebElement tipoProducto = general.expandContainer("2css", CBX_TIPO_DOCUMENTO, "div > ibk-select", "#dropdown-select", "", "");
        tipoProducto.click();

        if (tipoDocumento.equals("RUC")) {
            waitForCondition().until(ExpectedConditions.visibilityOf(CBX_TIPO_DOCUMENTO));
            WebElement tipoProductoCE = general.expandContainer("2css", CBX_TIPO_DOCUMENTO, "div > ibk-select", "#RUC", "", "");
            tipoProductoCE.click();
        }

        //TODO: DOCUMENTOS GIRU RETAIL
        if (tipoDocumento.equals("PTP")) {
            waitForCondition().until(ExpectedConditions.visibilityOf(CBX_TIPO_DOCUMENTO));
            WebElement tipoDocumentoPTP = general.expandContainer("2css", CBX_TIPO_DOCUMENTO, "div > ibk-select", "#PTP", "", "");
            tipoDocumentoPTP.click();
        }

        if (tipoDocumento.equals("PASAPORTE")) {
            waitForCondition().until(ExpectedConditions.visibilityOf(CBX_TIPO_DOCUMENTO));
            WebElement tipoDocumentoPasaporte = general.expandContainer("2css", CBX_TIPO_DOCUMENTO, "div > ibk-select", "#PASAPORTE", "", "");
            tipoDocumentoPasaporte.click();
        }

        if (tipoDocumento.equals("DNI")) {
            waitForCondition().until(ExpectedConditions.visibilityOf(CBX_TIPO_DOCUMENTO));
            WebElement tipoDocumentoDNI = general.expandContainer("2css", CBX_TIPO_DOCUMENTO, "div > ibk-select", "#DNI", "", "");
            tipoDocumentoDNI.click();
        }

        if (tipoDocumento.equals("CE")) {
            waitForCondition().until(ExpectedConditions.visibilityOf(CBX_TIPO_DOCUMENTO));
            WebElement tipoDocumentoCE = general.expandContainer("2css", CBX_TIPO_DOCUMENTO, "div > ibk-select", "#CE", "", "");
            tipoDocumentoCE.click();
        }

    }

    public void ingresarDocumento(String nroDocumento) {
        WebElement inputDocumento = general.expandContainer("2css", INPUT_NUMERO_DOCUMENTO, "div > ibk-input", "div", "", "");
        inputDocumento.findElement(By.cssSelector("input")).sendKeys(nroDocumento);
    }

}
