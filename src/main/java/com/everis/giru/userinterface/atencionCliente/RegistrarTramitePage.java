package com.everis.giru.userinterface.atencionCliente;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class RegistrarTramitePage {

    //Button Subir Archivo
    public static final Target BTN_SUBIR_ARCHIVO = Target.the("Subir Archivo").located(By.xpath("//giru-upload-files-items/label"));
    
    //Valida color textbox Fecha
    public static final Target TXT_FECHA_ROJO = Target.the("fecha incorrecta").located(By.xpath("//input[@formcontrolname='Fecha'][1]"));
    //Valida color textbox Monto Incorrecto
    public static final Target TXT_MONTO_ROJO = Target.the("monto incorrecta").located(By.xpath("//input[@formcontrolname='Monto'][1] "));
    //Valida campo obligatorio Respuesta
    public static final Target AREA_RESPUESTA_CLIENTE = Target.the("no ingresa respuesta").located(By.xpath("//textarea[@formcontrolname='comments']"));
    //Valida color textbox Numero Documento Incorrecto
    public static final Target TXT_NUMERO_DNI_ROJO = Target.the("numero DNI incorrecto").located(By.xpath("//*[@class='invalid-feedback flex w-100 fs-10 mt-4p c-red-exception h-bold']"));

    //Valida color textbox Numero Documento Incorrecto
    public static final Target TXT_COMENTARIO_BLOQUEADO = Target.the("selecciona campo de texto").located(By.xpath("//*[text()=' * Campo obligatorio.']"));

}
