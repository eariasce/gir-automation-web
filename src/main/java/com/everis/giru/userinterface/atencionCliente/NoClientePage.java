package com.everis.giru.userinterface.atencionCliente;

import com.everis.giru.utils.Shadow;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class NoClientePage extends PageObject {

    private Shadow general = new Shadow();


    @FindBy(xpath = "(//ibk-inputgroup-text[@formcontrolname='firstName'])")
    WebElement INPUT_NOMBRE_COMERCIO;


    public void ingresarNombreComercio(String nomComercio){
        WebElement inputComercio = general.expandContainer("2css", INPUT_NOMBRE_COMERCIO, "div > ibk-input", "div", "", "");
        inputComercio.findElement(By.cssSelector("input")).sendKeys(nomComercio);
    }

}
