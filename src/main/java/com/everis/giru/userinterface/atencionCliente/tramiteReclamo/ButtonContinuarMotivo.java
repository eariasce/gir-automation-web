package com.everis.giru.userinterface.atencionCliente.tramiteReclamo;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class ButtonContinuarMotivo extends PageObject {

    public void clickContinuar(int valorarray) {
        List<WebElement> listacontinuar = getDriver().findElements(By.id("claim-form__footer-continue"));
        listacontinuar.get(valorarray).click();
    }

}
