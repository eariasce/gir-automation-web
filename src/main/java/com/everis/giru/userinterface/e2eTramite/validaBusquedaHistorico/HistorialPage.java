package com.everis.giru.userinterface.e2eTramite.validaBusquedaHistorico;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class HistorialPage {

    //Combo tipo de documento

    public static final Target CBX_TIPO_DOC_HISTORIAL = Target.the("Combo  tipo de documento").located(By.id("search-claim-form__document-type"));

    public static final Target DIV_SELECT_NRO_TRAMITE = Target.the(" N° de tramite").located(By.id("TRAMITE"));
    public static final Target DIV_SELECT_DNI = Target.the("Select DNI").located(By.id("DNI"));
    public static final Target DIV_SELECT_CE = Target.the("Select CE").located(By.id("CE"));
    //Button Buscar
    public static final Target INP_NUMERO_DOC = Target.the("Número de documento").located(By.id("search-claim-form__txt-document-number"));
    public static final Target BTN_BUSCAR = Target.the("Botón buscar").located(By.xpath("//img[@alt='buscar']"));

}
