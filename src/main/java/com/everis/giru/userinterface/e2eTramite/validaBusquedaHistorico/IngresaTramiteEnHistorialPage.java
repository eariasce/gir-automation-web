package com.everis.giru.userinterface.e2eTramite.validaBusquedaHistorico;

import com.everis.giru.utils.Shadow;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.junit.Assert;
import org.openqa.selenium.WebElement;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class IngresaTramiteEnHistorialPage extends PageObject {

    Shadow general = new Shadow();

    @FindBy(xpath = "/html/body/app-root/giru-layout/div/div[3]/giru-general-history/div/div[2]/giru-history-search-form/form/div/div[2]/ibk-inputgroup-text")
    WebElement LBL_ID_TEXTBOX_BUSCARTRAMITE;

    public void ingresarTramite(String numerotramite) {

//            WaitUntil.the((Target) LBL_ID_TEXTBOX_BUSCARTRAMITE, isVisible()).forNoMoreThan(320).seconds();

            WebElement valueTramite = general.expandContainer("2css", LBL_ID_TEXTBOX_BUSCARTRAMITE, "ibk-input", "div > input", "", "");

            valueTramite.sendKeys(numerotramite);



    }


}
