package com.everis.giru.userinterface.e2eTramite;

import com.everis.giru.tasks.e2eTramites.GetHistoricalInbox;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import net.serenitybdd.core.annotations.findby.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class GetNumeroTramitePage extends PageObject {

    GetHistoricalInbox getHistoricalInbox = new GetHistoricalInbox();

    @FindBy(css = "div[class='w-50 flex flex-column'] span[class='parrafo h-bold']")
    WebElement LBL_NUMERO_TRAMITE;

    public void validaObtenerTramiteFront() {
        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_NUMERO_TRAMITE));
        String getNumeroTramite = LBL_NUMERO_TRAMITE.getText();

        getHistoricalInbox.setNumeroTramiteFront(getNumeroTramite);

        System.out.println("Numero tramite Front: " + getNumeroTramite);
    }

    public void validaTerminarAtencion() {
        WebElement btnTerminarAtencion = getDriver().findElement(By.id("end-atention"));
        btnTerminarAtencion.click();
    }

}
