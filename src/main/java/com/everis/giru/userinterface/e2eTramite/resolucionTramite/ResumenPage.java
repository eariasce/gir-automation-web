package com.everis.giru.userinterface.e2eTramite.resolucionTramite;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

/**
 * @author Nilo Carrion
 * @author Renato Paco
 */
public class ResumenPage {

    public static final Target LBL_NUMTARJETA = Target.the("Numero Tarjeta").located(By.xpath("//*[@id=\"procedure-info\"]/div[2]/div[1]/label[2]"));
    public static final Target LBL_TIPOLOGIA = Target.the("Tipologia").located(By.xpath("//*[@id=\"procedure-info\"]/div[2]/div[2]/label[2]"));
    public static final Target LBL_NUMEROTRAMITE = Target.the("Numero Tramite").located(By.xpath("//*[@id=\"procedure-info\"]/div[3]/div[2]/label[2]"));
    public static final Target BTN_GESTIONAR_TRAMITE = Target.the("Boton Gestionar").located(By.cssSelector(".toggle-action.hide"));
    public static final Target BTN_PROCEDE = Target.the("Boton Procede").located(By.xpath("//*[@id=\"claim-detail-tabs__buttons\"]/div/div/div/div[2]/div[2]/div[1]/div"));
    public static final Target BTN_PROCEDE_CONFIRMAR = Target.the("Boton Confirmar Procede").located(By.xpath("//*[@id=\"add-email__btn-add\"]"));

    public static final Target BTN_NOPROCEDE = Target.the("Boton No Procede").located(By.xpath("//*[@id=\"claim-detail-tabs__buttons\"]/div/div/div/div[2]/div[2]/div[3]/div"));
    public static final Target BTN_NOPROCEDE_CONFIRMAR = Target.the("Boton Confirmar NO Procede").located(By.xpath("//*[@id=\"add-email__btn-add\"]"));

    public static final Target BTN_DERIVAR = Target.the("Boton Derivar").located(By.xpath("//*[@id=\"claim-detail-tabs__buttons\"]/div/div/div/div[2]/div[2]/div[2]/div"));

}
