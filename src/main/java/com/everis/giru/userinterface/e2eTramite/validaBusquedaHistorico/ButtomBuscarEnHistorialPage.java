package com.everis.giru.userinterface.e2eTramite.validaBusquedaHistorico;

import com.everis.giru.utils.Shadow;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.WebElement;

public class ButtomBuscarEnHistorialPage extends PageObject {

    Shadow general = new Shadow();

    @FindBy(xpath = "/html/body/app-root/giru-layout/div/div[3]/giru-general-history/div/div[2]/giru-history-search-form/form/div/div[2]/ibk-inputgroup-text")
    WebElement BTN_ID_IMAGE_BUSCARTRAMITE;

    public void BuscarTramite() {
        WebElement buttomBuscar = general.expandContainer("2css", BTN_ID_IMAGE_BUSCARTRAMITE, "ibk-input", "div > span", "", "");
        buttomBuscar.click();

    }


}
