package com.everis.giru.userinterface.e2eTramite.resolucionTramite;

import com.everis.giru.utils.Shadow;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class SeleccionaTramitePage extends PageObject {

    Shadow general = new Shadow();

    //TODO: MAPEO DE ELEMENTOS - VALIDACIÓN FILTROS
    @FindBy(xpath = "//*[@id='grid']/giru-specialist-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR;

    @FindBy(xpath = "//*[@id='grid']/giru-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SUPERVISOR_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-my-team/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-to-assigned/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR;
    @FindBy(xpath = "//*[@id='grid']/giru-to-resolve/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR;

    @FindBy(xpath = "//*[@id='grid']/giru-specialist-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ESPECIALISTA_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-specialist-to-monitor/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS;

    @FindBy(xpath = "//*[@id='grid']/giru-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SUPERVISOR_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-my-team/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-assigned/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-resolve/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-monitor/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS;

    @FindBy(xpath = "//*[@id='grid']/giru-specialist-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-specialist-to-monitor/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-specialist-to-transactions-in-process/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS;

    @FindBy(xpath = "//*[@id='grid']/giru-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-my-team/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-assigned/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-resolve/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-to-monitor/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS;
    @FindBy(xpath = "//*[@id='grid']/giru-supervisor-trx/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS;

    @FindBy(xpath = "//*[@id='grid']/giru-specialist-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-tipology/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-task/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR;

    @FindBy(xpath = "//*[@id='grid']/giru-all-procedures/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_SUPERVISOR_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-my-team/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-to-assigned/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR;
    @FindBy(xpath = "//*[@id='grid']/giru-to-resolve/div[3]/ibk-grid-group")
    WebElement LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR;

    public void seleccionarTramiteBandeja() {

        String departamento = Serenity.sessionVariableCalled("departamentoBuscar").toString();
        String perfil = Serenity.sessionVariableCalled("perfilBuscar").toString();
        String tipologia = Serenity.sessionVariableCalled("tipologiaBuscar").toString();
        
        switch (departamento) {
            case "GPYR":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":

                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ESPECIALISTA_GPYR));
                                WebElement valueTodosMisTramites = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                valueTodosMisTramites.click();
                                break;
                            case "CobrosIndebidos":

                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR));
                                WebElement valueCobrosIndebidos = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                valueCobrosIndebidos.click();

                                break;
                            case "ExoneracionCobros":

                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR));
                                WebElement valueExoneracionCobros = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                valueExoneracionCobros.click();
                                break;
                            case "AnulacionTC":

                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR));
                                WebElement valueAnulacionTC = general.expandContainer("2css", LBL_ID_TABLA_ANULACIONTC_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                valueAnulacionTC.click();
                                break;
                            case "FinalizacionTC":

                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR));
                                WebElement valueFinalizacionTC = general.expandContainer("2css", LBL_ID_TABLA_FINALIZACIONTC_ESPECIALISTA_GPYR, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                valueFinalizacionTC.click();

                                break;
                            default:
                                System.out.println("Título equivocado según Bandeja de la tipología !!!");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":

                                waitForCondition().until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='grid']/giru-all-procedures/div[3]/ibk-grid-group")));
                                WebElement valueTodosMisTramites = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                valueTodosMisTramites.click();

                                break;
                            case "MiEquipo":

                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR));
                                WebElement valueMiEquipo = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr > td:nth-child(2)", "", "");
                                valueMiEquipo.click();
                                break;
                            case "Asignar":

                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR));
                                WebElement valueAsignar = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr > td:nth-child(2)", "", "");
                                valueAsignar.click();
                                break;
                            case "Resolver":

                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR));
                                WebElement valueResolver = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_GPYR, "ibk-grid", "div > table > tbody > tr > td:nth-child(2)", "", "");
                                valueResolver.click();
                                break;
                            default:
                                System.out.println("No se encuentra tipología en Supervisor GPYR !!!");
                        }
                        break;
                }
                break;
            case "Seguros":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":

                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ESPECIALISTA_SEGUROS));
                                WebElement valueTodosMisTramites = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                valueTodosMisTramites.click();
                                break;
                            case "CobrosIndebidos":

                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS));
                                WebElement valueCobrosIndebidos = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                valueCobrosIndebidos.click();
                                break;
                            case "ExoneracionCobros":

                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS));
                                WebElement valueExoneracionCobros = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                valueExoneracionCobros.click();
                                break;
                            case "ConsumosNoReconocidos":

                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS));
                                WebElement valueConsumosNoReconocidos = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                valueConsumosNoReconocidos.click();
                                break;
                            case "Monitorear":

                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS));
                                WebElement valueMonitorear = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_SEGUROS, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                valueMonitorear.click();
                                break;
                            default:
                                System.out.println("No se encuentra tipología en especialista SEGUROS !!!");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":

                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SUPERVISOR_SEGUROS));
                                WebElement valueTodosMisTramites = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                valueTodosMisTramites.click();
                                break;
                            case "MiEquipo":

                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS));
                                WebElement valueMiEquipo = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr > td:nth-child(2)", "", "");
                                valueMiEquipo.click();
                                break;
                            case "Asignar":

                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS));
                                WebElement valueAsignar = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr > td:nth-child(2)", "", "");
                                valueAsignar.click();
                                break;
                            case "Resolver":

                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS));
                                WebElement valueResolver = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr > td:nth-child(2)", "", "");
                                valueResolver.click();
                                break;
                            case "Monitorear":

                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS));
                                WebElement valueMonitorear = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_SEGUROS, "ibk-grid", "div > table > tbody > tr > td:nth-child(2)", "", "");
                                valueMonitorear.click();
                                break;
                            default:
                                System.out.println("No se encuentra tipología en Supervisor SEGUROS !!!");
                        }
                        break;
                }
                break;
            case "Retenciones":
                break;
            case "Controversias":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":

                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS));
                                WebElement valueTodosMisTramites = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                valueTodosMisTramites.click();
                                break;
                            case "ConsumosNoReconocidos":

                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS));
                                WebElement valueConsumosNoReconocidos = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                valueConsumosNoReconocidos.click();
                                break;
                            case "ConsumosMalProcesados":

                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS));
                                WebElement valueConsumosMalProcesados = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                valueConsumosMalProcesados.click();
                                break;
                            case "Monitorear":

                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS));
                                WebElement valueMonitorear = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                valueMonitorear.click();
                                break;
                            case "TransaccionesEnProceso":

                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS));
                                WebElement valueTransaccionesEnProceso = general.expandContainer("2css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_ESPECIALISTA_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                valueTransaccionesEnProceso.click();
                                break;
                            default:
                                System.out.println("No se encuentra tipología en especialista CONTROVERSIAS !!!");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":

                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS));
                                WebElement valueTodosMisTramites = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                valueTodosMisTramites.click();
                                break;
                            case "MiEquipo":

                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS));
                                WebElement valueMiEquipo = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr > td:nth-child(2)", "", "");
                                valueMiEquipo.click();
                                break;
                            case "Asignar":

                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS));
                                WebElement valueAsignar = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr > td:nth-child(2)", "", "");
                                valueAsignar.click();
                                break;
                            case "Resolver":

                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS));
                                WebElement valueResolver = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr > td:nth-child(2)", "", "");
                                valueResolver.click();
                                break;
                            case "Monitorear":

                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS));
                                WebElement valueMonitorear = general.expandContainer("2css", LBL_ID_TABLA_MONITOREAR_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr > td:nth-child(2)", "", "");
                                valueMonitorear.click();
                                break;
                            case "TransaccionesEnProceso":

                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS));
                                WebElement valueTransaccionesEnProceso = general.expandContainer("2css", LBL_ID_TABLA_TRANSACCIONES_ENPROCESO_SUPERVISOR_CONTROVERSIAS, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                valueTransaccionesEnProceso.click();
                                break;
                            default:
                                System.out.println("No se encuentra tipología en Supervisor CONTROVERSIAS !!!");
                        }
                        break;
                }
                break;
            case "USPR":
                switch (perfil) {
                    case "Especialista":
                        switch (tipologia) {
                            case "TodosMisTramites":
                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ESPECIALISTA_USPR));
                                WebElement valueTodosMisTramites = general.expandContainer("2css", LBL_ID_TABLA_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                valueTodosMisTramites.click();
                                break;
                            case "CobrosIndebidos":

                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR));
                                WebElement valueCobrosIndebidos = general.expandContainer("2css", LBL_ID_TABLA_COBROINDEBIDO_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                valueCobrosIndebidos.click();
                                break;
                            case "ExoneracionCobros":

                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR));
                                WebElement valueExoneracionCobros = general.expandContainer("2css", LBL_ID_TABLA_EXONERACION_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                valueExoneracionCobros.click();
                                break;
                            case "ConsumosMalProcesados":

                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR));
                                WebElement valueConsumosMalProcesados = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_MALPROCESADOS_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                valueConsumosMalProcesados.click();
                                break;
                            case "ConsumosNoReconocidos":

                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR));
                                WebElement valueConsumosNoReconocidos = general.expandContainer("2css", LBL_ID_TABLA_CONSUMOS_NORECONOCIDOS_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                valueConsumosNoReconocidos.click();
                                break;
                            case "Retipificar":

                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR));
                                WebElement valueRetipificar = general.expandContainer("2css", LBL_ID_TABLA_RETIPIFICAR_ESPECIALISTA_USPR, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                valueRetipificar.click();
                                break;
                            default:
                                System.out.println("No se encuentra tipología en especialista USPR !!!");
                        }
                        break;
                    case "Supervisor":
                        switch (tipologia) {
                            case "TodosMisTramites":

                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_SUPERVISOR_USPR));
                                WebElement valueTodosMisTramites = general.expandContainer("2css", LBL_ID_TABLA_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
                                valueTodosMisTramites.click();
                                break;
                            case "MiEquipo":

                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR));
                                WebElement valueMiEquipo = general.expandContainer("2css", LBL_ID_TABLA_MIEQUIPO_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr > td:nth-child(2)", "", "");
                                valueMiEquipo.click();
                                break;
                            case "Asignar":

                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR));
                                WebElement valueAsignar = general.expandContainer("2css", LBL_ID_TABLA_ASIGNAR_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr > td:nth-child(2)", "", "");
                                valueAsignar.click();
                                break;
                            case "Resolver":

                                waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR));
                                WebElement valueResolver = general.expandContainer("2css", LBL_ID_TABLA_RESOLVER_SUPERVISOR_USPR, "ibk-grid", "div > table > tbody > tr > td:nth-child(2)", "", "");
                                valueResolver.click();
                                break;
                            default:
                                System.out.println("No se encuentra tipología en Supervisor USPR !!!");
                        }
                        break;
                }
                break;
        }


    }


}
