package com.everis.giru.userinterface.e2eTramite.validaBusquedaHistorico;

import com.everis.giru.tasks.e2eTramites.GetHistoricalInbox;
import com.everis.giru.utils.Shadow;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class ValidaBuscarTramiteEnHistorialPage extends PageObject {

    Shadow general = new Shadow();
    GetHistoricalInbox getHistoricalInbox = new GetHistoricalInbox();

    @FindBy(xpath = "//ibk-grid-group[@class='hydrated']")
    WebElement LBL_ID_TABLA_HISTORIAL;

    public void validaBuscarTramite() {

        waitForCondition().until(ExpectedConditions.visibilityOf(LBL_ID_TABLA_HISTORIAL));
        WebElement valueClaimNumber = general.expandContainer("2css", LBL_ID_TABLA_HISTORIAL, "ibk-grid", "div > table > tbody > tr > td:nth-child(1)", "", "");
        WebElement valueClainStatusDescription = general.expandContainer("2css", LBL_ID_TABLA_HISTORIAL, "ibk-grid", "div > table > tbody > tr > td:nth-child(6)", "", "");
        WebElement valueUserNameAssigned = general.expandContainer("2css", LBL_ID_TABLA_HISTORIAL, "ibk-grid", "div > table > tbody > tr > td:nth-child(8) > div > div", "", "");

        String getClaimNumber = valueClaimNumber.getText();
        String getClainStatusDescription = valueClainStatusDescription.getText();
        String getUserNameAssigned = valueUserNameAssigned.getText();
        System.out.println("=======================================\nNº Trámite: " + getClaimNumber + "\nEstado Trámite: " + getClainStatusDescription + "\nSupervisor Asignado: " + getUserNameAssigned);

        Assert.assertEquals(getClaimNumber, getHistoricalInbox.getClaimNumber());
        System.out.println(" ENDPOINT: ClainNumber: "+ getHistoricalInbox.getClaimNumber());

        Assert.assertEquals(getClainStatusDescription, getHistoricalInbox.getClainStatusDescription());
        System.out.println(" ENDPOINT: Status Description: "+ getHistoricalInbox.getClainStatusDescription());

//        if (getUserNameAssigned.equals(getHistoricalInbox.getUserNameAssigned())) {
//            Assert.assertEquals(getUserNameAssigned, getHistoricalInbox.getUserNameAssigned());
//        } else {
            Assert.assertEquals(getUserNameAssigned, getHistoricalInbox.getcurrentAreaUserName());
//        }

        System.out.println(" ENDPOINT: Username de la Consulta: "+ getHistoricalInbox.getcurrentAreaUserName());



    }


}
