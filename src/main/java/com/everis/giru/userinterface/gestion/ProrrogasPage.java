package com.everis.giru.userinterface.gestion;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

/**
 * @author Nilo Carrion
 * @author Renato Paco
 */
public class ProrrogasPage {
    public static final Target BTN_PROCESAR = Target.the("Boton procesar").located(By.xpath("//ibk-button[@text='Procesar']"));
    public static final Target BTN_CONFIRMAR_PROCESAR = Target.the("Boton confirmar procesar").located(By.id("confirmation__btn-confirm"));
    public static final Target BTN_TERMINAR = Target.the("Boton terminar procesar").located(By.id("end-atention"));
    public static final Target BTN_DESCARGAR_ARCHIVO = Target.the("Boton terminar procesar").located(By.xpath("//*[@id='proceed-section']/a/ibk-icon-svg"));
    public static final Target BTN_ERROR = Target.the("Boton terminar procesar").located(By.id("confirmation__btn-cancel"));

}
