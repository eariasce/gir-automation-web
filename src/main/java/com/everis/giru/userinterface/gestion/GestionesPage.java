package com.everis.giru.userinterface.gestion;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

/**
 * @author Nilo Carrion
 * @author Renato Paco
 */
public class GestionesPage {
    public static final Target BTN_FILTRO_TELESOFT = Target.the("Boton filtro Telesoft").located(By.xpath("//button[contains(text(), 'Telesoft')]"));
    public static final Target BTN_FILTRO_GIRU = Target.the("Boton filtro GIRU").located(By.xpath("//button[contains(text(), 'Giru')]"));

    public static final Target BTN_CLIENTE_GESTIONES = Target.the("Boton cliente gestiones").located(By.xpath("//button[contains(text(), 'Cliente')]"));

    public static final Target BTN_USUARIO_GESTIONES = Target.the("Boton usuario gestiones").located(By.xpath("//button[contains(text(), 'Usuario')]"));

    public static final Target CBX_TIPO_DOCUMENTO = Target.the("Opciones tipo documento").located(By.xpath("//span[contains(text(), 'Seleccione')]/../../../.."));
    public static final Target CBX_TIPO_DOCUMENTO_DNI = Target.the("Opciones documento DNI").located(By.xpath("//span[contains(text(), 'Seleccione')]/../../../..//span[contains(text(), 'DNI')]"));
    public static final Target CBX_TIPO_DOCUMENTO_CE = Target.the("Opciones documento CE").located(By.xpath("//span[contains(text(), 'Seleccione')]/../../../..//span[contains(text(), 'CE')]"));
    public static final Target CBX_TIPO_DOCUMENTO_PTP = Target.the("Opciones documento PTP").located(By.xpath("//span[contains(text(), 'Seleccione')]/../../../..//span[contains(text(), 'PTP')]"));
    public static final Target CBX_TIPO_DOCUMENTO_PASAPORTE = Target.the("Opciones documento pasaporte").located(By.xpath("//span[contains(text(), 'Seleccione')]/../../../..//span[contains(text(), 'PASAPORTE')]"));
    public static final Target CBX_TIPO_DOCUMENTO_RUC = Target.the("Opciones documento RUC").located(By.xpath("//span[contains(text(), 'Seleccione')]/../../../..//span[contains(text(), 'RUC')]"));
    public static final Target TXT_BUSCAR_GESTION = Target.the("Input buscar gestion").located(By.xpath("//ibk-inputgroup-text[@formcontrolname='document']"));
//REGISTRO
    public static final Target BTN_REGISTRAR_GESTION = Target.the("Boton registrar gestion").located(By.xpath("//button[contains(text(), 'Registrar nueva gestión')]"));
    public static final Target CBX_TIPOGESTIONES = Target.the("opciones tipo gestiones").located(By.xpath("//app-ibk-select[@id='managementType']"));
    public static final Target CBX_GESTIONES = Target.the("opciones gestiones").located(By.xpath("//app-ibk-select[@id='management']"));
    public static final Target CBX_TIPOLLAMADA = Target.the("opciones tipo llamadas").located(By.xpath("//app-ibk-select[@id='callType']"));
    public static final Target CBX_TIPOLLAMADA_IN = Target.the("opciones tipo llamada in").located(By.xpath("//app-ibk-select[@id='callType']//span[contains(text(), 'In')]"));
    public static final Target CBX_TIPOLLAMADA_OUT = Target.the("opciones tipo llamada out").located(By.xpath("//app-ibk-select[@id='callType']//span[contains(text(), 'Out')]"));
    public static final Target CBX_COMENTARIOS = Target.the("opciones comentarios").located(By.xpath("//textarea[@formcontrolname='comments']"));
    public static final Target BTN_COMPLETAR_REGISTRO_GESTION = Target.the("boton completar registron gestion").located(By.xpath("//*[@id='claim-form__btn-submit']"));
   // public static final Target BTN_FINALIZAR_REG_GESTION = Target.the("Numero Tarjeta").located(By.xpath("//*[contains(text(), ' Terminar atención ')]"));

}
