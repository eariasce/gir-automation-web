package com.everis.giru.runners;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.annotations.Managed;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import static net.serenitybdd.annotations.ClearCookiesPolicy.BeforeEachTest;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = {"src/test/resources/features"},
        glue = {"com.everis.giru.stepsdefinitions"},
        tags = "@registroSegundaInstanciaEGH"
       // ,name = "CASE11"
)

public class CucumberWebTest {
    @Managed(uniqueSession = true, clearCookies = BeforeEachTest)
    WebDriver driver;

}
