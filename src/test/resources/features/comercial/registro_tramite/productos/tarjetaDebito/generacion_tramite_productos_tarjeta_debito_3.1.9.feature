@all_generacion_tramite_atencionCliente
Feature: Registro del trámite producto Tarjeta de Débito en Giru Comercial

  @happy_path @regresion @version3.1.8 @registroC_TD_consumo_no_reconocido_test1 @registroC_TD
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto TARJETA DÉBITO para la tipología: CONSUMOS NO RECONOCIDOS POR FRAUDE en GIRU Comercial
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU COMERCIAL
    And se obtiene informacion de comercio con producto TARJETA DE DÉBITO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo tramite comercial RECLAMO y la tipologia CONSUMOS NO RECONOCIDOS POR FRAUDE
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | canal   | CantMovimientosPorMotivo | moneda   | monto   | numCuenta|numTarjeta|cuentaMancomunada|quePasoConLaTarjeta|flagTarjetaBloqueo|razonNoBloqueo|
      | <motivos> | <canal> | <cantMovimientos>        | <moneda> | <monto> |<numCuenta>|<numTarjeta>|<cuentaMancomunada>|<quePasoConLaTarjeta>|<flagTarjetaBloqueo>|<razonNoBloqueo>|
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos| moneda | monto   | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |canal                  |cantMovimientos|   numCuenta |     numTarjeta  |cuentaMancomunada|         quePasoConLaTarjeta                              |flagTarjetaBloqueo| razonNoBloqueo  |
      #| CASE01   | 1      | PEN    | 100     | Si         | Si                   | Correo         | Registrado      | Interbank por Facebook|       1       |1233333444456|2788899992838399999|     Si          |Fue robada o usada en un secuestro                        |         0        |No tenia internet|
      | CASE02   | 2      | PEN    | 100     | Si         | Si                   | Correo         | Registrado      | Interbank por Facebook|       2       |1233333444456|2788899992838399999|     No          |Está en poder del cliente                               |         1        |                   |
      | CASE03   | 3      | PEN    | 100     | Si         | Si                   | Correo         | Registrado      | Interbank por Facebook|       2       |1233333444456|2788899992838399999|     Si          |Fue usada a través de las aplicaciones por robo de celular|         1        |                   |
      | CASE04   | 1-3      | PEN    | 100     | Si         | Si                   | Correo         | Registrado      | Interbank por Facebook|       1       |1233333444456|2788899992838399999|     Si          |Escoge una situación similar                             |         0        |No tenia internet|


  @happy_path @regresion @version3.1.8 @registroC_TD_consumo_mal_procesado_test1 @registroC_TD
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto TARJETA DÉBITO para la tipología: CONSUMOS MAL PROCESADO en GIRU Comercial
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU COMERCIAL
    And se obtiene informacion de comercio con producto TARJETA DE DÉBITO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo tramite comercial RECLAMO y la tipologia CONSUMOS MAL PROCESADOS POR EL COMERCIO/MARCA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | canal   | CantMovimientosPorMotivo | moneda   | monto   | numCuenta|numTarjeta|cuentaMancomunada|productoServicioAdquirido|tipoServicio|esperabaRecibirFecha|
      | <motivos> | <canal> | <cantMovimientos>        | <moneda> | <monto> |<numCuenta>|<numTarjeta>|<cuentaMancomunada>|<productoServicioAdquirido>|<tipoServicio>|<esperabaRecibirFecha>|
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos| moneda | monto   | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |canal                 |cantMovimientos|numCuenta    |numTarjeta           |cuentaMancomunada|productoServicioAdquirido|tipoServicio|esperabaRecibirFecha|
      | CASE01   | 1    | PEN    | 100     | Si         | Si                   | Correo         | Registrado        | Interbank por Facebook|        2     |1233333444456|2788899992838399999  | Si              |                         |            |                    |
      | CASE02   | 2     | PEN    | 100     | Si         | No                   | Correo         | Registrado        | Interbank por Facebook|        1      |1233333444456|2788899992838399999  |No               |Producto                 |            |   12/10/2021       |
      | CASE03   | 1-2     | PEN    | 100     | Si         | No                   | Direccion      | Registrado        | Interbank por Facebook|        1      |1233333444456|2788899992838399999  |No               |Servicio                 |   Vuelos   |   12/10/2021       |

  @happy_path @regresion @version3.1.8 @registroC_TD_consumo_retiro_no_efectuado_test1 @registroC_TD
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto TARJETA DE DÉBITO para la tipología: CONSUMO/ RETIRO NO EFECTUADO en GIRU Comercial
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU COMERCIAL
    And se obtiene informacion de comercio con producto TARJETA DE DÉBITO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo tramite comercial RECLAMO y la tipologia CONSUMO/ RETIRO NO EFECTUADO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | canal   | CantMovimientosPorMotivo | moneda   | monto   | tipoOperacion | numCuenta | numTarjeta | numCredito | numPoliza |nomSeguro|
      | <motivos> | <canal> | <cantMovimientos>        | <moneda> | <monto> |<tipoOperacion>|<numCuenta>|<numTarjeta>|<numCredito>|<numPoliza>|<nomSeguro>|
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento


    Examples:
      | TESTCASE | motivos| moneda | monto   | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |canal                |cantMovimientos|  tipoOperacion|  numCuenta  |     numTarjeta    |numCredito|   numPoliza    | nomSeguro  |
      | CASE01   | 1      | PEN    | 100     | Si         | Si                   | Correo         | Registrado      | Interbank por Facebook|       2       |   Ninguno     |             |                   |          |                |            |
      | CASE02   | 2      | PEN    | 160.5   | Si         | Si                   | Direccion      | Registrado      | ATM GlobalNet         |       2       |Cuentas TC - TD|1233333444456|2788899992838399999|          |                |            |
      | CASE03   | 2      | PEN    | 20.6    | Si         | Si                   | Direccion      | Registrado      | ATM GlobalNet         |       2       |   Seguros     |1233333444456|2788899992838399999| 12221110 | 12345567       |CARITAS SUL |
      | CASE04   | 1      | PEN    | 160.5   | Si         | Si                   | Correo         | Registrado      | ATM GlobalNet         |       2       |Cuentas TC - TD|             |                   |          |                |            |
      | CASE05   | 1-2    | PEN    | 20.6    | Si         | Si                   | Correo         | Registrado      | ATM GlobalNet         |       2       |   Seguros     |             |                   |          |                |            |

