@all_generacion_tramite_atencionCliente
Feature: Registro del trámite producto Cuenta Corriente en Giru Comercial
  @happy_path @regresion @version3.1.8 @registroC_CC_cancelacion_cuenta_test1
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo SOLICITUD con producto CUENTA CORRIENTE para la tipología: CANCELACIÓN DE CUENTA en GIRU Comercial
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU COMERCIAL
    And se obtiene informacion de comercio con producto CUENTA CORRIENTE
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo tramite comercial SOLICITUD y la tipologia CANCELACIÓN DE CUENTA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos| moneda|monto|CantMovimientosPorMotivo|
      | <motivos>| <moneda>|<monto>|<CantMovimientosPorMotivo>|
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos| moneda | monto   | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |CantMovimientosPorMotivo|
      #| CASE01   | 1      | USD    | 100     | Si         | Si                   | Correo         | Registrado        |1                       |
      | CASE02   | 1      | PEN    | 100     | Si         | Si                   | Direccion         | Registrado        |2                      |


  @happy_path @regresion @version3.1.8 @registroC_CC_cobros_indebidos_test1
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto CUENTA CORRIENTE para la tipología: COBROS INDEBIDOS en GIRU Comercial
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU COMERCIAL
    And se obtiene informacion de comercio con producto CUENTA CORRIENTE
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo tramite comercial RECLAMO y la tipologia COBROS INDEBIDOS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | canal   | CantMovimientosPorMotivo | moneda   | monto   |
      | <motivos> | <canal> | <cantMovimientos>        | <moneda> | <monto> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos| moneda | monto   | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |canal|cantMovimientos|
      | CASE01   | 1     | PEN    | 100     | Si         | Si                   | Correo         | Registrado        | Interbank por Facebook|1|
      | CASE02   | 4     | USD    | 100     | Si         | Si                   | Direccion         | Registrado        | Interbank por Facebook|1|
      | CASE03   | 5-3     | USD    | 100     | Si       | Si                   | Direccion         | Registrado        | Interbank por Facebook|1|
      | CASE04   | 9-12     | USD    | 100     | Si      | Si                   | Correo         | Registrado        | Interbank por Facebook|1|

  @happy_path @regresion @version3.1.8 @registroC_CC_duplicado_estado_cuenta_test1
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto CUENTA CORRIENTE para la tipología: DUPLICADO DE ESTADO DE CUENTA/SERVICIOS DE COPIAS en GIRU Comercial
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU COMERCIAL
    And se obtiene informacion de comercio con producto CUENTA CORRIENTE
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo tramite comercial PEDIDO y la tipologia DUPLICADO DE ESTADO DE CUENTA/SERVICIO DE COPIAS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos  | numCuenta  |numTarjeta|   periodoIni | periodoFin | recojoTienda | Tienda | CantMovimientosPorMotivo |medioPago|
      | <motivos>| <numCuenta>|<numTarjeta>|<periodoIni>|<periodoFin>|<recojoTienda>|<Tienda>|<CantMovimientosPorMotivo>|<medioPago>|
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos|   numCuenta  |      numTarjeta     | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |CantMovimientosPorMotivo|periodoIni|periodoFin|recojoTienda|     Tienda   |medioPago|
      | CASE01   | 1      | 1234445534114| 23334444200099990001| Si         | No                   | Correo         | Registrado        |1                       |12/10/2021|12/10/2022|Si          | Tienda Larco |Ventanilla|
      | CASE02   | 6      | 1234445534114| 23334444200099990001| Si         | Si                   | Direccion      | Registrado        |2                    |12/10/2021|12/10/2022|No          | Tienda Larco |Cargo en Cuenta|
      | CASE03   | 2-5      | 1234445534114| 23334444200099990001| Si         | No                 | Correo        | Registrado         |3                     |12/10/2021|12/10/2022|Si          | Tienda Larco |Ventanilla|


  @happy_path @regresion @version3.1.8 @registroC_CC_no_recepcion_documento_test1
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto CUENTA CORRIENTE para la tipología: NO RECEPCIÓN DE DOCUMENTO en GIRU Comercial
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU COMERCIAL
    And se obtiene informacion de comercio con producto CUENTA CORRIENTE
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo tramite comercial RECLAMO y la tipologia NO RECEPCIÓN DE DOCUMENTO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos  | numCuenta  | CantMovimientosPorMotivo |tipoDocumentoTramite|canal|
      | <motivos>| <numCuenta>|<CantMovimientosPorMotivo>|<tipoDocumentoTramite>|<canal>|
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador BITEL
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos|   numCuenta  | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |CantMovimientosPorMotivo|  tipoDocumentoTramite   |           canal          |
      | CASE01   | 1      | 1234445534114| Si         | No                   | Correo         | Registrado        |1                       |   Constancia de Pago    |Banca por Internet Empresa|
      | CASE02   | 1      | 1234445534114| Si         | No                   | Direccion         | Registrado        |1                    |   Contrato    |Red Tiendas|
      | CASE03   | 1      | 1234445534114| Si         | Si                   | Direccion         | Registrado        |2                   |   Otros    |ATM GlobalNet|


  @happy_path @regresion @version3.1.8 @registroC_CC_exoneracion_cobros_test1
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto CUENTA CORRIENTE para la tipología: EXONERACIÓN DE COBROS en GIRU Comercial
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU COMERCIAL
    And se obtiene informacion de comercio con producto CUENTA CORRIENTE
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo tramite comercial PEDIDO y la tipologia EXONERACIÓN DE COBROS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos  | numCuenta  | CantMovimientosPorMotivo |moneda|monto|Tienda|
      | <motivos>| <numCuenta>|<CantMovimientosPorMotivo>|<moneda>|<monto>|<Tienda>|
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador BITEL
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos|   numCuenta  | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |CantMovimientosPorMotivo|moneda | monto|Tienda|
      #| CASE01   | 1      | 1234445534114| Si         | No                   | Correo         | Registrado        |1                       |PEN    |145.63|Tambo |
      #| CASE02   | 12     | 1234445534114| Si         | Si                   | Direccion      | Registrado        |1                       |USD    |145.63|Mercurio |
      | CASE03   | 5-9    | 1234445534114| Si         | No                   | Correo         | Registrado        |1                       |PEN    |100.63|Tambo |
      #| CASE04   | 3      | 1234445534114| Si         | Si                   | Correo         | Registrado        |1                       |USD    |200.63|Tambo |
