@all_generacion_tramite_creditoBpe
Feature: Registro del trámite Producto de CRÉDITO BPE

  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO PEDIDO -------------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.9 @registroCreditoBpe_test1
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto CRÉDITO BPE para la tipología: DOCUMENTOS Y CONSTANCIAS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU COMERCIAL
    And se obtiene informacion de comercio con producto CRÉDITO BPE
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo tramite comercial PEDIDO y la tipologia DOCUMENTOS Y CONSTANCIAS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |medioDevolucion|
      | <motivos> |<medioDevolucion>|
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |medioDevolucion|
      | CASE01   | 1-2-3   | Si         | Si                   | Correo         | Registrado        | Orden de pago |
      | CASE02   | 3-4     | Si         | Si                   | Direccion      | Registrado        | Devolución en cuenta  |
      | CASE03   | 1       | Si         | Si                   | Correo         | Registrado        | Orden de pago |
      | CASE04   | 3       | Si         | Si                   | Direccion      | Registrado        | Devolución en cuenta  |


  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO RECLAMO -------------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.9 @registroCreditoBpe_test2
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto CRÉDITO BPE para la tipología: COBROS INDEBIDOS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU COMERCIAL
    And se obtiene informacion de comercio con producto CRÉDITO BPE
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo tramite comercial RECLAMO y la tipologia COBROS INDEBIDOS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | medioDevolucion   | canal   |tipoCuenta|
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <medioDevolucion> | <canal> |<tipoCuenta>|
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | medioDevolucion | canal                    | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |tipoCuenta|
      #| CASE01   | 1      | 3               | USD    | 100   | 1               | Banca Telefónica            | Si         | Si                   | Correo         | Registrado        |2  |
      #| CASE02   | 2     | 1              | PEN    | 100   | 2               | Banca por Internet Personas | Si         | No                   | Direccion      | Registrado        |3|
      #revisar aqui se cae
      | CASE03   | 1-2      | 1               | PEN    | 100   | 1               | Banca por Internet Empresa  | Si         | No                   | Correo         | Registrado     |1  |
      #| CASE04   | 1       | 1               | PEN    | 100   | 2               | App Interbank Personas      | Si         | No                   | Direccion      | Registrado        |
      #| CASE05   | 1       | 1               | PEN    | 100   | 1               | App Interbank Empresas      | Si         | No                   | Correo         | Registrado        |
      #| CASE06   | 1       | 1               | PEN    | 100   | 2               | IBK Agentes                 | Si         | No                   | Direccion      | Registrado        |
      #| CASE07   | 1       | 1               | PEN    | 100   | 1               | Red Tiendas                 | Si         | No                   | Correo         | Registrado        |
      #| CASE08   | 1       | 1               | PEN    | 100   | 2               | ATM GlobalNet               | Si         | No                   | Direccion      | Registrado        |
      #| CASE09   | 1       | 1               | PEN    | 100   | 1               | Interbank por Facebook      | Si         | No                   | Correo         | Registrado        |
      #| CASE10   | 1       | 1               | PEN    | 100   | 2               | WhatsApp AVI                | Si         | No                   | Direccion      | Registrado        |
      #| CASE11   | 1       | 1               | PEN    | 100   | 1               | Entrega TC                  | Si         | No                   | Correo         | Registrado        |
      #| CASE12   | 1       | 1               | PEN    | 100   | 2               | Módulo de Cobranza          | Si         | No                   | Direccion      | Registrado        |
      #| CASE13   | 1       | 1               | PEN    | 100   | 1               | Televentas                  | Si         | No                   | Correo         | Registrado        |
      #| CASE14   | 1       | 1               | PEN    | 100   | 2               | Canal Select                | Si         | No                   | Correo         | Registrado        |
      #| CASE15   | 1       | 1               | PEN    | 100   | 1               | Otros                       | Si         | No                   | Correo         | Registrado        |

