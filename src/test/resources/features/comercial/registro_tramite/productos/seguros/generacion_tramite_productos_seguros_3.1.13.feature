@all_generacion_tramite_atencionCliente
Feature: Registro del trámite producto Seguros en Giru Comercial

  @happy_path @regresion @version3.1.8 @registroC_Seguros_duplicado_documento_test1 @registroC_Seguros
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto SEGUROS para la tipología: DUPLICADO DE DOCUMENTO en GIRU Comercial
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU COMERCIAL
    And se obtiene informacion de comercio con producto SEGUROS
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo tramite comercial PEDIDO y la tipologia DUPLICADO DE DOCUMENTO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | canal   | CantMovimientosPorMotivo | moneda   | monto   | tipoOperacion | numCuenta | numTarjeta | numCredito | numPoliza |nomSeguro|
      | <motivos> | <canal> | <cantMovimientos>        | <moneda> | <monto> |<tipoOperacion>|<numCuenta>|<numTarjeta>|<numCredito>|<numPoliza>|<nomSeguro>|
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos| moneda | monto   | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |canal                |cantMovimientos|  tipoOperacion|  numCuenta  |     numTarjeta    |numCredito|   numPoliza    | nomSeguro  |
      #| CASE01   | 2      | PEN    | 100     | Si         | Si                   | Correo         | Registrado      | Interbank por Facebook|       2       |   Ninguno     |             |                   |          |                |            |
      #| CASE02   | 3      | PEN    | 160.5   | Si         | Si                   | Direccion      | Registrado      | ATM GlobalNet         |       2       |Cuentas TC - TD|1233333444456|2788899992838399999|          |                |            |
      | CASE03   | 1      | PEN    | 20.6    | Si         | Si                   | Correo         | Registrado      | ATM GlobalNet         |       2       |   Seguros     |1233333444456|2788899992838399999| 12221110 | 12345567       |CARITAS SUL |
      #| CASE04   | 1      | PEN    | 160.5   | Si         | Si                   | Correo         | Registrado      | ATM GlobalNet         |       2       |Cuentas TC - TD|             |                   |          |                |            |
      | CASE05   | 1      | PEN    | 20.6    | Si         | Si                   | Correo         | Registrado      | ATM GlobalNet         |       2       |   Seguros     |             |                   |          |                |            |
