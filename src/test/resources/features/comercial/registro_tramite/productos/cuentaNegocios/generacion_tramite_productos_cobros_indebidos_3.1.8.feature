@all_generacion_tramite_atencionCliente
Feature: Registro del trámite producto CREDITO BPE en Giru Comercial
  
  @happy_path @regresion @version3.1.8 @registroC_CN_cobros_indebidos_test2
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto ATENCION AL CLIENTE para la tipología: COBROS INDEBIDOS en GIRU Comercial
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU COMERCIAL
    And se obtiene informacion de comercio con producto CUENTA NEGOCIOS
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo tramite comercial RECLAMO y la tipologia COBROS INDEBIDOS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | canal   | CantMovimientosPorMotivo | moneda   | monto   | tipoCuenta|
      | <motivos> | <canal> | <cantMovimientos>        | <moneda> | <monto> | <tipoCuenta>    |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | canal   | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |tipoCuenta|
      | CASE01   | 1-2     | 1               | PEN    | 100   | App IBK | Si         | Si                   | Correo         | Registrado        |Cuenta corriente|
      | CASE02   | 2     | 3               | PEN    | 100   | App IBK | Si         | Si                   | Direccion         | Registrado        |Cuenta corriente|
      | CASE03   | 1     | 1               | USD    | 100   | App IBK | Si         | Si                   | Correo         | Registrado        |Cuenta corriente|

