@all_generacion_tramite_atencionCliente
Feature: Registro del trámite producto LETRAS Y CARTA FIANZA en Giru Comercial

  @happy_path @regresion @version3.1.8 @registroC_letras_y_cartafianza_test1
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto LETRAS Y CARTA FIANZA para la tipología: DOCUMENTOS Y CONSTANCIAS en GIRU Comercial
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU COMERCIAL
    And se obtiene informacion de comercio con producto LETRAS Y CARTA FIANZA
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo tramite comercial PEDIDO y la tipologia DOCUMENTOS Y CONSTANCIAS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | canal   | CantMovimientosPorMotivo | moneda   | monto   |
      | <motivos> | <canal> | <cantMovimientos>        | <moneda> | <monto> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | canal   | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1-2     | 3               | PEN    | 100   |Banca Telefónica  | Si         | Si                   | Correo         | Registrado        |
      | CASE02   | 2       | 2              | PEN    | 100   |Banca Telefónica  | Si         | Si                   | Direccion         | Registrado        |
      | CASE03   | 1       | 1               | USD    | 100   |Banca Telefónica  | Si         | Si                   | Direccion         | Registrado        |

