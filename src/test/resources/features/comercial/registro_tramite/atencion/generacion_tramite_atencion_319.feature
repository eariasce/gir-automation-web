@all_generacion_tramite_atencionCliente
Feature: Registro del trámite ATENCIÓN AL CLIENTE

  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO RECLAMO ------------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.9 @registroAtencionClienteComercial_test1
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto ATENCIÓN AL CLIENTE para la tipología: DESACUERDO CON LA ATENCIÓN
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU COMERCIAL
    And se obtiene informacion de comercio con producto ATENCIÓN AL CLIENTE
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo tramite comercial RECLAMO y la tipologia DESACUERDO CON LA ATENCIÓN
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | canal   |
      | <motivos> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | canal                       | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1       | Banca Telefónica            | Si         | Si                   | Correo         | Registrado        |
      | CASE02   | 1       | Banca por Internet Personas | Si         | No                   | Direccion      | Registrado        |
      | CASE03   | 1       | Banca por Internet Empresa  | Si         | No                   | Correo         | Registrado        |
      | CASE04   | 1       | App Interbank Personas      | Si         | No                   | Direccion      | Registrado        |
      | CASE05   | 1       | App Interbank Empresas      | Si         | No                   | Correo         | Registrado        |
      | CASE06   | 1       | IBK Agentes                 | Si         | No                   | Direccion      | Registrado        |
     # | CASE07   | 1       | Red Tiendas                 | Si         | No                   | Correo         | Registrado        |
     # | CASE08   | 1       | ATM GlobalNet               | Si         | No                   | Direccion      | Registrado        |
     # | CASE09   | 1       | Interbank por Facebook      | Si         | No                   | Correo         | Registrado        |
     # | CASE10   | 1       | WhatsApp AVI                | Si         | No                   | Direccion      | Registrado        |
    #  | CASE11   | 1       | Entrega TC                  | Si         | No                   | Correo         | Registrado        |
    #  | CASE12   | 1       | Módulo de Cobranza          | Si         | No                   | Direccion      | Registrado        |
    #  | CASE13   | 1       | Televentas                  | Si         | No                   | Correo         | Registrado        |
    #  | CASE14   | 1       | Canal Select                | Si         | No                   | Correo         | Registrado        |
    #  | CASE15   | 1       | Otros                       | Si         | No                   | Correo         | Registrado        |

