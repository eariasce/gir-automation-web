@all_generacion_tramite_atencionCliente
Feature: Resolución del trámite ATENCION AL CLIENTE

  @happy_path @regresion @version3.1.8 @resolucionC_desacuerdo_atencion_test1
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolución del trámite de tipo RECLAMO con producto ATENCION AL CLIENTE para la tipología: DESACUERDO CON LA ATENCIÓN en GIRU Comercial
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU COMERCIAL
    And se obtiene informacion de comercio con producto ATENCIÓN AL CLIENTE
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo tramite comercial RECLAMO y la tipologia DESACUERDO CON LA ATENCIÓN
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | canal   | carta   |
      | <motivos> | <canal> | <carta> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | TESTCASE | motivos | canal                  | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | 1       | Banca Telefónica       | Si         | Si                   | Correo         | Registrado        | Procede    |                        | No    |
      | CASE02   | 1       | ATM GlobalNet          | Si         | Si                   | Direccion      | Registrado        | No Procede |                        | Si    |
      | CASE03   | 1       | Interbank por Facebook | Si         | Si                   | Direccion      | Registrado        | No Procede |                        | Si    |

