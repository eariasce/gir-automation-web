@all_generacion_tramite_atencionCliente
Feature: Resolución del trámite producto CUENTA CORRIENTE
#SIN ASIGNAR
  @happy_path @regresion @version3.1.8 @resolucionC_CC_cancelacion_cuenta_test1
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolución del trámite de tipo SOLICITUD con producto CUENTA CORRIENTE para la tipología: CANCELACIÓN DE CUENTA en GIRU Comercial
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU COMERCIAL
    And se obtiene informacion de comercio con producto CUENTA CORRIENTE
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo tramite comercial SOLICITUD y la tipologia CANCELACIÓN DE CUENTA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | canal   | CantMovimientosPorMotivo | moneda   | monto   | medioDevolucion|
      | <motivos> | <canal> | <cantMovimientos>        | <moneda> | <monto> | <medioDevolucion>|
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador BITEL
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | TESTCASE | resolucion | motivos | cantMovimientos | moneda | monto | canal            | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | medioDevolucion | areaValidacionOpcional |
      | CASE01   | Procede    | 1       | 1               | PEN    | 100   | Interbank por Facebook | Si         | Si                   | Correo         | Registrado        | 1         |                        |
      | CASE02   | No Procede | 1       | 1               | USD    | 100   | Interbank por Facebook | Si         | Si                   | Direccion         | Registrado     | 1         |                        |

  @happy_path @regresion @version3.1.8 @resolucionC_CC_cobros_indebidos_test2
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolución del trámite de tipo RECLAMO con producto CUENTA CORRIENTE para la tipología: COBROS INDEBIDOS en GIRU Comercial
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU COMERCIAL
    And se obtiene informacion de comercio con producto CUENTA CORRIENTE
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo tramite comercial RECLAMO y la tipologia COBROS INDEBIDOS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | canal   | CantMovimientosPorMotivo | moneda   | monto   | carta|
      | <motivos> | <canal> | <cantMovimientos>        | <moneda> | <monto> | <carta>|
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador BITEL
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | TESTCASE | resolucion | motivos | cantMovimientos | moneda | monto | canal       | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | areaValidacionOpcional | carta|
      | CASE01   |No Procede  | 1       | 1               | PEN    | 100   | Red Tiendas | Si         | Si                   | Correo         | Registrado        |                        |Si    |
      | CASE02   |Procede     | 1       | 1               | PEN    | 100   | Entrega TC  | Si         | Si                   | Correo         | Registrado        |                        |No    |

  @happy_path @regresion @version3.1.8 @resolucionC_CC_exoneracion_cobros_test1
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolución del trámite de tipo PEDIDO con producto CUENTA CORRIENTE para la tipología: EXONERACIÓN DE COBROS en GIRU Comercial
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU COMERCIAL
    And se obtiene informacion de comercio con producto CUENTA CORRIENTE
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo tramite comercial PEDIDO y la tipologia EXONERACIÓN DE COBROS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos  | numCuenta  | CantMovimientosPorMotivo |moneda|monto|Tienda|carta|
      | <motivos>| <numCuenta>|<CantMovimientosPorMotivo>|<moneda>|<monto>|<Tienda>|<carta>|
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA DERIVACION Y RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se selecciona razon <Razon> y el area <Area> al cual se derivara el tramite
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la derivacion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion


    Examples:
      | TESTCASE | resolucion          | motivos | numCuenta     | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | CantMovimientosPorMotivo | moneda | monto  | Tienda   | carta | areaValidacionOpcional | Area                | Razon                                    |
      #| CASE01   | Derivar-2-NoProcede | 1       | 1234445534114 | Si         | No                   | Correo         | Registrado        | 1                        | PEN    | 145.63 | Tambo    | Si    | Poderes PJ             | Poderes PJ          | NecesitaInformacionDeDocumentos          |
      | CASE02   | Derivar-2-Procede   | 2       | 1234445534114 | Si         | Si                   | Direccion      | Registrado        | 1                        | USD    | 145.63 | Mercurio | No    | Poderes PJ             | Poderes PJ          | RequiereSerRevisadoPorOtraAreaValidadora |
      #| CASE03   | Derivar-2-NoProcede | 5-9     | 1234445534114 | Si         | No                   | Correo         | Registrado        | 1                        | PEN    | 100.63 | Tambo    | Si    |                        | U-Segto-Pedidos Rec | Necesitavalidacióndeáreacorrespondiente  |
      | CASE04   | Derivar-2-Procede   | 3       | 1234445534114 | Si         | Si                   | Correo         | Registrado        | 1                        | USD    | 200.63 | Tambo    | Si    | Poderes PJ             | Poderes PJ          | NecesitaInformacionDeDocumentos          |
