@all_generacion_tramite_atencionCliente
Feature: Resolución del trámite producto Tarjeta de Crédito en Giru Comercial


  @happy_path @regresion @version3.1.8 @resoluciónC_TC_CNR_test2 @resoluciónC_Rezagadas
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolución del trámite de tipo RECLAMO con producto TARJETA DE CRÉDITO para la tipología: CONSUMOS NO RECONOCIDOS POR FRAUDE en GIRU Comercial
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU COMERCIAL
    And se obtiene informacion de comercio con producto TARJETA DE CRÉDITO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo tramite comercial RECLAMO y la tipologia CONSUMOS NO RECONOCIDOS POR FRAUDE
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | canal   | CantMovimientosPorMotivo | moneda   | monto   | tipoOperacion | numCuenta | numTarjeta | numCredito | numPoliza |nomSeguro|carta|
      | <motivos> | <canal> | <cantMovimientos>        | <moneda> | <monto> |<tipoOperacion>|<numCuenta>|<numTarjeta>|<numCredito>|<numPoliza>|<nomSeguro>|<carta>|
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador BITEL
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | TESTCASE | resolucion|motivos| moneda | monto   | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |canal                |cantMovimientos|  tipoOperacion|  numCuenta  |     numTarjeta    |numCredito|   numPoliza    | nomSeguro  |     areaValidacionOpcional     |carta |
      | CASE01   | No Procede   |1      | PEN    | 100     | Si      | Si                   | Correo         | Registrado      | Interbank por Facebook|       2       |   Ninguno     |             |                   |          |                |            |                             |Si    |
      #| CASE02   |     Procede   |2      | PEN    | 100     | Si      | Si                   | Direccion         | Registrado      | Interbank por Facebook|       2       |   Cuentas TC - TD     |  1233333444456           |  2788899992838399999                 |          |                |            |                             |Si    |
      #| CASE03   | Procede    | 2      | PEN    | 20.6    | Si        | Si                   | Direccion      | Registrado      | ATM GlobalNet         |       2       |   Seguros     |1233333444456|2788899992838399999| 12221110 | 12345567       |CARITAS SUL |                             |Si   |
      #| CASE04   | No Procede| 1      | PEN    | 160.5   | Si        | Si                   | Correo         | Registrado      | ATM GlobalNet         |       2       |Cuentas TC - TD|             |                   |          |                |            ||Si    |
      #| CASE05   | Procede   | 1      | PEN    | 20.6    | Si        | Si                   | Correo         | Registrado      | ATM GlobalNet         |       2       |   Seguros     |             |                   |          |                |            ||Si    |




  @happy_path @regresion @version3.1.8 @deriResoluciónC_TC_CMP_test2 @resoluciónC_Rezagadas
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida Derivación y resolución del trámite de tipo RECLAMO con producto TARJETA DE CRÉDITO para la tipología: CONSUMOS MAL PROCESADOS POR EL COMERCIO/MARCA en GIRU Comercial
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU COMERCIAL
    And se obtiene informacion de comercio con producto TARJETA DE CRÉDITO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo tramite comercial RECLAMO y la tipologia CONSUMOS MAL PROCESADOS POR EL COMERCIO/MARCA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | canal   | CantMovimientosPorMotivo | moneda   | monto   | tipoOperacion | numCuenta | numTarjeta | numCredito | numPoliza |nomSeguro|carta|
      | <motivos> | <canal> | <cantMovimientos>        | <moneda> | <monto> |<tipoOperacion>|<numCuenta>|<numTarjeta>|<numCredito>|<numPoliza>|<nomSeguro>|<carta>|
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion


    Examples:
      | TESTCASE | resolucion|motivos| moneda | monto   | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |canal                |cantMovimientos|  tipoOperacion|  numCuenta  |     numTarjeta    |numCredito|   numPoliza    | nomSeguro  |     areaValidacionOpcional     |carta |
      | CASE01   | No Procede   |1      | PEN    | 100     | Si      | Si                   | Correo         | Registrado      | Interbank por Facebook|       2       |   Ninguno     |             |                   |          |                |            |                             |Si    |
      | CASE02   |     Procede   |2      | PEN    | 100     | Si      | Si                   | Direccion         | Registrado      | Interbank por Facebook|       2       |   Cuentas TC - TD     |  1233333444456           |  2788899992838399999                 |          |                |            |                             |Si    |
      | CASE03   | Procede    | 2      | PEN    | 20.6    | Si        | Si                   | Direccion      | Registrado      | ATM GlobalNet         |       2       |   Seguros     |1233333444456|2788899992838399999| 12221110 | 12345567       |CARITAS SUL |                             |No   |
      | CASE04   | No Procede| 1      | PEN    | 160.5   | Si        | Si                   | Correo         | Registrado      | ATM GlobalNet         |       2       |Cuentas TC - TD|             |                   |          |                |            ||Si    |
      | CASE05   | Procede   | 1      | PEN    | 20.6    | Si        | Si                   | Correo         | Registrado      | ATM GlobalNet         |       2       |   Seguros     |             |                   |          |                |            ||Si    |

