@all_generacion_tramite_atencionCliente
Feature: Resolución del trámite producto CREDITO BPE y CUENTA NEGOCIO
#SIN ASIGNAR
  @happy_path @regresion @version3.1.8 @resolucionC_CBPE_cobros_indebidos_test1 @resolucionC_Fase1
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolución del trámite de tipo RECLAMO con producto CREDITO BPE para la tipología: COBROS INDEBIDOS en GIRU Comercial
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU COMERCIAL
    And se obtiene informacion de comercio con producto CRÉDITO BPE
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo tramite comercial RECLAMO y la tipologia COBROS INDEBIDOS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | canal   | CantMovimientosPorMotivo | moneda   | monto   | medioDevolucion|tipoCuenta|carta|
      | <motivos> | <canal> | <cantMovimientos>        | <moneda> | <monto> | <medioDevolucion>|<tipoCuenta>|<carta>|
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | TESTCASE | resolucion | motivos | cantMovimientos | moneda | monto | canal            | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | medioDevolucion | areaValidacionOpcional |tipoCuenta|carta|
      | CASE01   | Procede    | 1       | 3               | PEN    | 100   | Banca Telefónica | Si         | Si                   | Correo         | Registrado        | 1               |                        |2         |No   |
      | CASE02   | No Procede | 2       | 3               | USD    | 100   | Banca Telefónica | Si         | Si                   | Correo         | Registrado        | 2               |                        |3         |Si   |


  @happy_path @regresion @version3.1.8 @resolucionC_CN_cobros_indebidos_test2 @resolucionC_Fase1
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolución del trámite de tipo RECLAMO con producto CUENTA NEGOCIO para la tipología: COBROS INDEBIDOS en GIRU Comercial
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU COMERCIAL
    And se obtiene informacion de comercio con producto CUENTA NEGOCIOS
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo tramite comercial RECLAMO y la tipologia COBROS INDEBIDOS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | canal   | CantMovimientosPorMotivo | moneda   | monto   |carta|tipoCuenta|
      | <motivos> | <canal> | <cantMovimientos>        | <moneda> | <monto> |<carta>|<tipoCuenta>    |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | TESTCASE | resolucion | motivos | cantMovimientos | moneda | monto | canal            | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta  | areaValidacionOpcional | carta|tipoCuenta|
      | CASE01   | Procede    | 1       | 3               | PEN    | 100   | Banca Telefónica | Si         | Si                   | Correo         | Registrado         |                                       | Si|Cuenta corriente|
      | CASE02   | No Procede | 2       | 2               | PEN    | 100   | Banca Telefónica | Si         | Si                   | Correo         | Registrado         |                                       |No |Cuenta corriente|


