@derivacion_test
Feature: Derivacion tramites

  @happy_path @regresion @version3.0.9 @derivacionCuenta_test1
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida la derivacion y resolucion de trámite de tipo PEDIDO con producto CUENTA para la tipología: EXONERACIÓN DE COBROS cuando PROCEDE o NO PROCEDE
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto CUENTA y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia EXONERACIÓN DE COBROS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | carta   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <carta> |
    #And si aplica comision de membresía, se selecciona la Razon Operativa "<RazonOperativa>" y el Tipo de Razon Operativa "<TipoRazonOperativa>"
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA DERIVACION Y RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se selecciona razon <Razon> y el area <Area> al cual se derivara el tramite
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la derivacion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | TESTCASE | Razon                           | Area                    | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd | RazonOperativa | TipoRazonOperativa | resolucion | areaValidacionOpcional  | carta |
      | CASE01   | NecesitaInformacionDeDocumentos | CustodiaDeDocumentos    | 5-6-7   | 1               | PEN    | 200   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | No             | Ninguno            | No Procede | CustodiaDeDocumentos               | No    |
      | CASE02   | NecesitaInformacionDeDocumentos | Procesos de Captaciones | 2-3-4   | 1               | PEN    | 300   | Si         | No                   | Correo         | Registrado        | ACTIVA     | No             | Ninguno            | Procede    | Procesos de Captaciones | No    |
      | CASE03   | NecesitaInformacionDeDocumentos | Legal                   | 2-3-4   | 1               | PEN    | 300   | Si         | No                   | Correo         | Registrado        | ACTIVA     | No             | Ninguno            | Procede    | Legal                   | No    |


  @happy_path @regresion @version3.0.9 @derivacionCuenta_test2
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida la derivacion y resolucion de trámite de tipo RECLAMO con producto CUENTA para la tipología: COBROS INDEBIDOS cuando PROCEDE o NO PROCEDE
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto CUENTA
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia COBROS INDEBIDOS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | carta   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <carta> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA DERIVACION Y RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se selecciona razon <Razon> y el area <Area> al cual se derivara el tramite
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la derivacion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | TESTCASE | Razon                           | Area                    | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd | RazonOperativa | TipoRazonOperativa | resolucion | areaValidacionOpcional  | carta |
      | CASE01   | NecesitaInformacionDeDocumentos | CustodiaDeDocumentos    | 5-6-7   | 1               | PEN    | 200   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | No             | Ninguno            | No Procede | CustodiaDeDocumentos               | No    |
      | CASE02   | NecesitaInformacionDeDocumentos | Procesos de Captaciones | 2-3-4   | 1               | PEN    | 300   | Si         | No                   | Correo         | Registrado        | ACTIVA     | No             | Ninguno            | Procede    | Procesos de Captaciones | No    |
      | CASE03   | NecesitaInformacionDeDocumentos | Legal                   | 2-3-4   | 1               | PEN    | 300   | Si         | No                   | Correo         | Registrado        | ACTIVA     | No             | Ninguno            | Procede    | Legal                   | No    |


  @happy_path @regresion @version3.1.8 @derivacionCuenta_test17
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida la derivación y resolución del trámite de tipo RECLAMO con producto CUENTA para la tipología: TRANSACCIÓN MAL O NO PROCESADA cuando PROCEDE o NO PROCEDE
    Given que el usuario SUPERVISOR accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto CUENTA
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia TRANSACCIÓN MAL O NO PROCESADA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | canal   | carta   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <canal> | <carta> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
  #SE INICIA LA DERIVACION Y RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se selecciona razon <Razon> y el area <Area> al cual se derivara el tramite
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la derivacion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | TESTCASE | Razon                           | Area                    | motivos | cantMovimientos | moneda | monto | canal   | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional  | carta |
      | CASE01   | NecesitaInformacionDeDocumentos | Legal                   | 1       | 3               | PEN    | 100   | App IBK | Si         | Si                   | Direccion      | Registrado        | Procede    | Legal                   | Si    |
      | CASE01   | NecesitaInformacionDeDocumentos | Procesos de Captaciones | 1       | 3               | PEN    | 100   | App IBK | Si         | Si                   | Direccion      | Registrado        | Procede    | Procesos de Captaciones | Si    |
