@derivacion_test
Feature: Derivacion tramites

  @happy_path @regresion @version3.1.7 @derivacionAtencionCliente_test8
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolución del trámite de tipo SOLICITUD con producto ATENCION AL CLIENTE para la tipología: DENUNCIAS LEGAL cuando PROCEDE o NO PROCEDE
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto ATENCION AL CLIENTE
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia DENUNCIAS LEGAL
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | carta   |
      | <motivos> | <carta> |
    And se ingresa una respuesta al cliente "No" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se selecciona razon <Razon> y el area <Area> al cual se derivara el tramite
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la derivacion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | TESTCASE | Razon                           | Area                             | motivos | comentario | indicaArchivoAdjunto | resolucion | carta | resolucion | areaValidacionOpcional |
      | CASE01   | NecesitaInformacionDeDocumentos | Gestión De Pedidos Y Reclamos TC | 1       | Si         | Si                   | Procede    | No    | No Procede | CUSTODIA               |
      | CASE01   | NecesitaInformacionDeDocumentos | Gestión De Reclamos Regulatorios | 1       | Si         | Si                   | Procede    | No    | No Procede | CUSTODIA               |
      | CASE01   | NecesitaInformacionDeDocumentos | U-Segto-Pedidos Recl             | 1       | Si         | Si                   | Procede    | No    | No Procede | CUSTODIA               |
      | CASE01   | NecesitaInformacionDeDocumentos | Controversias                    | 1       | Si         | Si                   | Procede    | No    | No Procede | CUSTODIA               |
      | CASE01   | NecesitaInformacionDeDocumentos | Retenciones Judiciales           | 1       | Si         | Si                   | Procede    | No    | No Procede | CUSTODIA               |
      | CASE01   | NecesitaInformacionDeDocumentos | Hipotecario Post Venta           | 1       | Si         | Si                   | Procede    | No    | No Procede | CUSTODIA               |
      | CASE01   | NecesitaInformacionDeDocumentos | Gestión de la Información COB    | 1       | Si         | Si                   | Procede    | No    | No Procede | CUSTODIA               |
      | CASE01   | NecesitaInformacionDeDocumentos | Seguros Relacionados             | 1       | Si         | Si                   | Procede    | No    | No Procede | CUSTODIA               |
      | CASE01   | NecesitaInformacionDeDocumentos | Nóminas                          | 1       | Si         | Si                   | Procede    | No    | No Procede | CUSTODIA               |
      | CASE01   | NecesitaInformacionDeDocumentos | Convenios Post Venta             | 1       | Si         | Si                   | Procede    | No    | No Procede | CUSTODIA               |
