@derivacion_test
Feature: Derivacion tramites

  @happy_path @regresion @version3.0.9 @derivacionTarjetaCredito_test1
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida la derivacion y resolucion de trámite de tipo PEDIDO con producto TARJETA DE CREDITO para la tipología: EXONERACIÓN DE COBROS cuando PROCEDE o NO PROCEDE
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia EXONERACIÓN DE COBROS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | carta   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <carta> |
    #And si aplica comision de membresía, se selecciona la Razon Operativa "<RazonOperativa>" y el Tipo de Razon Operativa "<TipoRazonOperativa>"
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA DERIVACION Y RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se selecciona razon <Razon> y el area <Area> al cual se derivara el tramite
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la derivacion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | TESTCASE | Razon                                              | Area                 | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd | RazonOperativa | TipoRazonOperativa | resolucion | areaValidacionOpcional | carta |
      | CASE01   | NecesitaInformacionDeDocumentos                    | EECC-TC              | 2-3-4   | 1               | PEN    | 100   | Si         | No                   | Correo         | Registrado        | ACTIVA     | No             | Ninguno            | Procede    | EECC TC                | No    |
      | CASE02   | NecesitaInformacionDeDocumentos                    | CustodiaDeDocumentos | 5-6-7   | 1               | PEN    | 200   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | No             | Ninguno            | No Procede | CUSTODIA               | No    |
      | CASE03   | NecesitaValidacionDeAfilaciondeAreaCorrespondiente | EECC-TC              | 2-3-4   | 1               | PEN    | 300   | Si         | No                   | Correo         | Registrado        | ACTIVA     | No             | Ninguno            | Procede    | EECC TC                | No    |
      | CASE04   | NecesitaValidacionDeAfilaciondeAreaCorrespondiente | CustodiaDeDocumentos | 5-6-7   | 1               | PEN    | 400   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | No             | Ninguno            | No Procede | CUSTODIA               | No    |


  @happy_path @regresion @version3.0.9 @derivacionTarjetaCredito_test2
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida derivación y resolución de trámite de tipo RECLAMO con producto TARJETA DE CREDITO para la tipología: COBROS INDEBIDOS cuando PROCEDE o NO PROCEDE
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia COBROS INDEBIDOS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | carta   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <carta> |
   # And si aplica comision de membresía, se selecciona la Razon Operativa "<RazonOperativa>" y el Tipo de Razon Operativa "<TipoRazonOperativa>"
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA DERIVACION Y RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se selecciona razon <Razon> y el area <Area> al cual se derivara el tramite
    And se adjuntara un archivo adicional en caso sea necesario
    #And se confirma la resolucion del tramite
    And se confirma la derivacion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | TESTCASE | Razon                           | Area                 | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd | RazonOperativa | TipoRazonOperativa | resolucion | areaValidacionOpcional         | carta |
      | CASE01   | NecesitaInformacionDeDocumentos | EECC-TC              | 2-3-4   | 1               | PEN    | 100   | Si         | No                   | Correo         | Registrado        | ACTIVA     | No             | Ninguno            | Procede    | EECC TC                        | No    |
      | CASE02   | NecesitaInformacionDeDocumentos | CustodiaDeDocumentos | 5-6-7   | 1               | PEN    | 200   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | No             | Ninguno            | No Procede | CUSTODIA                       | No    |
      | CASE03   | NecesitaInformacionDeDocumentos | Prod.YDistribucionTC | 2-3-4   | 1               | PEN    | 100   | Si         | No                   | Correo         | Registrado        | ACTIVA     | No             | Ninguno            | Procede    | PRODUCCCION Y DISTRIBUCCIÓN TC | No    |
      | CASE04   | NecesitaInformacionDeDocumentos | MensajeriaIE         | 5-6-7   | 1               | PEN    | 200   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | No             | Ninguno            | No Procede | MENSAJERIA                     | No    |


  @happy_path @regresion @version3.0.9 @derivacionTarjetaCredito_test3
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida la derivacion y resolucion de trámite de tipo RECLAMO con producto TARJETA DE CREDITO para la tipología: CONSUMOS NO RECONOCIDOS POR FRAUDE cuando PROCEDE o NO PROCEDE
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia CONSUMOS NO RECONOCIDOS POR FRAUDE
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | carta   | abono   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <carta> | <abono> |
    And selecciono lo sucedido "<MotivoSucedidoTarjeta>" con la tarjeta, si se ha bloqueado "<flagTarjetaBloqueo>"
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA DERIVACION Y RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se selecciona razon <Razon> y el area <Area> al cual se derivara el tramite
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la derivacion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | TESTCASE | Razon                           | Area                             | abono               | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd | MotivoSucedidoTarjeta | flagTarjetaBloqueo | resolucion | areaValidacionOpcional           | carta |
      | CASE01   | NecesitaInformacionDeDocumentos | Gestión De Pedidos Y Reclamos TC | TodosLosMovimientos | 1       | 3               | PEN    | 100   | Si         | Si                   | Correo         | Registrado        | ACTIVA     | CFS001                | Si                 | Procede    | Gestión De Pedidos Y Reclamos TC | No    |

#FALTA TERMINAR
  @happy_path @regresion @version3.0.9 @resolucionTarjetaCredito_test4_d
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolucion de trámite de tipo RECLAMO con producto TARJETA DE CREDITO para la tipología: CONSUMOS MAL PROCESADOS POR EL COMERCIO/MARCA cuando PROCEDE o NO PROCEDE
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia CONSUMOS MAL PROCESADOS POR EL COMERCIO/MARCA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | productoServicioAdquirido   | tipoServicio   | carta   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <productoServicioAdquirido> | <tipoServicio> | <carta> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se selecciona razon <Razon> y el area <Area> al cual se derivara el tramite
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la derivacion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples: #ADJUNTAR ARCHIVO - OBLIGATORIO
      | TESTCASE | Razon                                           | Area                             | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd | productoServicioAdquirido | tipoServicio | resolucion | areaValidacionOpcional           | carta |
      | CASE01   | NecesitaSerAnalizadoSegundoEspecialistaEnMiArea | Gestión De Pedidos Y Reclamos TC | 1-3     | 1               | PEN    | 100   | Si         | Si                   | Correo         | Registrado        | ACTIVA     |                           |              | Procede    | Gestión De Pedidos Y Reclamos TC | Si    |

