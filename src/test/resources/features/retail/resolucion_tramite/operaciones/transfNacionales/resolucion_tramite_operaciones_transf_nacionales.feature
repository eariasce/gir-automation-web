@all_regresionOperation1
@all_resolucion_tramite_transfNacionales
Feature: Resolucion del trámite Operaciones de TRANSF. NACIONALES

  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- RESOLUCIÓN RECLAMO ----------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.4.1 @resolucionTransfNacionales_test1
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolucion de trámite de tipo RECLAMO con operaciones TRANSF. NACIONALES para la tipología: TRANSACCIÓN MAL O NO PROCESADA cuando PROCEDE o NO PROCEDE
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TRANSF. NACIONALES
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia TRANSACCIÓN MAL O NO PROCESADA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | tipoTransferencia   | canal   | carta   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <tipoTransferencia> | <canal> | <carta> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | tipoTransferencia | canal     | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | 1-2-3   | 3               | PEN    | 100   | Entrada           | App IBK   | Si         | Si                   | Correo         | Registrado        | Procede    |                        | Si    |
      | CASE02   | 3-4-5   | 1               | PEN    | 200   | Salida            | Bca Telef | Si         | No                   | Direccion      | Registrado        | No Procede |                        | Si    |
      | CASE03   | 1       | 1               | PEN    | 300   | Entrada           | App IBK   | Si         | No                   | Correo         | Registrado        | Procede    |                        | No    |
      | CASE04   | 2       | 1               | PEN    | 400   | Salida            | Bca Telef | Si         | No                   | Direccion      | Registrado        | No Procede |                        | No    |


  @happy_path @regresion @version3.1.8 @resolucionTransfNacionales_test2
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolución del trámite de tipo RECLAMO con operaciones TRANSF. NACIONALES para la tipología: COBROS INDEBIDOS cuando PROCEDE o NO PROCEDE
    Given que el usuario SUPERVISOR accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TRANSF. NACIONALES
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia COBROS INDEBIDOS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | tipoTransferencia   | canal   | carta   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <tipoTransferencia> | <canal> | <carta> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | tipoTransferencia | canal          | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | 1-2     | 3               | PEN    | 100   | Entrada           | App IBK        | Si         | Si                   | Correo         | Registrado        | Procede    |                        | Si    |
      | CASE02   | 1       | 1               | PEN    | 100   | Salida            | Bca Telef      | Si         | No                   | Direccion      | Registrado        | No Procede |                        | Si    |
      | CASE03   | 2       | 1               | PEN    | 100   | Entrada           | Bca x internet | Si         | No                   | Correo         | Registrado        | Procede    |                        | No    |
      | CASE04   | 1       | 1               | PEN    | 100   | Salida            | Canal Select   | Si         | No                   | Direccion      | Registrado        | No Procede |                        | No    |

