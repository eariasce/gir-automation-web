@all_regresionOperation1
@all_resolucion_tramite_remesas
Feature: Resolucion del trámite Operaciones de REMESAS

  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- RESOLUCIÓN RECLAMO ----------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.4.1 @resolucionRemesas_test1
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolucion de trámite de tipo RECLAMO con operaciones REMESAS para la tipología: ATM DE IB NO ENTREGÓ DINERO cuando PROCEDE o NO PROCEDE
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto REMESAS
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia ATM DE IB NO ENTREGÓ DINERO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | canal   | carta   |
      | <motivos> | <cantMovimientos>        | <moneda> | <canal> | <carta> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | canal     | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | 1-2-3   | 3               | PEN    | App IBK   | Si         | Si                   | Correo         | Registrado        | Procede    |                        | Si    |
      | CASE02   | 2-3-4   | 1               | PEN    | Bca Telef | Si         | No                   | Direccion      | Registrado        | No Procede |                        | Si    |
      | CASE03   | 1-2-3   | 1               | PEN    | App IBK   | Si         | No                   | Correo         | Registrado        | Procede    |                        | No    |
      | CASE04   | 2-3-4   | 1               | PEN    | Bca Telef | Si         | No                   | Direccion      | Registrado        | No Procede |                        | No    |


  @happy_path @regresion @version3.1.5 @resolucionRemesas_test2
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolucion de trámite de tipo PEDIDO con operaciones REMESAS para la tipología: CONSTANCIA POR TRANSFERENCIAS RECIBIDAS cuando PROCEDE o NO PROCEDE
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto REMESAS
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia CONSTANCIA POR TRANSFERENCIAS RECIBIDAS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | tipoRemesa   | carta   |
      | <motivos> | <tipoRemesa> | <carta> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples: #* TIPOLOGIA CON ARPI
      | TESTCASE | motivos | tipoRemesa | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | 1       | Entregada  | Si         | Si                   | Correo         | Registrado        | Procede    |                        | Si    |
      | CASE02   | 1       | Recibida   | Si         | No                   | Direccion      | Registrado        | No Procede |                        | Si    |
      | CASE03   | 1       | Entregada  | Si         | No                   | Correo         | Registrado        | Procede    |                        | No    |
      | CASE04   | 1       | Recibida   | Si         | No                   | Direccion      | Registrado        | No Procede |                        | No    |


  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- RESOLUCIÓN RECLAMO ----------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.5 @resolucionRemesas_test3
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolucion de trámite de tipo RECLAMO con operaciones REMESAS para la tipología: COBROS INDEBIDOS cuando PROCEDE o NO PROCEDE
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto REMESAS
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia COBROS INDEBIDOS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | tipoRemesa   | carta   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <tipoRemesa> | <carta> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | tipoRemesa | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | 1-2-3   | 3               | PEN    | 100   | Entregada  | Si         | Si                   | Correo         | Registrado        | Procede    |                        | Si    |
      | CASE02   | 2-3-4   | 1               | PEN    | 100   | Recibida   | Si         | No                   | Direccion      | Registrado        | No Procede |                        | Si    |
      | CASE03   | 1       | 1               | PEN    | 100   | Entregada  | Si         | No                   | Correo         | Registrado        | Procede    |                        | No    |
      | CASE04   | 2       | 1               | PEN    | 100   | Recibida   | Si         | No                   | Direccion      | Registrado        | No Procede |                        | No    |


  @happy_path @regresion @version3.1.5 @resolucionRemesas_test4
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolucion de trámite de tipo RECLAMO con operaciones REMESAS para la tipología: NO RECEPCIÓN DE TRANSFERENCIA IN/OUT cuando PROCEDE o NO PROCEDE
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto REMESAS
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia NO RECEPCIÓN DE TRANSFERENCIA IN/OUT
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | carta   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <carta> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | 1       | 3               | PEN    | 100   | Si         | Si                   | Correo         | Registrado        | Procede    |                        | Si    |
      | CASE02   | 1       | 1               | PEN    | 100   | Si         | No                   | Direccion      | Registrado        | No Procede |                        | Si    |
      | CASE03   | 1       | 1               | PEN    | 100   | Si         | No                   | Correo         | Registrado        | Procede    |                        | No    |
      | CASE04   | 1       | 1               | PEN    | 100   | Si         | No                   | Direccion      | Registrado        | No Procede |                        | No    |


  @happy_path @regresion @version3.1.7 @resolucionRemesas_test5
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolucion de trámite de tipo RECLAMO con operaciones REMESAS para la tipología: CONSUMO/ RETIRO NO EFECTUADO cuando PROCEDE o NO PROCEDE
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto REMESAS
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia CONSUMO/ RETIRO NO EFECTUADO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | canal   | carta   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <canal> | <carta> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | canal          | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | 1       | 3               | PEN    | 100   | App IBK        | Si         | Si                   | Correo         | Registrado        | Procede    |                        | Si    |
      | CASE02   | 1       | 1               | PEN    | 200   | Bca Telef      | Si         | No                   | Correo         | Registrado        | No Procede |                        | Si    |
      | CASE03   | 1       | 1               | PEN    | 300   | Bca x internet | Si         | No                   | Direccion      | Registrado        | Procede    |                        | No    |
      | CASE04   | 1       | 1               | PEN    | 400   | Canal Select   | Si         | No                   | Direccion      | Registrado        | No Procede |                        | No    |


  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- RESOLUCIÓN SOLICITUD --------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.7 @resolucionRemesas_test6
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolucion de trámite de tipo SOLICITUD con operaciones REMESAS para la tipología: ATENCIÓN IBK AGENTE cuando PROCEDE o NO PROCEDE
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto REMESAS
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia ATENCIÓN IBK AGENTE
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | carta   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <carta> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | 1-2-3   | 3               | PEN    | 100   | Si         | Si                   | Correo         | Registrado        | Procede    |                        | Si    |
      | CASE02   | 2-3-4   | 1               | PEN    | 100   | Si         | No                   | Correo         | Registrado        | No Procede |                        | Si    |
      | CASE03   | 1-2-3   | 1               | PEN    | 100   | Si         | No                   | Correo         | Registrado        | Procede    |                        | No    |
      | CASE04   | 2-3-4   | 1               | PEN    | 100   | Si         | No                   | Correo         | Registrado        | No Procede |                        | No    |



