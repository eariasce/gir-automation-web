@all_regresionProduct2
@all_resolucion_tramite_segurosRelacionados
Feature: Resolucion del trámite Producto de SEGUROS RELACIONADOS

  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- RESOLUCIÓN PEDIDO------------------------------------------
  #===========================================================================================================
  @happy_path @regresion @version3.1.7 @resolucionSegurosRelacionados_test1
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolucion del trámite de tipo PEDIDO con producto SEGUROS RELACIONADOS para la tipología: ENDOSO SEGUROS DEL BIEN cuando PROCEDE o NO PROCEDE
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto SEGUROS RELACIONADOS
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia ENDOSO SEGUROS DEL BIEN
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | carta   |
      | <motivos> | <carta> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | 1-2     | Si         | Si                   | Correo         | Registrado        | Procede    |                        | Si    |
      | CASE02   | 1       | Si         | Si                   | Correo         | Registrado        | No Procede |                        | Si    |
      | CASE03   | 2       | Si         | Si                   | Correo         | Registrado        | Procede    |                        | No    |
      | CASE04   | 1       | Si         | Si                   | Correo         | Registrado        | No Procede |                        | No    |



  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- RESOLUCIÓN SOLICITUD---------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.7 @resolucionSegurosRelacionados_test2
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolucion del trámite de tipo SOLICITUD con producto SEGUROS RELACIONADOS para la tipología: APLICACIÓN DE FONDOS POR SINIESTROS/DESGRAVAMEN cuando PROCEDE o NO PROCEDE
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto SEGUROS RELACIONADOS
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia APLICACIÓN DE FONDOS POR SINIESTROS/DESGRAVAMEN
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | carta   |
      | <motivos> | <carta> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | 1-2     | Si         | Si                   | Correo         | Registrado        | Procede    |                        | Si    |
      | CASE02   | 1-2     | Si         | Si                   | Correo         | Registrado        | No Procede |                        | Si    |
      | CASE03   | 1-2     | Si         | Si                   | Correo         | Registrado        | Procede    |                        | No    |
      | CASE04   | 1-2     | Si         | Si                   | Correo         | Registrado        | No Procede |                        | No    |


  @happy_path @regresion @version3.1.7 @resolucionSegurosRelacionados_test3
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolucion del trámite de tipo SOLICITUD con producto SEGUROS RELACIONADOS para la tipología: SOLICITUD DE INFORMACIÓN DE SINIESTROS cuando PROCEDE o NO PROCEDE
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto SEGUROS RELACIONADOS
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia SOLICITUD DE INFORMACIÓN DE SINIESTROS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | carta   |
      | <motivos> | <carta> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | 1-2-3   | Si         | Si                   | Correo         | Registrado        | Procede    |                        | Si    |
      | CASE02   | 5-4-6   | Si         | No                   | Correo         | Registrado        | No Procede |                        | Si    |
      | CASE03   | 1-2-7   | Si         | No                   | Correo         | Registrado        | Procede    |                        | No    |
      | CASE04   | 3-5     | Si         | No                   | Correo         | Registrado        | No Procede |                        | No    |


  @happy_path @regresion @version3.1.7 @resolucionSegurosRelacionados_test4
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolucion del trámite de tipo SOLICITUD con producto SEGUROS RELACIONADOS para la tipología: LEVANTAMIENTO DE ENDOSO cuando PROCEDE o NO PROCEDE
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto SEGUROS RELACIONADOS
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia LEVANTAMIENTO DE ENDOSO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | carta   |
      | <motivos> | <carta> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | 1       | Si         | Si                   | Correo         | Registrado        | Procede    |                        | Si    |
      | CASE02   | 1       | Si         | No                   | Correo         | Registrado        | No Procede |                        | Si    |
      | CASE03   | 1       | Si         | No                   | Correo         | Registrado        | Procede    |                        | No    |
      | CASE04   | 1       | Si         | No                   | Correo         | Registrado        | No Procede |                        | No    |


  @happy_path @regresion @version3.1.8 @resolucionSegurosRelacionados_test5
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolución del trámite de tipo PEDIDO con producto SEGUROS RELACIONADOS para la tipología: DUPLICADO DE DOCUMENTO cuando PROCEDE o NO PROCEDE
    Given que el usuario SUPERVISOR accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto SEGUROS RELACIONADOS
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia DUPLICADO DE DOCUMENTO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | tipoFiltroProducto   | carta   |
      | <motivos> | <tipoFiltroProducto> | <carta> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | TESTCASE | motivos | tipoFiltroProducto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | 1-2-3   | Tarjeta de Crédito | Si         | Si                   | Correo         | Registrado        | Procede    |                        | Si    |
      | CASE02   | 2-3-4   | Crédito            | Si         | No                   | Direccion      | Registrado        | No Procede |                        | Si    |
      | CASE03   | 1       | Tarjeta de Crédito | Si         | No                   | Correo         | Registrado        | Procede    |                        | No    |
      | CASE04   | 2       | Crédito            | Si         | No                   | Direccion      | Registrado        | No Procede |                        | No    |


  @happy_path @regresion @version3.1.8 @resolucionSegurosRelacionados_test6
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolución del trámite de tipo PEDIDO con producto SEGUROS RELACIONADOS para la tipología: INFORMACION DE MOVIMIENTOS cuando PROCEDE o NO PROCEDE
    Given que el usuario SUPERVISOR accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto SEGUROS RELACIONADOS
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia INFORMACION DE MOVIMIENTOS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | canal   | carta   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <canal> | <carta> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | canal          | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | 1-2-3   | 3               | PEN    | 100   | App IBK        | Si         | Si                   | Correo         | Registrado        | Procede    |                        | Si    |
      | CASE02   | 2-3-4   | 1               | PEN    | 100   | Bca Telef      | Si         | No                   | Direccion      | Registrado        | No Procede |                        | Si    |
      | CASE03   | 1       | 1               | PEN    | 100   | Bca x internet | Si         | No                   | Correo         | Registrado        | Procede    |                        | No    |
      | CASE04   | 2       | 1               | PEN    | 100   | Canal Select   | Si         | No                   | Direccion      | Registrado        | No Procede |                        | No    |


  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- RESOLUCIÓN RECLAMO ----------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.8 @resolucionSegurosRelacionados_test7
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolución del trámite de tipo RECLAMO con producto SEGUROS RELACIONADOS para la tipología: COBROS INDEBIDOS cuando PROCEDE o NO PROCEDE
    Given que el usuario SUPERVISOR accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto SEGUROS RELACIONADOS
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia COBROS INDEBIDOS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | tipoProducto   | canal   | carta   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <tipoProducto> | <canal> | <carta> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | tipoProducto       | canal          | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | 1-2-3   | 3               | PEN    | 100   | Crédito            | App IBK        | Si         | Si                   | Correo         | Registrado        | Procede    |                        | Si    |
      | CASE02   | 2-3-4   | 1               | PEN    | 100   | Cuenta             | Bca Telef      | Si         | No                   | Direccion      | Registrado        | No Procede |                        | Si    |
      | CASE03   | 1       | 1               | PEN    | 100   | Tarjeta de crédito | Bca x internet | Si         | No                   | Correo         | Registrado        | Procede    |                        | No    |
      | CASE04   | 2       | 1               | PEN    | 100   | Crédito            | Canal Select   | Si         | No                   | Direccion      | Registrado        | No Procede |                        | No    |

