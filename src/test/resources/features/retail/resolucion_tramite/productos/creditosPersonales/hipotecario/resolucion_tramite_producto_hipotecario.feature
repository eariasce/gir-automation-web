@all_regresionProduct
@all_resolucion_tramite_hipotecario
Feature: Resolucion de trámite de Producto HIPOTECARIO

  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- RESOLUCION PEDIDO -----------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.5 @resolucionHipotecario_test1
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolucion del trámite de tipo PEDIDO con producto HIPOTECARIO para la tipología: BAJA DE TASA cuando PROCEDE o NO PROCEDE
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto HIPOTECARIO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia BAJA DE TASA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | carta   |
      | <motivos> | <carta> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | 1       | Si         | Si                   | Correo         | Registrado        | Procede    |                        | Si    |
      | CASE02   | 1       | Si         | No                   | Direccion      | Registrado        | No Procede |                        | Si    |
      | CASE03   | 1       | Si         | No                   | Correo         | Registrado        | Procede    |                        | No    |
      | CASE04   | 1       | Si         | No                   | Direccion      | Registrado        | No Procede |                        | No    |


  @happy_path @regresion @version3.1.5 @resolucionHipotecario_test2
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolucion del trámite de tipo PEDIDO con producto HIPOTECARIO para la tipología: DUPLICADO DE CRONOGRAMA cuando PROCEDE o NO PROCEDE
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto HIPOTECARIO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia DUPLICADO DE CRONOGRAMA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | carta   |
      | <motivos> | <carta> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | 1       | Si         | Si                   | Correo         | Registrado        | Procede    |                        | Si    |
      | CASE02   | 1       | Si         | No                   | Direccion      | Registrado        | No Procede |                        | Si    |
      | CASE03   | 1       | Si         | No                   | Correo         | Registrado        | Procede    |                        | No    |
      | CASE04   | 1       | Si         | No                   | Direccion      | Registrado        | No Procede |                        | No    |


  @happy_path @regresion @version3.1.5 @resolucionHipotecario_test3
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolucion del trámite de tipo PEDIDO con producto HIPOTECARIO para la tipología: DEVOLUCIÓN DE DOCUMENTOS cuando PROCEDE o NO PROCEDE
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto HIPOTECARIO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia DEVOLUCIÓN DE DOCUMENTOS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | tipoDocumentoTramite   | carta   |
      | <motivos> | <tipoDocumentoTramite> | <carta> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | TESTCASE | motivos | tipoDocumentoTramite    | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | 1       | Contrato                | Si         | Si                   | Correo         | Registrado        | Procede    |                        | Si    |
      | CASE02   | 1       | Constancia de Operación | Si         | No                   | Direccion      | Registrado        | No Procede |                        | Si    |
      | CASE03   | 1       | Constancia de Pago      | Si         | No                   | Correo         | Registrado        | Procede    |                        | No    |
      | CASE04   | 1       | Constancia de Retiro    | Si         | No                   | Direccion      | Registrado        | No Procede |                        | No    |


  @happy_path @regresion @version3.1.5 @resolucionHipotecario_test4
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolucion del trámite de tipo PEDIDO con producto HIPOTECARIO para la tipología: DUPLICADO DE DOCUMENTO cuando PROCEDE o NO PROCEDE
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto HIPOTECARIO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia DUPLICADO DE DOCUMENTO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | tipoDocumentoTramite   | carta   |
      | <motivos> | <tipoDocumentoTramite> | <carta> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | TESTCASE | motivos | tipoDocumentoTramite    | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | 1-2     | Contrato                | Si         | Si                   | Correo         | Registrado        | Procede    |                        | Si    |
      | CASE02   | 1       | Constancia de Operación | Si         | No                   | Direccion      | Registrado        | No Procede |                        | Si    |
      | CASE03   | 2       | Constancia de Pago      | Si         | No                   | Correo         | Registrado        | Procede    |                        | No    |
      | CASE04   | 1       | Constancia de Retiro    | Si         | No                   | Direccion      | Registrado        | No Procede |                        | No    |


  @happy_path @regresion @version3.1.5 @resolucionHipotecario_test5
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolucion del trámite de tipo PEDIDO con producto HIPOTECARIO para la tipología: PAGO ANTICIPADO Y ADELANTO DE CUOTAS cuando PROCEDE o NO PROCEDE
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto HIPOTECARIO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia PAGO ANTICIPADO Y ADELANTO DE CUOTAS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | carta   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <carta> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | 1-2-3   | 3               | PEN    | 100   | Si         | Si                   | Correo         | Registrado        | Procede    |                        | Si    |
      | CASE02   | 2-3-4   | 1               | PEN    | 100   | Si         | No                   | Direccion      | Registrado        | No Procede |                        | Si    |
      | CASE03   | 1       | 1               | PEN    | 100   | Si         | No                   | Correo         | Registrado        | Procede    |                        | No    |
      | CASE04   | 2       | 1               | PEN    | 100   | Si         | No                   | Direccion      | Registrado        | No Procede |                        | No    |


  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- RESOLUCION RECLAMO ----------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.5 @resolucionHipotecario_test6
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolucion del trámite de tipo RECLAMO con producto HIPOTECARIO para la tipología: DESACUERDO DE CONDICIONES/TASAS/TARIFAS cuando PROCEDE o NO PROCEDE
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto HIPOTECARIO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia DESACUERDO DE CONDICIONES/TASAS/TARIFAS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | carta   |
      | <motivos> | <carta> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | 1-2-3   | Si         | Si                   | Correo         | Registrado        | Procede    |                        | Si    |
      | CASE02   | 1       | Si         | No                   | Direccion      | Registrado        | No Procede |                        | Si    |
      | CASE03   | 2       | Si         | No                   | Correo         | Registrado        | Procede    |                        | No    |
      | CASE04   | 1       | Si         | No                   | Direccion      | Registrado        | No Procede |                        | No    |


  @happy_path @regresion @version3.1.5 @resolucionHipotecario_test7
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolucion del trámite de tipo RECLAMO con producto HIPOTECARIO para la tipología: NO RECEPCIÓN DE DOCUMENTO cuando PROCEDE o NO PROCEDE
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto HIPOTECARIO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia NO RECEPCIÓN DE DOCUMENTO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | tipoDocumentoTramite   | carta   |
      | <motivos> | <tipoDocumentoTramite> | <carta> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | TESTCASE | motivos | tipoDocumentoTramite    | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | 1       | Contrato                | Si         | Si                   | Correo         | Registrado        | Procede    |                        | Si    |
      | CASE02   | 1       | Constancia de Operación | Si         | No                   | Direccion      | Registrado        | No Procede |                        | Si    |
      | CASE03   | 1       | Constancia de Pago      | Si         | No                   | Correo         | Registrado        | Procede    |                        | No    |
      | CASE04   | 1       | Constancia de Retiro    | Si         | No                   | Direccion      | Registrado        | No Procede |                        | No    |


  @happy_path @regresion @version3.1.5 @resolucionHipotecario_test8
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolucion del trámite de tipo RECLAMO con producto HIPOTECARIO para la tipología: NO TRANSACCIÓN MAL O NO PROCESADA cuando PROCEDE o NO PROCEDE
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto HIPOTECARIO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia TRANSACCIÓN MAL O NO PROCESADA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | canal   | carta   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <canal> | <carta> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | canal     | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | 1-2-3   | 3               | PEN    | 100   | App IBK   | Si         | Si                   | Correo         | Registrado        | Procede    |                        | Si    |
      | CASE02   | 3-4-5   | 1               | PEN    | 200   | Bca Telef | Si         | No                   | Direccion      | Registrado        | No Procede |                        | Si    |
      | CASE03   | 1       | 1               | PEN    | 100   | App IBK   | Si         | No                   | Correo         | Registrado        | Procede    |                        | No    |
      | CASE04   | 2       | 1               | PEN    | 200   | Bca Telef | Si         | No                   | Direccion      | Registrado        | No Procede |                        | No    |


  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- RESOLUCIÓN SOLICITUD---------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.5 @resolucionHipotecario_test9
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolucion del trámite de tipo SOLICITUD con producto HIPOTECARIO para la tipología: CUOTA INICIAL HASTA 25% AFP cuando PROCEDE o NO PROCEDE
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto HIPOTECARIO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia CUOTA INICIAL HASTA 25% AFP
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | carta   |
      | <motivos> | <carta> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador ENTEL
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples: #*ADJUNTAR DOCUMENTOS OBLIGATORIOS - EN TAL CASO GIRU NO DEBERIA PERMITIR REGISTRO
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | 1       | Si         | Si                   | Correo         | Registrado        | Procede    |                        | Si    |
      | CASE02   | 1       | Si         | Si                   | Direccion      | Registrado        | No Procede |                        | Si    |
      | CASE03   | 1       | Si         | Si                   | Correo         | Registrado        | Procede    |                        | No    |
      | CASE04   | 1       | Si         | Si                   | Direccion      | Registrado        | No Procede |                        | No    |


  @happy_path @regresion @version3.1.6 @resolucionHipotecario_test10
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolución del trámite de tipo RECLAMO con producto HIPOTECARIO para la tipología: COBROS INDEBIDOS cuando PROCEDE o NO PROCEDE
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto HIPOTECARIO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia COBROS INDEBIDOS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | canal   | carta   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <canal> | <carta> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | canal     | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | 1-2-3   | 3               | PEN    | 100   | Whatsapp  | Si         | Si                   | Correo         | Registrado        | Procede    |                        | Si    |
      | CASE02   | 2-3-4   | 3               | PEN    | 200   | Bca Telef | Si         | No                   | Direccion      | Registrado        | No Procede |                        | Si    |
      | CASE03   | 1-2-3   | 3               | PEN    | 100   | Whatsapp  | Si         | Si                   | Correo         | Registrado        | Procede    |                        | No    |
      | CASE04   | 2-3-4   | 3               | PEN    | 200   | Bca Telef | Si         | No                   | Direccion      | Registrado        | No Procede |                        | No    |


  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- RESOLUCIÓN SOLICITUD---------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.6 @resolucionHipotecario_test11
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolución del trámite de tipo PEDIDO con producto HIPOTECARIO para la tipología: EMISIÓN DE PRE CONFORMIDAD AFP 25%
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto HIPOTECARIO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia EMISIÓN DE PRE CONFORMIDAD AFP 25%
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | moneda   | carta   |
      | <motivos> | <moneda> | <carta> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | TESTCASE | motivos | moneda | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | 1       | PEN    | Si         | Si                   | Correo         | Registrado        | Procede    |                        | Si    |
      | CASE02   | 1       | USD    | Si         | Si                   | Direccion      | Registrado        | No Procede |                        | Si    |
      | CASE03   | 1       | PEN    | Si         | Si                   | Correo         | Registrado        | Procede    |                        | No    |
      | CASE04   | 1       | USD    | Si         | Si                   | Direccion      | Registrado        | No Procede |                        | No    |


  @happy_path @regresion @version3.1.7 @resolucionHipotecario_test12
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolución del trámite de tipo PEDIDO con producto HIPOTECARIO para la tipología: INFORMACION DE MOVIMIENTOS cuando PROCEDE o NO PROCEDE
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto HIPOTECARIO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia INFORMACION DE MOVIMIENTOS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | carta   |
      | <motivos> | <carta> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | 1       | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE02   | 1       | Si         | No                   | Direccion      | Registrado        | No Procede |                        | Si    |
      | CASE03   | 1       | Si         | No                   | Direccion      | Registrado        | Procede    |                        | No    |
      | CASE04   | 1       | Si         | No                   | Direccion      | Registrado        | No Procede |                        | No    |


  @happy_path @regresion @version3.1.7 @resolucionHipotecario_test13
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolución del trámite de tipo PEDIDO con producto HIPOTECARIO para la tipología: EXONERACIÓN DE COBROS cuando PROCEDE o NO PROCEDE
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto HIPOTECARIO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia EXONERACIÓN DE COBROS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | carta   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <carta> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | 1-2-3   | 3               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE02   | 1       | 1               | PEN    | 100   | Si         | No                   | Direccion      | Registrado        | No Procede |                        | Si    |
      | CASE03   | 2       | 1               | PEN    | 300   | Si         | No                   | Direccion      | Registrado        | Procede    |                        | No    |
      | CASE04   | 3       | 1               | PEN    | 400   | Si         | No                   | Direccion      | Registrado        | No Procede |                        | No    |


  @happy_path @regresion @version3.1.7 @resolucionHipotecario_test14
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolución del trámite de tipo PEDIDO con producto HIPOTECARIO para la tipología: LIQUIDACIÓN DE DEUDA cuando PROCEDE o NO PROCEDE
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto HIPOTECARIO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia LIQUIDACIÓN DE DEUDA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | carta   |
      | <motivos> | <carta> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | 1       | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE02   | 1       | Si         | No                   | Direccion      | Registrado        | No Procede |                        | Si    |
      | CASE03   | 1       | Si         | No                   | Direccion      | Registrado        | Procede    |                        | No    |
      | CASE04   | 1       | Si         | No                   | Direccion      | Registrado        | No Procede |                        | No    |


  @happy_path @regresion @version3.1.7 @resolucionHipotecario_test15
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolución del trámite de tipo PEDIDO con producto HIPOTECARIO para la tipología: CAMBIO DE FORMA DE ENVÍO DE CORRESPONDENCIA cuando PROCEDE o NO PROCEDE
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto HIPOTECARIO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia CAMBIO DE FORMA DE ENVÍO DE CORRESPONDENCIA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | carta   |
      | <motivos> | <carta> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | 1-2-3   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE02   | 2-3-4   | Si         | No                   | Direccion      | Registrado        | No Procede |                        | Si    |
      | CASE03   | 1       | Si         | No                   | Direccion      | Registrado        | Procede    |                        | No    |
      | CASE04   | 2       | Si         | No                   | Direccion      | Registrado        | No Procede |                        | No    |


  @happy_path @regresion @version3.1.7 @resolucionHipotecario_test16
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolución del trámite de tipo PEDIDO con producto HIPOTECARIO para la tipología: CUOTA FLEXIBLE cuando PROCEDE o NO PROCEDE
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto HIPOTECARIO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia CUOTA FLEXIBLE
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | carta   |
      | <motivos> | <carta> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples: #*ADJUNTAR DOCUMENTOS OBLIGATORIOS - EN TAL CASO GIRU NO DEBERIA PERMITIR REGISTRO
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | 1       | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE02   | 1       | Si         | Si                   | Direccion      | Registrado        | No Procede |                        | Si    |
      | CASE03   | 1       | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | No    |
      | CASE04   | 1       | Si         | Si                   | Direccion      | Registrado        | No Procede |                        | No    |


  @happy_path @regresion @version3.1.7 @resolucionHipotecario_test17
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolución del trámite de tipo PEDIDO con producto HIPOTECARIO para la tipología: ENDOSO SEGUROS DE DESGRAVAMEN cuando PROCEDE o NO PROCEDE
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto HIPOTECARIO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia ENDOSO SEGUROS DE DESGRAVAMEN
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | carta   |
      | <motivos> | <carta> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | 1       | Si         | Si                   | Correo         | Registrado        | Procede    |                        | Si    |
      | CASE02   | 1       | Si         | No                   | Correo         | Registrado        | No Procede |                        | Si    |
      | CASE03   | 1       | Si         | No                   | Correo         | Registrado        | Procede    |                        | No    |
      | CASE04   | 1       | Si         | No                   | Correo         | Registrado        | No Procede |                        | No    |



  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- RESOLUCIÓN RECLAMO-----------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.7 @resolucionHipotecario_test18
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolución del trámite de tipo RECLAMO con producto HIPOTECARIO para la tipología: REPORTE INDEBIDO CENTRALES DE RIESGO cuando PROCEDE o NO PROCEDE
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto HIPOTECARIO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia REPORTE INDEBIDO CENTRALES DE RIESGO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | canal   | carta   |
      | <motivos> | <canal> | <carta> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | TESTCASE | motivos | canal          | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | 1       | App IBK        | Si         | Si                   | Correo         | Registrado        | Procede    |                        | Si    |
      | CASE02   | 1       | Bca Telef      | Si         | No                   | Correo         | Registrado        | No Procede |                        | Si    |
      | CASE03   | 1       | Bca x internet | Si         | No                   | Correo         | Registrado        | Procede    |                        | No    |
      | CASE04   | 1       | Canal Select   | Si         | No                   | Correo         | Registrado        | No Procede |                        | No    |


  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- RESOLUCIÓN SOLICITUD---------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.7 @resolucionHipotecario_test19
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolución del trámite de tipo SOLICITUD con producto HIPOTECARIO para la tipología: REPROGRAMACIÓN DE DEUDA cuando PROCEDE o NO PROCEDE
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto HIPOTECARIO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia REPROGRAMACIÓN DE DEUDA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | carta   |
      | <motivos> | <carta> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | 1       | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE02   | 1       | Si         | No                   | Direccion      | Registrado        | No Procede |                        | Si    |
      | CASE03   | 1       | Si         | No                   | Direccion      | Registrado        | Procede    |                        | No    |
      | CASE04   | 1       | Si         | No                   | Direccion      | Registrado        | No Procede |                        | No    |

  @happy_path @regresion @version3.1.8 @resolucionHipotecario_test20
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolución del trámite de tipo PEDIDO con producto HIPOTECARIO para la tipología: MEJORA DE CONDICIONES CHIP- RET cuando PROCEDE o NO PROCEDE
    Given que el usuario SUPERVISOR accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto HIPOTECARIO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia MEJORA DE CONDICIONES CHIP- RET
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | carta   |
      | <motivos> | <carta> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | 1-2-3   | Si         | Si                   | Correo         | Registrado        | Procede    |                        | Si    |
      | CASE02   | 6-5-4   | Si         | No                   | Direccion      | Registrado        | No Procede |                        | Si    |
      | CASE03   | 7       | Si         | No                   | Correo         | Registrado        | Procede    |                        | No    |
      | CASE04   | 1       | Si         | No                   | Direccion      | Registrado        | No Procede |                        | No    |


  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- RESOLUCIÓN SOLICITUD---------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.8 @resolucionHipotecario_test21
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolución del trámite de tipo SOLICITUD con producto HIPOTECARIO para la tipología: TRÁMITES DOCUMENTARIOS POST VENTA cuando PROCEDE o NO PROCEDE
    Given que el usuario SUPERVISOR accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto HIPOTECARIO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia TRÁMITES DOCUMENTARIOS POST VENTA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | carta   |
      | <motivos> | <carta> |
    And se ingresa una respuesta al cliente "No" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | resolucion | areaValidacionOpcional | carta |
      | CASE01   | 1-2-3   | Si         | Si                   | Procede    |                        | No    |
      | CASE02   | 2-5-4   | Si         | No                   | No Procede |                        | No    |
      | CASE03   | 1       | Si         | No                   | Procede    |                        | No    |
      | CASE04   | 2       | Si         | No                   | No Procede |                        | No    |


  @happy_path @regresion @version3.1.8 @resolucionHipotecario_test22
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolución del trámite de tipo SOLICITUD con producto HIPOTECARIO para la tipología: SOLICITUD TRAMITACIÓN DE DOCUMENTOS cuando PROCEDE o NO PROCEDE
    Given que el usuario SUPERVISOR accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto HIPOTECARIO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia SOLICITUD TRAMITACIÓN DE DOCUMENTOS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | carta   |
      | <motivos> | <carta> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | 1       | Si         | Si                   | Correo         | Registrado        | Procede    |                        | Si    |
      | CASE02   | 1       | Si         | No                   | Direccion      | Registrado        | No Procede |                        | Si    |
      | CASE03   | 1       | Si         | No                   | Correo         | Registrado        | Procede    |                        | No    |
      | CASE04   | 1       | Si         | No                   | Direccion      | Registrado        | No Procede |                        | No    |


  @happy_path @regresion @version3.1.9 @resolucionHipotecario_test23
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolución del trámite de tipo RECLAMO con producto HIPOTECARIO para la tipología: CONSUMO/ RETIRO NO EFECTUADO cuando PROCEDE o NO PROCEDE
    Given que el usuario SUPERVISOR accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto HIPOTECARIO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia CONSUMO/ RETIRO NO EFECTUADO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | canal   | carta   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <canal> | <carta> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | canal          | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | 1       | 3               | PEN    | 100   | App IBK        | Si         | Si                   | Correo         | Registrado        | Procede    |                        | Si    |
      | CASE02   | 1       | 1               | PEN    | 100   | Bca Telef      | Si         | No                   | Direccion      | Registrado        | No Procede |                        | Si    |
      | CASE03   | 1       | 1               | PEN    | 100   | Bca x internet | Si         | No                   | Correo         | Registrado        | Procede    |                        | No    |
      | CASE04   | 1       | 1               | PEN    | 100   | Canal Select   | Si         | No                   | Direccion      | Registrado        | No Procede |                        | No    |


  @happy_path @regresion @version3.1.10 @resolucionHipotecario_test24
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolución del trámite de tipo PEDIDO con producto HIPOTECARIO para la tipología: LEVANTAMIENTO DE GARANTÍA cuando PROCEDE o NO PROCEDE
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto HIPOTECARIO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia LEVANTAMIENTO DE GARANTÍA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | carta   |
      | <motivos> | <carta> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples: #REGISTRA MOTIVO INDIVIDUAL - *ADJUNTAR DOCUMENTOS OBLIGATORIOS - TIPOLOGIA CON ARPI
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | 1       | Si         | Si                   | Correo         | Registrado        | Procede    |                        | Si    |
      | CASE02   | 2       | Si         | Si                   | Direccion      | Registrado        | No Procede |                        | Si    |
      | CASE03   | 3       | Si         | Si                   | Correo         | Registrado        | Procede    |                        | No    |
      | CASE04   | 1       | Si         | Si                   | Direccion      | Registrado        | No Procede |                        | No    |


  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- RESOLUCIÓN SOLICITUD --------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.10 @resolucionHipotecario_test25
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolución del trámite de tipo SOLICITUD con producto HIPOTECARIO para la tipología: EMISIÓN PROACTIVA DE LEVANTAMIENTO DE GARANTÍA cuando PROCEDE o NO PROCEDE
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto HIPOTECARIO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia EMISIÓN PROACTIVA DE LEVANTAMIENTO DE GARANTÍA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | carta   |
      | <motivos> | <carta> |
    And se ingresa una respuesta al cliente "No" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples: #* TIPOLOGIA CON ARPI
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | resolucion | areaValidacionOpcional | carta |
      | CASE01   | 1       | Si         | Si                   | Procede    |                        | No    |
      | CASE02   | 1       | Si         | No                   | No Procede |                        | No    |
      | CASE03   | 1       | Si         | No                   | Procede    |                        | No    |
      | CASE04   | 1       | Si         | No                   | No Procede |                        | No    |

