@all_generacion_tramite_cartaFianzaCertificado
Feature: Registro del trámite Producto de CARTA FIANZA/CERTIFICADO

  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO SOLICITUD-----------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.7 @registroCartaFianzaCertificado_test1
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo SOLICITUD con producto CARTA FIANZA/CERTIFICADO para la tipología: LIBERACIÓN CARTA FIANZA Y CERTIFICADO BANCARIO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto el CARTA FIANZA/CERTIFICADO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia LIBERACIÓN CARTA FIANZA Y CERTIFICADO BANCARIO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "No" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos |comentario | indicaArchivoAdjunto |estadoProd |
      | CASE01   | 1       |Si         | Si                   |ACTIVA     |

    # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO PEDIDO-----------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.8 @registroCartaFianzaCertificado_test2
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto CARTA FIANZA/CERTIFICADO para la tipología: DUPLICADO DE DOCUMENTO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto el CARTA FIANZA/CERTIFICADO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia DUPLICADO DE DOCUMENTO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | tipoDocumentoTramite   |
      | <motivos> | <tipoDocumentoTramite> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | tipoDocumentoTramite    | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | Contrato                | Si         | Si                   | Direccion      | Registrado        | ACTIVA     |
      | CASE02   | 1       | Constancia de Operación | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE03   | 1       | Constancia de Pago      | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE04   | 1       | Constancia de Retiro    | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE05   | 1       | Letra                   | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE06   | 1       | Pagaré                  | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE07   | 1       | Voucher                 | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE08   | 1       | Estado de cuenta        | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE09   | 1       | Otros                   | Si         | No                   | Direccion      | Registrado        | ACTIVA     |

