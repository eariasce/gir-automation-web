@registroautonomias_tc
@all_generacion_tramite_tarjetaCredito
Feature: Registro del Trámite de Producto TARJETA DE CREDITO

  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO PEDIDO -----------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.0.9 @registroTarjetaCredito_autonomia_test1
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto TARJETA DE CREDITO para la tipología: EXONERACIÓN DE COBROS
    Given que el usuario <ROL> accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM y el producto TARJETA DE CREDITO validar autonomia y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia EXONERACIÓN DE COBROS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> |
    #And si aplica comision de membresía, se selecciona la Razon Operativa "<RazonOperativa>" y el Tipo de Razon Operativa "<TipoRazonOperativa>"
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    #step valida autonomia en mensaje de registro
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
     #step valida textos de autonomia en historial
    Then se valida en historial el reclamo registrado y seguimiento

    Examples: #DNI del cliente debe pertenecer al segmento Joven/Oro (validar por heidisql)
      | TESTCASE | ROL                                 | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd   | RazonOperativa | TipoRazonOperativa |
      | CASE01   | ASESOR                              | 2       | 1               | PEN    | 150   | Si         | Si                   | Correo         | Registrado        | AUTONOMIA_TC | Si             | RO001              |
      | CASE02   | ASESOR DE RETENCIÓN                 | 3       | 1               | PEN    | 150   | Si         | Si                   | Correo         | Registrado        | AUTONOMIA_TC | Si             | RO001              |
      | CASE03   | SUPERVISOR RETENCIÓN                | 4       | 1               | PEN    | 50    | Si         | Si                   | Correo         | Registrado        | AUTONOMIA_TC | Si             | RO001              |
      | CASE04   | GERENTE DE TIENDA/GERENTE ASISTENTE | 5       | 1               | PEN    | 150   | Si         | Si                   | Correo         | Registrado        | AUTONOMIA_TC | Si             | RO001              |
      | CASE05   | SUPERVISOR CC                       | 6       | 1               | PEN    | 150   | Si         | Si                   | Correo         | Registrado        | AUTONOMIA_TC | Si             | RO001              |
      | CASE06   | REPRESENTANTE FINANCIERO            | 7       | 1               | PEN    | 150   | Si         | Si                   | Correo         | Registrado        | AUTONOMIA_TC | Si             | RO001              |
      | CASE07   | ASESOR                              | 8       | 1               | PEN    | 150   | Si         | Si                   | Correo         | Registrado        | AUTONOMIA_TC | Si             | RO001              |


  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO RECLAMO ----------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.0.9 @registroTarjetaCredito_autonomia_test2
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto TARJETA DE CREDITO para la tipología: COBROS INDEBIDOS
    Given que el usuario <ROL> accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM y el producto TARJETA DE CREDITO validar autonomia y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia COBROS INDEBIDOS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> |
    #And si aplica comision de membresía, se selecciona la Razon Operativa "<RazonOperativa>" y el Tipo de Razon Operativa "<TipoRazonOperativa>"
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    #step valida autonomia en mensaje de registro
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
     #step valida textos de autonomia en historial
    Then se valida en historial el reclamo registrado y seguimiento

    Examples: #DNI del cliente debe pertenecer al segmento Premium/Plata (validar por heidisql)
      | TESTCASE | ROL           | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd   | RazonOperativa | TipoRazonOperativa |
      | CASE01   | SUPERVISOR CC | 3       | 1               | PEN    | 150   | Si         | Si                   | Correo         | Registrado        | AUTONOMIA_TC | Si             | RO001              |
      | CASE02   | SUPERVISOR CC | 2       | 1               | PEN    | 150   | Si         | Si                   | Correo         | Registrado        | AUTONOMIA_TC | Si             | RO001              |
      | CASE03   | SUPERVISOR CC | 5       | 1               | PEN    | 150   | Si         | Si                   | Correo         | Registrado        | AUTONOMIA_TC | Si             | RO001              |
      | CASE04   | SUPERVISOR CC | 6       | 1               | PEN    | 150   | Si         | Si                   | Correo         | Registrado        | AUTONOMIA_TC | Si             | RO001              |
      | CASE05   | SUPERVISOR CC | 7       | 1               | PEN    | 150   | Si         | Si                   | Correo         | Registrado        | AUTONOMIA_TC | Si             | RO001              |
      | CASE06   | SUPERVISOR CC | 4       | 1               | PEN    | 150   | Si         | Si                   | Correo         | Registrado        | AUTONOMIA_TC | Si             | RO001              |


  @happy_path @regresion @version3.0.9 @registroTarjetaCredito_autonomia_test3
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto TARJETA DE CREDITO para la tipología: CONSUMOS NO RECONOCIDOS POR FRAUDE
    Given que el usuario <ROL> accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM y el producto TARJETA DE CREDITO validar autonomia y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia CONSUMOS NO RECONOCIDOS POR FRAUDE
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> |
    And selecciono lo sucedido "<MotivoSucedidoTarjeta>" con la tarjeta, si se ha bloqueado "<flagTarjetaBloqueo>"
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    #step valida autonomia en mensaje de registro
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
     #step valida textos de autonomia en historial
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:  #DNI del cliente debe pertenecer al segmento Preferente/Bronce (validar por heidisql)
      | TESTCASE | ROL                                 | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd   | MotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | CASE01   | ASESOR                              | 1       | 1               | PEN    | 150   | Si         | Si                   | Correo         | Registrado        | AUTONOMIA_TC | CFS001                | Si                 |
      | CASE02   | ASESOR DE RETENCIÓN                 | 2       | 1               | PEN    | 100   | Si         | Si                   | Correo         | Registrado        | AUTONOMIA_TC | CFS001                | Si                 |
      | CASE03   | ASESOR DE RETENCIÓN                 | 1       | 1               | PEN    | 100   | Si         | Si                   | Correo         | Registrado        | AUTONOMIA_TC | CFS001                | Si                 |
      | CASE04   | GERENTE DE TIENDA/GERENTE ASISTENTE | 2       | 1               | PEN    | 100   | Si         | Si                   | Correo         | Registrado        | AUTONOMIA_TC | CFS001                | Si                 |
      | CASE05   | SUPERVISOR CC                       | 1       | 1               | PEN    | 100   | Si         | Si                   | Correo         | Registrado        | AUTONOMIA_TC | CFS001                | Si                 |
      | CASE06   | REPRESENTANTE FINANCIERO            | 2       | 1               | PEN    | 100   | Si         | Si                   | Correo         | Registrado        | AUTONOMIA_TC | CFS001                | Si                 |


  @happy_path @regresion @version3.0.9 @registroTarjetaCredito_autonomia_test4
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto TARJETA DE CREDITO para la tipología: CONSUMOS MAL PROCESADOS POR EL COMERCIO/MARCA
    Given que el usuario <ROL> accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM y el producto TARJETA DE CREDITO validar autonomia y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia CONSUMOS MAL PROCESADOS POR EL COMERCIO/MARCA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | productoServicioAdquirido   | tipoServicio   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <productoServicioAdquirido> | <tipoServicio> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    #step valida autonomia en mensaje de registro
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
     #step valida textos de autonomia en historial
    Then se valida en historial el reclamo registrado y seguimiento

    Examples: #ADJUNTAR ARCHIVO - OBLIGATORIO - #DNI del cliente debe pertenecer al segmento Preferente/Platino (validar por heidisql)
      | TESTCASE | ROL    | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd   | productoServicioAdquirido | tipoServicio |
      | CASE01   | ASESOR | 1       | 1               | PEN    | 50    | Si         | Si                   | Correo         | Registrado        | AUTONOMIA_TC |                           |              |
      | CASE02   | ASESOR | 2       | 1               | PEN    | 50    | Si         | Si                   | Correo         | Registrado        | AUTONOMIA_TC | Producto                  |              |
      | CASE03   | ASESOR | 3       | 1               | PEN    | 50    | Si         | Si                   | Correo         | Registrado        | AUTONOMIA_TC |                           |              |
      | CASE04   | ASESOR | 1       | 1               | PEN    | 50    | Si         | Si                   | Correo         | Registrado        | AUTONOMIA_TC |                           |              |
      | CASE05   | ASESOR | 5       | 1               | PEN    | 50    | Si         | Si                   | Correo         | Registrado        | AUTONOMIA_TC |                           |              |
      | CASE06   | ASESOR | 6       | 1               | PEN    | 50    | Si         | Si                   | Correo         | Registrado        | AUTONOMIA_TC |                           |              |

