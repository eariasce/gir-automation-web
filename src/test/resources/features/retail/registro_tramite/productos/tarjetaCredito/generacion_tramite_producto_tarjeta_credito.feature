@registro5
@all_generacion_tramite_tarjetaCredito
Feature: Registro del Trámite de Producto TARJETA DE CREDITO

  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO PEDIDO -----------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.0.9 @registroTarjetaCredito_test1
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto TARJETA DE CREDITO para la tipología: EXONERACIÓN DE COBROS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia EXONERACIÓN DE COBROS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> |
    #And si aplica comision de membresía, se selecciona la Razon Operativa "<RazonOperativa>" y el Tipo de Razon Operativa "<TipoRazonOperativa>"
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples: #NOTA: MOTIVO ES 1:  SOLO PARA: COMISIÓN DE MEMBRESÍA - ¿Aplica razón operativa? MONTO MENOR 500 - CANTIDAD MOVIMIENTO INGRESA: 3
      | TESTCASE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd | RazonOperativa | TipoRazonOperativa |
      | CASE01   | 1       | 3               | PEN    | 100   | Si         | Si                   | Correo         | Registrado        | ACTIVA     | Si             | RO001              |
      | CASE02   | 2-3-4   | 3               | PEN    | 150   | Si         | No                   | Correo         | Registrado        | ACTIVA     | No             | Ninguno            |
      | CASE03   | 5-6-7   | 3               | PEN    | 300   | Si         | No                   | Direccion      | Registrado        | ACTIVA     | No             | Ninguno            |
      | CASE04   | 8       | 3               | PEN    | 450   | Si         | No                   | Direccion      | Registrado        | ACTIVA     | No             | Ninguno            |


  @happy_path @regresion @version3.1.1 @registroTarjetaCredito_test5
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto TARJETA DE CREDITO para la tipología: MODIFICACION PLAZO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia MODIFICACION PLAZO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1-2-3   | 5               | PEN    | 150   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.1 @registroTarjetaCredito_test6
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto TARJETA DE CREDITO para la tipología: PROGRAMA DE RECOMPENSAS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia PROGRAMA DE RECOMPENSAS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | tipoPrograma   | cantidad   | importe   |
      | <motivos> | <tipoPrograma> | <cantidad> | <importe> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | tipoPrograma | cantidad | importe | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | Efectivo     | .        | 100     | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 2       | Millas       | 150      | .       | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE03   | 3-4-1   | Vales        | 200      | 500     | Si         | No                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.1 @registroTarjetaCredito_test7
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto TARJETA DE CREDITO para la tipología: PAGO ANTICIPADO Y ADELANTO DE CUOTAS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia PAGO ANTICIPADO Y ADELANTO DE CUOTAS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | aplicaAbono   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <aplicaAbono> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples: Para este caso el cliente debe tener cuenta activa o td - Abono en cuenta
      | TESTCASE | motivos | cantMovimientos | moneda | monto | aplicaAbono        | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | 5               | PEN    | 100   | No                 | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 2       | 5               | PEN    | 200   | Abono en cuenta    | Si         | No                   | Correo         | Registrado        | ACTIVA2    |
      | CASE03   | 3       | 5               | PEN    | 850   | Orden de pago      | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE04   | 1-3-4   | 5               | PEN    | 900   | Tarjeta de crédito | Si         | No                   | Direccion      | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.1 @registroTarjetaCredito_test8
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto TARJETA DE CREDITO para la tipología: DEVOLUCION DE SALDO ACREEDOR
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia DEVOLUCION DE SALDO ACREEDOR
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | aplicaAbono   | origenSaldo   |
      | <motivos> | <aplicaAbono> | <origenSaldo> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples: #REGISTRA MOTIVO - INDIVIDUAL - #* TIPOLOGIA CON ARPI - PARA ESTE CASO EL CLIENTE DEBE TENER CUENTA ACTIVA O TD - ABONO EN CUENTA
      | TESTCASE | motivos | aplicaAbono        | origenSaldo                 | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | Tarjeta de Crédito | Transferencia Interbancaria | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 2       | Orden de Pago      | Pago canales                | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE03   | 3       | Orden de Pago      | Devolución Comisión         | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE04   | 4       | Abono en Cuenta    | Devolución Establecimiento  | Si         | No                   | Direccion      | Registrado        | ACTIVA2    |


  @happy_path @regresion @version3.1.1 @registroTarjetaCredito_test9
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto TARJETA DE CREDITO para la tipología: DUPLICADO DE ESTADO DE CUENTA/SERVICIO DE COPIAS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia DUPLICADO DE ESTADO DE CUENTA/SERVICIO DE COPIAS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | Tienda   | Autonomia   |
      | <motivos> | <Tienda> | <Autonomia> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples: #REQUIERE SI O SI ARCHIVO VOUCHER ADJUNTO!
      | TESTCASE | motivos | Tienda | Autonomia | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | No     | Si        | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 2       | Si     | No        | Si         | Si                   | Direccion      | Registrado        | ACTIVA     |



  @happy_path @regresion @version3.1.3 @registroTarjetaCredito_test15
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto TARJETA DE CREDITO para la tipología: DUPLICADO DE VOUCHER
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia DUPLICADO DE VOUCHER
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | 5               | PEN    | 150   | Si         | Si                   | Correo         | Registrado        | ACTIVA     |

  @happy_path @regresion @version3.1.3 @registroTarjetaCredito_test16
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto TARJETA DE CREDITO para la tipología: BAJA DE TASA
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia BAJA DE TASA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples: #ADJUNTAR ARCHIVO - OBLIGATORIO
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.3 @registroTarjetaCredito_test17
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto TARJETA DE CREDITO para la tipología: MODIFICACIÓN DE GRUPO DE LIQUIDACIÓN
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia MODIFICACIÓN DE GRUPO DE LIQUIDACIÓN
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | GrupoLiquidacionActual   | GrupoLiquidacionSolicitado   |
      | <motivos> | <GrupoLiquidacionActual> | <GrupoLiquidacionSolicitado> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | Pag. 1 Fact 7          | Pag. 5 Fact 11             | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 1       | Pag. 1 Fact 18         | Pag. 5 Fact 11             | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE03   | 1       | Pag. 2 Fact 8          | Pag. 5 Fact 11             | Si         | Si                   | Direccion      | Registrado        | ACTIVA     |
      | CASE04   | 1       | Pag. 3 Fact 9          | Pag. 5 Fact 11             | Si         | Si                   | Direccion      | Registrado        | ACTIVA     |
      | CASE05   | 1       | Pag. 5 Fact 11         | Pag. 5 Fact 11             | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE06   | 1       | Pag. 5 Fact 19         | Pag. 5 Fact 11             | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE07   | 1       | Pag. 5 Fact 21         | Pag. 18 Fact 24            | Si         | Si                   | Direccion      | Registrado        | ACTIVA     |
      | CASE08   | 1       | Pag. 7 Fact 13         | Pag. 18 Fact 24            | Si         | Si                   | Direccion      | Registrado        | ACTIVA     |
      | CASE09   | 1       | Pag. 10 Fact 26        | Pag. 18 Fact 24            | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE10   | 1       | Pag. 12 Fact 18        | Pag. 18 Fact 24            | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE11   | 1       | Pag. 13 Fact 18        | Pag. 18 Fact 24            | Si         | Si                   | Direccion      | Registrado        | ACTIVA     |
      | CASE12   | 1       | Pag. 14 Fact 20        | Pag. 18 Fact 24            | Si         | Si                   | Direccion      | Registrado        | ACTIVA     |
      | CASE13   | 1       | Pag. 15 Fact 2         | Pag. 18 Fact 24            | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE14   | 1       | Pag. 15 Fact 21        | Pag. 18 Fact 24            | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE15   | 1       | Pag. 16 Fact 22        | Pag. 21 Fact 27            | Si         | Si                   | Direccion      | Registrado        | ACTIVA     |
      | CASE16   | 1       | Pag. 17 Fact 2         | Pag. 21 Fact 27            | Si         | Si                   | Direccion      | Registrado        | ACTIVA     |
      | CASE17   | 1       | Pag. 17 Fact 23        | Pag. 21 Fact 27            | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE18   | 1       | Pag. 20 Fact 6         | Pag. 21 Fact 27            | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE19   | 1       | Pag. 20 Fact 26        | Pag. 21 Fact 27            | Si         | Si                   | Direccion      | Registrado        | ACTIVA     |
      | CASE20   | 1       | Pag. 25 Fact 11        | Pag. 21 Fact 27            | Si         | Si                   | Direccion      | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.3 @registroTarjetaCredito_test18
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto TARJETA DE CREDITO para la tipología: MODIFICACIÓN LÍNEA TC ADICIONAL
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia MODIFICACIÓN LÍNEA TC ADICIONAL
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | moneda   | monto   |
      | <motivos> | <moneda> | <monto> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    #revisar que este este este encapsulado en otro step
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | PEN    | 150   | Si         | Si                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.3 @registroTarjetaCredito_test19
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto TARJETA DE CREDITO para la tipología: REDUCCIÓN DE LÍNEA TC
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia REDUCCIÓN DE LÍNEA TC
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | MonedaTramite   | LineaActual   | LineaNueva   |
      | <motivos> | <MonedaTramite> | <LineaActual> | <LineaNueva> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | MonedaTramite | LineaActual | LineaNueva | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | PEN           | 2000        | 1000       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.3 @registroTarjetaCredito_test20
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto TARJETA DE CREDITO para la tipología: TRASLADO DE SALDO ACREEDOR
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia TRASLADO DE SALDO ACREEDOR
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | MonedaTramite   | ImporteSolicitado   |
      | <motivos> | <MonedaTramite> | <ImporteSolicitado> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | MonedaTramite | ImporteSolicitado | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1-2     | PEN           | 200               | Si         | Si                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.3 @registroTarjetaCredito_test21
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto TARJETA DE CREDITO para la tipología: AFILIACIÓN / DESAFILIACIÓN DÉBITO AUTOMÁTICO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia AFILIACIÓN / DESAFILIACIÓN DÉBITO AUTOMÁTICO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | NombreMotivo   |
      | <motivos> | <NombreMotivo> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples: #CLIENTE DEBE TENER CUENTAS!!!! - NOTA: SOLO PERMITE SELECCIONAR UNO POR UNO MOTIVO
      | TESTCASE | motivos | NombreMotivo                    | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | Afiliación pago mínimo          | Si         | Si                   | Correo         | Registrado        | ACTIVA2    |
      | CASE02   | 2       | Afiliación pago mes             | Si         | No                   | Correo         | Registrado        | ACTIVA2    |
      | CASE03   | 3       | Modificación cuenta / modalidad | Si         | No                   | Direccion      | Registrado        | ACTIVA2    |
      | CASE04   | 4       | Desafiliación                   | Si         | No                   | Direccion      | Registrado        | ACTIVA2    |


  @happy_path @regresion @version3.1.3 @registroTarjetaCredito_test22
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto TARJETA DE CREDITO para la tipología: DUPLICADO DE DOCUMENTO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia DUPLICADO DE DOCUMENTO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | tipoDocumentoTramite   |
      | <motivos> | <tipoDocumentoTramite> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples: #REGISTRA MOTIVO INDIVIDUAL - #* TIPOLOGIA CON ARPI
      | TESTCASE | motivos | tipoDocumentoTramite    | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | Contrato                | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 2       | Constancia de Operación | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE03   | 3       | Constancia de Pago      | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE04   | 4       | Constancia de Retiro    | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE05   | 5       | Letra                   | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE06   | 1       | Pagaré                  | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE07   | 2       | Voucher                 | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE08   | 3       | Estado de cuenta        | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE09   | 4       | Otros                   | Si         | No                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.3 @registroTarjetaCredito_test23
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto TARJETA DE CREDITO para la tipología: REGRABACIÓN DE PLÁSTICO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia REGRABACIÓN DE PLÁSTICO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1-2     | No         | Si                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.7 @registroTarjetaCredito_test39
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto TARJETA DE CREDITO para la tipología: CORRECCION DE PAGO REALIZADO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia CORRECCIÓN DE PAGO REALIZADO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | canal   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1-2-3   | 3               | PEN    | 100   | App IBK            | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 2-3-4   | 3               | PEN    | 100   | Bca Telef          | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE03   | 1       | 1               | PEN    | 100   | Bca x internet     | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE04   | 1       | 1               | PEN    | 100   | Canal Select       | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE05   | 1       | 1               | PEN    | 100   | Entrega TC         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE06   | 1       | 1               | PEN    | 100   | IBK Agentes        | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE07   | 1       | 1               | PEN    | 100   | Interbank x FB     | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE08   | 1       | 1               | PEN    | 100   | Módulo de cobranza | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE09   | 1       | 1               | PEN    | 100   | Otros              | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE10   | 1       | 1               | PEN    | 100   | Red CjGnet         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE11   | 1       | 1               | PEN    | 100   | Red Tiendas        | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE12   | 1       | 1               | PEN    | 100   | Televentas         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE13   | 1       | 1               | PEN    | 100   | Whatsapp           | Si         | No                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.7 @registroTarjetaCredito_test40
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto TARJETA DE CREDITO para la tipología: ENDOSO SEGUROS DE DESGRAVAMEN
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia ENDOSO SEGUROS DE DESGRAVAMEN
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | Si         | Si                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.7 @registroTarjetaCredito_test41
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto TARJETA DE CREDITO para la tipología: MODIFICACIÓN DISPOSICIÓN DE EFECTIVO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia MODIFICACIÓN DISPOSICIÓN DE EFECTIVO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | agregarTarjetaCredito   |
      | <motivos> | <agregarTarjetaCredito> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    #PARA ESTE CASO SOLO SE PUEDE AGREGAR DOS TARJETA DE CREDITO YA QUE ESTE CLIENTE 40402131 TIENE SOLO DOS TARJETA DE CREDITO QUE AGREGAR
    #SOLO PERMITE 1 MOTIVO PARA REGISTRAR
    Examples:
      | TESCASE | motivos | agregarTarjetaCredito | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01  | 1       | 2                     | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02  | 2       | 2                     | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE03  | 3       | 2                     | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE04  | 4       | 2                     | Si         | Si                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.7 @registroTarjetaCredito_test42
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto TARJETA DE CREDITO para la tipología: REVERSIÓN DE UPGRADE
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia REVERSIÓN DE UPGRADE
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | Si         | Si                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.8 @registroTarjetaCredito_test48
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto TARJETA DE CREDITO para la tipología: INFORMACIÓN DE ESTABLECIMIENTO/ RET.EFECTIVO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia INFORMACIÓN DE ESTABLECIMIENTO/ RET.EFECTIVO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | 3               | PEN    | 100   | Si         | Si                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.8 @registroTarjetaCredito_test49
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto TARJETA DE CREDITO para la tipología: INFORMACION DE MOVIMIENTOS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia INFORMACION DE MOVIMIENTOS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | 3               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.8 @registroTarjetaCredito_test50
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto TARJETA DE CREDITO para la tipología: CAMBIO DE FORMA DE ENVÍO DE CORRESPONDENCIA
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia CAMBIO DE FORMA DE ENVÍO DE CORRESPONDENCIA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | tipoEnvio   |
      | <motivos> | <tipoEnvio> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | tipoEnvio               | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | Envío físico            | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 1       | Envío por mail          | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE03   | 1       | Envío físico y por mail | Si         | No                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.4.1 @registroTarjetaCredito_test30
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto TARJETA DE CREDITO para la tipología: UNIFICACIÓN DE MARCAS/TIPOS DE TARJETAS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia UNIFICACIÓN DE MARCAS/TIPOS DE TARJETAS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | Si         | Si                   | Correo         | Registrado        | ACTIVA     |



  #NOTA: TIPOLOGIA - CONSTANCIA DE BLOQUEO DE TARJETA: SE REGISTRA CON RESOLUCIÓN AUTOMATICA PROCEDE, POR ENDE NO TIENE ESCENARIO RESOLUCIÓN
  @happy_path @regresion @version3.1.4.1 @registroTarjetaCredito_test31
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto TARJETA DE CREDITO para la tipología: CONSTANCIA DE BLOQUEO DE TARJETA
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia CONSTANCIA DE BLOQUEO DE TARJETA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | canal   |
      | <motivos> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | App IBK            | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 1       | Bca Telef          | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE03   | 1       | Bca x internet     | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE04   | 1       | Canal Select       | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE05   | 1       | Entrega TC         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE06   | 1       | IBK Agentes        | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE07   | 1       | Interbank x FB     | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE08   | 1       | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE09   | 1       | Otros              | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE10   | 1       | Red CjGnet         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE11   | 1       | Red Tiendas        | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE12   | 1       | Televentas         | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE13   | 1       | Whatsapp           | Si         | No                   | Correo         | Registrado        | ACTIVA     |




  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO RECLAMO ----------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.0.9 @registroTarjetaCredito_test2
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto TARJETA DE CREDITO para la tipología: COBROS INDEBIDOS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia COBROS INDEBIDOS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> |
   # And si aplica comision de membresía, se selecciona la Razon Operativa "<RazonOperativa>" y el Tipo de Razon Operativa "<TipoRazonOperativa>"
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples: #NOTA: MOTIVO ES 1:  SOLO PARA: COMISIÓN DE MEMBRESÍA - ¿Aplica razón operativa? MONTO MENOR 500 - CANTIDAD MOVIMIENTO INGRESA: 3
      | TESTCASE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd | RazonOperativa | TipoRazonOperativa |
      | CASE01   | 1       | 3               | PEN    | 100   | Si         | Si                   | Correo         | Registrado        | ACTIVA | Si             | RO001              |
      | CASE02   | 2-3-4   | 3               | PEN    | 150   | Si         | No                   | Correo         | Registrado        | ACTIVA | No             | Ninguno            |
      | CASE03   | 5-6-7   | 3               | PEN    | 300   | Si         | No                   | Direccion      | Registrado        | ACTIVA | No             | Ninguno            |
      | CASE04   | 8       | 3               | PEN    | 450   | Si         | No                   | Direccion      | Registrado        | ACTIVA | No             | Ninguno            |


  @happy_path @regresion @version3.0.9 @registroTarjetaCredito_test3
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto TARJETA DE CREDITO para la tipología: CONSUMOS NO RECONOCIDOS POR FRAUDE
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia CONSUMOS NO RECONOCIDOS POR FRAUDE
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   |getMotivoSucedidoTarjeta|flagTarjetaBloqueo|
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> |<MotivoSucedidoTarjeta> |<flagTarjetaBloqueo>|
    #And selecciono lo sucedido "<MotivoSucedidoTarjeta>" con la tarjeta, si se ha bloqueado "<flagTarjetaBloqueo>"
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd | MotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | CASE01   | 1       | 20              | PEN    | 150   | Si         | Si                   | Correo         | Registrado        | ACTIVA     | CFS001                | Si                 |
      | CASE02   | 2       | 20              | PEN    | 250   | Si         | No                   | Direccion      | Registrado        | ACTIVA     | CFS002                | No                 |


  @happy_path @regresion @version3.0.9 @registroTarjetaCredito_test4
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto TARJETA DE CREDITO para la tipología: CONSUMOS MAL PROCESADOS POR EL COMERCIO/MARCA
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia CONSUMOS MAL PROCESADOS POR EL COMERCIO/MARCA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | productoServicioAdquirido   | tipoServicio   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <productoServicioAdquirido> | <tipoServicio> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples: #ADJUNTAR ARCHIVO - OBLIGATORIO
       #OJO Motivo: 22 - SELECT - 2 = Se cargó consumo no concretado y desistió de la compra - Se intentó adquirir un: - indicaArchivoAdjunto: Si - Obligatorio - para auto asignar
      #NOTA: productoServicioAdquirido: Producto - No tiene tipo Servicio
      #NOTA: productoServicioAdquirido: Servicio - Si tiene tipo Servicio SON: Hotel - Tour - Vuelos - Otros

      | TESTCASE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd | productoServicioAdquirido | tipoServicio |
      | CASE01   | 1-3     | 5               | PEN    | 120   | Si         | Si                   | Correo         | Registrado        | ACTIVA     |                           |              |
      | CASE02   | 2       | 3               | PEN    | 150   | Si         | Si                   | Correo         | Registrado        | ACTIVA     | Producto                  |              |
      | CASE03   | 2       | 3               | PEN    | 200   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Servicio                  | Tour         |
      | CASE04   | 5-4-6   | 3               | PEN    | 250   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     |                           |              |


  @happy_path @regresion @version3.1.1 @registroTarjetaCredito_test10
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto TARJETA DE CREDITO para la tipología: SISTEMA DE RECOMPENSAS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia SISTEMA DE RECOMPENSAS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | tipoPrograma   | cantidad   | importe   |
      | <motivos> | <tipoPrograma> | <cantidad> | <importe> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | tipoPrograma        | cantidad | importe | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | Descuento en Cuotas | 10       | 100     | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 2-3     | Efectivo            | 20       | 200     | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE03   | 4-5     | Millas              | 30       | 50      | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE04   | 6-7     | Vales               | 40       | 150     | Si         | No                   | Direccion      | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.1 @registroTarjetaCredito_test11
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto TARJETA DE CREDITO para la tipología: INSUFICIENTE INFORMACIÓN
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia INSUFICIENTE INFORMACIÓN
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | Si         | Si                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.1 @registroTarjetaCredito_test12
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto TARJETA DE CREDITO para la tipología: TRANSACCIÓN MAL O NO PROCESADA
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia TRANSACCIÓN MAL O NO PROCESADA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | canal   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | 3               | PEN    | 100   | App IBK            | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 2       | 3               | PEN    | 200   | Bca Telef          | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE03   | 3       | 3               | PEN    | 300   | Bca x internet     | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE04   | 4       | 3               | PEN    | 400   | Canal Select       | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE05   | 5       | 3               | PEN    | 500   | Entrega TC         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE06   | 6       | 3               | PEN    | 600   | IBK Agentes        | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE07   | 1       | 3               | PEN    | 700   | Interbank x FB     | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE08   | 1-2     | 1               | PEN    | 800   | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE09   | 3-4     | 1               | PEN    | 900   | Otros              | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE010  | 5-6     | 1               | PEN    | 100   | Red CjGnet         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE011  | 1-2-3   | 1               | PEN    | 200   | Red Tiendas        | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE012  | 3-4-5   | 1               | PEN    | 300   | Televentas         | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE013  | 6-2-3   | 1               | PEN    | 400   | Whatsapp           | Si         | No                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.1 @registroTarjetaCredito_test13
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto TARJETA DE CREDITO para la tipología: INCORRECTA APLICACIÓN CD/EC/C. CUOTAS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia INCORRECTA APLICACIÓN CD/EC/C. CUOTAS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1-2-3   | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 3-4-1   | Si         | No                   | Direccion      | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.3 @registroTarjetaCredito_test24
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto TARJETA DE CREDITO para la tipología: NO RECEPCIÓN DE DOCUMENTO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia NO RECEPCIÓN DE DOCUMENTO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | tipoDocumentoTramite   | tipoEnvio   |
      | <motivos> | <tipoDocumentoTramite> | <tipoEnvio> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | tipoDocumentoTramite    | tipoEnvio               | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | Contrato                | Envío físico            | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 1       | Constancia de Operación | Envío por mail          | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE03   | 1       | Constancia de Pago      | Envío físico y por mail | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE04   | 1       | Constancia de Retiro    | Envío físico            | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE05   | 1       | Letra                   | Envío por mail          | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE06   | 1       | Pagaré                  | Envío físico y por mail | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE07   | 1       | Voucher                 | Envío físico            | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE08   | 1       | Estado de cuenta        | Envío por mail          | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE09   | 1       | Otros                   | Envío físico y por mail | Si         | No                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.3 @registroTarjetaCredito_test25
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto TARJETA DE CREDITO para la tipología: LIBERACION DE RETENCIONES
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia LIBERACION DE RETENCIONES
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | 10              | PEN    | 100   | Si         | Si                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.3 @registroTarjetaCredito_test26
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto TARJETA DE CREDITO para la tipología: OPERACIÓN DENEGADA
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia OPERACIÓN DENEGADA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1-2-3   | 5               | PEN    | 100   | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 6-5-4   | 5               | PEN    | 100   | Si         | No                   | Direccion      | Registrado        | ACTIVA     |

  @happy_path @regresion @version3.1.3 @registroTarjetaCredito_test27
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto TARJETA DE CREDITO para la tipología: DESACUERDO DE CONDICIONES/TASAS/TARIFAS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia DESACUERDO DE CONDICIONES/TASAS/TARIFAS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | canal   |
      | <motivos> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | App IBK            | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 2       | Bca Telef          | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE03   | 3       | Bca x internet     | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE04   | 4       | Canal Select       | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE05   | 5       | Entrega TC         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE06   | 6-7     | IBK Agentes        | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE07   | 8-1     | Interbank x FB     | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE08   | 2-3     | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE09   | 4-5     | Otros              | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE10   | 1-2-3   | Red CjGnet         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE11   | 4-5-6   | Red Tiendas        | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE12   | 6-7-8   | Televentas         | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE13   | 5-4-1   | Whatsapp           | Si         | No                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.3 @registroTarjetaCredito_test28
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto TARJETA DE CREDITO para la tipología: REPORTE INDEBIDO CENTRALES DE RIESGO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia REPORTE INDEBIDO CENTRALES DE RIESGO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | canal   |
      | <motivos> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | App IBK            | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 1       | Bca Telef          | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE03   | 1       | Bca x internet     | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE04   | 1       | Canal Select       | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE05   | 1       | Entrega TC         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE06   | 1       | IBK Agentes        | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE07   | 1       | Interbank x FB     | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE08   | 1       | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE09   | 1       | Otros              | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE10   | 1       | Red CjGnet         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE11   | 1       | Red Tiendas        | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE12   | 1       | Televentas         | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE13   | 1       | Whatsapp           | Si         | No                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.8 @registroTarjetaCredito_test51
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto TARJETA DE CREDITO para la tipología: INCUMPLIMIENTO DE CAMPAÑA
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia INCUMPLIMIENTO DE CAMPAÑA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | beneficioReclamado   | canal   |
      | <motivos> | <beneficioReclamado> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | beneficioReclamado | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1-2-3   | Cashback           | App IBK            | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 3-4     | Millas             | Bca Telef          | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE03   | 1       | Cashback           | Bca x internet     | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE04   | 2       | Millas             | Canal Select       | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE05   | 3       | Cashback           | Entrega TC         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE06   | 4       | Millas             | IBK Agentes        | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE07   | 1       | Cashback           | Interbank x FB     | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE08   | 2       | Millas             | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE09   | 3       | Cashback           | Otros              | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE10   | 4       | Millas             | Red CjGnet         | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE11   | 1       | Cashback           | Red Tiendas        | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE12   | 2       | Millas             | Televentas         | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE13   | 3       | Cashback           | Whatsapp           | Si         | No                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.9 @registroTarjetaCredito_test54
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto TARJETA DE CREDITO para la tipología: ATM DE OTRO BANCO NO ENTREGÓ DINERO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia ATM DE OTRO BANCO NO ENTREGÓ DINERO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | canal   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | 3               | PEN    | 100   | App IBK            | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 1       | 1               | PEN    | 100   | Bca Telef          | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE03   | 1       | 1               | PEN    | 100   | Bca x internet     | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE04   | 1       | 1               | PEN    | 100   | Canal Select       | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE05   | 1       | 1               | PEN    | 100   | Entrega TC         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE06   | 1       | 1               | PEN    | 100   | IBK Agentes        | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE07   | 1       | 1               | PEN    | 100   | Interbank x FB     | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE08   | 1       | 1               | PEN    | 100   | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE09   | 1       | 1               | PEN    | 100   | Otros              | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE10   | 1       | 1               | PEN    | 100   | Red CjGnet         | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE11   | 1       | 1               | PEN    | 100   | Red Tiendas        | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE12   | 1       | 1               | PEN    | 100   | Televentas         | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE13   | 1       | 1               | PEN    | 100   | Whatsapp           | Si         | No                   | Correo         | Registrado        | ACTIVA     |



  @happy_path @regresion @version3.1.4.1 @registroTarjetaCredito_test32
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto TARJETA DE CREDITO para la tipología: ATM DE IB NO DEPOSITÓ DINERO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia ATM DE IB NO DEPOSITÓ DINERO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | importeSolicitado   | importeReclamado   | medioAbono   | canal   |
      | <motivos> | <cantMovimientos>        | <moneda> | <importeSolicitado> | <importeReclamado> | <medioAbono> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESCASE | motivos | cantMovimientos | moneda | importeSolicitado | importeReclamado | medioAbono         | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01  | 1       | 3               | PEN    | 100               | 100              | Tarjeta de Crédito | App IBK            | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02  | 2       | 3               | PEN    | 200               | 200              | Orden de Pago      | Bca Telef          | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE03  | 3       | 3               | PEN    | 300               | 300              | Abono en Cuenta    | Bca x internet     | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE04  | 4       | 3               | PEN    | 400               | 400              | Tarjeta de Crédito | Canal Select       | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE05  | 1-2     | 1               | PEN    | 500               | 500              | Orden de Pago      | Entrega TC         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE06  | 2-3     | 1               | PEN    | 600               | 600              | Abono en Cuenta    | IBK Agentes        | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE07  | 3-4     | 1               | PEN    | 100               | 100              | Tarjeta de Crédito | Interbank x FB     | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE08  | 4-1     | 1               | PEN    | 200               | 200              | Orden de Pago      | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE09  | 1-2-3   | 1               | PEN    | 300               | 300              | Abono en Cuenta    | Otros              | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE10  | 4-1-2   | 1               | PEN    | 400               | 400              | Tarjeta de Crédito | Red CjGnet         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE11  | 3-1     | 1               | PEN    | 500               | 500              | Orden de Pago      | Red Tiendas        | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE12  | 3-2     | 1               | PEN    | 600               | 600              | Abono en Cuenta    | Televentas         | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE13  | 1-4     | 1               | PEN    | 100               | 100              | Tarjeta de Crédito | Whatsapp           | Si         | No                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.4.1 @registroTarjetaCredito_test33
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto TARJETA DE CREDITO para la tipología: ATM DE IB NO ENTREGÓ DINERO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia ATM DE IB NO ENTREGÓ DINERO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | importeSolicitado   | importeReclamado   | canal   |
      | <motivos> | <cantMovimientos>        | <moneda> | <importeSolicitado> | <importeReclamado> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESCASE | motivos | cantMovimientos | moneda | importeSolicitado | importeReclamado | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01  | 1       | 3               | PEN    | 100               | 100              | App IBK            | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02  | 2       | 3               | PEN    | 200               | 200              | Bca Telef          | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE03  | 3       | 3               | PEN    | 300               | 300              | Bca x internet     | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE04  | 4       | 3               | PEN    | 400               | 400              | Canal Select       | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE05  | 5       | 3               | PEN    | 500               | 500              | Entrega TC         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE06  | 1       | 1               | PEN    | 600               | 600              | IBK Agentes        | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE07  | 2       | 1               | PEN    | 100               | 100              | Interbank x FB     | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE08  | 3       | 1               | PEN    | 200               | 200              | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE09  | 4       | 1               | PEN    | 300               | 300              | Otros              | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE10  | 5       | 1               | PEN    | 400               | 400              | Red CjGnet         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE11  | 1-2-3   | 1               | PEN    | 500               | 500              | Red Tiendas        | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE12  | 4-5-1   | 1               | PEN    | 600               | 600              | Televentas         | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE13  | 1-2-4   | 1               | PEN    | 100               | 100              | Whatsapp           | Si         | No                   | Correo         | Registrado        | ACTIVA     |




  # ----------------------------------------------------------------------------------------------------------
  # --------------------------------------------- REGISTRO SOLICITUD ---------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.1 @registroTarjetaCredito_test14
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo SOLICITUD con producto TARJETA DE CREDITO para la tipología: ATENCIÓN POR PRENVENCIÓN Y FRAUDES
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia ATENCIÓN POR PRENVENCIÓN Y FRAUDES
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | operacionReconocida   | medioComunicacion   | flagTarjetaBloqueo   |
      | <motivos> | <operacionReconocida> | <medioComunicacion> | <flagTarjetaBloqueo> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | operacionReconocida | medioComunicacion | flagTarjetaBloqueo | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1-2     | Si                  | CALL              | Si                 | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 1       | No                  | Email             | Si                 | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE03   | 2       | Si                  | SMS               | Si                 | Si         | No                   | Correo         | Registrado        | ACTIVA     |


  #NOTA: EMISIÓN DE TARJETA SE REGISTRA CON RESOLUCIÓN AUTOMATICA PROCEDE, POR ENDE NO TIENE ESCENARIO RESOLUCIÓN
  @happy_path @regresion @version3.1.3 @registroTarjetaCredito_test29
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo SOLICITUD con producto TARJETA DE CREDITO para la tipología: EMISIÓN DE TARJETA
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia EMISIÓN DE TARJETA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CodigoMarca   | IndicadorTipo   |
      | <motivos> | <CodigoMarca> | <IndicadorTipo> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | IndicadorTipo          | CodigoMarca | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | Clásica                | VISA        | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 1       | American Express Green | AMEX        | Si         | No                   | Direccion      | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.6 @registroTarjetaCredito_test36
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo SOLICITUD con producto TARJETA DE CREDITO para la tipología: REPROGRAMACIÓN DE DEUDA TC SIN CAMPAÑA
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia REPROGRAMACIÓN DE DEUDA TC SIN CAMPAÑA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | Si         | Si                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.6 @registroTarjetaCredito_test37
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo SOLICITUD con producto TARJETA DE CREDITO para la tipología: REPROGRAMACIÓN DE DEUDA TC CON CAMPAÑA
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia REPROGRAMACIÓN DE DEUDA TC CON CAMPAÑA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | reducirLineaCredito   |
      | <motivos> | <reducirLineaCredito> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento


    Examples:
      | TESTCASE | motivos | reducirLineaCredito | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | Si                  | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 1       | No                  | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE03   | 1       | Si                  | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE04   | 1       | No                  | Si         | No                   | Direccion      | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.6 @registroTarjetaCredito_test38
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo SOLICITUD con producto TARJETA DE CREDITO para la tipología: COMPRA DE DEUDA INTERNA
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia COMPRA DE DEUDA INTERNA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | compraDeuda   | agregarTarjetaCredito   |
      | <motivos> | <compraDeuda> | <agregarTarjetaCredito> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESCASE | motivos | agregarTarjetaCredito | compraDeuda | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01  | 1-2     | 2                     | Paralela    | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02  | 2-1     | 3                     | Paralela    | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE03  | 1       | 4                     | Revolvente  | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE04  | 2       | 5                     | Revolvente  | Si         | No                   | Direccion      | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.7 @registroTarjetaCredito_test43
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo SOLICITUD con producto TARJETA DE CREDITO para la tipología: REGULARIZACIÓN DE COMPRA DE DEUDA TC
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia REGULARIZACIÓN DE COMPRA DE DEUDA TC
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | Si         | Si                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.7 @registroTarjetaCredito_test44
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo SOLICITUD con producto TARJETA DE CREDITO para la tipología: DEVOLUCIÓN DE DOCUMENTOS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia DEVOLUCION DE DOCUMENTOS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | tipoDocumentoTramite   |
      | <motivos> | <tipoDocumentoTramite> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | tipoDocumentoTramite    | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | Contrato                | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 1       | Constancia de Operación | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE03   | 1       | Constancia de Pago      | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE04   | 1       | Constancia de Retiro    | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE05   | 1       | Letra                   | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE06   | 1       | Pagaré                  | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE07   | 1       | Voucher                 | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE08   | 1       | Otros                   | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE09   | 1       | Estado de cuenta        | Si         | No                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.7 @registroTarjetaCredito_test45
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo SOLICITUD con producto TARJETA DE CREDITO para la tipología: ATENCIÓN IBK AGENTE
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia ATENCIÓN IBK AGENTE
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1-2-3   | 3               | PEN    | 100   | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 2-3-4   | 1               | PEN    | 100   | Si         | Si                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.7 @registroTarjetaCredito_test46
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo SOLICITUD con producto TARJETA DE CREDITO para la tipología: COMPRA DE DEUDA SIN AMPLIACIÓN
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia COMPRA DE DEUDA SIN AMPLIACIÓN
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | agregarTarjetaCredito   |
      | <motivos> | <agregarTarjetaCredito> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESCASE | motivos | agregarTarjetaCredito | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01  | 1-2     | 2                     | Si         | Si                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.7 @registroTarjetaCredito_test47
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo SOLICITUD con producto TARJETA DE CREDITO para la tipología: MODIFICACIÓN DE TASA COMPRA DE DEUDA TC
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia MODIFICACIÓN DE TASA COMPRA DE DEUDA TC
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | Si         | Si                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.8 @registroTarjetaCredito_test52
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo SOLICITUD con producto TARJETA DE CREDITO para la tipología: ABONO DE MILLAS
    Given que el usuario RETENCIÓN MULTIPRODUCTO TC accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia ABONO DE MILLAS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | Si         | Si                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.8 @registroTarjetaCredito_test53
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo SOLICITUD con producto TARJETA DE CREDITO para la tipología: CAMBIO DE TARJETA
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia CAMBIO DE TARJETA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | lugarEntrega   |
      | <motivos> | <lugarEntrega> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples: #REGISTRA MOTIVO - INDIVIDUAL
      | TESTCASE | motivos | lugarEntrega     | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | Tienda           | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 2       | Dirección exacta | Si         | No                   | Direccion      | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.9 @registroTarjetaCredito_test55
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo SOLICITUD con producto TARJETA DE CREDITO para la tipología: CLIENTE VIAJERO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia CLIENTE VIAJERO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples: #*** TIPOLOGIA - CIERRE AUTOMÁTICO
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | Si         | Si                   | Correo         | Registrado        | ACTIVA     |

  @happy_path @regresion @version3.1.4.1 @registroTarjetaCredito_test34
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo SOLICITUD con producto TARJETA DE CREDITO para la tipología: REPROGRAMACIÓN DE DEUDA
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia REPROGRAMACIÓN DE DEUDA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | reducirLineaCredito   |
      | <motivos> | <reducirLineaCredito> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | reducirLineaCredito | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1-2     | Si                  | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 2-1     | No                  | Si         | No                   | Direccion      | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.4.1 @registroTarjetaCredito_test35
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo SOLICITUD con producto TARJETA DE CREDITO para la tipología: DESBLOQUEO PREVENTIVO/SOBREENDEUDAMIENTO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia DESBLOQUEO PREVENTIVO/SOBREENDEUDAMIENTO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | Si         | Si                   | Correo         | Registrado        | ACTIVA     |

