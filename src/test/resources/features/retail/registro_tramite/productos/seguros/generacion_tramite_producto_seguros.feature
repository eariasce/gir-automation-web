@registro5
@all_generacion_tramite_seguro
Feature: Registro del trámite de Producto SEGUROS

 # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO PEDIDO ----------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.4.1 @registroSeguro_test2
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto SEGUROS para la tipología: DUPLICADO DE DOCUMENTO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto SEGUROS y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia DUPLICADO DE DOCUMENTO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | tipoProducto   |
      | <motivos> | <tipoProducto> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos  | tipoProducto       | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1-2-3    | Crédito            | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 4-5-6    | Cuenta             | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE03   | 7-8-9    | Tarjeta de crédito | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE04   | 10-11-12 | Crédito            | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE05   | 13-14-15 | Cuenta             | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE06   | 14-15-16 | Tarjeta de crédito | Si         | No                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.7 @registroSeguro_test7
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto SEGUROS para la tipología: INFORMACION DE MOVIMIENTOS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto SEGUROS y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia INFORMACION DE MOVIMIENTOS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | tipoProducto   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <tipoProducto> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos  | cantMovimientos | moneda | monto | tipoProducto       | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1-2-3    | 3               | PEN    | 100   | Crédito            | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 5-4-6    | 3               | PEN    | 100   | Cuenta             | Si         | Si                   | Direccion      | Registrado        | ACTIVA     |
      | CASE03   | 7-8-9    | 3               | PEN    | 100   | Tarjeta de crédito | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE03   | 10-11-12 | 3               | PEN    | 100   | Crédito            | Si         | Si                   | Direccion      | Registrado        | ACTIVA     |
      | CASE05   | 12-13-14 | 3               | PEN    | 100   | Cuenta             | Si         | Si                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.8 @registroSeguro_test11
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto SEGUROS para la tipología: MODIFICACIÓN DE CONDICIONES CONTRACTUALES
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto SEGUROS y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia MODIFICACIÓN DE CONDICIONES CONTRACTUALES
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | Si         | Si                   | Correo         | Registrado        | ACTIVA     |


  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO RECLAMO ----------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.4.1 @registroSeguro_test3
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto SEGUROS para la tipología: COBROS INDEBIDOS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto SEGUROS y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia COBROS INDEBIDOS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | tipoProducto   | canal   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <tipoProducto> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples: #ADJUNTAR ARCHIVO ADICIONALES DEBE SER OPCIONAL
      | TESTCASE | motivos  | cantMovimientos | moneda | monto | tipoProducto       | canal          | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1-2-3    | 3               | PEN    | 100   | Crédito            | App IBK        | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 4-5-6    | 3               | PEN    | 100   | Cuenta             | Bca Telef      | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE03   | 7-8-9    | 3               | PEN    | 100   | Tarjeta de crédito | Bca x internet | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE04   | 10-11-12 | 3               | PEN    | 100   | Crédito            | Canal Select   | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE05   | 13-14-15 | 3               | PEN    | 100   | Cuenta             | Entrega TC     | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE06   | 17-18-19 | 3               | PEN    | 100   | Tarjeta de crédito | IBK Agentes    | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE07   | 20-21-22 | 3               | PEN    | 100   | Crédito            | Interbank x FB | Si         | No                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.7 @registroSeguro_test8
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto SEGUROS para la tipología: NO RECEPCIÓN DE DOCUMENTO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto SEGUROS y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia NO RECEPCIÓN DE DOCUMENTO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | canal   |
      | <motivos> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | App IBK            | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 1       | Bca Telef          | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE03   | 1       | Bca x internet     | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE04   | 1       | Canal Select       | Si         | No                   | Direccion      | Nueva             | ACTIVA     |
      | CASE05   | 1       | Entrega TC         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE06   | 1       | IBK Agentes        | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE07   | 1       | Interbank x FB     | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE08   | 1       | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE09   | 1       | Otros              | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE10   | 1       | Red CjGnet         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE11   | 1       | Red Tiendas        | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE12   | 1       | Televentas         | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE13   | 1       | Whatsapp           | Si         | No                   | Correo         | Registrado        | ACTIVA     |


  # ----------------------------------------------------------------------------------------------------------
  # --------------------------------------------- REGISTRO SOLICITUD ---------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.4 @registroSeguro_test1
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo SOLICITUD con producto SEGUROS para la tipología: SOLICITUD DE ANULACIÓN DE SEGURO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto SEGUROS y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia SOLICITUD DE ANULACIÓN DE SEGURO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1-2     | Si         | Si                   | Direccion      | Registrado        | ACTIVA     |

  @happy_path @regresion @version3.1.4.1 @registroSeguro_test4
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo SOLICITUD con producto SEGUROS para la tipología: SEGURO DESGRAVAMEN FALLECIMIENTO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto SEGUROS y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia SEGURO DESGRAVAMEN FALLECIMIENTO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:  #ES NECESARIO ADJUNTAR ARCHIVO PARA QUE SALGA ASIGNAR
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1-2-3   | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 4-3-2   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.4.1 @registroSeguro_test5
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo SOLICITUD con producto SEGUROS para la tipología: PASE A CUOTAS SEGURO EXTRACASH
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto SEGUROS y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia PASE A CUOTAS SEGURO EXTRACASH
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples: #* TIPOLOGIA CON ARPI
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | Si         | Si                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.4.1 @registroSeguro_test6
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo SOLICITUD con producto SEGUROS para la tipología: DESAFILIACIÓN DE SEGURO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto SEGUROS y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia DESAFILIACIÓN DE SEGURO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | tipoProducto   |
      | <motivos> | <tipoProducto> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples: #REGISTRA MOTIVO - INDIVIDUAL
      | TESTCASE | motivos | tipoProducto       | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | Crédito            | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 2       | Cuenta             | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE03   | 3       | Tarjeta de crédito | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE04   | 4       | Crédito            | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE05   | 5       | Cuenta             | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE06   | 6       | Tarjeta de crédito | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE07   | 7       | Crédito            | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE08   | 8       | Cuenta             | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE09   | 9       | Tarjeta de crédito | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE10   | 10      | Crédito            | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE11   | 11      | Cuenta             | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE12   | 12      | Tarjeta de crédito | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE13   | 13      | Crédito            | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE14   | 14      | Cuenta             | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE15   | 15      | Tarjeta de crédito | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE16   | 16      | Crédito            | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE17   | 17      | Cuenta             | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE18   | 18      | Tarjeta de crédito | Si         | No                   | Direccion      | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.7 @registroSeguro_test9
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo SOLICITUD con producto SEGUROS para la tipología: CAMBIO DE SEGURO EXTERNO A INTERNO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto SEGUROS y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia CAMBIO DE SEGURO EXTERNO A INTERNO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1-2-3   | Si         | Si                   | Correo         | Registrado        | ACTIVA     |

