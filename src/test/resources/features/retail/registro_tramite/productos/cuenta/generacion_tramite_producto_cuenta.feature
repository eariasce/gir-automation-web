@all_generacion_tramite_cuenta
Feature: Registro de trámite de Producto CUENTA

  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO PEDIDO -----------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.0.9 @registroCuenta_test1
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto CUENTA para la tipología: EXONERACIÓN DE COBROS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto CUENTA y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia EXONERACIÓN DE COBROS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos  | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1-2-3    | 3               | PEN    | 50    | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 4-5-6    | 3               | PEN    | 100   | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE03   | 7-8-9    | 3               | PEN    | 150   | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE04   | 10-11-12 | 3               | PEN    | 200   | Si         | No                   | Direccion      | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.1 @registroCuenta_test4
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto CUENTA para la tipología: DUPLICADO DE ESTADO DE CUENTA/SERVICIO DE COPIAS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto CUENTA y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia DUPLICADO DE ESTADO DE CUENTA/SERVICIO DE COPIAS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | Tienda   | Autonomia   |
      | <motivos> | <Tienda> | <Autonomia> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples: # *ADJUNTAR DOCUMENTOS OBLIGATORIOS
      | TESTCASE | motivos | Tienda | Autonomia | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | No     | Si        | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 2-3     | Si     | No        | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE03   | 4-5-6   | Si     | Si        | Si         | Si                   | Direccion      | Registrado        | ACTIVA     |
      | CASE04   | 7       | No     | No        | Si         | Si                   | Direccion      | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.3 @registroCuenta_test8
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto CUENTA para la tipología: INFORMACION DE MOVIMIENTOS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto CUENTA y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia INFORMACION DE MOVIMIENTOS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1-2-3   | 5               | PEN    | 100   | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 4-5     | 5               | PEN    | 200   | Si         | No                   | Direccion      | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.3 @registroCuenta_test9
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto CUENTA para la tipología: DUPLICADO DE DOCUMENTO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto CUENTA y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia DUPLICADO DE DOCUMENTO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | tipoDocumentoTramite   |
      | <motivos> | <tipoDocumentoTramite> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | tipoDocumentoTramite    | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | Contrato                | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 1       | Constancia de Operación | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE03   | 1       | Constancia de Pago      | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE04   | 1       | Constancia de Retiro    | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE05   | 1       | Letra                   | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE06   | 1       | Pagaré                  | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE07   | 1       | Voucher                 | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE08   | 1       | Estado de cuenta        | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE09   | 1       | Otros                   | Si         | No                   | Correo         | Registrado        | ACTIVA     |


  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO RECLAMO ----------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.0.9 @registroCuenta_test2
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto CUENTA para la tipología: COBROS INDEBIDOS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto CUENTA y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia COBROS INDEBIDOS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos  | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1-2-3    | 3               | PEN    | 50    | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 4-5-6    | 3               | PEN    | 100   | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE03   | 7-8-9    | 3               | PEN    | 150   | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE04   | 10-11-12 | 3               | PEN    | 200   | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE05   | 13       | 5               | PEN    | 250   | Si         | No                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.0.9 @registroCuenta_test3
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto CUENTA para la tipología: CUENTA/CANCELACIÓN NO RECONOCIDA
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto CUENTA y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia CUENTA/CANCELACIÓN NO RECONOCIDA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1-2-3   | 5               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.1 @registroCuenta_test5
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto CUENTA para la tipología: SISTEMA DE RECOMPENSAS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto CUENTA y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia SISTEMA DE RECOMPENSAS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | tipoPrograma   | cantidad   | importe   |
      | <motivos> | <tipoPrograma> | <cantidad> | <importe> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | tipoPrograma               | cantidad | importe | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | Descuento cuenta sueldo    | 10       | 100     | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 2-3     | Descuento en Cuotas        | 20       | 150     | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE03   | 4-5     | Efectivo                   | 30       | 200     | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE04   | 6-7     | Millas                     | 40       | 100     | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE05   | 1-2-3   | Opciones sorteo millonario | 50       | 150     | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE06   | 4-5-6   | Vales                      | 60       | 200     | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE07   | 5-6-7   | Vales ruleta               | 70       | 300     | Si         | No                   | Direccion      | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.1 @registroCuenta_test6
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto CUENTA para la tipología: DESACUERDO DE CONDICIONES/TASAS/TARIFAS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto CUENTA y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia DESACUERDO DE CONDICIONES/TASAS/TARIFAS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1-2-3   | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 1-3-4   | Si         | No                   | Direccion      | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.3 @registroCuenta_test10
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto CUENTA para la tipología: NO RECEPCIÓN DE DOCUMENTO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto CUENTA y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia NO RECEPCIÓN DE DOCUMENTO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | tipoDocumentoTramite   |
      | <motivos> | <tipoDocumentoTramite> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | tipoDocumentoTramite    | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | Contrato                | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 1       | Constancia de Operación | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE03   | 1       | Constancia de Pago      | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE04   | 1       | Constancia de Retiro    | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE05   | 1       | Letra                   | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE06   | 1       | Pagaré                  | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE07   | 1       | Voucher                 | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE08   | 1       | Estado de cuenta        | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE09   | 1       | Otros                   | Si         | No                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.3 @registroCuenta_test11
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto CUENTA para la tipología: LIBERACIÓN DE RETENCIONES JUDICIALES
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto CUENTA y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia LIBERACIÓN DE RETENCIONES JUDICIALES
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESCASE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01  | 1       | 5               | PEN    | 100   | Si         | Si                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.4.1 @registroCuenta_test12
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto CUENTA para la tipología: ATM DE IB NO DEPOSITÓ DINERO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto CUENTA y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia ATM DE IB NO DEPOSITÓ DINERO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | importeSolicitado   | importeReclamado   | medioAbono   | canal   |
      | <motivos> | <cantMovimientos>        | <moneda> | <importeSolicitado> | <importeReclamado> | <medioAbono> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESCASE | motivos | cantMovimientos | moneda | importeSolicitado | importeReclamado | medioAbono         | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01  | 1       | 3               | PEN    | 100               | 100              | Tarjeta de Crédito | App IBK            | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02  | 2       | 3               | PEN    | 200               | 200              | Orden de Pago      | Bca Telef          | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE03  | 3       | 3               | PEN    | 300               | 300              | Abono en Cuenta    | Bca x internet     | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE04  | 4       | 3               | PEN    | 400               | 400              | Tarjeta de Crédito | Canal Select       | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE05  | 5       | 3               | PEN    | 500               | 500              | Orden de Pago      | Entrega TC         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE06  | 6       | 3               | PEN    | 600               | 600              | Abono en Cuenta    | IBK Agentes        | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE07  | 1       | 1               | PEN    | 100               | 100              | Tarjeta de Crédito | Interbank x FB     | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE08  | 2       | 1               | PEN    | 200               | 200              | Orden de Pago      | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE09  | 3       | 1               | PEN    | 300               | 300              | Abono en Cuenta    | Otros              | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE10  | 4       | 1               | PEN    | 400               | 400              | Tarjeta de Crédito | Red CjGnet         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE11  | 5       | 1               | PEN    | 500               | 500              | Orden de Pago      | Red Tiendas        | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE12  | 6       | 1               | PEN    | 600               | 600              | Abono en Cuenta    | Televentas         | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE13  | 1       | 1               | PEN    | 100               | 100              | Tarjeta de Crédito | Whatsapp           | Si         | No                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.7 @registroCuenta_test13
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo SOLICITUD con producto CUENTA para la tipología: SOLICITUD DE ALCANCÍA
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto CUENTA y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia SOLICITUD DE ALCANCÍA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01  | 1       | Si         | Si                   | Correo         | Registrado        | ACTIVA     |




  @happy_path @regresion @version3.1.8 @registroCuenta_test16
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto CUENTA para la tipología: INCUMPLIMIENTO DE CAMPAÑA
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto CUENTA y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia INCUMPLIMIENTO DE CAMPAÑA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | canal   | beneficioReclamado   |
      | <motivos> | <canal> | <beneficioReclamado> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | beneficioReclamado | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1-2-3   | Cashback           | App IBK            | Si         | Si                   | Direccion      | Registrado        | ACTIVA     |
      | CASE02   | 2-3-4   | Millas             | Bca Telef          | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE03   | 1       | Cashback           | Bca x internet     | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE04   | 2       | Millas             | Canal Select       | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE05   | 3       | Cashback           | Entrega TC         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE06   | 4       | Millas             | IBK Agentes        | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE07   | 1       | Cashback           | Interbank x FB     | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE08   | 2       | Millas             | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE09   | 3       | Cashback           | Otros              | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE10   | 4       | Millas             | Red CjGnet         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE11   | 1       | Cashback           | Red Tiendas        | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE12   | 2       | Millas             | Televentas         | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE13   | 3       | Cashback           | Whatsapp           | Si         | No                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.8 @registroCuenta_test17
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto CUENTA para la tipología: TRANSACCIÓN MAL O NO PROCESADA
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto CUENTA y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia TRANSACCIÓN MAL O NO PROCESADA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | canal   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | 5               | PEN    | 100   | App IBK            | Si         | Si                   | Direccion      | Registrado        | ACTIVA     |
      | CASE02   | 1       | 3               | PEN    | 100   | Bca Telef          | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE03   | 1       | 1               | PEN    | 100   | Bca x internet     | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE04   | 1       | 1               | PEN    | 100   | Canal Select       | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE05   | 1       | 1               | PEN    | 100   | Entrega TC         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE06   | 1       | 1               | PEN    | 100   | IBK Agentes        | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE07   | 1       | 1               | PEN    | 100   | Interbank x FB     | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE08   | 1       | 1               | PEN    | 100   | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE09   | 1       | 1               | PEN    | 100   | Otros              | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE10   | 1       | 1               | PEN    | 100   | Red CjGnet         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE11   | 1       | 1               | PEN    | 100   | Red Tiendas        | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE12   | 1       | 1               | PEN    | 100   | Televentas         | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE13   | 1       | 1               | PEN    | 100   | Whatsapp           | Si         | No                   | Correo         | Registrado        | ACTIVA     |


  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO SOLICITUD ----------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.1 @registroCuenta_test7
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo SOLICITUD con producto CUENTA para la tipología: CANCELACIÓN DE CUENTA
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto CUENTA y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia CANCELACIÓN DE CUENTA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     |

  @happy_path @regresion @version3.1.7 @registroCuenta_test14
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo SOLICITUD con producto CUENTA para la tipología: PAGO CTS PESCA
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto CUENTA y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia PAGO CTS PESCA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01  | 1       | Si         | Si                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.7 @registroCuenta_test15
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo SOLICITUD con producto CUENTA para la tipología: ATENCIÓN IBK AGENTE
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto CUENTA y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia ATENCIÓN IBK AGENTE
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1-2-3   | 3               | PEN    | 100   | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 4-5-6   | 3               | PEN    | 100   | Si         | No                   | Correo         | Nueva             | ACTIVA     |
      | CASE03   | 5-6-7   | 1               | PEN    | 100   | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE04   | 6-7-8   | 1               | PEN    | 100   | Si         | No                   | Direccion      | Nueva             | ACTIVA     |


  @happy_path @regresion @version3.1.8 @registroCuenta_test18
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo SOLICITUD con producto CUENTA para la tipología: TRASLADOS DE CTS A OTRO BANCO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto CUENTA y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia TRASLADOS DE CTS A OTRO BANCO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01  | 1-2     | Si         | Si                   | Correo         | Registrado        | ACTIVA     |

