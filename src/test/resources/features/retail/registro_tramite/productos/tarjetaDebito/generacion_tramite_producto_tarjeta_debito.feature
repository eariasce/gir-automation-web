@registro6
@all_generacion_tramite_tarjetaDebito
Feature: Registro del Trámite de Producto TARJETA DE DEBITO

  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO PEDIDO -----------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.0.9 @registroTarjetaDebito_test1
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto TARJETA DE DEBITO para la tipología: EXONERACIÓN DE COBROS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto TARJETA DE DEBITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia EXONERACIÓN DE COBROS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | 5               | PEN    | 100   | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 2-3     | 1               | PEN    | 150   | Si         | No                   | Direccion      | Registrado        | ACTIVA     |



  @happy_path @regresion @version3.1.7 @registroTarjetaDebito_test9
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto TARJETA DE DEBITO para la tipología: PROGRAMA DE RECOMPENSAS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto TARJETA DE DEBITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia PROGRAMA DE RECOMPENSAS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | Si         | Si                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.8 @registroTarjetaDebito_test12
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto TARJETA DE DEBITO para la tipología: INFORMACIÓN DE ESTABLECIMIENTO/ RET.EFECTIVO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto TARJETA DE DEBITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia INFORMACIÓN DE ESTABLECIMIENTO/ RET.EFECTIVO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1-2     | 3               | PEN    | 100   | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE01   | 1-2     | 3               | PEN    | 100   | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE01   | 1-2     | 3               | PEN    | 100   | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE01   | 1-2     | 3               | PEN    | 100   | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE01   | 1-2     | 3               | PEN    | 100   | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE01   | 1-2     | 3               | PEN    | 100   | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE01   | 1-2     | 3               | PEN    | 100   | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE01   | 1-2     | 3               | PEN    | 100   | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE01   | 1-2     | 3               | PEN    | 100   | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE01   | 1-2     | 3               | PEN    | 100   | Si         | Si                   | Correo         | Registrado        | ACTIVA     |



  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO RECLAMO ------------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.0.9 @registroTarjetaDebito_test2
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto TARJETA DE DEBITO para la tipología: COBROS INDEBIDOS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto TARJETA DE DEBITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia COBROS INDEBIDOS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1-2-3   | 5               | PEN    | 100   | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 4-5-6   | 3               | PEN    | 200   | Si         | No                   | Direccion      | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.0.9 @registroTarjetaDebito_test3
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto TARJETA DE DEBITO para la tipología: CONSUMOS NO RECONOCIDOS POR FRAUDE
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto TARJETA DE DEBITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia CONSUMOS NO RECONOCIDOS POR FRAUDE
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   |getMotivoSucedidoTarjeta	|flagTarjetaBloqueo|
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <MotivoSucedidoTarjeta>                | <flagTarjetaBloqueo>                |
    #And selecciono lo sucedido "<MotivoSucedidoTarjeta>" con la tarjeta, si se ha bloqueado "<flagTarjetaBloqueo>"
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd | MotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | CASE01   | 1-2     | 5               | PEN    | 100   | Si         | Si                   | Correo         | Registrado        | ACTIVA     | CFS001                | Si                 |
      | CASE02   | 3       | 1               | PEN    | 150   | Si         | No                   | Direccion      | Registrado        | ACTIVA     | CFS005                | No                 |


  @happy_path @regresion @version3.0.9 @registroTarjetaDebito_test4
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto TARJETA DE DEBITO para la tipología: CONSUMOS MAL PROCESADOS POR EL COMERCIO/MARCA
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto TARJETA DE DEBITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia CONSUMOS MAL PROCESADOS POR EL COMERCIO/MARCA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | productoServicioAdquirido   | tipoServicio   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <productoServicioAdquirido> | <tipoServicio> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    #revisar que este este este encapsulado en otro step
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd | productoServicioAdquirido | tipoServicio |
      | CASE01   | 1-3-4   | 3               | PEN    | 180   | Si         | No                   | Correo         | Registrado        | ACTIVA     |                           |              |
      | CASE02   | 5-6     | 3               | PEN    | 250   | Si         | No                   | Direccion      | Registrado        | ACTIVA     |                           |              |

      #OJO Motivo 2 ES: Id - 35 = Se cargó consumo no concretado y desistió de la compra - Se intentó adquirir un: - indicaArchivoAdjunto: Si - Obligatorio - para auto asignar
      #NOTA: productoServicioAdquirido: Producto - No tiene tipo Servicio
      #NOTA: productoServicioAdquirido: Servicio - Si tiene tipo Servicio SON: Hotel - Tour - Vuelos - Otros
      | CASE03   | 2       | 3               | PEN    | 350   | Si         | Si                   | Correo         | Registrado        | ACTIVA     | Producto                  |              |
      | CASE04   | 2       | 1               | PEN    | 450   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Servicio                  | Hotel        |



  @happy_path @regresion @version3.1.3 @registroTarjetaDebito_test6
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto TARJETA DE DEBITO para la tipología: LIBERACION DE RETENCIONES
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto TARJETA DE DEBITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia LIBERACION DE RETENCIONES
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | 5               | PEN    | 100   | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 1-2     | 5               | PEN    | 30    | Si         | No                   | Direccion      | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.3 @registroTarjetaDebito_test7
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto TARJETA DE DEBITO para la tipología: OPERACIÓN DENEGADA
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto TARJETA DE DEBITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia OPERACIÓN DENEGADA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    #revisar que este este este encapsulado en otro step
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1-2-3   | 5               | PEN    | 150   | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 4-5-6   | 5               | PEN    | 250   | Si         | No                   | Correo         | Direccion         | ACTIVA     |


  @happy_path @regresion @version3.1.4.1 @registroTarjetaDebito_test8
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto TARJETA DE DEBITO para la tipología: ATM DE IB NO ENTREGÓ DINERO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto TARJETA DE DEBITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia ATM DE IB NO ENTREGÓ DINERO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | importeSolicitado   | importeReclamado   | canal   |
      | <motivos> | <cantMovimientos>        | <moneda> | <importeSolicitado> | <importeReclamado> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESCASE | motivos | cantMovimientos | moneda | importeSolicitado | importeReclamado | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01  | 1       | 3               | PEN    | 100               | 100              | App IBK            | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02  | 2       | 3               | PEN    | 200               | 200              | Bca Telef          | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE03  | 3       | 3               | PEN    | 300               | 300              | Bca x internet     | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE04  | 4       | 3               | PEN    | 400               | 400              | Canal Select       | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE05  | 5       | 3               | PEN    | 500               | 500              | Entrega TC         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE06  | 6       | 3               | PEN    | 600               | 600              | IBK Agentes        | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE07  | 7       | 3               | PEN    | 100               | 100              | Interbank x FB     | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE08  | 1       | 1               | PEN    | 200               | 200              | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE09  | 2       | 1               | PEN    | 300               | 300              | Otros              | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE10  | 3       | 1               | PEN    | 400               | 400              | Red CjGnet         | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE11  | 4       | 1               | PEN    | 500               | 500              | Red Tiendas        | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE12  | 5       | 1               | PEN    | 600               | 600              | Televentas         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE13  | 1-2-6   | 1               | PEN    | 100               | 100              | Whatsapp           | Si         | No                   | Direccion      | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.7 @registroTarjetaDebito_test10
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto TARJETA DE DEBITO para la tipología: ATM DE OTRO BANCO NO ENTREGO DINERO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto TARJETA DE DEBITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia ATM DE OTRO BANCO NO ENTREGÓ DINERO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | canal   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESCASE | motivos | cantMovimientos | moneda | monto | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01  | 1-2     | 5               | PEN    | 100   | App IBK            | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02  | 2-1     | 5               | PEN    | 200   | Bca Telef          | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE03  | 1       | 1               | PEN    | 300   | Bca x internet     | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE04  | 2       | 1               | PEN    | 400   | Canal Select       | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE05  | 1       | 1               | PEN    | 500   | Entrega TC         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE06  | 2       | 1               | PEN    | 600   | IBK Agentes        | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE07  | 1       | 1               | PEN    | 700   | Interbank x FB     | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE08  | 2       | 1               | PEN    | 800   | Módulo de cobranza | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE09  | 1       | 1               | PEN    | 900   | Otros              | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE10  | 2       | 1               | PEN    | 100   | Red CjGnet         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE11  | 1       | 1               | PEN    | 200   | Red Tiendas        | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE12  | 2       | 1               | PEN    | 300   | Televentas         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE13  | 1       | 1               | PEN    | 400   | Whatsapp           | Si         | No                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.7 @registroTarjetaDebito_test11
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto TARJETA DE DEBITO para la tipología: REVISIÓN DE DEPÓSITOS A TERCEROS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto TARJETA DE DEBITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia REVISIÓN DE DEPÓSITOS A TERCEROS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | canal   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESCASE | motivos | cantMovimientos | moneda | monto | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01  | 1       | 5               | PEN    | 100   | App IBK            | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02  | 1       | 1               | PEN    | 200   | Bca Telef          | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE03  | 1       | 1               | PEN    | 300   | Bca x internet     | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE04  | 1       | 1               | PEN    | 400   | Canal Select       | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE05  | 1       | 1               | PEN    | 500   | Entrega TC         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE06  | 1       | 1               | PEN    | 600   | IBK Agentes        | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE07  | 1       | 1               | PEN    | 700   | Interbank x FB     | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE08  | 1       | 1               | PEN    | 800   | Módulo de cobranza | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE09  | 1       | 1               | PEN    | 900   | Otros              | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE10  | 1       | 1               | PEN    | 100   | Red CjGnet         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE11  | 1       | 1               | PEN    | 200   | Red Tiendas        | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE12  | 1       | 1               | PEN    | 300   | Televentas         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE13  | 1       | 1               | PEN    | 400   | Whatsapp           | Si         | No                   | Correo         | Registrado        | ACTIVA     |


  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO SOLICITUD ----------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.1 @registroTarjetaDebito_test5
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo SOLICITUD con producto TARJETA DE DEBITO para la tipología: ATENCIÓN POR PRENVENCIÓN Y FRAUDES
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto TARJETA DE DEBITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia ATENCIÓN POR PRENVENCIÓN Y FRAUDES
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | operacionReconocida   | medioComunicacion   | flagTarjetaBloqueo   |
      | <motivos> | <operacionReconocida> | <medioComunicacion> | <flagTarjetaBloqueo> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | operacionReconocida | medioComunicacion | flagTarjetaBloqueo | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1-2     | Si                  | CALL              | Si                 | Si         | Si                   | Direccion      | Registrado        | ACTIVA     |
      | CASE02   | 1       | No                  | Email             | Si                 | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE03   | 2       | Si                  | SMS               | Si                 | Si         | No                   | Direccion      | Registrado        | ACTIVA     |

  @happy_path @regresion @version3.1.8 @registroTarjetaDebito_test13
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo SOLICITUD con producto TARJETA DE DEBITO para la tipología: ABONO DE MILLAS
    Given que el usuario RETENCIÓN MULTIPRODUCTO TC accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto TARJETA DE DEBITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia ABONO DE MILLAS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01  | 1       | Si         | Si                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.8 @registroTarjetaDebito_test14
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo SOLICITUD con producto TARJETA DE DEBITO para la tipología: REPOSICIÓN DE TARJETA DE DEBITO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto TARJETA DE DEBITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia REPOSICIÓN DE TARJETA DE DEBITO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01  | 1       | Si         | Si                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.9 @registroTarjetaDebito_test15
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo SOLICITUD con producto TARJETA DE DEBITO para la tipología: CLIENTE VIAJERO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto TARJETA DE DEBITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia CLIENTE VIAJERO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples: #*** TIPOLOGIA - CIERRE AUTOMÁTICO
      | TESCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01  | 1       | Si         | Si                   | Correo         | Registrado        | ACTIVA     |

