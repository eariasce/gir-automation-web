@registroautonomias_td
@all_generacion_tramite_tarjetaDebito
Feature: Registro del Trámite de Producto TARJETA DE DEBITO

  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO PEDIDO -----------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.0.9 @registroTarjetaDebito_autonomia_test1
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto TARJETA DE DEBITO para la tipología: EXONERACIÓN DE COBROS
    Given que el usuario <ROL> accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM y el producto TARJETA DE DEBITO validar autonomia y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia EXONERACIÓN DE COBROS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
      #step valida autonomia en mensaje de registro
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
     #step valida textos de autonomia en historial
    Then se valida en historial el reclamo registrado y seguimiento

    Examples: #DNI del cliente debe pertenecer al segmento Joven/Oro (validar por heidisql)
      | TESTCASE | ROL                                 | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd   |
      | CASE01   | ASESOR                              | 1       | 1               | PEN    | 50    | Si         | Si                   | Correo         | Registrado        | AUTONOMIA_TD |
      | CASE02   | ASESOR DE RETENCIÓN                 | 2       | 1               | PEN    | 15    | Si         | Si                   | Correo         | Registrado        | AUTONOMIA_TD |
      | CASE03   | GERENTE DE TIENDA/GERENTE ASISTENTE | 3       | 1               | PEN    | 40    | Si         | Si                   | Correo         | Registrado        | AUTONOMIA_TD |
      | CASE04   | REPRESENTANTE FINANCIERO            | 3       | 1               | PEN    | 40    | Si         | Si                   | Correo         | Registrado        | AUTONOMIA_TD |
      | CASE05   | SUPERVISOR RETENCIÓN                | 1       | 1               | PEN    | 50    | Si         | Si                   | Correo         | Registrado        | AUTONOMIA_TD |
      | CASE06   | SUPERVISOR CC                       | 2       | 1               | PEN    | 15    | Si         | Si                   | Correo         | Registrado        | AUTONOMIA_TD |
      | CASE07   | SUPERVISOR RETENCIÓN                | 3       | 1               | PEN    | 40    | Si         | Si                   | Correo         | Registrado        | AUTONOMIA_TD |


  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO RECLAMO ------------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.0.9 @registroTarjetaDebito_autonomia_test2
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto TARJETA DE DEBITO para la tipología: COBROS INDEBIDOS
    Given que el usuario <ROL> accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM y el producto TARJETA DE DEBITO validar autonomia y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia COBROS INDEBIDOS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
     #step valida autonomia en mensaje de registro
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
     #step valida textos de autonomia en historial
    Then se valida en historial el reclamo registrado y seguimiento

    Examples: #DNI del cliente debe pertenecer al segmento Premium/Plata (validar por heidisql)
      | TESTCASE | ROL                                 | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd   |
      | CASE01   | ASESOR                              | 2       | 1               | PEN    | 50    | Si         | Si                   | Correo         | Registrado        | AUTONOMIA_TD |
      | CASE02   | ASESOR DE RETENCIÓN                 | 3       | 1               | PEN    | 15    | Si         | Si                   | Correo         | Registrado        | AUTONOMIA_TD |
      | CASE03   | GERENTE DE TIENDA/GERENTE ASISTENTE | 4       | 1               | PEN    | 40    | Si         | Si                   | Correo         | Registrado        | AUTONOMIA_TD |
      | CASE03   | REPRESENTANTE FINANCIERO            | 4       | 1               | PEN    | 40    | Si         | Si                   | Correo         | Registrado        | AUTONOMIA_TD |
      | CASE04   | SUPERVISOR RETENCIÓN                | 2       | 1               | PEN    | 50    | Si         | Si                   | Correo         | Registrado        | AUTONOMIA_TD |
      | CASE05   | SUPERVISOR CC                       | 3       | 1               | PEN    | 15    | Si         | Si                   | Correo         | Registrado        | AUTONOMIA_TD |
      | CASE06   | SUPERVISOR RETENCIÓN                | 4       | 1               | PEN    | 40    | Si         | Si                   | Correo         | Registrado        | AUTONOMIA_TD |

