@all_generacion_tramite_rappiBankCuentas
Feature: Registro del trámite Producto de RAPPIBANK - CUENTAS

  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO RECLAMO -------------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.8 @registroRappiBankCuentas_test1
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto RAPPIBANK - CUENTAS para la tipología: COBROS INDEBIDOS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto RAPPIBANK - CUENTAS y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia COBROS INDEBIDOS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | canal   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1-2-3   | 3               | PEN    | 100   | App IBK            | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 2-5-4   | 3               | PEN    | 100   | Bca Telef          | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE03   | 1       | 1               | PEN    | 100   | Bca x internet     | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE04   | 2       | 1               | PEN    | 100   | Canal Select       | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE05   | 3       | 1               | PEN    | 100   | Entrega TC         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE06   | 4       | 1               | PEN    | 100   | IBK Agentes        | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE07   | 5       | 1               | PEN    | 100   | Interbank x FB     | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE08   | 1       | 1               | PEN    | 100   | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE09   | 2       | 1               | PEN    | 100   | Otros              | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE10   | 3       | 1               | PEN    | 100   | Red CjGnet         | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE11   | 4       | 1               | PEN    | 100   | Red Tiendas        | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE12   | 5       | 1               | PEN    | 100   | Televentas         | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE13   | 1       | 1               | PEN    | 100   | Whatsapp           | Si         | No                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.8 @registroRappiBankCuentas_test2
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto RAPPIBANK - CUENTAS para la tipología: TRANSACCIÓN MAL O NO PROCESADA
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto RAPPIBANK - CUENTAS y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia TRANSACCIÓN MAL O NO PROCESADA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | canal   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1-2-3   | 3               | PEN    | 100   | App IBK            | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 2-5-4   | 3               | PEN    | 100   | Bca Telef          | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE03   | 1       | 1               | PEN    | 100   | Bca x internet     | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE04   | 2       | 1               | PEN    | 100   | Canal Select       | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE05   | 3       | 1               | PEN    | 100   | Entrega TC         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE06   | 4       | 1               | PEN    | 100   | IBK Agentes        | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE07   | 5       | 1               | PEN    | 100   | Interbank x FB     | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE08   | 1       | 1               | PEN    | 100   | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE09   | 2       | 1               | PEN    | 100   | Otros              | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE10   | 3       | 1               | PEN    | 100   | Red CjGnet         | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE11   | 4       | 1               | PEN    | 100   | Red Tiendas        | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE12   | 5       | 1               | PEN    | 100   | Televentas         | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE13   | 1       | 1               | PEN    | 100   | Whatsapp           | Si         | No                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.9 @registroRappiBankCuentas_test3
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto RAPPIBANK - CUENTAS para la tipología: CONSUMO/ RETIRO NO EFECTUADO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto RAPPIBANK - CUENTAS y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia CONSUMO/ RETIRO NO EFECTUADO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | canal   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1-2-3   | 3               | PEN    | 100   | App IBK            | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 2-5-4   | 1               | PEN    | 100   | Bca Telef          | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE03   | 1       | 1               | PEN    | 100   | Bca x internet     | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE04   | 2       | 1               | PEN    | 100   | Canal Select       | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE05   | 3       | 1               | PEN    | 100   | Entrega TC         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE06   | 4       | 1               | PEN    | 100   | IBK Agentes        | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE07   | 5       | 1               | PEN    | 100   | Interbank x FB     | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE08   | 1       | 1               | PEN    | 100   | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE09   | 2       | 1               | PEN    | 100   | Otros              | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE10   | 3       | 1               | PEN    | 100   | Red CjGnet         | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE11   | 4       | 1               | PEN    | 100   | Red Tiendas        | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE12   | 5       | 1               | PEN    | 100   | Televentas         | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE13   | 1       | 1               | PEN    | 100   | Whatsapp           | Si         | No                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.9 @registroRappiBankCuentas_test4
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto RAPPIBANK - CUENTAS para la tipología: CUENTA/CANCELACIÓN NO RECONOCIDA
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto RAPPIBANK - CUENTAS y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia CUENTA/CANCELACIÓN NO RECONOCIDA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | canal   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1-2     | 3               | PEN    | 100   | App IBK            | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 1       | 1               | PEN    | 100   | Bca Telef          | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE03   | 2       | 1               | PEN    | 100   | Bca x internet     | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE04   | 1       | 1               | PEN    | 100   | Canal Select       | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE05   | 2       | 1               | PEN    | 100   | Entrega TC         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE06   | 1       | 1               | PEN    | 100   | IBK Agentes        | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE07   | 2       | 1               | PEN    | 100   | Interbank x FB     | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE08   | 1       | 1               | PEN    | 100   | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE09   | 2       | 1               | PEN    | 100   | Otros              | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE10   | 1       | 1               | PEN    | 100   | Red CjGnet         | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE11   | 2       | 1               | PEN    | 100   | Red Tiendas        | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE12   | 1       | 1               | PEN    | 100   | Televentas         | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE13   | 2       | 1               | PEN    | 100   | Whatsapp           | Si         | No                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.9 @registroRappiBankCuentas_test5
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto RAPPIBANK - CUENTAS para la tipología: DESACUERDO CON EL SERVICIO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto RAPPIBANK - CUENTAS y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia DESACUERDO CON EL SERVICIO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | canal   |
      | <motivos> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | App IBK            | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 1       | Bca Telef          | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE03   | 1       | Bca x internet     | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE04   | 1       | Canal Select       | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE05   | 1       | Entrega TC         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE06   | 1       | IBK Agentes        | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE07   | 1       | Interbank x FB     | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE08   | 1       | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE09   | 1       | Otros              | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE10   | 1       | Red CjGnet         | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE11   | 1       | Red Tiendas        | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE12   | 1       | Televentas         | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE13   | 1       | Whatsapp           | Si         | No                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.9 @registroRappiBankCuentas_test6
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto RAPPIBANK - CUENTAS para la tipología: DESACUERDO DE CONDICIONES/TASAS/TARIFAS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto RAPPIBANK - CUENTAS y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia DESACUERDO DE CONDICIONES/TASAS/TARIFAS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | canal   |
      | <motivos> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | App IBK            | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 1       | Bca Telef          | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE03   | 1       | Bca x internet     | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE04   | 1       | Canal Select       | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE05   | 1       | Entrega TC         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE06   | 1       | IBK Agentes        | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE07   | 1       | Interbank x FB     | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE08   | 1       | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE09   | 1       | Otros              | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE10   | 1       | Red CjGnet         | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE11   | 1       | Red Tiendas        | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE12   | 1       | Televentas         | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE13   | 1       | Whatsapp           | Si         | No                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.9 @registroRappiBankCuentas_test7
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto RAPPIBANK - CUENTAS para la tipología: INSATISFACCIÓN GESTIÓN DE ENTREGA DE TARJETA
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto RAPPIBANK - CUENTAS y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia INSATISFACCIÓN GESTIÓN DE ENTREGA DE TARJETA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | canal   |
      | <motivos> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | App IBK            | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 1       | Bca Telef          | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE03   | 1       | Bca x internet     | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE04   | 1       | Canal Select       | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE05   | 1       | Entrega TC         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE06   | 1       | IBK Agentes        | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE07   | 1       | Interbank x FB     | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE08   | 1       | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE09   | 1       | Otros              | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE10   | 1       | Red CjGnet         | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE11   | 1       | Red Tiendas        | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE12   | 1       | Televentas         | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE13   | 1       | Whatsapp           | Si         | No                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.9 @registroRappiBankCuentas_test8
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto RAPPIBANK - CUENTAS para la tipología: LIBERACIÓN DE RETENCIONES
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto RAPPIBANK - CUENTAS y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia LIBERACIÓN DE RETENCIONES
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | canal   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1-2     | 3               | PEN    | 100   | App IBK            | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 1       | 1               | PEN    | 100   | Bca Telef          | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE03   | 2       | 1               | PEN    | 100   | Bca x internet     | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE04   | 1       | 1               | PEN    | 100   | Canal Select       | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE05   | 2       | 1               | PEN    | 100   | Entrega TC         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE06   | 1       | 1               | PEN    | 100   | IBK Agentes        | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE07   | 2       | 1               | PEN    | 100   | Interbank x FB     | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE08   | 1       | 1               | PEN    | 100   | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE09   | 2       | 1               | PEN    | 100   | Otros              | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE10   | 1       | 1               | PEN    | 100   | Red CjGnet         | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE11   | 2       | 1               | PEN    | 100   | Red Tiendas        | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE12   | 1       | 1               | PEN    | 100   | Televentas         | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE13   | 2       | 1               | PEN    | 100   | Whatsapp           | Si         | No                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.9 @registroRappiBankCuentas_test9
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto RAPPIBANK - CUENTAS para la tipología: OPERACIÓN DENEGADA
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto RAPPIBANK - CUENTAS y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia OPERACIÓN DENEGADA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | canal   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1-2-3   | 3               | PEN    | 100   | App IBK            | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 2-5-4   | 1               | PEN    | 100   | Bca Telef          | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE03   | 1       | 1               | PEN    | 100   | Bca x internet     | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE04   | 2       | 1               | PEN    | 100   | Canal Select       | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE05   | 3       | 1               | PEN    | 100   | Entrega TC         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE06   | 4       | 1               | PEN    | 100   | IBK Agentes        | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE07   | 5       | 1               | PEN    | 100   | Interbank x FB     | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE08   | 1       | 1               | PEN    | 100   | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE09   | 2       | 1               | PEN    | 100   | Otros              | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE10   | 3       | 1               | PEN    | 100   | Red CjGnet         | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE11   | 4       | 1               | PEN    | 100   | Red Tiendas        | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE12   | 5       | 1               | PEN    | 100   | Televentas         | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE13   | 1       | 1               | PEN    | 100   | Whatsapp           | Si         | No                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.9 @registroRappiBankCuentas_test10
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto RAPPIBANK - CUENTAS para la tipología: REVISIÓN DE DEPÓSITOS A TERCEROS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto RAPPIBANK - CUENTAS y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia REVISIÓN DE DEPÓSITOS A TERCEROS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | canal   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | 3               | PEN    | 100   | App IBK            | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 1       | 1               | PEN    | 100   | Bca Telef          | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE03   | 1       | 1               | PEN    | 100   | Bca x internet     | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE04   | 1       | 1               | PEN    | 100   | Canal Select       | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE05   | 1       | 1               | PEN    | 100   | Entrega TC         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE06   | 1       | 1               | PEN    | 100   | IBK Agentes        | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE07   | 1       | 1               | PEN    | 100   | Interbank x FB     | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE08   | 1       | 1               | PEN    | 100   | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE09   | 1       | 1               | PEN    | 100   | Otros              | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE10   | 1       | 1               | PEN    | 100   | Red CjGnet         | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE11   | 1       | 1               | PEN    | 100   | Red Tiendas        | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE12   | 1       | 1               | PEN    | 100   | Televentas         | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE13   | 1       | 1               | PEN    | 100   | Whatsapp           | Si         | No                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.9 @registroRappiBankCuentas_test11
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto RAPPIBANK - CUENTAS para la tipología: SISTEMA DE RECOMPENSAS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto RAPPIBANK - CUENTAS y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia SISTEMA DE RECOMPENSAS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | canal   |
      | <motivos> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | App IBK            | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 1       | Bca Telef          | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE03   | 1       | Bca x internet     | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE04   | 1       | Canal Select       | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE05   | 1       | Entrega TC         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE06   | 1       | IBK Agentes        | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE07   | 1       | Interbank x FB     | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE08   | 1       | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE09   | 1       | Otros              | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE10   | 1       | Red CjGnet         | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE11   | 1       | Red Tiendas        | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE12   | 1       | Televentas         | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE13   | 1       | Whatsapp           | Si         | No                   | Correo         | Registrado        | ACTIVA     |

