@all_generacion_tramite_segurosRelacionados
Feature: Registro del trámite Producto de SEGUROS RELACIONADOS

  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO PEDIDO--------------------------------------------
  #===========================================================================================================
  @happy_path @regresion @version3.1.7 @segurosRelacionados_test1
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto SEGUROS RELACIONADOS para la tipología: ENDOSO SEGUROS DEL BIEN
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto SEGUROS RELACIONADOS y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia ENDOSO SEGUROS DEL BIEN
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1-2     | Si         | Si                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.8 @segurosRelacionados_test5
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto SEGUROS RELACIONADOS para la tipología: DUPLICADO DE DOCUMENTO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM el con producto SEGUROS RELACIONADOS y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia DUPLICADO DE DOCUMENTO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | tipoFiltroProducto   |
      | <motivos> | <tipoFiltroProducto> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | tipoFiltroProducto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1-2-3   | Tarjeta de Crédito | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 2-3-4   | Crédito            | Si         | No                   | Direccion      | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.8 @segurosRelacionados_test6
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto SEGUROS RELACIONADOS para la tipología: INFORMACION DE MOVIMIENTOS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto SEGUROS RELACIONADOS y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia INFORMACION DE MOVIMIENTOS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | canal   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1-2-3   | 3               | PEN    | 100   | App IBK            | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 2-3-4   | 3               | PEN    | 100   | Bca Telef          | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE03   | 1       | 1               | PEN    | 100   | Bca x internet     | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE04   | 2       | 1               | PEN    | 100   | Canal Select       | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE05   | 3       | 1               | PEN    | 100   | Entrega TC         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE06   | 4       | 1               | PEN    | 100   | IBK Agentes        | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE07   | 1       | 1               | PEN    | 100   | Interbank x FB     | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE08   | 2       | 1               | PEN    | 100   | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE09   | 3       | 1               | PEN    | 100   | Otros              | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE10   | 4       | 1               | PEN    | 100   | Red CjGnet         | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE11   | 1       | 1               | PEN    | 100   | Red Tiendas        | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE12   | 2       | 1               | PEN    | 100   | Televentas         | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE13   | 3       | 1               | PEN    | 100   | Whatsapp           | Si         | No                   | Correo         | Registrado        | ACTIVA     |


  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO SOLICITUD-----------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.7 @segurosRelacionados_test2
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo SOLICITUD con producto SEGUROS RELACIONADOS para la tipología: APLICACIÓN DE FONDOS POR SINIESTROS/DESGRAVAMEN
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto SEGUROS RELACIONADOS y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia APLICACIÓN DE FONDOS POR SINIESTROS/DESGRAVAMEN
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1-2     | Si         | Si                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.7 @segurosRelacionados_test3
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo SOLICITUD con producto SEGUROS RELACIONADOS para la tipología: SOLICITUD DE INFORMACIÓN DE SINIESTROS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto SEGUROS RELACIONADOS y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia SOLICITUD DE INFORMACIÓN DE SINIESTROS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1-2-3   | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 5-4-6   | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE03   | 1-2-7   | Si         | No                   | Correo         | Registrado        | ACTIVA     |


  @happy_path @regresion @version3.1.7 @segurosRelacionados_test4
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo SOLICITUD con producto SEGUROS RELACIONADOS para la tipología: LEVANTAMIENTO DE ENDOSO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto SEGUROS RELACIONADOS y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia LEVANTAMIENTO DE ENDOSO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | Si         | Si                   | Correo         | Registrado        | ACTIVA     |



  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO RECLAMO -----------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.8 @segurosRelacionados_test7
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto SEGUROS RELACIONADOS para la tipología: COBROS INDEBIDOS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto SEGUROS RELACIONADOS y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia COBROS INDEBIDOS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | tipoProducto   | canal   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <tipoProducto> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | tipoProducto       | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1-2-3   | 3               | PEN    | 100   | Crédito            | App IBK            | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 2-3-4   | 3               | PEN    | 100   | Cuenta             | Bca Telef          | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE03   | 1       | 1               | PEN    | 100   | Tarjeta de crédito | Bca x internet     | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE04   | 2       | 1               | PEN    | 100   | Crédito            | Canal Select       | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE05   | 3       | 1               | PEN    | 100   | Cuenta             | Entrega TC         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE06   | 4       | 1               | PEN    | 100   | Tarjeta de crédito | IBK Agentes        | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE07   | 1       | 1               | PEN    | 100   | Crédito            | Interbank x FB     | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE08   | 2       | 1               | PEN    | 100   | Cuenta             | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE09   | 3       | 1               | PEN    | 100   | Tarjeta de crédito | Otros              | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE10   | 4       | 1               | PEN    | 100   | Crédito            | Red CjGnet         | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE11   | 1       | 1               | PEN    | 100   | Cuenta             | Red Tiendas        | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE12   | 2       | 1               | PEN    | 100   | Tarjeta de crédito | Televentas         | Si         | No                   | Direccion      | Registrado        | ACTIVA     |
      | CASE13   | 3       | 1               | PEN    | 100   | Crédito            | Whatsapp           | Si         | No                   | Correo         | Registrado        | ACTIVA     |

