@all_generacion_tramite_cuentaCorriente
Feature: Registro del trámite Producto de CUENTA CORRIENTE

  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO PEDIDO--------------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.7 @registroCuentaCorriente_test1
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto CUENTA CORRIENTE para la tipología: COPIA DE BOLETA DE DEPÓSITO LIMA
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto CUENTA CORRIENTE y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia COPIA DE BOLETA DE DEPÓSITO LIMA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1-2     | 5               | PEN    | 100   | Si         | Si                   | Correo         | Registrado        | ACTIVA     |


  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO RECLAMO-------------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.7 @registroCuentaCorriente_test2
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto CUENTA CORRIENTE para la tipología: REPORTE INDEBIDO CENTRALES DE RIESGO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto CUENTA CORRIENTE y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia REPORTE INDEBIDO CENTRALES DE RIESGO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | canal   |
      | <motivos> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd |
      | CASE01   | 1       | App IBK            | Si         | Si                   | Correo         | Registrado        | ACTIVA     |
      | CASE02   | 1       | Bca Telef          | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE03   | 1       | Bca x internet     | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE04   | 1       | Canal Select       | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE05   | 1       | Entrega TC         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE06   | 1       | IBK Agentes        | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE07   | 1       | Interbank x FB     | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE08   | 1       | Módulo de cobranza | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE09   | 1       | Otros              | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE10   | 1       | Red CjGnet         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE11   | 1       | Red Tiendas        | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE12   | 1       | Televentas         | Si         | No                   | Correo         | Registrado        | ACTIVA     |
      | CASE13   | 1       | Whatsapp           | Si         | No                   | Correo         | Registrado        | ACTIVA     |

