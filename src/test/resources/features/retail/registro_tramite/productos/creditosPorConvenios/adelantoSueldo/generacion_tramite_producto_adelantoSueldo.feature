@registro4
@all_generacion_tramite_adelantoSueldo
Feature: Registro de trámite de Producto ADELANTO SUELDO

  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO PEDIDO ------------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.5 @registroAdelantoSueldo_test1
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto ADELANTO SUELDO para la tipología: ANULACIÓN DE CRÉDITO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto ADELANTO SUELDO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia ANULACIÓN DE CRÉDITO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples: #NOTA: REGISTRAR - ADMITE UN SOLO MOTIVO
      | TESTCASE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        |

  @happy_path @regresion @version3.1.7 @registroAdelantoSueldo_test3
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto ADELANTO SUELDO para la tipología: INFORMACION DE MOVIMIENTOS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto ADELANTO SUELDO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia INFORMACION DE MOVIMIENTOS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1       | Si         | Si                   | Direccion      | Registrado        |



  @happy_path @regresion @version3.1.8 @registroAdelantoSueldo_test6
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto ADELANTO SUELDO para la tipología: DUPLICADO DE DOCUMENTO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto ADELANTO SUELDO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia DUPLICADO DE DOCUMENTO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | tipoDocumentoTramite   |
      | <motivos> | <tipoDocumentoTramite> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | tipoDocumentoTramite    | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1       | Contrato                | Si         | Si                   | Direccion      | Registrado        |
      | CASE02   | 1       | Constancia de Operación | Si         | No                   | Direccion      | Registrado        |
      | CASE03   | 1       | Constancia de Pago      | Si         | No                   | Direccion      | Registrado        |
      | CASE04   | 1       | Constancia de Retiro    | Si         | No                   | Direccion      | Registrado        |
      | CASE05   | 1       | Letra                   | Si         | No                   | Direccion      | Registrado        |
      | CASE06   | 1       | Pagaré                  | Si         | No                   | Direccion      | Registrado        |
      | CASE07   | 1       | Voucher                 | Si         | No                   | Direccion      | Registrado        |
      | CASE08   | 1       | Estado de cuenta        | Si         | No                   | Direccion      | Registrado        |
      | CASE09   | 1       | Otros                   | Si         | No                   | Direccion      | Registrado        |


  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO RECLAMO ------------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.7 @registroAdelantoSueldo_test4
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto ADELANTO SUELDO para la tipología: CONSUMO/ RETIRO NO EFECTUADO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto ADELANTO SUELDO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia CONSUMO/ RETIRO NO EFECTUADO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | canal   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1       | 5               | PEN    | 100   | App IBK            | Si         | Si                   | Direccion      | Registrado        |
      | CASE02   | 1       | 1               | PEN    | 100   | Bca Telef          | Si         | Si                   | Direccion      | Registrado        |
      | CASE03   | 1       | 1               | PEN    | 300   | Bca x internet     | Si         | No                   | Direccion      | Registrado        |
      | CASE04   | 1       | 1               | PEN    | 400   | Canal Select       | Si         | No                   | Direccion      | Registrado        |
      | CASE05   | 1       | 1               | PEN    | 500   | Entrega TC         | Si         | No                   | Direccion      | Registrado        |
      | CASE06   | 1       | 1               | PEN    | 100   | IBK Agentes        | Si         | No                   | Direccion      | Registrado        |
      | CASE07   | 1       | 1               | PEN    | 200   | Interbank x FB     | Si         | No                   | Direccion      | Registrado        |
      | CASE08   | 1       | 1               | PEN    | 300   | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        |
      | CASE09   | 1       | 1               | PEN    | 400   | Otros              | Si         | No                   | Direccion      | Registrado        |
      | CASE10   | 1       | 1               | PEN    | 500   | Red CjGnet         | Si         | No                   | Direccion      | Registrado        |
      | CASE11   | 1       | 1               | PEN    | 600   | Red Tiendas        | Si         | No                   | Direccion      | Registrado        |
      | CASE12   | 1       | 1               | PEN    | 700   | Televentas         | Si         | No                   | Direccion      | Registrado        |
      | CASE13   | 1       | 1               | PEN    | 800   | Whatsapp           | Si         | No                   | Direccion      | Registrado        |


  @happy_path @regresion @version3.1.6 @registroAdelantoSueldo_test2
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto ADELANTO SUELDO para la tipología: COBROS INDEBIDOS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto ADELANTO SUELDO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia COBROS INDEBIDOS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | canal   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1       | 5               | PEN    | 100   | App IBK            | Si         | Si                   | Direccion      | Registrado        |
      | CASE02   | 1       | 1               | PEN    | 100   | Bca Telef          | Si         | Si                   | Direccion      | Registrado        |
      | CASE03   | 1       | 1               | PEN    | 300   | Bca x internet     | Si         | No                   | Direccion      | Registrado        |
      | CASE04   | 1       | 1               | PEN    | 400   | Canal Select       | Si         | No                   | Direccion      | Registrado        |
      | CASE05   | 1       | 1               | PEN    | 500   | Entrega TC         | Si         | No                   | Correo         | Registrado        |
      | CASE06   | 1       | 1               | PEN    | 100   | IBK Agentes        | Si         | No                   | Correo         | Registrado        |
      | CASE07   | 1       | 1               | PEN    | 200   | Interbank x FB     | Si         | No                   | Direccion      | Registrado        |
      | CASE08   | 1       | 1               | PEN    | 300   | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        |
      | CASE09   | 1       | 1               | PEN    | 400   | Otros              | Si         | No                   | Correo         | Registrado        |
      | CASE10   | 1       | 1               | PEN    | 500   | Red CjGnet         | Si         | No                   | Correo         | Registrado        |
      | CASE11   | 1       | 1               | PEN    | 600   | Red Tiendas        | Si         | No                   | Direccion      | Registrado        |
      | CASE12   | 1       | 1               | PEN    | 700   | Televentas         | Si         | No                   | Direccion      | Registrado        |
      | CASE13   | 1       | 1               | PEN    | 800   | Whatsapp           | Si         | No                   | Correo         | Registrado        |


  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO SOLICITUD ----------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.7 @registroAdelantoSueldo_test5
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto ADELANTO SUELDO para la tipología: MODIFICACIÓN DE CUENTA SUELDO ASOCIADA
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto ADELANTO SUELDO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia MODIFICACIÓN DE CUENTA SUELDO ASOCIADA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1       | Si         | Si                   | Direccion      | Registrado        |
