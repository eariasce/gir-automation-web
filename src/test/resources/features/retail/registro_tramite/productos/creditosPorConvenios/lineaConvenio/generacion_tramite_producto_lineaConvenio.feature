@registro4
@all_generacion_tramite_lineaConvenio
Feature: Registro de trámite de Producto LINEA CONVENIO

  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO PEDIDO ------------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.5 @registroLineaConvenio_test1
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto LINEA CONVENIO para la tipología: BAJA DE TASA
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto LINEA CONVENIO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia BAJA DE TASA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1       | Si         | Si                   | Direccion      | Registrado        |


  @happy_path @regresion @version3.1.5 @registroLineaConvenio_test2
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto LINEA CONVENIO para la tipología: MODIFICACIÓN DE CONDICIONES CONTRACTUALES
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto LINEA CONVENIO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia MODIFICACIÓN DE CONDICIONES CONTRACTUALES
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1       | Si         | Si                   | Direccion      | Registrado        |


  @happy_path @regresion @version3.1.5 @registroLineaConvenio_test3
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto LINEA CONVENIO para la tipología: ANULACIÓN DE CRÉDITO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto LINEA CONVENIO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia ANULACIÓN DE CRÉDITO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1-2-3   | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        |
      | CASE02   | 4-5-6   | 1               | PEN    | 150   | Si         | Si                   | Direccion      | Registrado        |


  @happy_path @regresion @version3.1.5 @registroLineaConvenio_test4
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto LINEA CONVENIO para la tipología: DUPLICADO DE DOCUMENTO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto LINEA CONVENIO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia DUPLICADO DE DOCUMENTO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | tipoDocumentoTramite   |
      | <motivos> | <tipoDocumentoTramite> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples: #Se cambio correo-registrado a correo-nuevo por errores en registro (falta data de correo registrado)
      | TESTCASE | motivos | tipoDocumentoTramite    | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1       | Contrato                | Si         | Si                   | Correo         | Nueva             |
      | CASE02   | 1       | Constancia de Operación | Si         | No                   | Correo         | Nueva             |
      | CASE03   | 1       | Constancia de Pago      | Si         | No                   | Direccion      | Registrado        |
      | CASE04   | 1       | Constancia de Retiro    | Si         | No                   | Direccion      | Registrado        |
      | CASE05   | 1       | Letra                   | Si         | No                   | Correo         | Nueva             |
      | CASE06   | 1       | Pagaré                  | Si         | No                   | Correo         | Nueva             |
      | CASE07   | 1       | Voucher                 | Si         | No                   | Direccion      | Registrado        |
      | CASE08   | 1       | Otros                   | Si         | No                   | Correo         | Nueva             |


  @happy_path @regresion @version3.1.5 @registroLineaConvenio_test5
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto LINEA CONVENIO para la tipología: DUPLICADO DE VOUCHER
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto LINEA CONVENIO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia DUPLICADO DE VOUCHER
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1       | 5               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        |


  @happy_path @regresion @version3.1.5 @registroLineaConvenio_test6
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto LINEA CONVENIO para la tipología: PAGO ANTICIPADO Y ADELANTO DE CUOTAS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto LINEA CONVENIO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia PAGO ANTICIPADO Y ADELANTO DE CUOTAS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1-2-3   | 5               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        |
      | CASE02   | 2-3-4   | 5               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        |


  @happy_path @regresion @version3.1.5 @registroLineaConvenio_test7
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto LINEA CONVENIO para la tipología: INFORMACION DE MOVIMIENTOS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto LINEA CONVENIO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia INFORMACION DE MOVIMIENTOS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1       | 5               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        |




  @happy_path @regresion @version3.1.7 @registroLineaConvenio_test9
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto LINEA CONVENIO para la tipología: ENDOSO SEGUROS DE DESGRAVAMEN
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto LINEA CONVENIO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia ENDOSO SEGUROS DE DESGRAVAMEN
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1       | 5               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        |



  @happy_path @regresion @version3.1.8 @registroLineaConvenio_test14
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto LINEA CONVENIO para la tipología: DEVOLUCIÓN CUOTAS EN TRÁNSITO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto LINEA CONVENIO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia DEVOLUCIÓN CUOTAS EN TRÁNSITO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1       | 5               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        |



  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO RECLAMO ------------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.5 @registroLineaConvenio_test8
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto LINEA CONVENIO para la tipología: TRANSACCIÓN MAL O NO PROCESADA
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto LINEA CONVENIO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia TRANSACCIÓN MAL O NO PROCESADA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1       | 5               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        |

  @happy_path @regresion @version3.1.7 @registroLineaConvenio_test10
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto LINEA CONVENIO para la tipología: DESACUERDO DE CONDICIONES/TASAS/TARIFAS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto LINEA CONVENIO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia DESACUERDO DE CONDICIONES/TASAS/TARIFAS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | canal   |
      | <motivos> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1-2     | App IBK            | Si         | Si                   | Direccion      | Registrado        |
      | CASE02   | 2-1     | Bca Telef          | Si         | No                   | Direccion      | Registrado        |
      | CASE03   | 1       | Bca x internet     | Si         | No                   | Direccion      | Registrado        |
      | CASE04   | 2       | Canal Select       | Si         | No                   | Direccion      | Registrado        |
      | CASE05   | 1       | Entrega TC         | Si         | No                   | Direccion      | Registrado        |
      | CASE06   | 2       | IBK Agentes        | Si         | No                   | Direccion      | Registrado        |
      | CASE07   | 1       | Interbank x FB     | Si         | No                   | Direccion      | Registrado        |
      | CASE08   | 2       | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        |
      | CASE09   | 1       | Otros              | Si         | No                   | Direccion      | Registrado        |
      | CASE10   | 2       | Red CjGnet         | Si         | No                   | Direccion      | Registrado        |
      | CASE11   | 1       | Red Tiendas        | Si         | No                   | Direccion      | Registrado        |
      | CASE12   | 2       | Televentas         | Si         | No                   | Direccion      | Registrado        |
      | CASE13   | 1       | Whatsapp           | Si         | No                   | Direccion      | Registrado        |


  @happy_path @regresion @version3.1.7 @registroLineaConvenio_test11
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto LINEA CONVENIO para la tipología: NO RECEPCIÓN DE DOCUMENTO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto LINEA CONVENIO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia NO RECEPCIÓN DE DOCUMENTO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | canal   |
      | <motivos> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1       | App IBK            | Si         | Si                   | Direccion      | Registrado        |
      | CASE02   | 1       | Bca Telef          | Si         | No                   | Direccion      | Registrado        |
      | CASE03   | 1       | Bca x internet     | Si         | No                   | Direccion      | Registrado        |
      | CASE04   | 1       | Canal Select       | Si         | No                   | Direccion      | Registrado        |
      | CASE05   | 1       | Entrega TC         | Si         | No                   | Direccion      | Registrado        |
      | CASE06   | 1       | IBK Agentes        | Si         | No                   | Direccion      | Registrado        |
      | CASE07   | 1       | Interbank x FB     | Si         | No                   | Direccion      | Registrado        |
      | CASE08   | 1       | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        |
      | CASE09   | 1       | Otros              | Si         | No                   | Direccion      | Registrado        |
      | CASE10   | 1       | Red CjGnet         | Si         | No                   | Direccion      | Registrado        |
      | CASE11   | 1       | Red Tiendas        | Si         | No                   | Direccion      | Registrado        |
      | CASE12   | 1       | Televentas         | Si         | No                   | Direccion      | Registrado        |
      | CASE13   | 1       | Whatsapp           | Si         | No                   | Direccion      | Registrado        |


  @happy_path @regresion @version3.1.7 @registroLineaConvenio_test12
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto LINEA CONVENIO para la tipología: CONSUMO/ RETIRO NO EFECTUADO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto LINEA CONVENIO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia CONSUMO/ RETIRO NO EFECTUADO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | canal   | quePasoConLaTarjeta   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <canal> | <quePasoConLaTarjeta> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | quePasoConLaTarjeta                                        | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1       | 3               | PEN    | 100   | Fue robada o usada en un secuestro                         | App IBK            | Si         | Si                   | Direccion      | Registrado        |
      | CASE02   | 1       | 3               | PEN    | 100   | La perdió o no recuerda                                    | Bca Telef          | Si         | No                   | Direccion      | Registrado        |
      | CASE03   | 1       | 1               | PEN    | 100   | Está en poder del cliente                                  | Bca x internet     | Si         | No                   | Direccion      | Registrado        |
      | CASE04   | 1       | 1               | PEN    | 100   | El cliente nunca contrató el producto                      | Canal Select       | Si         | No                   | Direccion      | Registrado        |
      | CASE05   | 1       | 1               | PEN    | 100   | Fue usada a través de las aplicaciones por robo de celular | Entrega TC         | Si         | No                   | Direccion      | Registrado        |
      | CASE06   | 1       | 1               | PEN    | 100   | Fue robada o usada en un secuestro                         | IBK Agentes        | Si         | No                   | Direccion      | Registrado        |
      | CASE07   | 1       | 1               | PEN    | 100   | La perdió o no recuerda                                    | Interbank x FB     | Si         | No                   | Direccion      | Registrado        |
      | CASE08   | 1       | 1               | PEN    | 100   | Está en poder del cliente                                  | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        |
      | CASE09   | 1       | 1               | PEN    | 100   | El cliente nunca contrató el producto                      | Otros              | Si         | No                   | Direccion      | Registrado        |
      | CASE10   | 1       | 1               | PEN    | 100   | Fue usada a través de las aplicaciones por robo de celular | Red CjGnet         | Si         | No                   | Direccion      | Registrado        |
      | CASE11   | 1       | 1               | PEN    | 100   | Fue robada o usada en un secuestro                         | Red Tiendas        | Si         | No                   | Direccion      | Registrado        |
      | CASE12   | 1       | 1               | PEN    | 100   | La perdió o no recuerda                                    | Televentas         | Si         | No                   | Direccion      | Registrado        |
      | CASE13   | 1       | 1               | PEN    | 100   | Está en poder del cliente                                  | Whatsapp           | Si         | No                   | Direccion      | Registrado        |


  @happy_path @regresion @version3.1.7 @registroLineaConvenio_test13
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto LINEA CONVENIO para la tipología: REPORTE INDEBIDO CENTRALES DE RIESGO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto LINEA CONVENIO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia REPORTE INDEBIDO CENTRALES DE RIESGO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | canal   |
      | <motivos> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento


    Examples:
      | TESTCASE | motivos | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1       | App IBK            | Si         | Si                   | Direccion      | Registrado        |
      | CASE02   | 1       | Bca Telef          | Si         | No                   | Direccion      | Registrado        |
      | CASE03   | 1       | Bca x internet     | Si         | No                   | Direccion      | Registrado        |
      | CASE04   | 1       | Canal Select       | Si         | No                   | Direccion      | Registrado        |
      | CASE05   | 1       | Entrega TC         | Si         | No                   | Direccion      | Registrado        |
      | CASE06   | 1       | IBK Agentes        | Si         | No                   | Direccion      | Registrado        |
      | CASE07   | 1       | Interbank x FB     | Si         | No                   | Direccion      | Registrado        |
      | CASE08   | 1       | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        |
      | CASE09   | 1       | Otros              | Si         | No                   | Direccion      | Registrado        |
      | CASE10   | 1       | Red CjGnet         | Si         | No                   | Direccion      | Registrado        |
      | CASE11   | 1       | Red Tiendas        | Si         | No                   | Direccion      | Registrado        |
      | CASE12   | 1       | Televentas         | Si         | No                   | Direccion      | Registrado        |
      | CASE13   | 1       | Whatsapp           | Si         | No                   | Direccion      | Registrado        |
