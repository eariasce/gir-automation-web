@all_generacion_tramite_refinanciado
Feature: Registro de trámite de Producto REFINANCIADO

  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO PEDIDO ------------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.5 @registroRefinanciado_test1
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto REFINANCIADO para la tipología: BAJA DE TASA
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto REFINANCIADO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia BAJA DE TASA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1       | Si         | Si                   | Direccion      | Registrado        |


  @happy_path @regresion @version3.1.5 @registroRefinanciado_test2
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto REFINANCIADO para la tipología: DUPLICADO DE CRONOGRAMA
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto REFINANCIADO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia DUPLICADO DE CRONOGRAMA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1       | Si         | Si                   | Direccion      | Registrado        |


  @happy_path @regresion @version3.1.5 @registroRefinanciado_test3
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto REFINANCIADO para la tipología: INFORMACIÓN DE MOVIMIENTOS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto REFINANCIADO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia INFORMACION DE MOVIMIENTOS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1-2     | Si         | Si                   | Correo         | Registrado        |


  @happy_path @regresion @version3.1.5 @registroRefinanciado_test4
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto REFINANCIADO para la tipología: MODIFICACIÓN CONFIGURACIÓN CRÉDITO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto REFINANCIADO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia MODIFICACIÓN CONFIGURACIÓN CRÉDITO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples: #REGISTRA MOTIVO INDIVIDUAL
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1       | Si         | Si                   | Correo         | Registrado        |
      | CASE02   | 2       | Si         | Si                   | Direccion      | Registrado        |
      | CASE03   | 3       | Si         | Si                   | Correo         | Registrado        |


  @happy_path @regresion @version3.1.5 @registroRefinanciado_test5
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto REFINANCIADO para la tipología: ANULACIÓN DE CRÉDITO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto REFINANCIADO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia ANULACIÓN DE CRÉDITO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        |


  @happy_path @regresion @version3.1.5 @registroRefinanciado_test6
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto REFINANCIADO para la tipología: DEVOLUCIÓN DE DOCUMENTOS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto REFINANCIADO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia DEVOLUCIÓN DE DOCUMENTOS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | tipoDocumentoTramite   |
      | <motivos> | <tipoDocumentoTramite> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | tipoDocumentoTramite    | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1       | Contrato                | Si         | Si                   | Correo         | Registrado        |
      | CASE02   | 1       | Constancia de Operación | Si         | No                   | Correo         | Registrado        |
      | CASE03   | 1       | Constancia de Pago      | Si         | No                   | Direccion      | Registrado        |
      | CASE04   | 1       | Constancia de Retiro    | Si         | No                   | Direccion      | Registrado        |
      | CASE05   | 1       | Letra                   | Si         | No                   | Correo         | Registrado        |
      | CASE06   | 1       | Pagaré                  | Si         | No                   | Correo         | Registrado        |
      | CASE07   | 1       | Voucher                 | Si         | No                   | Direccion      | Registrado        |
      | CASE08   | 1       | Otros                   | Si         | No                   | Correo         | Registrado        |


  @happy_path @regresion @version3.1.5 @registroRefinanciado_test7
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto REFINANCIADO para la tipología: DUPLICADO DE DOCUMENTO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto REFINANCIADO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia DUPLICADO DE DOCUMENTO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | tipoDocumentoTramite   |
      | <motivos> | <tipoDocumentoTramite> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | tipoDocumentoTramite    | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1       | Contrato                | Si         | Si                   | Correo         | Registrado        |
      | CASE02   | 1       | Constancia de Operación | Si         | No                   | Correo         | Registrado        |
      | CASE03   | 1       | Constancia de Pago      | Si         | No                   | Direccion      | Registrado        |
      | CASE04   | 1       | Constancia de Retiro    | Si         | No                   | Direccion      | Registrado        |
      | CASE05   | 1       | Letra                   | Si         | No                   | Correo         | Registrado        |
      | CASE06   | 1       | Pagaré                  | Si         | No                   | Correo         | Registrado        |
      | CASE07   | 1       | Voucher                 | Si         | No                   | Direccion      | Registrado        |
      | CASE08   | 1       | Otros                   | Si         | No                   | Correo         | Registrado        |


  @happy_path @regresion @version3.1.5 @registroRefinanciado_test8
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto REFINANCIADO para la tipología: EXONERACIÓN DE COBROS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto REFINANCIADO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia EXONERACIÓN DE COBROS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1-2-3   | 5               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        |
      | CASE02   | 3-4-5   | 5               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        |


  @happy_path @regresion @version3.1.5 @registroRefinanciado_test9
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto REFINANCIADO para la tipología: MODIFICACIÓN CONDICIONES CUOTAS CRÉDITO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto REFINANCIADO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia MODIFICACIÓN CONDICIONES CUOTAS CRÉDITO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | tipoCuota   |
      | <motivos> | <tipoCuota> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples: #REGISTRA MOTIVO INDIVIDUAL
      | TESTCASE | motivos | tipoCuota      | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1       | 1              | Si         | Si                   | Correo         | Registrado        |
      | CASE02   | 2       | Ordinaria      | Si         | No                   | Correo         | Registrado        |
      | CASE03   | 2       | Extraordinaria | Si         | No                   | Direccion      | Registrado        |
      | CASE04   | 3       | 12             | Si         | No                   | Correo         | Registrado        |


  @happy_path @regresion @version3.1.8 @registroRefinanciado_test14
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto REFINANCIADO para la tipología: ENDOSO SEGUROS DE DESGRAVAMEN
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto REFINANCIADO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia ENDOSO SEGUROS DE DESGRAVAMEN
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1       | Si         | Si                   | Correo         | Registrado        |


  @happy_path @regresion @version3.1.8 @registroRefinanciado_test15
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto REFINANCIADO para la tipología: CAMBIO DE FORMA DE ENVÍO DE CORRESPONDENCIA
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto REFINANCIADO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia CAMBIO DE FORMA DE ENVÍO DE CORRESPONDENCIA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1-2-3   | Si         | Si                   | Direccion      | Registrado        |
      | CASE02   | 3-4     | Si         | No                   | Correo         | Registrado        |


  @happy_path @regresion @version3.1.8 @registroRefinanciado_test16
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto REFINANCIADO para la tipología: CUOTA FLEXIBLE
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto REFINANCIADO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia CUOTA FLEXIBLE
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples: #*ADJUNTAR DOCUMENTOS OBLIGATORIOS - EN TAL CASO GIRU NO DEBERIA PERMITIR REGISTRO
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1       | Si         | Si                   | Correo         | Registrado        |


  @happy_path @regresion @version3.1.8 @registroRefinanciado_test17
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto REFINANCIADO para la tipología: PAGO ANTICIPADO Y ADELANTO DE CUOTAS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto REFINANCIADO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia PAGO ANTICIPADO Y ADELANTO DE CUOTAS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1-2-3   | 3               | PEN    | 100   | Si         | Si                   | Correo         | Registrado        |
      | CASE02   | 2-4     | 3               | PEN    | 100   | Si         | No                   | Direccion      | Registrado        |



  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO RECLAMO ------------------------------------------
  #===========================================================================================================


  @happy_path @regresion @version3.1.5 @registroRefinanciado_test10
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto REFINANCIADO para la tipología: COBROS INDEBIDOS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto REFINANCIADO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia COBROS INDEBIDOS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1-2-3   | 5               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        |
      | CASE02   | 3-4-5   | 5               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        |


  @happy_path @regresion @version3.1.5 @registroRefinanciado_test11
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto REFINANCIADO para la tipología: DESACUERDO DE CONDICIONES/TASAS/TARIFAS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto REFINANCIADO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia DESACUERDO DE CONDICIONES/TASAS/TARIFAS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1-2-3   | Si         | Si                   | Correo         | Registrado        |


  @happy_path @regresion @version3.1.5 @registroRefinanciado_test12
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto REFINANCIADO para la tipología: REPORTE INDEBIDO CENTRALES DE RIESGO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto REFINANCIADO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia REPORTE INDEBIDO CENTRALES DE RIESGO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1       | Si         | Si                   | Direccion      | Registrado        |


  @happy_path @regresion @version3.1.5 @registroRefinanciado_test13
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto REFINANCIADO para la tipología: TRANSACCIÓN MAL O NO PROCESADA
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto REFINANCIADO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia TRANSACCIÓN MAL O NO PROCESADA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | canal   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    
    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1-2-3   | 3               | PEN    | 100   | App IBK            | Si         | Si                   | Direccion      | Registrado        |
      | CASE02   | 3-4-5   | 3               | PEN    | 200   | Bca Telef          | Si         | No                   | Correo         | Registrado        |
      | CASE03   | 1       | 1               | PEN    | 200   | Bca x internet     | Si         | No                   | Correo         | Registrado        |
      | CASE04   | 2       | 1               | PEN    | 200   | Canal Select       | Si         | No                   | Correo         | Registrado        |
      | CASE05   | 3       | 1               | PEN    | 200   | Entrega TC         | Si         | No                   | Correo         | Registrado        |
      | CASE06   | 4       | 1               | PEN    | 200   | IBK Agentes        | Si         | No                   | Correo         | Registrado        |
      | CASE07   | 5       | 1               | PEN    | 200   | Interbank x FB     | Si         | No                   | Correo         | Registrado        |
      | CASE08   | 1       | 1               | PEN    | 200   | Módulo de cobranza | Si         | No                   | Correo         | Registrado        |
      | CASE09   | 2       | 1               | PEN    | 200   | Otros              | Si         | No                   | Correo         | Registrado        |
      | CASE10   | 3       | 1               | PEN    | 200   | Red CjGnet         | Si         | No                   | Correo         | Registrado        |
      | CASE11   | 4       | 1               | PEN    | 200   | Red Tiendas        | Si         | No                   | Correo         | Registrado        |
      | CASE12   | 5       | 1               | PEN    | 200   | Televentas         | Si         | No                   | Correo         | Registrado        |
      | CASE13   | 1       | 1               | PEN    | 200   | Whatsapp           | Si         | No                   | Correo         | Registrado        |


  @happy_path @regresion @version3.1.8 @registroRefinanciado_test18
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto REFINANCIADO para la tipología: CONSUMO/ RETIRO NO EFECTUADO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto REFINANCIADO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia CONSUMO/ RETIRO NO EFECTUADO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | canal   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1       | 3               | PEN    | 100   | App IBK            | Si         | Si                   | Correo         | Registrado        |
      | CASE02   | 1       | 1               | PEN    | 100   | Bca Telef          | Si         | No                   | Direccion      | Registrado        |
      | CASE03   | 1       | 1               | PEN    | 100   | Bca x internet     | Si         | No                   | Correo         | Registrado        |
      | CASE04   | 1       | 1               | PEN    | 100   | Canal Select       | Si         | No                   | Direccion      | Registrado        |
      | CASE05   | 1       | 1               | PEN    | 100   | Entrega TC         | Si         | No                   | Correo         | Registrado        |
      | CASE06   | 1       | 1               | PEN    | 100   | IBK Agentes        | Si         | No                   | Direccion      | Registrado        |
      | CASE07   | 1       | 1               | PEN    | 100   | Interbank x FB     | Si         | No                   | Correo         | Registrado        |
      | CASE08   | 1       | 1               | PEN    | 100   | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        |
      | CASE09   | 1       | 1               | PEN    | 100   | Otros              | Si         | No                   | Correo         | Registrado        |
      | CASE10   | 1       | 1               | PEN    | 100   | Red CjGnet         | Si         | No                   | Direccion      | Registrado        |
      | CASE11   | 1       | 1               | PEN    | 100   | Red Tiendas        | Si         | No                   | Correo         | Registrado        |
      | CASE12   | 1       | 1               | PEN    | 100   | Televentas         | Si         | No                   | Direccion      | Registrado        |
      | CASE13   | 1       | 1               | PEN    | 100   | Whatsapp           | Si         | No                   | Correo         | Registrado        |


  @happy_path @regresion @version3.1.8 @registroRefinanciado_test19
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto REFINANCIADO para la tipología: NO RECEPCIÓN DE DOCUMENTO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto REFINANCIADO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia NO RECEPCIÓN DE DOCUMENTO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | canal   |
      | <motivos> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1       | App IBK            | Si         | Si                   | Correo         | Registrado        |
      | CASE02   | 1       | Bca Telef          | Si         | No                   | Direccion      | Registrado        |
      | CASE03   | 1       | Bca x internet     | Si         | No                   | Correo         | Registrado        |
      | CASE04   | 1       | Canal Select       | Si         | No                   | Direccion      | Registrado        |
      | CASE05   | 1       | Entrega TC         | Si         | No                   | Correo         | Registrado        |
      | CASE06   | 1       | IBK Agentes        | Si         | No                   | Direccion      | Registrado        |
      | CASE07   | 1       | Interbank x FB     | Si         | No                   | Correo         | Registrado        |
      | CASE08   | 1       | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        |
      | CASE09   | 1       | Otros              | Si         | No                   | Correo         | Registrado        |
      | CASE10   | 1       | Red CjGnet         | Si         | No                   | Direccion      | Registrado        |
      | CASE11   | 1       | Red Tiendas        | Si         | No                   | Correo         | Registrado        |
      | CASE12   | 1       | Televentas         | Si         | No                   | Direccion      | Registrado        |
      | CASE13   | 1       | Whatsapp           | Si         | No                   | Correo         | Registrado        |


# ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO SOLICITUD ------------------------------------------
  #===========================================================================================================


  @happy_path @regresion @version3.1.9 @registroRefinanciado_test20
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo SOLICITUD con producto REFINANCIADO para la tipología: REPROGRAMACIÓN DE DEUDA CON CAMPAÑA
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto REFINANCIADO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia REPROGRAMACIÓN DE DEUDA CON CAMPAÑA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1       | Si         | Si                   | Correo         | Registrado        |


  @happy_path @regresion @version3.1.9 @registroRefinanciado_test21
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo SOLICITUD con producto REFINANCIADO para la tipología: REPROGRAMACIÓN DE DEUDA SIN CAMPAÑA
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto REFINANCIADO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia REPROGRAMACIÓN DE DEUDA SIN CAMPAÑA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1-2     | Si         | Si                   | Direccion      | Registrado        |

