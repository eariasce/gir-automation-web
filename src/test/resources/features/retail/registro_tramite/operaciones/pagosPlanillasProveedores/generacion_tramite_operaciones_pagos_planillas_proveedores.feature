@all_generacion_tramite_pagosPlanillasProveedores
Feature: Registro del trámite Operaciones de PAGOS PLANILLAS/PROVEEDORES

  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO PEDIDO--------------------------------------------
  #===========================================================================================================


  @happy_path @regresion @version3.1.7 @registroPagosPlanillasProveedores_test1
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con operaciones PAGOS PLANILLAS/PROVEEDORES para la tipología: INFORMACION DE MOVIMIENTOS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto PAGOS PLANILLAS/PROVEEDORES
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia INFORMACION DE MOVIMIENTOS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1       | 3               | PEN    | 100   | Si         | Si                   | Correo         | Registrado        |


  @happy_path @regresion @version3.1.8 @registroPagosPlanillasProveedores_test3
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con operaciones PAGOS PLANILLAS/PROVEEDORES para la tipología: ACTIVACIÓN DE ORDENES DE PAGO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto PAGOS PLANILLAS/PROVEEDORES
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia ACTIVACIÓN DE ORDENES DE PAGO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1-2     | Si         | Si                   | Correo         | Registrado        |



  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO RECLAMO-------------------------------------------
  #===========================================================================================================


  @happy_path @regresion @version3.1.7 @registroPagosPlanillasProveedores_test2
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con operaciones PAGOS PLANILLAS/PROVEEDORES para la tipología: TRANSACCIÓN MAL O NO PROCESADA
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto PAGOS PLANILLAS/PROVEEDORES
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia TRANSACCIÓN MAL O NO PROCESADA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | canal   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1-2-3   | 3               | PEN    | 100   | App IBK            | Si         | Si                   | Correo         | Registrado        |
      | CASE02   | 2-3-1   | 3               | PEN    | 100   | Bca Telef          | Si         | No                   | Correo         | Nueva             |
      | CASE03   | 1       | 1               | PEN    | 100   | Bca x internet     | Si         | No                   | Direccion      | Registrado        |
      | CASE04   | 2       | 1               | PEN    | 100   | Canal Select       | Si         | No                   | Direccion      | Nueva             |
      | CASE05   | 3       | 1               | PEN    | 100   | Entrega TC         | Si         | No                   | Correo         | Registrado        |
      | CASE06   | 1       | 1               | PEN    | 100   | IBK Agentes        | Si         | No                   | Correo         | Registrado        |
      | CASE07   | 2       | 1               | PEN    | 100   | Interbank x FB     | Si         | No                   | Direccion      | Registrado        |
      | CASE08   | 3       | 1               | PEN    | 100   | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        |
      | CASE09   | 1       | 1               | PEN    | 100   | Otros              | Si         | No                   | Correo         | Registrado        |
      | CASE10   | 2       | 1               | PEN    | 100   | Red CjGnet         | Si         | No                   | Correo         | Registrado        |
      | CASE11   | 3       | 1               | PEN    | 100   | Red Tiendas        | Si         | No                   | Direccion      | Registrado        |
      | CASE12   | 1       | 1               | PEN    | 100   | Televentas         | Si         | No                   | Direccion      | Registrado        |
      | CASE13   | 2       | 1               | PEN    | 100   | Whatsapp           | Si         | No                   | Correo         | Registrado        |


  @happy_path @regresion @version3.1.8 @registroPagosPlanillasProveedores_test4
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con operaciones PAGOS PLANILLAS/PROVEEDORES para la tipología: COBROS INDEBIDOS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto PAGOS PLANILLAS/PROVEEDORES
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia COBROS INDEBIDOS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | canal   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1-2-3   | 3               | PEN    | 100   | App IBK            | Si         | Si                   | Correo         | Registrado        |
      | CASE02   | 1       | 3               | PEN    | 100   | Bca Telef          | Si         | No                   | Direccion      | Nueva             |
      | CASE03   | 2       | 1               | PEN    | 100   | Bca x internet     | Si         | No                   | Correo         | Registrado        |
      | CASE04   | 3       | 1               | PEN    | 100   | Canal Select       | Si         | No                   | Direccion      | Registrado        |
      | CASE05   | 1       | 1               | PEN    | 100   | Entrega TC         | Si         | No                   | Correo         | Registrado        |
      | CASE06   | 2       | 1               | PEN    | 100   | IBK Agentes        | Si         | No                   | Direccion      | Registrado        |
      | CASE07   | 3       | 1               | PEN    | 100   | Interbank x FB     | Si         | No                   | Correo         | Registrado        |
      | CASE08   | 1       | 1               | PEN    | 100   | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        |
      | CASE09   | 2       | 1               | PEN    | 100   | Otros              | Si         | No                   | Correo         | Registrado        |
      | CASE10   | 3       | 1               | PEN    | 100   | Red CjGnet         | Si         | No                   | Direccion      | Registrado        |
      | CASE11   | 1       | 1               | PEN    | 100   | Red Tiendas        | Si         | No                   | Correo         | Registrado        |
      | CASE12   | 2       | 1               | PEN    | 100   | Televentas         | Si         | No                   | Direccion      | Registrado        |
      | CASE13   | 3       | 1               | PEN    | 100   | Whatsapp           | Si         | No                   | Correo         | Registrado        |





