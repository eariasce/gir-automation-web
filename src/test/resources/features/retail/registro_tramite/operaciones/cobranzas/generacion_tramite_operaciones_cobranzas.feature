@all_generacion_tramite_cobranza
Feature: Registro del trámite Operaciones de COBRANZAS

  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO PEDIDO ------------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.4 @registroCobranza_test1
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con operaciones COBRANZAS para la tipología: CONSTANCIA DE NO ADEUDO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto COBRANZAS
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia CONSTANCIA DE NO ADEUDO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples: #REGISTRA MOTIVO INDIVIDUAL
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1       | Si         | Si                   | Correo         | Registrado        |
      | CASE02   | 2       | Si         | No                   | Direccion      | Nueva             |
      | CASE03   | 3       | Si         | No                   | Correo         | Registrado        |


  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO RECLAMO ------------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.4.1 @registroCobranza_test2
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con operaciones COBRANZAS para la tipología: DIRECCIÓN/TELF. INDEBIDOS DE COBRANZA
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto COBRANZAS
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia DIRECCIÓN/TELF. INDEBIDOS DE COBRANZA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | tipoProducto   | canal   |
      | <motivos> | <tipoProducto> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | tipoProducto       | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1-2-3   | Credito            | App IBK            | Si         | Si                   | Correo         | Registrado        |
      | CASE02   | 1-3-4   | Tarjeta de Credito | Bca Telef          | Si         | Si                   | Correo         | Nueva             |
      | CASE03   | 1-2-3   | Credito            | Bca x internet     | Si         | Si                   | Direccion      | Registrado        |
      | CASE04   | 1-3-4   | Tarjeta de Credito | Canal Select       | Si         | Si                   | Direccion      | Nueva             |
      | CASE05   | 1-2-3   | Credito            | Entrega TC         | Si         | Si                   | Correo         | Registrado        |
      | CASE06   | 1-3-4   | Tarjeta de Credito | IBK Agentes        | Si         | Si                   | Correo         | Registrado        |
      | CASE07   | 1-2-3   | Credito            | Interbank x FB     | Si         | Si                   | Direccion      | Registrado        |
      | CASE08   | 1-3-4   | Tarjeta de Credito | Módulo de cobranza | Si         | Si                   | Direccion      | Registrado        |
      | CASE09   | 1-2-3   | Credito            | Otros              | Si         | Si                   | Correo         | Registrado        |
      | CASE10   | 1-3-4   | Tarjeta de Credito | Red CjGnet         | Si         | Si                   | Correo         | Registrado        |
      | CASE11   | 1-2-3   | Credito            | Red Tiendas        | Si         | Si                   | Direccion      | Registrado        |
      | CASE12   | 1-3-4   | Tarjeta de Credito | Televentas         | Si         | Si                   | Direccion      | Registrado        |
      | CASE13   | 1-3-4   | Credito            | Whatsapp           | Si         | Si                   | Correo         | Registrado        |


  @happy_path @regresion @version3.1.4.1 @registroCobranza_test3
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con operaciones COBRANZAS para la tipología: COBROS INDEBIDOS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto COBRANZAS
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia COBROS INDEBIDOS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | tipoProducto   | canal   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <tipoProducto> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | tipoProducto       | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1-2-3   | 3               | PEN    | 100   | Crédito            | App IBK            | Si         | Si                   | Correo         | Registrado        |
      | CASE02   | 1-2-4   | 3               | PEN    | 200   | Cuenta             | Bca Telef          | Si         | No                   | Correo         | Nueva             |
      | CASE03   | 1       | 1               | PEN    | 300   | Tarjeta de crédito | Bca x internet     | Si         | No                   | Direccion      | Registrado        |
      | CASE04   | 2       | 1               | PEN    | 400   | Crédito            | Canal Select       | Si         | No                   | Direccion      | Nueva             |
      | CASE05   | 3       | 1               | PEN    | 100   | Cuenta             | Entrega TC         | Si         | No                   | Correo         | Registrado        |
      | CASE06   | 4       | 1               | PEN    | 200   | Tarjeta de crédito | IBK Agentes        | Si         | No                   | Correo         | Registrado        |
      | CASE07   | 1       | 1               | PEN    | 300   | Crédito            | Interbank x FB     | Si         | No                   | Direccion      | Registrado        |
      | CASE08   | 2       | 1               | PEN    | 400   | Cuenta             | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        |
      | CASE09   | 3       | 1               | PEN    | 100   | Tarjeta de crédito | Otros              | Si         | No                   | Correo         | Registrado        |
      | CASE10   | 4       | 1               | PEN    | 200   | Crédito            | Red CjGnet         | Si         | No                   | Correo         | Registrado        |
      | CASE11   | 1       | 1               | PEN    | 300   | Cuenta             | Red Tiendas        | Si         | No                   | Direccion      | Registrado        |
      | CASE12   | 2       | 1               | PEN    | 400   | Tarjeta de crédito | Televentas         | Si         | No                   | Direccion      | Registrado        |
      | CASE13   | 3       | 1               | PEN    | 100   | Crédito            | Whatsapp           | Si         | No                   | Correo         | Registrado        |


  @happy_path @regresion @version3.1.7 @registroCobranza_test4
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con operaciones COBRANZAS para la tipología: REPORTE INDEBIDO CENTRALES DE RIESGO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto COBRANZAS
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia REPORTE INDEBIDO CENTRALES DE RIESGO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | tipoProducto   | canal   |
      | <motivos> | <tipoProducto> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | tipoProducto       | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1       | Crédito            | App IBK            | Si         | Si                   | Correo         | Registrado        |
      | CASE02   | 1       | Cuenta             | Bca Telef          | Si         | Si                   | Correo         | Nueva             |
      | CASE03   | 1       | Tarjeta de crédito | Bca x internet     | Si         | Si                   | Direccion      | Registrado        |
      | CASE04   | 1       | Crédito            | Canal Select       | Si         | Si                   | Correo         | Nueva             |
      | CASE05   | 1       | Cuenta             | Entrega TC         | Si         | Si                   | Correo         | Registrado        |
      | CASE06   | 1       | Tarjeta de crédito | IBK Agentes        | Si         | Si                   | Correo         | Registrado        |
      | CASE07   | 1       | Crédito            | Interbank x FB     | Si         | Si                   | Correo         | Registrado        |
      | CASE08   | 1       | Cuenta             | Módulo de cobranza | Si         | Si                   | Correo         | Registrado        |
      | CASE09   | 1       | Tarjeta de crédito | Otros              | Si         | Si                   | Correo         | Registrado        |
      | CASE10   | 1       | Crédito            | Red CjGnet         | Si         | Si                   | Correo         | Registrado        |
      | CASE11   | 1       | Cuenta             | Red Tiendas        | Si         | Si                   | Correo         | Registrado        |
      | CASE12   | 1       | Tarjeta de crédito | Televentas         | Si         | Si                   | Correo         | Registrado        |
      | CASE13   | 1       | Crédito            | Whatsapp           | Si         | Si                   | Correo         | Registrado        |



  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO SOLICITUD-----------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.7 @registroCobranza_test5
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo SOLICITUD con operaciones COBRANZAS para la tipología: RECTIFICACIÓN SBS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto COBRANZAS
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia RECTIFICACIÓN SBS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "No" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos |  comentario | indicaArchivoAdjunto |
      | CASE01   | 1       |  Si         | Si                   |
