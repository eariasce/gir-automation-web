@all_generacion_tramite_trasladosCts
Feature: Registro del trámite Operaciones de TRASLADOS CTS

  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO SOLICITUD-----------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.8 @registroTrasladosCts_test1
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo SOLICITUD con operaciones TRASLADOS CTS para la tipología: TRASLADO DE CTS DIGITAL A INTERBANK
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TRASLADOS CTS
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia TRASLADO DE CTS DIGITAL A INTERBANK
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | cuentaSueldo   | salaryAccountCreation   |
      | <motivos> | <cuentaSueldo> | <salaryAccountCreation> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cuentaSueldo | salaryAccountCreation | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1-2     | Si           | No                    | Si         | Si                   | Correo         | Registrado        |
      | CASE02   | 1       | No           | Si                    | Si         | No                   | Direccion      | Registrado        |


  @happy_path @regresion @version3.1.8 @registroTrasladosCts_test2
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo SOLICITUD con operaciones TRASLADOS CTS para la tipología: TRASLADO DE CTS FÍSICO A INTERBANK
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TRASLADOS CTS
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia TRASLADO DE CTS FÍSICO A INTERBANK
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | cuentaSueldo   | salaryAccountCreation   |
      | <motivos> | <cuentaSueldo> | <salaryAccountCreation> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cuentaSueldo | salaryAccountCreation | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1-2     | Si           | No                    | Si         | Si                   | Correo         | Registrado        |
      | CASE02   | 1       | No           | Si                    | Si         | No                   | Direccion      | Registrado        |

