@all_generacion_tramite_bancaCelular
Feature: Registro del trámite Operaciones de BANCA CELULAR

  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO RECLAMO ------------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.6 @registroBancaCelular_test2
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con operaciones BANCA CELULAR para la tipología: TRANSACCIÓN MAL O NO PROCESADA
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto BANCA CELULAR
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia TRANSACCIÓN MAL O NO PROCESADA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | canal   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1       | 5               | PEN    | 100   | App IBK            | Si         | Si                   | Correo         | Registrado        |
      | CASE02   | 1       | 1               | PEN    | 100   | Bca Telef          | Si         | No                   | Correo         | Nueva             |
      | CASE03   | 1       | 1               | PEN    | 300   | Bca x internet     | Si         | No                   | Direccion      | Registrado        |
      | CASE04   | 1       | 1               | PEN    | 400   | Canal Select       | Si         | No                   | Direccion      | Nueva             |
      | CASE05   | 1       | 1               | PEN    | 500   | Entrega TC         | Si         | No                   | Correo         | Registrado        |
      | CASE06   | 1       | 1               | PEN    | 100   | IBK Agentes        | Si         | No                   | Correo         | Registrado        |
      | CASE07   | 1       | 1               | PEN    | 200   | Interbank x FB     | Si         | No                   | Direccion      | Registrado        |
      | CASE08   | 1       | 1               | PEN    | 300   | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        |
      | CASE09   | 1       | 1               | PEN    | 400   | Otros              | Si         | No                   | Correo         | Registrado        |
      | CASE10   | 1       | 1               | PEN    | 500   | Red CjGnet         | Si         | No                   | Correo         | Registrado        |
      | CASE11   | 1       | 1               | PEN    | 600   | Red Tiendas        | Si         | No                   | Direccion      | Registrado        |
      | CASE12   | 1       | 1               | PEN    | 700   | Televentas         | Si         | No                   | Direccion      | Registrado        |
      | CASE13   | 1       | 1               | PEN    | 800   | Whatsapp           | Si         | No                   | Correo         | Registrado        |

 # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO SOLICITUD ------------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.8 @registroBancaCelular_test3
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo SOLICITUD con operaciones BANCA CELULAR para la tipología: DESAFILIACIÓN/ACTUALIZACIÓN DE NÚMERO CELULAR
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto BANCA CELULAR
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia DESAFILIACIÓN/ACTUALIZACIÓN DE NÚMERO CELULAR
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples: #ADJUNTAR ARCHIVO PARA QUE SALGA ASIGNAR - ES OPCIONAL
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1-2     | Si         | Si                   | Correo         | Registrado        |
      | CASE02   | 1-2     | Si         | No                   | Correo         | Registrado        |



