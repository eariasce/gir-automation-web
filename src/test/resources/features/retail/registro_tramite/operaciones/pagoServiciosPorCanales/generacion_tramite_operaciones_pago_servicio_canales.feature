@registro1
@all_generacion_tramite_servicioCanales
Feature: Registro del trámite Operaciones de PAGO DE SERVICIO POR CANALES

  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO RECLAMO ------------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.4.1 @registroServicioCanales_test1
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con operaciones PAGO DE SERVICIO POR CANALES para la tipología: TRANSACCIÓN MAL O NO PROCESADA
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto PAGO DE SERVICIO POR CANALES
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia TRANSACCIÓN MAL O NO PROCESADA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | formaDePago   | canal   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <formaDePago> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | formaDePago        | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1-2-3   | 3               | PEN    | 100   | Cuenta             | App IBK            | Si         | Si                   | Correo         | Registrado        |
      | CASE02   | 4-5-6   | 3               | PEN    | 200   | Efectivo           | Bca Telef          | Si         | No                   | Correo         | Nueva             |
      | CASE03   | 1       | 1               | PEN    | 300   | Tarjeta de Credito | Bca x internet     | Si         | No                   | Direccion      | Registrado        |
      | CASE04   | 2       | 1               | PEN    | 400   | Cuenta             | Canal Select       | Si         | No                   | Direccion      | Nueva             |
      | CASE05   | 3       | 1               | PEN    | 500   | Efectivo           | Entrega TC         | Si         | No                   | Correo         | Registrado        |
      | CASE06   | 4       | 1               | PEN    | 100   | Tarjeta de Credito | IBK Agentes        | Si         | No                   | Correo         | Registrado        |
      | CASE07   | 5       | 1               | PEN    | 200   | Cuenta             | Interbank x FB     | Si         | No                   | Direccion      | Registrado        |
      | CASE08   | 6       | 1               | PEN    | 300   | Efectivo           | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        |
      | CASE09   | 1       | 1               | PEN    | 400   | Tarjeta de Credito | Otros              | Si         | No                   | Correo         | Registrado        |
      | CASE10   | 2       | 1               | PEN    | 500   | Cuenta             | Red CjGnet         | Si         | No                   | Correo         | Registrado        |
      | CASE11   | 3       | 1               | PEN    | 600   | Efectivo           | Red Tiendas        | Si         | No                   | Direccion      | Registrado        |
      | CASE12   | 4       | 1               | PEN    | 700   | Tarjeta de Credito | Televentas         | Si         | No                   | Direccion      | Registrado        |
      | CASE13   | 5       | 1               | PEN    | 800   | Cuenta             | Whatsapp           | Si         | No                   | Correo         | Registrado        |


  @happy_path @regresion @version3.1.4.1 @registroServicioCanales_test2
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con operaciones PAGO DE SERVICIO POR CANALES para la tipología: ATM DE IB NO DEPOSITÓ DINERO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto PAGO DE SERVICIO POR CANALES
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia ATM DE IB NO DEPOSITÓ DINERO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | canal   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1-2-3   | 3               | PEN    | 100   | App IBK            | Si         | Si                   | Correo         | Registrado        |
      | CASE02   | 4-1-2   | 3               | PEN    | 200   | Bca Telef          | Si         | No                   | Correo         | Nueva             |
      | CASE03   | 1       | 1               | PEN    | 300   | Bca x internet     | Si         | No                   | Direccion      | Registrado        |
      | CASE04   | 2       | 1               | PEN    | 400   | Canal Select       | Si         | No                   | Direccion      | Nueva             |
      | CASE05   | 3       | 1               | PEN    | 500   | Entrega TC         | Si         | No                   | Correo         | Registrado        |
      | CASE06   | 4       | 1               | PEN    | 100   | IBK Agentes        | Si         | No                   | Correo         | Registrado        |
      | CASE07   | 1       | 1               | PEN    | 200   | Interbank x FB     | Si         | No                   | Correo         | Registrado        |
      | CASE08   | 2       | 1               | PEN    | 300   | Módulo de cobranza | Si         | No                   | Correo         | Registrado        |
      | CASE09   | 3       | 1               | PEN    | 400   | Otros              | Si         | No                   | Correo         | Registrado        |
      | CASE10   | 4       | 1               | PEN    | 500   | Red CjGnet         | Si         | No                   | Correo         | Registrado        |
      | CASE11   | 1       | 1               | PEN    | 600   | Red Tiendas        | Si         | No                   | Correo         | Registrado        |
      | CASE12   | 2       | 1               | PEN    | 700   | Televentas         | Si         | No                   | Correo         | Registrado        |
      | CASE13   | 3       | 1               | PEN    | 800   | Whatsapp           | Si         | No                   | Correo         | Registrado        |



  @happy_path @regresion @version3.1.8 @registroServicioCanales_test5
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con operaciones PAGO DE SERVICIO POR CANALES para la tipología: COBROS INDEBIDOS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto PAGO DE SERVICIO POR CANALES
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia COBROS INDEBIDOS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | formaDePago   | canal   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <formaDePago> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | formaDePago        | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1-2-3   | 3               | PEN    | 100   | Cuenta             | App IBK            | Si         | Si                   | Correo         | Registrado        |
      | CASE02   | 2-5-4   | 3               | PEN    | 100   | Efectivo           | Bca Telef          | Si         | No                   | Direccion      | Registrado        |
      | CASE03   | 1       | 1               | PEN    | 100   | Tarjeta de Credito | Bca x internet     | Si         | No                   | Correo         | Registrado        |
      | CASE04   | 2       | 1               | PEN    | 100   | Cuenta             | Canal Select       | Si         | No                   | Direccion      | Registrado        |
      | CASE05   | 3       | 1               | PEN    | 100   | Efectivo           | Entrega TC         | Si         | No                   | Correo         | Registrado        |
      | CASE06   | 4       | 1               | PEN    | 100   | Tarjeta de Credito | IBK Agentes        | Si         | No                   | Direccion      | Registrado        |
      | CASE07   | 5       | 1               | PEN    | 100   | Cuenta             | Interbank x FB     | Si         | No                   | Correo         | Registrado        |
      | CASE08   | 1       | 1               | PEN    | 100   | Efectivo           | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        |
      | CASE09   | 2       | 1               | PEN    | 100   | Tarjeta de Credito | Otros              | Si         | No                   | Correo         | Registrado        |
      | CASE10   | 3       | 1               | PEN    | 100   | Cuenta             | Red CjGnet         | Si         | No                   | Direccion      | Registrado        |
      | CASE11   | 4       | 1               | PEN    | 100   | Efectivo           | Red Tiendas        | Si         | No                   | Correo         | Registrado        |
      | CASE12   | 5       | 1               | PEN    | 100   | Tarjeta de Credito | Televentas         | Si         | No                   | Direccion      | Registrado        |
      | CASE13   | 1       | 1               | PEN    | 100   | Cuenta             | Whatsapp           | Si         | No                   | Correo         | Registrado        |


# ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO PEDIDO ------------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.7 @registroServicioCanales_test3
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con operaciones PAGO DE SERVICIO POR CANALES para la tipología: CORRECCIÓN DE PAGO REALIZADO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto PAGO DE SERVICIO POR CANALES
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia CORRECCIÓN DE PAGO REALIZADO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1-2-3   | 5               | PEN    | 100   | Si         | Si                   | Correo         | Registrado        |


  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO SOLICITUD-----------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.7 @registroServicioCanales_test4
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo SOLICITUD con operaciones PAGO DE SERVICIO POR CANALES para la tipología: ATENCIÓN IBK AGENTE
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto PAGO DE SERVICIO POR CANALES
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia ATENCIÓN IBK AGENTE
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1-2-3   | 5               | PEN    | 100   | Si         | Si                   | Correo         | Registrado        |
      | CASE02   | 3-4-5   | 3               | PEN    | 200   | Si         | No                   | Correo         | Nueva             |
