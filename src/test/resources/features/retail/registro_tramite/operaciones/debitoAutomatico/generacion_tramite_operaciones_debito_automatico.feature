@all_generacion_tramite_debitoAutomatico
Feature: Registro del trámite Operaciones de DÉBITO AUTOMÁTICO

  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO RECLAMO ------------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.8 @registroDebitoAutomatico_test2
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con operaciones DEBITO AUTOMATICO para la tipología: COBROS INDEBIDOS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto DEBITO AUTOMATICO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia COBROS INDEBIDOS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | formaDePago   | canal   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <formaDePago> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | formaDePago        | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1-2-3   | 3               | PEN    | 100   | Cuenta             | App IBK            | Si         | Si                   | Correo         | Registrado        |
      | CASE02   | 5-4     | 3               | PEN    | 200   | Efectivo           | Bca Telef          | Si         | No                   | Direccion      | Nueva             |
      | CASE03   | 1       | 1               | PEN    | 300   | Tarjeta de Credito | Bca x internet     | Si         | No                   | Correo         | Registrado        |
      | CASE04   | 2       | 1               | PEN    | 400   | Cuenta             | Canal Select       | Si         | No                   | Direccion      | Registrado        |
      | CASE05   | 3       | 1               | PEN    | 500   | Efectivo           | Entrega TC         | Si         | No                   | Correo         | Registrado        |
      | CASE06   | 4       | 1               | PEN    | 100   | Tarjeta de Credito | IBK Agentes        | Si         | No                   | Direccion      | Registrado        |
      | CASE07   | 5       | 1               | PEN    | 200   | Cuenta             | Interbank x FB     | Si         | No                   | Correo         | Registrado        |
      | CASE08   | 6       | 1               | PEN    | 300   | Efectivo           | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        |
      | CASE09   | 1       | 1               | PEN    | 400   | Tarjeta de Credito | Otros              | Si         | No                   | Correo         | Registrado        |
      | CASE10   | 2       | 1               | PEN    | 500   | Cuenta             | Red CjGnet         | Si         | No                   | Direccion      | Registrado        |
      | CASE11   | 3       | 1               | PEN    | 600   | Efectivo           | Red Tiendas        | Si         | No                   | Correo         | Registrado        |
      | CASE12   | 4       | 1               | PEN    | 700   | Tarjeta de Credito | Televentas         | Si         | No                   | Direccion      | Registrado        |
      | CASE13   | 5       | 1               | PEN    | 800   | Cuenta             | Whatsapp           | Si         | No                   | Correo         | Registrado        |


  @happy_path @regresion @version3.1.4.1 @registroDebitoAutomatico_test1
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con operaciones DEBITO AUTOMATICO para la tipología: TRANSACCIÓN MAL O NO PROCESADA
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto DEBITO AUTOMATICO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia TRANSACCIÓN MAL O NO PROCESADA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | formaDePago   | canal   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <formaDePago> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | formaDePago        | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1-2-3   | 3               | PEN    | 100   | Cuenta             | App IBK            | Si         | Si                   | Correo         | Registrado        |
      | CASE02   | 4-5-6   | 3               | PEN    | 200   | Efectivo           | Bca Telef          | Si         | No                   | Correo         | Nueva             |
      | CASE03   | 1       | 1               | PEN    | 300   | Tarjeta de Credito | Bca x internet     | Si         | No                   | Direccion      | Registrado        |
      | CASE04   | 2       | 1               | PEN    | 400   | Cuenta             | Canal Select       | Si         | No                   | Direccion      | Nueva             |
      | CASE05   | 3       | 1               | PEN    | 500   | Efectivo           | Entrega TC         | Si         | No                   | Correo         | Registrado        |
      | CASE06   | 4       | 1               | PEN    | 100   | Tarjeta de Credito | IBK Agentes        | Si         | No                   | Correo         | Registrado        |
      | CASE07   | 5       | 1               | PEN    | 200   | Cuenta             | Interbank x FB     | Si         | No                   | Correo         | Registrado        |
      | CASE08   | 6       | 1               | PEN    | 300   | Efectivo           | Módulo de cobranza | Si         | No                   | Correo         | Registrado        |
      | CASE09   | 1       | 1               | PEN    | 400   | Tarjeta de Credito | Otros              | Si         | No                   | Correo         | Registrado        |
      | CASE10   | 2       | 1               | PEN    | 500   | Cuenta             | Red CjGnet         | Si         | No                   | Correo         | Registrado        |
      | CASE11   | 3       | 1               | PEN    | 600   | Efectivo           | Red Tiendas        | Si         | No                   | Correo         | Registrado        |
      | CASE12   | 4       | 1               | PEN    | 700   | Tarjeta de Credito | Televentas         | Si         | No                   | Correo         | Registrado        |
      | CASE13   | 5       | 1               | PEN    | 800   | Cuenta             | Whatsapp           | Si         | No                   | Correo         | Registrado        |

