@all_generacion_tramite_token
Feature: Registro del trámite Operaciones de TOKEN

  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO RECLAMO ------------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.6 @registroToken_test1
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con operaciones TOKEN para la tipología: TRANSACCIÓN MAL O NO PROCESADA
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TOKEN
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia TRANSACCIÓN MAL O NO PROCESADA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | canal   |
      | <motivos> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1       | App IBK            | Si         | Si                   | Correo         | Registrado        |
      | CASE02   | 1       | Bca Telef          | Si         | No                   | Correo         | Nueva             |
      | CASE03   | 1       | Bca x internet     | Si         | No                   | Direccion      | Registrado        |
      | CASE04   | 1       | Canal Select       | Si         | No                   | Direccion      | Nueva             |
      | CASE05   | 1       | Entrega TC         | Si         | No                   | Correo         | Registrado        |
      | CASE06   | 1       | IBK Agentes        | Si         | No                   | Correo         | Registrado        |
      | CASE07   | 1       | Interbank x FB     | Si         | No                   | Direccion      | Registrado        |
      | CASE08   | 1       | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        |
      | CASE09   | 1       | Otros              | Si         | No                   | Correo         | Registrado        |
      | CASE10   | 1       | Red CjGnet         | Si         | No                   | Correo         | Registrado        |
      | CASE11   | 1       | Red Tiendas        | Si         | No                   | Direccion      | Registrado        |
      | CASE12   | 1       | Televentas         | Si         | No                   | Direccion      | Registrado        |
      | CASE13   | 1       | Whatsapp           | Si         | No                   | Correo         | Registrado        |

