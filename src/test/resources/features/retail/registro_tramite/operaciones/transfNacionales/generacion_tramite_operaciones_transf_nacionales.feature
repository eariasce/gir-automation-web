@registro2
@all_generacion_tramite_transfNacionales
Feature: Registro del trámite Operaciones de TRANSF. NACIONALES

  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO RECLAMO ------------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.4.1 @registroTransfNacionales_test1
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con operaciones TRANSF. NACIONALES para la tipología: TRANSACCIÓN MAL O NO PROCESADA
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TRANSF. NACIONALES
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia TRANSACCIÓN MAL O NO PROCESADA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | tipoTransferencia   | canal   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <tipoTransferencia> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | tipoTransferencia | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1-2-3   | 3               | PEN    | 100   | Entrada           | App IBK            | Si         | Si                   | Correo         | Registrado        |
      | CASE02   | 3-4-5   | 3               | PEN    | 100   | Salida            | Bca Telef          | Si         | No                   | Direccion      | Nueva             |
      | CASE03   | 1       | 1               | PEN    | 100   | Entrada           | Bca x internet     | Si         | No                   | Correo         | Registrado        |
      | CASE04   | 2       | 1               | PEN    | 100   | Salida            | Canal Select       | Si         | No                   | Direccion      | Registrado        |
      | CASE05   | 3       | 1               | PEN    | 100   | Entrada           | Entrega TC         | Si         | No                   | Correo         | Registrado        |
      | CASE06   | 4       | 1               | PEN    | 100   | Salida            | IBK Agentes        | Si         | No                   | Direccion      | Registrado        |
      | CASE07   | 5       | 1               | PEN    | 100   | Entrada           | Interbank x FB     | Si         | No                   | Correo         | Registrado        |
      | CASE08   | 1       | 1               | PEN    | 100   | Salida            | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        |
      | CASE09   | 2       | 1               | PEN    | 100   | Entrada           | Otros              | Si         | No                   | Correo         | Registrado        |
      | CASE10   | 3       | 1               | PEN    | 100   | Salida            | Red CjGnet         | Si         | No                   | Direccion      | Registrado        |
      | CASE11   | 4       | 1               | PEN    | 100   | Entrada           | Red Tiendas        | Si         | No                   | Correo         | Registrado        |
      | CASE12   | 5       | 1               | PEN    | 100   | Salida            | Televentas         | Si         | No                   | Direccion      | Registrado        |
      | CASE13   | 1       | 1               | PEN    | 100   | Entrada           | Whatsapp           | Si         | No                   | Correo         | Registrado        |

  @happy_path @regresion @version3.1.8 @registroTransfNacionales_test2
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con operaciones TRANSF. NACIONALES para la tipología: COBROS INDEBIDOS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TRANSF. NACIONALES
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia COBROS INDEBIDOS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | tipoTransferencia   | canal   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <tipoTransferencia> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | tipoTransferencia | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1-2     | 3               | PEN    | 100   | Entrada           | App IBK            | Si         | Si                   | Correo         | Registrado        |
      | CASE02   | 1       | 1               | PEN    | 100   | Salida            | Bca Telef          | Si         | No                   | Direccion      | Registrado        |
      | CASE03   | 2       | 1               | PEN    | 100   | Entrada           | Bca x internet     | Si         | No                   | Correo         | Registrado        |
      | CASE04   | 1       | 1               | PEN    | 100   | Salida            | Canal Select       | Si         | No                   | Direccion      | Registrado        |
      | CASE05   | 2       | 1               | PEN    | 100   | Entrada           | Entrega TC         | Si         | No                   | Correo         | Registrado        |
      | CASE06   | 1       | 1               | PEN    | 100   | Salida            | IBK Agentes        | Si         | No                   | Direccion      | Registrado        |
      | CASE07   | 2       | 1               | PEN    | 100   | Entrada           | Interbank x FB     | Si         | No                   | Correo         | Registrado        |
      | CASE08   | 1       | 1               | PEN    | 100   | Salida            | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        |
      | CASE09   | 2       | 1               | PEN    | 100   | Entrada           | Otros              | Si         | No                   | Correo         | Registrado        |
      | CASE10   | 1       | 1               | PEN    | 100   | Salida            | Red CjGnet         | Si         | No                   | Direccion      | Registrado        |
      | CASE11   | 2       | 1               | PEN    | 100   | Entrada           | Red Tiendas        | Si         | No                   | Correo         | Registrado        |
      | CASE12   | 1       | 1               | PEN    | 100   | Salida            | Televentas         | Si         | No                   | Direccion      | Registrado        |
      | CASE13   | 2       | 1               | PEN    | 100   | Entrada           | Whatsapp           | Si         | No                   | Correo         | Registrado        |


