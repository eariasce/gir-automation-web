@all_generacion_tramite_atencionCliente
Feature: Registro del trámite ATENCION AL CLIENTE

  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO RECLAMO ------------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.4.1 @registroAtencionCliente_test1 ##En stg seccion no activa - ESTA TIPOLOGIA ESTA DESPLEGADO EN V318
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto ATENCION AL CLIENTE para la tipología: DESACUERDO CON EL SERVICIO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto ATENCION AL CLIENTE
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia DESACUERDO CON EL SERVICIO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | canal   |
      | <motivos> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1       | App IBK            | Si         | Si                   | Correo         | Registrado        |
      | CASE02   | 1       | Bca Telef          | Si         | No                   | Correo         | Registrado        |
      | CASE03   | 1       | Bca x internet     | Si         | No                   | Direccion      | Registrado        |
      | CASE04   | 1       | Canal Select       | Si         | No                   | Direccion      | Registrado        |
      | CASE05   | 1       | Entrega TC         | Si         | No                   | Correo         | Registrado        |
      | CASE06   | 1       | IBK Agentes        | Si         | No                   | Correo         | Registrado        |
      | CASE07   | 1       | Interbank x FB     | Si         | No                   | Direccion      | Registrado        |
      | CASE08   | 1       | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        |
      | CASE09   | 1       | Otros              | Si         | No                   | Correo         | Registrado        |
      | CASE10   | 1       | Red CjGnet         | Si         | No                   | Correo         | Registrado        |
      | CASE11   | 1       | Red Tiendas        | Si         | No                   | Direccion      | Registrado        |
      | CASE12   | 1       | Televentas         | Si         | No                   | Direccion      | Registrado        |
      | CASE13   | 1       | Whatsapp           | Si         | No                   | Correo         | Registrado        |


  @happy_path @regresion @version3.1.7 @registroAtencionCliente_test4
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto ATENCION AL CLIENTE para la tipología: BILLETE FALSO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto ATENCION AL CLIENTE
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia BILLETE FALSO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | canal   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1-2     | 5               | PEN    | 100   | App IBK            | Si         | Si                   | Direccion      | Registrado        |
      | CASE02   | 2-1     | 1               | PEN    | 100   | Bca Telef          | Si         | Si                   | Direccion      | Registrado        |
      | CASE03   | 1       | 1               | PEN    | 300   | Bca x internet     | Si         | No                   | Direccion      | Registrado        |
      | CASE04   | 2       | 1               | PEN    | 400   | Canal Select       | Si         | No                   | Direccion      | Registrado        |
      | CASE05   | 1       | 1               | PEN    | 500   | Entrega TC         | Si         | No                   | Correo         | Registrado        |
      | CASE06   | 2       | 1               | PEN    | 100   | IBK Agentes        | Si         | No                   | Correo         | Registrado        |
      | CASE07   | 1       | 1               | PEN    | 200   | Interbank x FB     | Si         | No                   | Direccion      | Registrado        |
      | CASE08   | 2       | 1               | PEN    | 300   | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        |
      | CASE09   | 1       | 1               | PEN    | 400   | Otros              | Si         | No                   | Correo         | Registrado        |
      | CASE10   | 2       | 1               | PEN    | 500   | Red CjGnet         | Si         | No                   | Correo         | Registrado        |
      | CASE11   | 1       | 1               | PEN    | 600   | Red Tiendas        | Si         | No                   | Direccion      | Registrado        |
      | CASE12   | 2       | 1               | PEN    | 700   | Televentas         | Si         | No                   | Direccion      | Registrado        |
      | CASE13   | 1       | 1               | PEN    | 800   | Whatsapp           | Si         | No                   | Correo         | Registrado        |


  @happy_path @regresion @version3.1.8 @registroAtencionCliente_test11
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto ATENCION AL CLIENTE para la tipología: DESACUERDO CON EL SERVICIO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto ATENCION AL CLIENTE
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia DESACUERDO CON EL SERVICIO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | canal   |
      | <motivos> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1       | App IBK            | Si         | Si                   | Correo         | Registrado        |
      | CASE02   | 1       | Bca Telef          | Si         | No                   | Direccion      | Registrado        |
      | CASE03   | 1       | Bca x internet     | Si         | No                   | Correo         | Registrado        |
      | CASE04   | 1       | Canal Select       | Si         | No                   | Direccion      | Registrado        |
      | CASE05   | 1       | Entrega TC         | Si         | No                   | Correo         | Registrado        |
      | CASE06   | 1       | IBK Agentes        | Si         | No                   | Direccion      | Registrado        |
      | CASE07   | 1       | Interbank x FB     | Si         | No                   | Correo         | Registrado        |
      | CASE08   | 1       | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        |
      | CASE09   | 1       | Otros              | Si         | No                   | Correo         | Registrado        |
      | CASE10   | 1       | Red CjGnet         | Si         | No                   | Direccion      | Registrado        |
      | CASE11   | 1       | Red Tiendas        | Si         | No                   | Correo         | Registrado        |
      | CASE12   | 1       | Televentas         | Si         | No                   | Direccion      | Registrado        |
      | CASE13   | 1       | Whatsapp           | Si         | No                   | Correo         | Registrado        |


  @happy_path @regresion @version3.1.8 @registroAtencionCliente_test9
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto ATENCION AL CLIENTE para la tipología: CORREO/TELÉFONO INDEBIDO DE CAMPAÑAS
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto ATENCION AL CLIENTE
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia CORREO/TELÉFONO INDEBIDO DE CAMPAÑAS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | canal   |
      | <motivos> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1-2-3   | App IBK            | Si         | Si                   | Correo         | Registrado        |
      | CASE02   | 2-3-4   | Bca Telef          | Si         | No                   | Direccion      | Registrado        |
      | CASE03   | 1       | Bca x internet     | Si         | No                   | Correo         | Registrado        |
      | CASE04   | 2       | Canal Select       | Si         | No                   | Direccion      | Registrado        |
      | CASE05   | 3       | Entrega TC         | Si         | No                   | Correo         | Registrado        |
      | CASE06   | 4       | IBK Agentes        | Si         | No                   | Direccion      | Registrado        |
      | CASE07   | 1       | Interbank x FB     | Si         | No                   | Correo         | Registrado        |
      | CASE08   | 2       | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        |
      | CASE09   | 3       | Otros              | Si         | No                   | Correo         | Registrado        |
      | CASE10   | 4       | Red CjGnet         | Si         | No                   | Direccion      | Registrado        |
      | CASE11   | 1       | Red Tiendas        | Si         | No                   | Correo         | Registrado        |
      | CASE12   | 2       | Televentas         | Si         | No                   | Direccion      | Registrado        |
      | CASE13   | 3       | Whatsapp           | Si         | No                   | Correo         | Registrado        |


  @happy_path @regresion @version3.1.8 @registroAtencionCliente_test10
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo RECLAMO con producto ATENCION AL CLIENTE para la tipología: INSATISFACCIÓN GESTIÓN DE ENTREGA DE TARJETA
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto ATENCION AL CLIENTE
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia INSATISFACCIÓN GESTIÓN DE ENTREGA DE TARJETA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | canal   |
      | <motivos> | <canal> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | canal              | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1-2     | App IBK            | Si         | Si                   | Correo         | Registrado        |
      | CASE02   | 1       | Bca Telef          | Si         | No                   | Direccion      | Registrado        |
      | CASE03   | 2       | Bca x internet     | Si         | No                   | Correo         | Registrado        |
      | CASE04   | 1       | Canal Select       | Si         | No                   | Direccion      | Registrado        |
      | CASE05   | 2       | Entrega TC         | Si         | No                   | Correo         | Registrado        |
      | CASE06   | 1       | IBK Agentes        | Si         | No                   | Direccion      | Registrado        |
      | CASE07   | 2       | Interbank x FB     | Si         | No                   | Correo         | Registrado        |
      | CASE08   | 1       | Módulo de cobranza | Si         | No                   | Direccion      | Registrado        |
      | CASE09   | 2       | Otros              | Si         | No                   | Correo         | Registrado        |
      | CASE10   | 1       | Red CjGnet         | Si         | No                   | Direccion      | Registrado        |
      | CASE11   | 2       | Red Tiendas        | Si         | No                   | Correo         | Registrado        |
      | CASE12   | 1       | Televentas         | Si         | No                   | Direccion      | Registrado        |
      | CASE13   | 2       | Whatsapp           | Si         | No                   | Correo         | Registrado        |

      # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO PEDIDO ------------------------------------------
  #===========================================================================================================


  @happy_path @regresion @version3.1.7 @registroAtencionCliente_test1
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto ATENCION AL CLIENTE para la tipología: SOLICITUD DE INTÉRPRETE PARA USUARIO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto ATENCION AL CLIENTE
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia SOLICITUD DE INTÉRPRETE PARA USUARIO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1       | Si         | Si                   | Correo         | Registrado        |


  @happy_path @regresion @version3.1.7 @registroAtencionCliente_test2
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto ATENCION AL CLIENTE para la tipología: MODIFICACIÓN DATOS PERSONALES
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto ATENCION AL CLIENTE
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia MODIFICACIÓN DATOS PERSONALES
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1-2     | Si         | Si                   | Correo         | Registrado        |


  @happy_path @regresion @version3.1.7 @registroAtencionCliente_test3
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo PEDIDO con producto ATENCION AL CLIENTE para la tipología: VIDEO ARCO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto ATENCION AL CLIENTE
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia VIDEO ARCO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples: #*ADJUNTAR DOCUMENTOS OBLIGATORIOS - EN TAL CASO GIRU NO DEBERIA PERMITIR REGISTRO
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1-2     | Si         | Si                   | Correo         | Registrado        |


  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- REGISTRO SOLICITUD ------------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.7 @registroAtencionCliente_test5
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo SOLICITUD con producto ATENCION AL CLIENTE para la tipología: PROTECCIÓN DE DATOS - DERECHO ARCO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto ATENCION AL CLIENTE
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia PROTECCIÓN DE DATOS - DERECHO ARCO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1-2-3   | Si         | Si                   | Correo         | Registrado        |
      | CASE02   | 2-3-4   | Si         | No                   | Correo         | Registrado        |

  @happy_path @regresion @version3.1.7 @registroAtencionCliente_test6
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo SOLICITUD con producto ATENCION AL CLIENTE para la tipología: PROTECCIÓN DE DATOS - DERECHO ARCO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto ATENCION AL CLIENTE
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia PROTECCIÓN DE DATOS - DERECHO ARCO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta |
      | CASE01   | 1-2-3   | Si         | Si                   | Correo         | Registrado        |
      | CASE02   | 2-3-4   | Si         | No                   | Correo         | Registrado        |


  @happy_path @regresion @version3.1.7 @registroAtencionCliente_test7
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo SOLICITUD con producto ATENCION AL CLIENTE para la tipología: DENUNCIAS
    Given que el usuario GESTIÓN DE RECLAMOS REGULATORIOS accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto ATENCION AL CLIENTE
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia DENUNCIAS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "No" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples: #ESTA TIPOLOGIA SOLO PUEDE REGISTRAR - AREA - GESTIÓN DE RECLAMOS REGULATORIOS - NUEVO CAMBIO
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto |
      | CASE01   | 1-2-3   | Si         | Si                   |
      | CASE02   | 2-3-4   | Si         | No                   |


  @happy_path @regresion @version3.1.7 @registroAtencionCliente_test8
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro del trámite de tipo SOLICITUD con producto ATENCION AL CLIENTE para la tipología: DENUNCIAS LEGAL
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto ATENCION AL CLIENTE
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite SOLICITUD y la tipologia DENUNCIAS LEGAL
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   |
      | <motivos> |
    And se ingresa una respuesta al cliente "No" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    #el escenario define como seguira el flujo procede/no procede/validacion
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto |
      | CASE01   | 1       | Si         | Si                   |

