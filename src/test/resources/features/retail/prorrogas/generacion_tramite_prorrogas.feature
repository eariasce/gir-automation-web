@prorrogas
@generacion_prorrogas
Feature: Registro del Trámite con prorrogas

  @happy_path @regresion @registroProrrogasTC
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto TARJETA DE CREDITO para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | prorroga   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <prorroga> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    #PRORROGA
    And se cierra sesion del usuario
    And se inicia sesion con el usuario para realizar Prorrogas
    And se selecciona Procesos Masivos del menu principal
    And sube el archivo de prorrogas con los tramites generados
    And se selecciona Historial del menu principal
    Then se valida en historial los tramites en prorroga enviados en seguimiento
    Examples:
      | TESTCASE | TIPOLOGIA                                        | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd | prorroga |
      | CASE01   | EXONERACIÓN DE COBROS                            | PEDIDO      | 1       | 3               | PEN    | 100   | Si         | Si                   | Correo         | Registrado        | ACTIVA     | 1        |
      | CASE02   | MODIFICACION PLAZO                               | PEDIDO      | 1       | 1               | PEN    | 150   | Si         | Si                   | Correo         | Registrado        | ACTIVA     | 1        |
      | CASE03   | PROGRAMA DE RECOMPENSAS                          | PEDIDO      | 1       | 3               | PEN    | 300   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE04   | PAGO ANTICIPADO Y ADELANTO DE CUOTAS             | PEDIDO      | 2       | 3               | PEN    | 450   | Si         | Si                   | Direccion      | Registrado        | ACTIVA2    | 1        |
      | CASE05   | DEVOLUCION DE SALDO ACREEDOR                     | PEDIDO      | 2       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA2    | 1        |
      | CASE06   | DUPLICADO DE ESTADO DE CUENTA/SERVICIO DE COPIAS | PEDIDO      | 2       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE07   | DUPLICADO DE VOUCHER                             | PEDIDO      | 1       | 5               | PEN    | 150   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE08   | BAJA DE TASA                                     | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE09   | MODIFICACIÓN DE GRUPO DE LIQUIDACIÓN             | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE10   | MODIFICACIÓN LÍNEA TC ADICIONAL                  | PEDIDO      | 1       | 1               | PEN    | 150   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE11   | REDUCCIÓN DE LÍNEA TC                            | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE12   | TRASLADO DE SALDO ACREEDOR                       | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE13   | AFILIACIÓN / DESAFILIACIÓN DÉBITO AUTOMÁTICO     | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA2    | 1        |
      | CASE14   | DUPLICADO DE DOCUMENTO                           | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE15   | REGRABACIÓN DE PLÁSTICO                          | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE16   | CORRECCIÓN DE PAGO REALIZADO                     | PEDIDO      | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE17   | ENDOSO SEGUROS DE DESGRAVAMEN                    | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE18   | MODIFICACIÓN DISPOSICIÓN DE EFECTIVO             | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE19   | REVERSIÓN DE UPGRADE                             | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE20   | INFORMACIÓN DE ESTABLECIMIENTO/ RET.EFECTIVO     | PEDIDO      | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE21   | INFORMACION DE MOVIMIENTOS                       | PEDIDO      | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE22   | CAMBIO DE FORMA DE ENVÍO DE CORRESPONDENCIA      | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE23   | UNIFICACIÓN DE MARCAS/TIPOS DE TARJETAS          | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE24   | CONSTANCIA DE BLOQUEO DE TARJETA                 | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE25   | COBROS INDEBIDOS                                 | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE26   | CONSUMOS NO RECONOCIDOS POR FRAUDE               | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE27   | CONSUMOS MAL PROCESADOS POR EL COMERCIO/MARCA    | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE28   | SISTEMA DE RECOMPENSAS                           | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE29   | INSUFICIENTE INFORMACIÓN                         | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE30   | INCORRECTA APLICACIÓN CD/EC/C. CUOTAS            | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE31   | NO RECEPCIÓN DE DOCUMENTO                        | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE32   | LIBERACION DE RETENCIONES                        | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE33   | OPERACIÓN DENEGADA                               | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE34   | DESACUERDO DE CONDICIONES/TASAS/TARIFAS          | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE35   | REPORTE INDEBIDO CENTRALES DE RIESGO             | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE36   | INCUMPLIMIENTO DE CAMPAÑA                        | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE37   | ATM DE IB NO DEPOSITÓ DINERO                     | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |

  @happy_path @regresion @registroProrrogasTD
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto TARJETA DE DEBITO para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto TARJETA DE DEBITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | prorroga   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <prorroga> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    #PRORROGA
    And se cierra sesion del usuario
    And se inicia sesion con el usuario para realizar Prorrogas
    And se selecciona Procesos Masivos del menu principal
    And sube el archivo de prorrogas con los tramites generados
    And se selecciona Historial del menu principal
    Then se valida en historial los tramites en prorroga enviados en seguimiento
    Examples:
      | TESTCASE | TIPOLOGIA                                     | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd | prorroga |
      | CASE01   | EXONERACIÓN DE COBROS                         | PEDIDO      | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE02   | PROGRAMA DE RECOMPENSAS                       | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE03   | INFORMACIÓN DE ESTABLECIMIENTO/ RET.EFECTIVO  | PEDIDO      | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE04   | COBROS INDEBIDOS                              | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE05   | CONSUMOS NO RECONOCIDOS POR FRAUDE            | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE06   | CONSUMOS MAL PROCESADOS POR EL COMERCIO/MARCA | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE07   | LIBERACION DE RETENCIONES                     | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE08   | OPERACIÓN DENEGADA                            | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE09   | ATM DE IB NO ENTREGÓ DINERO                   | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE10   | ATM DE OTRO BANCO NO ENTREGÓ DINERO           | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE11   | REVISIÓN DE DEPÓSITOS A TERCEROS              | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |

  @happy_path @regresion @registroProrrogasSegRel
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto SEGUROS RELACIONADOS para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto SEGUROS RELACIONADOS y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | prorroga   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <prorroga> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    #PRORROGA
    And se cierra sesion del usuario
    And se inicia sesion con el usuario para realizar Prorrogas
    And se selecciona Procesos Masivos del menu principal
    And sube el archivo de prorrogas con los tramites generados
    And se selecciona Historial del menu principal
    Then se valida en historial los tramites en prorroga enviados en seguimiento
    Examples:
      | TESTCASE | TIPOLOGIA                  | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd | prorroga |
      | CASE01   | ENDOSO SEGUROS DEL BIEN    | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE02   | DUPLICADO DE DOCUMENTO     | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE03   | INFORMACION DE MOVIMIENTOS | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |

      #REVISAR
      | CASE04   | COBROS INDEBIDOS           | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |


  @happy_path @regresion @registroProrrogasSeg
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto SEGUROS para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto SEGUROS y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | prorroga   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <prorroga> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    #PRORROGA
    And se cierra sesion del usuario
    And se inicia sesion con el usuario para realizar Prorrogas
    And se selecciona Procesos Masivos del menu principal
    And sube el archivo de prorrogas con los tramites generados
    And se selecciona Historial del menu principal
    Then se valida en historial los tramites en prorroga enviados en seguimiento
    Examples:
      | TESTCASE | TIPOLOGIA                                 | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd | prorroga |
      | CASE01   | DUPLICADO DE DOCUMENTO                    | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE02   | INFORMACION DE MOVIMIENTOS                | PEDIDO      | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE03   | MODIFICACIÓN DE CONDICIONES CONTRACTUALES | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE04   | COBROS INDEBIDOS                          | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE05   | NO RECEPCIÓN DE DOCUMENTO                 | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |

  @happy_path @regresion @registroProrrogasRappiTC
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto RAPPIBANK - TARJETA DE CRÉDITO para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto RAPPIBANK - TARJETA DE CRÉDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | prorroga   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <prorroga> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    #PRORROGA
    And se cierra sesion del usuario
    And se inicia sesion con el usuario para realizar Prorrogas
    And se selecciona Procesos Masivos del menu principal
    And sube el archivo de prorrogas con los tramites generados
    And se selecciona Historial del menu principal
    Then se valida en historial los tramites en prorroga enviados en seguimiento
    Examples:
      | TESTCASE | TIPOLOGIA                                    | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd | prorroga |
      | CASE01   | ATM DE IB NO ENTREGÓ DINERO                  | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE02   | CORREO/TELÉFONO INDEBIDO DE CAMPAÑAS         | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE03   | DESACUERDO DE CONDICIONES/TASAS/TARIFAS      | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE04   | DIRECCIÓN/TELF. INDEBIDOS DE COBRANZA        | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE05   | INSATISFACCIÓN GESTIÓN DE ENTREGA DE TARJETA | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE06   | REPORTE INDEBIDO CENTRALES DE RIESGO         | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE07   | TRANSACCIÓN MAL O NO PROCESADA               | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE08   | COBROS INDEBIDOS                             | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE09   | CONSUMO/ RETIRO NO EFECTUADO                 | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE10   | LIBERACIÓN DE RETENCIONES                    | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE11   | NO RECEPCIÓN DE DOCUMENTO                    | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE12   | OPERACIÓN DENEGADA                           | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE13   | SISTEMA DE RECOMPENSAS                       | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
    #REVISAR
      | CASE14   | OTROS                                        | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |

  @happy_path @regresion @registroProrrogasRappiCuent
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto RAPPIBANK - CUENTAS para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto RAPPIBANK - CUENTAS y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | prorroga   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <prorroga> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    #PRORROGA
    And se cierra sesion del usuario
    And se inicia sesion con el usuario para realizar Prorrogas
    And se selecciona Procesos Masivos del menu principal
    And sube el archivo de prorrogas con los tramites generados
    And se selecciona Historial del menu principal
    Then se valida en historial los tramites en prorroga enviados en seguimiento
    Examples:
      | TESTCASE | TIPOLOGIA                                    | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd | prorroga |
      | CASE01   | COBROS INDEBIDOS                             | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE02   | TRANSACCIÓN MAL O NO PROCESADA               | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE03   | CONSUMO/ RETIRO NO EFECTUADO                 | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE04   | CUENTA/CANCELACIÓN NO RECONOCIDA             | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE05   | DESACUERDO CON EL SERVICIO                   | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE06   | DESACUERDO DE CONDICIONES/TASAS/TARIFAS      | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE07   | INSATISFACCIÓN GESTIÓN DE ENTREGA DE TARJETA | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE08   | LIBERACIÓN DE RETENCIONES                    | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE09   | OPERACIÓN DENEGADA                           | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE10   | REVISIÓN DE DEPÓSITOS A TERCEROS             | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE11   | SISTEMA DE RECOMPENSAS                       | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |

  @happy_path @regresion @registroProrrogasCC
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto CUENTA CORRIENTE para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto CUENTA CORRIENTE y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | prorroga   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <prorroga> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    #PRORROGA
    And se cierra sesion del usuario
    And se inicia sesion con el usuario para realizar Prorrogas
    And se selecciona Procesos Masivos del menu principal
    And sube el archivo de prorrogas con los tramites generados
    And se selecciona Historial del menu principal
    Then se valida en historial los tramites en prorroga enviados en seguimiento
    Examples:
      | TESTCASE | TIPOLOGIA                            | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd | prorroga |
      | CASE01   | COPIA DE BOLETA DE DEPÓSITO LIMA     | PEDIDO      | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE02   | REPORTE INDEBIDO CENTRALES DE RIESGO | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |

  @happy_path @regresion @registroProrrogasCuent
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto CUENTA para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto CUENTA y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | prorroga   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <prorroga> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    #PRORROGA
    And se cierra sesion del usuario
    And se inicia sesion con el usuario para realizar Prorrogas
    And se selecciona Procesos Masivos del menu principal
    And sube el archivo de prorrogas con los tramites generados
    And se selecciona Historial del menu principal
    Then se valida en historial los tramites en prorroga enviados en seguimiento
    Examples:
      | TESTCASE | TIPOLOGIA                                        | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd | prorroga |
      | CASE01   | EXONERACIÓN DE COBROS                            | PEDIDO      | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE02   | DUPLICADO DE ESTADO DE CUENTA/SERVICIO DE COPIAS | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE03   | INFORMACION DE MOVIMIENTOS                       | PEDIDO      | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE04   | DUPLICADO DE DOCUMENTO                           | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE05   | COBROS INDEBIDOS                                 | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE06   | CUENTA/CANCELACIÓN NO RECONOCIDA                 | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE07   | SISTEMA DE RECOMPENSAS                           | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE08   | DESACUERDO DE CONDICIONES/TASAS/TARIFAS          | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE09   | NO RECEPCIÓN DE DOCUMENTO                        | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE10   | LIBERACIÓN DE RETENCIONES JUDICIALES             | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE11   | ATM DE IB NO DEPOSITÓ DINERO                     | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE12   | SOLICITUD DE ALCANCÍA                            | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE13   | INCUMPLIMIENTO DE CAMPAÑA                        | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |
      | CASE14   | TRANSACCIÓN MAL O NO PROCESADA                   | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | 1        |

  @happy_path @regresion @registroProrrogasAdeSuel
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto ADELANTO SUELDO para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto ADELANTO SUELDO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | prorroga   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <prorroga> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    #PRORROGA
    And se cierra sesion del usuario
    And se inicia sesion con el usuario para realizar Prorrogas
    And se selecciona Procesos Masivos del menu principal
    And sube el archivo de prorrogas con los tramites generados
    And se selecciona Historial del menu principal
    Then se valida en historial los tramites en prorroga enviados en seguimiento
    Examples:
      | TESTCASE | TIPOLOGIA                    | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | prorroga |
      | CASE01   | ANULACIÓN DE CRÉDITO         | PEDIDO      | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE02   | INFORMACION DE MOVIMIENTOS   | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE03   | DUPLICADO DE DOCUMENTO       | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE04   | CONSUMO/ RETIRO NO EFECTUADO | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE05   | COBROS INDEBIDOS             | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |

  @happy_path @regresion @registroProrrogasLinConv
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto LINEA CONVENIO para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto LINEA CONVENIO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | prorroga   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <prorroga> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    #PRORROGA
    And se cierra sesion del usuario
    And se inicia sesion con el usuario para realizar Prorrogas
    And se selecciona Procesos Masivos del menu principal
    And sube el archivo de prorrogas con los tramites generados
    And se selecciona Historial del menu principal
    Then se valida en historial los tramites en prorroga enviados en seguimiento
    Examples:
      | TESTCASE | TIPOLOGIA                                 | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | prorroga |
      | CASE01   | BAJA DE TASA                              | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE02   | MODIFICACIÓN DE CONDICIONES CONTRACTUALES | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE03   | ANULACIÓN DE CRÉDITO                      | PEDIDO      | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE04   | DUPLICADO DE DOCUMENTO                    | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE05   | DUPLICADO DE VOUCHER                      | PEDIDO      | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE06   | PAGO ANTICIPADO Y ADELANTO DE CUOTAS      | PEDIDO      | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE07   | INFORMACION DE MOVIMIENTOS                | PEDIDO      | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE08   | ENDOSO SEGUROS DE DESGRAVAMEN             | PEDIDO      | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE09   | DEVOLUCIÓN CUOTAS EN TRÁNSITO             | PEDIDO      | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE10   | TRANSACCIÓN MAL O NO PROCESADA            | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE11   | DESACUERDO DE CONDICIONES/TASAS/TARIFAS   | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE12   | NO RECEPCIÓN DE DOCUMENTO                 | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE13   | CONSUMO/ RETIRO NO EFECTUADO              | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE14   | REPORTE INDEBIDO CENTRALES DE RIESGO      | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
 #Revisar
      | CASE15   | DUPLICADO DE CRONOGRAMA                   | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE16   | DUPLICADO DE CRONOGRAMA (C)               | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE17   | LIQUIDACIÓN DE DEUDA                      | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE18   | LIQUIDACIÓN DE DEUDA(C)                   | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |


  @happy_path @regresion @registroProrrogasEGH
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto EFECTIVO CON GARANTÍA HIPOTECARIO para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto EFECTIVO CON GARANTÍA HIPOTECARIO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | prorroga   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <prorroga> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    #PRORROGA
    And se cierra sesion del usuario
    And se inicia sesion con el usuario para realizar Prorrogas
    And se selecciona Procesos Masivos del menu principal
    And sube el archivo de prorrogas con los tramites generados
    And se selecciona Historial del menu principal
    Then se valida en historial los tramites en prorroga enviados en seguimiento
    Examples:
      | TESTCASE | TIPOLOGIA                                   | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | prorroga |
      | CASE01   | BAJA DE TASA                                | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE02   | DUPLICADO DE CRONOGRAMA                     | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE03   | INFORMACION DE MOVIMIENTOS                  | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE04   | MODIFICACIÓN CONFIGURACIÓN CRÉDITO          | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE05   | DEVOLUCIÓN DE DOCUMENTOS                    | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE06   | DUPLICADO DE DOCUMENTO                      | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE07   | EXONERACIÓN DE COBROS                       | PEDIDO      | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE08   | MODIFICACIÓN CONDICIONES CUOTAS CRÉDITO     | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE09   | ENDOSO SEGUROS DE DESGRAVAMEN               | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE10   | CAMBIO DE FORMA DE ENVÍO DE CORRESPONDENCIA | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE11   | CUOTA FLEXIBLE                              | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE12   | PAGO ANTICIPADO Y ADELANTO DE CUOTAS        | PEDIDO      | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE13   | EMISIÓN DE PRE CONFORMIDAD AFP 25%          | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE14   | LIQUIDACIÓN DE DEUDA                        | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE15   | MEJORA DE CONDICIONES CHIP- RET             | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE16   | COBROS INDEBIDOS                            | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE17   | DESACUERDO DE CONDICIONES/TASAS/TARIFAS     | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE18   | REPORTE INDEBIDO CENTRALES DE RIESGO        | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE19   | TRANSACCIÓN MAL O NO PROCESADA              | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE20   | CONSUMO/ RETIRO NO EFECTUADO                | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE21   | NO RECEPCIÓN DE DOCUMENTO                   | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
 #REVISAR
      | CASE22   | PAGO ANTICIPADO Y ADELANTO DE CUOTAS BPI    | PEDIDO      | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE23   | ANULACIÓN DE CRÉDITO                        | PEDIDO      | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |

  @happy_path @regresion @registroProrrogasHip
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto HIPOTECARIO para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto HIPOTECARIO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | prorroga   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <prorroga> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    #PRORROGA
    And se cierra sesion del usuario
    And se inicia sesion con el usuario para realizar Prorrogas
    And se selecciona Procesos Masivos del menu principal
    And sube el archivo de prorrogas con los tramites generados
    And se selecciona Historial del menu principal
    Then se valida en historial los tramites en prorroga enviados en seguimiento
    Examples:
      | TESTCASE | TIPOLOGIA                                   | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | prorroga |
      | CASE01   | BAJA DE TASA                                | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE02   | DUPLICADO DE CRONOGRAMA                     | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE03   | DEVOLUCIÓN DE DOCUMENTOS                    | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE04   | DUPLICADO DE DOCUMENTO                      | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE05   | PAGO ANTICIPADO Y ADELANTO DE CUOTAS        | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE06   | EMISIÓN DE PRE CONFORMIDAD AFP 25%          | PEDIDO      | 1       |                 | PEN    |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE07   | INFORMACION DE MOVIMIENTOS                  | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE08   | EXONERACIÓN DE COBROS                       | PEDIDO      | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE09   | LIQUIDACIÓN DE DEUDA                        | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE10   | CAMBIO DE FORMA DE ENVÍO DE CORRESPONDENCIA | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE11   | CUOTA FLEXIBLE                              | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE12   | ENDOSO SEGUROS DE DESGRAVAMEN               | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE13   | MEJORA DE CONDICIONES CHIP- RET             | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE14   | LEVANTAMIENTO DE GARANTÍA                   | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE15   | DESACUERDO DE CONDICIONES/TASAS/TARIFAS     | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE16   | NO RECEPCIÓN DE DOCUMENTO                   | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE17   | TRANSACCIÓN MAL O NO PROCESADA              | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE18   | COBROS INDEBIDOS                            | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE19   | REPORTE INDEBIDO CENTRALES DE RIESGO        | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE20   | CONSUMO/ RETIRO NO EFECTUADO                | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |

  @happy_path @regresion @registroProrrogasPrestPers
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto PRÉSTAMO PERSONAL para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto PRÉSTAMO PERSONAL
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | prorroga   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <prorroga> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    #PRORROGA
    And se cierra sesion del usuario
    And se inicia sesion con el usuario para realizar Prorrogas
    And se selecciona Procesos Masivos del menu principal
    And sube el archivo de prorrogas con los tramites generados
    And se selecciona Historial del menu principal
    Then se valida en historial los tramites en prorroga enviados en seguimiento
    Examples:
      | TESTCASE | TIPOLOGIA                                   | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | prorroga |
      | CASE01   | BAJA DE TASA                                | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE02   | DUPLICADO DE CRONOGRAMA                     | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE03   | INFORMACION DE MOVIMIENTOS                  | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE04   | MODIFICACIÓN CONFIGURACIÓN CRÉDITO          | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE05   | ANULACIÓN DE CRÉDITO                        | PEDIDO      | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE06   | DEVOLUCIÓN DE DOCUMENTOS                    | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE07   | EXONERACIÓN DE COBROS                       | PEDIDO      | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE08   | MODIFICACIÓN CONDICIONES CUOTAS CRÉDITO     | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE09   | PAGO ANTICIPADO Y ADELANTO DE CUOTAS        | PEDIDO      | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE10   | CAMBIO DE FORMA DE ENVÍO DE CORRESPONDENCIA | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE11   | CUOTA FLEXIBLE                              | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE12   | ENDOSO SEGUROS DE DESGRAVAMEN               | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE13   | COBROS INDEBIDOS                            | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE14   | DESACUERDO DE CONDICIONES/TASAS/TARIFAS     | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE15   | REPORTE INDEBIDO CENTRALES DE RIESGO        | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE16   | TRANSACCIÓN MAL O NO PROCESADA              | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE17   | NO RECEPCIÓN DE DOCUMENTO                   | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE18   | CONSUMO/ RETIRO NO EFECTUADO                | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
#REVISAR
      | CASE19   | DUPLICADO DE CRONOGRAMA BPI                 | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE20   | DUPLICADO DE DOCUMENTO                      | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE21   | PAGO ANTICIPADO Y ADELANTO DE CUOTAS BPI    | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |

  @happy_path @regresion @registroProrrogasVehic
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto VEHICULAR para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto VEHICULAR
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | prorroga   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <prorroga> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    #PRORROGA
    And se cierra sesion del usuario
    And se inicia sesion con el usuario para realizar Prorrogas
    And se selecciona Procesos Masivos del menu principal
    And sube el archivo de prorrogas con los tramites generados
    And se selecciona Historial del menu principal
    Then se valida en historial los tramites en prorroga enviados en seguimiento
    Examples:
      | TESTCASE | TIPOLOGIA                                   | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | prorroga |
      | CASE01   | BAJA DE TASA                                | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE02   | DUPLICADO DE CRONOGRAMA                     | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE03   | INFORMACION DE MOVIMIENTOS                  | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE04   | MODIFICACIÓN CONFIGURACIÓN CRÉDITO          | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE05   | ANULACIÓN DE CRÉDITO                        | PEDIDO      | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE06   | DEVOLUCIÓN DE DOCUMENTOS                    | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE07   | DUPLICADO DE DOCUMENTO                      | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE08   | EXONERACIÓN DE COBROS                       | PEDIDO      | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE09   | MODIFICACIÓN CONDICIONES CUOTAS CRÉDITO     | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE10   | CAMBIO DE FORMA DE ENVÍO DE CORRESPONDENCIA | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE11   | CUOTA FLEXIBLE                              | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE12   | PAGO ANTICIPADO Y ADELANTO DE CUOTAS        | PEDIDO      | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE13   | COBROS INDEBIDOS                            | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE14   | DESACUERDO DE CONDICIONES/TASAS/TARIFAS     | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE15   | REPORTE INDEBIDO CENTRALES DE RIESGO        | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE16   | TRANSACCIÓN MAL O NO PROCESADA              | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE17   | CONSUMO/ RETIRO NO EFECTUADO                | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE18   | NO RECEPCIÓN DE DOCUMENTO                   | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
#REVISAR
      | CASE19   | DUPLICADO DE CRONOGRAMA                     | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE20   | DUPLICADO DE CRONOGRAMA BPI                 | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE21   | ENDOSO SEGUROS DE DESGRAVAMEN               | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE22   | LEVANTAMIENTO DE GARANTÍA                   | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE23   | PAGO ANTICIPADO Y ADELANTO DE CUOTAS BPI    | PEDIDO      | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE24   | ENDOSO SEGUROS DEL BIEN                     | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |

  @happy_path @regresion @registroProrrogasCBME
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto CBME para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto CBME
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | prorroga   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <prorroga> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    #PRORROGA
    And se cierra sesion del usuario
    And se inicia sesion con el usuario para realizar Prorrogas
    And se selecciona Procesos Masivos del menu principal
    And sube el archivo de prorrogas con los tramites generados
    And se selecciona Historial del menu principal
    Then se valida en historial los tramites en prorroga enviados en seguimiento
    Examples:
      | TESTCASE | TIPOLOGIA                        | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | prorroga |
      | CASE01   | COPIA DE BOLETA DE DEPÓSITO LIMA | PEDIDO      | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |

  @happy_path @regresion @registroProrrogasCartFianCert
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto CARTA FIANZA/CERTIFICADO para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto CARTA FIANZA/CERTIFICADO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | prorroga   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <prorroga> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    #PRORROGA
    And se cierra sesion del usuario
    And se inicia sesion con el usuario para realizar Prorrogas
    And se selecciona Procesos Masivos del menu principal
    And sube el archivo de prorrogas con los tramites generados
    And se selecciona Historial del menu principal
    Then se valida en historial los tramites en prorroga enviados en seguimiento
    Examples:
      | TESTCASE | TIPOLOGIA              | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | prorroga |
      | CASE01   | DUPLICADO DE DOCUMENTO | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |


  @happy_path @regresion @registroProrrogasTunki
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto TUNKI para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TUNKI
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | prorroga   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <prorroga> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    #PRORROGA
    And se cierra sesion del usuario
    And se inicia sesion con el usuario para realizar Prorrogas
    And se selecciona Procesos Masivos del menu principal
    And sube el archivo de prorrogas con los tramites generados
    And se selecciona Historial del menu principal
    Then se valida en historial los tramites en prorroga enviados en seguimiento
    Examples:
      | TESTCASE | TIPOLOGIA                      | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | prorroga |
      | CASE01   | TRANSACCIÓN MAL O NO PROCESADA | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE02   | ATM DE IB NO ENTREGÓ DINERO    | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |


  @happy_path @regresion @registroProrrogasTransfNac
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto TRANSF. NACIONALES para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TRANSF. NACIONALES
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | prorroga   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <prorroga> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    #PRORROGA
    And se cierra sesion del usuario
    And se inicia sesion con el usuario para realizar Prorrogas
    And se selecciona Procesos Masivos del menu principal
    And sube el archivo de prorrogas con los tramites generados
    And se selecciona Historial del menu principal
    Then se valida en historial los tramites en prorroga enviados en seguimiento
    Examples:
      | TESTCASE | TIPOLOGIA                      | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | prorroga |
      | CASE01   | TRANSACCIÓN MAL O NO PROCESADA | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE02   | COBROS INDEBIDOS               | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
#REVISAR
      | CASE03   | CONSTANCIA DE PAGO REALIZADO   | PEDIDO      | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE04   | INFORMACIÓN DE MOVIMIENTOS     | PEDIDO      | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |

  @happy_path @regresion @registroProrrogasTransfInter
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto TRANSF. INTERNACIONALES para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TRANSF. INTERNACIONALES
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | prorroga   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <prorroga> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    #PRORROGA
    And se cierra sesion del usuario
    And se inicia sesion con el usuario para realizar Prorrogas
    And se selecciona Procesos Masivos del menu principal
    And sube el archivo de prorrogas con los tramites generados
    And se selecciona Historial del menu principal
    Then se valida en historial los tramites en prorroga enviados en seguimiento
    Examples:
      | TESTCASE | TIPOLOGIA                               | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | prorroga |
      | CASE01   | TRANSACCIÓN MAL O NO PROCESADA          | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE02   | COBROS INDEBIDOS                        | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE03   | NO RECEPCIÓN DE TRANSFERENCIA IN/OUT    | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE04   | CONSTANCIA POR TRANSFERENCIAS RECIBIDAS | PEDIDO      | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE05   | INFORMACION DE MOVIMIENTOS              | PEDIDO      | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |

  @happy_path @regresion @registroProrrogasToken
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto TOKEN para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TOKEN
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | prorroga   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <prorroga> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    #PRORROGA
    And se cierra sesion del usuario
    And se inicia sesion con el usuario para realizar Prorrogas
    And se selecciona Procesos Masivos del menu principal
    And sube el archivo de prorrogas con los tramites generados
    And se selecciona Historial del menu principal
    Then se valida en historial los tramites en prorroga enviados en seguimiento
    Examples:
      | TESTCASE | TIPOLOGIA                      | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | prorroga |
      | CASE01   | TRANSACCIÓN MAL O NO PROCESADA | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |

  @happy_path @regresion @registroProrrogasSunat
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto SUNAT para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto SUNAT
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | prorroga   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <prorroga> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    #PRORROGA
    And se cierra sesion del usuario
    And se inicia sesion con el usuario para realizar Prorrogas
    And se selecciona Procesos Masivos del menu principal
    And sube el archivo de prorrogas con los tramites generados
    And se selecciona Historial del menu principal
    Then se valida en historial los tramites en prorroga enviados en seguimiento
    Examples:
      | TESTCASE | TIPOLOGIA                      | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | prorroga |
      | CASE01   | CONSTANCIA DE PAGO REALIZADO   | PEDIDO      | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE02   | TRANSACCIÓN MAL O NO PROCESADA | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE03   | COBROS INDEBIDOS               | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
#REVISAR
      | CASE04   | CORRECCIÓN DE PAGO REALIZADO   | PEDIDO      | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |

  @happy_path @regresion @registroProrrogasRem
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto REMESAS para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto REMESAS
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | prorroga   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <prorroga> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    #PRORROGA
    And se cierra sesion del usuario
    And se inicia sesion con el usuario para realizar Prorrogas
    And se selecciona Procesos Masivos del menu principal
    And sube el archivo de prorrogas con los tramites generados
    And se selecciona Historial del menu principal
    Then se valida en historial los tramites en prorroga enviados en seguimiento
    Examples:
      | TESTCASE | TIPOLOGIA                               | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | prorroga |
      | CASE01   | ATM DE IB NO ENTREGÓ DINERO             | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE02   | COBROS INDEBIDOS                        | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE03   | NO RECEPCIÓN DE TRANSFERENCIA IN/OUT    | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE04   | CONSUMO/ RETIRO NO EFECTUADO            | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE05   | CONSTANCIA POR TRANSFERENCIAS RECIBIDAS | PEDIDO      | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |

  @happy_path @regresion @registroProrrogasPlin
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto PLIN para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto PLIN
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | prorroga   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <prorroga> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    #PRORROGA
    And se cierra sesion del usuario
    And se inicia sesion con el usuario para realizar Prorrogas
    And se selecciona Procesos Masivos del menu principal
    And sube el archivo de prorrogas con los tramites generados
    And se selecciona Historial del menu principal
    Then se valida en historial los tramites en prorroga enviados en seguimiento
    Examples:
      | TESTCASE | TIPOLOGIA                      | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | prorroga |
      | CASE01   | TRANSACCIÓN MAL O NO PROCESADA | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE02   | ATM DE IB NO ENTREGÓ DINERO    | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |

  @happy_path @regresion @registroProrrogasPayPal
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto PAY PAL para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto PAY PAL
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | prorroga   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <prorroga> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    #PRORROGA
    And se cierra sesion del usuario
    And se inicia sesion con el usuario para realizar Prorrogas
    And se selecciona Procesos Masivos del menu principal
    And sube el archivo de prorrogas con los tramites generados
    And se selecciona Historial del menu principal
    Then se valida en historial los tramites en prorroga enviados en seguimiento
    Examples:
      | TESTCASE | TIPOLOGIA                      | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | prorroga |
      | CASE01   | TRANSACCIÓN MAL O NO PROCESADA | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |

  @happy_path @regresion @registroProrrogasPagPlanProv
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto PAGOS PLANILLAS/PROVEEDORES para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto PAGOS PLANILLAS/PROVEEDORES
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | prorroga   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <prorroga> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    #PRORROGA
    And se cierra sesion del usuario
    And se inicia sesion con el usuario para realizar Prorrogas
    And se selecciona Procesos Masivos del menu principal
    And sube el archivo de prorrogas con los tramites generados
    And se selecciona Historial del menu principal
    Then se valida en historial los tramites en prorroga enviados en seguimiento
    Examples:
      | TESTCASE | TIPOLOGIA                      | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | prorroga |
      | CASE01   | INFORMACION DE MOVIMIENTOS     | PEDIDO      | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE02   | ACTIVACIÓN DE ORDENES DE PAGO  | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE03   | TRANSACCIÓN MAL O NO PROCESADA | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE04   | COBROS INDEBIDOS               | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |

  @happy_path @regresion @registroProrrogasPagServCan
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto PAGO DE SERVICIO POR CANALES para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto PAGO DE SERVICIO POR CANALES
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | prorroga   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <prorroga> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    #PRORROGA
    And se cierra sesion del usuario
    And se inicia sesion con el usuario para realizar Prorrogas
    And se selecciona Procesos Masivos del menu principal
    And sube el archivo de prorrogas con los tramites generados
    And se selecciona Historial del menu principal
    Then se valida en historial los tramites en prorroga enviados en seguimiento
    Examples:
      | TESTCASE | TIPOLOGIA                      | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | prorroga |
      | CASE01   | TRANSACCIÓN MAL O NO PROCESADA | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE02   | ATM DE IB NO DEPOSITÓ DINERO   | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE03   | COBROS INDEBIDOS               | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE04   | CORRECCIÓN DE PAGO REALIZADO   | PEDIDO      | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
#REVISAR
      | CASE05   | CONSTANCIA DE PAGO REALIZADO   | PEDIDO      | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE06   | INFORMACIÓN DE MOVIMIENTOS     | PEDIDO      | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |


  @happy_path @regresion @registroProrrogasMonExApp
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto MONEY EXCHANGE APP para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto MONEY EXCHANGE APP
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | prorroga   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <prorroga> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    #PRORROGA
    And se cierra sesion del usuario
    And se inicia sesion con el usuario para realizar Prorrogas
    And se selecciona Procesos Masivos del menu principal
    And sube el archivo de prorrogas con los tramites generados
    And se selecciona Historial del menu principal
    Then se valida en historial los tramites en prorroga enviados en seguimiento
    Examples:
      | TESTCASE | TIPOLOGIA                      | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | prorroga |
      | CASE01   | TRANSACCIÓN MAL O NO PROCESADA | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |

  @happy_path @regresion @registroProrrogasIbAg
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto IB AGENTE para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto IB AGENTE
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | prorroga   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <prorroga> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    #PRORROGA
    And se cierra sesion del usuario
    And se inicia sesion con el usuario para realizar Prorrogas
    And se selecciona Procesos Masivos del menu principal
    And sube el archivo de prorrogas con los tramites generados
    And se selecciona Historial del menu principal
    Then se valida en historial los tramites en prorroga enviados en seguimiento
    Examples:
      | TESTCASE | TIPOLOGIA                      | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | prorroga |
      | CASE01   | TRANSACCIÓN MAL O NO PROCESADA | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE02   | COBROS INDEBIDOS               | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |

  @happy_path @regresion @registroProrrogasDebAut
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto DEBITO AUTOMATICO para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto DEBITO AUTOMATICO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | prorroga   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <prorroga> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    #PRORROGA
    And se cierra sesion del usuario
    And se inicia sesion con el usuario para realizar Prorrogas
    And se selecciona Procesos Masivos del menu principal
    And sube el archivo de prorrogas con los tramites generados
    And se selecciona Historial del menu principal
    Then se valida en historial los tramites en prorroga enviados en seguimiento
    Examples:
      | TESTCASE | TIPOLOGIA                      | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | prorroga |
      | CASE01   | TRANSACCIÓN MAL O NO PROCESADA | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE02   | COBROS INDEBIDOS               | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
     #REVISAR
      | CASE03   | INFORMACIÓN DE MOVIMIENTOS     | PEDIDO      | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |


  @happy_path @regresion @registroProrrogasCobr
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto COBRANZAS para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto COBRANZAS
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | prorroga   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <prorroga> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    #PRORROGA
    And se cierra sesion del usuario
    And se inicia sesion con el usuario para realizar Prorrogas
    And se selecciona Procesos Masivos del menu principal
    And sube el archivo de prorrogas con los tramites generados
    And se selecciona Historial del menu principal
    Then se valida en historial los tramites en prorroga enviados en seguimiento
    Examples:
      | TESTCASE | TIPOLOGIA                             | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | prorroga |
      | CASE01   | CONSTANCIA DE NO ADEUDO               | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE02   | DIRECCIÓN/TELF. INDEBIDOS DE COBRANZA | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE03   | COBROS INDEBIDOS                      | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE04   | REPORTE INDEBIDO CENTRALES DE RIESGO  | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |

  @happy_path @regresion @registroProrrogasBillElec
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto BILLETERA ELECTRÓNICA para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto BILLETERA ELECTRÓNICA
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | prorroga   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <prorroga> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    #PRORROGA
    And se cierra sesion del usuario
    And se inicia sesion con el usuario para realizar Prorrogas
    And se selecciona Procesos Masivos del menu principal
    And sube el archivo de prorrogas con los tramites generados
    And se selecciona Historial del menu principal
    Then se valida en historial los tramites en prorroga enviados en seguimiento
    Examples:
      | TESTCASE | TIPOLOGIA                      | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | prorroga |
      | CASE01   | TRANSACCIÓN MAL O NO PROCESADA | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE02   | CONSUMO/ RETIRO NO EFECTUADO   | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE03   | INFORMACION DE MOVIMIENTOS     | PEDIDO      | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |

  @happy_path @regresion @registroProrrogasBancInt
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto BANCA POR INTERNET para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto BANCA POR INTERNET
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | prorroga   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <prorroga> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    #PRORROGA
    And se cierra sesion del usuario
    And se inicia sesion con el usuario para realizar Prorrogas
    And se selecciona Procesos Masivos del menu principal
    And sube el archivo de prorrogas con los tramites generados
    And se selecciona Historial del menu principal
    Then se valida en historial los tramites en prorroga enviados en seguimiento
    Examples:
      | TESTCASE | TIPOLOGIA                      | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | prorroga |
      | CASE01   | TRANSACCIÓN MAL O NO PROCESADA | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
#REVISAR
      | CASE02   | CONSTANCIA DE PAGO REALIZADO   | PEDIDO      | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |


  @happy_path @regresion @registroProrrogasBancCel
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto BANCA CELULAR para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto BANCA CELULAR
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | prorroga   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <prorroga> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    #PRORROGA
    And se cierra sesion del usuario
    And se inicia sesion con el usuario para realizar Prorrogas
    And se selecciona Procesos Masivos del menu principal
    And sube el archivo de prorrogas con los tramites generados
    And se selecciona Historial del menu principal
    Then se valida en historial los tramites en prorroga enviados en seguimiento
    Examples:
      | TESTCASE | TIPOLOGIA                      | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | prorroga |
      | CASE01   | TRANSACCIÓN MAL O NO PROCESADA | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |

  @happy_path @regresion @registroProrrogasAppInt
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto APP INTERBANK para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto APP INTERBANK
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | prorroga   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <prorroga> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    #PRORROGA
    And se cierra sesion del usuario
    And se inicia sesion con el usuario para realizar Prorrogas
    And se selecciona Procesos Masivos del menu principal
    And sube el archivo de prorrogas con los tramites generados
    And se selecciona Historial del menu principal
    Then se valida en historial los tramites en prorroga enviados en seguimiento
    Examples:
      | TESTCASE | TIPOLOGIA                      | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | prorroga |
      | CASE01   | CONSTANCIA DE PAGO REALIZADO   | PEDIDO      | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE02   | TRANSACCIÓN MAL O NO PROCESADA | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |

  @happy_path @regresion @registroProrrogasAtCli
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto ATENCION AL CLIENTE para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto ATENCION AL CLIENTE
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | prorroga   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <prorroga> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    #PRORROGA
    And se cierra sesion del usuario
    And se inicia sesion con el usuario para realizar Prorrogas
    And se selecciona Procesos Masivos del menu principal
    And sube el archivo de prorrogas con los tramites generados
    And se selecciona Historial del menu principal
    Then se valida en historial los tramites en prorroga enviados en seguimiento
    Examples:
      | TESTCASE | TIPOLOGIA                                    | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | prorroga |
      | CASE01   | DESACUERDO CON EL SERVICIO                   | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE02   | BILLETE FALSO                                | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE03   | CORREO/TELÉFONO INDEBIDO DE CAMPAÑAS         | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE04   | INSATISFACCIÓN GESTIÓN DE ENTREGA DE TARJETA | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE05   | SOLICITUD DE INTÉRPRETE PARA USUARIO         | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE06   | MODIFICACIÓN DATOS PERSONALES                | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |
      | CASE07   | VIDEO ARCO                                   | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | 1        |


  @happy_path @regresion @registroProrrogas2 @unhappypath
  Scenario: <TESTCASE> [UNHAPPY PATH] Valida la prórroga de un tramite con 6 prorrogas realizadas, de tipo solicitud y con tramite sin carta enviada
    Given se inicia sesion con el usuario para realizar Prorrogas
    And se selecciona Procesos Masivos del menu principal
    And sube el archivo de prorrogas con un tramite de seis prorrogas realizadas
    And se muestra el mensaje de error de tramites

  @happy_path @regresion @registroProrrogas3 @unhappypath
  Scenario Outline: <TESTCASE> [UNHAPPY PATH] Valida registro y prórroga con excel de 301 tramites del trámite de tipo <TIPOTRAMITE> con producto <PRODUCTO> para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto <PRODUCTO> y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | prorroga   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <prorroga> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
#    #PRORROGA
    And se cierra sesion del usuario
    And se inicia sesion con el usuario para realizar Prorrogas
    And se selecciona Procesos Masivos del menu principal
    And sube el archivo de prorrogas con mas de trecientos tramites
    And se muestra el mensaje de error de tramites

    Examples:
      | TESTCASE | TIPOLOGIA             | TIPOTRAMITE | PRODUCTO           | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd | prorroga |
      | CASE01   | EXONERACIÓN DE COBROS | PEDIDO      | TARJETA DE CREDITO | 1       | 3               | PEN    | 100   | Si         | Si                   | Correo         | Registrado        | ACTIVA     | 6        |

  @happy_path @regresion @registroProrrogas4 @unhappypath
  Scenario Outline: <TESTCASE> [UNHAPPY PATH] Valida registro y prórroga con 21 dias del trámite de tipo <TIPOTRAMITE> con producto <PRODUCTO> para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto <PRODUCTO> y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | prorroga   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <prorroga> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
#    #PRORROGA
    And se cierra sesion del usuario
    And se inicia sesion con el usuario para realizar Prorrogas
    And se selecciona Procesos Masivos del menu principal
    And sube el archivo de prorrogas con los tramites generados
    And se selecciona Historial del menu principal
    Then se valida en historial los tramites en prorroga enviados en seguimiento

    Examples:
      | TESTCASE | TIPOLOGIA             | TIPOTRAMITE | PRODUCTO           | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd | prorroga |
      | CASE01   | EXONERACIÓN DE COBROS | PEDIDO      | TARJETA DE CREDITO | 1       | 3               | PEN    | 100   | Si         | Si                   | Correo         | Registrado        | ACTIVA     | 21       |


  @happy_path @regresion @registroProrrogas5 @happypath
  Scenario: <TESTCASE> [UNHAPPY PATH] Valida la prórroga de un tramite con más de 7 dias de creación, buscando en bandeja tramites
    Given se inicia sesion con el usuario para realizar Prorrogas
    And se selecciona Tramites del menu principal
    And se realiza filtro de tipo de tramites por Pedido para agregar 1 dias de prorroga
    And se selecciona Procesos Masivos del menu principal
    And sube el archivo de prorrogas con los tramites generados
    And se selecciona Historial del menu principal
    Then se valida en historial los tramites en prorroga enviados en seguimiento
