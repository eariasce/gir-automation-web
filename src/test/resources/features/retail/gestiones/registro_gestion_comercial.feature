 @registro_gestiones
 Feature: Registro de Gestión Comercial

  @happy_path @regresion @registro_gestion_RUC
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida el REGISTRO de una GESTIÓN COMERCIAL cuando se RECOPILA DATA DE GIRU para un CLIENTE con RUC
    Given que el usuario <usuario> accede a la aplicacion GIRU COMERCIAL
    And se selecciona Gestiones del menu principal
    When realiza el registro de la gestión con los datos ingresados en la opcion recopila data de "Giru" para un cliente
      | tipoDocumento   | gestionDescripcion   | llamada   | comentario   |
      | RUC             | <gestionDescripcion> | <llamada> | <comentario> |
    Then se crea la gestion de manera exitosa
    And se valida la gestion creada por usuario

    Examples:
      |  TESTCASE | usuario      | gestionDescripcion             | llamada | comentario |
      | CASE01   | ESPECIALISTA | (ABP)Campañas Select           | Out     | Prueba     |
      | CASE02   | SUPERVISOR   | (ABP)Contacto Efectivo         | In      | Prueba     |
      | CASE03   | ESPECIALISTA | (ABP)Contacto No Efectivo      | Out     | Prueba     |
      | CASE04   | SUPERVISOR   | (ABP)No Contactar              | In      | Prueba     |
      | CASE05   | ESPECIALISTA | ADMINISTRACIÓN DE EFECTIVO     | Out     | Prueba     |
      | CASE06   | SUPERVISOR   | Activación/Bloqueo             | In      | Prueba     |
      | CASE07   | ESPECIALISTA | Aduanas                        | Out     | Prueba     |
      | CASE08   | SUPERVISOR   | Ahorros (Millonaria/Max/Libre) | In      | Prueba     |
      | CASE09   | ESPECIALISTA | American Express de Interbank  | Out     | Prueba     |
      | CASE10   | SUPERVISOR   | App / Web                      | In      | Prueba     |
      | CASE11   | ESPECIALISTA | Back Office                    | Out     | Prueba     |
      | CASE12   | SUPERVISOR   | Banca Empresa Lima/Corporativa | In      | Prueba     |
      | CASE13   | ESPECIALISTA | Bloqueo TC - Prosegur          | Out     | Prueba     |
      | CASE14   | SUPERVISOR   | Bloqueo/desbloqueo temporal TC | In      | Prueba     |
      | CASE15   | ESPECIALISTA | Bono Alimentario               | Out     | Prueba     |
      | CASE16   | SUPERVISOR   | Campañas                       | In      | Prueba     |
      | CASE17   | ESPECIALISTA | Campañas TC                    | Out     | Prueba     |
      | CASE18   | SUPERVISOR   | Campañas/Promociones IBK       | In      | Prueba     |
      | CASE19   | ESPECIALISTA | Cancelado CTS                  | Out     | Prueba     |
      | CASE20   | SUPERVISOR   | Capital de Trabajo BPE         | In      | Prueba     |
      | CASE21   | ESPECIALISTA | Clave Dinámica                 | Out     | Prueba     |
      | CASE22   | SUPERVISOR   | Cobranza/Descuentos - Letras   | In      | Prueba     |
      | CASE23   | ESPECIALISTA | Cobranzas de Exportación       | Out     | Prueba     |
      | CASE24   | SUPERVISOR   | Colaboradores IBK              | In      | Prueba     |
      | CASE25   | ESPECIALISTA | Combo                          | Out     | Prueba     |
      | CASE26   | SUPERVISOR   | Consulta App Convenios         | In      | Prueba     |
      | CASE27   | ESPECIALISTA | Consulta Situación Pedido/Rec  | Out     | Prueba     |
      | CASE28   | SUPERVISOR   | Consultas (direc/hor/nomb/etc) | In      | Prueba     |
      | CASE29   | ESPECIALISTA | Consultas Legales              | Out     | Prueba     |
      | CASE30   | SUPERVISOR   | Contacto Efectivo              | In      | Prueba     |
      | CASE31   | ESPECIALISTA | Crédito Hipotecario            | Out     | Prueba     |
      | CASE32   | SUPERVISOR   | Crédito Vehicular              | In      | Prueba     |
      | CASE33   | ESPECIALISTA | Créditos BPE                   | Out     | Prueba     |
      | CASE34   | SUPERVISOR   | Cta Ahorr,Corriente,CTS,Sueldo | In      | Prueba     |
      | CASE35   | ESPECIALISTA | Cuenta Corriente               | Out     | Prueba     |
      | CASE36   | SUPERVISOR   | Depósito a Plazo               | In      | Prueba     |
      | CASE37   | ESPECIALISTA | Deriv.Canal Digital y Alter.   | Out     | Prueba     |
      | CASE38   | SUPERVISOR   | Derivación a Fide              | In      | Prueba     |
      | CASE39   | ESPECIALISTA | Derivación a reclamos          | Out     | Prueba     |
      | CASE40   | SUPERVISOR   | Descuento Cuenta Sueldo        | In      | Prueba     |
      | CASE41   | ESPECIALISTA | Envío TC/TD al Exterior        | Out     | Prueba     |
      | CASE42   | SUPERVISOR   | Envío status atendido IVR      | In      | Prueba     |
      | CASE43   | ESPECIALISTA | Equipo Reprogramación TC       | Out     | Prueba     |
      | CASE44   | SUPERVISOR   | Extracash                      | In      | Prueba     |
      | CASE45   | ESPECIALISTA | Factoring                      | Out     | Prueba     |
      | CASE46   | SUPERVISOR   | Financiamiento Import / Export | In      | Prueba     |
      | CASE47   | ESPECIALISTA | Financiamiento de Exportación  | Out     | Prueba     |
      | CASE48   | SUPERVISOR   | Financiamiento de Importación  | In      | Prueba     |
      | CASE49   | ESPECIALISTA | Garantías                      | Out     | Prueba     |
      | CASE50   | SUPERVISOR   | Gestiones Adicionales          | In      | Prueba     |
      | CASE51   | ESPECIALISTA | IBK Agente                     | Out     | Prueba     |
      | CASE52   | SUPERVISOR   | Incidencias                    | In      | Prueba     |
      | CASE53   | ESPECIALISTA | Incidencias - UPG              | Out     | Prueba     |
      | CASE54   | SUPERVISOR   | Incidencias Benefit            | In      | Prueba     |
      | CASE55   | ESPECIALISTA | Incidencias Web                | Out     | Prueba     |
      | CASE56   | SUPERVISOR   | Información general No asoc.   | In      | Prueba     |
      | CASE57   | ESPECIALISTA | Información general asociada   | Out     | Prueba     |
      | CASE58   | SUPERVISOR   | Ingreso de Pedido y Reclamo    | In      | Prueba     |
      | CASE59   | ESPECIALISTA | Interbank Benefit              | Out     | Prueba     |
      | CASE60   | SUPERVISOR   | Interbank Visa                 | In      | Prueba     |
      | CASE61   | ESPECIALISTA | Llamada cortada                | Out     | Prueba     |
      | CASE62   | SUPERVISOR   | Mail pase a cuotas             | In      | Prueba     |
      | CASE63   | ESPECIALISTA | Mastercard                     | Out     | Prueba     |
      | CASE64   | SUPERVISOR   | Mora Temprana                  | In      | Prueba     |
      | CASE65   | ESPECIALISTA | Mypes                          | Out     | Prueba     |
      | CASE66   | SUPERVISOR   | No desea inf Campaña           | In      | Prueba     |
      | CASE67   | ESPECIALISTA | No ubicable                    | Out     | Prueba     |
      | CASE68   | SUPERVISOR   | Otros Web                      | In      | Prueba     |
      | CASE69   | ESPECIALISTA | Pagaré                         | Out     | Prueba     |
      | CASE70   | SUPERVISOR   | Pago Automático                | In      | Prueba     |
      | CASE71   | ESPECIALISTA | Pagos Planilla/CTS             | Out     | Prueba     |
      | CASE72   | SUPERVISOR   | Pagos Varios                   | In      | Prueba     |
      | CASE73   | ESPECIALISTA | Pedido                         | Out     | Prueba     |
      | CASE74   | SUPERVISOR   | Pepper                         | In      | Prueba     |
      | CASE75   | ESPECIALISTA | Piloto                         | Out     | Prueba     |
      | CASE76   | SUPERVISOR   | Préstamos                      | In      | Prueba     |
      | CASE77   | ESPECIALISTA | Recaudación                    | Out     | Prueba     |
      | CASE78   | SUPERVISOR   | Reclamos duplicados            | In      | Prueba     |
      | CASE79   | ESPECIALISTA | Referencias comerciales        | Out     | Prueba     |
      | CASE80   | SUPERVISOR   | Rellamadas                     | In      | Prueba     |
      | CASE81   | ESPECIALISTA | Retenciones judiciales         | Out     | Prueba     |
      | CASE82   | SUPERVISOR   | Retención Seguros              | In      | Prueba     |
      | CASE83   | ESPECIALISTA | Retenido CTS                   | Out     | Prueba     |
      | CASE84   | SUPERVISOR   | Retiros TD Colombia            | In      | Prueba     |
      | CASE85   | ESPECIALISTA | Robinson Total                 | Out     | Prueba     |
      | CASE86   | SUPERVISOR   | Seguros                        | In      | Prueba     |
      | CASE87   | ESPECIALISTA | Servicio No Conforme           | Out     | Prueba     |
      | CASE88   | SUPERVISOR   | Servicios Varios               | In      | Prueba     |
      | CASE89   | ESPECIALISTA | Sin Venta - Contacto Efectivo  | Out     | Prueba     |
      | CASE90   | SUPERVISOR   | Situación Pedido/Reclamo/Solic | In      | Prueba     |
      | CASE91   | ESPECIALISTA | Status devuelto IVR            | Out     | Prueba     |
      | CASE92   | SUPERVISOR   | Status en proceso              | In      | Prueba     |
      | CASE93   | ESPECIALISTA | Sunat/Aduanas                  | Out     | Prueba     |
      | CASE94   | SUPERVISOR   | T. Débito Digital              | In      | Prueba     |
      | CASE95   | ESPECIALISTA | TC EECC                        | Out     | Prueba     |
      | CASE96   | SUPERVISOR   | TIE 1                          | In      | Prueba     |
      | CASE97   | ESPECIALISTA | Tarjeta BPE                    | Out     | Prueba     |
      | CASE98   | SUPERVISOR   | Tarjeta Crédito Empresarial    | In      | Prueba     |
      | CASE99   | ESPECIALISTA | Tarjeta de Crédito             | Out     | Prueba     |
      | CASE100  | SUPERVISOR   | Tarjeta de Débito              | In      | Prueba     |
      | CASE101  | ESPECIALISTA | Transferencia IVR              | Out     | Prueba     |
      | CASE102  | SUPERVISOR   | Transferencia de llamada       | In      | Prueba     |
      | CASE103  | ESPECIALISTA | Transferencia del Exterior     | Out     | Prueba     |
      | CASE104  | SUPERVISOR   | Transferencias                 | In      | Prueba     |
      | CASE105  | ESPECIALISTA | Transferencias BCR             | Out     | Prueba     |
      | CASE106  | SUPERVISOR   | Transferencias al Exterior     | In      | Prueba     |
      | CASE107  | ESPECIALISTA | Transferencias entre Cuentas   | Out     | Prueba     |
      | CASE108  | SUPERVISOR   | Transferencias-141             | In      | Prueba     |
      | CASE109  | ESPECIALISTA | UPF                            | Out     | Prueba     |
      | CASE110  | SUPERVISOR   | Upgrade TC                     | In      | Prueba     |
      | CASE111  | ESPECIALISTA | Visa CashBack                  | Out     | Prueba     |
      | CASE112  | SUPERVISOR   | Web                            | In      | Prueba     |
      | CASE113  | ESPECIALISTA | WhatsApp Comercial             | Out     | Prueba     |
      | CASE114  | SUPERVISOR   | Wong/Visa CashBack             | In      | Prueba     |


   @happy_path @regresion @registro_gestion_RUC_telesoft
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro de gestión y valida su búsqueda
    Given que el usuario <usuario> accede a la aplicacion GIRU COMERCIAL
    And se selecciona Gestiones del menu principal
    When realiza el registro de la gestión con los datos ingresados en la opcion recopila data de "Telesoft" para un cliente
      | tipoDocumento   | gestionDescripcion   | llamada   | comentario   |
      | RUC             | <gestionDescripcion> | <llamada> | <comentario> |
    Then se crea la gestion de manera exitosa
     And se valida la gestion creada por usuario

    Examples:
      | TESTCASE | usuario      | gestionDescripcion             | llamada | comentario |
      | CASE01   | ESPECIALISTA | AFP                            | Out     | Prueba     |
      | CASE02   | SUPERVISOR   | AFP Net                        | In      | Prueba     |
      | CASE03   | ESPECIALISTA | Acciones primera línea         | Out     | Prueba     |
      | CASE04   | SUPERVISOR   | Activación TC IVR (P)          | In      | Prueba     |
      | CASE05   | ESPECIALISTA | Asesoría Uso Web               | Out     | Prueba     |
      | CASE06   | SUPERVISOR   | Asesoría contrato              | In      | Prueba     |
      | CASE07   | ESPECIALISTA | Atencion de Solicitudes /Outbo | Out     | Prueba     |
      | CASE08   | SUPERVISOR   | Atención al Cliente            | In      | Prueba     |
      | CASE09   | ESPECIALISTA | Bono Universal                 | Out     | Prueba     |
      | CASE10   | SUPERVISOR   | CIMA                           | In      | Prueba     |
      | CASE11   | ESPECIALISTA | CTS                            | Out     | Prueba     |
      | CASE12   | SUPERVISOR   | Campaña CD preferente          | In      | Prueba     |
      | CASE13   | ESPECIALISTA | Carta Fianza                   | Out     | Prueba     |
      | CASE14   | SUPERVISOR   | Carta de Crédito Exportación   | In      | Prueba     |
      | CASE15   | ESPECIALISTA | Carta de Crédito Importación   | Out     | Prueba     |
      | CASE16   | SUPERVISOR   | Cheques                        | In      | Prueba     |
      | CASE17   | ESPECIALISTA | Combos PJ                      | Out     | Prueba     |
      | CASE18   | SUPERVISOR   | Comex                          | In      | Prueba     |
      | CASE19   | ESPECIALISTA | Confirmación                   | Out     | Prueba     |
      | CASE20   | SUPERVISOR   | Confirmación Saldos y Firmas   | In      | Prueba     |
      | CASE21   | ESPECIALISTA | Contacto No Efectivo           | Out     | Prueba     |
      | CASE22   | SUPERVISOR   | Convenios                      | In      | Prueba     |
      | CASE23   | ESPECIALISTA | Coordinación TC                | Out     | Prueba     |
      | CASE24   | SUPERVISOR   | Crédito Efectivo               | In      | Prueba     |
      | CASE25   | ESPECIALISTA | Cuenta Corriente/Ahorro        | Out     | Prueba     |
      | CASE26   | SUPERVISOR   | Cuenta Negocio                 | In      | Prueba     |
      | CASE27   | ESPECIALISTA | Cálculo de cuotas              | Out     | Prueba     |
      | CASE28   | SUPERVISOR   | Datos asociados errados        | In      | Prueba     |
      | CASE29   | ESPECIALISTA | Descuento Electrónico          | Out     | Prueba     |
      | CASE30   | SUPERVISOR   | Desea Información Campaña      | In      | Prueba     |
      | CASE31   | ESPECIALISTA | Dirección/Teléfono             | Out     | Prueba     |
      | CASE32   | SUPERVISOR   | Envio EECC IVR (P)             | In      | Prueba     |
      | CASE33   | ESPECIALISTA | Factoring Electrónico          | Out     | Prueba     |
      | CASE34   | SUPERVISOR   | Factura Negociable             | In      | Prueba     |
      | CASE35   | ESPECIALISTA | Fax                            | Out     | Prueba     |
      | CASE36   | SUPERVISOR   | Fidelización                   | In      | Prueba     |
      | CASE37   | ESPECIALISTA | Gestión de Atención            | Out     | Prueba     |
      | CASE38   | SUPERVISOR   | Gestión de poderes             | In      | Prueba     |
      | CASE39   | ESPECIALISTA | Hipot., Vehic. y Préstamos     | Out     | Prueba     |
      | CASE40   | SUPERVISOR   | Hipotecario                    | In      | Prueba     |
      | CASE41   | ESPECIALISTA | Inconvenientes Web             | Out     | Prueba     |
      | CASE42   | SUPERVISOR   | Inconvenientes Web Empresas    | In      | Prueba     |
      | CASE43   | ESPECIALISTA | Incremento de Línea            | Out     | Prueba     |
      | CASE44   | SUPERVISOR   | Información                    | In      | Prueba     |
      | CASE45   | ESPECIALISTA | Leasing                        | Out     | Prueba     |
      | CASE46   | SUPERVISOR   | Letras                         | In      | Prueba     |
      | CASE47   | ESPECIALISTA | Llamada Concretada             | Out     | Prueba     |
      | CASE48   | SUPERVISOR   | Llamada No Concretada          | In      | Prueba     |
      | CASE49   | ESPECIALISTA | Netactiva                      | Out     | Prueba     |
      | CASE50   | SUPERVISOR   | No Contacto                    | In      | Prueba     |
      | CASE51   | ESPECIALISTA | No Retención/Status tarjeta    | Out     | Prueba     |
      | CASE52   | SUPERVISOR   | No Tiene Campaña               | In      | Prueba     |
      | CASE53   | ESPECIALISTA | Pago Automático - Empresa      | Out     | Prueba     |
      | CASE54   | SUPERVISOR   | Pago Automático - Usuario      | In      | Prueba     |
      | CASE55   | ESPECIALISTA | Pago Proveedores               | Out     | Prueba     |
      | CASE56   | SUPERVISOR   | Pago de Recibos                | In      | Prueba     |
      | CASE57   | ESPECIALISTA | Priority Pass                  | Out     | Prueba     |
      | CASE58   | SUPERVISOR   | Prosegur                       | In      | Prueba     |
      | CASE59   | ESPECIALISTA | Proveedores                    | Out     | Prueba     |
      | CASE60   | SUPERVISOR   | Préstamo Personal              | In      | Prueba     |
      | CASE61   | ESPECIALISTA | Remuneraciones                 | Out     | Prueba     |
      | CASE62   | SUPERVISOR   | Reprogramaciones TLV           | In      | Prueba     |
      | CASE63   | ESPECIALISTA | Requisitos generales           | Out     | Prueba     |
      | CASE64   | SUPERVISOR   | Requisitos mype                | In      | Prueba     |
      | CASE65   | ESPECIALISTA | Robinson promociones           | Out     | Prueba     |
      | CASE66   | SUPERVISOR   | Robinson venta                 | In      | Prueba     |
      | CASE67   | ESPECIALISTA | SUNAT                          | Out     | Prueba     |
      | CASE68   | SUPERVISOR   | Seguro Desempleo               | In      | Prueba     |
      | CASE69   | ESPECIALISTA | Solicitudes                    | Out     | Prueba     |
      | CASE70   | SUPERVISOR   | Status Atendido IVR            | In      | Prueba     |
      | CASE71   | ESPECIALISTA | Status Cuentas/Crédito         | Out     | Prueba     |
      | CASE72   | SUPERVISOR   | Status Vencido IVR             | In      | Prueba     |
      | CASE73   | ESPECIALISTA | TC Extracash y Compra de deuda | Out     | Prueba     |
      | CASE74   | SUPERVISOR   | TC Status                      | In      | Prueba     |
      | CASE75   | ESPECIALISTA | TELEFONO NO CORRESPONDE        | Out     | Prueba     |
      | CASE76   | SUPERVISOR   | TIE                            | In      | Prueba     |
      | CASE77   | ESPECIALISTA | Tarjetas Adicionales           | Out     | Prueba     |
      | CASE78   | SUPERVISOR   | Tasa Mejorada-TC Deuda Actual  | In      | Prueba     |
      | CASE79   | ESPECIALISTA | Tiendas                        | Out     | Prueba     |
      | CASE80   | SUPERVISOR   | Token                          | In      | Prueba     |
      | CASE81   | ESPECIALISTA | Transferencias CCE             | Out     | Prueba     |
      | CASE82   | SUPERVISOR   | Transferencias In/Out          | In      | Prueba     |
      | CASE83   | ESPECIALISTA | Transferencias Saldos          | Out     | Prueba     |
      | CASE84   | SUPERVISOR   | Transferencias a Terceros      | In      | Prueba     |
      | CASE85   | ESPECIALISTA | Validación Banca Comercial     | Out     | Prueba     |
      | CASE86   | SUPERVISOR   | Vea Privada                    | In      | Prueba     |
      | CASE87   | ESPECIALISTA | Vehicular                      | Out     | Prueba     |
      | CASE88   | SUPERVISOR   | Ventas                         | In      | Prueba     |
