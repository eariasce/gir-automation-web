@registro_gestiones
Feature: Registro de Gestión Retail

  @happy_path @regresion @registro_gestionfullregistros
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida el REGISTRO de una GESTIÓN cuando se RECOPILA DATA DE GIRU para un CLIENTE con DNI
    Given que el usuario <usuario> accede a la aplicacion GIRU
    And se selecciona Gestiones del menu principal
    When realiza la recopilacion de data de Giru para gestiones
      | tipoDocumento   | gestionDescripcion   | llamada   | comentario   |
      | DNI             | <gestionDescripcion> | <llamada> | <comentario> |

    Examples:
      | TESTCASE | usuario      | gestionDescripcion            | llamada | comentario |
      | CASE01   | ESPECIALISTA | (ABP)Campañas Select          | In      | Prueba     |