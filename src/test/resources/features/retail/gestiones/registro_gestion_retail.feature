@registro_gestiones
Feature: Registro de Gestión Retail

  @happy_path @regresion @registro_gestion_DNI
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida el REGISTRO de una GESTIÓN cuando se RECOPILA DATA DE GIRU para un CLIENTE con DNI
    Given que el usuario <usuario> accede a la aplicacion GIRU
    And se selecciona Gestiones del menu principal
    When realiza el registro de la gestión con los datos ingresados en la opcion recopila data de "Giru" para un cliente
      | tipoDocumento   | gestionDescripcion   | llamada   | comentario   |
      | DNI             | <gestionDescripcion> | <llamada> | <comentario> |
    Then se crea la gestion de manera exitosa
    And se valida la gestion creada por usuario

    Examples:
      | TESTCASE | usuario      | gestionDescripcion            | llamada | comentario |
      | CASE01   | ESPECIALISTA | (ABP)Campañas Select          | In      | Prueba     |
      | CASE02   | SUPERVISOR   | ADMINISTRACIÓN DE EFECTIVO    | Out     | Prueba     |
      | CASE03   | ESPECIALISTA | American Express de Interbank | In      | Prueba     |
      | CASE04   | SUPERVISOR   | Bloqueo TC - Prosegur         | Out     | Prueba     |
      | CASE05   | ESPECIALISTA | Campañas TC                   | In      | Prueba     |
      | CASE06   | SUPERVISOR   | Clave Dinámica                | Out     | Prueba     |
      | CASE07   | ESPECIALISTA | Combo                         | In      | Prueba     |
      | CASE08   | SUPERVISOR   | Consultas Legales             | Out     | Prueba     |
      | CASE09   | ESPECIALISTA | Créditos BPE                  | In      | Prueba     |
      | CASE10   | SUPERVISOR   | Deriv.Canal Digital y Alter.  | Out     | Prueba     |
      | CASE11   | ESPECIALISTA | Envío TC/TD al Exterior       | In      | Prueba     |
      | CASE12   | SUPERVISOR   | Factoring                     | Out     | Prueba     |
      | CASE13   | ESPECIALISTA | Garantías                     | In      | Prueba     |
      | CASE14   | SUPERVISOR   | Incidencias - UPG             | Out     | Prueba     |
      | CASE15   | ESPECIALISTA | Información general asociada  | In      | Prueba     |
      | CASE16   | SUPERVISOR   | Llamada cortada               | Out     | Prueba     |
      | CASE17   | ESPECIALISTA | Mypes                         | In      | Prueba     |
      | CASE18   | SUPERVISOR   | Pagaré                        | Out     | Prueba     |
      | CASE19   | ESPECIALISTA | Pedido                        | In      | Prueba     |
      | CASE20   | SUPERVISOR   | Recaudación                   | Out     | Prueba     |
      | CASE21   | ESPECIALISTA | Retenciones judiciales        | In      | Prueba     |
      | CASE22   | SUPERVISOR   | Robinson Total                | Out     | Prueba     |
      | CASE23   | ESPECIALISTA | Sin Venta - Contacto Efectivo | In      | Prueba     |
      | CASE24   | SUPERVISOR   | Sunat/Aduanas                 | Out     | Prueba     |
      | CASE25   | ESPECIALISTA | Tarjeta BPE                   | In      | Prueba     |
      | CASE26   | SUPERVISOR   | Transferencia IVR             | Out     | Prueba     |
      | CASE27   | ESPECIALISTA | Transferencias BCR            | In      | Prueba     |
      | CASE28   | SUPERVISOR   | UPF                           | Out     | Prueba     |
      | CASE29   | SUPERVISOR   | WhatsApp Comercial            | Out     | Prueba     |

  @happy_path @regresion @registro_gestion_CE
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida el REGISTRO de una GESTIÓN cuando se RECOPILA DATA DE GIRU para un CLIENTE con CE
    Given que el usuario <usuario> accede a la aplicacion GIRU
    And se selecciona Gestiones del menu principal
    When realiza el registro de la gestión con los datos ingresados en la opcion recopila data de "Giru" para un cliente
      | tipoDocumento   | gestionDescripcion   | llamada   | comentario   |
      | CE              | <gestionDescripcion> | <llamada> | <comentario> |
    Then se crea la gestion de manera exitosa
    And se valida la gestion creada por usuario

    Examples:
      | TESTCASE | usuario      | gestionDescripcion             | llamada | comentario |
      | CASE01   | ESPECIALISTA | (ABP)Contacto Efectivo         | In      | Prueba     |
      | CASE02   | SUPERVISOR   | Activación/Bloqueo             | Out     | Prueba     |
      | CASE03   | ESPECIALISTA | App / Web                      | In      | Prueba     |
      | CASE04   | SUPERVISOR   | Bloqueo/desbloqueo temporal TC | Out     | Prueba     |
      | CASE05   | ESPECIALISTA | Campañas/Promociones IBK       | In      | Prueba     |
      | CASE06   | SUPERVISOR   | Cobranza/Descuentos - Letras   | Out     | Prueba     |
      | CASE07   | ESPECIALISTA | Consulta App Convenios         | In      | Prueba     |
      | CASE08   | SUPERVISOR   | Contacto Efectivo              | Out     | Prueba     |
      | CASE09   | ESPECIALISTA | Cta Ahorr,Corriente,CTS,Sueldo | In      | Prueba     |
      | CASE10   | SUPERVISOR   | Derivación a Fide              | Out     | Prueba     |
  # ESTE CASO TIENE ERROR EN FRONT NOM CARGA COMBOBOX
      | CASE11   | ESPECIALISTA | Envío status atendido IVR      | In      | Prueba     |
      | CASE12   | SUPERVISOR   | Financiamiento Import / Export | Out     | Prueba     |
      | CASE13   | ESPECIALISTA | Gestiones Adicionales          | In      | Prueba     |
      | CASE14   | SUPERVISOR   | Incidencias Benefit            | Out     | Prueba     |
      | CASE15   | ESPECIALISTA | Ingreso de Pedido y Reclamo    | In      | Prueba     |
      | CASE16   | SUPERVISOR   | Mail pase a cuotas             | Out     | Prueba     |
    #ESTE CASO NO SE ENCUENTRA GESTION EN STG
      | CASE17   | ESPECIALISTA | No desea inf Campaña           | In      | Prueba     |
      | CASE18   | SUPERVISOR   | Pago Automático                | Out     | Prueba     |
      | CASE19   | ESPECIALISTA | Pepper                         | In      | Prueba     |
      | CASE20   | SUPERVISOR   | Reclamos duplicados            | Out     | Prueba     |
      | CASE21   | ESPECIALISTA | Retención Seguros              | In      | Prueba     |
      | CASE22   | SUPERVISOR   | Seguros                        | Out     | Prueba     |
      | CASE23   | ESPECIALISTA | Situación Pedido/Reclamo/Solic | In      | Prueba     |
      | CASE24   | SUPERVISOR   | T. Débito Digital              | Out     | Prueba     |
      | CASE25   | ESPECIALISTA | Tarjeta Crédito Empresarial    | In      | Prueba     |
      | CASE26   | SUPERVISOR   | Transferencia de llamada       | Out     | Prueba     |
      | CASE27   | ESPECIALISTA | Transferencias al Exterior     | In      | Prueba     |
      | CASE28   | SUPERVISOR   | Upgrade TC                     | Out     | Prueba     |
      | CASE29   | ESPECIALISTA | Wong/Visa CashBack             | In      | Prueba     |



  @happy_path @regresion @registro_gestion_PASAPORTE
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida el REGISTRO de una GESTIÓN cuando se RECOPILA DATA DE GIRU para un CLIENTE con PASAPORTE
    Given que el usuario <usuario> accede a la aplicacion GIRU
    And se selecciona Gestiones del menu principal
    When realiza el registro de la gestión con los datos ingresados en la opcion recopila data de "Giru" para un cliente
      | tipoDocumento   | gestionDescripcion   | llamada   | comentario   |
      | PASAPORTE       | <gestionDescripcion> | <llamada> | <comentario> |
    Then se crea la gestion de manera exitosa
    And se valida la gestion creada por usuario

    Examples:
      |   TESTCASE | usuario      | gestionDescripcion             | llamada | comentario |
      | CASE01   | ESPECIALISTA | (ABP)No Contactar              | In      | Prueba     |
      | CASE02   | SUPERVISOR   | Ahorros (Millonaria/Max/Libre) | Out     | Prueba     |
      | CASE03   | ESPECIALISTA | Banca Empresa Lima/Corporativa | In      | Prueba     |
      | CASE04   | SUPERVISOR   | Campañas                       | Out     | Prueba     |
      | CASE05   | ESPECIALISTA | Capital de Trabajo BPE         | In      | Prueba     |
      | CASE06   | SUPERVISOR   | Colaboradores IBK              | Out     | Prueba     |
      | CASE07   | ESPECIALISTA | Consultas (direc/hor/nomb/etc) | In      | Prueba     |
      | CASE08   | SUPERVISOR   | Crédito Vehicular              | Out     | Prueba     |
      | CASE09   | ESPECIALISTA | Depósito a Plazo               | In      | Prueba     |
      | CASE10   | SUPERVISOR   | Descuento Cuenta Sueldo        | Out     | Prueba     |
      | CASE11   | ESPECIALISTA | Extracash                      | In      | Prueba     |
      | CASE12   | SUPERVISOR   | Financiamiento de Importación  | Out     | Prueba     |
      | CASE13   | ESPECIALISTA | Incidencias                    | In      | Prueba     |
      | CASE14   | SUPERVISOR   | Información general No asoc.   | Out     | Prueba     |
      | CASE15   | ESPECIALISTA | Interbank Visa                 | In      | Prueba     |
      | CASE16   | SUPERVISOR   | Mora Temprana                  | Out     | Prueba     |
      | CASE17   | ESPECIALISTA | Otros Web                      | In      | Prueba     |
      | CASE18   | SUPERVISOR   | Pagos Varios                   | Out     | Prueba     |
      | CASE19   | ESPECIALISTA | Préstamos                      | In      | Prueba     |
      | CASE20   | SUPERVISOR   | Rellamadas                     | Out     | Prueba     |
      | CASE21   | ESPECIALISTA | Retiros TD Colombia            | In      | Prueba     |
      | CASE22   | SUPERVISOR   | Servicios Varios               | Out     | Prueba     |
#ERROR! POR FRONT NO HAY GESTIONES HIJA ERROR TDM
     | CASE23   | ESPECIALISTA | Status en proceso              | In      | Prueba     |
      | CASE24   | SUPERVISOR   | TIE 1                          | Out     | Prueba     |
      | CASE25   | ESPECIALISTA | Tarjeta de Débito              | In      | Prueba     |
      | CASE26   | SUPERVISOR   | Transferencias                 | Out     | Prueba     |
      | CASE27   | ESPECIALISTA | Transferencias-141             | In      | Prueba     |
     | CASE28   | SUPERVISOR   | Web                            | Out     | Prueba     |

  @happy_path @regresion @registro_gestion_PTP
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida el REGISTRO de una GESTIÓN cuando se RECOPILA DATA DE GIRU para un CLIENTE con PTP
    Given que el usuario <usuario> accede a la aplicacion GIRU
    And se selecciona Gestiones del menu principal
    When realiza el registro de la gestión con los datos ingresados en la opcion recopila data de "Giru" para un cliente
      | tipoDocumento   | gestionDescripcion   | llamada   | comentario   |
      | PTP             | <gestionDescripcion> | <llamada> | <comentario> |
    Then se crea la gestion de manera exitosa
    And se valida la gestion creada por usuario

    Examples:
      | TESTCASE | usuario      | gestionDescripcion            | llamada | comentario |
      | CASE01   | ESPECIALISTA | (ABP)Contacto No Efectivo     | In      | Prueba     |
      | CASE02   | SUPERVISOR   | Aduanas                       | Out     | Prueba     |
      | CASE03   | ESPECIALISTA | Back Office                   | In      | Prueba     |
      | CASE04   | SUPERVISOR   | Bono Alimentario              | Out     | Prueba     |
      | CASE05   | ESPECIALISTA | Cancelado CTS                 | In      | Prueba     |
      | CASE06   | SUPERVISOR   | Cobranzas de Exportación      | Out     | Prueba     |
      | CASE07   | ESPECIALISTA | Consulta Situación Pedido/Rec | In      | Prueba     |
      | CASE08   | SUPERVISOR   | Crédito Hipotecario           | Out     | Prueba     |
      | CASE09   | ESPECIALISTA | Cuenta Corriente              | In      | Prueba     |
      | CASE10   | SUPERVISOR   | Derivación a reclamos         | Out     | Prueba     |
      | CASE11   | ESPECIALISTA | Equipo Reprogramación TC      | In      | Prueba     |
      | CASE12   | SUPERVISOR   | Financiamiento de Exportación | Out     | Prueba     |
      | CASE13   | ESPECIALISTA | IBK Agente                    | In      | Prueba     |
      | CASE14   | SUPERVISOR   | Incidencias Web               | Out     | Prueba     |
      | CASE15   | ESPECIALISTA | Interbank Benefit             | In      | Prueba     |
      | CASE16   | SUPERVISOR   | Mastercard                    | Out     | Prueba     |
      | CASE17   | ESPECIALISTA | No ubicable                   | In      | Prueba     |
      | CASE18   | SUPERVISOR   | Pagos Planilla/CTS            | Out     | Prueba     |
      | CASE19   | ESPECIALISTA | Piloto                        | In      | Prueba     |
      | CASE20   | SUPERVISOR   | Referencias comerciales       | Out     | Prueba     |
      | CASE21   | ESPECIALISTA | Retenido CTS                  | In      | Prueba     |
      | CASE22   | SUPERVISOR   | Servicio No Conforme          | Out     | Prueba     |
      #ERROR! POR FRONT NO HAY GESTIONES HIJA ERROR TDM
      | CASE23   | ESPECIALISTA | Status devuelto IVR           | In      | Prueba     |
      | CASE24   | SUPERVISOR   | TC EECC                       | Out     | Prueba     |
      | CASE25   | ESPECIALISTA | Tarjeta de Crédito            | In      | Prueba     |
      | CASE26   | SUPERVISOR   | Transferencia del Exterior    | Out     | Prueba     |
      | CASE27   | ESPECIALISTA | Transferencias entre Cuentas  | In      | Prueba     |
      | CASE28   | SUPERVISOR   | Visa CashBack                 | Out     | Prueba     |


  @happy_path @regresion @registro_gestion_DNI_telesoft
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida el REGISTRO de una GESTIÓN cuando se RECOPILA DATA DE TELESOFT para un CLIENTE con DNI
    Given que el usuario <usuario> accede a la aplicacion GIRU
    And se selecciona Gestiones del menu principal
    When realiza el registro de la gestión con los datos ingresados en la opcion recopila data de "Telesoft" para un cliente
      | tipoDocumento   | gestionDescripcion   | llamada   | comentario   |
      | DNI             | <gestionDescripcion> | <llamada> | <comentario> |
    Then se crea la gestion de manera exitosa
    And se valida la gestion creada por usuario

    Examples:
      | TESTCASE | usuario      | gestionDescripcion             | llamada | comentario |
      | CASE01   | SUPERVISOR   | Activación TC IVR (P)          | Out     | Prueba     |
      | CASE02   | ESPECIALISTA | Atencion de Solicitudes /Outbo | In      | Prueba     |
      | CASE03   | SUPERVISOR   | CIMA                           | Out     | Prueba     |
      | CASE04   | ESPECIALISTA | Carta Fianza                   | In      | Prueba     |
      | CASE05   | SUPERVISOR   | Confirmación Saldos y Firmas   | In      | Prueba     |
      | CASE06   | ESPECIALISTA | Coordinación TC                | Out     | Prueba     |
      | CASE07   | SUPERVISOR   | Cuenta Negocio                 | In      | Prueba     |
      | CASE08   | ESPECIALISTA | Descuento Electrónico          | Out     | Prueba     |
      | CASE09   | SUPERVISOR   | Fidelización                   | Out     | Prueba     |
      | CASE10   | ESPECIALISTA | Hipot., Vehic. y Préstamos     | In      | Prueba     |
      | CASE11   | SUPERVISOR   | Inconvenientes Web Empresas    | Out     | Prueba     |
      | CASE12   | ESPECIALISTA | Leasing                        | In      | Prueba     |
      | CASE13   | SUPERVISOR   | No Tiene Campaña               | In      | Prueba     |
      | CASE14   | ESPECIALISTA | Pago Proveedores               | Out     | Prueba     |
      | CASE15   | SUPERVISOR   | Prosegur                       | In      | Prueba     |
      | CASE16   | ESPECIALISTA | Remuneraciones                 | Out     | Prueba     |
      | CASE17   | SUPERVISOR   | Seguro Desempleo               | Out     | Prueba     |
      | CASE18   | ESPECIALISTA | Status Cuentas/Crédito         | In      | Prueba     |
      | CASE19   | SUPERVISOR   | TC Status                      | Out     | Prueba     |
      | CASE20   | ESPECIALISTA | Tarjetas Adicionales           | In      | Prueba     |
      | CASE21   | SUPERVISOR   | Transferencias a Terceros      | In      | Prueba     |
      | CASE22   | ESPECIALISTA | Vehicular                      | Out     | Prueba     |


  @happy_path @regresion @registro_gestion_CE_telesoft
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida el REGISTRO de una GESTIÓN cuando se RECOPILA DATA DE TELESOFT para un CLIENTE con CE
    Given que el usuario <usuario> accede a la aplicacion GIRU
    And se selecciona Gestiones del menu principal
    When realiza el registro de la gestión con los datos ingresados en la opcion recopila data de "Telesoft" para un cliente
      | tipoDocumento   | gestionDescripcion   | llamada   | comentario   |
      | CE              | <gestionDescripcion> | <llamada> | <comentario> |
    Then se crea la gestion de manera exitosa
    And se valida la gestion creada por usuario

    Examples:
      | TESTCASE | usuario      | gestionDescripcion            | llamada | comentario |
      | CASE01   | SUPERVISOR   | AFP                           | Out     | Prueba     |
      | CASE02   | ESPECIALISTA | Atención al Cliente           | In      | Prueba     |
      | CASE03   | SUPERVISOR   | CTS                           | Out     | Prueba     |
      | CASE04   | ESPECIALISTA | Carta de Crédito Exportación  | In      | Prueba     |
      | CASE05   | SUPERVISOR   | Combos PJ                     | In      | Prueba     |
      | CASE06   | ESPECIALISTA | Crédito Efectivo              | Out     | Prueba     |
      | CASE07   | SUPERVISOR   | Cálculo de cuotas             | In      | Prueba     |
      | CASE08   | ESPECIALISTA | Desea Información Campaña     | Out     | Prueba     |
      | CASE09   | SUPERVISOR   | Factoring Electrónico         | Out     | Prueba     |
      | CASE10   | ESPECIALISTA | Hipotecario                   | In      | Prueba     |
      | CASE11   | SUPERVISOR   | Incremento de Línea           | Out     | Prueba     |
      | CASE12   | ESPECIALISTA | Letras                        | In      | Prueba     |
      | CASE13   | SUPERVISOR   | Netactiva                     | In      | Prueba     |
      | CASE14   | ESPECIALISTA | Pago de Recibos               | Out     | Prueba     |
      | CASE15   | SUPERVISOR   | Proveedores                   | In      | Prueba     |
      | CASE16   | ESPECIALISTA | Reprogramaciones TLV          | Out     | Prueba     |
      | CASE17   | SUPERVISOR   | Robinson promociones          | Out     | Prueba     |
      #ERROR FRONT NO MUESTRA LISTA COMBOBOX
      | CASE18   | ESPECIALISTA | Status Vencido IVR            | In      | Prueba     |
      | CASE19   | SUPERVISOR   | TELEFONO NO CORRESPONDE       | Out     | Prueba     |
      | CASE20   | ESPECIALISTA | Tasa Mejorada-TC Deuda Actual | In      | Prueba     |
      | CASE21   | SUPERVISOR   | Transferencias CCE            | In      | Prueba     |
      | CASE22   | ESPECIALISTA | Ventas                        | Out     | Prueba     |


  @happy_path @regresion @registro_gestion_PASAPORTE_telesoft
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida el REGISTRO de una GESTIÓN cuando se RECOPILA DATA DE TELESOFT para un CLIENTE con PASAPORTE
    Given que el usuario <usuario> accede a la aplicacion GIRU
    And se selecciona Gestiones del menu principal
    When realiza el registro de la gestión con los datos ingresados en la opcion recopila data de "Telesoft" para un cliente
      | tipoDocumento   | gestionDescripcion   | llamada   | comentario   |
      | PASAPORTE       | <gestionDescripcion> | <llamada> | <comentario> |
    Then se crea la gestion de manera exitosa
    And se valida la gestion creada por usuario

    Examples:
      | TESTCASE | usuario      | gestionDescripcion             | llamada | comentario |
      | CASE01   | SUPERVISOR   | Acciones primera línea         | Out     | Prueba     |
      | CASE02   | ESPECIALISTA | Asesoría contrato              | In      | Prueba     |
      | CASE03   | SUPERVISOR   | Bono Universal                 | Out     | Prueba     |
      | CASE04   | ESPECIALISTA | Cheques                        | In      | Prueba     |
      | CASE05   | SUPERVISOR   | Confirmación                   | In      | Prueba     |
      | CASE06   | ESPECIALISTA | Convenios                      | Out     | Prueba     |
      | CASE07   | SUPERVISOR   | Cuenta Corriente/Ahorro        | In      | Prueba     |
      | CASE08   | ESPECIALISTA | Envio EECC IVR (P)             | Out     | Prueba     |
      | CASE09   | SUPERVISOR   | Fax                            | Out     | Prueba     |
      | CASE10   | ESPECIALISTA | Gestión de poderes             | In      | Prueba     |
      | CASE11   | SUPERVISOR   | Inconvenientes Web             | Out     | Prueba     |
      | CASE12   | ESPECIALISTA | Llamada No Concretada          | In      | Prueba     |
      | CASE13   | SUPERVISOR   | No Retención/Status tarjeta    | In      | Prueba     |
      | CASE14   | ESPECIALISTA | Pago Automático - Usuario      | Out     | Prueba     |
      | CASE15   | SUPERVISOR   | Priority Pass                  | In      | Prueba     |
      | CASE16   | ESPECIALISTA | Requisitos mype                | Out     | Prueba     |
      | CASE17   | SUPERVISOR   | SUNAT                          | Out     | Prueba     |
      #ERROR FRONT NO MUESTRA LISTA COMBOBOX
      | CASE18   | ESPECIALISTA | Status Atendido IVR            | In      | Prueba     |
      | CASE19   | SUPERVISOR   | TC Extracash y Compra de deuda | Out     | Prueba     |
      | CASE20   | ESPECIALISTA | Token                          | In      | Prueba     |
      | CASE21   | SUPERVISOR   | Transferencias Saldos          | In      | Prueba     |
      | CASE22   | ESPECIALISTA | Vea Privada                    | Out     | Prueba     |

  @happy_path @regresion @registro_gestion_PTP_telesoft
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida el REGISTRO de una GESTIÓN cuando se RECOPILA DATA DE TELESOFT para un CLIENTE con PTP
    Given que el usuario <usuario> accede a la aplicacion GIRU
    And se selecciona Gestiones del menu principal
    When realiza el registro de la gestión con los datos ingresados en la opcion recopila data de "Telesoft" para un cliente
      | tipoDocumento   | gestionDescripcion   | llamada   | comentario   |
      | PTP             | <gestionDescripcion> | <llamada> | <comentario> |
    Then se crea la gestion de manera exitosa
    And se valida la gestion creada por usuario

    Examples:
      | TESTCASE | usuario      | gestionDescripcion           | llamada | comentario |
      | CASE01   | SUPERVISOR   | AFP Net                      | Out     | Prueba     |
      | CASE02   | ESPECIALISTA | Asesoría Uso Web             | In      | Prueba     |
      | CASE03   | SUPERVISOR   | Campaña CD preferente        | Out     | Prueba     |
      | CASE04   | ESPECIALISTA | Carta de Crédito Importación | In      | Prueba     |
      | CASE05   | SUPERVISOR   | Comex                        | In      | Prueba     |
      | CASE06   | ESPECIALISTA | Contacto No Efectivo         | Out     | Prueba     |
      | CASE07   | SUPERVISOR   | Datos asociados errados      | In      | Prueba     |
      | CASE08   | ESPECIALISTA | Dirección/Teléfono           | Out     | Prueba     |
      | CASE09   | SUPERVISOR   | Factura Negociable           | Out     | Prueba     |
      | CASE10   | ESPECIALISTA | Gestión de Atención          | In      | Prueba     |
      | CASE11   | SUPERVISOR   | Información                  | Out     | Prueba     |
      | CASE12   | ESPECIALISTA | Llamada Concretada           | In      | Prueba     |
      | CASE13   | SUPERVISOR   | No Contacto                  | In      | Prueba     |
      | CASE14   | ESPECIALISTA | Pago Automático - Empresa    | Out     | Prueba     |
      | CASE15   | SUPERVISOR   | Préstamo Personal            | In      | Prueba     |
      | CASE16   | ESPECIALISTA | Requisitos generales         | Out     | Prueba     |
      | CASE17   | SUPERVISOR   | Robinson venta               | Out     | Prueba     |
      | CASE18   | ESPECIALISTA | Solicitudes                  | In      | Prueba     |
      | CASE19   | SUPERVISOR   | TIE                          | Out     | Prueba     |
      | CASE20   | ESPECIALISTA | Tiendas                      | In      | Prueba     |
      | CASE21   | SUPERVISOR   | Transferencias In/Out        | In      | Prueba     |
      | CASE22   | ESPECIALISTA | Validación Banca Comercial   | Out     | Prueba     |
