@all_retipificacion_tramite_rappiBankCuentas
Feature: Retipificación del trámite Producto de RAPPIBANK - CUENTAS

  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- RETIPIFICACION RECLAMO ------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.9 @retipificacionRappiBankCuentas_test3
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida retipificación del trámite de tipo RECLAMO con producto RAPPIBANK - CUENTAS para tipología origen: CONSUMO/ RETIRO NO EFECTUADO - tipología destino: TRANSACCIÓN MAL O NO PROCESADA
    Given que el usuario SUPERVISOR accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto RAPPIBANK - CUENTAS y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia CONSUMO/ RETIRO NO EFECTUADO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | canal   | nuevoMotivo   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <canal> | <nuevoMotivo> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #RETIPIFICACIÓN
    And segun el tramite se resuelve o deriva a area respectiva hasta <resolver_o_retipificar>:<retipifica> el tramite <areaValidacionOpcional>
    #VALIDACION
    Then se valida en historial el tramite con su retipificacion
    #DERIVACION
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion Derivar-2 con area a derivar <areaValidacionOpcional>

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | canal   | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd | resolucion  | areaValidacionOpcional | resolver_o_retipificar | retipifica                                           | nuevoMotivo |
      | CASE01   | 1-2-3   | 3               | PEN    | 100   | App IBK | Si         | Si                   | Correo         | Registrado        | ACTIVA     | Retipificar |                        | RETIPIFICA             | RAPPIBANK_CUENTAS-RECLAMO-transaccionMalONoProcesada | 1           |


  @happy_path @regresion @version3.1.9 @retipificacionRappiBankCuentas_test4
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida retipificación del trámite de tipo RECLAMO con producto RAPPIBANK - CUENTAS para tipología origen: CUENTA/CANCELACIÓN NO RECONOCIDA - tipología destino: CONSUMO/ RETIRO NO EFECTUADO
    Given que el usuario SUPERVISOR accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto RAPPIBANK - CUENTAS y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia CUENTA/CANCELACIÓN NO RECONOCIDA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | canal   | nuevoMotivo   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <canal> | <nuevoMotivo> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #RETIPIFICACIÓN
    And segun el tramite se resuelve o deriva a area respectiva hasta <resolver_o_retipificar>:<retipifica> el tramite <areaValidacionOpcional>
    #VALIDACION
    Then se valida en historial el tramite con su retipificacion
    #DERIVACION
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion Derivar-2 con area a derivar <areaValidacionOpcional>

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | canal   | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd | resolucion  | areaValidacionOpcional | resolver_o_retipificar | retipifica                                         | nuevoMotivo |
      | CASE01   | 1-2     | 3               | PEN    | 100   | App IBK | Si         | Si                   | Correo         | Registrado        | ACTIVA     | Retipificar |                        | RETIPIFICA             | RAPPIBANK_CUENTAS-RECLAMO-consumoRetiroNoEfectuado | 1           |


  @happy_path @regresion @version3.1.9 @retipificacionRappiBankCuentas_test5
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida retipificación del trámite de tipo RECLAMO con producto RAPPIBANK - CUENTAS para tipología origen: DESACUERDO CON EL SERVICIO - tipología destino: DESACUERDO DE CONDICIONES/TASAS/TARIFAS
    Given que el usuario SUPERVISOR accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto RAPPIBANK - CUENTAS y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia DESACUERDO CON EL SERVICIO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | canal   | nuevoMotivo   |
      | <motivos> | <canal> | <nuevoMotivo> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #RETIPIFICACIÓN
    And segun el tramite se resuelve o deriva a area respectiva hasta <resolver_o_retipificar>:<retipifica> el tramite <areaValidacionOpcional>
    #VALIDACION
    Then se valida en historial el tramite con su retipificacion
    #DERIVACION
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion Derivar-2 con area a derivar <areaValidacionOpcional>

    Examples:
      | TESTCASE | motivos | canal   | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd | resolucion  | areaValidacionOpcional | resolver_o_retipificar | retipifica                                        | nuevoMotivo |
      | CASE01   | 1       | App IBK | Si         | Si                   | Correo         | Registrado        | ACTIVA     | Retipificar |                        | RETIPIFICA             | RAPPIBANK_CUENTAS-RECLAMO-desacuerdoCondicionesTT | 1           |


  @happy_path @regresion @version3.1.9 @retipificacionRappiBankCuentas_test6
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida retipificación del trámite de tipo RECLAMO con producto RAPPIBANK - CUENTAS para tipología origen: DESACUERDO DE CONDICIONES/TASAS/TARIFAS - tipología destino: DESACUERDO CON EL SERVICIO
    Given que el usuario SUPERVISOR accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto RAPPIBANK - CUENTAS y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia DESACUERDO DE CONDICIONES/TASAS/TARIFAS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | canal   | nuevoMotivo   |
      | <motivos> | <canal> | <nuevoMotivo> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #RETIPIFICACIÓN
    And segun el tramite se resuelve o deriva a area respectiva hasta <resolver_o_retipificar>:<retipifica> el tramite <areaValidacionOpcional>
    #VALIDACION
    Then se valida en historial el tramite con su retipificacion
    #DERIVACION
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion Derivar-2 con area a derivar <areaValidacionOpcional>

    Examples:
      | TESTCASE | motivos | canal   | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd | resolucion  | areaValidacionOpcional | resolver_o_retipificar | retipifica                                        | nuevoMotivo |
      | CASE01   | 1       | App IBK | Si         | Si                   | Correo         | Registrado        | ACTIVA     | Retipificar |                        | RETIPIFICA             | RAPPIBANK_CUENTAS-RECLAMO-DesacuerdoConElServicio | 1           |


  @happy_path @regresion @version3.1.9 @retipificacionRappiBankCuentas_test7
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida retipificación del trámite de tipo RECLAMO con producto RAPPIBANK - CUENTAS para tipología origen: INSATISFACCIÓN GESTIÓN DE ENTREGA DE TARJETA - tipología destino: SISTEMA DE RECOMPENSAS
    Given que el usuario SUPERVISOR accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto RAPPIBANK - CUENTAS y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia INSATISFACCIÓN GESTIÓN DE ENTREGA DE TARJETA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | canal   | nuevoMotivo   |
      | <motivos> | <canal> | <nuevoMotivo> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #RETIPIFICACIÓN
    And segun el tramite se resuelve o deriva a area respectiva hasta <resolver_o_retipificar>:<retipifica> el tramite <areaValidacionOpcional>
    #VALIDACION
    Then se valida en historial el tramite con su retipificacion
    #DERIVACION
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion Derivar-2 con area a derivar <areaValidacionOpcional>

    Examples:
      | TESTCASE | motivos | canal   | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd | resolucion  | areaValidacionOpcional | resolver_o_retipificar | retipifica                                   | nuevoMotivo |
      | CASE01   | 1       | App IBK | Si         | Si                   | Correo         | Registrado        | ACTIVA     | Retipificar |                        | RETIPIFICA             | RAPPIBANK_CUENTAS-RECLAMO-sistemaRecompensas | 1           |


  @happy_path @regresion @version3.1.9 @retipificacionRappiBankCuentas_test8
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida retipificación del trámite de tipo RECLAMO con producto RAPPIBANK - CUENTAS para tipología origen: LIBERACIÓN DE RETENCIONES - tipología destino: OPERACIÓN DENEGADA
    Given que el usuario SUPERVISOR accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto RAPPIBANK - CUENTAS y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia LIBERACIÓN DE RETENCIONES
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | canal   | nuevoMotivo   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <canal> | <nuevoMotivo> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #RETIPIFICACIÓN
    And segun el tramite se resuelve o deriva a area respectiva hasta <resolver_o_retipificar>:<retipifica> el tramite <areaValidacionOpcional>
    #VALIDACION
    Then se valida en historial el tramite con su retipificacion
    #DERIVACION
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion Derivar-2 con area a derivar <areaValidacionOpcional>

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | canal   | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd | resolucion  | areaValidacionOpcional | resolver_o_retipificar | retipifica                                  | nuevoMotivo |
      | CASE01   | 1-2     | 3               | PEN    | 100   | App IBK | Si         | Si                   | Correo         | Registrado        | ACTIVA     | Retipificar |                        | RETIPIFICA             | RAPPIBANK_CUENTAS-RECLAMO-operacionDenegada | 1           |


  @happy_path @regresion @version3.1.9 @retipificacionRappiBankCuentas_test9
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida retipificación del trámite de tipo RECLAMO con producto RAPPIBANK - CUENTAS para tipología origen: OPERACIÓN DENEGADA - tipología destino: LIBERACIÓN DE RETENCIONES
    Given que el usuario SUPERVISOR accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto RAPPIBANK - CUENTAS y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia OPERACIÓN DENEGADA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | canal   | nuevoMotivo   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <canal> | <nuevoMotivo> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #RETIPIFICACIÓN
    And segun el tramite se resuelve o deriva a area respectiva hasta <resolver_o_retipificar>:<retipifica> el tramite <areaValidacionOpcional>
    #VALIDACION
    Then se valida en historial el tramite con su retipificacion
    #DERIVACION
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion Derivar-2 con area a derivar <areaValidacionOpcional>

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | canal   | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd | resolucion  | areaValidacionOpcional | resolver_o_retipificar | retipifica                                      | nuevoMotivo |
      | CASE01   | 1-2     | 3               | PEN    | 100   | App IBK | Si         | Si                   | Correo         | Registrado        | ACTIVA     | Retipificar |                        | RETIPIFICA             | RAPPIBANK_CUENTAS-RECLAMO-liberaciónRetenciones | 1           |


  @happy_path @regresion @version3.1.9 @retipificacionRappiBankCuentas_test10
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida retipificación del trámite de tipo RECLAMO con producto RAPPIBANK - CUENTAS para tipología origen: REVISIÓN DE DEPÓSITOS A TERCEROS - tipología destino: OPERACIÓN DENEGADA
    Given que el usuario SUPERVISOR accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto RAPPIBANK - CUENTAS y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia REVISIÓN DE DEPÓSITOS A TERCEROS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | canal   | nuevoMotivo   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <canal> | <nuevoMotivo> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #RETIPIFICACIÓN
    And segun el tramite se resuelve o deriva a area respectiva hasta <resolver_o_retipificar>:<retipifica> el tramite <areaValidacionOpcional>
    #VALIDACION
    Then se valida en historial el tramite con su retipificacion
    #DERIVACION
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion Derivar-2 con area a derivar <areaValidacionOpcional>

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | canal   | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd | resolucion  | areaValidacionOpcional | resolver_o_retipificar | retipifica                                  | nuevoMotivo |
      | CASE01   | 1       | 3               | PEN    | 100   | App IBK | Si         | Si                   | Correo         | Registrado        | ACTIVA     | Retipificar |                        | RETIPIFICA             | RAPPIBANK_CUENTAS-RECLAMO-operacionDenegada | 1           |


  @happy_path @regresion @version3.1.9 @retipificacionRappiBankCuentas_test11
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida retipificación del trámite de tipo RECLAMO con producto RAPPIBANK - CUENTAS para tipología origen: SISTEMA DE RECOMPENSAS - tipología destino: INSATISFACCIÓN GESTIÓN DE ENTREGA DE TARJETA
    Given que el usuario SUPERVISOR accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto RAPPIBANK - CUENTAS y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia SISTEMA DE RECOMPENSAS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | canal   | nuevoMotivo   |
      | <motivos> | <canal> | <nuevoMotivo> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #RETIPIFICACIÓN
    And segun el tramite se resuelve o deriva a area respectiva hasta <resolver_o_retipificar>:<retipifica> el tramite <areaValidacionOpcional>
    #VALIDACION
    Then se valida en historial el tramite con su retipificacion
    #DERIVACION
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion Derivar-2 con area a derivar <areaValidacionOpcional>

    Examples:
      | TESTCASE | motivos | canal   | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd | resolucion  | areaValidacionOpcional | resolver_o_retipificar | retipifica                                                    | nuevoMotivo |
      | CASE01   | 1       | App IBK | Si         | Si                   | Correo         | Registrado        | ACTIVA     | Retipificar |                        | RETIPIFICA             | RAPPIBANK_CUENTAS-RECLAMO-insatisfaccionGestionEntregaTarjeta | 1           |

