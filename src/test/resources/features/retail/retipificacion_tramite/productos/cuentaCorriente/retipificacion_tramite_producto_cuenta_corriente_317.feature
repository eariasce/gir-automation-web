@all_retipificacion_tramite_cuentaCorriente
Feature: Retipificación del trámite Producto de CUENTA CORRIENTE

  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- RETIPIFICAR PEDIDO-----------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.7 @retipificacionCuentaCorriente_test1
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida retipificación de trámite de tipo PEDIDO con producto LINEA CONVENIO para Tipología origen: COPIA DE BOLETA DE DEPÓSITO LIMA - Tipología destino:  REPORTE INDEBIDO CENTRALES DE RIESGO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto CUENTA CORRIENTE y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia COPIA DE BOLETA DE DEPÓSITO LIMA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | nuevoMotivo   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <nuevoMotivo> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #RETIPIFICACIÓN
    And segun el tramite se resuelve o deriva a area respectiva hasta <resolver_o_retipificar>:<retipifica> el tramite <areaValidacionOpcional>
    #VALIDACION
    Then se valida en historial el tramite con su retipificacion
    #DERIVACION
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion Derivar-2 con area a derivar <areaValidacionOpcional>

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd | resolucion | areaValidacionOpcional | resolver_o_retipificar | retipifica                                              | nuevoMotivo |
      | CASE01   | 1       | 3               | PEN    | 100   | Si         | Si                   | Correo         | Registrado        | ACTIVA     | Procede    |                        | RETIPIFICA             | CUENTA_CORRIENTE-RECLAMO-reporteIndebidoCentralesRiesgo | 1           |


  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- RETIPIFICAR RECLAMO----------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.7 @retipificacionCuentaCorriente_test2
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida retipificación de trámite de tipo PEDIDO con producto LINEA CONVENIO para Tipología origen: REPORTE INDEBIDO CENTRALES DE RIESGO - Tipología destino: TC - INSUFICIENTE INFORMACIÓN
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto CUENTA CORRIENTE y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia REPORTE INDEBIDO CENTRALES DE RIESGO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | canal   | nuevoMotivo   |
      | <motivos> | <canal> | <nuevoMotivo> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #RETIPIFICACIÓN
    And segun el tramite se resuelve o deriva a area respectiva hasta <resolver_o_retipificar>:<retipifica> el tramite <areaValidacionOpcional>
    #VALIDACION
    Then se valida en historial el tramite con su retipificacion
    #DERIVACION
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion Derivar-2 con area a derivar <areaValidacionOpcional>

    Examples:
      | TESTCASE | motivos | canal   | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd | resolucion  | areaValidacionOpcional | resolver_o_retipificar | retipifica                         | nuevoMotivo |
      | CASE01   | 1       | App IBK | Si         | Si                   | Correo         | Registrado        | ACTIVA     | Retipificar |                        | RETIPIFICA             | TC-RECLAMO-insuficienteInformacion | 1           |

