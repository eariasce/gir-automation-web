@all_retipificacion_tramite_tarjetaCredito
Feature: Retipificación del Trámite de Producto TARJETA DE CREDITO

  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- RETIPIFICAR PEDIDO------------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.7 @retipificacionTarjetaCredito_test37
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida retipificación de trámite de tipo PEDIDO con producto TARJETA DE CREDITO para tipología origen: CORRECCION DE PAGO REALIZADO - tipología destino: ENDOSO SEGUROS DE DESGRAVAMEN
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia CORRECCIÓN DE PAGO REALIZADO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | canal   | nuevoMotivo   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <canal> | <nuevoMotivo> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #RETIPIFICACIÓN
    And segun el tramite se resuelve o deriva a area respectiva hasta <resolver_o_retipificar>:<retipifica> el tramite <areaValidacionOpcional>
    #VALIDACION
    Then se valida en historial el tramite con su retipificacion
    #DERIVACION
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion Derivar-2 con area a derivar <areaValidacionOpcional>

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | canal   | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd | resolucion  | areaValidacionOpcional | resolver_o_retipificar | retipifica                         | nuevoMotivo |
      | CASE01   | 1       | 3               | PEN    | 100   | App IBK | Si         | Si                   | Correo         | Registrado        | ACTIVA     | Retipificar |                        | RETIPIFICA             | TC-PEDIDO-endosoSegurosDesgravamen | 1           |


  @happy_path @regresion @version3.1.7 @retipificacionTarjetaCredito_test38
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida retipificación de trámite de tipo PEDIDO con producto TARJETA DE CREDITO para tipología origen: ENDOSO SEGUROS DE DESGRAVAMEN - tipología destino: MODIFICACIÓN DISPOSICIÓN DE EFECTIVO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia ENDOSO SEGUROS DE DESGRAVAMEN
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | nuevoMotivo   |
      | <motivos> | <nuevoMotivo> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #RETIPIFICACIÓN
    And segun el tramite se resuelve o deriva a area respectiva hasta <resolver_o_retipificar>:<retipifica> el tramite <areaValidacionOpcional>
    #VALIDACION
    Then se valida en historial el tramite con su retipificacion
    #DERIVACION
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion Derivar-2 con area a derivar <areaValidacionOpcional>

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd | resolucion  | areaValidacionOpcional | resolver_o_retipificar | retipifica                                | nuevoMotivo |
      | CASE01   | 1       | Si         | Si                   | Correo         | Registrado        | ACTIVA     | Retipificar |                        | RETIPIFICA             | TC-PEDIDO-modificacionDisposicionEfectivo | 1           |


  @happy_path @regresion @version3.1.7 @retipificacionTarjetaCredito_test39
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida retipificación de trámite de tipo PEDIDO con producto TARJETA DE CREDITO para tipología origen: MODIFICACIÓN DISPOSICIÓN DE EFECTIVO - tipología destino: REVERSIÓN DE UPGRADE
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia MODIFICACIÓN DISPOSICIÓN DE EFECTIVO
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | agregarTarjetaCredito   | nuevoMotivo   |
      | <motivos> | <agregarTarjetaCredito> | <nuevoMotivo> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #RETIPIFICACIÓN
    And segun el tramite se resuelve o deriva a area respectiva hasta <resolver_o_retipificar>:<retipifica> el tramite <areaValidacionOpcional>
    #VALIDACION
    Then se valida en historial el tramite con su retipificacion
    #DERIVACION
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion Derivar-2 con area a derivar <areaValidacionOpcional>

    Examples: #SOLO PERMITE 1 MOTIVO PARA REGISTRAR - PARA ESTE CASO SOLO SE PUEDE AGREGAR DOS TARJETA DE CREDITO YA QUE ESTE CLIENTE 40402131 TIENE SOLO DOS TARJETA DE CREDITO QUE AGREGAR
      | TESTCASE | motivos | agregarTarjetaCredito | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd | resolucion  | areaValidacionOpcional | resolver_o_retipificar | retipifica                 | nuevoMotivo |
      | CASE01   | 1       | 2                     | Si         | Si                   | Correo         | Registrado        | ACTIVA     | Retipificar |                        | RETIPIFICA             | TC-PEDIDO-reversionUpgrade | 1           |


  @happy_path @regresion @version3.1.7 @retipificacionTarjetaCredito_test40
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida retipificación de trámite de tipo PEDIDO con producto TARJETA DE CREDITO para tipología origen: REVERSIÓN DE UPGRADE - tipología destino: MODIFICACIÓN DISPOSICIÓN DE EFECTIVO
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia REVERSIÓN DE UPGRADE
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | nuevoMotivo   |
      | <motivos> | <nuevoMotivo> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #RETIPIFICACIÓN
    And segun el tramite se resuelve o deriva a area respectiva hasta <resolver_o_retipificar>:<retipifica> el tramite <areaValidacionOpcional>
    #VALIDACION
    Then se valida en historial el tramite con su retipificacion
    #DERIVACION
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion Derivar-2 con area a derivar <areaValidacionOpcional>

    Examples:
      | TESTCASE | motivos | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd | resolucion  | areaValidacionOpcional | resolver_o_retipificar | retipifica                                | nuevoMotivo |
      | CASE01   | 1       | Si         | Si                   | Correo         | Registrado        | ACTIVA     | Retipificar |                        | RETIPIFICA             | TC-PEDIDO-modificacionDisposicionEfectivo | 1           |

