@all_retipificacion_tramite_pagosPlanillasProveedores
Feature: Retipificación del trámite Operaciones de PAGOS PLANILLAS/PROVEEDORES

  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- RETIPIFICAR PEDIDO-------------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.7 @retipificacionPagosPlanillasProveedores_test1
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida retipificación del trámite de tipo PEDIDO con operaciones PAGOS PLANILLAS/PROVEEDORES para tipología origen: INFORMACION DE MOVIMIENTOS - tipología destino: PAGOS PLANILLAS/PROVEEDORES TRANSACCIÓN MAL O NO PROCESADA
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto PAGOS PLANILLAS/PROVEEDORES
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite PEDIDO y la tipologia INFORMACION DE MOVIMIENTOS
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | nuevoMotivo   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <nuevoMotivo> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #RETIPIFICACIÓN
    And segun el tramite se resuelve o deriva a area respectiva hasta <resolver_o_retipificar>:<retipifica> el tramite <areaValidacionOpcional>
    #VALIDACION
    Then se valida en historial el tramite con su retipificacion
    #DERIVACION
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion Derivar-2 con area a derivar <areaValidacionOpcional>

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion  | areaValidacionOpcional | resolver_o_retipificar | retipifica                                                     | nuevoMotivo |
      | CASE01   | 1       | 3               | PEN    | 100   | Si         | Si                   | Correo         | Registrado        | Retipificar |                        | RETIPIFICA             | PAGOS_PLANILLAS_PROVEEDORES-RECLAMO-transaccionMalONoProcesada | 1           |


  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- RETIPIFICAR RECLAMO-----------------------------------------
  #===========================================================================================================

  @happy_path @regresion @version3.1.7 @retipificacionPagosPlanillasProveedores_test2
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida retipificación del trámite de tipo RECLAMO con operaciones PAGOS PLANILLAS/PROVEEDORES para tipología origen: TRANSACCIÓN MAL O NO PROCESADA - tipología destino: TC - INSUFICIENTE INFORMACIÓN
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto PAGOS PLANILLAS/PROVEEDORES
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite RECLAMO y la tipologia TRANSACCIÓN MAL O NO PROCESADA
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | canal   | nuevoMotivo   |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <canal> | <nuevoMotivo> |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador MOVISTAR
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se valida usuario reasignado por servicios en bandeja
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #RETIPIFICACIÓN
    And segun el tramite se resuelve o deriva a area respectiva hasta <resolver_o_retipificar>:<retipifica> el tramite <areaValidacionOpcional>
    #VALIDACION
    Then se valida en historial el tramite con su retipificacion
    #DERIVACION
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion Derivar-2 con area a derivar <areaValidacionOpcional>

    Examples:
      | TESTCASE | motivos | cantMovimientos | moneda | monto | canal   | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion  | areaValidacionOpcional | resolver_o_retipificar | retipifica                         | nuevoMotivo |
      | CASE01   | 1       | 3               | PEN    | 100   | App IBK | Si         | Si                   | Correo         | Registrado        | Retipificar |                        | RETIPIFICA             | TC-RECLAMO-insuficienteInformacion | 1           |

