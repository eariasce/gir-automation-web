@segunda_instancia
Feature: Resolucion de trámite de Producto TARJETA DE CREDITO

  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- RESOLUCION PEDIDO -----------------------------------------
  #===========================================================================================================

  @happy_path @resolucionsegundainstancia
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolucion de trámite de tipo PEDIDO con producto TARJETA DE CREDITO para la tipología: EXONERACIÓN DE COBROS cuando PROCEDE o NO PROCEDE
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU en segunda instancia con tramite <Tramite>
   #SEGUNDA INSTANCIA
    #And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    #And se selecciona Historial del menu principal
    #Then se valida en historial el tramite con su resolucion
 #SE INICIA LA RESOLUCION
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en segunda instancia en la busqueda para seleccionarlo
    |tipoProducto | tipologia   |indicaArchivoAdjunto| carta|
     | <Producto>   | <Tipologia> |<indicaArchivoAdjunto>|<carta>|
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion

    Examples:
      | Tramite | Tipologia                                   | Producto                          | resolucion | areaValidacionOpcional | carta | indicaArchivoAdjunto |
      | 216597  | CAMBIO DE FORMA DE ENVÍO DE CORRESPONDENCIA | EFECTIVO CON GARANTÍA HIPOTECARIO | Procede    |                        | Si    | Si                   |
