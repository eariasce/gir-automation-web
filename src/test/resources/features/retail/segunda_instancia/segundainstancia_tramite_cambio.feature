@segunda_instancia
Feature: Resolucion de trámite de Producto TARJETA DE CREDITO

  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- RESOLUCION PEDIDO -----------------------------------------
  #===========================================================================================================

  @happy_path @segundainstancia
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida resolucion de trámite de tipo PEDIDO con producto TARJETA DE CREDITO para la tipología: EXONERACIÓN DE COBROS cuando PROCEDE o NO PROCEDE
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite "<numeroTramite>" con su resolucion a segunda instancia
    #SEGUNDA INSTANCIA
    And se continua con la validacion del flujo en segunda instancia
    #And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    #And se selecciona Historial del menu principal
    #Then se valida en historial el tramite con su resolucion

    Examples:
      | numeroTramite |
      |	217192	|
