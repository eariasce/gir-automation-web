@segunda_instancia
Feature: Registro y resolucion de trámite en segunda instancia

  # ----------------------------------------------------------------------------------------------------------
  # ---------------------------------------------- RESOLUCION PEDIDO -----------------------------------------
  #===========================================================================================================

  @happy_path @regresion @registroSegundaInstanciaTC
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto TARJETA DE CREDITO para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto TARJETA DE CREDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | carta   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <carta> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion a segunda instancia
    And se continua con la validacion del flujo en segunda instancia
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION DE 2DA INSTANCIA
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion
    Examples:
      | TESTCASE | TIPOLOGIA                                     | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd | resolucion | areaValidacionOpcional | carta |
      | CASE01   | COBROS INDEBIDOS                              | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE02   | CONSUMOS NO RECONOCIDOS POR FRAUDE            | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE03   | CONSUMOS MAL PROCESADOS POR EL COMERCIO/MARCA | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE04   | SISTEMA DE RECOMPENSAS                        | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE05   | INSUFICIENTE INFORMACIÓN                      | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE06   | INCORRECTA APLICACIÓN CD/EC/C. CUOTAS         | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE07   | NO RECEPCIÓN DE DOCUMENTO                     | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE08   | LIBERACION DE RETENCIONES                     | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE09   | OPERACIÓN DENEGADA                            | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE10   | DESACUERDO DE CONDICIONES/TASAS/TARIFAS       | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE11   | REPORTE INDEBIDO CENTRALES DE RIESGO          | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE12   | INCUMPLIMIENTO DE CAMPAÑA                     | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE13   | ATM DE IB NO DEPOSITÓ DINERO                  | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |

  @happy_path @regresion @registroSegundaInstanciaTD
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto TARJETA DE DEBITO para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto TARJETA DE DEBITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | carta   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <carta> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion a segunda instancia
    And se continua con la validacion del flujo en segunda instancia
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION DE 2DA INSTANCIA
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion
    Examples:
      | TESTCASE | TIPOLOGIA                                     | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd | resolucion | areaValidacionOpcional | carta |
      | CASE01   | COBROS INDEBIDOS                              | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE02   | CONSUMOS NO RECONOCIDOS POR FRAUDE            | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE03   | CONSUMOS MAL PROCESADOS POR EL COMERCIO/MARCA | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE04   | LIBERACION DE RETENCIONES                     | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE05   | OPERACIÓN DENEGADA                            | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE06   | ATM DE IB NO ENTREGÓ DINERO                   | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE07   | ATM DE OTRO BANCO NO ENTREGÓ DINERO           | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE08   | REVISIÓN DE DEPÓSITOS A TERCEROS              | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |

  @happy_path @regresion @registroSegundaInstanciaSeg
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto SEGUROS para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto SEGUROS y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | carta   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <carta> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion a segunda instancia
    And se continua con la validacion del flujo en segunda instancia
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION DE 2DA INSTANCIA
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion
    Examples:
      | TESTCASE | TIPOLOGIA                 | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd | resolucion | areaValidacionOpcional | carta |
      | CASE01   | COBROS INDEBIDOS          | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE02   | NO RECEPCIÓN DE DOCUMENTO | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |

  @happy_path @regresion @registroSegundaInstanciaRappiTC
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto RAPPIBANK - TARJETA DE CRÉDITO para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto RAPPIBANK - TARJETA DE CRÉDITO y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | carta   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <carta> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion a segunda instancia
    And se continua con la validacion del flujo en segunda instancia
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION DE 2DA INSTANCIA
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion
    Examples:
      | TESTCASE | TIPOLOGIA                                    | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd | resolucion | areaValidacionOpcional | carta |
      | CASE01   | ATM DE IB NO ENTREGÓ DINERO                  | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE02   | CORREO/TELÉFONO INDEBIDO DE CAMPAÑAS         | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE03   | DESACUERDO DE CONDICIONES/TASAS/TARIFAS      | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE04   | DIRECCIÓN/TELF. INDEBIDOS DE COBRANZA        | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE05   | INSATISFACCIÓN GESTIÓN DE ENTREGA DE TARJETA | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE06   | REPORTE INDEBIDO CENTRALES DE RIESGO         | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE07   | TRANSACCIÓN MAL O NO PROCESADA               | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE08   | COBROS INDEBIDOS                             | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE09   | CONSUMO/ RETIRO NO EFECTUADO                 | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE10   | LIBERACIÓN DE RETENCIONES                    | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE11   | NO RECEPCIÓN DE DOCUMENTO                    | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE12   | OPERACIÓN DENEGADA                           | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE13   | SISTEMA DE RECOMPENSAS                       | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
    #REVISAR
      | CASE14   | OTROS                                        | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |

  @happy_path @regresion @registroSegundaInstanciaRappiCuent
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto RAPPIBANK - CUENTAS para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto RAPPIBANK - CUENTAS y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | carta   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <carta> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion a segunda instancia
    And se continua con la validacion del flujo en segunda instancia
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION DE 2DA INSTANCIA
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion
    Examples:
      | TESTCASE | TIPOLOGIA                                    | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd | resolucion | areaValidacionOpcional | carta |
      | CASE01   | COBROS INDEBIDOS                             | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE02   | TRANSACCIÓN MAL O NO PROCESADA               | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE03   | CONSUMO/ RETIRO NO EFECTUADO                 | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE04   | CUENTA/CANCELACIÓN NO RECONOCIDA             | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE05   | DESACUERDO CON EL SERVICIO                   | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE06   | DESACUERDO DE CONDICIONES/TASAS/TARIFAS      | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE07   | INSATISFACCIÓN GESTIÓN DE ENTREGA DE TARJETA | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE08   | LIBERACIÓN DE RETENCIONES                    | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE09   | OPERACIÓN DENEGADA                           | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE10   | REVISIÓN DE DEPÓSITOS A TERCEROS             | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE11   | SISTEMA DE RECOMPENSAS                       | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |

  @happy_path @regresion @registroSegundaInstanciaCC
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto CUENTA CORRIENTE para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto CUENTA CORRIENTE y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | prorroga   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <prorroga> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion a segunda instancia
    And se continua con la validacion del flujo en segunda instancia
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION DE 2DA INSTANCIA
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion
    Examples:
      | TESTCASE | TIPOLOGIA                            | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd | resolucion | areaValidacionOpcional | carta |
      | CASE01   | REPORTE INDEBIDO CENTRALES DE RIESGO | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |

  @happy_path @regresion @registroSegundaInstanciaCuent
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto CUENTA para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con el producto CUENTA y estado <estadoProd>
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | carta   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <carta> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion a segunda instancia
    And se continua con la validacion del flujo en segunda instancia
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION DE 2DA INSTANCIA
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion
    Examples:
      | TESTCASE | TIPOLOGIA                               | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | estadoProd | resolucion | areaValidacionOpcional | carta |
      | CASE01   | COBROS INDEBIDOS                        | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE02   | CUENTA/CANCELACIÓN NO RECONOCIDA        | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE03   | SISTEMA DE RECOMPENSAS                  | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE04   | DESACUERDO DE CONDICIONES/TASAS/TARIFAS | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE05   | NO RECEPCIÓN DE DOCUMENTO               | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE06   | LIBERACIÓN DE RETENCIONES JUDICIALES    | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE07   | ATM DE IB NO DEPOSITÓ DINERO            | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE08   | SOLICITUD DE ALCANCÍA                   | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE09   | INCUMPLIMIENTO DE CAMPAÑA               | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |
      | CASE10   | TRANSACCIÓN MAL O NO PROCESADA          | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | ACTIVA     | Procede    |                        | Si    |

  @happy_path @regresion @registroSegundaInstanciaAdeSuel
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto ADELANTO SUELDO para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto ADELANTO SUELDO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | carta   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <carta> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion a segunda instancia
    And se continua con la validacion del flujo en segunda instancia
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION DE 2DA INSTANCIA
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion
    Examples:
      | TESTCASE | TIPOLOGIA                    | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | CONSUMO/ RETIRO NO EFECTUADO | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE02   | COBROS INDEBIDOS             | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |

  @happy_path @regresion @registroSegundaInstanciaLinConv
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto LINEA CONVENIO para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto LINEA CONVENIO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | carta   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <carta> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion a segunda instancia
    And se continua con la validacion del flujo en segunda instancia
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION DE 2DA INSTANCIA
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion
    Examples:
      | TESTCASE | TIPOLOGIA                               | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | TRANSACCIÓN MAL O NO PROCESADA          | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE02   | DESACUERDO DE CONDICIONES/TASAS/TARIFAS | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE03   | NO RECEPCIÓN DE DOCUMENTO               | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE04   | CONSUMO/ RETIRO NO EFECTUADO            | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE05   | REPORTE INDEBIDO CENTRALES DE RIESGO    | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
 #Revisar
      | CASE06   | DUPLICADO DE CRONOGRAMA                 | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE07   | DUPLICADO DE CRONOGRAMA (C)             | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE08   | LIQUIDACIÓN DE DEUDA                    | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE09   | LIQUIDACIÓN DE DEUDA(C)                 | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |


  @happy_path @regresion @registroSegundaInstanciaEGH
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto EFECTIVO CON GARANTÍA HIPOTECARIO para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto EFECTIVO CON GARANTÍA HIPOTECARIO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | carta   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <carta> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion a segunda instancia
    And se continua con la validacion del flujo en segunda instancia
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION DE 2DA INSTANCIA
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion
    Examples:
      | TESTCASE | TIPOLOGIA                                | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | COBROS INDEBIDOS                         | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE02   | DESACUERDO DE CONDICIONES/TASAS/TARIFAS  | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE03   | REPORTE INDEBIDO CENTRALES DE RIESGO     | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE04   | TRANSACCIÓN MAL O NO PROCESADA           | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE05   | CONSUMO/ RETIRO NO EFECTUADO             | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE06   | NO RECEPCIÓN DE DOCUMENTO                | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
 #REVISAR
      | CASE07   | PAGO ANTICIPADO Y ADELANTO DE CUOTAS BPI | PEDIDO      | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE08   | ANULACIÓN DE CRÉDITO                     | PEDIDO      | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |

  @happy_path @regresion @registroSegundaInstanciaHip
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto HIPOTECARIO para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto HIPOTECARIO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | carta   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <carta> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion a segunda instancia
    And se continua con la validacion del flujo en segunda instancia
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION DE 2DA INSTANCIA
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion
    Examples:
      | TESTCASE | TIPOLOGIA                               | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | DESACUERDO DE CONDICIONES/TASAS/TARIFAS | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE02   | NO RECEPCIÓN DE DOCUMENTO               | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE03   | TRANSACCIÓN MAL O NO PROCESADA          | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE04   | COBROS INDEBIDOS                        | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE05   | REPORTE INDEBIDO CENTRALES DE RIESGO    | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE06   | CONSUMO/ RETIRO NO EFECTUADO            | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |

  @happy_path @regresion @registroSegundaInstanciaPrestPers
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto PRÉSTAMO PERSONAL para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto PRÉSTAMO PERSONAL
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | carta   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <carta> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion a segunda instancia
    And se continua con la validacion del flujo en segunda instancia
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION DE 2DA INSTANCIA
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion
    Examples:
      | TESTCASE | TIPOLOGIA                                | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | COBROS INDEBIDOS                         | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE02   | DESACUERDO DE CONDICIONES/TASAS/TARIFAS  | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE03   | REPORTE INDEBIDO CENTRALES DE RIESGO     | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE04   | TRANSACCIÓN MAL O NO PROCESADA           | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE05   | NO RECEPCIÓN DE DOCUMENTO                | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE06   | CONSUMO/ RETIRO NO EFECTUADO             | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
#REVISAR
      | CASE07   | DUPLICADO DE CRONOGRAMA BPI              | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE08   | DUPLICADO DE DOCUMENTO                   | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE09   | PAGO ANTICIPADO Y ADELANTO DE CUOTAS BPI | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |

  @happy_path @regresion @registroSegundaInstanciaVehic
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto VEHICULAR para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto VEHICULAR
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | carta   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <carta> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion a segunda instancia
    And se continua con la validacion del flujo en segunda instancia
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION DE 2DA INSTANCIA
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion
    Examples:
      | TESTCASE | TIPOLOGIA                                | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | COBROS INDEBIDOS                         | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE02   | DESACUERDO DE CONDICIONES/TASAS/TARIFAS  | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE03   | REPORTE INDEBIDO CENTRALES DE RIESGO     | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE04   | TRANSACCIÓN MAL O NO PROCESADA           | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE05   | CONSUMO/ RETIRO NO EFECTUADO             | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE06   | NO RECEPCIÓN DE DOCUMENTO                | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
#REVISAR
      | CASE07   | DUPLICADO DE CRONOGRAMA                  | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE08   | DUPLICADO DE CRONOGRAMA BPI              | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE09   | ENDOSO SEGUROS DE DESGRAVAMEN            | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE10   | LEVANTAMIENTO DE GARANTÍA                | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE11   | PAGO ANTICIPADO Y ADELANTO DE CUOTAS BPI | PEDIDO      | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE12   | ENDOSO SEGUROS DEL BIEN                  | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |

  @happy_path @regresion @registroSegundaInstanciaCartFianCert
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto CARTA FIANZA/CERTIFICADO para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto CARTA FIANZA/CERTIFICADO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | carta   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <carta> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion a segunda instancia
    And se continua con la validacion del flujo en segunda instancia
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION DE 2DA INSTANCIA
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion
    Examples:
      | TESTCASE | TIPOLOGIA              | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | DUPLICADO DE DOCUMENTO | PEDIDO      | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |


  @happy_path @regresion @registroSegundaInstanciaTunki
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto TUNKI para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TUNKI
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | carta   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <carta> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion a segunda instancia
    And se continua con la validacion del flujo en segunda instancia
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION DE 2DA INSTANCIA
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion
    Examples:
      | TESTCASE | TIPOLOGIA                      | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | TRANSACCIÓN MAL O NO PROCESADA | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE02   | ATM DE IB NO ENTREGÓ DINERO    | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |


  @happy_path @regresion @registroSegundaInstanciaTransfNac
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto TRANSF. NACIONALES para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TRANSF. NACIONALES
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | carta   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <carta> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion a segunda instancia
    And se continua con la validacion del flujo en segunda instancia
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION DE 2DA INSTANCIA
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion
    Examples:
      | TESTCASE | TIPOLOGIA                      | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | TRANSACCIÓN MAL O NO PROCESADA | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE02   | COBROS INDEBIDOS               | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |

  @happy_path @regresion @registroSegundaInstanciaTransfInter
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto TRANSF. INTERNACIONALES para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TRANSF. INTERNACIONALES
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | carta   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <carta> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion a segunda instancia
    And se continua con la validacion del flujo en segunda instancia
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION DE 2DA INSTANCIA
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion
    Examples:
      | TESTCASE | TIPOLOGIA                            | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | TRANSACCIÓN MAL O NO PROCESADA       | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE02   | COBROS INDEBIDOS                     | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE03   | NO RECEPCIÓN DE TRANSFERENCIA IN/OUT | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |

  @happy_path @regresion @registroSegundaInstanciaToken
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto TOKEN para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto TOKEN
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | carta   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <carta> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion a segunda instancia
    And se continua con la validacion del flujo en segunda instancia
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION DE 2DA INSTANCIA
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion
    Examples:
      | TESTCASE | TIPOLOGIA                      | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | TRANSACCIÓN MAL O NO PROCESADA | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |

  @happy_path @regresion @registroSegundaInstanciaSunat
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto SUNAT para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto SUNAT
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | carta   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <carta> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion a segunda instancia
    And se continua con la validacion del flujo en segunda instancia
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION DE 2DA INSTANCIA
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion
    Examples:
      | TESTCASE | TIPOLOGIA                      | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | TRANSACCIÓN MAL O NO PROCESADA | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE02   | COBROS INDEBIDOS               | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |

  @happy_path @regresion @registroSegundaInstanciaRem
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto REMESAS para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto REMESAS
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | carta   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <carta> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion a segunda instancia
    And se continua con la validacion del flujo en segunda instancia
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION DE 2DA INSTANCIA
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion
    Examples:
      | TESTCASE | TIPOLOGIA                            | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | ATM DE IB NO ENTREGÓ DINERO          | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE02   | COBROS INDEBIDOS                     | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE03   | NO RECEPCIÓN DE TRANSFERENCIA IN/OUT | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE04   | CONSUMO/ RETIRO NO EFECTUADO         | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |

  @happy_path @regresion @registroSegundaInstanciaPlin
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto PLIN para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto PLIN
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | carta   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <carta> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion a segunda instancia
    And se continua con la validacion del flujo en segunda instancia
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION DE 2DA INSTANCIA
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion
    Examples:
      | TESTCASE | TIPOLOGIA                      | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | TRANSACCIÓN MAL O NO PROCESADA | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE02   | ATM DE IB NO ENTREGÓ DINERO    | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |

  @happy_path @regresion @registroSegundaInstanciaPayPal
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto PAY PAL para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto PAY PAL
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | carta   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <carta> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion a segunda instancia
    And se continua con la validacion del flujo en segunda instancia
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION DE 2DA INSTANCIA
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion
    Examples:
      | TESTCASE | TIPOLOGIA                      | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | TRANSACCIÓN MAL O NO PROCESADA | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |

  @happy_path @regresion @registroSegundaInstanciaPagPlanProv
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto PAGOS PLANILLAS/PROVEEDORES para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto PAGOS PLANILLAS/PROVEEDORES
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | carta   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <carta> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion a segunda instancia
    And se continua con la validacion del flujo en segunda instancia
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION DE 2DA INSTANCIA
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion
    Examples:
      | TESTCASE | TIPOLOGIA                      | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | TRANSACCIÓN MAL O NO PROCESADA | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE02   | COBROS INDEBIDOS               | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |

  @happy_path @regresion @registroSegundaInstanciaPagServCan
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto PAGO DE SERVICIO POR CANALES para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto PAGO DE SERVICIO POR CANALES
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | carta   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <carta> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion a segunda instancia
    And se continua con la validacion del flujo en segunda instancia
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION DE 2DA INSTANCIA
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion
    Examples:
      | TESTCASE | TIPOLOGIA                      | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | TRANSACCIÓN MAL O NO PROCESADA | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE02   | ATM DE IB NO DEPOSITÓ DINERO   | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE03   | COBROS INDEBIDOS               | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |

  @happy_path @regresion @registroSegundaInstanciaMonExApp
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto MONEY EXCHANGE APP para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto MONEY EXCHANGE APP
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | carta   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <carta> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion a segunda instancia
    And se continua con la validacion del flujo en segunda instancia
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION DE 2DA INSTANCIA
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion
    Examples:
      | TESTCASE | TIPOLOGIA                      | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | TRANSACCIÓN MAL O NO PROCESADA | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |

  @happy_path @regresion @registroSegundaInstanciaIbAg
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto IB AGENTE para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto IB AGENTE
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | carta   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <carta> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion a segunda instancia
    And se continua con la validacion del flujo en segunda instancia
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION DE 2DA INSTANCIA
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion
    Examples:
      | TESTCASE | TIPOLOGIA                      | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | TRANSACCIÓN MAL O NO PROCESADA | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE02   | COBROS INDEBIDOS               | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |

  @happy_path @regresion @registroSegundaInstanciaDebAut
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto DEBITO AUTOMATICO para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto DEBITO AUTOMATICO
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | carta   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <carta> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion a segunda instancia
    And se continua con la validacion del flujo en segunda instancia
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION DE 2DA INSTANCIA
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion
    Examples:
      | TESTCASE | TIPOLOGIA                      | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | TRANSACCIÓN MAL O NO PROCESADA | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE02   | COBROS INDEBIDOS               | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |

  @happy_path @regresion @registroSegundaInstanciaCobr
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto COBRANZAS para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto COBRANZAS
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | carta   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <carta> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion a segunda instancia
    And se continua con la validacion del flujo en segunda instancia
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION DE 2DA INSTANCIA
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion
    Examples:
      | TESTCASE | TIPOLOGIA                             | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | DIRECCIÓN/TELF. INDEBIDOS DE COBRANZA | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE02   | COBROS INDEBIDOS                      | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |

  @happy_path @regresion @registroSegundaInstanciaBillElec
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto BILLETERA ELECTRÓNICA para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto BILLETERA ELECTRÓNICA
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | carta   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <carta> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion a segunda instancia
    And se continua con la validacion del flujo en segunda instancia
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION DE 2DA INSTANCIA
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion
    Examples:
      | TESTCASE | TIPOLOGIA                      | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | TRANSACCIÓN MAL O NO PROCESADA | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE02   | CONSUMO/ RETIRO NO EFECTUADO   | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |

  @happy_path @regresion @registroSegundaInstanciaBancInt
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto BANCA POR INTERNET para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto BANCA POR INTERNET
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | carta   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <carta> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion a segunda instancia
    And se continua con la validacion del flujo en segunda instancia
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION DE 2DA INSTANCIA
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion
    Examples:
      | TESTCASE | TIPOLOGIA                      | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | TRANSACCIÓN MAL O NO PROCESADA | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |

  @happy_path @regresion @registroSegundaInstanciaBancCel
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto BANCA CELULAR para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto BANCA CELULAR
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | carta   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <carta> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion a segunda instancia
    And se continua con la validacion del flujo en segunda instancia
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION DE 2DA INSTANCIA
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion
    Examples:
      | TESTCASE | TIPOLOGIA                      | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | TRANSACCIÓN MAL O NO PROCESADA | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |

  @happy_path @regresion @registroSegundaInstanciaAppInt
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto APP INTERBANK para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto APP INTERBANK
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | carta   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <carta> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion a segunda instancia
    And se continua con la validacion del flujo en segunda instancia
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION DE 2DA INSTANCIA
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion
    Examples:
      | TESTCASE | TIPOLOGIA                      | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | TRANSACCIÓN MAL O NO PROCESADA | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |

  @happy_path @regresion @registroSegundaInstanciaAtCli
  Scenario Outline: <TESTCASE> [HAPPY PATH] Valida registro y prórroga del trámite de tipo <TIPOTRAMITE> con producto ATENCION AL CLIENTE para la tipología: <TIPOLOGIA>
    Given que el usuario ESPECIALISTA accede a la aplicacion GIRU
    And se obtiene informacion de cliente a traves de TDM con producto ATENCION AL CLIENTE
    #el tipo de producto se obtiene de TDM y se maneja internamente
    And se inicia con el proceso de registro de tramite
    And se elige el tipo de tramite <TIPOTRAMITE> y la tipologia <TIPOLOGIA>
    And se selecciona uno o varios MOTIVOS y se agregan movimientos
      | motivos   | CantMovimientosPorMotivo | moneda   | monto   | carta   | tipoPrograma | cantidad | importe | aplicaAbono     | origenSaldo                | Tienda | Autonomia | GrupoLiquidacionActual | GrupoLiquidacionSolicitado | MonedaTramite | LineaActual | LineaNueva | ImporteSolicitado | NombreMotivo           | tipoDocumentoTramite | canal   | agregarTarjetaCredito | tipoEnvio    | beneficioReclamado | importeSolicitado | importeReclamado | medioAbono         | productoServicioAdquirido | tipoServicio | tipoFiltroProducto | quePasoConLaTarjeta                | tipoCuota | tipoTransferencia | medioPago       | tipoOperacion | tipoRemesa | formaDePago | tipoProducto | getMotivoSucedidoTarjeta | flagTarjetaBloqueo |
      | <motivos> | <cantMovimientos>        | <moneda> | <monto> | <carta> | Efectivo     | 100      | 100     | Abono en cuenta | Devolución Establecimiento | Si     | No        | Pag. 1 Fact 7          | Pag. 5 Fact 11             | PEN           | 2000        | 1000       | 200               | Afiliación pago mínimo | Contrato             | App IBK | 1                     | Envío físico | Cashback           | 100               | 100              | Tarjeta de Crédito |                           |              | Tarjeta de Crédito | Fue robada o usada en un secuestro | Ordinaria | Entrada           | Cargo en Cuenta | Depósito      | Entregada  | Cuenta      | Cuenta       | CFS002                   | No                 |
    And se ingresa una respuesta al cliente "Si" y un comentario "<comentario>" opcional
    And se adjunta opcionalmente un archivo "<indicaArchivoAdjunto>"
    And se selecciona o añade un numero de celular REGISTRADO y un operador CLARO
    And se selecciona el medio de respuesta "<medioRespuesta>" segun la configuracion de la respuesta "<nuevoRegistroRpta>"
    And se registra el tramite
    And se realiza la identificacion del cliente
    And se verifica que el nuevo usuario asignado pueda visualizar y atender el tramite
    And se selecciona Historial del menu principal
    Then se valida en historial el reclamo registrado y seguimiento
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion a segunda instancia
    And se continua con la validacion del flujo en segunda instancia
    And se cierra sesion del usuario
    #SE INICIA LA RESOLUCION DE 2DA INSTANCIA
    Given que se ingresara con el usuario y password que tenga el tramite reasignado en bandeja
    And se selecciona Tramites del menu principal
    When se ingresa el numero de tramite en la busqueda para seleccionarlo
    And se abre la vista del tramite de Resumen y selecciona el tipo de resolucion <resolucion> con area a derivar <areaValidacionOpcional>
    And ingresa comentario comentarioInterno opcional
    And se adjuntara un archivo adicional en caso sea necesario
    And se confirma la resolucion del tramite
    #STEP
    And segun el tramite se resuelve o deriva a area respectiva hasta resolver el tramite <areaValidacionOpcional>
    And se selecciona Historial del menu principal
    Then se valida en historial el tramite con su resolucion
    Examples:
      | TESTCASE | TIPOLOGIA                                    | TIPOTRAMITE | motivos | cantMovimientos | moneda | monto | comentario | indicaArchivoAdjunto | medioRespuesta | nuevoRegistroRpta | resolucion | areaValidacionOpcional | carta |
      | CASE01   | DESACUERDO CON EL SERVICIO                   | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE02   | BILLETE FALSO                                | RECLAMO     | 1       | 1               | PEN    | 100   | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE03   | CORREO/TELÉFONO INDEBIDO DE CAMPAÑAS         | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
      | CASE04   | INSATISFACCIÓN GESTIÓN DE ENTREGA DE TARJETA | RECLAMO     | 1       |                 |        |       | Si         | Si                   | Direccion      | Registrado        | Procede    |                        | Si    |
